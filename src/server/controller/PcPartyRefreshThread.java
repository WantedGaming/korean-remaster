package server.controller;

import java.util.Iterator;

import javolution.util.FastTable;
import l1j.server.server.model.Instance.L1PcInstance;

public class PcPartyRefreshThread implements Runnable {
	private static PcPartyRefreshThread _instance;
	private static FastTable<L1PcInstance> _pcPartyRefreshs = new FastTable<L1PcInstance>();

	public static PcPartyRefreshThread getInstance() {
		if (_instance == null) {
			_instance = new PcPartyRefreshThread();
		}
		return _instance;
	}

	public void run() {
		while (true) {
			try {
				for (Iterator<L1PcInstance> iter = _pcPartyRefreshs.iterator(); iter.hasNext();) {
					L1PcInstance pc = (L1PcInstance) iter.next();
					if (pc == null || pc.getNetConnection() == null || pc.getParty() == null || pc.noPlayerCK) {
						iter.remove();
						continue;
					}
					pc.getParty().refresh(pc);
					continue;
				}
				
				Thread.sleep(5000L);
				continue;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public void addPlayer(L1PcInstance pc) {
		if (!_pcPartyRefreshs.contains(pc))
			_pcPartyRefreshs.add(pc);
	}

	public void removePlayer(L1PcInstance pc) {
		if (_pcPartyRefreshs.contains(pc))
			_pcPartyRefreshs.remove(pc);
	}
}
package server.controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import l1j.server.Config;
import l1j.server.L1DatabaseFactory;
import l1j.server.server.GeneralThreadPool;
import l1j.server.server.datatables.CharacterAttendTable.UseAttendTemp;
import l1j.server.server.datatables.CharacterAttendTable.idTemp;
import l1j.server.server.model.L1World;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.serverpackets.S_AttenDance;
import l1j.server.server.utils.CommonUtil;
import l1j.server.server.utils.SQLUtil;

public class AttendController implements Runnable {
	private static Logger _log = Logger.getLogger(AttendController.class.getName());

	private static AttendController _instance;

	public static AttendController getInstance() {
		if (_instance == null)
			_instance = new AttendController();
		return _instance;
	}

	public AttendController() {
		GeneralThreadPool.getInstance().execute(this);
	}

	private boolean isNow = false;
	
	@Override
	public void run() {
		while (true) {
			try {
				Thread.sleep(1000);
				AttendNormal();
				AttendPcRoom();
				ClearTimeCheck();
				continue;
			} catch (Exception e) {
				e.printStackTrace();
				_log.log(Level.SEVERE, e.getLocalizedMessage(), e);
			}
		}
	}

	private int _time = 0;

	private void ClearTimeCheck() {
		_time = Integer.valueOf(CommonUtil.dateFormat("HH"));
		if (!isNow) {
			if (_time == Config.ClearClock) {
				isNow = true;
				// 메모리상의 정보도 초기화
				// 데이터베이스의 시간초기화
				updateAll(); // 위험 메모리 누수위험하다.
				// 월드상의 접속유저
				updateOnline();
			}
		} else {
			int cktime = Config.ClearClock - 1;
			if (cktime < 0) {
				cktime = 23;
			}

			if (_time == cktime) {
				isNow = false;
			}
		}
	}

	public void updateOnline() {
		UseAttendTemp temp = null;
		for (L1PcInstance pc : L1World.getInstance().getAllPlayers()) {
			if (pc == null || pc.noPlayerCK || pc.getNetConnection() == null) {
				continue;
			}
			temp = pc.attendTemp;
			if (temp == null || temp.id_Normal >= 43 || temp.id_PcRoom >= 43) {
				continue;
			}

			temp.isNormal = 0;
			temp.time_Normal = 0;
			temp.Count_Normal = 0;

			temp.isPcRoom = 0;
			temp.time_PcRoom = 0;
			temp.Count_PcRoom = 0;

			pc.sendPackets(new S_AttenDance(pc, S_AttenDance.WatchPcPorfile, 0));
		}
	}

	public void updateAll() {
		Connection con = null;
		PreparedStatement pstm = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con.prepareStatement("UPDATE character_attend SET isSuccessNormal=?, SuccessNormalCount=?, isSuccessPcRoom=?, SuccessPcRoomCount=?, NormalTime=?, PcRoomTime=?");
			pstm.setInt(1, 0);
			pstm.setInt(2, 0);
			pstm.setInt(3, 0);
			pstm.setInt(4, 0);
			pstm.setInt(5, 0);
			pstm.setInt(6, 0);

			pstm.execute();
		} catch (SQLException e) {
			_log.log(Level.SEVERE, "CharacterAttendTable[]Error4", e);
		} finally {
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
	}

	private void AttendNormal() {
		if (L1World.getInstance().getAllPlayers().size() <= 0) {
			return;
		}

		UseAttendTemp temp = null;
		for (L1PcInstance pc : L1World.getInstance().getAllPlayers()) {
			if (pc == null || pc.noPlayerCK || pc.getNetConnection() == null) { // 출석체크 수정요청 일반 제한 부분, 혹시 제한걸거 있으면 여기 거세요
				continue;
			}

			temp = pc.attendTemp;
			if (temp == null || temp.isNormal >= 1 || temp.Count_Normal >= Config.NormalMaxCount || temp.id_Normal >= 43) {
				continue;
			}

			temp.time_Normal += 1;
			if (temp.time_Normal < Config.NormalMaxTime) {
				continue;
			}

			temp.time_Normal = 0; // 시간초기화
			temp.Count_Normal += 1; // 오늘 완료겟수양 추가
			if (temp.Count_Normal >= Config.NormalMaxCount) {
				temp.isNormal = 1;
			}

			for (idTemp att : temp.Nomarlist) {
				if (att.id == temp.id_Normal) {
			        att.state += 1;
			        temp.id_Normal += 1;
			        pc.sendPackets(new S_AttenDance(S_AttenDance.WatchTimeOver, att.id, false));
			        break;
				}
			}

		}
	}

	private void AttendPcRoom() {
		if (L1World.getInstance().getAllPlayers().size() <= 0) {
			return;
		}

		UseAttendTemp temp = null;
		for (L1PcInstance pc : L1World.getInstance().getAllPlayers()) {
			// 출석체크 수정요청
			// pc방 제한 부분, 혹시 제한걸거 있으면 여기 거세요
			// 혈맹가입부분 제거하셔도 될것같네요. 그리고 pc방 버프 체크추가하시면 될것같습니다.
			if (pc == null || pc.noPlayerCK || pc.getNetConnection() == null || !pc.PC방_버프) {
				continue;
			}

			temp = pc.attendTemp;
			if (temp == null || temp.isPcRoom >= 1 || temp.Count_PcRoom >= Config.PcRoomMaxCount || temp.id_PcRoom >= 43) {
				continue;
			}

			temp.time_PcRoom += 1;
			if (temp.time_PcRoom < Config.PcRoomMaxTime) {
				continue;
			}

			temp.time_PcRoom = 0; // 시간초기화
			temp.Count_PcRoom += 1; // 오늘 완료겟수양 추가
			if (temp.Count_PcRoom >= Config.PcRoomMaxCount) {
				temp.isPcRoom = 1;
			}

			for (idTemp att : temp.PcRoomlist) {
				if (att.id == temp.id_PcRoom) {
			        att.state += 1;
			        temp.id_PcRoom += 1;
			        pc.sendPackets(new S_AttenDance(S_AttenDance.WatchTimeOver, att.id, true));
				    break;
				}
			}

		}
	}

}
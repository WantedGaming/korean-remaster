package server.threads.pc;

import l1j.server.Config;
import l1j.server.GameSystem.Robot.L1RobotInstance;
import l1j.server.server.model.L1World;
import l1j.server.server.model.Instance.L1PcInstance;

public class AutoSaveThread extends Thread {

	private static AutoSaveThread _instance;
	// private static Logger _log =
	// Logger.getLogger(AutoSaveThread.class.getName());
	private final int _saveCharTime;
	private final int _saveInvenTime;

	public static AutoSaveThread getInstance() {
		if (_instance == null) {
			_instance = new AutoSaveThread();
			_instance.start();
		}
		return _instance;
	}

	public AutoSaveThread() {
		super("server.threads.pc.AutoSaveThread");
		_saveCharTime = Config.AUTOSAVE_INTERVAL;
		_saveInvenTime = Config.AUTOSAVE_INTERVAL_INVENTORY;
	}

	public void run() {
		System.out.println("────────────────────────────────────────────────────────");
		//System.out.println("[서버 메세지] 서버 통합 쓰레드 로딩을 시작합니다.");
		//System.out.println("──────────────────────────────────");
		//System.out.println("■ 자동저장관리 쓰레드 ........................ ■ 로딩 정상 완료 ");
		while (true) {
			try {
				for (L1PcInstance pc : L1World.getInstance().getAllPlayers()) {
					if (pc instanceof L1RobotInstance) {
						continue;
					}
					if (pc == null || pc.getNetConnection() == null) {
						continue;
					} else {
						// 캐릭터 정보
						if (_saveCharTime * 1000 < System.currentTimeMillis()
								- pc.getlastSavedTime()) {
							pc.save();
							pc.setlastSavedTime(System.currentTimeMillis());
						}

						// 소지 아이템 정보
						if (_saveInvenTime * 1000 < System.currentTimeMillis()
								- pc.getlastSavedTime_inventory()) {
							pc.saveInventory();
							pc.setlastSavedTime_inventory(System
									.currentTimeMillis());
						}
					}

				}
			} catch (Exception e) {
				e.printStackTrace();
				// /_log.log(Level.SEVERE, e.getLocalizedMessage(), e);
			} finally {
				try {
					Thread.sleep(60000);
				} catch (InterruptedException e) {
					// TODO 자동 생성된 catch 블록
					e.printStackTrace();
				}
			}
		}

	}

}

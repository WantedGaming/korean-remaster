package server.manager;
import java.awt.Color;

import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import l1j.server.Config;

@SuppressWarnings("serial")
public class ServerGmWindow extends JInternalFrame {
	
	private JCheckBox 메티스 = null;
	private JCheckBox 미소피아 = null;
	private JCheckBox 카시오페아 = null;
	private JCheckBox 나델만 = null;
	private JCheckBox 유르멘 = null;
	
	public ServerGmWindow() {
		super();
		
		initialize();
	}
	
	public void initialize() {
		title = "운영자 부재 설정";
		closable = true;      
	    isMaximum = false;	
	    maximizable = false;
	    resizable = false;
        iconable = true;
	    isIcon = false;		
	    setSize(400, 360);
		setBounds(400, 0, 250, 240);
		setVisible(true);
		frameIcon = new ImageIcon("");
		setRootPaneCheckingEnabled(true);
	    updateUI();
	    
		GroupLayout layout = new GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
			
	    메티스 = new JCheckBox("메티스 부재시 체크");
	    메티스.setSelected(!Config.메티스귓켬);
	    메티스.setToolTipText("해당 운영진이 부재중일경우 체크하세요.");
	    
	    미소피아 = new JCheckBox("미소피아 부재시 체크");
	    미소피아.setSelected(!Config.미소피아귓켬);
	    미소피아.setToolTipText("해당 운영진이 부재중일경우 체크하세요.");
	    
	    카시오페아 = new JCheckBox("카시오페아 부재시 체크");
	    카시오페아.setSelected(!Config.카시오페아귓켬);
	    카시오페아.setToolTipText("해당 운영진이 부재중일경우 체크하세요.");
	    
	    나델만 = new JCheckBox("나델만 부재시 체크");
	    나델만.setSelected(!Config.나델만귓켬);
	    나델만.setToolTipText("해당 운영진이 부재중일경우 체크하세요.");
	    
	    유르멘 = new JCheckBox("유르멘 부재시 체크");
	    유르멘.setSelected(!Config.유르멘귓켬);
	    유르멘.setToolTipText("해당 운영진이 부재중일경우 체크하세요.");
	    
		JButton btn_ok = new JButton("설정저장");
		btn_ok.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				try {	    
					if (메티스.isSelected()) {
						Config.메티스귓켬 = Boolean.parseBoolean("false");
					} else {
						Config.메티스귓켬 = Boolean.parseBoolean("true");
					}
					if (미소피아.isSelected()) {
						Config.미소피아귓켬 = Boolean.parseBoolean("false");
					} else {
						Config.미소피아귓켬 = Boolean.parseBoolean("true");
					}
					if (카시오페아.isSelected()) {
						Config.카시오페아귓켬 = Boolean.parseBoolean("false");
					} else {
						Config.카시오페아귓켬 = Boolean.parseBoolean("true");
					}
					if (나델만.isSelected()) {
						Config.나델만귓켬 = Boolean.parseBoolean("false");
					} else {
						Config.나델만귓켬 = Boolean.parseBoolean("true");
					}
					if (유르멘.isSelected()) {
						Config.유르멘귓켬 = Boolean.parseBoolean("false");
					} else {
						Config.유르멘귓켬 = Boolean.parseBoolean("true");
					}
					
					JOptionPane.showMessageDialog(null, "정상적으로 변경되었습니다.", " Server Message", JOptionPane.INFORMATION_MESSAGE);
					setClosed(true);			
				} catch (Exception e) {
					// TODO: handle exception
				}
			}
		});
		
		JButton btn_cancel = new JButton("닫기");
		btn_cancel.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				try {
					setClosed(true);					
				} catch (Exception e) {
					// TODO: handle exception
				}
			}
		});
		
		//Main
		GroupLayout.SequentialGroup main_horizontal_grp = layout.createSequentialGroup();
		
		GroupLayout.SequentialGroup horizontal_grp = layout.createSequentialGroup();
		GroupLayout.SequentialGroup vertical_grp   = layout.createSequentialGroup();
		
		//Main
		GroupLayout.ParallelGroup main = layout.createParallelGroup(GroupLayout.Alignment.LEADING);
		
		// 레이블이 들어갈 열
		GroupLayout.ParallelGroup col1 = layout.createParallelGroup(GroupLayout.Alignment.LEADING);
		
		main.addGroup(horizontal_grp);
		main_horizontal_grp.addGroup(main);
			
		layout.setHorizontalGroup(main_horizontal_grp);
		layout.setVerticalGroup(vertical_grp);
		
		
		col1.addComponent(메티스)		
		.addComponent(미소피아)
		.addComponent(카시오페아)
		.addComponent(유르멘)
		.addComponent(나델만);
		
		
		horizontal_grp.addGap(50).addContainerGap().addGroup(col1).addContainerGap();
				
		vertical_grp.addGap(5).addContainerGap().addGroup(layout.createBaselineGroup(false, false).addComponent(메티스));
		vertical_grp.addGap(5).addContainerGap().addGroup(layout.createBaselineGroup(false, false).addComponent(미소피아));				
		vertical_grp.addGap(5).addContainerGap().addGroup(layout.createBaselineGroup(false, false).addComponent(카시오페아));
		vertical_grp.addGap(5).addContainerGap().addGroup(layout.createBaselineGroup(false, false).addComponent(나델만));
		vertical_grp.addGap(5).addContainerGap().addGroup(layout.createBaselineGroup(false, false).addComponent(유르멘));

		main.addGroup(layout.createSequentialGroup().addGap(50, 50, 50).addComponent(btn_ok).addGap(10).addComponent(btn_cancel));	
		vertical_grp.addGap(15).addContainerGap().addGroup(layout.createBaselineGroup(false, false).addGap(19, 19, 19).addComponent(btn_ok).addComponent(btn_cancel));

	}
}

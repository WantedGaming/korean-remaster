package kr.PeterMonk.DogFightSystem;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;

import l1j.server.L1DatabaseFactory;

public class DogFightTable {
	private static DogFightTable _instance;
	private HashMap<Integer, L1DogFight> _namelist;

	public static DogFightTable getInstance() {
		if (_instance == null) {
			_instance = new DogFightTable();
		}
		return _instance;
	}

	public DogFightTable() {
		_namelist = new HashMap<Integer, L1DogFight>();
		bnl();
	}

	private void bnl() {
		java.sql.Connection con = null;
		PreparedStatement statement = null;
		ResultSet namelist = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("SELECT * FROM util_fighter");
			namelist = statement.executeQuery();

			BadnameTable(namelist);
			namelist.close();
			statement.close();
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (namelist != null)
					namelist.close();
			} catch (Exception e) {
			}
			;
			try {
				if (statement != null)
					statement.close();
			} catch (Exception e) {
			}
			;
			try {
				if (con != null)
					con.close();
			} catch (Exception e) {
			}
			;
			namelist = null;
			statement = null;
			con = null;
		}
	}

	private void BadnameTable(ResultSet Data) throws Exception {
		L1DogFight name = null;
		;
		while (Data.next()) {
			name = new L1DogFight();
			name.setNum(Data.getInt(1));
			name.setWinCount(Data.getInt(2));
			name.setLoseCount(Data.getInt(3));
			_namelist.put(name.getNum(), name);
		}
	}

	public L1DogFight getTemplate(int name) {
		return _namelist.get(new Integer(name));
	}

}

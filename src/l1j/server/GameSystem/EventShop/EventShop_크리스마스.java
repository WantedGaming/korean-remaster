package l1j.server.GameSystem.EventShop;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.util.ArrayList;

import l1j.server.L1DatabaseFactory;
import l1j.server.GameSystem.Robot.L1RobotInstance;
import l1j.server.Warehouse.ClanWarehouse;
import l1j.server.Warehouse.PrivateWarehouse;
import l1j.server.Warehouse.WarehouseManager;
import l1j.server.server.model.L1Inventory;
import l1j.server.server.model.L1Object;
import l1j.server.server.model.L1World;
import l1j.server.server.model.Instance.L1ItemInstance;
import l1j.server.server.model.Instance.L1NpcInstance;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.utils.L1SpawnUtil;
import l1j.server.server.utils.SQLUtil;

public class EventShop_크리스마스 {
	private static EventShop_크리스마스 _instance;

	public static EventShop_크리스마스 getInstance() {
		if (_instance == null) {
			_instance = new EventShop_크리스마스();
		}
		return _instance;
	}

	public static int NPCID = 100691;

	public static L1NpcInstance 주티스;
	public static L1NpcInstance 배경1;
	public static L1NpcInstance 배경2;
	public static L1NpcInstance 배경3;
	public static L1NpcInstance 배경4;
	public static L1NpcInstance 배경5;
	public static L1NpcInstance 배경6;
	private static ArrayList<L1NpcInstance> 상점 = new ArrayList<L1NpcInstance>();

	public synchronized static ArrayList<L1NpcInstance> getEventShopNpc() {
		return 상점;
	}

	private static boolean 진행중 = false;

	private static Timestamp _남은시간 = null;

	public synchronized static void 진행(boolean f) {
		진행중 = f;
	}

	public synchronized static boolean 진행() {
		return 진행중;
	}

	public synchronized static void 남은시간(Timestamp f) {
		_남은시간 = f;
	}

	public synchronized static Timestamp 남은시간() {
		return _남은시간;
	}

	@SuppressWarnings("deprecation")
	public synchronized static void add남은시간(long f) {
		int month = _남은시간.getMonth() + 1;
		long time = 남은시간().getTime() + f;
		Timestamp deleteTime = null;
		deleteTime = new Timestamp(time);
		_남은시간 = deleteTime;
		L1World.getInstance().broadcastServerMessage(
				"운영자의 권한으로 크리스마스 이벤트가 연장되었습니다.");
		L1World.getInstance().broadcastServerMessage(
				"종료시간은 [" + month + "월 " + _남은시간.getDate() + "일 "
						+ _남은시간.getHours() + "시 " + _남은시간.getMinutes() + "분 "
						+ _남은시간.getSeconds() + "초]입니다.");
		EventShopTable.Update(NPCID, deleteTime);
	}

	public synchronized static void addEventShopNpc(L1NpcInstance npc,
			Timestamp time) {
		if (npc.getNpcId() == NPCID) {
			if (상점.contains(npc)) {
				return;
			}
			상점.add(npc);
			진행(true);
			남은시간(time);
			System.out.print("■ 진행중이벤트 브리핑 ........................ ");
			System.out.println("■ 크리스마스 진행");
			System.out.println("──────────────────────────────────");
			EventShopList.Add(npc);
			주티스 = L1SpawnUtil.spawn2(33450, 32798, (short) 4, 100692, 0, 0, 0);
			배경1 = L1SpawnUtil.spawn2(33450, 32795, (short) 4, 100693, 0, 0, 0);
			배경2 = L1SpawnUtil.spawn2(33450, 32795, (short) 4, 100694, 0, 0, 0);
			배경3 = L1SpawnUtil.spawn2(33450, 32795, (short) 4, 100695, 0, 0, 0);
			배경4 = L1SpawnUtil.spawn2(33450, 32795, (short) 4, 100696, 0, 0, 0);
			배경5 = L1SpawnUtil.spawn2(33450, 32795, (short) 4, 100697, 0, 0, 0);
			배경6 = L1SpawnUtil.spawn2(33453, 32793, (short) 4, 100698, 0, 0, 0);
		}
	}

	public synchronized static void oddEventShopNpc(L1NpcInstance npc) {
		if (npc.getNpcId() == NPCID) {
			if (!상점.contains(npc)) {
				return;
			}
			상점.remove(npc);
			진행(false);
			EventItemDelete();
			남은시간(null);
			System.out.println("[이벤트상점] 종료 - 크리스마스");
			EventShopList.Odd(npc);
			주티스.deleteMe();
			배경1.deleteMe();
			배경2.deleteMe();
			배경3.deleteMe();
			배경4.deleteMe();
			배경5.deleteMe();
			배경6.deleteMe();
		}
	}

	private static int delItemlist[] = { 60427, 60428, 60429, 60430, 60431,
			60432, 60433, 60434, 60435, 60436, 60437, 60438, 60439, 60440,
			60441, 60442, 60443, 60444, 60445, 60446, 60447, 60448, 60449,
			60450, 60451, 60452, 60453, 60454, 60455, 60456, 60457, 60458,
			60459, 60460, 60471 };

	public synchronized static void EventItemDelete() {
		try {
			for (L1PcInstance tempPc : L1World.getInstance().getAllPlayers()) {
				if (tempPc == null)
					continue;
				for (int i = 0; i < delItemlist.length; i++) {
					L1ItemInstance[] item = tempPc.getInventory().findItemsId(
							delItemlist[i]);
					if (item != null && item.length > 0) {
						for (int o = 0; o < item.length; o++) {
							tempPc.getInventory().removeItem(item[o]);
						}
					}
					try {
						PrivateWarehouse pw = WarehouseManager.getInstance()
								.getPrivateWarehouse(tempPc.getAccountName());
						L1ItemInstance[] item2 = pw.findItemsId(delItemlist[i]);
						if (item2 != null && item2.length > 0) {
							for (int o = 0; o < item2.length; o++) {
								pw.removeItem(item2[o]);
							}
						}
					} catch (Exception e) {
					}
					try {
						if (tempPc.getClanid() > 0) {
							ClanWarehouse cw = WarehouseManager.getInstance()
									.getClanWarehouse(tempPc.getClanname());
							L1ItemInstance[] item3 = cw
									.findItemsId(delItemlist[i]);
							if (item3 != null && item3.length > 0) {
								for (int o = 0; o < item3.length; o++) {
									cw.removeItem(item3[o]);
								}
							}
						}
					} catch (Exception e) {
					}
					try {
						if (tempPc.getPetListSize() > 0) {
							for (L1NpcInstance npc : tempPc.getPetList()) {
								L1ItemInstance[] pitem = npc.getInventory()
										.findItemsId(delItemlist[i]);
								if (pitem != null && pitem.length > 0) {
									for (int o = 0; o < pitem.length; o++) {
										npc.getInventory().removeItem(pitem[o]);
									}
								}
							}
						}
					} catch (Exception e) {
					}
				}
			}
			try {
				for (L1Object obj : L1World.getInstance().getAllItem()) {
					if (!(obj instanceof L1ItemInstance))
						continue;
					L1ItemInstance temp_item = (L1ItemInstance) obj;
					if (temp_item.getItemOwner() == null
							|| !(temp_item.getItemOwner() instanceof L1RobotInstance)) {
						if (temp_item.getX() == 0 && temp_item.getY() == 0)
							continue;
					}
					for (int ii = 0; ii < delItemlist.length; ii++) {
						if (delItemlist[ii] == temp_item.getItemId()) {
							L1Inventory groundInventory = L1World.getInstance()
									.getInventory(temp_item.getX(),
											temp_item.getY(),
											temp_item.getMapId());
							groundInventory.removeItem(temp_item);
							break;
						}
					}

				}
			} catch (Exception e) {
			}
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < delItemlist.length; i++) {
				sb.append(+delItemlist[i]);
				if (i < delItemlist.length - 1) {
					sb.append(",");
				}
			}
			Delete(sb.toString());

			/*
			 * for(int i = 0; i < delItemlist.length; i++){
			 * Delete(delItemlist[i]); wareDelete(delItemlist[i]);
			 * ClanwareDelete(delItemlist[i]); }
			 */
		} catch (Exception e) {
		}
	}

	private static void Delete(String id_name) {
		Connection con = null;
		PreparedStatement pstm = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con
					.prepareStatement("delete FROM character_items WHERE item_id IN ("
							+ id_name + ")");
			pstm.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			SQLUtil.close(pstm);
		}
		try {
			pstm = con
					.prepareStatement("delete FROM character_warehouse WHERE item_id in ("
							+ id_name + ")");
			pstm.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			SQLUtil.close(pstm);
		}
		try {
			pstm = con
					.prepareStatement("delete FROM clan_warehouse WHERE item_id in ("
							+ id_name + ")");
			pstm.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
	}

	private static void Delete(int itemid) {
		Connection con = null;
		PreparedStatement pstm = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con
					.prepareStatement("delete FROM character_items WHERE item_id=?");
			pstm.setInt(1, itemid);
			pstm.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
	}

	private static void wareDelete(int itemid) {
		Connection con = null;
		PreparedStatement pstm = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con
					.prepareStatement("delete FROM character_warehouse WHERE item_id=?");
			pstm.setInt(1, itemid);
			pstm.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
	}

	private static void ClanwareDelete(int itemid) {
		Connection con = null;
		PreparedStatement pstm = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con
					.prepareStatement("delete FROM clan_warehouse WHERE item_id=?");
			pstm.setInt(1, itemid);
			pstm.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
	}
}
package l1j.server.GameSystem.EventShop;

import java.sql.Timestamp;
import java.util.ArrayList;

import l1j.server.server.model.L1World;
import l1j.server.server.model.Instance.L1NpcInstance;

public class EventShop_룸티스 {
	private static EventShop_룸티스 _instance;

	public static EventShop_룸티스 getInstance() {
		if (_instance == null) {
			_instance = new EventShop_룸티스();
		}
		return _instance;
	}

	public static int NPCID = 4500164;

	private static ArrayList<L1NpcInstance> 상점 = new ArrayList<L1NpcInstance>();

	public synchronized static ArrayList<L1NpcInstance> getEventShopNpc() {
		return 상점;
	}

	private static boolean 진행중 = false;

	private static Timestamp _남은시간 = null;

	public synchronized static void 진행(boolean f) {
		진행중 = f;
	}

	public synchronized static boolean 진행() {
		return 진행중;
	}

	public synchronized static void 남은시간(Timestamp f) {
		_남은시간 = f;
	}

	public synchronized static Timestamp 남은시간() {
		return _남은시간;
	}

	@SuppressWarnings("deprecation")
	public synchronized static void add남은시간(long f) {
		int month = _남은시간.getMonth() + 1;
		long time = 남은시간().getTime() + f;
		Timestamp deleteTime = null;
		deleteTime = new Timestamp(time);
		_남은시간 = deleteTime;
		L1World.getInstance().broadcastServerMessage(
				"운영자의 권한으로 룸티스 이벤트가 연장되었습니다.");
		L1World.getInstance().broadcastServerMessage(
				"종료시간은 [" + month + "월 " + _남은시간.getDate() + "일 "
						+ _남은시간.getHours() + "시 " + _남은시간.getMinutes() + "분 "
						+ _남은시간.getSeconds() + "초]입니다.");
		EventShopTable.Update(NPCID, deleteTime);
	}

	public synchronized static void addEventShopNpc(L1NpcInstance npc,
			Timestamp time) {
		if (npc.getNpcId() == NPCID) {
			if (상점.contains(npc)) {
				return;
			}
			상점.add(npc);
			진행(true);
			남은시간(time);
			System.out.print("■ 진행중이벤트 브리핑 ........................ ");
			System.out.println("■ 룸티스 진행");
			System.out.println("──────────────────────────────────");
			EventShopList.Add(npc);
		}

	}

	public synchronized static void oddEventShopNpc(L1NpcInstance npc) {
		if (npc.getNpcId() == NPCID) {
			if (!상점.contains(npc)) {
				return;
			}
			상점.remove(npc);
			진행(false);
			남은시간(null);
			System.out.println("[이벤트상점] 종료 - 룸티스");
			EventShopList.Odd(npc);
		}
	}

}
package l1j.server.GameSystem.EventShop;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import l1j.server.L1DatabaseFactory;
import l1j.server.server.ObjectIdFactory;
import l1j.server.server.datatables.NpcTable;
import l1j.server.server.model.L1World;
import l1j.server.server.model.Instance.L1NpcInstance;
import l1j.server.server.templates.L1Npc;
import l1j.server.server.utils.SQLUtil;

public class EventShopTable {
	private static EventShopTable _instance;

	public static EventShopTable getInstance() {
		if (_instance == null) {
			_instance = new EventShopTable();
		}
		return _instance;
	}

	private EventShopTable() {
	}

	public static L1NpcInstance Spawn(int npcid, int x, int y, short map,
			int h, Timestamp time) {
		L1NpcInstance npc = null;
		try {
			npc = NpcTable.getInstance().newNpcInstance(npcid);
			npc.setId(ObjectIdFactory.getInstance().nextId());

			npc.setMap(map);
			npc.getLocation().set(x, y, map);

			npc.setHomeX(x);
			npc.setHomeY(y);
			npc.getMoveState().setHeading(h);

			L1World.getInstance().storeObject(npc);
			L1World.getInstance().addVisibleObject(npc);

			EventShopList.AddEvents(npc, time);

			npc.getLight().turnOnOffLight();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return npc;
	}

	public static L1NpcInstance 드다엔피씨스폰(int npcid, int x, int y, short map,
			int h) {
		L1NpcInstance npc = null;
		try {
			npc = NpcTable.getInstance().newNpcInstance(npcid);
			npc.setId(ObjectIdFactory.getInstance().nextId());

			npc.setMap(map);
			npc.getLocation().set(x, y, map);

			npc.setHomeX(x);
			npc.setHomeY(y);
			npc.getMoveState().setHeading(h);

			L1World.getInstance().storeObject(npc);
			L1World.getInstance().addVisibleObject(npc);

			npc.getLight().turnOnOffLight();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return npc;
	}

	public static void InitSpawn() {
		Connection con = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con.prepareStatement("SELECT * FROM Event_Shop");
			rs = pstm.executeQuery();
			while (rs.next()) {
				int npcid = rs.getInt("npc_id");
				Timestamp time = rs.getTimestamp("OutTime");
				if (time.getTime() <= System.currentTimeMillis()) {
					Delete(npcid);
					if (npcid == 4500163) {
						System.out.println("[이벤트상점] 종료 - 신기한반지");
					} else if (npcid == 4500164) {
						System.out.println("[이벤트상점] 종료 - 룸티스");
					} else if (npcid == 100672) {
						System.out.println("[이벤트상점] 종료 - 할로윈");
						EventShop_할로윈.EventItemDelete();
					} else if (npcid == 4500165) {
						System.out.println("[이벤트상점] 종료 - 인나드릴");
					} else if (npcid == 7000043) {
						System.out.println("[이벤트상점] 종료 - 신묘한");
						EventShop_신묘한삭제.기본시간후삭제시간(time.getTime());
					} else if (npcid == 100031) {
						System.out.println("[이벤트상점] 종료 - 옛 말하는 섬의 셀로브");
					} else if (npcid == 100093) {
						System.out.println("[이벤트상점] 종료 - 판도라의 유물 상자");
					} else if (npcid == 100324) {
						System.out.println("[이벤트상점] 종료 - 수렵");
					} else if (npcid == 100396) {
						System.out.println("[이벤트상점] 종료 - 바칸스");
					} else if (npcid == 100411) {
						System.out.println("[이벤트상점] 종료 - 신묘한 삭제");
					} else if (npcid == 100412) {
						System.out.println("[이벤트상점] 종료 - 음유성");
						EventShop_음유성.EventItemDelete();
					} else if (npcid == 100429) {
						System.out.println("[이벤트상점] 종료 - 싸이");
						EventShop_싸이.EventItemDelete();
					} else if (npcid == 100578) {
						System.out.println("[이벤트상점] 종료 - 다이노스");
						EventShop_다이노스.EventItemDelete();
					} else if (npcid == 100577) {
						System.out.println("[이벤트상점] 종료 - 단테스");
						EventShop_단테스.EventItemDelete();
					} else if (npcid == 100645) {
						System.out.println("[이벤트상점] 종료 - 붉은기사단의 보급물자 수색작전");
						EventShop_붉은기사단.EventItemDelete();
					} else if (npcid == 100662) {
						System.out.println("[이벤트상점] 종료 - 붉은 사자의 투지");
						EventShop_붉은사자.EventItemDelete();
					} else if (npcid == 100670) {
						System.out.println("[이벤트상점] 종료 - 스냅퍼의 반지");
					} else if (npcid == 100691) {
						System.out.println("[이벤트상점] 종료 - 크리스마스");
						EventShop_크리스마스.EventItemDelete();
					} else if (npcid == 100708) {
						System.out.println("[이벤트상점] 종료 - 아인하사드의 따스한 시선");
					} else if (npcid == 100709) {
						System.out.println("[이벤트상점] 종료 - 내 10검에 자비란 없다");
						EventShop_10검.EventItemDelete();
					} else if (npcid == 100764) {
						System.out.println("[이벤트상점] 종료 - 드래곤2Day");
					} else if (npcid == 100765) {
						System.out.println("[이벤트상점] 종료 - 단테스2Day");

					} else if (npcid == 100900) {
						System.out.println("[이벤트상점] 종료 - 오림");

					} else if (npcid == 100766) {
						System.out.println("[이벤트상점] 종료 - 감자캐기");
					}
					continue;
				}
				L1Npc l1npc = NpcTable.getInstance().getTemplate(npcid);
				if (l1npc != null) {
					L1NpcInstance field;
					try {
						field = NpcTable.getInstance().newNpcInstance(npcid);
						field.setId(ObjectIdFactory.getInstance().nextId());
						field.setX(rs.getInt("locx"));
						field.setY(rs.getInt("locy"));
						field.setMap((short) rs.getInt("mapid"));
						field.setHomeX(field.getX());
						field.setHomeY(field.getY());
						field.getMoveState().setHeading(rs.getInt("heading"));
						field.setLightSize(l1npc.getLightSize());
						field.getLight().turnOnOffLight();

						EventShopList.AddEvents(field, time);

						L1World.getInstance().storeObject(field);
						L1World.getInstance().addVisibleObject(field);

					} catch (Exception e) {
					}
				}
			}
		} catch (SQLException e) {
		} catch (SecurityException e) {
		} catch (IllegalArgumentException e) {
		} finally {
			SQLUtil.close(rs);
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
	}

	public static void Store(L1Npc npc, int x, int y, int mapid, int h,
			Timestamp time) {
		Connection con = null;
		PreparedStatement pstm = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con
					.prepareStatement("INSERT INTO Event_Shop SET Name=?,npc_id=?,locx=?,locy=?,mapid=?,heading=?,OutTime=?");
			pstm.setString(1, npc.get_name());
			pstm.setInt(2, npc.get_npcId());
			pstm.setInt(3, x);
			pstm.setInt(4, y);
			pstm.setInt(5, mapid);
			pstm.setInt(6, h);
			pstm.setTimestamp(7, time);
			pstm.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
	}

	public static void Update(int npcid, Timestamp time) {
		Connection con = null;
		PreparedStatement pstm = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con
					.prepareStatement("UPDATE Event_Shop SET OutTime =? WHERE npc_id=? ");
			pstm.setTimestamp(1, time);
			pstm.setInt(2, npcid);
			pstm.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
	}

	public static void Delete(int npcid) {
		Connection con = null;
		PreparedStatement pstm = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con
					.prepareStatement("delete FROM Event_Shop WHERE npc_id=? ");
			pstm.setInt(1, npcid);
			pstm.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
	}
}

package l1j.server.GameSystem.EventShop;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;

import l1j.server.server.model.L1World;
import l1j.server.server.model.Instance.L1NpcInstance;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.serverpackets.S_ACTION_UI;

public class EventShop_아인_따스한_시선 {
	private static EventShop_아인_따스한_시선 _instance;

	public static EventShop_아인_따스한_시선 getInstance() {
		if (_instance == null) {
			_instance = new EventShop_아인_따스한_시선();
		}
		return _instance;
	}

	public static int NPCID = 100708;

	private static ArrayList<L1NpcInstance> 상점 = new ArrayList<L1NpcInstance>();

	public synchronized static ArrayList<L1NpcInstance> getEventShopNpc() {
		return 상점;
	}

	private static boolean 진행중 = false;

	private static Timestamp _남은시간 = null;

	public synchronized static void 진행(boolean f) {
		진행중 = f;
	}

	public synchronized static boolean 진행() {
		return 진행중;
	}

	// public static boolean 진행(){return true;}

	public synchronized static void 남은시간(Timestamp f) {
		_남은시간 = f;
	}

	public synchronized static Timestamp 남은시간() {
		return _남은시간;
	}

	@SuppressWarnings("deprecation")
	public synchronized static void add남은시간(long f) {
		int month = _남은시간.getMonth() + 1;
		long time = 남은시간().getTime() + f;
		Timestamp deleteTime = null;
		deleteTime = new Timestamp(time);
		_남은시간 = deleteTime;
		L1World.getInstance().broadcastServerMessage(
				"운영자의 권한으로 아인하사드의 따스한 시선 이벤트가 연장되었습니다.");
		L1World.getInstance().broadcastServerMessage(
				"종료시간은 [" + month + "월 " + _남은시간.getDate() + "일 "
						+ _남은시간.getHours() + "시 " + _남은시간.getMinutes() + "분 "
						+ _남은시간.getSeconds() + "초]입니다.");
		EventShopTable.Update(NPCID, deleteTime);
	}

	public synchronized static void addEventShopNpc(L1NpcInstance npc,
			Timestamp time) {
		if (npc.getNpcId() == NPCID) {
			if (상점.contains(npc)) {
				return;
			}
			상점.add(npc);
			진행(true);
			남은시간(time);
			System.out.print("■ 진행중이벤트 브리핑 ........................ ");
			System.out.println("■ 아인 시선 진행");
			System.out.println("──────────────────────────────────");
			EventShopList.Add(npc);
			for (L1PcInstance pc : L1World.getInstance().getAllPlayers()) {
				if (EventZone(pc)) {
					if (!pc.아인_시선_존) {
						pc.아인_시선_존 = true;
						pc.sendPackets(new S_ACTION_UI(S_ACTION_UI.EINHASAD, pc));
					}
				}
			}
		}
	}

	public synchronized static void oddEventShopNpc(L1NpcInstance npc) {
		if (npc.getNpcId() == NPCID) {
			if (!상점.contains(npc)) {
				return;
			}
			상점.remove(npc);
			진행(false);
			남은시간(null);
			System.out.println("[이벤트상점] 종료 - 아인하사드의 따스한 시선");
			EventShopList.Odd(npc);
			for (L1PcInstance pc : L1World.getInstance().getAllPlayers()) {
				if (pc.아인_시선_존) {
					pc.아인_시선_존 = false;
					pc.sendPackets(new S_ACTION_UI(S_ACTION_UI.EINHASAD, pc));
				}
			}
		}
	}

	public static boolean EventZone(L1PcInstance pc) {
		Calendar cal = (Calendar) Calendar.getInstance();
		int CurrentDay = cal.getTime().getDay();
		int CurrentHour = cal.getTime().getHours();
		boolean ck = false;
		if (CurrentDay == 0 || (CurrentDay == 1 && CurrentHour < 9)) {
			if (pc.getMapId() == 4
					&& pc.getX() >= 33856
					&& pc.getX() <= 34304
					&& (pc.getY() >= 32191 && pc.getY() <= 32575 || pc.getY() >= 32127
							&& pc.getY() <= 32739)) {
				ck = true;
			}
		} else if (CurrentDay == 1 || (CurrentDay == 2 && CurrentHour < 9)) {
			if (pc.getMapId() >= 280 && pc.getMapId() <= 289)
				ck = true;
		} else if (CurrentDay == 2 || (CurrentDay == 3 && CurrentHour < 9)) {
			if (pc.getMapId() >= 53 && pc.getMapId() <= 56)
				ck = true;
		} else if (CurrentDay == 3 || (CurrentDay == 4 && CurrentHour < 9)) {
			if (pc.getMapId() >= 807 && pc.getMapId() <= 813)
				ck = true;
		} else if (CurrentDay == 4 || (CurrentDay == 5 && CurrentHour < 9)) {
			if ((pc.getMapId() >= 451 && pc.getMapId() <= 536
					&& pc.getMapId() != 480 && pc.getMapId() != 481
					&& pc.getMapId() != 482 && pc.getMapId() != 483
					&& pc.getMapId() != 484 && pc.getMapId() != 521
					&& pc.getMapId() != 522 && pc.getMapId() != 523 && pc
					.getMapId() != 524)
					|| (pc.getMapId() == 4 && pc.getX() >= 32511
							&& pc.getX() <= 32960 && pc.getY() >= 32191 && pc
							.getY() <= 32537))
				ck = true;
		} else if (CurrentDay == 5 || (CurrentDay == 6 && CurrentHour < 9)) {
			if ((pc.getMapId() >= 30 && pc.getMapId() <= 33)
					|| (pc.getMapId() >= 35 && pc.getMapId() <= 37)
					|| (pc.getMapId() == 4 && pc.getX() >= 32692
							&& pc.getX() <= 32937 && pc.getY() >= 33091 && pc
							.getY() <= 33489))
				ck = true;
		} else if (CurrentDay == 6 || (CurrentDay == 0 && CurrentHour < 9)) {
			if (pc.getMapId() >= 0 && pc.getMapId() <= 2)
				ck = true;
		}

		return ck;
	}

}
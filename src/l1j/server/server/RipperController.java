package l1j.server.server;

import java.util.Calendar;

import l1j.server.server.model.L1World;
import l1j.server.server.serverpackets.S_PacketBox;
import l1j.server.server.utils.L1SpawnUtil;
import server.manager.eva;

public class RipperController extends Thread {
	
		private static RipperController _instance;

		private boolean _RipperStart;
		public boolean getRipperStart() {
			return _RipperStart;
		}
		public void setRipperStart(boolean Ripper) {
			_RipperStart = Ripper;
		}

		public boolean isGmOpen = false;
		
		public static RipperController getInstance() {
			if(_instance == null) {
				_instance = new RipperController();
			}
			return _instance;
		}
		
		@Override
			public void run() {
					while (true) {
						try	{
						if(isOpen()){
						Spawn0();
						Thread.sleep(120000);
						End();
					}
				} catch(Exception e){
					e.printStackTrace();
				} finally {
					try{
						Thread.sleep(1000L);
					} catch(Exception e){
						e.printStackTrace();
					}
					}
					}
					}
		
		 private void Spawn0(){ 
			 try{
				 L1SpawnUtil.spawn2(32692, 32903, (short) 111, 45673, 50, 3600*1000, 0);//그림리퍼
					L1World.getInstance().broadcastServerMessage("\\aI[보스 알림]: 사신 그림리퍼(오만의 탑 정상)가 나타났습니다.");
					L1World.getInstance().broadcastPacketToAll(
					new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "\\fO[보스 알림]: 사신 그림리퍼(오만의 탑 정상)가 나타났습니다."));
					eva.BossLogAppend("[보스스폰] 지배의 탑 정상 (사신 그림리퍼)");
			 }catch(Exception e2){
					e2.printStackTrace();
			 }
			}
		 
			 
			 private boolean isOpen() {
				  Calendar calender = Calendar.getInstance();
				  int hour, minute;
				  hour = calender.get(Calendar.HOUR_OF_DAY);
				  minute = calender.get(Calendar.MINUTE);	 
			   if ((hour == 2 && minute == 30)) {
				  return true;
				  }
				  return false;
				 }
			 
			 /** 종료 **/
			 public void End() {
				 setRipperStart(false);
			 }
}
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */
package l1j.server.server.model;

import static l1j.server.server.model.skill.L1SkillId.ABSOLUTE_BARRIER;
import static l1j.server.server.model.skill.L1SkillId.ABSOLUTE_BLADE;
import static l1j.server.server.model.skill.L1SkillId.ARMOR_BREAK;
import static l1j.server.server.model.skill.L1SkillId.MAJESTY;
import static l1j.server.server.model.skill.L1SkillId.STRIKER_GALE;
import static l1j.server.server.model.skill.L1SkillId.BERSERKERS;
import static l1j.server.server.model.skill.L1SkillId.BRAVE_AURA;
import static l1j.server.server.model.skill.L1SkillId.BURNING_SLASH;
import static l1j.server.server.model.skill.L1SkillId.BURNING_SPIRIT;
import static l1j.server.server.model.skill.L1SkillId.BURNING_WEAPON;
import static l1j.server.server.model.skill.L1SkillId.퀘이크;
import static l1j.server.server.model.skill.L1SkillId.COOKING_NEW_닭고기;
import static l1j.server.server.model.skill.L1SkillId.COOKING_NEW_연어;
import static l1j.server.server.model.skill.L1SkillId.COOKING_NEW_칠면조;
import static l1j.server.server.model.skill.L1SkillId.COOKING_NEW_한우;
import static l1j.server.server.model.skill.L1SkillId.COUNTER_MAGIC;
import static l1j.server.server.model.skill.L1SkillId.DOUBLE_BRAKE;
import static l1j.server.server.model.skill.L1SkillId.DRAGON_SKIN;
import static l1j.server.server.model.skill.L1SkillId.EARTH_BIND;
import static l1j.server.server.model.skill.L1SkillId.ELEMENTAL_FIRE;
import static l1j.server.server.model.skill.L1SkillId.ENCHANT_VENOM;
import static l1j.server.server.model.skill.L1SkillId.FEAR;
import static l1j.server.server.model.skill.L1SkillId.FEATHER_BUFF_A;
import static l1j.server.server.model.skill.L1SkillId.FEATHER_BUFF_B;
import static l1j.server.server.model.skill.L1SkillId.어스웨폰;
import static l1j.server.server.model.skill.L1SkillId.FREEZING_BREATH;
import static l1j.server.server.model.skill.L1SkillId.ICE_LANCE;
import static l1j.server.server.model.skill.L1SkillId.IMMUNE_TO_HARM;
import static l1j.server.server.model.skill.L1SkillId.IllUSION_AVATAR;
import static l1j.server.server.model.skill.L1SkillId.LIFE_MAAN;
import static l1j.server.server.model.skill.L1SkillId.MIRROR_IMAGE;
import static l1j.server.server.model.skill.L1SkillId.MOB_BASILL;
import static l1j.server.server.model.skill.L1SkillId.MOB_COCA;
import static l1j.server.server.model.skill.L1SkillId.PATIENCE;
import static l1j.server.server.model.skill.L1SkillId.REDUCTION_ARMOR;
import static l1j.server.server.model.skill.L1SkillId.SOUL_OF_FLAME;
import static l1j.server.server.model.skill.L1SkillId.SPECIAL_COOKING;
import static l1j.server.server.model.skill.L1SkillId.SPECIAL_COOKING2;
import static l1j.server.server.model.skill.L1SkillId.STATUS_CURSE_BARLOG;
import static l1j.server.server.model.skill.L1SkillId.STATUS_CURSE_YAHEE;
import static l1j.server.server.model.skill.L1SkillId.STATUS_HOLY_MITHRIL_POWDER;
import static l1j.server.server.model.skill.L1SkillId.STATUS_HOLY_WATER;
import static l1j.server.server.model.skill.L1SkillId.STATUS_HOLY_WATER_OF_EVA;
import static l1j.server.server.model.skill.L1SkillId.STATUS_SPOT1;
import static l1j.server.server.model.skill.L1SkillId.STATUS_SPOT2;
import static l1j.server.server.model.skill.L1SkillId.STATUS_SPOT3;
import static l1j.server.server.model.skill.L1SkillId.UNCANNY_DODGE;
import static l1j.server.server.model.skill.L1SkillId.VALA_MAAN;
import static l1j.server.server.model.skill.L1SkillId.메티스정성스프;
import static l1j.server.server.model.skill.L1SkillId.메티스정성요리;
import static l1j.server.server.model.skill.L1SkillId.메티스축복주문서;
import static l1j.server.server.model.skill.L1SkillId.싸이매콤한라면;
import static l1j.server.server.model.skill.L1SkillId.싸이시원한음료;

import java.util.Random;

import l1j.server.Config;
import l1j.server.GameSystem.Astar.World;
import l1j.server.GameSystem.Robot.L1RobotInstance;
import l1j.server.server.ActionCodes;
import l1j.server.server.Opcodes;
import l1j.server.server.TimeController.WarTimeController;
import l1j.server.server.datatables.CharacterBalance;
import l1j.server.server.datatables.CharacterHitRate;
import l1j.server.server.datatables.CharacterReduc;
import l1j.server.server.datatables.SkillsTable;
import l1j.server.server.datatables.WeaponAddDamage;
import l1j.server.server.datatables.WeaponAddHitRate;
import l1j.server.server.datatables.WeaponPerDamage;
import l1j.server.server.model.Instance.L1DollInstance;
import l1j.server.server.model.Instance.L1DoorInstance;
import l1j.server.server.model.Instance.L1ItemInstance;
import l1j.server.server.model.Instance.L1MonsterInstance;
import l1j.server.server.model.Instance.L1NpcInstance;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.model.Instance.L1PetInstance;
import l1j.server.server.model.Instance.L1SummonInstance;
import l1j.server.server.model.gametime.GameTimeClock;
import l1j.server.server.model.poison.L1DamagePoison;
import l1j.server.server.model.poison.L1ParalysisPoison;
import l1j.server.server.model.poison.L1SilencePoison;
import l1j.server.server.model.skill.L1SkillId;
import l1j.server.server.serverpackets.S_ACTION_UI;
import l1j.server.server.serverpackets.S_AttackMissPacket;
import l1j.server.server.serverpackets.S_AttackPacket;
import l1j.server.server.serverpackets.S_AttackPacketForNpc;
import l1j.server.server.serverpackets.S_ChangeHeading;
import l1j.server.server.serverpackets.S_ChatPacket;
import l1j.server.server.serverpackets.S_DoActionGFX;
import l1j.server.server.serverpackets.S_OwnCharAttrDef;
import l1j.server.server.serverpackets.S_OwnCharStatus;
import l1j.server.server.serverpackets.S_PacketBox;
import l1j.server.server.serverpackets.S_PetWindow;
import l1j.server.server.serverpackets.S_SPMR;
import l1j.server.server.serverpackets.S_ServerMessage;
import l1j.server.server.serverpackets.S_SkillBrave;
import l1j.server.server.serverpackets.S_SkillSound;
import l1j.server.server.serverpackets.S_SystemMessage;
import l1j.server.server.serverpackets.S_UseArrowSkill;
import l1j.server.server.serverpackets.S_UseAttackSkill;
import l1j.server.server.templates.L1EnchantDamage;
import l1j.server.server.templates.L1Skills;
import l1j.server.server.types.Point;
import l1j.server.server.utils.CalcStat;
import l1j.server.server.utils.CommonUtil;

public class L1Attack {

	private L1PcInstance _pc = null;

	private L1Character _target = null;

	private L1PcInstance _targetPc = null;

	private L1NpcInstance _npc = null;

	private L1NpcInstance _targetNpc = null;

	private final int _targetId;

	private int _targetX;

	private int _targetY;

	private int _statusDamage = 0;

	private static final Random _random = new Random(System.nanoTime());

	private int _hitRate = 0;

	private int _calcType;

	private static final int PC_PC = 1;

	private static final int PC_NPC = 2;

	private static final int NPC_PC = 3;

	private static final int NPC_NPC = 4;

	private boolean _isHit = false;

	private int _damage = 0;

	private int _drainMana = 0;

	/** 조우의 돌골렘 **/

	private int _drainHp = 0;

	/** 조우의 돌골렘 **/

	private int _attckGrfxId = 0;

	private int _attckActId = 0;

	// 공격자가 플레이어의 경우의 무기 정보
	private L1ItemInstance Sweapon = null;// 세컨웨폰
	private int _SweaponId = 0;
	private int _SweaponType = 0;
	private int _SweaponType1 = 0;
	private int _SweaponAddHit = 0;
	private int _SweaponAddDmg = 0;
	private int _SweaponSmall = 0;
	private int _SweaponLarge = 0;
	private int _SweaponRange = 1;
	private int _SweaponBless = 1;
	private int _SweaponEnchant = 0;
	private int _SweaponMaterial = 0;
	private int _SweaponAttrEnchantLevel = 0;

	private L1ItemInstance weapon = null;

	private int _weaponId = 0;

	private int _weaponType = 0;

	private int _weaponType1 = 0;

	private int _weaponAddHit = 0;

	private int _weaponAddDmg = 0;

	private int _weaponSmall = 0;

	private int _weaponLarge = 0;

	private int _weaponRange = 1;

	private int _weaponBless = 1;

	private int _weaponEnchant = 0;

	private int _weaponMaterial = 0;

	private int _weaponAttr = 0;

	private int _weaponAttrEnchantLevel = 0;

	private int _weaponDoubleDmgChance = 0;

	private int _attackType = 0;

	private boolean _크리티컬 = false;
	
	private boolean _발라근거리가호 = false;
	private boolean _발라원거리가호 = false;
	
	private boolean _속성공격 = false;
	private int _속성공격임팩트 = 0;
	
	private boolean _속성치명 = false;
	private int _속성치명임팩트  = 0;;
	
	private L1ItemInstance _arrow = null;

	private L1ItemInstance _sting = null;

	private int _leverage = 10; // 1/10배로 표현한다.

	// private int weaponDamage;

	public void setLeverage(int i) {
		_leverage = i;
	}

	private int getLeverage() {
		return _leverage;
	}

	// ////////// 키링크 인트 데미지 보정 //////////

	public void setActId(int actId) {
		_attckActId = actId;
	}

	public void setGfxId(int gfxId) {
		_attckGrfxId = gfxId;
	}

	public int getActId() {
		return _attckActId;
	}

	public int getGfxId() {
		return _attckGrfxId;
	}

	public L1Attack(L1Character attacker, L1Character target) {
		if (attacker instanceof L1PcInstance) {
			_pc = (L1PcInstance) attacker;
			if (target instanceof L1PcInstance) {
				_targetPc = (L1PcInstance) target;
				_calcType = PC_PC;
			} else if (target instanceof L1NpcInstance) {
				_targetNpc = (L1NpcInstance) target;
				_calcType = PC_NPC;
			}
			// 무기 정보의 취득
			weapon = _pc.getWeapon();
			Sweapon = _pc.getSecondWeapon();
			if (Sweapon != null) {
				_SweaponId = Sweapon.getItem().getItemId();
				_SweaponType = Sweapon.getItem().getType1();
				_SweaponAddHit = Sweapon.getItem().getHitModifier() + Sweapon.getHitByMagic();
				_SweaponAddDmg = Sweapon.getItem().getDmgModifier()
						+ Sweapon.getDmgByMagic();
				_SweaponType1 = Sweapon.getItem().getType();
				_SweaponSmall = Sweapon.getItem().getDmgSmall();
				_SweaponLarge = Sweapon.getItem().getDmgLarge();

				_SweaponRange = Sweapon.getItem().getRange();
				_SweaponBless = Sweapon.getItem().getBless();
				_SweaponEnchant = Sweapon.getEnchantLevel() - Sweapon.get_durability(); // 손상분 마이너스
				_SweaponMaterial = Sweapon.getItem().getMaterial();
				_SweaponAttrEnchantLevel = Sweapon.getAttrEnchantLevel();
			}
			if (weapon != null) {
				_weaponId = weapon.getItem().getItemId();
				_weaponType = weapon.getItem().getType1();
				_weaponAddHit = weapon.getItem().getHitModifier() + weapon.getHitByMagic();
				_weaponAddDmg = weapon.getItem().getDmgModifier() + weapon.getDmgByMagic();
				_weaponType1 = weapon.getItem().getType();
				_weaponSmall = weapon.getItem().getDmgSmall();
				_weaponLarge = weapon.getItem().getDmgLarge();
				_weaponRange = weapon.getItem().getRange();
				_weaponBless = weapon.getItem().getBless();
				if (_weaponType != 20 && _weaponType != 62) {
					_weaponEnchant = weapon.getEnchantLevel() - weapon.get_durability(); // 손상분 마이너스
				} else {
					_weaponEnchant = weapon.getEnchantLevel();
				}
				_weaponMaterial = weapon.getItem().getMaterial();
				if (_weaponType == 20) { // 화살의 취득
					_arrow = _pc.getInventory().getArrow();
					if (_arrow != null) {
						_weaponBless = _arrow.getItem().getBless();
						_weaponMaterial = _arrow.getItem().getMaterial();
					}
				} else if (_weaponType == 62) { // 스팅의 취득
					_sting = _pc.getInventory().getSting();
					if (_sting != null) {
						_weaponBless = _sting.getItem().getBless();
						_weaponMaterial = _sting.getItem().getMaterial();
					}
				}
				_weaponDoubleDmgChance = weapon.getItem().getDoubleDmgChance();
				_weaponAttrEnchantLevel = weapon.getAttrEnchantLevel();
			}
			// 스테이터스에 의한 추가 데미지 보정
			if (_weaponType == 20 || _weaponType == 62) { // 활의 경우는 DEX치 참조
				_statusDamage = CalcStat.원거리대미지(_pc.getAbility().getTotalDex());
			} else if (_weaponType1 == 17) { // 키링크의 경우 INT치 참조
				_statusDamage = CalcStat.마법대미지(_pc.getAbility().getTotalInt());
			} else {
				_statusDamage = CalcStat.근거리대미지(_pc.getAbility().getTotalStr());
			}
		
		} else if (attacker instanceof L1NpcInstance) {
			_npc = (L1NpcInstance) attacker;
			if (target instanceof L1PcInstance) {
				_targetPc = (L1PcInstance) target;
				_calcType = NPC_PC;
			} else if (target instanceof L1NpcInstance) {
				_targetNpc = (L1NpcInstance) target;
				_calcType = NPC_NPC;
			}
		}
		if (target != null) {
			_target = target;
			_targetId = target.getId();
			_targetX = target.getX();
			_targetY = target.getY();
		} else {
			_targetId = 0;
		}
	}

	/* ■■■■■■■■■■■■■■■■ 명중 판정 ■■■■■■■■■■■■■■■■ */
	/**
	 * 해당하는 좌표로 방향을 전환할때 사용.
	 */
	public int calcheading(int myx, int myy, int tx, int ty) {
		if (tx > myx && ty > myy) {
			return 3;
		} else if (tx < myx && ty < myy) {
			return 7;
		} else if (tx > myx && ty == myy) {
			return 2;
		} else if (tx < myx && ty == myy) {
			return 6;
		} else if (tx == myx && ty < myy) {
			return 0;
		} else if (tx == myx && ty > myy) {
			return 4;
		} else if (tx < myx && ty > myy) {
			return 5;
		} else {
			return 1;
		}
	}

	public int calcheading(L1Character o, int x, int y) {
		return calcheading(o.getX(), o.getY(), x, y);
	}

	public boolean calcHit() {
		if (_calcType == PC_PC || _calcType == PC_NPC) {
			if (_target.getX() == _pc.getX() && _target.getY() == _pc.getY()
					&& _target.getMapId() == _target.getMapId()) {
				_isHit = true;
				return true;
			}
			if (_target instanceof L1MonsterInstance) {
				L1MonsterInstance mon = (L1MonsterInstance) _target;
				if (mon.isreodie) {
					return false;
				}
			}
			boolean door = World.문이동(_pc.getX(), _pc.getY(), _pc.getMapId(), calcheading(_pc, _target.getX(), _target.getY()));
			boolean tail = World.isThroughAttack(_pc.getX(), _pc.getY(), _pc.getMapId(), calcheading(_pc, _target.getX(), _target.getY()));
			if (_pc.getX() == _target.getX() && _pc.getY() == _target.getY() && _pc.getMapId() == _target.getMapId())
				tail = true;
			if (door || !tail) {
				if (!(_target instanceof L1DoorInstance)) {
					_isHit = false;
					return _isHit;
				}
			}
			if (_pc instanceof L1RobotInstance && _pc.isElf()) {
				if (!_pc.getLocation().isInScreen(_target.getLocation())) {
					_isHit = false;
					return _isHit;
				}
			} else if (_weaponRange != -1) {
				if (_target instanceof L1MonsterInstance) {
					if (((L1MonsterInstance) _target).getNpcId() == 100584
							|| ((L1MonsterInstance) _target).getNpcId() == 100588
							|| ((L1MonsterInstance) _target).getNpcId() == 100589
							|| ((L1MonsterInstance) _target).getNpcId() == 707026){
						_weaponRange++;
					}
				}
				if (_pc.getLocation().getTileLineDistance(_target.getLocation()) > _weaponRange + 2) {
					_isHit = false;
					return _isHit;
				}
				if (_weaponType1 == 17) {
					_isHit = true;
					if (_target instanceof L1PcInstance) {
						if (_target.getSkillEffectTimerSet().hasSkillEffect(COUNTER_MAGIC)) {
							_target.getSkillEffectTimerSet().removeSkillEffect(COUNTER_MAGIC);
							int castgfx = 10702;
							Broadcaster.broadcastPacket(_target, new S_SkillSound(_target.getId(), castgfx), true);
							((L1PcInstance) _target).sendPackets(new S_SkillSound(_target.getId(), castgfx), true);
							_isHit = false;
						}
					}
					return _isHit;
				}
			} else {
				if (!_pc.getLocation().isInScreen(_target.getLocation())) {
					_isHit = false;
					return _isHit;
				}
			}
			if (!(_pc instanceof L1RobotInstance) && _weaponType == 20 && _weaponId != 190 && _weaponId != 100190 && _weaponId != 9100
					&& _weaponId != 11011 && _weaponId != 11012 && _weaponId != 11013 && _arrow == null) {
				return _isHit = false; // 화살이 없는 경우는 미스
			} else if (_weaponType == 62 && _sting == null) {
				return _isHit = false; // 스팅이 없는 경우는 미스
			} else if (!(_target instanceof L1DoorInstance) && (!CharPosUtil.isAreaAttack(_pc, _targetX, _targetY, _target.getMapId()) || !CharPosUtil.isAreaAttack(_target, _pc.getX(), _pc.getY(), _pc.getMapId()))) {
				return _isHit = false; // 공격자가 플레이어의 경우는 장애물 판정
			} else if (_weaponId == 247 || _weaponId == 248 || _weaponId == 249) {
				return _isHit = false; // 시련의 검B~C　공격 무효
			} else if (_calcType == PC_PC) {
				if (CharPosUtil.getZoneType(_pc) == 1 || CharPosUtil.getZoneType(_targetPc) == 1) {
					return _isHit = false;
				}
				return _isHit = calcPcPcHit();
			} else if (_calcType == PC_NPC) {
				return _isHit = calcPcNpcHit();

			}
		} else if (_calcType == NPC_PC) {
			return _isHit = calcNpcPcHit();
		} else if (_calcType == NPC_NPC) {
			return _isHit = calcNpcNpcHit();
		}
		return _isHit;
	}

	// ●●●● 플레이어로부터 플레이어에의 명중 판정 ●●●●
	/*
	 * PC에의 명중율 =(PC의 Lv＋클래스 보정＋STR 보정＋DEX 보정＋무기 보정＋DAI의 매수/2＋마법 보정)×0.68－10
	 * 이것으로 산출된 수치는 자신이 최대 명중(95%)을 주는 일을 할 수 있는 상대측 PC의 AC 거기로부터 상대측 PC의 AC가
	 * 1좋아질 때마다 자명중율로부터 1당겨 간다 최소 명중율5% 최대 명중율95%
	 */
	private boolean calcPcPcHit() {

		_hitRate = _pc.getLevel();

		if(_pc.getWeapon() == null){
			return false;
		}
		
		if (_calcType == 1){
		    if (_targetPc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.MOEBIUS)){
        	    if (_weaponType == 20 || _weaponType == 62){
                return false;
        	    }
            }
		}
		
		if (_weaponType != 20 && _weaponType != 62) {
			_hitRate += CalcStat.근거리명중(_pc.getAbility().getTotalStr());
			_hitRate += (_weaponAddHit*2) + _pc.getHitup() + (_pc.getHitupByArmor() * 2) + (_weaponEnchant);
		} else {
			_hitRate += CalcStat.원거리명중(_pc.getAbility().getTotalDex());
			_hitRate += (_weaponAddHit*2) + _pc.getBowHitup() + (_pc.getBowHitupByArmor()*2) + _pc.getBowHitupByDoll() + (_weaponEnchant);
			if (_weaponType == 20 || _weaponType == 62) {
				if (_arrow != null) {
					int add_Hit = 0;
					if (_arrow.getItemId() == 8602){
						 add_Hit = 3;
					 }
					 _hitRate += add_Hit;
				}
			} else if (_weaponType == 62){
				int add_Hit = 0;
				 if (_arrow.getItemId() == 40740){
					 add_Hit = 3;
				 }
				 _hitRate += add_Hit;
			}
		}

		 _pc.타겟아이디 = _targetPc.getId();
		 
		int attackerDice = _random.nextInt(20) + 1 + _hitRate - 10;

		int defenderDice = 0;

		if (_targetPc.getSkillEffectTimerSet().hasSkillEffect(FEAR)) {
			attackerDice += 5;
		}


		int defenderValue = (int) (_targetPc.getAC().getAc() * 1.2) * -1;

		if (_targetPc.getAC().getAc() >= 0)
			defenderDice = 10 - _targetPc.getAC().getAc();
		else if (_targetPc.getAC().getAc() < 0) {
			defenderDice = defenderValue;
			// defenderDice = 10 + _random.nextInt(defenderValue) + 1;
			int ac = _targetPc.getAC().getAc();
			if (ac <= -170)
				defenderDice += Config.AC_170;
			else if (ac <= -165)
				defenderDice += Config.AC_165;
			else if (ac <= -160)
				defenderDice += Config.AC_160;
			else if (ac <= -155)
				defenderDice += Config.AC_155;
			else if (ac <= -150)
				defenderDice += Config.AC_150;
			else if (ac <= -145)
				defenderDice += Config.AC_145;
			else if (ac <= -140)
				defenderDice += Config.AC_140;
			else if (ac <= -135)
				defenderDice += Config.AC_135;
			else if (ac <= -130)
				defenderDice += Config.AC_130;
			else if (ac <= -125)
				defenderDice += Config.AC_125;
			else if (ac <= -120)
				defenderDice += Config.AC_120;
			else if (ac <= -115)
				defenderDice += Config.AC_115;
			else if (ac <= -110)
				defenderDice += Config.AC_110;
			else if (ac <= -105)
				defenderDice += Config.AC_105;
			else if (ac <= -100)
				defenderDice += Config.AC_100;
			else if (ac <= -95)
				defenderDice += Config.AC_95;
			else if (ac <= -90)
				defenderDice += Config.AC_90;
			else if (ac <= -85)
				defenderDice += Config.AC_85;
			else if (ac <= -80)
				defenderDice += Config.AC_80;
			else if (ac <= -75)
				defenderDice += Config.AC_75;
			else if (ac <= -70)
				defenderDice += Config.AC_70;
			else if (ac <= -65)
				defenderDice += Config.AC_65;
			else if (ac <= -60)
				defenderDice += Config.AC_60;
			else if (ac <= -55)
				defenderDice += Config.AC_55;
			else if (ac <= -50)
				defenderDice += Config.AC_50;
			else if (ac <= -45)
				defenderDice += Config.AC_45;
			else if (ac <= -40)
				defenderDice += Config.AC_40;
			else if (ac <= -35)
				defenderDice += Config.AC_35;
			else if (ac <= -30)
				defenderDice += Config.AC_30;
			else if (ac <= -25)
				defenderDice += Config.AC_25;
			else if (ac <= -20)
				defenderDice += Config.AC_20;
			else if (ac <= -15)
				defenderDice += Config.AC_15;
			else if (ac <= -10)
				defenderDice += Config.AC_10;
		}

		if (_weaponType == 20 || _weaponType == 62) {
			defenderDice += (_targetPc.get_PlusEr() * 4);
		}

		// _pc.sendPackets(new
		// S_SystemMessage("pc_pc 명중 포인트: "+attackerDice+" 방어포인트 : "+defenderDice));

		double 절대미스확률 = 5;

		double 최종공격성공확률 = 0;

		/*
		 * if (attackerDice > defenderDice){
		 * 
		 * 
		 * 
		 * }else
		 */if (attackerDice <= defenderDice) {
			 double temp = ((defenderDice - attackerDice) * 0.1);

			 if (temp < 1) {
				 temp = 1;
			 }

			 절대미스확률 += temp;
			 // _pc.sendPackets(new
			 // S_SystemMessage("[방어포인트 높음] 방어포인트미스확률 : "+temp+"%"));
			 /*
			  * if(_random.nextInt(100) >= temp){ _pc.sendPackets(new
			  * S_SystemMessage("[방어포인트 높음] 공격 성공 확률 : "+(100-temp)+"% (성공함)"));
			  * 최종공격성공확률 = 100-절대미스확률; }else{ _pc.sendPackets(new
			  * S_SystemMessage("[방어포인트 높음] 공격 미스 확률 : "+temp+"% (미스남)"));
			  * _hitRate = 0; }
			  */
		 }
		 if (_targetPc.getSkillEffectTimerSet().hasSkillEffect(MIRROR_IMAGE)) {
			 if (_targetPc.getAC().getAc() >= -69) {
				 절대미스확률 += 3;
			 } else if (_targetPc.getAC().getAc() >= -79) {
				 절대미스확률 += 5;
			 } else if (_targetPc.getAC().getAc() >= -89) {
				 절대미스확률 += 7;
			 } else if (_targetPc.getAC().getAc() >= -99) {
				 절대미스확률 += 10;
			 } else if (_targetPc.getAC().getAc() <= -100) {
				 절대미스확률 += Config.MIRROR_RATE;
			 }
		 }
		 if (_targetPc.getSkillEffectTimerSet().hasSkillEffect(UNCANNY_DODGE)) {
			 if (_targetPc.getAC().getAc() >= -69) {
				 절대미스확률 += 5;
			 } else if (_targetPc.getAC().getAc() >= -79) {
				 절대미스확률 += 7;
			 } else if (_targetPc.getAC().getAc() >= -89) {
				 절대미스확률 += 8;
			 } else if (_targetPc.getAC().getAc() >= -99) {
				 절대미스확률 += 10;
			 } else if (_targetPc.getAC().getAc() <= -100) {
				 절대미스확률 += Config.GOLDEN_DODGE_RATE;
			 }
		 }
		 
		 if (_targetPc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.각성린드비오르)) {
				 절대미스확률 += 7;
		 }

		 if (_targetPc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.ANTA_MAAN) // 지룡의 마안 - 물리 일정확률 회피
				|| _targetPc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.BIRTH_MAAN) // 탄생의 마안 - 물리 일정확률 회피
				|| _targetPc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.SHAPE_MAAN) // 형상의 마안 - 물리 일정확률 회피
				|| _targetPc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.LIFE_MAAN)) { // 생명의 마안 - 물리 일정확률 회피
			 int MaanHitRnd = _random.nextInt(100) + 1;
			 if (MaanHitRnd <= 10) { // 확률
				 절대미스확률 += 10;
			 }
		 }

		 최종공격성공확률 = 100 - 절대미스확률;

		 // _pc.sendPackets(new S_SystemMessage("최종 공격 성공 확률 : "+최종공격성공확률+"%"));

		 if (_random.nextInt(100) + 1 < 절대미스확률) {
			 최종공격성공확률 = 0;
			 // _pc.sendPackets(new
			 // S_SystemMessage("[공격 실패] 닷지+절대미스확률+방어포인트미스확률 : "+절대미스확률+"% (미스남)"));
		 }

		 L1Skills l1skills = SkillsTable.getInstance().getTemplate(L1SkillId.ABSOLUTE_BLADE);
		 int chance = ((_pc.getLevel() - _targetPc.getLevel()) * l1skills.getProbabilityDice()) + l1skills.getProbabilityValue();
		 
		 if (최종공격성공확률 > 0) {
			 if(_targetPc.getSkillEffectTimerSet().hasSkillEffect(ABSOLUTE_BARRIER)){
				 if(_pc.getSkillEffectTimerSet().hasSkillEffect(ABSOLUTE_BLADE)){
					 if(_random.nextInt(100) + 1 <= chance){
						 if(weapon.getItem().getType1() != 20 && weapon.getItem().getType1() != 62){
							 _targetPc.sendPackets(new S_SkillSound(_targetPc.getId(), 14539), true);
							 Broadcaster.broadcastPacket(_targetPc, new S_SkillSound(_targetPc.getId(), 14539), true);
							 _target.getSkillEffectTimerSet().killSkillEffectTimer(L1SkillId.ABSOLUTE_BARRIER);
						 }
					 } else {
						 최종공격성공확률 = 0; 
					 }
				 } else {
					 최종공격성공확률 = 0;
				 }
			 }
			 if (_targetPc.getSkillEffectTimerSet().hasSkillEffect(ICE_LANCE)
				|| _targetPc.getSkillEffectTimerSet().hasSkillEffect(FREEZING_BREATH)
				|| _targetPc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.STATUS_안전모드)) {
				 최종공격성공확률 = 0;
			 }
		 }
		 
		 int rnd = _random.nextInt(100) + 1;

		 /** 원거리버그 방어 */

		 int _jX = _pc.getX() - _targetPc.getX();
		 int _jY = _pc.getY() - _targetPc.getY();
		 if (!(_pc instanceof L1RobotInstance)) {
			 if (_weaponType == 24) { // 창일때
				 if ((_jX > 3 || _jX < -3) && (_jY > 3 || _jY < -3)) {
					 최종공격성공확률 = 0;
				 }
			 } else if (_weaponType == 20 || _weaponType == 62) {// 활일때
				 if ((_jX > 15 || _jX < -15) && (_jY > 15 || _jY < -15)) {
					 최종공격성공확률 = 0;
				 }
			 } else {
				 if ((_jX > 2 || _jX < -2) && (_jY > 2 || _jY < -2)) {
					 최종공격성공확률 = 0;
				 }
			 }
		 }
		 /** 원거리버그 방어 */
			try {
				if(weapon != null){
				if(_pc.isCrown()){
					최종공격성공확률 += CharacterHitRate.getInstance().getCharacterHitRate(0);
				} else if(_pc.isKnight()){
					최종공격성공확률 += CharacterHitRate.getInstance().getCharacterHitRate(1);
				} else if(_pc.isElf()){
					최종공격성공확률 += CharacterHitRate.getInstance().getCharacterHitRate(2);
				} else if(_pc.isWizard()){
					최종공격성공확률 += CharacterHitRate.getInstance().getCharacterHitRate(3);
				} else if(_pc.isDarkelf()){
					최종공격성공확률 += CharacterHitRate.getInstance().getCharacterHitRate(4);
				} else if(_pc.isIllusionist()){
					최종공격성공확률 += CharacterHitRate.getInstance().getCharacterHitRate(5);
				} else if(_pc.isDragonknight()){
					최종공격성공확률 += CharacterHitRate.getInstance().getCharacterHitRate(6);
				} else if(_pc.isWarrior()){
					최종공격성공확률 += CharacterHitRate.getInstance().getCharacterHitRate(7);
				}
				최종공격성공확률 += WeaponAddHitRate.getInstance().getWeaponAddHitRate(_weaponId);
				}else{
					최종공격성공확률 += 0;
				}
			} catch (Exception e){
				 System.out.println("Character Hit Add Error");
			}
			
		 return 최종공격성공확률 >= rnd;
	}

	// ●●●● 플레이어로부터 NPC 에의 명중 판정 ●●●●
	private boolean calcPcNpcHit() {
		try {
			/** SPR체크 **/
			if (_pc.AttackSpeedCheck2 >= 1) {
				if (_pc.AttackSpeedCheck2 == 1) {
					_pc.AttackSpeed2 = System.currentTimeMillis();
					_pc.sendPackets(new S_SystemMessage("\\fY[체크시작]"));
				}
				_pc.AttackSpeedCheck2++;
				if (_pc.AttackSpeedCheck2 >= 12) {
					_pc.AttackSpeedCheck2 = 0;
					double k = (System.currentTimeMillis() - _pc.AttackSpeed2) / 10D;
					String s = String.format("%.0f", k);
					_pc.AttackSpeed2 = 0;
					_pc.sendPackets(new S_SystemMessage(_pc, "-----------------------------------------"), true);
					_pc.sendPackets(new S_SystemMessage(_pc, "해당변신은 " + s + "이 공속으로 적절한값입니다."), true);
					_pc.sendPackets(new S_SystemMessage(_pc, "-----------------------------------------"), true);
				}
			}
			/** SPR체크 **/
			_hitRate = _pc.getLevel();
			if (_weaponType != 20 && _weaponType != 62) {
				_hitRate += CalcStat.근거리명중(_pc.getAbility().getTotalStr()) * 2;
				_hitRate += (_weaponAddHit * 3) + _pc.getHitup() + (_pc.getHitupByArmor() * 2) + (_weaponEnchant / 2);
			} else {
				_hitRate += CalcStat.원거리명중(_pc.getAbility().getTotalDex()) * 2;
				_hitRate += (_weaponAddHit * 3) + _pc.getBowHitup() + (_pc.getBowHitupByArmor() * 2) + _pc.getBowHitupByDoll() + (_weaponEnchant / 2);
				if (_weaponType == 20 || _weaponType == 62) {
					if (_arrow != null) {
						int add_Hit = 0;
						if (_arrow.getItemId() == 8602){
							 add_Hit = 3;
						 }
						 _hitRate += add_Hit;
					}
				} else if (_weaponType == 62){
					if (_sting != null) {
					int add_Hit = 0;
					 if (_sting.getItemId() == 40740){
						 add_Hit = 3;
					 }
					 _hitRate += add_Hit;
					}
				}
			}

			if (_pc.isWarrior()) {
				_hitRate += 10;
			}

			int attackerDice = _random.nextInt(20) + 1 + _hitRate - 10;

			if (_targetNpc.getSkillEffectTimerSet().hasSkillEffect(FEAR))
				attackerDice += 5;

			int defenderDice = 0;

			int defenderValue = (int) (_targetNpc.getAC().getAc() * 3.2) * -1;

			if (_targetNpc.getAC().getAc() >= 0)
				defenderDice = 10 - _targetNpc.getAC().getAc();
			else if (_targetNpc.getAC().getAc() < 0) {
				defenderDice = defenderValue;
				// defenderDice = 10 + _random.nextInt(defenderValue) + 1;
				int ac = _targetNpc.getAC().getAc();
				if (ac <= -170)
					defenderDice += Config.AC_170;
				else if (ac <= -165)
					defenderDice += Config.AC_165;
				else if (ac <= -160)
					defenderDice += Config.AC_160;
				else if (ac <= -155)
					defenderDice += Config.AC_155;
				else if (ac <= -150)
					defenderDice += Config.AC_150;
				else if (ac <= -145)
					defenderDice += Config.AC_145;
				else if (ac <= -140)
					defenderDice += Config.AC_140;
				else if (ac <= -135)
					defenderDice += Config.AC_135;
				else if (ac <= -130)
					defenderDice += Config.AC_130;
				else if (ac <= -125)
					defenderDice += Config.AC_125;
				else if (ac <= -120)
					defenderDice += Config.AC_120;
				else if (ac <= -115)
					defenderDice += Config.AC_115;
				else if (ac <= -110)
					defenderDice += Config.AC_110;
				else if (ac <= -105)
					defenderDice += Config.AC_105;
				else if (ac <= -100)
					defenderDice += Config.AC_100;
				else if (ac <= -95)
					defenderDice += Config.AC_95;
				else if (ac <= -90)
					defenderDice += Config.AC_90;
				else if (ac <= -85)
					defenderDice += Config.AC_85;
				else if (ac <= -80)
					defenderDice += Config.AC_80;
				else if (ac <= -75)
					defenderDice += Config.AC_75;
				else if (ac <= -70)
					defenderDice += Config.AC_70;
				else if (ac <= -65)
					defenderDice += Config.AC_65;
				else if (ac <= -60)
					defenderDice += Config.AC_60;
				else if (ac <= -55)
					defenderDice += Config.AC_55;
				else if (ac <= -50)
					defenderDice += Config.AC_50;
				else if (ac <= -45)
					defenderDice += Config.AC_45;
				else if (ac <= -40)
					defenderDice += Config.AC_40;
				else if (ac <= -35)
					defenderDice += Config.AC_35;
				else if (ac <= -30)
					defenderDice += Config.AC_30;
				else if (ac <= -25)
					defenderDice += Config.AC_25;
				else if (ac <= -20)
					defenderDice += Config.AC_20;
				else if (ac <= -15)
					defenderDice += Config.AC_15;
				else if (ac <= -10)
					defenderDice += Config.AC_10;
			}

			double 절대미스확률 = 5;
			if (_targetNpc.getSkillEffectTimerSet().hasSkillEffect(MIRROR_IMAGE)) {
				if (_targetNpc.getAC().getAc() >= -69) {
					절대미스확률 += 3;
				} else if (_targetNpc.getAC().getAc() >= -79) {
					절대미스확률 += 5;
				} else if (_targetNpc.getAC().getAc() >= -89) {
					절대미스확률 += 7;
				} else if (_targetNpc.getAC().getAc() >= -99) {
					절대미스확률 += 10;
				} else if (_targetNpc.getAC().getAc() <= -100) {
					절대미스확률 += 15;
				}
			}
			if (_targetNpc.getSkillEffectTimerSet().hasSkillEffect(UNCANNY_DODGE)) {
				if (_targetNpc.getAC().getAc() >= -69) {
					절대미스확률 += 5;
				} else if (_targetNpc.getAC().getAc() >= -79) {
					절대미스확률 += 6;
				} else if (_targetNpc.getAC().getAc() >= -89) {
					절대미스확률 += 8;
				} else if (_targetNpc.getAC().getAc() >= -99) {
					절대미스확률 += 11;
				} else if (_targetNpc.getAC().getAc() <= -100) {
					절대미스확률 += 15;
				}
			}
			if (_targetNpc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.각성린드비오르)) {
					절대미스확률 += 7;
			}

			double 최종공격성공확률 = 0;
			
			if (attackerDice <= defenderDice) {
				 double temp = ((defenderDice - attackerDice) * 0.1);

				 if (temp < 1) {
					 temp = 1;
				 }
				 절대미스확률 += temp;
			 }

			 최종공격성공확률 = 100 - 절대미스확률;

			 if (_random.nextInt(100) + 1 < 절대미스확률) {
				 최종공격성공확률 = 0;
			 }

			 if (최종공격성공확률 > 0) {
				 int npcId = _targetNpc.getNpcTemplate().get_npcId();
				 if (npcId >= 45912 && npcId <= 45915  && !_pc.getSkillEffectTimerSet().hasSkillEffect( STATUS_HOLY_WATER)) {
					 최종공격성공확률 = 0;
				 } else if (npcId >= 145685 && npcId <= 145686  && !_pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.할파스권속버프)) {
					 최종공격성공확률 = 0;
				 } else if (npcId == 45916 && !_pc.getSkillEffectTimerSet().hasSkillEffect( STATUS_HOLY_MITHRIL_POWDER)) {
					 최종공격성공확률 = 0;
				 } else if (npcId == 45941 && !_pc.getSkillEffectTimerSet().hasSkillEffect(STATUS_HOLY_WATER_OF_EVA)) {
					 최종공격성공확률 = 0;
				 } else if (npcId >= 46068 && npcId <= 46091 && _pc.getGfxId().getTempCharGfx() == 6035) {
					 최종공격성공확률 = 0;
				 } else if (npcId >= 46092 && npcId <= 46106 && _pc.getGfxId().getTempCharGfx() == 6034) {
					 최종공격성공확률 = 0;
				 }
				 

				 /** 원거리버그 방어 */
				 int _jX = _pc.getX() - _targetNpc.getX();
				 int _jY = _pc.getY() - _targetNpc.getY();
				 if (!(_pc instanceof L1RobotInstance)) {
					 if (_weaponType == 24) { // 창일때
						 if ((_jX > 3 || _jX < -3) && (_jY > 3 || _jY < -3)) {
							 최종공격성공확률 = 0;
						 }
					 } else if (_weaponType == 20 || _weaponType == 62) { // 활일때
						 if ((_jX > 20 || _jX < -20) && (_jY > 20 || _jY < -20)) {
							 최종공격성공확률 = 0;
						 }
					 } else {
						 if ((_jX > 2 || _jX < -2) && (_jY > 2 || _jY < -2)) {
							 최종공격성공확률 = 0;
						 }
					 }
				 }
			 }
			 /** 원거리버그 방어 */
			 int rnd = _random.nextInt(100) + 1;
			 
			 return 최종공격성공확률 >= rnd;
			 
		} catch (Exception e) {
			System.out.println("공격성공 계산중 오류 >> " + _pc.getName());
			e.printStackTrace();
		}
		return false;
	}

	// ●●●● NPC 로부터 플레이어에의 명중 판정 ●●●●
	private boolean calcNpcPcHit() {
		_hitRate += _npc.getLevel();

		if (_calcType == 3){
		if (_targetPc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.MOEBIUS)) {
        	if (_npc.getNpcTemplate().get_ranged() >= 10 && _npc.getLocation().getTileLineDistance(new Point(_targetX, _targetY)) >= 2) {
                return false;
        	}
        }
		}
		
		int attackerValue = 0;
		if (_npc instanceof L1PetInstance) {
			L1PetInstance Pet = (L1PetInstance)_npc;
			_hitRate =  Pet.getLevel();
			_hitRate += Pet.SkillsTable(L1PetInstance.천리안);
			int PetLevel = Pet.getLevel() + 10;
			attackerValue = _random.nextInt(PetLevel / 4);
			int Bonus = Pet.getHunt() + Pet.getElixirHunt();
			if(Bonus >= 20) attackerValue += (Bonus -  10) / 10;
		}

		_hitRate += _npc.getHitup() + attackerValue;

		int attackerDice = _random.nextInt(20) + 1 + _hitRate - 1;

		if (_targetPc.getSkillEffectTimerSet().hasSkillEffect(UNCANNY_DODGE)) {
			if (_targetPc.getAC().getAc() >= -89) {
				attackerDice -= 5;
			} else if (_targetPc.getAC().getAc() >= -99) {
				attackerDice -= 7;
			} else if (_targetPc.getAC().getAc() < -99) {
				attackerDice -= 9;
			}
		}
		 if (_targetPc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.각성린드비오르)) {
			 attackerDice -= 7;
		 }

		if (_targetPc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.ANTA_MAAN) // 지룡의 마안 - 물리 일정확률 회피
				|| _targetPc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.BIRTH_MAAN) // 탄생의 마안 - 물리 일정확률 회피
				|| _targetPc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.SHAPE_MAAN) // 형상의 마안 - 물리 일정확률 회피
				|| _targetPc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.LIFE_MAAN)) { // 생명의 마안 - 물리 일정확률 회피
			int MaanHitRnd = _random.nextInt(100) + 1;
			if (MaanHitRnd <= 3) { // 확률
				_hitRate = 0;
			}
		}

		if (_targetPc.getSkillEffectTimerSet().hasSkillEffect(FEAR)) {
			attackerDice += 5;
		}
		int defenderDice = 0;

		int defenderValue = (_targetPc.getAC().getAc()) * -1;

		if (_targetPc.getAC().getAc() >= 0) {
			defenderDice = 10 - _targetPc.getAC().getAc();
		} else if (_targetPc.getAC().getAc() < 0) {
			defenderDice = 10 + _random.nextInt(defenderValue) + 1;
		}
		int fumble = _hitRate;
		int critical = _hitRate + 19;

		if (attackerDice <= fumble) {
			_hitRate = 0;
		} else if (attackerDice >= critical) {
			_hitRate = 100;
		} else {
			if (attackerDice > defenderDice) {
				_hitRate = 100;
			} else if (attackerDice <= defenderDice) {
				_hitRate = 0;
			}
		}

		if (_targetPc.getSkillEffectTimerSet().hasSkillEffect(ABSOLUTE_BARRIER) || _targetPc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.STATUS_안전모드)) {
			_hitRate = 0;
		} else if (_npc.getNpcId() >= 100584 && _npc.getNpcId() <= 100589) {
			_hitRate = 100;
			return true;
		}

		int rnd = _random.nextInt(100) + 1;
		// NPC의 공격 레인지가 10이상의 경우로, 2이상 떨어져 있는 경우활공격으로 간주한다
		if (_npc.getNpcTemplate().get_ranged() >= 10
				&& _hitRate > rnd
				&& _npc.getLocation().getTileLineDistance(
						new Point(_targetX, _targetY)) >= 3) {
			return calcErEvasion();
		}
		if (_hitRate <= 100) {
			if (_npc.getMapId() >= 550 && _npc.getMapId() <= 558) {
				if (_hitRate == 0)
					_hitRate = _random.nextInt(100) > 90 ? 100 : 0;
			} else if (_npc.getNpcId() == 100081) {
				_hitRate *= 0.75;
			} else if (_npc.getNpcId() >= 100055 && _npc.getNpcId() <= 100058) {
				if (_hitRate == 0)
					_hitRate = _random.nextInt(100) > 85 ? 100 : 0;
			} else if (_npc.getNpcId() >= 100059 && _npc.getNpcId() <= 100060) {
				if (_hitRate == 0)
					_hitRate = _random.nextInt(100) > 85 ? 100 : 0;
			} else if (_npc.getNpcId() == 45295 || _npc.getNpcId() == 45318
					|| _npc.getNpcId() == 45337 || _npc.getNpcId() == 45351) {
				if (_hitRate == 0)
					_hitRate = _random.nextInt(100) > 90 ? 100 : 0;
			} else if (_npc.getMapId() >= 307 && _npc.getMapId() <= 309) { // 침공로
				if (_hitRate == 0)
					_hitRate = _random.nextInt(100) > 90 ? 100 : 0;
			} else if (_npc.getMapId() >= 59 && _npc.getMapId() <= 63) {
				if (_hitRate == 0)
					_hitRate = _random.nextInt(100) > 85 ? 100 : 0;
			} else if (_npc.getMapId() == 440) {
				if (_hitRate == 0)
					_hitRate = _random.nextInt(100) > 85 ? 100 : 0;
			} else if (_npc.getMapId() == 4
					&& ((_npc.getX() >= 33472 && _npc.getX() <= 33856
					&& _npc.getY() >= 32191 && _npc.getY() <= 32511) || (_npc
							.getX() >= 32511
							&& _npc.getX() <= 32960
							&& _npc.getY() >= 32191 && _npc.getY() <= 32537))) {
				if (_hitRate == 0)
					_hitRate = _random.nextInt(100) > 85 ? 100 : 0;
			} else if (_npc.getMapId() >= 30 && _npc.getMapId() <= 36) {
				if (_npc.getNpcTemplate().get_undead() == 1)
					_hitRate *= 0.55;
				else
					_hitRate *= 0.70;
			} else if (_npc.getMapId() >= 1 && _npc.getMapId() <= 2) {
				if (_npc.getNpcTemplate().get_undead() == 1)
					_hitRate *= 0.80;
			} else {
				_hitRate *= 0.80;
			}
		}
		return _hitRate >= rnd;
	}

	// ●●●● NPC 로부터 NPC 에의 명중 판정 ●●●●
	private boolean calcNpcNpcHit() {
		int target_ac = 10 - _targetNpc.getAC().getAc();
		int attacker_lvl = _npc.getNpcTemplate().get_level();

		if (target_ac != 0) {
			_hitRate = (100 / target_ac * attacker_lvl); // 피공격자 AC = 공격자 Lv
			// 의 때 명중율 100%
		} else {
			_hitRate = 100 / 1 * attacker_lvl;
		}

		
		if (_npc instanceof L1PetInstance) {
			L1PetInstance Pet = (L1PetInstance) _npc;
			int PetLevel = Pet.getLevel() / 2 + 10;
			attacker_lvl  = attacker_lvl + (PetLevel + _random.nextInt(PetLevel / 4));
			attacker_lvl += Pet.SkillsTable(L1PetInstance.천리안);
			int Bonus = Pet.getHunt() + Pet.getElixirHunt();
			if(Bonus >= 20) attacker_lvl += (Bonus -  10) / 10;
		}

		if (_hitRate < attacker_lvl) {
			_hitRate = attacker_lvl; // 최저 명중율=Lｖ％
		}
		if (_hitRate > 95) {
			_hitRate = 95; // 최고 명중율은 95%
		}
		if (_hitRate < 5) {
			_hitRate = 5; // 공격자 Lv가 5 미만때는 명중율 5%
		}

		int rnd = _random.nextInt(100) + 1;
		return _hitRate >= rnd;
	}

	// ●●●● ER에 의한 회피 판정 ●●●●
	private boolean calcErEvasion() {
		int er = _targetPc.get_PlusEr();

		int rnd = _random.nextInt(130) + 1;
		return er < rnd;
	}

	/* ■■■■■■■■■■■■■■■ 데미지 산출 ■■■■■■■■■■■■■■■ */
	public int calcDamage(int adddmg, boolean flag) {
		switch (_calcType) {
		case PC_PC:
			_damage = calcPcPcDamage(adddmg);
			break;
		case PC_NPC:
			_damage = calcPcNpcDamage(adddmg);
			break;
		case NPC_PC:
			_damage = calcNpcPcDamage();
			break;
		case NPC_NPC:
			_damage = calcNpcNpcDamage();
			break;
		default:
			break;
		}
		if(flag)
			_damage /= 2;
		return _damage;
	}

	public int calcDamage(boolean flag) {
		switch (_calcType) {
		case PC_PC:
			_damage = calcPcPcDamage(0);
			break;
		case PC_NPC:
			_damage = calcPcNpcDamage(0);
			break;
		case NPC_PC:
			_damage = calcNpcPcDamage();
			break;
		case NPC_NPC:
			_damage = calcNpcNpcDamage();
			break;
		default:
			break;
		}
		if(flag)
			_damage /= 2;
		return _damage;
	}

	boolean burning = false;
	boolean double_burning = false;

	// ●●●● 플레이어로부터 플레이어에의 데미지 산출 ●●●●
	public int calcPcPcDamage(int adddmg) {
		if (_pc instanceof L1RobotInstance) {
			if (_pc.getCurrentWeapon() == 46 || _pc.getCurrentWeapon() == 20 || _pc.getCurrentWeapon() == 24) {// 활
				return _random.nextInt(15) + 15;
			} else {
				return _random.nextInt(25) + 35;
			}
		}
		int weaponMaxDamage = 0;

		weaponMaxDamage = _weaponSmall + (_weaponAddDmg * 2);
		
		boolean secondw = false;
		if (_pc.isWarrior() && _pc.isSlayer && _pc.getSecondWeapon() != null) {
			int ran = _random.nextInt(100);
			if (ran < 50) {
				secondw = true;
				weaponMaxDamage = _SweaponSmall + (_SweaponAddDmg*2);
			}
		}

		int weaponDamage = 0;

		int doubleChance = _random.nextInt(120) + 1;

		if (_target != null) {
			int chance1 = _random.nextInt(100);
			for (L1ItemInstance item : _targetPc.getInventory().getItems()) {
				if (item.isEquipped()) {
					if (item.getItemId() >= 420104 && item.getItemId() <= 420107) {
						int addchan = 5;
						if (addchan < 0)
							addchan = 0;
						if (chance1 < 5) {
							// 123456 일때 80~100
							// 파푸 가호 7일때 120~140 / 8일때 140~160 9일때 160~180
							int addhp = _random.nextInt(20) + 1;
							int basehp = 80;
							if (item.getEnchantLevel() == 7)
								basehp = 120;
							if (item.getEnchantLevel() == 8)
								basehp = 140;
							if (item.getEnchantLevel() == 9)
								basehp = 160;
							_targetPc.setCurrentHp(_targetPc.getCurrentHp()
									+ basehp + addhp);
							_targetPc.sendPackets(
									new S_SkillSound(_targetPc.getId(), 2187),
									true);
							Broadcaster.broadcastPacket(_targetPc,
									new S_SkillSound(_targetPc.getId(), 2187),
									true);
						}
						break;
					} else if (item.getItemId() >= 420108
							&& item.getItemId() <= 420111) {
						int addchan = item.getEnchantLevel() - 5;
						if (addchan < 0)
							addchan = 0;
						if (chance1 < 6 + addchan) {
							if (_targetPc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.린드가호딜레이)) {
								break;
							}
							_targetPc.getSkillEffectTimerSet().setSkillEffect(
									L1SkillId.린드가호딜레이, 4000);

							if (item.getItemId() == 420108)// 완력
								_targetPc.setCurrentMp(_targetPc.getCurrentMp()
										+ 8 + _random.nextInt(7));
							else if (item.getItemId() == 420109)// 예지
								_targetPc.setCurrentMp(_targetPc.getCurrentMp()
										+ 9 + _random.nextInt(7));
							else if (item.getItemId() == 420110)// 인내
								_targetPc.setCurrentMp(_targetPc.getCurrentMp()
										+ 16 + _random.nextInt(9));
							else if (item.getItemId() == 420111)// 마력
								_targetPc.setCurrentMp(_targetPc.getCurrentMp()
										+ 20 + _random.nextInt(11));

							_targetPc.sendPackets(
									new S_SkillSound(_targetPc.getId(), 2188),
									true);
							Broadcaster.broadcastPacket(_targetPc,
									new S_SkillSound(_targetPc.getId(), 2188),
									true);
						}
						break;
					} else if (item.getItemId() == 9204) {
						if (chance1 < 6) {
							_targetPc.setCurrentHp(_targetPc.getCurrentHp() + 50);
							_targetPc.sendPackets(new S_SkillSound(_targetPc.getId(), 13429), true);
							Broadcaster.broadcastPacket(_targetPc, new S_SkillSound(_targetPc.getId(), 13429), true);
						}
						break;
					} else if (item.getItemId() == 21255) {
						if (chance1 < 4) {
							_targetPc
							.setCurrentHp(_targetPc.getCurrentHp() + 31);
							_targetPc.sendPackets(
									new S_SkillSound(_targetPc.getId(), 2183),
									true);
							Broadcaster.broadcastPacket(_targetPc,
									new S_SkillSound(_targetPc.getId(), 2183),
									true);
						}
						break;
					}
				}
			}
		}
		// 파푸가호

		int weaponTotalDamage = 0;
		double dmg = 0;

		if (_weaponId == 90083) { // 포효의이도류
			_weaponDoubleDmgChance += _weaponEnchant + 1;
		}
		
		if (_weaponType == 0) { // 맨손
			weaponDamage = 0;
		} else { // 맨손이 아닐때
			if (_weaponType == 58 && doubleChance <= _weaponDoubleDmgChance) { // 크로우
				// 더블
				weaponDamage = weaponMaxDamage;
				weaponDamage += weaponDamage * 0.2;
				_attackType = 2;
			} else {
				weaponDamage = _random.nextInt(weaponMaxDamage + 2) + 1;
			}

			if (weaponDamage > weaponMaxDamage) {
				_크리티컬 = true;
			}
			// //

			int 치명 = _random.nextInt(100) + 1;
			int 치명확률 = 0;
			if (_weaponType == 20 || _weaponType == 62) {
				치명확률 = CalcStat.원거리치명타(_pc.getAbility().getTotalDex());
				치명확률 += _pc.get원거리치명률();
			} else {
				치명확률 = CalcStat.근거리치명타(_pc.getAbility().getTotalStr());
				치명확률 += _pc.get근거리치명률();
				if (SkillsTable.getInstance().spellCheck(_pc.getId(), L1SkillId.FINAL_BURN)){
					int 추가확률 = (_pc.getLevel() - 80) / 2;
					if (_pc.getCurrentHp() <= _pc.getMaxHp() * 0.3){
						if (_pc.getLevel() >= 80){
							치명확률 += (5 + 추가확률);
						} else {
							치명확률 += 5;
						}
					}
				}
			}

			if (치명 <= 치명확률) {
				_크리티컬 = true;
				weaponDamage = weaponMaxDamage;
			}
			if(_발라원거리가호 || _발라근거리가호){
				weaponDamage = weaponMaxDamage * 2;
				_발라근거리가호 = false;
				_발라원거리가호 = false;
			}
			if (_pc.getSkillEffectTimerSet().hasSkillEffect(SOUL_OF_FLAME)) {
				if (_weaponType != 20 && _weaponType != 62) {
					weaponDamage = weaponMaxDamage;
					_크리티컬 = false;
				}
			}

			if (secondw) {
				weaponTotalDamage = weaponDamage + _SweaponEnchant * 2;
			} else {
				weaponTotalDamage = weaponDamage + _weaponEnchant * 2;
			}

			if (_weaponType == 54 && doubleChance <= _weaponDoubleDmgChance) { // 이도류
				// 더블
				weaponTotalDamage *= 2.4;
				if (_weaponId != 415011) {
					weaponTotalDamage *= 1.1;
				}
				_attackType = 4;
			}
			if (_pc.isCrash) {
				int rnd = _random.nextInt(100);
				if (rnd < Config.WARRIOR_CRASH) {
					int crashdmg = _pc.getLevel() / 2;
					double purrydmg = crashdmg * 2;
					int plusdmg = crashdmg;
					int gfx = 12487;
					if (_pc.isPurry) {
						int rnd2 = _random.nextInt(100);
						if (rnd2 < Config.WARRIOR_FURY) {
							gfx = 12489;
							plusdmg = (int) purrydmg;
						}
					}
					weaponTotalDamage += plusdmg;
					_pc.sendPackets(new S_SkillSound(_target.getId(), gfx));// 12489
					Broadcaster.broadcastPacket(_target, new S_SkillSound(
							_target.getId(), gfx));
				}
			}

			if (secondw) {
				weaponTotalDamage += calcSAttrEnchantDmg();
			} else {
				weaponTotalDamage += calcAttrEnchantDmg();
			}

		if (_pc.getSkillEffectTimerSet().hasSkillEffect(DOUBLE_BRAKE)  && (_weaponType == 54 || _weaponType == 58)) {
				 int rnd = 33;
				 if (_pc.getLevel() >= 90) {
					 rnd += 9;
				 } else if (_pc.getLevel() >= 85) {
					 rnd += 8;
				 } else if (_pc.getLevel() >= 80) {
					 rnd += 7;
				 } else if (_pc.getLevel() >= 75) {
					 rnd += 6;
				 } else if (_pc.getLevel() >= 70) {
					 rnd += 5;
				 } else if (_pc.getLevel() >= 65) {
					 rnd += 4;
				 } else if (_pc.getLevel() >= 60) {
					 rnd += 3;
				 } else if (_pc.getLevel() >= 55) {
					 rnd += 2;
				 } else if (_pc.getLevel() >= 50) {
					 rnd += 1;
				 } else if (_pc.getLevel() >= 45) {
					 rnd += 0;
				 }
				 
				if(_pc.isDBDestiny && _pc.getLevel() >= 80){
					rnd += _pc.getLevel() - 79;
				 }
				 
				 if ((_random.nextInt(100) + 1) <= rnd) {
					 weaponTotalDamage *= 2;
					 double_burning = true;
				 }
			 }
				
			 dmg = weaponTotalDamage + _statusDamage;

			 if (_weaponType != 20 && _weaponType != 62) {
				 dmg += _pc.getDmgup() + _pc.getDmgupByArmor() + _pc.getPvPDmgByArmor() - _pc.getPvPReductionByArmor();
			 } else {
				 dmg += _pc.getBowDmgup() + _pc.getBowDmgupByArmor()  + _pc.getBowDmgupByDoll() + _pc.getPvPDmgByArmor() - _pc.getPvPReductionByArmor();
			 }

			 if (_weaponType == 20) { // 활
				 if (_arrow != null) {
					 int add_dmg = 0;
					 if (_arrow.getItemId() == 8600){
						 add_dmg = 1;
					 } else if (_arrow.getItemId() == 8601){
						 add_dmg = 3;
					 } else if (_arrow.getItemId() == 8602){
						 add_dmg = 3;
						if (_weaponAttr != 0)
						 add_dmg += addattrdmg(_weaponAttr);
					 }
					 dmg += add_dmg;
				 } else if (_weaponId == 190 || _weaponId == 100190 || _weaponId == 9100 || (_weaponId >= 11011 && _weaponId <= 11013)) { // 사이하의 활
					 dmg += 2;
				 }
			 } else if (_weaponType == 62) { // 암 토토 렛
				 int add_dmg = 0;
				 if (_arrow.getItemId() == 40739){
					 add_dmg = 1;
				 } else if (_arrow.getItemId() == 40738){
					 add_dmg = 3;
				 } else if (_arrow.getItemId() == 40740){
					 add_dmg = 3;
				 }
				 dmg = add_dmg;
			 }
			 dmg = calcBuffDamage(dmg);
		}

		if (_weaponType == 0) { // 맨손
			dmg = (_random.nextInt(5) + 4) / 4;
		} else if (_weaponType == 46) {
			dmg += 2;
		} else if (_weaponType == 50) {
			dmg += 5;
		}
		if (_weaponType != 0) { // 맨손이 아닐때
			if (_weaponType1 == 17) {
				int 키링크대미지인트 = _pc.getAbility().getTotalInt();
				int 키링크대미지스펠 = _pc.getAbility().getSp();
				int 키링크최대 = 키링크대미지인트 + 키링크대미지스펠;
				int 키링크랜덤 = (_random.nextInt(키링크대미지인트) + 1) + (_random.nextInt(키링크대미지스펠) + 1);

				dmg += 키링크랜덤 - _targetPc.getPVPMagicDamageReduction();

				dmg = calcMrDefense(_target.getResistance().getEffectedMrBySkill(), dmg);

				if (키링크랜덤 >= 키링크최대) {
					_크리티컬 = true;
				}

			}
			if (_weaponType1 == 18) {
				dmg += WeaponSkill.getChainSwordDamage(_pc, _target, _weaponId);
			}
			switch(weapon.getPvPLevel()){
			case 0: dmg += 0; break;
			case 1: dmg += 1; break;
			case 2: dmg += 2; break;
			case 3: dmg += 3; break;
			case 4: dmg += 4; break;
			case 5: dmg += 5; break;
			default:
			break;
			}
			if (secondw) {
				switch (_SweaponId) {
				case 90086:
				case 90087:
				case 90088:
				case 90090:
				case 90091:
				case 90092:
					WeaponSkill.블레이즈쇼크(_pc, _targetPc, _weaponEnchant);
					break;
				case 7236:
					WeaponSkill.getDeathKnightjin(_pc, _targetPc);
					break;
				case 2:
				case 200002:
					dmg = WeaponSkill.getDiceDaggerDamage(_pc, _targetPc,
							weapon);
					return (int) dmg; // break;
				case 13:
				case 44:
					WeaponSkill.getPoisonSword(_pc, _targetPc);
					break;
				case 100047:
				case 47:
					WeaponSkill.getSilenceSword(_pc, _targetPc, _weaponEnchant);
					break;
				case 90085:
				case 134:
					
					dmg += WeaponSkill.get수결지Damage(_pc, _target,
							_weaponEnchant);
					break;

				case 54:
					dmg += WeaponSkill.getKurtSwordDamage(_pc, _targetPc,
							_weaponEnchant);
					break;
				case 58:
					dmg += WeaponSkill.getDeathKnightSwordDamage(_pc,
							_targetPc, _weaponEnchant);
					break;
					// case 59: dmg += WeaponSkill.getBarlogSwordDamage(_pc,
					// _target, _weaponEnchant); break;
				case 76:
					dmg += WeaponSkill.getRondeDamage(_pc, _targetPc,
							_weaponEnchant);
					break;
				case 121:
					dmg += WeaponSkill.getIceQueenStaffDamage(_pc, _target, _weaponEnchant);
					break;
				case 124:
				case 100124:
					dmg += WeaponSkill.getBaphometStaffDamage(_pc, _target,
							_weaponEnchant);
					break;
				case 9080:
				case 191:// 살천의활
					dmg += WeaponSkill.getEffectDamage(_pc, _target, _weaponEnchant, 9361);
					break;
				case 203:
					dmg += WeaponSkill.getBarlogSwordDamage(_pc, _target);
					break;
			//	case 86: // 붉이
				case 204: // 진홍의 크로스보우
				case 100204:
					WeaponSkill.giveFettersEffect(_pc, _targetPc);
					break;
				case 205:
				case 100205:
					dmg += WeaponSkill.getMoonBowDamage(_pc, _target,
							_weaponEnchant);
					break;
				case 264:
				case 265:
				case 256:
				case 4500027:
					dmg += WeaponSkill
					.getEffectSwordDamage(_pc, _target, 11107);
					break;
				case 9102:
				case 90089:
					dmg += WeaponSkill.getDarkMDamage(_pc, _target, _weaponEnchant);
					break;
				case 9079:
				case 412000: // dmg += WeaponSkill.getEffectSwordDamage(_pc,
					// _target, 10); break;
					dmg += WeaponSkill.getEffectDamage(_pc, _target,
							_weaponEnchant, 3940);
					break;
					/*
					 * case 410000: case 410001: case 450009: case 450014: case
					 * 450004: dmg += WeaponSkill.getChainSwordDamage(_pc, _target);
					 * break;
					 */
				case 412004: // dmg += WeaponSkill.getIceSpearDamage(_pc,
					// _target); break;
					dmg += WeaponSkill.getEffectDamage(_pc, _target, _weaponEnchant, 3704);
					break;
				case 7228:
				case 9075:
				case 412005:
					dmg += WeaponSkill.getEffectDamage(_pc, _target, _weaponEnchant, 5524);
					break;
				case 413101:
				case 413102:
				case 413104:
				case 413105:
					WeaponSkill.getDiseaseWeapon(_pc, _target, 413101);
					break;
				case 413103:
					calcStaffOfMana();
					WeaponSkill.getDiseaseWeapon(_pc, _target, 413101);
					break;
				case 415010:
				case 415011:
				case 415012:
				case 415013:
					WeaponSkill.체이서(_pc, _target, _weaponEnchant, 0);
					break;

				case 415015:
				case 415016:
					WeaponSkill.체이서(_pc, _target, _weaponEnchant, 1);
					break;
				case 6000:
					dmg += WeaponSkill.IceChainSword(_pc, _target,
							_weaponEnchant);
					break;
				case 6001:
					dmg += WeaponSkill.Icekiring(_pc, _target, _weaponEnchant);
					break;
				case 6101:
					dmg += WeaponSkill.소울커터(_pc, _target, _weaponEnchant);
					break;
				case 7:
				case 35:
				case 48:
				case 73:
				case 105:
				case 120:
				case 147:
				case 156:
				case 174:
				case 175:
				case 224:
				case 7232:
					dmg += WeaponSkill.플라즈마(_pc, _target, _weaponEnchant);
					break;
				case 450008:
				case 450022:
				case 450024:
				case 450010:
					WeaponSkill.이블리버스(_pc, _target, _weaponEnchant);
					break;
				case 450011:
				case 450012:
				case 450013:
				case 450023:
				case 450025:
					WeaponSkill.이블트릭(_pc, _target, _weaponEnchant);
					break;
				case 263:
				case 4500028: // dmg += WeaponSkill.halloweenCus(_pc, _target);
					// break;
				case 4500026:
					dmg += WeaponSkill.halloweenCus2(_pc, _target);
					break;// 각궁
				case 100259:
				case 100260:
				case 9081:
				case 259:
				case 260:
					dmg += WeaponSkill.getEffectDamage(_pc, _target,
							_weaponEnchant, 9359);
					break; // 파괴의 크로우, 이도류
				case 9077:
				case 266:
				case 100266:
					dmg += WeaponSkill.PhantomShock(_pc, _target, _weaponEnchant);
					break; // 공명의 키링크
				case 9076:
				case 275:
				case 100275:
					dmg += WeaponSkill.getEffectDamage(_pc, _target, _weaponEnchant, 7398);
					break; // 환영의 체인소드
				case 277:
				case 278:
				case 279:
				case 280:
				case 281:
				case 282:
				case 283:
					WeaponSkill.이블리버스(_pc, _target, _weaponEnchant);
					break; // 붉은 사자 무기
				case 284:
				case 285:
				case 286:
				case 287:
				case 288:
				case 289:
				case 290:
					dmg += WeaponSkill.getEffectDamage(_pc, _target, _weaponEnchant, 3939);
					break; // 환상의 무기 (10검 이벤트)
				case 291: // 제로스의지팡이
					dmg += WeaponSkill.getZerosDamage(_pc, _target, _weaponEnchant);
					break;
				case 9106:
					dmg += WeaponSkill.getJinSSaulDamage(_pc, _target, _weaponEnchant);
					break;
				case 293:
					dmg += WeaponSkill.getAkmongDamage(_pc, _target, _weaponEnchant);
					break;
				case 66:
					dmg += WeaponSkill.getDSDamage(_pc, _target, _weaponEnchant);
					break;
				case 90084:
					dmg += WeaponSkill.getSummyulDamage(_pc, _target, _weaponEnchant);
					break;
				default:
					break;
				}
			} else {
				switch (_weaponId) {
				case 90086:
				case 90087:
				case 90088:
				case 90090:
				case 90091:
				case 90092:
					WeaponSkill.블레이즈쇼크(_pc, _targetPc, _weaponEnchant);
					break;
				case 7236:
					WeaponSkill.getDeathKnightjin(_pc, _targetPc);
					break;
				case 2:
				case 200002:
					dmg = WeaponSkill.getDiceDaggerDamage(_pc, _targetPc,
							weapon);
					return (int) dmg; // break;
				case 13:
				case 44:
					WeaponSkill.getPoisonSword(_pc, _targetPc);
					break;
				case 100047:
				case 47:
					WeaponSkill.getSilenceSword(_pc, _targetPc, _weaponEnchant);
					break;
				case 90085:
				case 134:
					dmg += WeaponSkill.get수결지Damage(_pc, _target,
							_weaponEnchant);
					break;

				case 54:
					dmg += WeaponSkill.getKurtSwordDamage(_pc, _targetPc,
							_weaponEnchant);
					break;
				case 58:
					dmg += WeaponSkill.getDeathKnightSwordDamage(_pc,
							_targetPc, _weaponEnchant);
					break;
					// case 59: dmg += WeaponSkill.getBarlogSwordDamage(_pc,
					// _target, _weaponEnchant); break;
				case 76:
					dmg += WeaponSkill.getRondeDamage(_pc, _targetPc,
							_weaponEnchant);
					break;
				case 121:
					dmg += WeaponSkill.getIceQueenStaffDamage(_pc, _target,
							_weaponEnchant);
					break;
				case 124:
				case 100124:
					dmg += WeaponSkill.getBaphometStaffDamage(_pc, _target,
							_weaponEnchant);
					break;
				case 9080:
				case 191:// 살천의활
					// WeaponSkill.giveSalCheonEffect(_pc, _target);
					// DrainofEvil(); break;
					dmg += WeaponSkill.getEffectDamage(_pc, _target,
							_weaponEnchant, 9361);
					break;
					// case 114: WeaponSkill.getGunjuSword(_pc,_target
					// ,_weaponEnchant); break; //추가
				case 203:
					dmg += WeaponSkill.getBarlogSwordDamage(_pc, _target);
					break;
			//	case 86: // 붉이
				case 204: // 진홍의 크로스보우

				case 100204:
					WeaponSkill.giveFettersEffect(_pc, _targetPc);
					break;
				case 205:
				case 100205:
					dmg += WeaponSkill.getMoonBowDamage(_pc, _target,
							_weaponEnchant);
					break;
				case 264:
				case 265:
				case 256:
				case 4500027:
					dmg += WeaponSkill
					.getEffectSwordDamage(_pc, _target, 11107);
					break;
				case 9102:
				case 90089:
					dmg += WeaponSkill.getDarkMDamage(_pc, _target, _weaponEnchant);
					break;
				case 9079:
				case 412000: // dmg += WeaponSkill.getEffectSwordDamage(_pc,
					// _target, 10); break;
					dmg += WeaponSkill.getEffectDamage(_pc, _target,
							_weaponEnchant, 3940);
					break;
					/*
					 * case 410000: case 410001: case 450009: case 450014: case
					 * 450004: dmg += WeaponSkill.getChainSwordDamage(_pc, _target);
					 * break;
					 */
				case 412004: // dmg += WeaponSkill.getIceSpearDamage(_pc,
					// _target); break;
					dmg += WeaponSkill.getEffectDamage(_pc, _target,
							_weaponEnchant, 3704);
					break;
				case 7228:
				case 9075:
				case 412005: // dmg += WeaponSkill.geTornadoAxeDamage(_pc,
					// _target); break;
					dmg += WeaponSkill.getEffectDamage(_pc, _target,
							_weaponEnchant, 5524);
					break;
					// case 412003: WeaponSkill.getDiseaseWeapon(_pc, _target,
					// 412003); break;
				case 413101:
				case 413102:
				case 413104:
				case 413105:
					WeaponSkill.getDiseaseWeapon(_pc, _target, 413101);
					break;
				case 413103:
					calcStaffOfMana();
					WeaponSkill.getDiseaseWeapon(_pc, _target, 413101);
					break;
				case 415010:
				case 415011:
				case 415012:
				case 415013:
					WeaponSkill.체이서(_pc, _target, _weaponEnchant, 0);
					break;
				case 415015:
				case 415016:
					WeaponSkill.체이서(_pc, _target, _weaponEnchant, 1);
					break;
				case 6000:
					dmg += WeaponSkill.IceChainSword(_pc, _target,
							_weaponEnchant);
					break;
				case 6001:
					dmg += WeaponSkill.Icekiring(_pc, _target, _weaponEnchant);
					break;
				case 6101:
					dmg += WeaponSkill.소울커터(_pc, _target, _weaponEnchant);
					break;
				case 7:
				case 35:
				case 48:
				case 73:
				case 105:
				case 120:
				case 147:
				case 156:
				case 174:
				case 175:
				case 224:
				case 7232:
					dmg += WeaponSkill.플라즈마(_pc, _target, _weaponEnchant);
					break;
				case 450008:
				case 450022:
				case 450024:
				case 450010:
					WeaponSkill.이블리버스(_pc, _target, _weaponEnchant);
					break;

				case 450011:
				case 450012:
				case 450013:
				case 450023:
				case 450025:
					WeaponSkill.이블트릭(_pc, _target, _weaponEnchant);
					break;
				case 263:
				case 4500028: // dmg += WeaponSkill.halloweenCus(_pc, _target);
					// break;
				case 4500026:
					dmg += WeaponSkill.halloweenCus2(_pc, _target);
					break;// 각궁
					// case 263:
					// case 4500028: dmg += WeaponSkill.halloweenCus(_pc, _target);
					// break;
				case 100259:
				case 100260:
				case 9081:
				case 259:
				case 260:
					dmg += WeaponSkill.getEffectDamage(_pc, _target,
							_weaponEnchant, 9359);
					break; // 파괴의 크로우, 이도류
				case 9077:
				case 266:
				case 100266:
					dmg += WeaponSkill.PhantomShock(_pc, _target,
							_weaponEnchant);
					break; // 공명의 키링크
				case 9076:
				case 275:
				case 100275:
					dmg += WeaponSkill.getEffectDamage(_pc, _target,
							_weaponEnchant, 7398);
					break; // 환영의 체인소드
				case 277:
				case 278:
				case 279:
				case 280:
				case 281:
				case 282:
				case 283:
					WeaponSkill.이블리버스(_pc, _target, _weaponEnchant);
					break; // 붉은 사자 무기
				case 284:
				case 285:
				case 286:
				case 287:
				case 288:
				case 289:
				case 290:
					dmg += WeaponSkill.getEffectDamage(_pc, _target,
							_weaponEnchant, 3939);
					break; // 환상의 무기 (10검 이벤트)
				case 291: // 제로스의지팡이
					dmg += WeaponSkill.getZerosDamage(_pc, _target,
							_weaponEnchant);
					break;
				case 9106:
					dmg += WeaponSkill.getJinSSaulDamage(_pc, _target, _weaponEnchant);
					break;
				case 293:
					dmg += WeaponSkill.getAkmongDamage(_pc, _target, _weaponEnchant);
					break;
				case 66:
					dmg += WeaponSkill.getDSDamage(_pc, _target, _weaponEnchant);
					break;
				case 90084:
					dmg += WeaponSkill.getSummyulDamage(_pc, _target, _weaponEnchant);
					break;	
				default:
					break;
				}
			}
			if (_weaponId == 450009) {
				dmg += WeaponSkill.getChainSwordDamage(_pc, _target, _weaponId);
				WeaponSkill.이블리버스(_pc, _target, _weaponEnchant);
			}
			if (secondw) {
				if(_SweaponId == 7227){
					if(_SweaponEnchant > 9)
					dmg += WeaponSkill.getTaepoongDamage(_pc, _target, _SweaponEnchant);
				}
				if (_SweaponId == 7233) {
					WeaponSkill.이블리버스(_pc, _target, _SweaponEnchant);
				}
			} else {
				if(_weaponId == 7227){
					if(_weaponEnchant > 9)
					dmg += WeaponSkill.getTaepoongDamage(_pc, _target, _weaponEnchant);
				}
				if (_weaponId == 7233) {
					WeaponSkill.이블리버스(_pc, _target, _weaponEnchant);
				}
			}
		}

		// 축무기 데미지 추가
		if(weapon != null){ if(weapon.getBless() == 0){ dmg += 1; } }

		if (_weaponId >= 277 && _weaponId <= 283) {
			dmg += _weaponEnchant;
		}

		for (int i = 21242; i <= 21245; i++) {
			L1ItemInstance PVP방어구 = _targetPc.getInventory().checkEquippedItem(i);
			if (PVP방어구 != null) {
				dmg -= PVP방어구.getEnchantLevel() + 1;
			}
		}
		if(weapon != null){
		DistroyArmorDurability();
		}
		
		if(_targetPc.getInventory().checkEquipped(22002)){ // 크로노스의 벨트 착용
			dmg -= 2;
		}
		
		if (_calcType == PC_PC) {
				dmg += _pc.getRankpvpdmg();
				dmg -= _pc.getRankpvprdc();
			if(_targetPc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.정상의가호)){
				dmg -= 2;
			}
			if(_pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.승리의기운)){
				dmg += 2;
			}
			if(_targetPc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.승리의기운)){
				dmg -= 2;
			}
		}
		
		/** 포커스 웨이브 **/
		포커스웨이브(_pc, _targetPc);
		 
		if (_weaponId >= 277 && _weaponId <= 283) {
			dmg += _weaponEnchant;
		}

		if (_weaponId == 9078 || _weaponId == 276 || _weaponId == 100276 || // 붉은기사의대검
				_weaponId == 292 || _weaponId == 293) {// 진노의크로우, 악몽이장궁
			if (_pc.getLawful() <= -20000)
				dmg += 5;
			else if (_pc.getLawful() <= -10000)
				dmg += 3;
			else if (_pc.getLawful() <= 0)
				dmg += 1;
			/*
			 * double lawful = 32767 - _pc.getLawful(); lawful *= 0.0000037;//만카
			 * 24%정도 만라 0% System.out.println(_pc.getLawful()); dmg += dmg *
			 * lawful;
			 */
		}
		if (_pc.getDollList().size() > 0) {
			for (L1DollInstance doll : _pc.getDollList()) {
				if (_weaponType != 20 && _weaponType != 62) {
					int d = doll.getDamageByDoll(_targetPc);
					if (d > 0
							&& doll.getDollType() == L1DollInstance.DOLLTYPE_그레그)
						_pc.setCurrentHp(_pc.getCurrentHp()
								+ _random.nextInt(5) + 1);
					dmg += d;
				}

				if (doll.getDollType() == L1DollInstance.DOLLTYPE_흑장로
						|| doll.getDollType() == L1DollInstance.DOLLTYPE_데스나이트) {
					dmg += doll.getMagicDamageByDoll(_targetPc);
				}

				doll.attackPoisonDamage(_pc, _targetPc);
			}
		}

		if (adddmg != 0)
			dmg += adddmg;

		/** 마법인형 돌골램 **/
		if (_targetPc.getDollList().size() > 0 && _weaponType != 20
				&& _weaponType != 62) {
			for (L1DollInstance doll : _targetPc.getDollList()) {
				if (doll.getDollType() == L1DollInstance.DOLLTYPE_STONEGOLEM
						|| doll.getDollType() == L1DollInstance.DOLLTYPE_드레이크)
					dmg -= doll.getDamageReductionByDoll();
			}
		}
		/** 마법인형 돌골램 **/
		if (_pc.getSkillEffectTimerSet().hasSkillEffect(BURNING_SLASH)) {
			dmg += 50;
			_pc.sendPackets(new S_SkillSound(_targetPc.getId(), 6591), true);
			Broadcaster.broadcastPacket(_pc, new S_SkillSound(
					_targetPc.getId(), 6591), true);
			_pc.getSkillEffectTimerSet().killSkillEffectTimer(BURNING_SLASH);
		}

		if (_pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.주군의버프)) {
			if (_pc.getClanRank() >= L1Clan.CLAN_RANK_GUARDIAN)
				dmg += 5;
		}
		
		dmg += 룸티스검귀추가데미지();

		if (_targetPc.getSkillEffectTimerSet().hasSkillEffect(IMMUNE_TO_HARM)) {
			dmg *= _targetPc.immuneLevel;
		}

		if (_targetPc.isAmorGaurd) { // 아머가드에의한 데미지감소
			int d = _targetPc.getAC().getAc() / 20;

			if (d < 0) {
				dmg += d;
			} else {
				dmg -= d;
			}
		}

		// if(_weaponType1 != 17) // 키링크 아닐때만
		dmg -= _targetPc.getDamageReductionByArmor(); // 방어용 기구에 의한 데미지 경감

		if (_targetPc.getSkillEffectTimerSet().hasSkillEffect(SPECIAL_COOKING)) { // 스페셜요리에
			// 의한
			// 데미지
			// 경감
			dmg -= 5;
		}
		if (_targetPc.getSkillEffectTimerSet().hasSkillEffect(SPECIAL_COOKING2)) {
			dmg -= 5;
		}

		if (_targetPc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.EARTH_BLESS))
			dmg -= 2;

		if (_targetPc.getSkillEffectTimerSet().hasSkillEffect(COOKING_NEW_닭고기)
				|| _targetPc.getSkillEffectTimerSet().hasSkillEffect(
						COOKING_NEW_연어)
						|| _targetPc.getSkillEffectTimerSet().hasSkillEffect(
								COOKING_NEW_칠면조)
								|| _targetPc.getSkillEffectTimerSet().hasSkillEffect(
										COOKING_NEW_한우)) {
			dmg -= 2;
		}
		if (_targetPc.getSkillEffectTimerSet().hasSkillEffect(메티스정성스프)
				|| _targetPc.getSkillEffectTimerSet().hasSkillEffect(싸이시원한음료))
			dmg -= 5;
		if (_targetPc.getSkillEffectTimerSet().hasSkillEffect(메티스정성요리)
				|| _targetPc.getSkillEffectTimerSet().hasSkillEffect(싸이매콤한라면))
			dmg -= 5;
		if (_targetPc.getSkillEffectTimerSet().hasSkillEffect(메티스축복주문서)
				|| _targetPc.getSkillEffectTimerSet().hasSkillEffect(
						L1SkillId.흑사의기운))
			dmg -= 3;

		dmg -= 룸티스붉귀데미지감소();
		dmg -= 스냅퍼체반데미지감소();

		// 키링크 아닐때만 추가
		if (_weaponType1 != 17 && _targetPc.getSkillEffectTimerSet().hasSkillEffect(REDUCTION_ARMOR)) {
			int targetPcLvl = _targetPc.getLevel();
			if (targetPcLvl < 50) {
				targetPcLvl = 50;
			}
			int dmg2 = (targetPcLvl - 40) / 3 - 3;
			dmg -= dmg2 > 0 ? dmg2 : 0;// +1
		}

		if (_target != _targetNpc) {
			L1ItemInstance 반역자의방패 = _targetPc.getInventory().checkEquippedItem(21093);
			L1ItemInstance 신성요방 = _targetPc.getInventory().checkEquippedItem(9205);
			
			L1ItemInstance 암석부츠 = _targetPc.getInventory().checkEquippedItem(4641);
			L1ItemInstance 암석망토 = _targetPc.getInventory().checkEquippedItem(4643);
			L1ItemInstance 암석장갑 = _targetPc.getInventory().checkEquippedItem(4642);
			L1ItemInstance 암석각반 = _targetPc.getInventory().checkEquippedItem(4640);
			
			L1ItemInstance 마물부츠 = _pc.getInventory().checkEquippedItem(4541);
			L1ItemInstance 마물망토 = _pc.getInventory().checkEquippedItem(4543);
			L1ItemInstance 마물장갑 = _pc.getInventory().checkEquippedItem(4542);
			L1ItemInstance 마물각반 = _pc.getInventory().checkEquippedItem(4540);
			
			L1ItemInstance 발라카스의완력 = _pc.getInventory().checkEquippedItem(420112);
			L1ItemInstance 발라카스의예지력 = _pc.getInventory().checkEquippedItem(420113);
			L1ItemInstance 발라카스의인내력 = _pc.getInventory().checkEquippedItem(420114);
			L1ItemInstance 발라카스의마력 = _pc.getInventory().checkEquippedItem(420115);
			
			if (발라카스의완력 != null) {
				int ReducLevel = 0;
				int targetReduc = _targetPc.getDamageReductionByArmor();
				int 발동찬스 = _random.nextInt(100);
				switch(발라카스의완력.getEnchantLevel()){
				case 0: case 1: case 2: case 3: case 4: case 5:
					ReducLevel = 3;
					break;	
				case 6:
					ReducLevel = 4;
					break;
				case 7:
					ReducLevel = 5;
					break;
				case 8:
					ReducLevel = 6;
					break;
				case 9:
					ReducLevel = 7;
				break;
				default:
					if(발라카스의완력.getEnchantLevel() > 9){
						ReducLevel = 7;
					}
					break;
				}
				if (targetReduc < ReducLevel){
					ReducLevel = targetReduc;
				}
				if(ReducLevel < 0){
					ReducLevel = 0;
				}
				
				if(발동찬스 < 발라카스의완력.getEnchantLevel()){
					if (_weaponType != 20 && _weaponType != 62) {
						_발라근거리가호 = true;
					}
				}
				dmg += ReducLevel;
			}
			
			if (발라카스의예지력 != null) {
				int ReducLevel = 0;
				int targetReduc = _targetPc.getDamageReductionByArmor();
				int 발동찬스 = _random.nextInt(100);
				switch(발라카스의예지력.getEnchantLevel()){
				case 0: case 1: case 2: case 3: case 4: case 5:
					ReducLevel = 3;
					break;	
				case 6:
					ReducLevel = 4;
					break;
				case 7:
					ReducLevel = 5;
					break;
				case 8:
					ReducLevel = 6;
					break;
				case 9:
					ReducLevel = 7;
				break;
				default:
					if(발라카스의예지력.getEnchantLevel() > 9){
						ReducLevel = 7;
					}
					break;
				}
				if (targetReduc < ReducLevel){
					ReducLevel = targetReduc;
				}
				if(ReducLevel < 0){
					ReducLevel = 0;
				}
				if(발동찬스 < 발라카스의예지력.getEnchantLevel()){
					if (_weaponType != 20 && _weaponType != 62) {
						_발라근거리가호 = true;
					}
				}
				dmg += ReducLevel;
			}
			if (발라카스의인내력 != null) {
				int ReducLevel = 0;
				int targetReduc = _targetPc.getDamageReductionByArmor();
				int 발동찬스 = _random.nextInt(100);
				switch(발라카스의인내력.getEnchantLevel()){
				case 0: case 1: case 2: case 3: case 4: case 5:
					ReducLevel = 3;
					break;	
				case 6:
					ReducLevel = 4;
					break;
				case 7:
					ReducLevel = 5;
					break;
				case 8:
					ReducLevel = 6;
					break;
				case 9:
					ReducLevel = 7;
				break;
				default:
					if(발라카스의인내력.getEnchantLevel() > 9){
						ReducLevel = 7;
					}
					break;
				}
				if (targetReduc < ReducLevel){
					ReducLevel = targetReduc;
				}
				if(ReducLevel < 0){
					ReducLevel = 0;
				}
				if(발동찬스 < 발라카스의인내력.getEnchantLevel()){
					if (_weaponType == 20 || _weaponType == 62) {
						_발라원거리가호 = true;
					}
				}
				dmg += ReducLevel;
			}
			if (발라카스의마력 != null) {
				int ReducLevel = 0;
				int targetReduc = _targetPc.getDamageReductionByArmor();
				int 발동찬스 = _random.nextInt(100);
				switch(발라카스의마력.getEnchantLevel()){
				case 0: case 1: case 2: case 3: case 4: case 5:
					ReducLevel = 3;
					break;	
				case 6:
					ReducLevel = 4;
					break;
				case 7:
					ReducLevel = 5;
					break;
				case 8:
					ReducLevel = 6;
					break;
				case 9:
					ReducLevel = 7;
				break;
				default:
					if(발라카스의마력.getEnchantLevel() > 9){
						ReducLevel = 7;
					}
					break;
				}
				if (targetReduc < ReducLevel){
					ReducLevel = targetReduc;
				}
				if(ReducLevel < 0){
					ReducLevel = 0;
				}
				if(발동찬스 < 발라카스의마력.getEnchantLevel()){
					if (_weaponType1 == 17) {
						_발라근거리가호 = true;
					}
				}
				dmg += ReducLevel;
			}

			if (반역자의방패 != null) {
				int chance = 반역자의방패.getEnchantLevel() * 2;
				if (_random.nextInt(100) <= chance) {
					dmg -= 50;
					_targetPc.sendPackets(new S_SkillSound(_targetPc.getId(), 6320));
					Broadcaster.broadcastPacket(_targetPc, new S_SkillSound(_targetPc.getId(), 6320));
				}
			}
			
			if (신성요방 != null) {
				int chance = 신성요방.getEnchantLevel();
				if (_random.nextInt(100) <= chance) {
					dmg -= 10;
					_targetPc.sendPackets(new S_SkillSound(_targetPc.getId(), 14543));
					Broadcaster.broadcastPacket(_targetPc, new S_SkillSound(_targetPc.getId(), 14543));
				}
			}
			
			if(암석부츠 != null){
				dmg -= 2;
			}
			if(암석장갑 != null){
				dmg -= 2;
			}
			if(암석각반 != null){
				dmg -= 2;
			}
			if(암석망토 != null){
				dmg -= 2;
			}
			
			if(마물부츠 != null){
				dmg += 2;
			}
			if(마물장갑 != null){
				dmg += 2;
			}
			if(마물각반 != null){
				dmg += 2;
			}
			if(마물망토 != null){
				dmg += 2;
			}
		}
		if (_targetPc.getSkillEffectTimerSet().hasSkillEffect(ARMOR_BREAK)) {
			if (_weaponType != 20 && _weaponType != 62) {
				dmg *= 1.58;
			}
			}
			
		if (_targetPc.getSkillEffectTimerSet().hasSkillEffect(IllUSION_AVATAR)
				|| _targetPc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.CUBE_AVATAR)) {
			dmg += dmg / 20;
		}
		if (_targetPc.getSkillEffectTimerSet().hasSkillEffect(PATIENCE)) {
			dmg -= 4;
		}
		if (_targetPc.getSkillEffectTimerSet().hasSkillEffect(FEATHER_BUFF_A)) {
			dmg -= 3;
		}
		if (_targetPc.getSkillEffectTimerSet().hasSkillEffect(FEATHER_BUFF_B)) {
			dmg -= 2;
		}

		if (_pc.getSkillEffectTimerSet().hasSkillEffect(VALA_MAAN) // 화룡의 마안 - 일정확률로 물리추가타격+2
			|| _pc.getSkillEffectTimerSet().hasSkillEffect(LIFE_MAAN)) { // 생명의
			// 마안 -
			// 일정확률로
			// 물리추가타격+2
			int MaanAttDmg = _random.nextInt(100) + 1;
			if (MaanAttDmg <= 30) { // 확률
				dmg += 2;
			}
		}
	
		if (_targetPc != null) {
			if (_targetPc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.TRUE_TARGET)) {
				if (_pc != null) {
					if (_targetPc.tt_clanid == _pc.getClanid() || _targetPc.tt_partyid == _pc.getPartyID()) {
						if (_targetPc.tt_level >= 90) {
							dmg += dmg * 0.06;
						} else if (_targetPc.tt_level >= 75) {
							dmg += dmg * 0.05;
						} else if (_targetPc.tt_level >= 60) {
							dmg += dmg * 0.04;
						} else if (_targetPc.tt_level >= 45) {
							dmg += dmg * 0.03;
						} else if (_targetPc.tt_level >= 30) {
							dmg += dmg * 0.02;
						} else if (_targetPc.tt_level >= 15) {
							dmg += dmg * 0.01;
						}
					}
				}
			}
		}

		if (_targetPc.getInventory().checkEquipped(21104) || _targetPc.getInventory().checkEquipped(21111)) {
			// dmg += dmg*0.2;
			if (Config.수련자갑옷밸런스수치 != 0) {
				double balance = Config.수련자갑옷밸런스수치 * 0.01;
				dmg += dmg * balance;// 수련자 무기 35%하향
			}
		}
		if (_targetPc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.루시퍼)) {
			if(!_targetPc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.IMMUNE_TO_HARM))
			dmg *= 0.9;
		}

		if (_pc.getGfxId().getTempCharGfx() == 17541 || _pc.getGfxId().getTempCharGfx() == 17515
				|| _pc.getGfxId().getTempCharGfx() == 17531 || _pc.getGfxId().getTempCharGfx() == 17545
				|| _pc.getGfxId().getTempCharGfx() == 17535 || _pc.getGfxId().getTempCharGfx() == 17549
				|| _pc.getGfxId().getTempCharGfx() == 13715 || _pc.getGfxId().getTempCharGfx() == 13727
				|| _pc.getGfxId().getTempCharGfx() == 13717 || _pc.getGfxId().getTempCharGfx() == 13729
				|| _pc.getGfxId().getTempCharGfx() == 15115 || _pc.getGfxId().getTempCharGfx() == 13731
				|| _pc.getGfxId().getTempCharGfx() == 13721 || _pc.getGfxId().getTempCharGfx() == 13733
				|| _pc.getGfxId().getTempCharGfx() == 13723 || _pc.getGfxId().getTempCharGfx() == 13735
				|| _pc.getGfxId().getTempCharGfx() == 13725 || _pc.getGfxId().getTempCharGfx() == 13737
				|| _pc.getGfxId().getTempCharGfx() == 13739 || _pc.getGfxId().getTempCharGfx() == 13743
				|| _pc.getGfxId().getTempCharGfx() == 13741 || _pc.getGfxId().getTempCharGfx() == 13745) {
			dmg += 2;
		}
		
		if (_weaponId == 7 || _weaponId == 35 || _weaponId == 48
				|| _weaponId == 73 || _weaponId == 105 || _weaponId == 120
				|| _weaponId == 147 || _weaponId == 156 || _weaponId == 174
				|| _weaponId == 175 || _weaponId == 224 || _weaponId == 7232) {
			if (Config.수련자무기밸런스수치 != 0) {
				double balance = Config.수련자무기밸런스수치 * 0.01;
				dmg -= dmg * balance;// 수련자 무기 35%하향
			}
		}
		
		if (_calcType == 1){
		if (_targetPc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.MOEBIUS)){
        	if (_weaponType == 20 || _weaponType == 62){
        		dmg = 0;
        	}
        }
		}
		
		if (_targetPc.getSkillEffectTimerSet().hasSkillEffect(MAJESTY)) {
            if (_targetPc.getLevel() >= 80) {
                dmg -= 2 + ((_targetPc.getLevel() - 80) / 2);
            } else {
                dmg -= 2;
            }
        }
		
		if (dmg <= 0) {
			_isHit = false;
			_drainHp = 0;
		}
		// 피흡수 공식 바꾸기위해 옮김.
		if (_weaponId == 262) { // 블러드서커
			if (dmg >= 30 && _random.nextInt(100) <= 90) {
				// _pc.sendPackets(new
				// S_SystemMessage("피흡량 :"+_drainHp+" 데미지 :"+dmg));
				int _dhp = (int) ((dmg - 10) / 10);
				_drainHp += _dhp;// +_random.nextInt(3)+1;
			}
		}
		
		if(_weaponId == 9100){
			int targetReduc = _targetPc.getDamageReductionByArmor();
			if (targetReduc > weapon.getEnchantLevel() + 9){
				targetReduc = weapon.getEnchantLevel() + 9;
			}
			if(targetReduc > 0){
				dmg += targetReduc;
			} else {
				dmg += 0;
			}
		}
		
		if (_pc.getAddDamageRate() >= CommonUtil.random(100)) {
			dmg += _pc.getAddDamage();
		}			
		if (_targetPc.getAddReductionRate() >= CommonUtil.random(100)) {
			dmg -= _targetPc.getAddReduction();
		}
		
		/** 케릭터별 PVP대미지 감소 리뉴얼 **/   //pvp데미지
		if ((_targetPc.isCrown() || _targetPc.isElf() || _targetPc.isWizard()) && _targetPc.getLevel() >= 60){
			dmg -= (_targetPc.getLevel() - 60 ) / 4;
		} else if ((_targetPc.isKnight()) && _targetPc.getLevel() >= 60){
			dmg -= (_targetPc.getLevel() - 60 ) / 2;
		} else if ((_targetPc.isDarkelf() || _targetPc.isDragonknight() 
				|| _targetPc.isIllusionist() || _targetPc.isWarrior()) && _targetPc.getLevel() >= 60){
			dmg -= (_targetPc.getLevel() - 60 ) / 3;
		}
		
		/** 무기별 인첸당 추타 **/
		L1EnchantDamage l1enchantdamage = WeaponPerDamage.getInstance().getTemplate(weapon.getItem().getItemId());
		int e0 = l1enchantdamage.get0();
		int e1 = l1enchantdamage.get1();
		int e2 = l1enchantdamage.get2();
		int e3 = l1enchantdamage.get3();
		int e4 = l1enchantdamage.get4();
		int e5 = l1enchantdamage.get5();
		int e6 = l1enchantdamage.get6();
		int e7 = l1enchantdamage.get7();
		int e8 = l1enchantdamage.get8();
		int e9 = l1enchantdamage.get9();
		int e10 = l1enchantdamage.get10();
		int e11 = l1enchantdamage.get11();
		int e12 = l1enchantdamage.get12();
		int e13 = l1enchantdamage.get13();
		int e14 = l1enchantdamage.get14();
		
		try{
			if(weapon != null){
			int enchant = weapon.getEnchantLevel();
			switch(enchant){
			case 0 : dmg += e0; break;
			case 1 : dmg += e1; break;
			case 2 : dmg += e2; break;
			case 3 : dmg += e3; break;
			case 4 : dmg += e4; break;
			case 5 : dmg += e5; break;
			case 6 : dmg += e6; break;
			case 7 : dmg += e7; break;
			case 8 : dmg += e8; break;
			case 9 : dmg += e9; break;
			case 10 : dmg += e10; break;
			case 11 : dmg += e11; break;
			case 12 : dmg += e12; break;
			case 13 : dmg += e13; break;
			case 14 : dmg += e14; break;
			default:
			break;
			}
			if(_targetPc.isCrown()){
				dmg -= CharacterReduc.getInstance().getCharacterReduc(0);
			} else if(_targetPc.isKnight()){
				dmg -= CharacterReduc.getInstance().getCharacterReduc(1);
			} else if(_targetPc.isElf()){
				dmg -= CharacterReduc.getInstance().getCharacterReduc(2);
			} else if(_targetPc.isWizard()){
				dmg -= CharacterReduc.getInstance().getCharacterReduc(3);
			} else if(_targetPc.isDarkelf()){
				dmg -= CharacterReduc.getInstance().getCharacterReduc(4);
			} else if(_targetPc.isIllusionist()){
				dmg -= CharacterReduc.getInstance().getCharacterReduc(5);
			} else if(_targetPc.isDragonknight()){
				dmg -= CharacterReduc.getInstance().getCharacterReduc(6);
			} else if(_targetPc.isWarrior()){
				dmg -= CharacterReduc.getInstance().getCharacterReduc(7);
			}
			if(_pc.isCrown()){
				dmg += CharacterBalance.getInstance().getCharacterBalance(0);
			} else if(_pc.isKnight()){
				dmg += CharacterBalance.getInstance().getCharacterBalance(1);
			} else if(_pc.isElf()){
				dmg += CharacterBalance.getInstance().getCharacterBalance(2);
			} else if(_pc.isWizard()){
				dmg += CharacterBalance.getInstance().getCharacterBalance(3);
			} else if(_pc.isDarkelf()){
				dmg += CharacterBalance.getInstance().getCharacterBalance(4);
			} else if(_pc.isIllusionist()){
				dmg += CharacterBalance.getInstance().getCharacterBalance(5);
			} else if(_pc.isDragonknight()){
				dmg += CharacterBalance.getInstance().getCharacterBalance(6);
			} else if(_pc.isWarrior()){
				dmg += CharacterBalance.getInstance().getCharacterBalance(7);
			}
			dmg += WeaponAddDamage.getInstance().getWeaponAddDamage(_weaponId);
			} else {
				dmg = 1;
			}
		} catch (Exception e){
			 System.out.println("Character Enchant Per Damage for L1Attack");
		}
		return (int) dmg;
	}

	private double 룸티스검귀추가데미지() {
		int dmg = 0;
		if(_calcType == PC_PC || _calcType == PC_NPC) {
			L1ItemInstance blackRumti = _pc.getInventory().checkEquippedItem(500010);
			if(blackRumti == null)
				blackRumti = _pc.getInventory().checkEquippedItem(502010);
			if(blackRumti != null) {
				int chance = 0;
				if(blackRumti.getBless() == 0 && blackRumti.getEnchantLevel() >= 4) {
					chance = 2 + blackRumti.getEnchantLevel() - 4;
				}
				else if(blackRumti.getEnchantLevel() >= 5) {
					chance = 2 + blackRumti.getEnchantLevel() - 5;
				}
				if(chance != 0) {
					if(_random.nextInt(100) < chance) {
						dmg += 20;
						_pc.sendPackets(new S_SkillSound(_pc.getId(), 13931));
						//_pc.broadcastPacket(new S_SkillSound(_pc.getId(), 13931));
					}
				}
			}
		}
		return dmg;
	}

	private int calcSAttrEnchantDmg() {
		if (_SweaponAttrEnchantLevel <= 0)
			return 0;
		int dmg = 0;
		/** 속성인챈트 추가 타격치 */
		switch (_SweaponAttrEnchantLevel) {
		case 1:
		case 6:
		case 11:
		case 16:
			dmg = 1;
			break;
		case 2:
		case 7:
		case 12:
		case 17:
			dmg = 3;
			break;
		case 3:
		case 8:
		case 13:
		case 18:
			dmg = 5;
			break;
		case 4:
		case 9:
		case 14:
		case 19:
			dmg = 7;
			break;
		case 5:
		case 10:
		case 15:
		case 20:
			dmg = 9;
			break;

		case 33:
		case 35:
		case 37:
		case 39:
			dmg = 7;
			break;
		case 34:
		case 36:
		case 38:
		case 40:
			dmg = 9;
			break;
		default:
			dmg = 0;
			break;
		}
		int attr = 0;
		switch (_SweaponAttrEnchantLevel) {
		case 1:
		case 2:
		case 3:
		case 4:
		case 5:
			attr = 2;
			break;
		case 6:
		case 7:
		case 8:
		case 9:
		case 10:
			attr = 4;
			break;
		case 11:
		case 12:
		case 13:
		case 14:
		case 15:
			attr = 8;
			break;
		case 16:
		case 17:
		case 18:
		case 19:
		case 20:
			attr = 1;
			break;
		default:
			break;
		}
		dmg -= dmg * calcAttrResistance(attr);
		return dmg;

	}

	private int calcAttrEnchantDmg() {
		if (_weaponAttrEnchantLevel <= 0)
			return 0;
		int dmg = 0;
		/** 속성인챈트 추가 타격치 */

		switch (_weaponAttrEnchantLevel) {
		case 1:
		case 6:
		case 11:
		case 16:
			dmg = 1;
			break;
		case 2:
		case 7:
		case 12:
		case 17:
			dmg = 3;
			break;
		case 3:
		case 8:
		case 13:
		case 18:
			dmg = 5;
			break;
		case 4:
		case 9:
		case 14:
		case 19:
			dmg = 7;
			break;
		case 5:
		case 10:
		case 15:
		case 20:
			dmg = 9;
			break;

		case 33:
		case 35:
		case 37:
		case 39:
			dmg = 7;
			break;
		case 34:
		case 36:
		case 38:
		case 40:
			dmg = 9;
			break;
		default:
			dmg = 0;
			break;
		}

		int attr = 0;
		switch (_weaponAttrEnchantLevel) {
		case 1:
		case 2:
		case 3:
		case 4:
		case 5:
			attr = 2;
			break;
		case 6:
		case 7:
		case 8:
		case 9:
		case 10:
			attr = 4;
			break;
		case 11:
		case 12:
		case 13:
		case 14:
		case 15:
			attr = 8;
			break;
		case 16:
		case 17:
		case 18:
		case 19:
		case 20:
			attr = 1;
			break;
		default:
			break;
		}

		dmg -= dmg * calcAttrResistance(attr);
		return dmg;

	}

	private int addattrdmg(int attr) {
		int adddmg = 0;
		int orgattr = 0;
		int npc_att = _targetNpc.getNpcTemplate().get_weakAttr();
		if (npc_att == 0)
			return 0;
		switch (attr) {
		case 1:
			orgattr = 4;
			if (_pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.강화버프_수공)){
				adddmg += 2;
			}
			break;// 수
		case 2:
			orgattr = 8;
			if (_pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.강화버프_풍공)){
				adddmg += 2;
			}
			break;// 풍
		case 3:
			orgattr = 1;
			if (_pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.강화버프_지공)){
				adddmg += 2;
			}
			break;// 지
		case 4:
			orgattr = 2;
			if (_pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.강화버프_화공)){
				adddmg += 2;
			}
			break;// 화
		default:
			break;
		}

		if (orgattr == npc_att) {
			adddmg = 3;
		}

		return adddmg;
	}

	private double calcAttrResistance(int attr) {
		int resist = 0;
		if (_calcType == PC_PC || _calcType == NPC_PC) {
			switch (attr) {
			case L1Skills.ATTR_EARTH:
				resist = _targetPc.getResistance().getEarth();
				break;
			case L1Skills.ATTR_FIRE:
				resist = _targetPc.getResistance().getFire();
				break;
			case L1Skills.ATTR_WATER:
				resist = _targetPc.getResistance().getWater();
				break;
			case L1Skills.ATTR_WIND:
				resist = _targetPc.getResistance().getWind();
				break;
			default:
				break;
			}
		} else if (_calcType == PC_NPC || _calcType == NPC_NPC) {
			// 취약속성 이외 일경우 속성데미지 안들어가게
			int npc_att = _targetNpc.getNpcTemplate().get_weakAttr();
			if (npc_att == 0)
				return 0;
			if (npc_att >= 8) {
				npc_att -= 8;
				if (attr == 8)
					return 0;
			}
			if (npc_att >= 4) {
				npc_att -= 4;
				if (attr == 4)
					return 0;
			}
			if (npc_att >= 2) {
				npc_att -= 2;
				if (attr == 2)
					return 0;
			}
			if (npc_att >= 1) {
				npc_att -= 1;
				if (attr == 1)
					return 0;
			}
			return 1;
		}

		/*
		 * int resistFloor = (int) (0.32 * Math.abs(resist)); if (resist >= 0) {
		 * resistFloor *= 1; } else { resistFloor *= -1; }
		 * 
		 * double attrDeffence = resistFloor / 32.0;
		 */

		double attrDeffence = resist / 5 * 0.01;

		return attrDeffence;
	}

	// ●●●● 플레이어로부터 NPC 에의 데미지 산출 ●●●●
	private int calcPcNpcDamage(int adddmg) {
		if (_pc instanceof L1RobotInstance) {
			if (((L1RobotInstance) _pc).사냥봇_위치.equalsIgnoreCase("지저")
					|| ((L1RobotInstance) _pc).사냥봇_위치.equalsIgnoreCase("선박수면")
					|| ((L1RobotInstance) _pc).사냥봇_위치.equalsIgnoreCase("상아탑4층")
					|| ((L1RobotInstance) _pc).사냥봇_위치.equalsIgnoreCase("상아탑5층")) {
				if (_pc.getCurrentWeapon() == 46 // 단검
						|| _pc.getCurrentWeapon() == 20
						|| _pc.getCurrentWeapon() == 24) {// 활
					return _random.nextInt(35) + 15;
				} else {
					return _random.nextInt(45) + 20;
				}
			} else {
				if (_pc.getCurrentWeapon() == 46 // 단검
						|| _pc.getCurrentWeapon() == 20
						|| _pc.getCurrentWeapon() == 24) {// 활
					return _random.nextInt(30) + 10;
				} else {
					return _random.nextInt(40) + 15;
				}
			}
		}
		int weaponMaxDamage = 0;
		boolean secondw = false;

		int doubleChance = _random.nextInt(100) + 1;

		if (_targetNpc.getNpcTemplate().get_size().equalsIgnoreCase("small")
				&& _weaponSmall > 0) {
			weaponMaxDamage = _weaponSmall + _weaponAddDmg;
		} else if (_targetNpc.getNpcTemplate().get_size()
				.equalsIgnoreCase("large")
				&& _weaponLarge > 0) {
			weaponMaxDamage = _weaponLarge + _weaponAddDmg;
		} else {
			weaponMaxDamage = _weaponSmall + _weaponAddDmg;
		}

		if (_pc.isWarrior() && _pc.isSlayer && _pc.getSecondWeapon() != null) {
			int ran = _random.nextInt(100);
			if (ran < 50) {
				secondw = true;
				if (_targetNpc.getNpcTemplate().get_size()
						.equalsIgnoreCase("small")
						&& _SweaponSmall > 0) {
					weaponMaxDamage = _SweaponSmall + _SweaponAddDmg;
				} else if (_targetNpc.getNpcTemplate().get_size()
						.equalsIgnoreCase("large")
						&& _SweaponLarge > 0) {
					weaponMaxDamage = _SweaponLarge + _SweaponAddDmg;
				} else {
					weaponMaxDamage = _SweaponSmall + _SweaponAddDmg;
				}
			}
		}

		if (_weaponId == 90083 || _weaponId == 84 || _weaponId == 154) { // 포효의이도류
			_weaponDoubleDmgChance += _weaponEnchant + 1;
		}

		int weaponDamage = 0;
			if (_weaponType == 58 && doubleChance <= _weaponDoubleDmgChance) { // 위기 히트
				weaponDamage = weaponMaxDamage;
				_크리티컬 = true;
			} else if (_weaponType == 0) { // 맨손
				weaponDamage = 0;
			} else {
				weaponDamage = _random.nextInt(weaponMaxDamage + 1) + 1;
			}

		if (weaponDamage > weaponMaxDamage) {
			_크리티컬 = true;
		}
		
		if (_weaponId == 189 || _weaponId == 30220 || _weaponId == 293 || _weaponId == 9100){ // 관통효과
			if(_targetNpc.getMaxHp() >= 10000){
				// 본섭과 다르게 무기 최대데미지에 피시의 레벨만큼 랜덤폭 지정하여 데미지 추가
				weaponDamage = weaponMaxDamage + _random.nextInt(_pc.getLevel());
			}
		}
		// //
		int 치명 = _random.nextInt(100) + 1;
		int 치명확률 = 0;
		if (_weaponType == 20 || _weaponType == 62) {
			치명확률 = CalcStat.원거리치명타(_pc.getAbility().getTotalDex());
			치명확률 += _pc.get원거리치명률();
		} else {
			치명확률 = CalcStat.근거리치명타(_pc.getAbility().getTotalStr());
			치명확률 = _pc.get근거리치명률();
			if (SkillsTable.getInstance().spellCheck(_pc.getId(), L1SkillId.FINAL_BURN)){
				int 추가확률 = (_pc.getLevel() - 80) / 2;
				if (_pc.getCurrentHp() <= _pc.getMaxHp() * 0.3){
					if (_pc.getLevel() >= 80){
						치명확률 += (5 + 추가확률);
					} else {
						치명확률 += 5;
					}
				}
			}
		}

		if (치명 <= 치명확률) {
			_크리티컬 = true;
			// System.out.println("333333");
			weaponDamage = weaponMaxDamage;
		}
		
		if(_발라원거리가호 || _발라근거리가호){
			weaponDamage = weaponMaxDamage * 2;
			_발라근거리가호 = false;
			_발라원거리가호 = false;
		}
		
		if (_weaponType != 20 && _weaponType != 62) {
			if (_pc.getSkillEffectTimerSet().hasSkillEffect(SOUL_OF_FLAME)) {
				weaponDamage = weaponMaxDamage;
				_크리티컬 = false;
			}
		}

		int weaponTotalDamage = 0;

		if (secondw) {
			weaponTotalDamage = weaponDamage + _SweaponEnchant;
			weaponTotalDamage += calcSMaterialBlessDmg();
		} else {
			weaponTotalDamage = weaponDamage + _weaponEnchant;
			weaponTotalDamage += calcMaterialBlessDmg();
		}

		// 은축복 데미지 보너스

		if (_weaponType == 54 && doubleChance <= _weaponDoubleDmgChance) { // 더블 히트
			weaponTotalDamage *= 2;
			_attackType = 4;
		}
		if (_pc.isCrash) {
			int rnd = _random.nextInt(100);
			if (rnd < Config.WARRIOR_CRASH) {
				int crashdmg = _pc.getLevel() / 2;
				double purrydmg = crashdmg * 2;
				int plusdmg = crashdmg;
				int gfx = 12487;
				if (_pc.isPurry) {
					int rnd2 = _random.nextInt(100);
					if (rnd2 < Config.WARRIOR_FURY) {
						gfx = 12489;
						plusdmg = (int) purrydmg;
					}
				}
				weaponTotalDamage += plusdmg;
				_pc.sendPackets(new S_SkillSound(_target.getId(), gfx));// 12489
				Broadcaster.broadcastPacket(_target, new S_SkillSound(_target.getId(), gfx));
			}
		}
		
		if (secondw) {
			weaponTotalDamage += calcSAttrEnchantDmg();
		} else {
			weaponTotalDamage += calcAttrEnchantDmg();
		}

		if ((_weaponType == 54 || _weaponType == 58) && _pc.getSkillEffectTimerSet().hasSkillEffect(DOUBLE_BRAKE)) {
			if ((_random.nextInt(100) + 1) <= 33) {
				weaponTotalDamage *= 2;
				double_burning = true;
			}
		}

		double dmg = weaponTotalDamage + _statusDamage;

		if (_weaponType != 20 && _weaponType != 62) {
			dmg += _pc.getDmgup() + _pc.getDmgupByArmor();
		} else {
			dmg += _pc.getBowDmgup() + _pc.getBowDmgupByArmor() + _pc.getBowDmgupByDoll();
		}

		if (_weaponType == 20) { // 활
			if (_arrow != null) {
				int add_dmg = 0;
				 if (_arrow.getItemId() == 8600){
					 add_dmg = 1;
				 } else if (_arrow.getItemId() == 8601){
					 add_dmg = 3;
				 } else if (_arrow.getItemId() == 8602){
					 add_dmg = 3;
						if (_weaponAttr != 0)
							 add_dmg += addattrdmg(_weaponAttr);
				 }
				dmg += add_dmg;
			} else if (_weaponId == 190 || _weaponId == 100190  || _weaponId == 9100 || (_weaponId >= 11011 && _weaponId <= 11013)) { // 사이하의 활
				dmg += 2;
			}
		} else if (_weaponType == 62) { // 암 토토 렛
			int add_dmg = 0;
			 if (_arrow.getItemId() == 40739){
				 add_dmg = 1;
			 } else if (_arrow.getItemId() == 40738){
				 add_dmg = 3;
			 } else if (_arrow.getItemId() == 40740){
				 add_dmg = 3;
			 }
			dmg += add_dmg;
		}

		dmg = calcBuffDamage(dmg);
		
		/** 포커스 웨이브 **/
		포커스웨이브(_pc, _targetNpc);
		
		if (_weaponType == 0) { // 맨손
			dmg = (_random.nextInt(5) + 4) / 4;
		} else {
			if (_weaponType1 == 17) { // 키링크
				//int 키링크대미지인트 = _pc.getAbility().getTotalInt();
				int 키링크대미지스펠 = _pc.getAbility().getSp();
				int 키링크최대 = 키링크대미지스펠 * 2;
				int 키링크랜덤 = (_random.nextInt(키링크대미지스펠)+1)* 2;
				dmg += 키링크랜덤;
				dmg = calcMrDefense(_target.getResistance().getEffectedMrBySkill(), dmg);
				if (키링크랜덤 >= 키링크최대) {
					_크리티컬 = true;
				}
			} else if (_weaponType1 == 18) {
				dmg += WeaponSkill.getChainSwordDamage(_pc, _target, _weaponId);
			}

			if (secondw) {
				switch (_SweaponId) {
				case 90086:
				case 90087:
				case 90088:
				case 90090:
				case 90091:
				case 90092:
					WeaponSkill.블레이즈쇼크(_pc, _target, _weaponEnchant);
					break;
				case 7236:
					WeaponSkill.getDeathKnightjin(_pc, _target);
					break;
				case 13:
				case 44:
					WeaponSkill.getPoisonSword(_pc, _target);
					break;
				case 100047:
				case 47:
					WeaponSkill.getSilenceSword(_pc, _target, _weaponEnchant);
					break;
				case 134:
				case 90085:
					dmg += WeaponSkill.get수결지Damage(_pc, _target,
							_weaponEnchant);
					break;

				case 54:
					dmg += WeaponSkill.getKurtSwordDamage(_pc, _target,
							_weaponEnchant);
					break;
				case 58:
					dmg += WeaponSkill.getDeathKnightSwordDamage(_pc, _target,
							_weaponEnchant);
					break;
					// case 59: dmg += WeaponSkill.getBarlogSwordDamage(_pc,
					// _target, _weaponEnchant); break;
				case 76:
					dmg += WeaponSkill.getRondeDamage(_pc, _target,
							_weaponEnchant);
					break;
				case 121:
					dmg += WeaponSkill.getIceQueenStaffDamage(_pc, _target,
							_weaponEnchant);
					break;
				case 124:
				case 100124:
					dmg += WeaponSkill.getBaphometStaffDamage(_pc, _target,
							_weaponEnchant);
					break;
				case 9080:
				case 191:// 살천의활
					// WeaponSkill.giveSalCheonEffect(_pc, _target);
					// DrainofEvil(); break;
					dmg += WeaponSkill.getEffectDamage(_pc, _target,
							_weaponEnchant, 9361);
					break;
					// case 114: WeaponSkill.getGunjuSword(_pc,
					// _target,_weaponEnchant); break;
				case 203:
					dmg += WeaponSkill.getBarlogSwordDamage(_pc, _target);
					break;
		//		case 86: // 붉이
				case 204:

				case 100204:
					WeaponSkill.giveFettersEffect(_pc, _target);
					break;
				case 205:
				case 100205:
					dmg += WeaponSkill.getMoonBowDamage(_pc, _target,
							_weaponEnchant);
					break;
				case 264:
				case 265:
				case 256:
				case 4500027:
					dmg += WeaponSkill
					.getEffectSwordDamage(_pc, _target, 11107);
					break;
				case 9102:
				case 90089:
					dmg += WeaponSkill.getDarkMDamage(_pc, _target, _weaponEnchant);
					break;
				case 9079:
				case 412000: // dmg += WeaponSkill.getEffectSwordDamage(_pc,
					// _target, 10); break;
					dmg += WeaponSkill.getEffectDamage(_pc, _target,
							_weaponEnchant, 3940);
					break;
					/*
					 * case 410000: case 410001: case 450009: case 450014: case
					 * 450004: dmg += WeaponSkill.getChainSwordDamage(_pc, _target);
					 * break;
					 */
				case 412004: // dmg += WeaponSkill.getIceSpearDamage(_pc,
					// _target); break;
					dmg += WeaponSkill.getEffectDamage(_pc, _target,
							_weaponEnchant, 3704);
					break;
				case 7228:
				case 9075:
				case 412005: // dmg += WeaponSkill.geTornadoAxeDamage(_pc,
					// _target); break;
					dmg += WeaponSkill.getEffectDamage(_pc, _target,
							_weaponEnchant, 5524);
					break;
					// case 412003: WeaponSkill.getDiseaseWeapon(_pc, _target,
					// 412003); break;
				case 413101:
				case 413102:
				case 413104:
				case 413105:
					WeaponSkill.getDiseaseWeapon(_pc, _target, 413101);
					break;
				case 413103:
					calcStaffOfMana();
					WeaponSkill.getDiseaseWeapon(_pc, _target, 413101);
					break;
				case 263:
				case 4500028:
				case 4500026:
					dmg += WeaponSkill.halloweenCus2(_pc, _target);
					break;// 각궁
					// case 263:
					// case 4500028: dmg += WeaponSkill.halloweenCus(_pc, _target);
					// break;//호지
				case 415010:
				case 415011:
				case 415012:
				case 415013:
					WeaponSkill.체이서(_pc, _target, _weaponEnchant, 0);
					break;
				case 415015:
				case 415016:
					WeaponSkill.체이서(_pc, _target, _weaponEnchant, 1);
					break;
				case 6000:
					dmg += WeaponSkill.IceChainSword(_pc, _target,
							_weaponEnchant);
					break;
				case 6001:
					dmg += WeaponSkill.Icekiring(_pc, _target, _weaponEnchant);
					break;
				case 6101:
					dmg += WeaponSkill.소울커터(_pc, _target, _weaponEnchant);
					break;
				case 7:
				case 35:
				case 48:
				case 73:
				case 105:
				case 120:
				case 147:
				case 156:
				case 174:
				case 175:
				case 224:
				case 7232:
					dmg += WeaponSkill.플라즈마(_pc, _target, _weaponEnchant);
					break;
				case 450008:
				case 450010:
					WeaponSkill.이블리버스(_pc, _target, _weaponEnchant);
					break;
				case 450011:
				case 450012:
				case 450013:
					WeaponSkill.이블트릭(_pc, _target, _weaponEnchant);
					break;
				case 450022:
				case 450024:
					WeaponSkill.이블리버스(_pc, _target, _weaponEnchant, 8981);
					break;
				case 450023:
				case 450025:
					WeaponSkill.이블트릭(_pc, _target, _weaponEnchant, 8981);
					break;
				case 100259:
				case 100260:
				case 9081:
				case 259:
				case 260:
					dmg += WeaponSkill.getEffectDamage(_pc, _target,
							_weaponEnchant, 9359);
					break; // 파괴의 크로우, 이도류
				case 9077:
				case 266:
				case 100266:
					dmg += WeaponSkill.PhantomShock(_pc, _target, _weaponEnchant);
					break; // 공명의 키링크
				case 9076:
				case 275:
				case 100275:
					dmg += WeaponSkill.getEffectDamage(_pc, _target,
							_weaponEnchant, 7398);
					break; // 환영의 체인소드
				case 277:
				case 278:
				case 279:
				case 280:
				case 281:
				case 282:
				case 283:
					WeaponSkill.이블리버스(_pc, _target, _weaponEnchant);
					break; // 붉은 사자 무기
				case 284:
				case 285:
				case 286:
				case 287:
				case 288:
				case 289:
				case 290:
					dmg += WeaponSkill.getEffectDamage(_pc, _target,
							_weaponEnchant, 3939);
					break; // 환상의 무기 (10검 이벤트)
				case 291: // 제로스의지팡이
					dmg += WeaponSkill.getZerosDamage(_pc, _target,
							_weaponEnchant);
					break;
				case 9106:
					dmg += WeaponSkill.getJinSSaulDamage(_pc, _target, _weaponEnchant);
					break;
				case 293:
					dmg += WeaponSkill.getAkmongDamage(_pc, _target, _weaponEnchant);
					break;
				case 66:
					dmg += WeaponSkill.getDSDamage(_pc, _target, _weaponEnchant);
					break;
				case 90084:
					dmg += WeaponSkill.getSummyulDamage(_pc, _target, _weaponEnchant);
					break;	
				default:
					break;
				}
			} else {
				switch (_weaponId) {

				case 90086:
				case 90087:
				case 90088:
				case 90090:
				case 90091:
				case 90092:
					WeaponSkill.블레이즈쇼크(_pc, _target, _weaponEnchant);
					break;

				case 7236:
					WeaponSkill.getDeathKnightjin(_pc, _target);
					break;
				case 13:
				case 44:
					WeaponSkill.getPoisonSword(_pc, _target);
					break;
				case 100047:
				case 47:
					WeaponSkill.getSilenceSword(_pc, _target, _weaponEnchant);
					break;
				case 134:
				case 90085:
					dmg += WeaponSkill.get수결지Damage(_pc, _target,
							_weaponEnchant);
					break;

				case 54:
					dmg += WeaponSkill.getKurtSwordDamage(_pc, _target,
							_weaponEnchant);
					break;
				case 58:
					dmg += WeaponSkill.getDeathKnightSwordDamage(_pc, _target,
							_weaponEnchant);
					break;
					// case 59: dmg += WeaponSkill.getBarlogSwordDamage(_pc,
					// _target, _weaponEnchant); break;
				case 76:
					dmg += WeaponSkill.getRondeDamage(_pc, _target,
							_weaponEnchant);
					break;
				case 121:
					dmg += WeaponSkill.getIceQueenStaffDamage(_pc, _target,
							_weaponEnchant);
					break;
				case 124:
				case 100124:
					dmg += WeaponSkill.getBaphometStaffDamage(_pc, _target,
							_weaponEnchant);
					break;
				case 9080:
				case 191:// 살천의활
					// WeaponSkill.giveSalCheonEffect(_pc, _target);
					// DrainofEvil(); break;
					dmg += WeaponSkill.getEffectDamage(_pc, _target,
							_weaponEnchant, 9361);
					break;
					// case 114: WeaponSkill.getGunjuSword(_pc,
					// _target,_weaponEnchant); break;
				case 203:
					dmg += WeaponSkill.getBarlogSwordDamage(_pc, _target);
					break;
		//		case 86: // 붉이
				case 204:

				case 100204:
					WeaponSkill.giveFettersEffect(_pc, _target);
					break;
				case 205:
				case 100205:
					dmg += WeaponSkill.getMoonBowDamage(_pc, _target,
							_weaponEnchant);
					break;
				case 264:
				case 265:
				case 256:
				case 4500027:
					dmg += WeaponSkill
					.getEffectSwordDamage(_pc, _target, 11107);
					break;
				case 9102:
				case 90089:
					dmg += WeaponSkill.getDarkMDamage(_pc, _target, _weaponEnchant);
					break;
				case 9079:
				case 412000: // dmg += WeaponSkill.getEffectSwordDamage(_pc,
					// _target, 10); break;
					dmg += WeaponSkill.getEffectDamage(_pc, _target,
							_weaponEnchant, 3940);
					break;
					/*
					 * case 410000: case 410001: case 450009: case 450014: case
					 * 450004: dmg += WeaponSkill.getChainSwordDamage(_pc, _target);
					 * break;
					 */
				case 412004: // dmg += WeaponSkill.getIceSpearDamage(_pc,
					// _target); break;
					dmg += WeaponSkill.getEffectDamage(_pc, _target,
							_weaponEnchant, 3704);
					break;
				case 7228:
				case 9075:
				case 412005: // dmg += WeaponSkill.geTornadoAxeDamage(_pc,
					// _target); break;
					dmg += WeaponSkill.getEffectDamage(_pc, _target,
							_weaponEnchant, 5524);
					break;
					// case 412003: WeaponSkill.getDiseaseWeapon(_pc, _target,
					// 412003); break;
				case 413101:
				case 413102:
				case 413104:
				case 413105:
					WeaponSkill.getDiseaseWeapon(_pc, _target, 413101);
					break;
				case 413103:
					calcStaffOfMana();
					WeaponSkill.getDiseaseWeapon(_pc, _target, 413101);
					break;
				case 263:
				case 4500028:
				case 4500026:
					dmg += WeaponSkill.halloweenCus2(_pc, _target);
					break;// 각궁
					// case 263:
					// case 4500028: dmg += WeaponSkill.halloweenCus(_pc, _target);
					// break;//호지
				case 415010:
				case 415011:
				case 415012:
				case 415013:
					WeaponSkill.체이서(_pc, _target, _weaponEnchant, 0);
					break;
				case 415015:
				case 415016:
					WeaponSkill.체이서(_pc, _target, _weaponEnchant, 1);
					break;
				case 6000:
					dmg += WeaponSkill.IceChainSword(_pc, _target,
							_weaponEnchant);
					break;
				case 6001:
					dmg += WeaponSkill.Icekiring(_pc, _target, _weaponEnchant);
					break;
				case 6101:
					dmg += WeaponSkill.소울커터(_pc, _target, _weaponEnchant);
					break;
				case 7:
				case 35:
				case 48:
				case 73:
				case 105:
				case 120:
				case 147:
				case 156:
				case 174:
				case 175:
				case 224:
				case 7232:
					dmg += WeaponSkill.플라즈마(_pc, _target, _weaponEnchant);
					break;
				case 450008:
				case 450010:
					WeaponSkill.이블리버스(_pc, _target, _weaponEnchant);
					break;
				case 450011:
				case 450012:
				case 450013:
					WeaponSkill.이블트릭(_pc, _target, _weaponEnchant);
					break;
				case 450022:
				case 450024:
					WeaponSkill.이블리버스(_pc, _target, _weaponEnchant, 8981);
					break;
				case 450023:
				case 450025:
					WeaponSkill.이블트릭(_pc, _target, _weaponEnchant, 8981);
					break;
				case 100259:
				case 100260:
				case 9081:
				case 259:
				case 260:
					dmg += WeaponSkill.getEffectDamage(_pc, _target,
							_weaponEnchant, 9359);
					break; // 파괴의 크로우, 이도류
				case 9077:
				case 266:
				case 100266:
					dmg += WeaponSkill.PhantomShock(_pc, _target,
							_weaponEnchant);
					break; // 공명의 키링크
				case 9076:
				case 275:
				case 100275:
					dmg += WeaponSkill.getEffectDamage(_pc, _target,
							_weaponEnchant, 7398);
					break; // 환영의 체인소드
				case 277:
				case 278:
				case 279:
				case 280:
				case 281:
				case 282:
				case 283:
					WeaponSkill.이블리버스(_pc, _target, _weaponEnchant);
					break; // 붉은 사자 무기
				case 284:
				case 285:
				case 286:
				case 287:
				case 288:
				case 289:
				case 290:
					dmg += WeaponSkill.getEffectDamage(_pc, _target,
							_weaponEnchant, 3939);
					break; // 환상의 무기 (10검 이벤트)
				case 291: // 제로스의지팡이
					dmg += WeaponSkill.getZerosDamage(_pc, _target,
							_weaponEnchant);
					break;
				case 9106:
					dmg += WeaponSkill.getJinSSaulDamage(_pc, _target, _weaponEnchant);
					break;
				case 293:
					dmg += WeaponSkill.getAkmongDamage(_pc, _target, _weaponEnchant);
					break;
				case 66:
					dmg += WeaponSkill.getDSDamage(_pc, _target, _weaponEnchant);
					break;
				case 90084:
					dmg += WeaponSkill.getSummyulDamage(_pc, _target, _weaponEnchant);
					break;	
				default:
					break;
				}
			}
			if (_weaponId == 450009) {
				dmg += WeaponSkill.getChainSwordDamage(_pc, _target, _weaponId);
				WeaponSkill.이블리버스(_pc, _target, _weaponEnchant);
			}
			
			if (secondw) {
				if(_SweaponId == 7227){
					if(_SweaponEnchant > 9)
					dmg += WeaponSkill.getTaepoongDamage(_pc, _target, _SweaponEnchant);
				}
				if (_SweaponId == 7233) {
					WeaponSkill.이블리버스(_pc, _target, _SweaponEnchant);
				}
			} else {
				if(_weaponId == 7227){
					if(_weaponEnchant > 9)
					dmg += WeaponSkill.getTaepoongDamage(_pc, _target, _weaponEnchant);
				}
				if (_weaponId == 7233) {
					WeaponSkill.이블리버스(_pc, _target, _weaponEnchant);
				}
				////몸빵조정?
				if((_targetNpc.getMapId() == 15403)){  //지배의결계 1층
					dmg *= Config.지배1층1;
				}	
				if((_targetNpc.getMapId() == 15404)){  //지배의결계 2층
					dmg *= Config.지배2층1;
				}	
				if((_targetNpc.getMapId() == 15440)){  //화룡의둥지
					dmg *= Config.신규화둥1;
				}	
				if((_targetNpc.getMapId() == 15410)){  //풍룡의둥지
					dmg *= Config.신규풍둥1;
				}	
				if((_targetNpc.getMapId() == 15420)){  //오렌설벽
					dmg *= Config.오렌설벽1;
				}	
				if((_targetNpc.getMapId() == 15430)){  //용의계곡
					dmg *= Config.신규용계1;
				}
					if(_targetNpc.getMapId() >= 101 && _targetNpc.getMapId() <= 106){ //0.65
						//	dmg *= 0.45;
							dmg *= Config.오만몸빵16층;
						}
						//7층
						if(_targetNpc.getMapId() == 107){ //0.45
						//	dmg *= 0.45;
							dmg *= Config.오만몸빵7층;
						}
						//8층
						if(_targetNpc.getMapId() == 108){ //0.45
						//	dmg *= 0.45;
							dmg *= Config.오만몸빵8층;
						}
						//9층
						if(_targetNpc.getMapId() == 109){ //0.45
						//	dmg *= 0.45;
							dmg *= Config.오만몸빵9층;
						}	
						//10층
						if(_targetNpc.getMapId() == 110){ //0.45
						//	dmg *= 0.45;
							dmg *= Config.오만몸빵10층;
						}
						//정상
						if(_targetNpc.getMapId() == 111){ //0.45
						//	dmg *= 0.45;
							dmg *= Config.오만몸빵정상;
						}
						if(_targetNpc.getMapId() >= 12852 && _targetNpc.getMapId() <= 12857){ 
								dmg *= Config.지배오만몸빵16층;
							}
							//7층
							if(_targetNpc.getMapId() == 12858){ //0.45
							//	dmg *= 0.45;
								dmg *= Config.지배오만몸빵7층;
							}
							//8층
							if(_targetNpc.getMapId() == 12859){ //0.45
							//	dmg *= 0.45;
								dmg *= Config.지배오만몸빵8층;
							}
							//9층
							if(_targetNpc.getMapId() == 12860){ //0.45
							//	dmg *= 0.45;
								dmg *= Config.지배오만몸빵9층;
							}	
							//10층
							if(_targetNpc.getMapId() == 12861){ //0.45
							//	dmg *= 0.45;
								dmg *= Config.지배오만몸빵10층;
							}
							
							if(_targetNpc.getMapId() == 12862){ //0.45
							//	dmg *= 0.45;
								dmg *= Config.지배정상1;
							}
							
							if(_targetNpc.getMapId() == 2004){ //0.45
							//	dmg *= 0.45;
								dmg *= Config.고라스1;
							}
							if(_targetNpc.getMapId() == 605){ //0.45
							//	dmg *= 0.45;
								dmg *= Config.뜨거운1;
							}
							
			
		}
	
			}
		
		
		/*L1ItemInstance 태풍의도끼 = _pc.getInventory().checkEquippedItem(7227);
		
		if (태풍의도끼 != null) {
			if(태풍의도끼.getEnchantLevel() >= 10){
				dmg += WeaponSkill.getTaepoongDamage(_pc, _target, 태풍의도끼.getEnchantLevel());
			}
		}*/
		L1ItemInstance 발라카스의완력 = _pc.getInventory().checkEquippedItem(420112);
		L1ItemInstance 발라카스의예지력 = _pc.getInventory().checkEquippedItem(420113);
		L1ItemInstance 발라카스의인내력 = _pc.getInventory().checkEquippedItem(420114);
		L1ItemInstance 발라카스의마력 = _pc.getInventory().checkEquippedItem(420115);
		
		if (발라카스의완력 != null) {
			int 발동찬스 = _random.nextInt(100);
			if(발동찬스 < 발라카스의완력.getEnchantLevel()){
				if (_weaponType != 20 && _weaponType != 62) {
					_발라근거리가호 = true;
				}
			}
		}
		if (발라카스의예지력 != null) {
			int 발동찬스 = _random.nextInt(100);
			if(발동찬스 < 발라카스의예지력.getEnchantLevel()){
				if (_weaponType != 20 && _weaponType != 62) {
					_발라근거리가호 = true;
				}
			}
		}
		if (발라카스의인내력 != null) {
			int 발동찬스 = _random.nextInt(100);
			if(발동찬스 < 발라카스의인내력.getEnchantLevel()){
				if (_weaponType == 20 || _weaponType == 62) {
					_발라원거리가호 = true;
				}
			}
		}
		if (발라카스의마력 != null) {
			int 발동찬스 = _random.nextInt(100);
			if(발동찬스 < 발라카스의마력.getEnchantLevel()){
				if (_weaponType1 == 17) {
					_발라근거리가호 = true;
				}
			}
		}
		if (_weaponId == 9078 || _weaponId == 276 || _weaponId == 100276 || // 붉은기사의대검
				_weaponId == 292 || _weaponId == 293) {// 진노의크로우, 악몽이장궁
			if (_pc.getLawful() <= -20000)
				dmg += 5;
			else if (_pc.getLawful() <= -10000)
				dmg += 3;
			else if (_pc.getLawful() <= 0)
				dmg += 1;
		}

		if (_weaponId >= 90085 && _weaponId <= 90092) {
			dmg += _weaponEnchant;
		}

		if (_pc.getDollList().size() > 0) {
			for (L1DollInstance doll : _pc.getDollList()) {
				if (_weaponType != 20 && _weaponType != 62) {
					int d = doll.getDamageByDoll(_targetNpc);
					if (d > 0
							&& doll.getDollType() == L1DollInstance.DOLLTYPE_그레그)
						_pc.setCurrentHp(_pc.getCurrentHp()
								+ _random.nextInt(5) + 1);
					dmg += d;
				}

				if (doll.getDollType() == L1DollInstance.DOLLTYPE_흑장로
						|| doll.getDollType() == L1DollInstance.DOLLTYPE_데스나이트
						|| doll.getDollType() == L1DollInstance.DOLLTYPE_축데스나이트
						|| doll.getDollType() == L1DollInstance.DOLLTYPE_축흑장로
						) {
					dmg += doll.getMagicDamageByDoll(_targetNpc);
				}

				doll.attackPoisonDamage(_pc, _targetNpc);
			}
		}

		if (adddmg != 0)
			dmg += adddmg;

		// 축무기 데미지 추가
		
		 if(weapon != null){ if(weapon.getBless() == 0){ dmg += 1; } }
		// 축무기 데미지 추가
		 if(Sweapon != null){ if(Sweapon.getBless() == 0){ dmg += 1; } }
		 
		if (_targetNpc.getSkillEffectTimerSet().hasSkillEffect(ARMOR_BREAK)) {
			if (_weaponType != 20 && _weaponType != 62) {
			dmg *= 1.58;
			}
		}


		if (_pc.getSkillEffectTimerSet().hasSkillEffect(BURNING_SLASH)) {
			dmg += 50;
			_pc.sendPackets(new S_SkillSound(_targetNpc.getId(), 6591), true);
			Broadcaster.broadcastPacket(_pc,
					new S_SkillSound(_targetNpc.getId(), 6591), true);
			_pc.getSkillEffectTimerSet().killSkillEffectTimer(BURNING_SLASH);
		}
		

		dmg += 룸티스검귀추가데미지();

		/*
		 * if(_weaponType1 != 17)//키링크가 아닐때
		 */
		dmg -= calcNpcDamageReduction();

		if (_targetNpc.getNpcId() == 45640) {
			dmg /= 2;
		}
		// 플레이어로부터 애완동물, 사몬에 공격
		boolean isNowWar = false;
		int castleId = L1CastleLocation.getCastleIdByArea(_targetNpc);
		if (castleId > 0) {
			isNowWar = WarTimeController.getInstance().isNowWar(castleId);
		}
		if (!isNowWar) {
			if (_targetNpc instanceof L1PetInstance) {
				dmg /= 8;
			}
			if (_targetNpc instanceof L1SummonInstance){
				L1SummonInstance summon = (L1SummonInstance) _targetNpc;
				if (summon.isExsistMaster()){
					dmg /= 2.0D;
					if(_targetNpc.getNpcId() >= 90096 && _targetNpc.getNpcId() >= 90101){
						dmg *= 0.90;
					}
				}		
			}
		}

		// 언데드 밤 데미지 감소
		if (isUndeadDamage(_targetNpc)) {
			dmg -= dmg * 0.15;
		}

		if (_weaponId == 7 || _weaponId == 35 || _weaponId == 48
				|| _weaponId == 73 || _weaponId == 105 || _weaponId == 120
				|| _weaponId == 147 || _weaponId == 156 || _weaponId == 174
				|| _weaponId == 175 || _weaponId == 224 || _weaponId == 7232) {
			if (Config.수련자무기밸런스수치 != 0) {
				double balance = Config.수련자무기밸런스수치 * 0.01;
				dmg -= dmg * balance;// 수련자 무기 35%하향
			}
		}

		if (_weaponAttr != 0)
			dmg += addattrdmg(_weaponAttr);

		if (dmg <= 0) {
			_isHit = false;
			_drainHp = 0;
		}
		// 피흡수 공식 바꾸기위해 옮김.
		if (_weaponId == 262) { // 블러드서커
			if (dmg >= 30 && _random.nextInt(100) <= 90) {
				// _pc.sendPackets(new
				// S_SystemMessage("피흡량 :"+_drainHp+" 데미지 :"+dmg));
				int _dhp = (int) ((dmg - 10) / 10);
				_drainHp += _dhp;// +_random.nextInt(3)+1;
			}
		}
		 if(_pc.isKnight()){
			 dmg *= Config.KNIGHT_TO_NPC;
		 } else if(_pc.isElf()){
			 dmg *= Config.ELF_TO_NPC;
		 } else if(_pc.isDarkelf()){
			 dmg *= Config.DARKELF_TO_NPC;
		 } else if(_pc.isCrown()){
			 dmg *= Config.CROWN_TO_NPC;
		 } else if(_pc.isDragonknight()){
			 dmg *= Config.DRAGONKNIGHT_TO_NPC;
		 } else if(_pc.isWizard()){
			 dmg *= Config.WIZARD_TO_NPC;
		 } else if(_pc.isIllusionist()){
			 dmg *= Config.ILLUSIONIST_TO_NPC;
		 } else if(_pc.isWarrior()){
			 dmg *= Config.WARRIOR_TO_NPC;
		 }
		if (dmg > 0 && _targetNpc instanceof L1PetInstance){
			dmg = 1;
		}
		if (dmg > 0 && _targetNpc instanceof L1SummonInstance){
			dmg *= 2;
		}
		return (int) dmg;
	}

	// ●●●● NPC 로부터 플레이어에의 데미지 산출 ●●●●
	private int calcNpcPcDamage() {
		int lvl = _npc.getLevel();
		double dmg = 0D;

		
		if (_npc instanceof L1PetInstance) { 
			L1PetInstance Pet = (L1PetInstance)_npc;
			int DmgTamp = 10;
			if(Pet.getLevel() > 10){
				DmgTamp += _random.nextInt(Pet.getLevel() / 3);
			}else DmgTamp += Pet.getLevel();
			DmgTamp += Pet.getHunt() + (Pet.getElixirHunt() * 5);
			int Bonus = Pet.getHunt() + Pet.getElixirHunt();
			if(Bonus >= 20) DmgTamp += (Bonus -  10) / 10;
			DmgTamp += Pet.SkillsTable(L1PetInstance.칠흑의발톱) + Pet.SkillsTable(L1PetInstance.도살의발톱);
			dmg = DmgTamp;
		}

		
		if(_npc instanceof L1PetInstance){
			L1PetInstance Pet = (L1PetInstance)_npc;
			
			int Random = _random.nextInt(100) + 1;
			int 치명확률 = 5;
			if (치명확률 >= Random) {
				dmg *= 1.25;
				_크리티컬 = true;
			}
			
			
			Random = _random.nextInt(100) + 1;
			int[] 속성정보 = Pet.AttributeDmg();
			if (속성정보[0] >= Random) {
				double calcAttr = calcAttrResistance(Pet.getNpcTemplate().get_weakAttr());
				dmg += (int)(1.0 - calcAttr) * 속성정보[1];
				_속성공격임팩트 = 속성정보[2];
				_속성공격 = true;
			}
			속성정보 = null;
			
			
			Random = _random.nextInt(100) + 1;
			int[] 속성치명정보 = Pet.AttributeCriticalDmg();
			if (속성치명정보 != null && 속성치명정보[0] >= Random) {
				double calcAttr = calcAttrResistance(Pet.getNpcTemplate().get_weakAttr());
				dmg += (int)(1.0 - calcAttr) * 속성치명정보[1];
				_속성치명임팩트 = 속성치명정보[2];
				_속성치명 = true;
			}
			속성치명정보 = null;
			
			
			
			int[] 흡혈 = Pet.SkillsBlood();
			if(흡혈[0] != 0){
				Random = _random.nextInt(100) + 1;
				if(흡혈[0] >= Random){
					dmg += 흡혈[1];
					Pet.setCurrentHp(Pet.getCurrentHp() + 흡혈[1]);
					L1PcInstance PetMaster = (L1PcInstance) Pet.getMaster();
					PetMaster.sendPackets(new S_PetWindow(S_PetWindow.PatStatUpDate, Pet), true);
				}
			}
			흡혈 = null;

			
			int DmgReduction = Pet.SkillsTable(L1PetInstance.백상아리이빨);
			if(DmgReduction != 0){
				int DmgHit = calcNpcDamageReduction() - DmgReduction;
				if(DmgHit < 0){
					dmg += calcNpcDamageReduction();
				}else{
					dmg += DmgReduction - DmgHit;
				}
			}
			
			
			if(Pet.SkillCheck(L1SkillId.Fighting)){
				if(Pet.getFightingTarget() == null) Pet.setFightingTarget(_targetNpc);
				if(Pet.getFightingTarget() != _targetNpc){
					Pet.setFightingCombo(0);
					Pet.setFightingTarget(_targetNpc);
				}
				switch(Pet.getFightingCombo()) {
					case 0:
						dmg *= 1.2;
						Pet.setFightingCombo(1);
						break;
						
					case 1:
						dmg *= 1.5;
						Pet.setFightingCombo(2);
						break;
						
					case 2:
						dmg *= 2;
						Pet.setFightingCombo(0);
						Broadcaster.broadcastPacket(Pet, new S_SkillSound(Pet.getId(), 17326), true);
						break;
				}
			}
			
			int DmgCombo = Pet.SkillsTable(L1PetInstance.포식자);
			if(DmgCombo != 0) DmgCombo = DmgCombo / 100; 
			switch(Pet.getFightingCombo()) {
				case 0:		
					dmg *= 1.2 + DmgCombo;
					Pet.setFightingCombo(1);
					break;
					
				case 1:
					dmg *= 1.5 + DmgCombo;
					Pet.setFightingCombo(2);
					break;
					
				case 2:
					dmg *= 2 + DmgCombo;
					Pet.setFightingCombo(0);
					Broadcaster.broadcastPacket(Pet, new S_SkillSound(Pet.getId(), 17326), true);
					break;
			}
			
		
		} else if (_targetNpc instanceof L1PetInstance) {
			L1PetInstance Pet = (L1PetInstance)_targetNpc;
			dmg -= Pet.SkillsTable(L1PetInstance.철갑등껍질);
			dmg /= 2;
		
		} else {
			if (lvl < 10) // 몹렙이 10미만
				dmg = _random.nextInt(lvl) + 10D
				+ _npc.getAbility().getTotalStr() + 2;
			else if (lvl >= 10 && lvl < 20) // 몹렙이 10 ~ 49
				dmg = _random.nextInt(lvl) + _npc.getAbility().getTotalStr()
				+ 2;
			else if (lvl >= 20 && lvl < 30) // 몹렙이 50 ~ 69
				dmg = _random.nextInt(lvl) + _npc.getAbility().getTotalStr()
				+ 4;
			else if (lvl >= 30 && lvl < 40) // 몹렙이 50 ~ 69
				dmg = _random.nextInt(lvl) + _npc.getAbility().getTotalStr()
				+ 8;
			else if (lvl >= 40 && lvl < 50) // 몹렙이 50 ~ 69
				dmg = _random.nextInt(lvl) + _npc.getAbility().getTotalStr()
				+ 12;
			else if (lvl >= 50 && lvl < 60) // 몹렙이 70 ~ 79
				dmg = _random.nextInt(lvl) + _npc.getAbility().getTotalStr()
				+ 16;
			else if (lvl >= 60 && lvl < 70) // 몹렙이 80 ~ 86
				dmg = _random.nextInt(lvl) + _npc.getAbility().getTotalStr()
				+ 20;
			else if (lvl >= 70 && lvl < 80) // 몹렙이 50 ~ 69
				dmg = _random.nextInt(lvl) + _npc.getAbility().getTotalStr()
				+ 30;
			else if (lvl >= 80 && lvl < 87) // 몹렙이 50 ~ 69
				dmg = _random.nextInt(lvl) + _npc.getAbility().getTotalStr()
				+ 40;
			else if (lvl >= 87) // 몹렙이 87 이상
				dmg = _random.nextInt(lvl) + _npc.getAbility().getTotalStr()
				* 2 + 100;
		}

		if((_npc.getMapId() == 15403)){  //지배의결계 1층
			dmg *= Config.지배1층;
		}	
		if((_npc.getMapId() == 15404)){  //지배의결계 2층
			dmg *= Config.지배2층;
		}	
		if((_npc.getMapId() == 15440)){  //화룡의둥지
			dmg *= Config.신규화둥;
		}	
		if((_npc.getMapId() == 15410)){  //풍룡의둥지
			dmg *= Config.신규풍둥;
		}	
		if((_npc.getMapId() == 15420)){  //오렌설벽
			dmg *= Config.오렌설벽;
		}	
		if((_npc.getMapId() == 15430)){  //용의계곡
			dmg *= Config.신규용계;
		}
			if(_npc.getMapId() == 101){  //오만 몹강하게 0.75
				dmg += dmg * Config.오만의탑1층;
			}		
			if(_npc.getMapId() == 102){  //오만2
				dmg += dmg* Config.오만의탑2층;
			}
			if(_npc.getMapId() == 103){  //오만3
				dmg += dmg* Config.오만의탑3층;
			}
			if(_npc.getMapId() == 104){  //오만4
				dmg += dmg* Config.오만의탑4층;
			}
			if(_npc.getMapId() == 105){  //오만5
				dmg += dmg* Config.오만의탑5층;
			}
			if(_npc.getMapId() == 106){  //오만6
				dmg += dmg* Config.오만의탑6층;
			}
			if(_npc.getMapId() == 107){  //오만7
				dmg += dmg* Config.오만의탑7층;
			}
			if(_npc.getMapId() == 108){  //오만8
				dmg += dmg* Config.오만의탑8층;
			}
			if(_npc.getMapId() == 109){  //오만9
				dmg += dmg* Config.오만의탑9층;
			}
			if(_npc.getMapId() == 110){  //오만10
				dmg += dmg* Config.오만의탑10층;
			}
				if((_npc.getMapId() == 111)){  //오만정상.
					dmg += dmg*Config.오만의탑정상;
			}
				if(_npc.getMapId() == 12852){  //오만 몹강하게 0.75
					dmg += dmg * Config.지배오만의탑1층;
				}		
				if(_npc.getMapId() == 12853){  //오만2
					dmg += dmg* Config.지배오만의탑2층;
				}
				if(_npc.getMapId() == 12854){  //오만3
					dmg += dmg* Config.지배오만의탑3층;
				}
				if(_npc.getMapId() == 12855){  //오만4
					dmg += dmg* Config.지배오만의탑4층;
				}
				if(_npc.getMapId() == 12856){  //오만5
					dmg += dmg* Config.지배오만의탑5층;
				}
				if(_npc.getMapId() == 12857){  //오만6
					dmg += dmg* Config.지배오만의탑6층;
				}
				if(_npc.getMapId() == 12858){  //오만7
					dmg += dmg* Config.지배오만의탑7층;
				}
				if(_npc.getMapId() == 12859){  //오만8
					dmg += dmg* Config.지배오만의탑8층;
				}
				if(_npc.getMapId() == 12860){  //오만9
					dmg += dmg* Config.지배오만의탑9층;
				}
				if(_npc.getMapId() == 12861){  //오만10
					dmg += dmg* Config.지배오만의탑10층;
				}
				if((_npc.getMapId() >= 53 && _npc.getMapId() <= 54)){  //기감 몹 약하게
						dmg *= Config.기감123;
				}	
				if((_npc.getMapId() == 56)){  //기감 몹 강하게
					dmg *= Config.기감4;
			}	
				if((_npc.getMapId() == 12862)){  //기감 몹 강하게
					dmg *= Config.지배정상;
			}	
				if((_npc.getMapId() == 2004)){  //기감 몹 강하게
					dmg *= Config.고라스;
			}	
				if((_npc.getMapId() == 605)){  //기감 몹 강하게
					dmg *= Config.뜨거운;
			}	
				if((_npc.getMapId() >= 19 && _npc.getMapId() <= 21)){  //요던몹강하게
					//	dmg += dmg*0.55;
						dmg *= Config.요던;
					}
				/**황혼의산맥(거인들)**/
				if(_npc.getId() == 45295 && _npc.getId() == 45337 && _npc.getId() == 45351){
					dmg += dmg * Config.거인;
				}
				if((_npc.getMapId() >= 807 && _npc.getMapId() <= 813)){  //본던
						dmg *= Config.본던;
					}	
				if((_npc.getMapId() >= 30 && _npc.getMapId() <= 37)){  //용던 몹 강하게 75
						dmg *= Config.용던;
					}	
				if((_npc.getMapId() >= 1700 && _npc.getMapId() <= 1710)){  //잊섬 몹강하게
						dmg *= Config.잊섬;
					}	
		
		if(this._npc.getNpcId() >= 90092 && this._npc.getNpcId() <= 90101){
			int HpRatio = 100 * (this._npc.getCurrentHp() / this._npc.getMaxHp());
			if(HpRatio < 10){
				dmg *= 1.5D;
			}else if(HpRatio < 20){
				dmg *= 1.4D;
			}else if(HpRatio < 30){
				dmg *= 1.3D;
			}else if(HpRatio < 40){
				dmg *= 1.2D;
			}else if(HpRatio < 60){
				dmg *= 1.1D;
			}
		}

		if (_target != null) {
			int chance1 = _random.nextInt(100);
			for (L1ItemInstance item : _targetPc.getInventory().getItems()) {
				if (item.isEquipped()) {
					if (item.getItemId() >= 420104
							&& item.getItemId() <= 420107) {
						int addchan = 5;/* item.getEnchantLevel()-5; */
						if (addchan < 0)
							addchan = 0;
						if (chance1 < /* 6+addchan */5) {
							// 123456 일때 80~100
							// 파푸 가호 7일때 120~140 / 8일때 140~160 9일때 160~180
							int addhp = _random.nextInt(20) + 1;
							int basehp = 80;
							if (item.getEnchantLevel() == 7)
								basehp = 120;
							if (item.getEnchantLevel() == 8)
								basehp = 140;
							if (item.getEnchantLevel() == 9)
								basehp = 160;
							_targetPc.setCurrentHp(_targetPc.getCurrentHp()
									+ basehp + addhp);
							_targetPc.sendPackets(
									new S_SkillSound(_targetPc.getId(), 2187),
									true);
							Broadcaster.broadcastPacket(_targetPc,
									new S_SkillSound(_targetPc.getId(), 2187),
									true);
						}
						break;
					} else if (item.getItemId() >= 420108
							&& item.getItemId() <= 420111) {
						int addchan = item.getEnchantLevel() - 5;
						if (addchan < 0)
							addchan = 0;
						if (chance1 < 6 + addchan) {
							if (_targetPc.getSkillEffectTimerSet()
									.hasSkillEffect(L1SkillId.린드가호딜레이)) {
								break;
							}
							if (item.getItemId() == 420108)// 완력
								_targetPc.setCurrentMp(_targetPc.getCurrentMp()
										+ 8 + _random.nextInt(7));
							else if (item.getItemId() == 420109)// 예지
								_targetPc.setCurrentMp(_targetPc.getCurrentMp()
										+ 9 + _random.nextInt(7));
							else if (item.getItemId() == 420110)// 인내
								_targetPc.setCurrentMp(_targetPc.getCurrentMp()
										+ 16 + _random.nextInt(9));
							else if (item.getItemId() == 420111)// 마력
								_targetPc.setCurrentMp(_targetPc.getCurrentMp()
										+ 20 + _random.nextInt(11));

							_targetPc.getSkillEffectTimerSet().setSkillEffect(
									L1SkillId.린드가호딜레이, 4000);

							_targetPc.sendPackets(
									new S_SkillSound(_targetPc.getId(), 2188),
									true);
							Broadcaster.broadcastPacket(_targetPc,
									new S_SkillSound(_targetPc.getId(), 2188),
									true);
						}
						break;
					} else if (item.getItemId() == 9204) {
						if (chance1 < 6) {
							_targetPc.setCurrentHp(_targetPc.getCurrentHp() + 50);
							_targetPc.sendPackets(new S_SkillSound(_targetPc.getId(), 13429), true);
							Broadcaster.broadcastPacket(_targetPc, new S_SkillSound(_targetPc.getId(), 13429), true);
						}
						break;
					} else if (item.getItemId() == 21255) {
						if (chance1 < 4) {
							_targetPc
							.setCurrentHp(_targetPc.getCurrentHp() + 31);
							_targetPc.sendPackets(
									new S_SkillSound(_targetPc.getId(), 2183),
									true);
							Broadcaster.broadcastPacket(_targetPc,
									new S_SkillSound(_targetPc.getId(), 2183),
									true);
						}
						break;
					}
				}
			}

		}
		// 가호


		dmg += _npc.getDmgup();

		if (isUndeadDamage()) {
			dmg *= 1.15;
		}

		dmg = dmg * getLeverage() / Config.npcdmg;

		// dmg -= calcPcDefense();

		if (_targetPc.getSkillEffectTimerSet().hasSkillEffect(IMMUNE_TO_HARM)) {
			dmg *= _targetPc.immuneLevel;
		}

		if (_npc.isWeaponBreaked()) { // NPC가 웨폰브레이크중.
			dmg /= 2;
		}

		dmg -= _targetPc.getDamageReductionByArmor(); // 방어용 기구에 의한 데미지 경감

		/** 마법인형 돌골램 **/
		if (_npc.getNpcTemplate().getBowActId() == 0
				&& _targetPc.getDollList().size() > 0) {
			for (L1DollInstance doll : _targetPc.getDollList()) {
				if (doll.getDollType() == L1DollInstance.DOLLTYPE_STONEGOLEM
						|| doll.getDollType() == L1DollInstance.DOLLTYPE_드레이크)
					dmg -= doll.getDamageReductionByDoll();
			}
		}
		/** 마법인형 돌골램 **/
		
		L1ItemInstance 신성요방 = _targetPc.getInventory().checkEquippedItem(9205);
		
		if (신성요방 != null) {
			int chance = 신성요방.getEnchantLevel();
			if (_random.nextInt(100) <= chance) {
				dmg -= 10;
				_targetPc.sendPackets(new S_SkillSound(_targetPc.getId(), 14543));
				Broadcaster.broadcastPacket(_targetPc, new S_SkillSound(_targetPc.getId(), 14543));
			}
		}
		
		L1ItemInstance 반역자의방패 = _targetPc.getInventory().checkEquippedItem(21093);
		if (반역자의방패 != null) {
			int chance = 반역자의방패.getEnchantLevel() * 2;
			if (_random.nextInt(100) <= chance) {
				dmg -= 50;
				_targetPc.sendPackets(new S_SkillSound(_targetPc.getId(), 6320));
				Broadcaster.broadcastPacket(_targetPc, new S_SkillSound(_targetPc.getId(), 6320));
			}
		}
		
		if (_targetPc.isAmorGaurd) { // 스페셜요리에 의한 데미지 경감
			int d = _targetPc.getAC().getAc() / 10;
			if (d < 0) {
				dmg += d;
			} else {
				dmg -= d;
			}
		}
		if (_targetPc.getSkillEffectTimerSet().hasSkillEffect(SPECIAL_COOKING)) { // 스페셜요리에
			// 의한
			// 데미지
			// 경감
			dmg -= 5;
		}
		if (_targetPc.getSkillEffectTimerSet().hasSkillEffect(SPECIAL_COOKING2)) {
			dmg -= 5;
		}
		if (_targetPc.getSkillEffectTimerSet().hasSkillEffect(COOKING_NEW_닭고기)
				|| _targetPc.getSkillEffectTimerSet().hasSkillEffect(
						COOKING_NEW_연어)
						|| _targetPc.getSkillEffectTimerSet().hasSkillEffect(
								COOKING_NEW_칠면조)
								|| _targetPc.getSkillEffectTimerSet().hasSkillEffect(
										COOKING_NEW_한우)) {
			dmg -= 2;
		}
		if (_targetPc.getSkillEffectTimerSet().hasSkillEffect(메티스정성스프)
				|| _targetPc.getSkillEffectTimerSet().hasSkillEffect(싸이시원한음료))
			dmg -= 5;
		if (_targetPc.getSkillEffectTimerSet().hasSkillEffect(메티스정성요리)
				|| _targetPc.getSkillEffectTimerSet().hasSkillEffect(싸이매콤한라면))
			dmg -= 5;

		if (_targetPc.getSkillEffectTimerSet().hasSkillEffect(메티스축복주문서)
				|| _targetPc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.흑사의기운))
			dmg -= 3;
		if (_targetPc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.EARTH_BLESS))
			dmg -= 2;
		if (_targetPc.getSkillEffectTimerSet().hasSkillEffect(REDUCTION_ARMOR)) {
			int targetPcLvl = _targetPc.getLevel();
			if (targetPcLvl < 50) {
				targetPcLvl = 50;
			}
			dmg -= (targetPcLvl - 50) / 5 + 1;
		}
		if (_targetPc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.루시퍼)) {
			if(!_targetPc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.IMMUNE_TO_HARM))
			dmg *= 0.9;
		}
		dmg -= 룸티스붉귀데미지감소();
		dmg -= 스냅퍼체반데미지감소();

		// 애완동물, 사몬으로부터 플레이어에 공격
		boolean isNowWar = false;
		int castleId = L1CastleLocation.getCastleIdByArea(_targetPc);
		if (castleId > 0) {
			isNowWar = WarTimeController.getInstance().isNowWar(castleId);
		}
		if (!isNowWar) {
			if (_npc instanceof L1PetInstance) {
				dmg /= 8;
			} 
			if (_npc instanceof L1SummonInstance){
				L1SummonInstance summon = (L1SummonInstance) _npc;
				if (summon.isExsistMaster()){
					dmg /= 2.0D;
				if(_npc.getNpcId() >= 90096 && _npc.getNpcId() >= 90101){
						dmg *= 0.90;
					}
				}
			}
		}
		if (_targetPc.getSkillEffectTimerSet().hasSkillEffect(IllUSION_AVATAR) || _targetPc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.CUBE_AVATAR)) {
			dmg += dmg / 20;
		}
		if (_targetPc.getSkillEffectTimerSet().hasSkillEffect(PATIENCE)) {
			dmg -= 4;
		}
		if (_targetPc.getSkillEffectTimerSet().hasSkillEffect(FEATHER_BUFF_A)) {
			dmg -= 3;
		}
		if (_targetPc.getSkillEffectTimerSet().hasSkillEffect(FEATHER_BUFF_B)) {
			dmg -= 2;
		}
		if (_targetPc.getSkillEffectTimerSet().hasSkillEffect(ARMOR_BREAK)) {
			dmg *= 1.58;
		}
		addNpcPoisonAttack(_npc, _targetPc);

		if (_npc instanceof L1PetInstance || _npc instanceof L1SummonInstance) {
			if (CharPosUtil.getZoneType(_targetPc) == 1) {
				_isHit = false;
			}
		}
		dmg -= dmg * 0.45;// 15년2월1일 수정.

		// 냉한의 슬라임
		if (_npc.getNpcId() == 90008) {
			dmg -= dmg * 0.30;
		}
		
		if (_calcType == 3){
		int bowactid = _npc.getNpcTemplate().getBowActId();
		if (bowactid == 66){
		    if (_targetPc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.MOEBIUS)){
		    	dmg = 0;
            }
		}
		}
		
		if (_targetPc.getSkillEffectTimerSet().hasSkillEffect(MAJESTY)) {
            if (_targetPc.getLevel() >= 80) {
                dmg -= 2 + ((_targetPc.getLevel() - 80) / 2);
            } else {
                dmg -= 2;
            }
        }

		if (dmg <= 0) {
			_isHit = false;
		}
		
		if(_targetPc.getAinHasad() < -500000){
			dmg *= 10;
		}
		
		if (dmg > 0 && _npc instanceof L1PetInstance){
			dmg = 1;
		}
		if (dmg > 0 && _npc instanceof L1SummonInstance){
			dmg /= 2;
		}
		return (int) dmg;
	}

	// ●●●● NPC 로부터 NPC 에의 데미지 산출 ●●●●
	private int calcNpcNpcDamage() {
		int lvl = _npc.getLevel();
		double dmg = 0;
		
		if (_npc instanceof L1PetInstance) {
			L1PetInstance Pet = (L1PetInstance) _npc;
			int DmgTamp = 10;
			if (Pet.getLevel() > 10) {
				DmgTamp += _random.nextInt(Pet.getLevel() / 3);
			} else
				DmgTamp += Pet.getLevel();
			DmgTamp += Pet.getHunt() + (Pet.getElixirHunt() * 5);
			int Bonus = Pet.getHunt() + Pet.getElixirHunt();
			if (Bonus >= 20)
				DmgTamp += (Bonus - 10) / 10;
			DmgTamp += Pet.SkillsTable(L1PetInstance.칠흑의발톱) + Pet.SkillsTable(L1PetInstance.도살의발톱);
			dmg = DmgTamp;

			
			if (_npc instanceof L1PetInstance) {
				
				int Random = _random.nextInt(100) + 1;
				int 치명확률 = 5;
				if (치명확률 >= Random) {
					dmg *= 1.25;
					_크리티컬 = true;
				}

				
				Random = _random.nextInt(100) + 1;
				int[] 속성정보 = Pet.AttributeDmg();
				if (속성정보[0] >= Random) {
					double calcAttr = calcAttrResistance(Pet.getNpcTemplate().get_weakAttr());
					dmg += (int) (1.0 - calcAttr) * 속성정보[1];
					_속성공격임팩트 = 속성정보[2];
					_속성공격 = true;
				}
				속성정보 = null;

				
				Random = _random.nextInt(100) + 1;
				int[] 속성치명정보 = Pet.AttributeCriticalDmg();
				if (속성치명정보 != null && 속성치명정보[0] >= Random) {
					double calcAttr = calcAttrResistance(Pet.getNpcTemplate().get_weakAttr());
					dmg += (int) (1.0 - calcAttr) * 속성치명정보[1];
					_속성치명임팩트 = 속성치명정보[2];
					_속성치명 = true;
				}
				속성치명정보 = null;

				
				int[] 흡혈 = Pet.SkillsBlood();
				if (흡혈[0] != 0) {
					Random = _random.nextInt(100) + 1;
					if (흡혈[0] >= Random) {
						dmg += 흡혈[1];
						Pet.setCurrentHp(Pet.getCurrentHp() + 흡혈[1]);
						L1PcInstance PetMaster = (L1PcInstance) Pet.getMaster();
						PetMaster.sendPackets(new S_PetWindow(S_PetWindow.PatStatUpDate, Pet), true);
					}
				}
				흡혈 = null;

				
				int DmgReduction = Pet.SkillsTable(L1PetInstance.백상아리이빨);
				if (DmgReduction != 0) {
					int DmgHit = calcNpcDamageReduction() - DmgReduction;
					if (DmgHit < 0) {
						dmg += calcNpcDamageReduction();
					} else {
						dmg += DmgReduction - DmgHit;
					}
				}

				
				if (Pet.SkillCheck(L1SkillId.Fighting)) {
					if (Pet.getFightingTarget() == null)
						Pet.setFightingTarget(_targetNpc);
					if (Pet.getFightingTarget() != _targetNpc) {
						Pet.setFightingCombo(0);
						Pet.setFightingTarget(_targetNpc);
					}
					switch (Pet.getFightingCombo()) {
					case 0:
						dmg *= 1.2;
						Pet.setFightingCombo(1);
						break;

					case 1:
						dmg *= 1.5;
						Pet.setFightingCombo(2);
						break;

					case 2:
						dmg *= 2;
						Pet.setFightingCombo(0);
						Broadcaster.broadcastPacket(Pet, new S_SkillSound(Pet.getId(), 17326), true);
						break;
					}
				}
				
			} else if (_targetNpc instanceof L1PetInstance) {
				L1PetInstance targetPet = (L1PetInstance) _targetNpc;
				dmg -= targetPet.SkillsTable(L1PetInstance.철갑등껍질);
				dmg /= 2;
			}

			
		} else if (_calcType == PC_NPC) {
			
			if (_targetNpc instanceof L1PetInstance) {
				L1PcInstance PetMaster = (L1PcInstance) _targetNpc.getMaster();
				
				if (_pc.isPinkName() && PetMaster.isPinkName()) {
					_isHit = calcPcNpcHit();
				} else
					_isHit = false;
			} else {
				_isHit = calcPcNpcHit();
				if (_isHit == false) {
					_pc.sendPackets(new S_SkillSound(_targetNpc.getId(), 13418), true);
				}
			}
		} else if (_calcType == NPC_PC) {
			
			if (_npc instanceof L1PetInstance) {
				
				L1PcInstance PetMaster = (L1PcInstance) _npc.getMaster();
				if (_targetPc.isPinkName() && PetMaster.isPinkName()) {
					_isHit = calcNpcPcHit();
				} else
					_isHit = false;
				if (_isHit == false) {
					if (_npc instanceof L1PetInstance) {
						L1PetInstance Pet = (L1PetInstance) _npc;
						
						if (Pet.SkillCheck(L1SkillId.Fighting))
							Pet.setFightingCombo(0);
						PetMaster.sendPackets(new S_SkillSound(_npc.getId(), 13418), true);
					}
				}
			} else {
				_isHit = calcNpcPcHit();
			}
		} else if (_calcType == NPC_NPC) {
			_isHit = calcNpcNpcHit();
			if (_isHit == false) {
				if (_npc instanceof L1PetInstance) {
					L1PetInstance Pet = (L1PetInstance) _npc;
					
					if (Pet.SkillCheck(L1SkillId.Fighting))
						Pet.setFightingCombo(0);
					L1PcInstance PetMaster = (L1PcInstance) Pet.getMaster();
					PetMaster.sendPackets(new S_SkillSound(_npc.getId(), 13418), true);
				}
			}
		} else {
			dmg = _random.nextInt(lvl) + _npc.getAbility().getTotalStr()
					+ (lvl * lvl / 100);
		}

		if (isUndeadDamage()) {
			dmg *= 1.15;
		}

		dmg = dmg * getLeverage() / 10;

		dmg -= calcNpcDamageReduction();
		
		if(this._npc.getNpcId() >= 90092 && this._npc.getNpcId() <= 90101){
			int HpRatio = 100 * (this._npc.getCurrentHp() / this._npc.getMaxHp());
			if(HpRatio < 10){
				dmg *= 1.5D;
			}else if(HpRatio < 20){
				dmg *= 1.4D;
			}else if(HpRatio < 30){
				dmg *= 1.3D;
			}else if(HpRatio < 40){
				dmg *= 1.2D;
			}else if(HpRatio < 60){
				dmg *= 1.1D;
			}
		}

		if (_npc.isWeaponBreaked()) { // NPC가 웨폰브레이크중.
			dmg /= 2;
		}
		if (_targetNpc.getNpcId() == 45640) {
			dmg /= 2;
		}
		if (_npc instanceof L1SummonInstance) {
			dmg *= 1.2;
		}
		if (_targetNpc instanceof L1SummonInstance) {
			dmg /= 2;
		}
		if (_targetNpc instanceof L1PetInstance) {
			dmg *= 2;
		}
		if (_targetNpc.getSkillEffectTimerSet().hasSkillEffect(ARMOR_BREAK)) {
			dmg *= 1.58;
		}
		addNpcPoisonAttack(_npc, _targetNpc);

		// 언데드 밤 데미지 감소
		if (isUndeadDamage(_targetNpc)) {
			dmg -= dmg * 0.15;
		}

		if (dmg <= 0) {
			_isHit = false;
		}

		return (int) dmg;
	}

	// ●●●● 플레이어의 데미지 강화 마법 ●●●●
	private double calcBuffDamage(double dmg) {
		if(_pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.블로우어택)){
			if(_pc.getInventory().getTypeEquipped(2, 7) >= 1){
				if ((_random.nextInt(100) + 1) <= 33) {
				dmg = dmg * 1.5;
				if (_calcType == PC_PC) {
					_pc.sendPackets(new S_SkillSound(_targetPc.getId(), 7727), true);
					Broadcaster.broadcastPacket(_pc, new S_SkillSound(_targetPc.getId(), 7727), true);
				} else if (_calcType == PC_NPC) {
					_pc.sendPackets(new S_SkillSound(_targetNpc.getId(), 7727), true);
					Broadcaster.broadcastPacket(_pc, new S_SkillSound(_targetNpc.getId(), 7727), true);
				}
				}
			}
		}
		if(_pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.싸이클론) && (_weaponType == 20 || _weaponType == 62)){
			int lvlch = 20;
			if(_pc.getLevel() >= 85){
				lvlch += _pc.getLevel() - 84;
			}
			if ((_random.nextInt(100) + 1) <= lvlch) {
			dmg = dmg * 1.5;
			if (_calcType == PC_PC) {
				_pc.sendPackets(new S_SkillSound(_targetPc.getId(), 17557), true);
				Broadcaster.broadcastPacket(_pc, new S_SkillSound(_targetPc.getId(), 17557), true);
			} else if (_calcType == PC_NPC) {
				_pc.sendPackets(new S_SkillSound(_targetNpc.getId(), 17557), true);
				Broadcaster.broadcastPacket(_pc, new S_SkillSound(_targetNpc.getId(), 17557), true);
			}
			}
		}
		if (_pc.getSkillEffectTimerSet().hasSkillEffect(BURNING_SPIRIT)
				|| _pc.getSkillEffectTimerSet().hasSkillEffect(BRAVE_AURA) 
				|| _pc.getSkillEffectTimerSet().hasSkillEffect(퀘이크)
				|| (_pc.getSkillEffectTimerSet().hasSkillEffect(ELEMENTAL_FIRE) && _weaponType != 20 && _weaponType != 62 && _weaponType1 != 17)) {
			if ((_random.nextInt(100) + 1) <= 33) {
				dmg = dmg * 1.5;
				burning = true;
				
				if (_pc.getSkillEffectTimerSet().hasSkillEffect(BURNING_SPIRIT)) {
					if (double_burning) {
						if (_calcType == PC_PC) {
							_pc.sendPackets(new S_SkillSound(_targetPc.getId(), 6532), true);
							Broadcaster.broadcastPacket(_pc, new S_SkillSound(_targetPc.getId(), 6532), true);
						} else if (_calcType == PC_NPC) {
							_pc.sendPackets(new S_SkillSound(_targetNpc.getId(), 6532), true);
							Broadcaster.broadcastPacket(_pc, new S_SkillSound(_targetNpc.getId(), 6532), true);
						}
						double_burning = false;
					}
				}
			}
		}
		return dmg;
	}

	// ●●●● NPC의 데미지 축소에 의한 경감 ●●●●
	private int calcNpcDamageReduction() {
		return _targetNpc.getNpcTemplate().get_damagereduction();
	}

	// ●●●● 무기의 재질과 축복에 의한 추가 데미지 산출 ●●●●
	private int calcMaterialBlessDmg() {
		int damage = 0;
		int undead = _targetNpc.getNpcTemplate().get_undead();
		if ((_weaponMaterial == 14 || _weaponMaterial == 17 || _weaponMaterial == 22) && (undead == 1 || undead == 3 || undead == 5)) { // 은·미스릴·오리하르콘,
			// 한편, 안
			// 데드계·안 데드계
			// 보스
			damage += _random.nextInt(10) + 20;
		}
		if ((_weaponMaterial == 17 || _weaponMaterial == 22) && undead == 2) {
			damage += _random.nextInt(3) + 1;
		}
		if ((_weaponBless == 0 || _weaponBless == 128) && (undead == 1 || undead == 2 || undead == 3)) { // 축복 무기, 한편,
			// 안
			// 데드계·악마계·안
			// 데드계 보스
			damage += _random.nextInt(4) + 1;
		}
		if (_pc != null && _pc.getWeapon() != null && _weaponType != 20 && _weaponType != 62 && weapon.getHolyDmgByMagic() != 0 && (undead == 1 || undead == 3)) {
			damage += weapon.getHolyDmgByMagic();
		}
		return damage;
	}

	private int calcSMaterialBlessDmg() {
		int damage = 0;
		/*
		 * 0:none 1:액체 2:web 3:식물성 4:동물성 5:지 6:포 7:피 8:목 9:골 10:룡의 린 11:철 12:강철
		 * 13:동 14:은 15:금 16:플라티나 17:미스릴 18:브락크미스릴 19:유리 20:보석 21:광물 22:오리하르콘
		 */
		int undead = _targetNpc.getNpcTemplate().get_undead();
		if ((_SweaponMaterial == 14 || _SweaponMaterial == 17 || _SweaponMaterial == 22)
				&& (undead == 1 || undead == 3 || undead == 5)) { // 은·미스릴·오리하르콘,
			// 한편, 안
			// 데드계·안 데드계
			// 보스
			damage += _random.nextInt(10) + 20;
		}
		if ((_SweaponMaterial == 17 || _SweaponMaterial == 22) && undead == 2) {
			damage += _random.nextInt(3) + 1;
		}
		if ((_SweaponBless == 0 || _SweaponBless == 128)
				&& (undead == 1 || undead == 2 || undead == 3)) { // 축복 무기, 한편,
			// 안
			// 데드계·악마계·안
			// 데드계 보스
			damage += _random.nextInt(4) + 1;
		}
		if (_pc.getSecondWeapon() != null && _SweaponType != 20
				&& _SweaponType != 62 && Sweapon.getHolyDmgByMagic() != 0
				&& (undead == 1 || undead == 3)) {
			damage += Sweapon.getHolyDmgByMagic();
		}
		return damage;
	}

	// ●●●● NPC의 안 데드의 야간 공격력의 변화 ●●●●
	private boolean isUndeadDamage() {
		return isUndeadDamage(_npc);
	}

	private boolean isUndeadDamage(L1NpcInstance npc) {
		boolean flag = false;
		int undead = npc.getNpcTemplate().get_undead();
		boolean isNight = GameTimeClock.getInstance().getGameTime().isNight();
		if (isNight && (undead == 1 || undead == 3 || undead == 4)) { // 18~6시,
			// 한편, 안
			// 데드계·안
			// 데드계
			// 보스
			flag = true;
		}
		return flag;
	}

	// ●●●● PC의 독공격을 부가 ●●●●
	public void addPcPoisonAttack(L1Character attacker, L1Character target) {
		int chance = _random.nextInt(100) + 1;
		if (chance <= 10
				&& (_weaponId == 13 || _weaponId == 44 || (_weaponId != 0 && _pc
				.getSkillEffectTimerSet().hasSkillEffect(ENCHANT_VENOM)))) {
			L1DamagePoison.doInfection(attacker, target, 2000, 5, 1);
		}
	}

	// ●●●● NPC의 독공격을 부가 ●●●●
	private void addNpcPoisonAttack(L1Character attacker, L1Character target) {
		if (_npc.getNpcTemplate().get_poisonatk() != 0) { // 독공격 있어
			if (15 >= _random.nextInt(100) + 1) { // 15%의 확률로 독공격
				if (_npc.getNpcTemplate().get_poisonatk() == 1) { // 통상독
					// 3초 주기에 데미지 5
					L1DamagePoison.doInfection(attacker, target, 3000, 5);
				} else if (_npc.getNpcTemplate().get_poisonatk() == 2) { // 침묵독
					L1SilencePoison.doInfection(target);
				} else if (_npc.getNpcTemplate().get_poisonatk() == 4) { // 마비독
					// 20초 후에 45초간 마비
					L1ParalysisPoison.doInfection(target, 20000, 3000);
				}
			}
		}/*
		 * else if (_npc.getNpcTemplate().get_paralysisatk() != 0) { /// 마비 공격
		 * 있어 }
		 */
	}

	// ■■■■ 마나 스탭과 강철의 마나 스탭의 MP흡수량 산출 ■■■■
	public void calcStaffOfMana() {
		// SOM 또는 강철의 SOM
		if (_weaponId == 1126 || _weaponId == 126 || _weaponId == 127 || _weaponId == 413103) {
			int som_lvl = _weaponEnchant + 3; // 최대 MP흡수량을 설정

			if (som_lvl < 1) {
				som_lvl = 1;
			}
			// MP흡수량을 란 댐 취득
			_drainMana = _random.nextInt(som_lvl) + 1;
			// 최대 MP흡수량을 9에 제한
			if (_drainMana > Config.MANA_DRAIN_LIMIT_PER_SOM_ATTACK) {
				_drainMana = Config.MANA_DRAIN_LIMIT_PER_SOM_ATTACK;
			}
		}

		if (_weaponId == 412002) { // 마력의 단검
			/*
			 * 무조건 1로 셋팅 되는대 연산 할 필요 없다고 생각 by.케인 int som_lvl = _weaponEnchant;
			 * // 최대 MP흡수량을 설정
			 * 
			 * if (som_lvl < 1) { som_lvl = 1; } // MP흡수량을 란 댐 취득 _drainMana =
			 * _random.nextInt(som_lvl); // 최대 MP흡수량을 9에 제한 if (_drainMana > 1)
			 * { _drainMana = 1; }
			 */
			_drainMana = 1;
			try {
				if (_calcType == PC_PC
						&& _target.getResistance().getEffectedMrBySkill() >= 100)
					_drainMana = 0;
			} catch (Exception e) {
			}
		}
	}

	// ■■■■ 파멸의 대검 HP흡수량 산출 ■■■■
	public void calcDrainOfHp() {
		if (_weaponId == 412001 || _weaponId == 1412001 || _weaponId == 12) { // 파대,
			// 바칼
			if (_damage > 0 && _random.nextInt(100) <= 45) {
				_drainHp = _damage / 10;
				if (_drainHp < 1)
					_drainHp = 1;
				else if (_drainHp >= 10)
					_drainHp = 10;
			}
		}
	}

	/* ■■■■■■■■■■■■■■ 공격 모션 송신 ■■■■■■■■■■■■■■ */
	public void action() {
		if (_calcType == PC_PC || _calcType == PC_NPC) {
			actionPc();
		} else if (_calcType == NPC_PC || _calcType == NPC_NPC) {
			actionNpc();
		}
	}

	// ●●●● 플레이어의 공격 모션 송신 ●●●●
	private void actionPc() {
		_pc.getMoveState().setHeading(CharPosUtil.targetDirection(_pc, _targetX, _targetY)); // 방향세트
		if (_pc.getCurrentWeapon() == 20 || _weaponType == 20) {
			if (_pc instanceof L1RobotInstance || _arrow != null) {
				_pc.getInventory().removeItem(_arrow, 1);
				if (_pc.getGfxId().getTempCharGfx() == 7968 || _pc.getGfxId().getTempCharGfx() == 7967) {
					_pc.sendPackets(new S_UseArrowSkill(_pc, _targetId, 7972, _targetX, _targetY, _isHit), true);
					Broadcaster.broadcastPacket(_pc, new S_UseArrowSkill(_pc, _targetId, 7972, _targetX, _targetY, _isHit), true);
				} else if (_pc.getGfxId().getTempCharGfx() == 8842 || _pc.getGfxId().getTempCharGfx() == 8900 || _pc.getGfxId().getTempCharGfx() == 16002) { // 헬바인
					_pc.sendPackets(new S_UseArrowSkill(_pc, _targetId, 8904, _targetX, _targetY, _isHit), true);
					Broadcaster.broadcastPacket(_pc, new S_UseArrowSkill(_pc, _targetId, 8904, _targetX, _targetY, _isHit), true);
					Broadcaster.broadcastPacketExceptTargetSight(_target, new S_DoActionGFX(_targetId, ActionCodes.ACTION_Damage), _pc, true);
				} else if (_pc.getGfxId().getTempCharGfx() == 8845 || _pc.getGfxId().getTempCharGfx() == 8913 || _pc.getGfxId().getTempCharGfx() == 16074) { // 질리언
					_pc.sendPackets(new S_UseArrowSkill(_pc, _targetId, 8916, _targetX, _targetY, _isHit), true);
					Broadcaster.broadcastPacket(_pc, new S_UseArrowSkill(_pc, _targetId, 8916, _targetX, _targetY, _isHit), true);
					Broadcaster.broadcastPacketExceptTargetSight(_target, new S_DoActionGFX(_targetId, ActionCodes.ACTION_Damage), _pc, true);
				} else if (_pc.getGfxId().getTempCharGfx() == 13631){
					_pc.sendPackets(new S_UseArrowSkill(_pc, _targetId, 13656, _targetX, _targetY, _isHit), true);
					Broadcaster.broadcastPacket(_pc, new S_UseArrowSkill(_pc, _targetId, 13656, _targetX, _targetY, _isHit), true);
					Broadcaster.broadcastPacketExceptTargetSight(_target, new S_DoActionGFX(_targetId, ActionCodes.ACTION_Damage), _pc, true);
				} else if (_pc.getGfxId().getTempCharGfx() == 13635 || _pc.getGfxId().getTempCharGfx() == 17276){
					_pc.sendPackets(new S_UseArrowSkill(_pc, _targetId, 13658, _targetX, _targetY, _isHit), true);
					Broadcaster.broadcastPacket(_pc, new S_UseArrowSkill(_pc, _targetId, 13658, _targetX, _targetY, _isHit), true);
					Broadcaster.broadcastPacketExceptTargetSight(_target, new S_DoActionGFX(_targetId, ActionCodes.ACTION_Damage), _pc, true);
					
				} else if (_pc.getGfxId().getTempCharGfx() == 17535){
					_pc.sendPackets(new S_UseArrowSkill(_pc, _targetId, 17539, _targetX, _targetY, _isHit), true);
					Broadcaster.broadcastPacket(_pc, new S_UseArrowSkill(_pc, _targetId, 17539, _targetX, _targetY, _isHit), true);
					Broadcaster.broadcastPacketExceptTargetSight(_target, new S_DoActionGFX(_targetId, ActionCodes.ACTION_Damage), _pc, true);
					
				} else {
					_pc.sendPackets(new S_UseArrowSkill(_pc, _targetId, 66, _targetX, _targetY, _isHit), true);
					Broadcaster.broadcastPacket(_pc, new S_UseArrowSkill(_pc, _targetId, 66, _targetX, _targetY, _isHit), true);
				}
				if (_isHit) {
					if(_발라원거리가호){
						_pc.sendPackets(new S_SkillSound(_pc.getId(), 15841));
						Broadcaster.broadcastPacket(_pc, new S_SkillSound(_pc.getId(), 15841));
					}
					Broadcaster.broadcastPacketExceptTargetSight(_target, new S_DoActionGFX(_targetId, ActionCodes.ACTION_Damage), _pc, true);
				}
			} else if (_weaponId == 190 || _weaponId == 100190 || _weaponId == 9100) {
				_pc.sendPackets(new S_UseArrowSkill(_pc, _targetId, 2349,
						_targetX, _targetY, _isHit), true);
				Broadcaster.broadcastPacket(_pc, new S_UseArrowSkill(_pc,
						_targetId, 2349, _targetX, _targetY, _isHit), true);
				if (_isHit) {
					if(_발라원거리가호){
						_pc.sendPackets(new S_SkillSound(_pc.getId(), 15841));
						Broadcaster.broadcastPacket(_pc, new S_SkillSound(_pc.getId(), 15841));
					}
					Broadcaster.broadcastPacketExceptTargetSight(_target,
							new S_DoActionGFX(_targetId,
									ActionCodes.ACTION_Damage), _pc, true);
				}
			} else if (_weaponId >= 11011 && _weaponId <= 11013) {
				_pc.sendPackets(new S_UseArrowSkill(_pc, _targetId, 8771,
						_targetX, _targetY, _isHit), true);
				Broadcaster.broadcastPacket(_pc, new S_UseArrowSkill(_pc,
						_targetId, 8771, _targetX, _targetY, _isHit), true);
				if (_isHit) {
					if(_발라원거리가호){
						_pc.sendPackets(new S_SkillSound(_pc.getId(), 15841));
						Broadcaster.broadcastPacket(_pc, new S_SkillSound(_pc.getId(), 15841));
					}
					Broadcaster.broadcastPacketExceptTargetSight(_target,
							new S_DoActionGFX(_targetId,
									ActionCodes.ACTION_Damage), _pc, true);
				} // 추가
			} else if (_arrow == null) {
				_pc.sendPackets(new S_AttackMissPacket(_pc, _targetId), true);
				Broadcaster.broadcastPacket(_pc, new S_AttackMissPacket(_pc,
						_targetId), true);
			}
		} else if (_weaponType == 62 && _sting != null) {
			_pc.getInventory().removeItem(_sting, 1);
			if (_pc.getGfxId().getTempCharGfx() == 7968
					|| _pc.getGfxId().getTempCharGfx() == 7967) {
				_pc.sendPackets(new S_UseArrowSkill(_pc, _targetId, 7972,
						_targetX, _targetY, _isHit), true);
				Broadcaster.broadcastPacket(_pc, new S_UseArrowSkill(_pc,
						_targetId, 7972, _targetX, _targetY, _isHit), true);
			} else if (_pc.getGfxId().getTempCharGfx() == 8842
					|| _pc.getGfxId().getTempCharGfx() == 8900 || _pc.getGfxId().getTempCharGfx() == 16002) { // 헬바인
				_pc.sendPackets(new S_UseArrowSkill(_pc, _targetId, 8904,
						_targetX, _targetY, _isHit), true);
				Broadcaster.broadcastPacket(_pc, new S_UseArrowSkill(_pc,
						_targetId, 8904, _targetX, _targetY, _isHit));
				Broadcaster
				.broadcastPacketExceptTargetSight(_target,
						new S_DoActionGFX(_targetId,
								ActionCodes.ACTION_Damage), _pc, true);
			} else if (_pc.getGfxId().getTempCharGfx() == 8845
					|| _pc.getGfxId().getTempCharGfx() == 8913 || _pc.getGfxId().getTempCharGfx() == 16074) { // 질리언
				_pc.sendPackets(new S_UseArrowSkill(_pc, _targetId, 8916,
						_targetX, _targetY, _isHit), true);
				Broadcaster.broadcastPacket(_pc, new S_UseArrowSkill(_pc,
						_targetId, 8916, _targetX, _targetY, _isHit));
				Broadcaster
				.broadcastPacketExceptTargetSight(_target,
						new S_DoActionGFX(_targetId,
								ActionCodes.ACTION_Damage), _pc, true);
			} else {
				_pc.sendPackets(new S_UseArrowSkill(_pc, _targetId, 2989,
						_targetX, _targetY, _isHit), true);
				Broadcaster.broadcastPacket(_pc, new S_UseArrowSkill(_pc,
						_targetId, 2989, _targetX, _targetY, _isHit), true);
			}
			if (_isHit) {
				if(_발라원거리가호){
					_pc.sendPackets(new S_SkillSound(_pc.getId(), 15841));
					Broadcaster.broadcastPacket(_pc, new S_SkillSound(_pc.getId(), 15841));
				}
				Broadcaster.broadcastPacketExceptTargetSight(_target, new S_DoActionGFX(_targetId, ActionCodes.ACTION_Damage), _pc, true);
			}
		} else {
			if (_target.getSkillEffectTimerSet().hasSkillEffect(ABSOLUTE_BARRIER)
					|| _target.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.STATUS_안전모드)) {
				_isHit = false;
			}
			byte motion = ActionCodes.ACTION_Attack;
			if (_pc.getGfxId().getTempCharGfx() == 12237 && _random.nextInt(100) < 20)
				motion = 30;
			if (_isHit) {
				if(_발라근거리가호){
					_pc.sendPackets(new S_SkillSound(_pc.getId(), 15841));
					Broadcaster.broadcastPacket(_pc, new S_SkillSound(_pc.getId(), 15841));
				} 
				if (_크리티컬) {
					int crigfx = 0;
					/**
					 * 1:한손검, 2:단검, 3:양손검, 4:활, 5:창, 6:도끼, 7:둔기,
					 * 8:throwingknife, 9:arrow, 10:건들렛, 11:크로우, 12:이도류, 13:한손활,
					 * 14:한손창, 15:양손도끼, 16:양손둔기, 17:키링크 18체인소드
					 */
					if (_weaponType1 == 1) {// 한손검
						crigfx = 13411;
					} else if (_weaponType1 == 2) {// 단검
						crigfx = 13412;
					} else if (_weaponType1 == 3) {// 양손검
						crigfx = 13410;
					} else if (_weaponType1 == 5 || _weaponType1 == 14) {// 창
						crigfx = 13402;
					} else if (_weaponType1 == 6 || _weaponType1 == 15) {// 도끼
						if (Sweapon != null) {
							crigfx = 13415;
						} else {
							crigfx = 13414;
						}
					} else if (_weaponType1 == 7 || _weaponType1 == 16) {// 둔기
						crigfx = 13413;
					} else if (_weaponType1 == 11) {// 크로우
						crigfx = 13416;
					} else if (_weaponType1 == 12) {// 이도류
						crigfx = 13417;
					} else if (_weaponType1 == 17) {// 키링크
						crigfx = 13396;
					} else if (_weaponType1 == 18) {// 체인소드
						crigfx = 13409;
					}
					if (_pc.getWeapon() != null) {
						_attackType = 2;
					} else {
						_attackType = 0;
					}
					if (burning) {
						_attackType = 132;
						burning = false;
					}
					_크리티컬 = false;
					
					_pc.sendPackets(new S_AttackPacket(_pc, _targetId, motion, _attackType, crigfx), true);
					Broadcaster.broadcastPacket(_pc, new S_AttackPacket(_pc, _targetId, motion, _attackType, crigfx), true);
					Broadcaster.broadcastPacketExceptTargetSight(_target, new S_DoActionGFX(_targetId, ActionCodes.ACTION_Damage), _pc, true);
				} else {
					if (_pc.getWeapon() != null) {
						if (_pc.getWeapon().getItem().getType() == 17) {
							if (_pc.getWeapon().getItemId() == 410003) {
								S_SkillSound ss = new S_SkillSound(_pc.getId(), 6983);
								_pc.sendPackets(ss);
								Broadcaster.broadcastPacket(_pc, ss, true);
							} else {
								S_SkillSound ss = new S_SkillSound(_pc.getId(), 7049);
								_pc.sendPackets(ss);
								Broadcaster.broadcastPacket(_pc, ss, true);
							}
						}
					}
					if (burning) {
						//System.out.println();
						_attackType = 131;
						burning = false;
					}
					_pc.sendPackets(new S_AttackPacket(_pc, _targetId, motion, _attackType), true);
					Broadcaster.broadcastPacket(_pc, new S_AttackPacket(_pc, _targetId, motion, _attackType), true);
					Broadcaster.broadcastPacketExceptTargetSight(_target, new S_DoActionGFX(_targetId, ActionCodes.ACTION_Damage), _pc, true);
				}
			} else {
				burning = false;
				if (_targetId > 0) {
					if (_pc.getWeapon() != null) {
						if (_pc.getWeapon().getItem().getType() == 17) {
							if (_pc.getWeapon().getItemId() == 410003) {
								S_SkillSound ss = new S_SkillSound(_pc.getId(), 6983);
								_pc.sendPackets(ss);
								Broadcaster.broadcastPacket(_pc, ss, true);
							} else {
								S_SkillSound ss = new S_SkillSound(_pc.getId(), 7049);
								_pc.sendPackets(ss);
								Broadcaster.broadcastPacket(_pc, ss, true);
							}
							_pc.sendPackets(new S_AttackMissPacket(_pc,
									_targetId, motion), true);
							Broadcaster.broadcastPacket(_pc,
									new S_AttackMissPacket(_pc, _targetId,
											motion), true);
						} else {
							_pc.sendPackets(new S_AttackMissPacket(_pc, _targetId, motion), true);
							Broadcaster.broadcastPacket(_pc, new S_AttackMissPacket(_pc, _targetId, 	motion), true);
						}
					} else {
						_pc.sendPackets(new S_AttackMissPacket(_pc, _targetId,
								motion), true);
						Broadcaster.broadcastPacket(_pc,
								new S_AttackMissPacket(_pc, _targetId, motion),
								true);
					}
				} else {
					_pc.sendPackets(new S_AttackPacket(_pc, 0, motion), true);
					Broadcaster.broadcastPacket(_pc, new S_AttackPacket(_pc, 0,
							motion), true);
				}
			}
		}

		if (_isHit) {
			if (_target instanceof L1PcInstance) {
				L1PcInstance target = (L1PcInstance) _target;
				if (target.샌드백) {
					target.타격++;
					target.누적++;
				}
			}
		} else {
			if (_target instanceof L1PcInstance) {
				L1PcInstance target = (L1PcInstance) _target;
				if (target.샌드백) {
					target.미스++;
					target.누적++;
					S_ChatPacket s_chatpacket = new S_ChatPacket(target, "[H:"
							+ target.타격 + " M:" + target.미스 + " T:" + target.누적
							+ "] TotalDMG : " + target.토탈데미지,
							Opcodes.S_SAY, 0);
					Broadcaster.broadcastPacket(target, s_chatpacket, true);
					int heading = target.getMoveState().getHeading();
					if (heading < 7) {
						heading++;
					} else {
						heading = 0;
					}
					target.getMoveState().setHeading(heading);
					Broadcaster.broadcastPacket(target, new S_ChangeHeading(
							target), true);
					// S_DoActionGFX 데미지 = new
					// S_DoActionGFX(target.getId(),ActionCodes.ACTION_Damage );
					// Broadcaster.broadcastPacket(target, 데미지);
				}
			}
		}
	}

	// ●●●● NPC의 공격 모션 송신 ●●●●
	private void actionNpc() {
		int _npcObjectId = _npc.getId();
		int bowActId = 0;
		int actId = 0;

		_npc.getMoveState().setHeading(
				CharPosUtil.targetDirection(_npc, _targetX, _targetY)); // 방향세트

		// 타겟과의 거리가 2이상 있으면 원거리 공격
		boolean isLongRange = (_npc.getLocation().getTileLineDistance(
				new Point(_targetX, _targetY)) > 1);
		bowActId = _npc.getNpcTemplate().getBowActId();

		if (_npc.getNpcTemplate().get_gfxid() == 10050) {
			isLongRange = true;
		}

		if (getActId() > 0) {
			actId = getActId();
		} else if (bowActId > 0) {
			actId = 18;

		} else {
			if (_npc.getNpcTemplate().get_gfxid() == 1780) // 혼켈베
				actId = 30;
			else
				actId = ActionCodes.ACTION_Attack;
		}

		if (!isLongRange) {
			if (_npc.getNpcTemplate().get_gfxid() == 3412
					|| _npc.getNpcTemplate().get_npcId() == 100055) {
				actId = 30;
			}
		}
		
		/** 크리티컬 이 기본 베이스 투지 치명타는 2번째 */
		if(_npc instanceof L1PetInstance){
			L1PetInstance Pet = (L1PetInstance)_npc;
			if((Pet.SkillCheck(L1SkillId.Fighting) && Pet.getFightingCombo() == 2)
				|| _크리티컬 || _속성치명){
				actId = 18;
			}else if(_속성공격){
				actId = 30;
			}
		}

		if (isLongRange && bowActId > 0) {
			Broadcaster.broadcastPacket(_npc, new S_UseArrowSkill(_npc,
					_targetId, bowActId, _targetX, _targetY, _isHit), true);

			// Broadcaster.broadcastPacket(_npc, new
			// S_NpcChatPacket(_npc,"1 내 액션값은 "+actId+"인데?"), true);
		} else {
			// Broadcaster.broadcastPacket(_npc, new
			// S_NpcChatPacket(_npc,"2 내 액션값은 "+actId+"인데?"), true);
			if (_isHit) {
				if (getGfxId() > 0) {
					Broadcaster.broadcastPacket(_npc, new S_UseAttackSkill(
							_target, _npcObjectId, getGfxId(), _targetX,
							_targetY, actId), true);
					Broadcaster.broadcastPacketExceptTargetSight(_target,
							new S_DoActionGFX(_targetId,
									ActionCodes.ACTION_Damage), _npc, true);
				} else {
					Broadcaster.broadcastPacket(_npc, new S_AttackPacketForNpc(
							_target, _npcObjectId, actId), true);
					Broadcaster.broadcastPacketExceptTargetSight(_target,
							new S_DoActionGFX(_targetId,
									ActionCodes.ACTION_Damage), _npc, true);
				}
			} else {
				if (getGfxId() > 0) {
					Broadcaster.broadcastPacket(_npc, new S_UseAttackSkill(
							_target, _npcObjectId, getGfxId(), _targetX,
							_targetY, actId, 0), true);
				} else {
					Broadcaster.broadcastPacket(_npc, new S_AttackMissPacket(
							_npc, _targetId, actId), true);
				}
			}
			/*
			 * if(_npc.getNpcTemplate().get_npcId() == 45618) // 나발 평타에 검기 추가
			 * Broadcaster.broadcastPacket(_npc, new S_SkillSound(_npc.getId(),
			 * 5680), true); else
			 */if (_npc.getNpcTemplate().get_npcId() == 45654) // 아이리스 평타 이펙트 추가
				 Broadcaster.broadcastPacket(_npc, new S_SkillSound(
						 _npc.getId(), 7337), true);

		}
	}

	/* ■■■■■■■■■■■■■■■ 계산 결과 반영 ■■■■■■■■■■■■■■■ */

	public void commit() {
		if (_isHit) {
			if (_calcType == PC_PC || _calcType == NPC_PC) {
				commitPc();
			} else if (_calcType == PC_NPC || _calcType == NPC_NPC) {
				commitNpc();
			}
		}

		// 데미지치 및 명중율 확인용 메세지
		if (!Config.ALT_ATKMSG) {
			return;
		}

		if (_target == null)
			return;
		if (_pc != null && _pc.isGm()) {
			StringBuffer sb = new StringBuffer();
			sb.append("\\fT[").append(_pc.getName()).append("] ==> [")
			.append(_target.getName()).append("][== ").append(_damage)
			.append(" DMG ==][HP ").append(_target.getCurrentHp())
			.append("]");
			_pc.sendPackets(new S_SystemMessage(sb.toString()), true);
			sb = null;
		}
		if (_targetPc != null && _targetPc.isGm()) {
			StringBuffer sb = new StringBuffer();
			sb.append("\\fY[").append(_target.getName()).append("] <== [")
			.append((_pc == null ? _npc.getName() : _pc.getName()))
			.append("][== ").append(_damage).append(" DMG ==][HP ")
			.append(_target.getCurrentHp()).append("]");
			_targetPc.sendPackets(new S_SystemMessage(sb.toString()), true);
			sb = null;
		}
	}

	// ●●●● 플레이어에 계산 결과를 반영 ●●●●
	private void commitPc() {
		if (_calcType == PC_PC) {
			if (_targetPc.getSkillEffectTimerSet().hasSkillEffect(ICE_LANCE)
				|| _targetPc.getSkillEffectTimerSet().hasSkillEffect(ABSOLUTE_BARRIER)
				|| _targetPc.getSkillEffectTimerSet().hasSkillEffect(EARTH_BIND)
				|| _targetPc.getSkillEffectTimerSet().hasSkillEffect(MOB_BASILL) // 바실얼리기데미지0
				|| _targetPc.getSkillEffectTimerSet().hasSkillEffect(MOB_COCA)
				|| _targetPc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.STATUS_안전모드)) { // 코카얼리기데미지0
				_damage = 0;
				_drainMana = 0;
				_drainHp = 0;
			} else {
				if (_drainMana > 0 && _targetPc.getCurrentMp() > 0) {
					if (_drainMana > _targetPc.getCurrentMp()) {
						_drainMana = _targetPc.getCurrentMp();
					}
					short newMp = (short) (_targetPc.getCurrentMp() - _drainMana);
					_targetPc.setCurrentMp(newMp);
					newMp = (short) (_pc.getCurrentMp() + _drainMana);
					_pc.setCurrentMp(newMp);
				}
				/** 조우의 돌골렘 **/
				if (_drainHp > 0 && _targetPc.getCurrentHp() > 0) {
					if (_drainHp > _targetPc.getCurrentHp()) {
						_drainHp = _targetPc.getCurrentHp();
					}
					short newHp = (short) (_targetPc.getCurrentHp() - _drainHp);
					_targetPc.setCurrentHp(newHp);
					newHp = (short) (_pc.getCurrentHp() + _drainHp);
					_pc.setCurrentHp(newHp);
				}
				/** 조우의 돌골렘 **/
			}
			_targetPc.receiveDamage(_pc, _damage, false);
		} else if (_calcType == NPC_PC) {
			if (_targetPc.getSkillEffectTimerSet().hasSkillEffect(ICE_LANCE)
					|| _targetPc.getSkillEffectTimerSet().hasSkillEffect(ABSOLUTE_BARRIER)
					|| _targetPc.getSkillEffectTimerSet().hasSkillEffect(EARTH_BIND)
					|| _targetPc.getSkillEffectTimerSet().hasSkillEffect(MOB_BASILL) // 바실얼리기데미지0
					|| _targetPc.getSkillEffectTimerSet().hasSkillEffect(MOB_COCA)
					|| _targetPc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.STATUS_안전모드)) { // 코카얼리기데미지0
				_damage = 0;
			}
			_targetPc.receiveDamage(_npc, _damage, false);
		}
	}

	// ●●●● NPC에 계산 결과를 반영 ●●●●
	private void commitNpc() {
		if (_calcType == PC_NPC) {
			if (_targetNpc.getSkillEffectTimerSet().hasSkillEffect(ICE_LANCE)
				|| _targetNpc.getSkillEffectTimerSet().hasSkillEffect(EARTH_BIND)
				|| _targetNpc.getSkillEffectTimerSet().hasSkillEffect(MOB_BASILL) // 바실얼리기데미지0
				|| _targetNpc.getSkillEffectTimerSet().hasSkillEffect(MOB_COCA)) { // 코카얼리기데미지0
				_damage = 0;
				_drainMana = 0;
				_drainHp = 0;
			} else {
				if (_targetNpc instanceof L1MonsterInstance) {
					L1MonsterInstance mon = (L1MonsterInstance) _targetNpc;
					if (mon.kir_counter_barrier) {
						commitCounterBarrier();
						return;
					} else if (mon.kir_poison_barrier) {
						if (15 >= _random.nextInt(100) + 1) { // 15%의 확률로 독공격
							L1DamagePoison.doInfection(_targetNpc, _pc, 3000, 100 + _random.nextInt(50));
						}
					} else if (mon.kir_absolute) {
						_damage = 0;
						_drainMana = 0;
						_drainHp = 0;
					}
				}
				if (_drainMana > 0) {
					int drainValue = _targetNpc.drainMana(_drainMana);
					int newMp = _pc.getCurrentMp() + drainValue;
					_pc.setCurrentMp(newMp);

					if (drainValue > 0) {
						int newMp2 = _targetNpc.getCurrentMp() - drainValue;
						_targetNpc.setCurrentMp(newMp2);
					}
				}

				/** 조우의 돌골렘 **/

				if (_drainHp > 0) {
					int newHp = _pc.getCurrentHp() + _drainHp;
					_pc.setCurrentHp(newHp);
				}
				/** 조우의 돌골렘 **/
			}
			damageNpcWeaponDurability(); // 무기를 손상시킨다.
			if (_damage > 0)
				_damage = (int) (_damage * 1.20);
			_targetNpc.receiveDamage(_pc, _damage);
		} else if (_calcType == NPC_NPC) {
			if (_targetNpc.getSkillEffectTimerSet().hasSkillEffect(ICE_LANCE)
				|| _targetNpc.getSkillEffectTimerSet().hasSkillEffect(EARTH_BIND)
				|| _targetNpc.getSkillEffectTimerSet().hasSkillEffect(MOB_BASILL) // 바실얼리기데미지0
				|| _targetNpc.getSkillEffectTimerSet().hasSkillEffect(MOB_COCA)) { // //코카얼리기데미지0
				_damage = 0;
			}
			_targetNpc.receiveDamage(_npc, _damage);
		}
	}

	public static double calcMrDefense(int MagicResistance, double dmg) {
		double cc = 0;
		if (MagicResistance <= 19) {
			cc = 0.05;
		} else if (MagicResistance <= 29) {
			cc = 0.07;
		} else if (MagicResistance <= 39) {
			cc = 0.1;
		} else if (MagicResistance <= 49) {
			cc = 0.12;
		} else if (MagicResistance <= 59) {
			cc = 0.17;
		} else if (MagicResistance <= 69) {
			cc = 0.20;
		} else if (MagicResistance <= 79) {
			cc = 0.22;
		} else if (MagicResistance <= 89) {
			cc = 0.25;
		} else if (MagicResistance <= 99) {
			cc = 0.27;
		} else if (MagicResistance <= 110) {
			cc = 0.31;
		} else if (MagicResistance <= 120) {
			cc = 0.32;
		} else if (MagicResistance <= 130) {
			cc = 0.34;
		} else if (MagicResistance <= 140) {
			cc = 0.36;
		} else if (MagicResistance <= 150) {
			cc = 0.38;
		} else if (MagicResistance <= 160) {
			cc = 0.40;
		} else if (MagicResistance <= 170) {
			cc = 0.42;
		} else if (MagicResistance <= 180) {
			cc = 0.44;
		} else if (MagicResistance <= 190) {
			cc = 0.46;
		} else if (MagicResistance <= 200) {
			cc = 0.48;
		} else if (MagicResistance <= 220) {
			cc = 0.49;
		} else {
			cc = 0.51;
		}

		dmg -= dmg * cc;

		if (dmg < 0) {
			dmg = 0;
		}

		return dmg;
	}

	// / * 이블리버스 효과를 구현.

	public void EveilReverse() { // 마족무기 피빠는무기들
		int chance = _random.nextInt(100) + 1;
		if (5 + _weaponEnchant >= chance) { // 확률은 5프로
			_drainHp = 30; // 피를 30빤다는얘기
			_pc.sendPackets(new S_SkillSound(_target.getId(), 8150), true);
			Broadcaster.broadcastPacket(_pc, new S_SkillSound(_target.getId(),
					8150), true);
		}

	}

	public void DrainofEvil() { // 여긴 활
		int chance = _random.nextInt(100) + 1;
		if (5 + _weaponEnchant >= chance) { // 확률은 5프로
			_drainMana = _random.nextInt(5) + 6; // 엠피를 3빤다는애기
			_pc.sendPackets(new S_SkillSound(_target.getId(), 8152), true);
			Broadcaster.broadcastPacket(_pc, new S_SkillSound(_target.getId(),
					8152), true);
		}
	}

	public void DrainofEvil1() { // 여긴 지팡이 키링크
		int chance = _random.nextInt(100) + 1;
		if (12 >= chance) {// 확률은 12프로
			_drainMana = _random.nextInt(5) + 6; // 엠피를 6빤다는애기
			_pc.sendPackets(new S_SkillSound(_target.getId(), 8152), true);
			Broadcaster.broadcastPacket(_pc, new S_SkillSound(_target.getId(),
					8152), true);
		}
	}

	public void actionTaitan(int type) {
		int gfx = 0;
		switch (type) {//
		case 0:// 락
			gfx = 12555;
			break;
		case 1:// 매직
			gfx = 12559;
			break;
		case 2:// 블릿
			gfx = 12557;
			break;
		}
		if (_calcType == PC_PC || _calcType == PC_NPC) {
			_pc.getMoveState().setHeading(CharPosUtil.targetDirection(_pc, _targetX, _targetY)); // 방향세트
			_pc.sendPackets(new S_AttackMissPacket(_pc, _targetId));
			Broadcaster.broadcastPacket(_pc, new S_AttackMissPacket(_pc, _targetId));
			_pc.sendPackets(new S_DoActionGFX(_pc.getId(), ActionCodes.ACTION_Damage));
			Broadcaster.broadcastPacket(_pc, new S_DoActionGFX(_pc.getId(), ActionCodes.ACTION_Damage));
			_targetPc.sendPackets(new S_SkillSound(_target.getId(), gfx));
			// Broadcaster.broadcastPacket(_target, new
			// S_SkillSound(_target.getId(), gfx));
		} else if (_calcType == NPC_PC) {
			int actId = 0;
			_npc.getMoveState().setHeading(
					CharPosUtil.targetDirection(_npc, _targetX, _targetY)); // 방향세트
			if (getActId() > 0) {
				actId = getActId();
			} else {
				actId = ActionCodes.ACTION_Attack;
			}

			if (getGfxId() > 0) {
				Broadcaster.broadcastPacket(_npc,
						new S_UseAttackSkill(_target, _npc.getId(), getGfxId(),
								_targetX, _targetY, actId, 0), true);
			} else {
				Broadcaster.broadcastPacket(_npc, new S_AttackMissPacket(_npc,
						_targetId, actId), true);
			}

			Broadcaster.broadcastPacket(_npc, new S_DoActionGFX(_npc.getId(), ActionCodes.ACTION_Damage), true);
			// Broadcaster.broadcastPacket(_npc, new S_SkillSound(_targetId,
			// gfx), true);
			_targetPc.sendPackets(new S_SkillSound(_target.getId(), gfx));
		}
	}
	/* ■■■■■■■■■■■■■■■ 인페르노 ■■■■■■■■■■■■■■■ */

	// ■■■■ 인페르노의 공격 모션 송신 ■■■■
	public void action인페르노(int step) {
		int gfxid = 0;
		
		if(step == 1)
			gfxid = 17563;
		else if (step == 2)
			gfxid = 17565;
		else if (step == 3)
			gfxid = 17567;
		else
			gfxid = 17560;
		
		if (_calcType == PC_PC || _calcType == PC_NPC) {
			_pc.getMoveState().setHeading(CharPosUtil.targetDirection(_pc, _targetX, _targetY)); // 방향세트
			_pc.sendPackets(new S_AttackMissPacket(_pc, _targetId), true);
			Broadcaster.broadcastPacket(_pc, new S_AttackMissPacket(_pc, _targetId), true);
			_pc.sendPackets(new S_DoActionGFX(_pc.getId(), ActionCodes.ACTION_Damage), true);
			Broadcaster.broadcastPacket(_pc, new S_DoActionGFX(_pc.getId(), ActionCodes.ACTION_Damage), true);
			_pc.sendPackets(new S_SkillSound(_targetId, gfxid), true);
			Broadcaster.broadcastPacket(_pc, new S_SkillSound(_targetId, gfxid), true);
		} else if (_calcType == NPC_PC) {
			int actId = 0;
			_npc.getMoveState().setHeading(CharPosUtil.targetDirection(_npc, _targetX, _targetY)); // 방향세트
			if (getActId() > 0) {
				actId = getActId();
			} else {
				actId = ActionCodes.ACTION_Attack;
			}
			if (getGfxId() > 0) {
				Broadcaster.broadcastPacket(_npc, new S_UseAttackSkill(_target, _npc.getId(), getGfxId(), _targetX, _targetY, actId, 0), true);
			} else {
				Broadcaster.broadcastPacket(_npc, new S_AttackMissPacket(_npc, _targetId, actId), true);
			}
			Broadcaster.broadcastPacket(_npc, new S_DoActionGFX(_npc.getId(), ActionCodes.ACTION_Damage), true);
			Broadcaster.broadcastPacket(_npc, new S_SkillSound(_targetId, gfxid), true);
		} 
	}
	
	// ■■■■ 카운터 바리어의 데미지를 반영 ■■■■
	public void commit인페르노(int step) {
		int damage = calc인페르노Damage();
		damage *= step;
		if (damage == 0) {
			return;
		}
		if (_calcType == PC_PC) {
			_pc.receiveDamage(_targetPc, damage, false);
		} else if (_calcType == PC_NPC) {
			_pc.receiveDamage(_targetNpc, damage, false);
		} else if (_calcType == NPC_PC) {
			_npc.receiveDamage(_targetPc, damage);
		}
	}
	// ●●●● 인페르노 데미지를 산출 ●●●●
	private int calc인페르노Damage() {
		int damage = 0;
		L1ItemInstance weapon = null;
		if (_calcType == NPC_PC || _calcType == PC_PC) {
			weapon = _targetPc.getWeapon();
			if (weapon != null) {
				if (weapon.getItem().getType() == 1) {
					damage = (weapon.getItem().getDmgSmall() + weapon.getEnchantLevel() + weapon.getItem().getDmgModifier());
				}
			}
		} else
			damage = 60 + _random.nextInt(41);
		return damage;
	}
	
	/* ■■■■■■■■■■■■■■■ 카운터 바리어 ■■■■■■■■■■■■■■■ */

	// ■■■■ 카운터 바리어시의 공격 모션 송신 ■■■■
	public void actionCounterBarrier() {
		int gfxid = 10710;
		if (_calcType == PC_PC || _calcType == PC_NPC) {
			if (_calcType == PC_PC){
				if(_targetPc.isBetterang){
					gfxid = 17220;
				}
			}
			if (_calcType == PC_NPC){
				if(_targetNpc.getNpcTemplate().get_npcId() == 78007){
					gfxid = 17220;
				}
			}
			_pc.getMoveState().setHeading(CharPosUtil.targetDirection(_pc, _targetX, _targetY)); // 방향세트
			_pc.sendPackets(new S_AttackMissPacket(_pc, _targetId), true);
			Broadcaster.broadcastPacket(_pc, new S_AttackMissPacket(_pc, _targetId), true);
			_pc.sendPackets(new S_DoActionGFX(_pc.getId(), ActionCodes.ACTION_Damage), true);
			Broadcaster.broadcastPacket(_pc, new S_DoActionGFX(_pc.getId(), ActionCodes.ACTION_Damage), true);
			_pc.sendPackets(new S_SkillSound(_targetId, gfxid), true);
			Broadcaster.broadcastPacket(_pc, new S_SkillSound(_targetId, gfxid), true);
		} else if (_calcType == NPC_PC) {
			if(_targetPc.isBetterang){
				gfxid = 17220;
			}
			int actId = 0;
			_npc.getMoveState().setHeading(CharPosUtil.targetDirection(_npc, _targetX, _targetY)); // 방향세트
			if (getActId() > 0) {
				actId = getActId();
			} else {
				actId = ActionCodes.ACTION_Attack;
			}
			if (getGfxId() > 0) {
				Broadcaster.broadcastPacket(_npc, new S_UseAttackSkill(_target, _npc.getId(), getGfxId(), _targetX, _targetY, actId, 0), true);
			} else {
				Broadcaster.broadcastPacket(_npc, new S_AttackMissPacket(_npc, _targetId, actId), true);
			}
			Broadcaster.broadcastPacket(_npc, new S_DoActionGFX(_npc.getId(), ActionCodes.ACTION_Damage), true);
			Broadcaster.broadcastPacket(_npc, new S_SkillSound(_targetId, gfxid), true);
		} 
	}

	// ■■■■ 모탈바디 발동시의 공격 모션 송신 ■■■■
	public void actionMortalBody() {
		if (_calcType == PC_PC) {
			_pc.getMoveState().setHeading(
					CharPosUtil.targetDirection(_pc, _targetX, _targetY)); // 방향세트

			_pc.sendPackets(new S_DoActionGFX(_pc.getId(),
					ActionCodes.ACTION_Damage));
			Broadcaster.broadcastPacket(_pc, new S_DoActionGFX(_pc.getId(),
					ActionCodes.ACTION_Damage));
			_targetPc.sendPackets(new S_SkillSound(_targetPc.getId(), 6519));
			Broadcaster.broadcastPacket(_targetPc,
					new S_SkillSound(_targetPc.getId(), 6519));

		} else if (_calcType == NPC_PC) {
			_npc.getMoveState().setHeading(
					CharPosUtil.targetDirection(_npc, _targetX, _targetY)); // 방향세트
			Broadcaster.broadcastPacket(_npc, new S_DoActionGFX(_npc.getId(),
					ActionCodes.ACTION_Damage));
			Broadcaster.broadcastPacket(_targetPc,
					new S_SkillSound(_targetPc.getId(), 6519));
		}
	}

	// ■■■■ 상대의 공격에 대해서 카운터 바리어가 유효한가를 판별 ■■■■
	public boolean isShortDistance() {
		boolean isShortDistance = true;
		if (_calcType == PC_PC) {
			if (_weaponType == 20 || _weaponType == 62) { // 활이나 간트렛트
				isShortDistance = false;
			}
		} else if (_calcType == NPC_PC) {
			boolean isLongRange = (_npc.getLocation().getTileLineDistance(new Point(_targetX, _targetY)) > 1);
			int bowActId = _npc.getNpcTemplate().getBowActId();
			// 거리가 2이상, 공격자의 활의 액션 ID가 있는 경우는 원공격
			if (isLongRange && bowActId > 0) {
				isShortDistance = false;
			}
		}
		return isShortDistance;
	}

	// ■■■■ 타이탄 데미지를 반영 ■■■■
	public void commitTaitan(int type) {
		int damage = calcTaitanDamage(type);
		// _pc.sendPackets(new S_SystemMessage("타이탄 반사데미지 :"+damage));
		if (damage == 0) {
			return;
		}
		if (_calcType == PC_PC) {
			_pc.receiveDamage(_targetPc, damage, false);
		} else if (_calcType == PC_NPC) {
			_pc.receiveDamage(_targetNpc, damage, false);
		} else if (_calcType == NPC_PC) {
			_npc.receiveDamage(_targetPc, damage);
		}
	}

	// ■■■■ 카운터 바리어의 데미지를 반영 ■■■■
	public void commitCounterBarrier() {
		int damage = calcCounterBarrierDamage();
		if (damage == 0) {
			return;
		}
		if (_calcType == PC_PC) {
			_pc.receiveDamage(_targetPc, damage, false);
		} else if (_calcType == PC_NPC) {
			_pc.receiveDamage(_targetNpc, damage, false);
		} else if (_calcType == NPC_PC) {
			_npc.receiveDamage(_targetPc, damage);
		}
	}

	// ■■■■ 모탈바디의 데미지를 반영 ■■■■
	public void commitMortalBody() {
		int damage = 0;
		
		damage = (_targetPc.getAC().getAc()/2) * -1;
		
		if (damage < 40) {
			damage = 40;
		}
		
		if (_calcType == PC_PC) {
			_pc.receiveDamage(_targetPc, damage, false);
		} else if (_calcType == NPC_PC) {
			_npc.receiveDamage(_targetPc, damage);
		}
	}

	// ■■■■ 린드 마갑주의 데미지를 반영 ■■■■
	public void commitLindArmor() {
		int damage = calcLindArmorDamage();
		if (damage == 0) {
			return;
		}
		if (_calcType == PC_PC) {
			_pc.receiveDamage(_targetPc, damage, false);
		} else if (_calcType == PC_NPC) {
			_pc.receiveDamage(_targetNpc, damage, false);
		} else if (_calcType == NPC_PC) {
			_npc.receiveDamage(_targetPc, damage);
		}
	}

	// ●●●● 린드 마갑주의 데미지를 산출 ●●●●
	private int calcLindArmorDamage() {
		int damage = 0;
		L1ItemInstance weapon = null;

		if (_calcType == PC_PC) {
			weapon = _pc.getWeapon();
		} else if (_calcType == NPC_PC) {
			weapon = _targetPc.getWeapon();
		} else
			damage = 60 + _random.nextInt(41);
		if (weapon != null) {
			if (weapon.getItem().getType1() == 20
					|| weapon.getItem().getType1() == 62) {
				// (큰 몬스터 타격치 + 추가 타격 옵션+ 인챈트 수치 ) x 2
				damage = (weapon.getItem().getDmgLarge()
						+ weapon.getEnchantLevel() + weapon.getItem()
						.getDmgModifier()) * 2;
			}
		}
		return damage;
	}

	// ●●●● 카운터 바리어의 데미지를 산출 ●●●●
	private int calcTaitanDamage(int type) {
		int damage = 0;
		L1ItemInstance weapon = null;
		weapon = _targetPc.getWeapon();
		if (weapon != null) {
			// (큰 몬스터 타격치 + 추가 타격 옵션+ 인챈트 수치 ) x 2
			damage = (weapon.getItem().getDmgLarge() + weapon.getEnchantLevel() + weapon
					.getItem().getDmgModifier()) * 2;
		}
		return damage;
	}

	// ●●●● 카운터 바리어의 데미지를 산출 ●●●●
	private int calcCounterBarrierDamage() {
		int damage = 0;
		L1ItemInstance weapon = null;
		if (_calcType == NPC_PC || _calcType == PC_PC) {
			weapon = _targetPc.getWeapon();
			if (weapon != null) {
				if (weapon.getItem().getType() == 3) {// (큰 몬스터 타격치 + 추가 타격 옵션+ 인챈트 수치 ) x 2
					damage = (weapon.getItem().getDmgLarge() + weapon.getEnchantLevel() + weapon.getItem().getDmgModifier()) * 2;
				}
			}
		} else
			damage = 60 + _random.nextInt(41);
		return damage;
	}
	
	public void DistroyArmorDurability() {
		int chance = 10;
		L1ItemInstance item = _targetPc.getInventory().getItemEquipped(2, 2);
		if(_calcType == PC_PC){
			if(_pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.DISTROY) && weapon.getItem().getType1() == 24){
				if (item != null){
				if(chance > _random.nextInt(100)+1){
					if((item.getItem().get_ac() * -1) > item.get_durability()){
					_pc.sendPackets(new S_SkillSound(_pc.getId(), 14549), true);
					_targetPc.getAC().addAc(1);
					_targetPc.getInventory().receiveDamage(item);
					_targetPc.sendPackets(new S_OwnCharAttrDef(_targetPc), true);
					_targetPc.sendPackets(new S_OwnCharStatus(_targetPc), true);
					_targetPc.sendPackets(new S_SPMR(_targetPc), true);
					}
					}
				}
			}
		}
	}
	/*
	 * 무기를 손상시킨다. 대NPC의 경우, 손상 확률은10%로 한다. 축복 무기는3%로 한다.
	 */
	private void damageNpcWeaponDurability() {
		int chance = 10;
		int bchance = 5;
		/*
		 * 손상하지 않는 NPC, 맨손, 손상하지 않는 무기 사용, SOF중의 경우 아무것도 하지 않는다.
		 */
		if (_calcType != PC_NPC
				|| _targetNpc.getNpcTemplate().is_hard() == false
				|| _weaponType == 0 || weapon.getItem().get_canbedmg() == 0
				|| _pc.getSkillEffectTimerSet().hasSkillEffect(SOUL_OF_FLAME)) {
			return;
		}
		
		if((weapon.getBless() == 0||weapon.getBless() == 128) && ((_random.nextInt(100) + 1) < bchance)) {
			_pc.sendPackets(new S_ServerMessage(268, weapon.getLogName()), true);
			_pc.getInventory().receiveDamage(weapon);
			_pc.sendPackets(new S_SkillSound(_pc.getId(), 10712), true);
		}
		 
		// 통상의 무기·저주해진 무기
		if ((_weaponBless == 1 || _weaponBless == 2) && ((_random.nextInt(100) + 1) < chance)) { // \f1당신의%0가 손상했습니다.
			_pc.sendPackets(new S_ServerMessage(268, weapon.getLogName()), true);
			_pc.getInventory().receiveDamage(weapon);
			_pc.sendPackets(new S_SkillSound(_pc.getId(), 10712), true);
		}
	}

	private int 룸티스붉귀데미지감소() {
		int damage = 0;
		if (_calcType == NPC_PC || _calcType == PC_PC) {
			L1ItemInstance item = _targetPc.getInventory().checkEquippedItem(500007);
			if (item != null && item.getEnchantLevel() >= 5) {
				if (_random.nextInt(100) < 2 + item.getEnchantLevel() - 5) {
					damage = 20;
					_targetPc.sendPackets(new S_SkillSound(_targetPc.getId(), 12118), true);
				}
			}

			L1ItemInstance item2 = _targetPc.getInventory().checkEquippedItem(502007);
			if (item2 != null && item2.getEnchantLevel() >= 4) {
				if (_random.nextInt(100) < 2 + item2.getEnchantLevel() - 4) {
					damage = 20;
					_targetPc.sendPackets(new S_SkillSound(_targetPc.getId(), 12118), true);
				}
			}
		}
		return damage;
	}
	
	private void 포커스웨이브(L1PcInstance pc, L1Character cha){
		
		int rnd = _random.nextInt(100)+1;
		
		if (pc.타겟아이디 != cha.getId()) {
			if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.포커스웨이브2단)) {
				 int wavetime2 = _pc.getSkillEffectTimerSet().getSkillEffectTimeSec(L1SkillId.포커스웨이브2단);
				pc.getSkillEffectTimerSet().killSkillEffectTimer(L1SkillId.포커스웨이브2단);
				pc.getSkillEffectTimerSet().setSkillEffect(L1SkillId.포커스웨이브, wavetime2 * 1000);
				pc.getMoveState().setBraveSpeed(1);
				pc.sendPackets(new S_SkillBrave(_pc.getId(), 10, wavetime2), true);
				Broadcaster.broadcastPacket(_pc, new S_SkillBrave(_pc.getId(), 10, 0), true);
				pc.sendPackets(new S_SkillSound(pc.getId(), 16540));
				Broadcaster.broadcastPacket(pc, new S_SkillSound(pc.getId(), 16540));
			} else if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.포커스웨이브3단)) {
				 int wavetime2 = _pc.getSkillEffectTimerSet().getSkillEffectTimeSec(L1SkillId.포커스웨이브3단);
				pc.getSkillEffectTimerSet().killSkillEffectTimer(L1SkillId.포커스웨이브3단);
				pc.getSkillEffectTimerSet().setSkillEffect(L1SkillId.포커스웨이브, wavetime2 * 1000);
				pc.getMoveState().setBraveSpeed(1);
				pc.sendPackets(new S_SkillBrave(_pc.getId(), 10, wavetime2), true);
				Broadcaster.broadcastPacket(_pc, new S_SkillBrave(_pc.getId(), 10, 0), true);
				pc.sendPackets(new S_SkillSound(pc.getId(), 16540));
				Broadcaster.broadcastPacket(pc, new S_SkillSound(pc.getId(), 16540));
			}
			pc.타겟아이디 = 0;
		}
		
			if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.포커스웨이브)) {
				if (15 > rnd) {
				int wavetime = _pc.getSkillEffectTimerSet().getSkillEffectTimeSec(L1SkillId.포커스웨이브);
				pc.getSkillEffectTimerSet().killSkillEffectTimer(L1SkillId.포커스웨이브);
				pc.getSkillEffectTimerSet().setSkillEffect(L1SkillId.포커스웨이브2단, wavetime * 1000);
				pc.getMoveState().setBraveSpeed(1);
				pc.sendPackets(new S_SkillBrave(_pc.getId(), 11, wavetime), true);
				Broadcaster.broadcastPacket(_pc, new S_SkillBrave(_pc.getId(), 11, 0), true);
				pc.sendPackets(new S_SkillSound(pc.getId(), 16534));
				Broadcaster.broadcastPacket(pc, new S_SkillSound(pc.getId(), 16534));
				pc.타겟아이디 = cha.getId();
				}
			} else if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.포커스웨이브2단)) {
				if (10 > rnd) {
				int wavetime = _pc.getSkillEffectTimerSet().getSkillEffectTimeSec(L1SkillId.포커스웨이브2단);
				pc.getSkillEffectTimerSet().killSkillEffectTimer(L1SkillId.포커스웨이브2단);
				pc.getSkillEffectTimerSet().setSkillEffect(L1SkillId.포커스웨이브3단, wavetime * 1000);
				pc.getMoveState().setBraveSpeed(1);
				pc.sendPackets(new S_SkillBrave(_pc.getId(), 12, wavetime), true);
				Broadcaster.broadcastPacket(_pc, new S_SkillBrave(_pc.getId(), 12, 0), true);
				pc.sendPackets(new S_SkillSound(pc.getId(), 16533));
				Broadcaster.broadcastPacket(pc, new S_SkillSound(pc.getId(), 16533));
				pc.타겟아이디 = cha.getId();
				}
			}
		}
	
	private int 스냅퍼체반데미지감소() {
		int damage = 0;
		if (_calcType == NPC_PC || _calcType == PC_PC) {
			L1ItemInstance item = _targetPc.getInventory().checkEquippedItem(21248);
			if (item != null && item.getEnchantLevel() >= 7) {
				if (_random.nextInt(100) < 1 + item.getEnchantLevel() - 7) {
					damage = 20;
					_targetPc.sendPackets(new S_SkillSound(_targetPc.getId(), 12118), true);
				}
			}

			L1ItemInstance item2 = _targetPc.getInventory().checkEquippedItem(21252);
			if (item2 != null && item2.getEnchantLevel() >= 6) {
				if (_random.nextInt(100) < 1 + item2.getEnchantLevel() - 6) {
					damage = 20;
					_targetPc.sendPackets(new S_SkillSound(_targetPc.getId(), 12118), true);
				}
			}
		}
		return damage;
	}
}
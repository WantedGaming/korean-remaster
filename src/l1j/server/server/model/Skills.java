package l1j.server.server.model;

import l1j.server.server.model.Instance.L1PcInstance;

public class Skills {

	private L1PcInstance pc;

	public Skills(L1PcInstance temp) {
		pc = temp;
		SCALES = new int[2];
	}

	private boolean aura;

	public boolean isAura() {
		return aura;
	}

	public void setAura(boolean b) {
		this.aura = b;
	}

	private static final int[] SCALES_SKILL = { 185, 190, 195, 197 }; // 각 스킬번호 인식
	private int[] SCALES;

	public int toScalesId(int type) {
		return SCALES[type];
	}

	public void startScales(int skillId) {
		if (isAura()) {
			int num1 = SCALES[0], num2 = SCALES[1]; 
			if (num1 != 0 && num2 != 0) {
				if (pc.getSkillEffectTimerSet().hasSkillEffect(num1)) {
					pc.getSkillEffectTimerSet().removeSkillEffect(num1);
				}
				SCALES[0] = num2;
				SCALES[1] = skillId;
			} else if (num1 != 0 && num2 == 0) {
				SCALES[1] = skillId;
			} else if (num1 == 0 && num2 != 0) {
				SCALES[0] = num2;
				SCALES[1] = 0;
			} else {
				SCALES[0] = skillId;
			}
		} else {
			for (int i = 0; i < SCALES_SKILL.length; i++) {
				if (pc.getSkillEffectTimerSet().hasSkillEffect(SCALES_SKILL[i])) {
					pc.getSkillEffectTimerSet().removeSkillEffect(SCALES_SKILL[i]);
				}
			}
			SCALES[0] = skillId;
		}
	}

	public void endScales(int skillId) {
		if (isAura()) {
			int num1 = SCALES[0], num2 = SCALES[1];
			if (num1 == skillId) {
				SCALES[0] = num2;
				SCALES[1] = 0;
			} else if (num2 == skillId) {
				SCALES[1] = 0;
			}
		} else {
			SCALES[0] = 0;
			SCALES[1] = 0;
		}
	}

}

package l1j.server.server.model.item.function;

import l1j.server.server.DGController;
import l1j.server.server.clientpackets.ClientBasePacket;
import l1j.server.server.datatables.ItemTable;
import l1j.server.server.model.L1Inventory;
import l1j.server.server.model.L1PcInventory;
import l1j.server.server.model.L1World;
import l1j.server.server.model.Instance.L1ItemInstance;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.model.item.L1ItemId;
import l1j.server.server.serverpackets.S_PacketBox;
import l1j.server.server.serverpackets.S_SabuTell;
import l1j.server.server.serverpackets.S_ServerMessage;
import l1j.server.server.serverpackets.S_SystemMessage;
import l1j.server.server.templates.L1EtcItem;
import l1j.server.server.utils.CommonUtil;

public class ItemUtil {

	//-- 여기 검색 하세요.
	//-- 이거에 모두 연동 시켜놨어요.
	private static ItemUtil in;
	public static ItemUtil getIn() {
		if (in == null) {
			in = new ItemUtil();
		}
		return in;
	}
	
	//-- 큰일이네..

	public void toUse(L1PcInstance pc, L1ItemInstance temp, ClientBasePacket cbp) {
		try {
			int id = temp.getItemId();
			int objectId = 0, count = 0;
			
			switch(id) {
			case 391033: {  //-- 지배자의 무기 분해 주문서
				objectId = cbp.readD();
				L1ItemInstance weapon = pc.getInventory().findItemObjId(objectId);
				if (weapon == null) {
					return;
				}
						
				int weaponId = weapon.getItemId(), e = weapon.getEnchantLevel();
				boolean is = (weaponId >= 90085 && weaponId <= 90092);
				
				if (weapon.getItem().getType2() != 1 || weapon.isEquipped() || e <= 9 || !is) {
					//-- 아무일도 일어나지 않았습니다.
					pc.sendPackets(new S_ServerMessage(79));
					return;
				}

				count = (e == 10 ? 1000 : 2000);
				
				if (weapon.getItemId() == 90090) {  //-- 지배자의 섬광도
					count = (count / 2);
				}
				
				pc.getInventory().removeItem(weapon);
				pc.getInventory().removeItem(temp, 1);
				pc.getInventory().storeItem(391037, count);
				pc.sendPackets(new S_SystemMessage("\\aH회득: 지배자의 결정체 (" + count + ")"));
			}
			break;  
			case 3500: {  
				//-- 조건이 맞는다면, 아이템 지급처리를 해주고
				//-- 아이템 삭제를 한다.
				//-- 대충 테스트니깐 집명황 무기 3인챈 129 봉인
				if (isBlessItem(pc, 61, 3, 128, 3)) {
					//-- 아이템 삭제 처리 한다. (상자)
					pc.getInventory().removeItem(temp, 1);
				} 
			}
			break; 
			case 391029: {
				if (pc.getLevel() >= 85)		{						// 아이템 상자
					if (pc.isWarrior()) {
						isBlessItem(pc, 9103,  10, 129,0); // 타이탄의 분노
						isBlessItem(pc, 9103,  10, 129,0); // 타이탄의 분노
						isBlessItem(pc, 21122,  10, 129,0); // 지휘관의투구
						isBlessItem(pc, 9113,  10, 129,0); // 실프의 티셔츠
						isBlessItem(pc, 420112,  10, 129,0); // 발라카스의 완력
						isBlessItem(pc, 20049,  10, 129,0); // 거대 여왕 개미의 금빛 날개
						isBlessItem(pc, 9304,  10, 129,0); // 은기사의 견갑
						isBlessItem(pc, 21259,  10, 129,0); // 완력의 부츠
						isBlessItem(pc, 130220,  10, 129,0); // 격분의장갑
						isBlessItem(pc, 500214,  10, 129,0); // 마법방어각반
						isBlessItem(pc, 9300,  8, 129,0); // 투사의휘장
						isBlessItem(pc, 500007,  8, 129,0); // 룸티스의붉은빛귀걸이
						isBlessItem(pc, 500010,  8, 129,0); // 룸티스의검은빛귀걸이
						isBlessItem(pc, 21249,  8, 129,0); // 스냅퍼의 용사 반지
						isBlessItem(pc, 21249,  8, 129,0); // 스냅퍼의 용사 반지
						isBlessItem(pc, 422209,  8, 129,0); // 신성한 기백의 반지
						isBlessItem(pc, 422209,  8, 129,0); // 신성한 기백의 반지
						isBlessItem(pc, 21258,  8, 129,0); // 투사의목걸이
						isBlessItem(pc, 33910,  8, 129,0); // 신성한 완력의 벨트
					}
					if (pc.isKnight()) {
						isBlessItem(pc, 61 , 10, 129,0); // 타이탄의 분노
						isBlessItem(pc, 9203,  10, 129,0); // 흑기사의면갑

						isBlessItem(pc, 9113,  10, 129,0); // 실프의 티셔츠
						isBlessItem(pc, 420112,  10, 129,0); // 발라카스의 완력
						isBlessItem(pc, 20049,  10, 129,0); // 거대 여왕 개미의 금빛 날개
						isBlessItem(pc, 9304,  10, 129,0); // 은기사의 견갑
						isBlessItem(pc, 21259,  10, 129,0); // 완력의 부츠
						isBlessItem(pc, 130220,  10, 129,0); // 격분의장갑
						isBlessItem(pc, 22110,  10, 129,0); // 화령의가더
						isBlessItem(pc, 500214,  10, 129,0); // 마법방어각반
						isBlessItem(pc, 9300,  8, 129,0); // 투사의휘장
						isBlessItem(pc, 500007,  8, 129,0); // 룸티스의붉은빛귀걸이
						isBlessItem(pc, 500010,  8, 129,0); // 룸티스의검은빛귀걸이
						isBlessItem(pc, 21249,  8, 129,0); // 스냅퍼의 용사 반지
						isBlessItem(pc, 21249,  8, 129,0); // 스냅퍼의 용사 반지
						isBlessItem(pc, 422209,  8, 129,0); // 신성한 기백의 반지
						isBlessItem(pc, 422209,  8, 129,0); // 신성한 기백의 반지
						isBlessItem(pc, 21258,  8, 129,0); // 투사의목걸이
						isBlessItem(pc, 33910,  8, 129,0); // 신성한 완력의 벨트

					}
					if (pc.isDragonknight()) {
						isBlessItem(pc, 9101 , 10, 129,0); // 공포
						isBlessItem(pc, 21122,  10, 129,0); // 지휘관의투구

						isBlessItem(pc, 9113,  10, 129,0); // 실프의 티셔츠
						isBlessItem(pc, 420113,  10, 129,0); // 발라카스의 예지력
						isBlessItem(pc, 20049,  10, 129,0); // 거대 여왕 개미의 금빛 날개
						isBlessItem(pc, 9304,  10, 129,0); // 은기사의 견갑
						isBlessItem(pc, 21259,  10, 129,0); // 완력의 부츠
						isBlessItem(pc, 130220,  10, 129,0); // 격분의장갑
						isBlessItem(pc, 22110,  10, 129,0); // 화령의가더
						isBlessItem(pc, 500214,  10, 129,0); // 마법방어각반
						isBlessItem(pc, 9300,  8, 129,0); // 투사의휘장
						isBlessItem(pc, 500007,  8, 129,0); // 룸티스의붉은빛귀걸이
						isBlessItem(pc, 500010,  8, 129,0); // 룸티스의검은빛귀걸이
						isBlessItem(pc, 21249,  8, 129,0); // 스냅퍼의 용사 반지
						isBlessItem(pc, 21249,  8, 129,0); // 스냅퍼의 용사 반지
						isBlessItem(pc, 422209,  8, 129,0); // 신성한 기백의 반지
						isBlessItem(pc, 422209,  8, 129,0); // 신성한 기백의 반지
						isBlessItem(pc, 21258,  8, 129,0); // 투사의목걸이
						isBlessItem(pc, 33910,  8, 129,0); // 신성한 완력의 벨트
					}
					if (pc.isCrown()) {
						isBlessItem(pc, 12,  10, 129,0); // 칼날
						isBlessItem(pc, 21122,  10, 129,0); // 지휘관의투구
						isBlessItem(pc, 9113,  10, 129,0); // 실프의 티셔츠
						isBlessItem(pc, 420113,  10, 129,0); // 발라카스의 예지력
						isBlessItem(pc, 20049,  10, 129,0); // 거대 여왕 개미의 금빛 날개
						isBlessItem(pc, 9304,  10, 129,0); // 은기사의 견갑
						isBlessItem(pc, 21259,  10, 129,0); // 완력의 부츠
						isBlessItem(pc, 130220,  10, 129,0); // 격분의장갑
						isBlessItem(pc, 21093,  10, 129,0); // 반역자의방패
						isBlessItem(pc, 500214,  10, 129,0); // 마법방어각반
						isBlessItem(pc, 9300,  8, 129,0); // 투사의휘장
						isBlessItem(pc, 500007,  8, 129,0); // 룸티스의붉은빛귀걸이
						isBlessItem(pc, 500010,  8, 129,0); // 룸티스의검은빛귀걸이
						isBlessItem(pc, 21249,  8, 129,0); // 스냅퍼의 용사 반지
						isBlessItem(pc, 21249,  8, 129,0); // 스냅퍼의 용사 반지
						isBlessItem(pc, 422209,  8, 129,0); // 신성한 기백의 반지
						isBlessItem(pc, 422209,  8, 129,0); // 신성한 기백의 반지
						isBlessItem(pc, 21258,  8, 129,0); // 투사의목걸이
						isBlessItem(pc, 33910,  8, 129,0); // 신성한 완력의 벨트
					}
					if (pc.isDarkelf()) {
						isBlessItem(pc, 86,  10, 129,0); // 붉이
						isBlessItem(pc, 21122,  10, 129,0); // 지휘관의투구

						isBlessItem(pc, 9113,  10, 129,0); // 실프의 티셔츠
						isBlessItem(pc, 420113,  10, 129,0); // 발라카스의 예지력
						isBlessItem(pc, 20079,  10, 129,0); // 뱀파이어의 망토
						isBlessItem(pc, 9304,  10, 129,0); // 은기사의 견갑
						isBlessItem(pc, 21259,  10, 129,0); // 완력의 부츠
						isBlessItem(pc, 9200,  10, 129,0); // 수호성파워글로브
						isBlessItem(pc, 22110,  10, 129,0); // 화령의가더
						isBlessItem(pc, 500214,  10, 129,0); // 마법방어각반
						isBlessItem(pc, 9300,  8, 129,0); // 투사의휘장
						isBlessItem(pc, 500007,  8, 129,0); // 룸티스의붉은빛귀걸이
						isBlessItem(pc, 500010,  8, 129,0); // 룸티스의검은빛귀걸이
						isBlessItem(pc, 21249,  8, 129,0); // 스냅퍼의 용사 반지
						isBlessItem(pc, 21249,  8, 129,0); // 스냅퍼의 용사 반지
						isBlessItem(pc, 422209,  8, 129,0); // 신성한 기백의 반지
						isBlessItem(pc, 422209,  8, 129,0); // 신성한 기백의 반지
						isBlessItem(pc, 21258,  8, 129,0); // 투사의목걸이
						isBlessItem(pc, 33910,  8, 129,0); // 신성한 완력의 벨트
					}
					if (pc.isWizard()) {
						isBlessItem(pc, 134,  10, 129,0); // 수결지
						isBlessItem(pc, 21166,  10, 129,0); // 대마법사의 모자

						isBlessItem(pc, 9113,  10, 129,0); // 실프의 티셔츠
						isBlessItem(pc, 20107,  10, 129,0); // 리치로브
						isBlessItem(pc, 20079,  10, 129,0); // 뱀파이어의 망토
						isBlessItem(pc, 9306,  10, 129,0); // 예언자의 견갑
						isBlessItem(pc, 21266,  10, 129,0); // 지식의 부츠
						isBlessItem(pc, 7246,  10, 129,0); // 빛나는 마력의 장갑
						isBlessItem(pc, 22112,  10, 129,0); // 수령의가더
						isBlessItem(pc, 500214,  10, 129,0); // 마법방어각반
						isBlessItem(pc, 9302,  8, 129,0); // 현자의휘장
						isBlessItem(pc, 500007,  8, 129,0); // 룸티스의붉은빛귀걸이
						isBlessItem(pc, 500009,  8, 129,0); // 룸티스의보라빛귀걸이
						isBlessItem(pc, 21246,  8, 129,0); // 스냅퍼의 지혜 반지
						isBlessItem(pc, 21246,  8, 129,0); // 스냅퍼의 지혜 반지
						isBlessItem(pc, 422208,  8, 129,0); // 신성한 마왕의 반지
						isBlessItem(pc, 422208,  8, 129,0); // 신성한 마왕의 반지
						isBlessItem(pc, 21260,  8, 129,0); // 현자의목걸이
						isBlessItem(pc, 33912,  8, 129,0); // 신성한 지식의 벨트
					}
					if (pc.isIllusionist()) {
						isBlessItem(pc, 9102,  10, 129,0); // 절망
						isBlessItem(pc, 21122,  10, 129,0); // 지휘관

						isBlessItem(pc, 9113,  10, 129,0); // 실프의 티셔츠
						isBlessItem(pc, 20107,  10, 129,0); // 리치로브
						isBlessItem(pc, 20079,  10, 129,0); // 뱀파이어의 망토
						isBlessItem(pc, 9306,  10, 129,0); // 예언자의 견갑
						isBlessItem(pc, 21266,  10, 129,0); // 지식의 부츠
						isBlessItem(pc, 7246,  10, 129,0); // 빛나는 마력의 장갑
						isBlessItem(pc, 22112,  10, 129,0); // 수령의가더
						isBlessItem(pc, 500214,  10, 129,0); // 마법방어각반
						isBlessItem(pc, 9302,  8, 129,0); // 현자의휘장
						isBlessItem(pc, 500007,  8, 129,0); // 룸티스의붉은빛귀걸이
						isBlessItem(pc, 500009,  8, 129,0); // 룸티스의보라빛귀걸이
						isBlessItem(pc, 21246,  8, 129,0); // 스냅퍼의 지혜 반지
						isBlessItem(pc, 21246,  8, 129,0); // 스냅퍼의 지혜 반지
						isBlessItem(pc, 422208,  8, 129,0); // 신성한 마왕의 반지
						isBlessItem(pc, 422208,  8, 129,0); // 신성한 마왕의 반지
						isBlessItem(pc, 21260,  8, 129,0); // 현자의목걸이
						isBlessItem(pc, 33912,  8, 129,0); // 신성한 지식의 벨트
					}
					if (pc.isElf()) {
						isBlessItem(pc, 9100,  10, 129,0); // 격노
						isBlessItem(pc, 20017,  10, 129,0); // 머미로드의왕관

						isBlessItem(pc, 9113,  10, 129,0); // 실프의 티셔츠
						isBlessItem(pc, 420114,  10, 129,0); // 발라카스의 인내력
						isBlessItem(pc, 20050,  10, 129,0); // 거대 여왕 개미의 은빛 날개
						isBlessItem(pc, 9305,  10, 129,0); // 사냥꾼의 견갑
						isBlessItem(pc, 21265,  10, 129,0); // 민첩의 부츠
						isBlessItem(pc, 9201 , 10, 129,0); // 수호성의 활 골무
						isBlessItem(pc, 2211 , 10, 129,0); // 풍령의가더
						isBlessItem(pc, 500214,  10, 129,0); // 마법방어각반
						isBlessItem(pc, 9301,  8, 129,0); // 명궁의휘장
						isBlessItem(pc, 500007,  8, 129,0); // 룸티스의붉은빛귀걸이
						isBlessItem(pc, 500010, 8, 129,0); // 룸티스의검은빛귀걸이
						isBlessItem(pc, 21249,  8, 129,0); // 스냅퍼의 용사 반지
						isBlessItem(pc, 21249,  8, 129,0); // 스냅퍼의 용사 반지
						isBlessItem(pc, 22267,  8, 129,0); // 신성한 일격의 반지
						isBlessItem(pc, 22267,  8, 129,0); // 신성한 일격의 반지
						isBlessItem(pc, 21268,  8, 129,0); // 빛나는 사이하의 목걸이
						isBlessItem(pc, 20028, 4, 129,0);
					}
					pc.getInventory().removeItem(temp, 1);
				}else {
					pc.sendPackets(new S_SystemMessage("85레벨보다 낮습니다."), true);

				}
			}
			break;

			case 6000036: {
				if (pc.isWarrior()) {
					isBlessItem(pc, 90090,  10, 0,0); // 타이탄의 분노
					isBlessItem(pc, 90090,  10, 0,0); // 타이탄의 분노
					isBlessItem(pc, 21122,  10, 1,0); // 지휘관의투구
					isBlessItem(pc, 9113,  10, 1,0); // 실프의 티셔츠
					isBlessItem(pc, 420112,  10, 1,0); // 발라카스의 완력
					isBlessItem(pc, 20049,  10, 1,0); // 거대 여왕 개미의 금빛 날개
					isBlessItem(pc, 9304,  10, 1,0); // 은기사의 견갑
					isBlessItem(pc, 21259,  10, 1,0); // 완력의 부츠
					isBlessItem(pc, 130220,  10, 1,0); // 격분의장갑
					isBlessItem(pc, 500214,  10, 1,0); // 마법방어각반
					isBlessItem(pc, 9300,  8, 1,0); // 투사의휘장
					isBlessItem(pc, 502010,  8, 0,0); // 룸티스의검은빛
					isBlessItem(pc, 502007, 8, 0,0); // 룸티스의붉은빛
					isBlessItem(pc, 21253,  8, 0,0); // 스냅퍼의 용사 반지
					isBlessItem(pc, 21253,  8, 0,0); // 스냅퍼의 용사 반지
					isBlessItem(pc, 222242,  8, 128,0); // 지배자귀걸이
					isBlessItem(pc, 222240,  8, 128,0); // 지배자의 반지
					isBlessItem(pc, 222240,  8, 128,0); // 지배자의 반지
					isBlessItem(pc, 222241,  8, 128,0); // 지배자의 목걸이
					isBlessItem(pc, 2222383, 8, 128,0); // 지배자의 벨트
				}
				if (pc.isKnight()) {
					isBlessItem(pc, 90086 , 10, 0,0); // 타이탄의 분노
					isBlessItem(pc, 9203,  10, 1,0); // 흑기사의면갑

					isBlessItem(pc, 9113,  10, 1,0); // 실프의 티셔츠
					isBlessItem(pc, 420112,  10, 1,0); // 발라카스의 완력
					isBlessItem(pc, 20049,  10, 1,0); // 거대 여왕 개미의 금빛 날개
					isBlessItem(pc, 9304,  10, 1,0); // 은기사의 견갑
					isBlessItem(pc, 21259,  10, 1,0); // 완력의 부츠
					isBlessItem(pc, 130220,  10, 1,0); // 격분의장갑
					isBlessItem(pc, 22110,  10, 1,0); // 화령의가더
					isBlessItem(pc, 500214,  10, 1,0); // 마법방어각반
					isBlessItem(pc, 9300,  8, 1,0); // 투사의휘장
					isBlessItem(pc, 502010,  8, 0,0); // 룸티스의검은빛
					isBlessItem(pc, 502007, 8, 0,0); // 룸티스의붉은빛
					isBlessItem(pc, 21253,  8, 0,0); // 스냅퍼의 용사 반지
					isBlessItem(pc, 21253,  8, 0,0); // 스냅퍼의 용사 반지
					isBlessItem(pc, 222242,  8, 128,0); // 지배자귀걸이
					isBlessItem(pc, 222240,  8, 128,0); // 지배자의 반지
					isBlessItem(pc, 222240,  8, 128,0); // 지배자의 반지
					isBlessItem(pc, 222241,  8, 128,0); // 지배자의 목걸이
					isBlessItem(pc, 2222383, 8, 128,0); // 지배자의 벨트

				}
				if (pc.isDragonknight()) {
					isBlessItem(pc, 90091 , 10, 0,0); // 공포
					isBlessItem(pc, 21122,  10, 1,0); // 지휘관의투구

					isBlessItem(pc, 9113,  10, 1,0); // 실프의 티셔츠
					isBlessItem(pc, 420113,  10, 1,0); // 발라카스의 예지력
					isBlessItem(pc, 20049,  10, 1,0); // 거대 여왕 개미의 금빛 날개
					isBlessItem(pc, 9304,  10, 1,0); // 은기사의 견갑
					isBlessItem(pc, 21259,  10, 1,0); // 완력의 부츠
					isBlessItem(pc, 130220,  10, 1,0); // 격분의장갑
					isBlessItem(pc, 22110,  10, 1,0); // 화령의가더
					isBlessItem(pc, 500214,  10, 1,0); // 마법방어각반
					isBlessItem(pc, 9300,  8, 1,0); // 투사의휘장
					isBlessItem(pc, 502010,  8, 0,0); // 룸티스의검은빛
					isBlessItem(pc, 502007, 8, 0,0); // 룸티스의붉은빛
					isBlessItem(pc, 21253,  8, 0,0); // 스냅퍼의 용사 반지
					isBlessItem(pc, 21253,  8, 0,0); // 스냅퍼의 용사 반지
					isBlessItem(pc, 222242,  8, 128,0); // 지배자귀걸이
					isBlessItem(pc, 222240,  8, 128,0); // 지배자의 반지
					isBlessItem(pc, 222240,  8, 128,0); // 지배자의 반지
					isBlessItem(pc, 222241,  8, 128,0); // 지배자의 목걸이
					isBlessItem(pc, 2222383, 8, 128,0); // 지배자의 벨트
				}
				if (pc.isCrown()) {
					isBlessItem(pc, 90092,  10, 0,0); // 칼날
					isBlessItem(pc, 21122,  10, 1,0); // 지휘관의투구
					isBlessItem(pc, 9113,  10, 1,0); // 실프의 티셔츠
					isBlessItem(pc, 420113,  10, 1,0); // 발라카스의 예지력
					isBlessItem(pc, 20049,  10, 1,0); // 거대 여왕 개미의 금빛 날개
					isBlessItem(pc, 9304,  10, 1,0); // 은기사의 견갑
					isBlessItem(pc, 21259,  10, 1,0); // 완력의 부츠
					isBlessItem(pc, 130220,  10, 1,0); // 격분의장갑
					isBlessItem(pc, 21093,  10, 1,0); // 반역자의방패
					isBlessItem(pc, 500214,  10, 1,0); // 마법방어각반
					isBlessItem(pc, 9300,  8, 1,0); // 투사의휘장
					isBlessItem(pc, 502010,  8, 0,0); // 룸티스의검은빛
					isBlessItem(pc, 502007, 8, 0,0); // 룸티스의붉은빛
					isBlessItem(pc, 21253,  8, 0,0); // 스냅퍼의 용사 반지
					isBlessItem(pc, 21253,  8, 0,0); // 스냅퍼의 용사 반지
					isBlessItem(pc, 222242,  8, 128,0); // 지배자귀걸이
					isBlessItem(pc, 222240,  8, 128,0); // 지배자의 반지
					isBlessItem(pc, 222240,  8, 128,0); // 지배자의 반지
					isBlessItem(pc, 222241,  8, 128,0); // 지배자의 목걸이
					isBlessItem(pc, 2222383, 8, 128,0); // 지배자의 벨트
				}
				if (pc.isDarkelf()) {
					isBlessItem(pc, 90087,  10, 0,0); // 붉이
					isBlessItem(pc, 21122,  10, 1,0); // 지휘관의투구

					isBlessItem(pc, 9113,  10, 1,0); // 실프의 티셔츠
					isBlessItem(pc, 420113,  10, 1,0); // 발라카스의 예지력
					isBlessItem(pc, 20079,  10, 1,0); // 뱀파이어의 망토
					isBlessItem(pc, 9304,  10, 1,0); // 은기사의 견갑
					isBlessItem(pc, 21259,  10, 1,0); // 완력의 부츠
					isBlessItem(pc, 9200,  10, 1,0); // 수호성파워글로브
					isBlessItem(pc, 22110,  10, 1,0); // 화령의가더
					isBlessItem(pc, 500214,  10, 1,0); // 마법방어각반
					isBlessItem(pc, 9300,  8, 1,0); // 투사의휘장
					isBlessItem(pc, 502010,  8, 0,0); // 룸티스의검은빛
					isBlessItem(pc, 502007, 8, 0,0); // 룸티스의붉은빛
					isBlessItem(pc, 21253,  8, 0,0); // 스냅퍼의 용사 반지
					isBlessItem(pc, 21253,  8, 0,0); // 스냅퍼의 용사 반지
					isBlessItem(pc, 222242,  8, 128,0); // 지배자귀걸이
					isBlessItem(pc, 222240,  8, 128,0); // 지배자의 반지
					isBlessItem(pc, 222240,  8, 128,0); // 지배자의 반지
					isBlessItem(pc, 222241,  8, 128,0); // 지배자의 목걸이
					isBlessItem(pc, 2222383, 8, 128,0); // 지배자의 벨트
				}
				if (pc.isWizard()) {
					isBlessItem(pc, 90085,  10, 0,0); // 수결지
					isBlessItem(pc, 21166,  10, 1,0); // 대마법사의 모자
					isBlessItem(pc, 9113,  10, 1,0); // 실프의 티셔츠
					isBlessItem(pc, 20107,  10, 1,0); // 리치로브
					isBlessItem(pc, 20079,  10, 1,0); // 뱀파이어의 망토
					isBlessItem(pc, 9306,  10, 1,0); // 예언자의 견갑
					isBlessItem(pc, 21266,  10, 1,0); // 지식의 부츠
					isBlessItem(pc, 7246,  10, 1,0); // 빛나는 마력의 장갑
					isBlessItem(pc, 22112,  10, 1,0); // 수령의가더
					isBlessItem(pc, 500214,  10, 1,0); // 마법방어각반
					isBlessItem(pc, 9302,  8, 1,0); // 현자의휘장
					isBlessItem(pc, 502007,  8, 0,0); // 룸티스의붉은빛귀걸이
					isBlessItem(pc, 502009,  8, 0,0); // 룸티스의보라빛귀걸이
					isBlessItem(pc, 21250,  8, 0,0); // 스냅퍼의 지혜 반지
					isBlessItem(pc, 21250,  8, 0,0); // 스냅퍼의 지혜 반지
					isBlessItem(pc, 222242,  8, 128,0); // 지배자귀걸이
					isBlessItem(pc, 222240,  8, 128,0); // 지배자의 반지
					isBlessItem(pc, 222240,  8, 128,0); // 지배자의 반지
					isBlessItem(pc, 222241,  8, 128,0); // 지배자의 목걸이
					isBlessItem(pc, 2222383, 8, 128,0); // 지배자의 벨트
				}
				if (pc.isIllusionist()) {
					isBlessItem(pc, 90089,  10, 0,0); // 절망
					isBlessItem(pc, 21122,  10, 1,0); // 지휘관
					isBlessItem(pc, 9113,  10, 1,0); // 실프의 티셔츠
					isBlessItem(pc, 20107,  10, 1,0); // 리치로브
					isBlessItem(pc, 20079,  10, 1,0); // 뱀파이어의 망토
					isBlessItem(pc, 9306,  10, 1,0); // 예언자의 견갑
					isBlessItem(pc, 21266,  10, 1,0); // 지식의 부츠
					isBlessItem(pc, 7246,  10, 1,0); // 빛나는 마력의 장갑
					isBlessItem(pc, 22112,  10, 1,0); // 수령의가더
					isBlessItem(pc, 500214,  10, 1,0); // 마법방어각반
					isBlessItem(pc, 9302,  8, 1,0); // 현자의휘장
					isBlessItem(pc, 502007,  8, 0,0); // 룸티스의붉은빛귀걸이
					isBlessItem(pc, 502009,  8, 0,0); // 룸티스의보라빛귀걸이
					isBlessItem(pc, 21250,  8, 0,0); // 스냅퍼의 지혜 반지
					isBlessItem(pc, 21250,  8, 0,0); // 스냅퍼의 지혜 반지
					isBlessItem(pc, 222242,  8, 128,0); // 지배자귀걸이
					isBlessItem(pc, 222240,  8, 128,0); // 지배자의 반지
					isBlessItem(pc, 222240,  8, 128,0); // 지배자의 반지
					isBlessItem(pc, 222241,  8, 128,0); // 지배자의 목걸이
					isBlessItem(pc, 2222383, 8, 128,0); // 지배자의 벨트
				}
				if (pc.isElf()) {
					isBlessItem(pc, 90088,  10, 0,0); // 격노
					isBlessItem(pc, 20017,  10, 1,0); // 머미로드의왕관

					isBlessItem(pc, 9113,  10, 1,0); // 실프의 티셔츠
					isBlessItem(pc, 420114,  10, 1,0); // 발라카스의 인내력
					isBlessItem(pc, 20050,  10, 1,0); // 거대 여왕 개미의 은빛 날개
					isBlessItem(pc, 9305,  10, 1,0); // 사냥꾼의 견갑
					isBlessItem(pc, 21265,  10, 1,0); // 민첩의 부츠
					isBlessItem(pc, 9201 , 10, 1,0); // 수호성의 활 골무
					isBlessItem(pc, 2211 , 10, 1,0); // 풍령의가더
					isBlessItem(pc, 500214,  10, 1,0); // 마법방어각반
					isBlessItem(pc, 9301,  8, 1,0); // 명궁의휘장
					isBlessItem(pc, 502010,  8, 0,0); // 룸티스의검은빛
					isBlessItem(pc, 502007, 8, 0,0); // 룸티스의붉은빛
					isBlessItem(pc, 21253,  8, 0,0); // 스냅퍼의 용사 반지
					isBlessItem(pc, 21253,  8, 0,0); // 스냅퍼의 용사 반지
					isBlessItem(pc, 222242,  8, 128,0); // 지배자귀걸이
					isBlessItem(pc, 222240,  8, 128,0); // 지배자의 반지
					isBlessItem(pc, 222240,  8, 128,0); // 지배자의 반지
					isBlessItem(pc, 222241,  8, 128,0); // 지배자의 목걸이
					isBlessItem(pc, 2222383, 8, 128,0); // 지배자의 벨트
				}
				pc.getInventory().removeItem(temp, 1);
		
		}
		break;
			case 6000037: {//2차패키
				if (pc.isWarrior()) {
					isBlessItem(pc, 90090,  12, 128,3); // 타이탄의 분노
					isBlessItem(pc, 90090,  12, 128,3); // 타이탄의 분노
				}
				if (pc.isKnight()) {
					isBlessItem(pc, 90086 , 12, 128,3); // 타이탄의 분노
				}
				if (pc.isDragonknight()) {
					isBlessItem(pc, 90091 , 12, 128,3); // 공포
				}
				if (pc.isCrown()) {
					isBlessItem(pc, 90092,  12, 128,3); // 칼날
				}
				if (pc.isDarkelf()) {
					isBlessItem(pc, 90087,  12, 128,3); // 붉이
				}
				if (pc.isWizard()) {
					isBlessItem(pc, 90085,  12, 128,3); // 수결지
				}
				if (pc.isIllusionist()) {
					isBlessItem(pc, 90089,  12, 128,3); // 절망
				}
				if (pc.isElf()) {
					isBlessItem(pc, 90088,  12, 128,3); // 격노
				}
				pc.getInventory().removeItem(temp, 1);
		
		}
		break;
			case 6000038: {//3차패키(룬)
				if (isBlessItem(pc, 222248,  12, 128,3)) {
					//-- 아이템 삭제 처리 한다. (상자)
					pc.getInventory().removeItem(temp, 1);
				} 
			}
			break;
			case 6000048: {//VIP혼돈의투구상자
				if (isBlessItem(pc, 20048,  9, 128,3)) {
					//-- 아이템 삭제 처리 한다. (상자)
					pc.getInventory().removeItem(temp, 1);
				} 
			}
			break;
			case 6000051: {//VIP티셔츠상자
				if (isBlessItem(pc, 222251,  12, 128,3)) {
					//-- 아이템 삭제 처리 한다. (상자)
					pc.getInventory().removeItem(temp, 1);
				} 
			}
			break;
			case 6000052: {//VIP각반상자
				if (isBlessItem(pc, 222250,  12, 128,3)) {
					//-- 아이템 삭제 처리 한다. (상자)
					pc.getInventory().removeItem(temp, 1);
				} 
			}
			break;
			case 6000053: {//VIP가더상자
				if (isBlessItem(pc, 222249,  12, 128,3)) {
					//-- 아이템 삭제 처리 한다. (상자)
					pc.getInventory().removeItem(temp, 1);
				} 
			}
		break;
			case 6000054: {//2차패키
				if (pc.isWarrior()) {
					isBlessItem(pc, 90090,  14, 128,3); // 타이탄의 분노
					isBlessItem(pc, 90090,  14, 128,3); // 타이탄의 분노
				}
				if (pc.isKnight()) {
					isBlessItem(pc, 90086 , 14, 128,3); // 타이탄의 분노
				}
				if (pc.isDragonknight()) {
					isBlessItem(pc, 90091 , 14, 128,3); // 공포
				}
				if (pc.isCrown()) {
					isBlessItem(pc, 90092,  14, 128,3); // 칼날
				}
				if (pc.isDarkelf()) {
					isBlessItem(pc, 90087,  14, 128,3); // 붉이
				}
				if (pc.isWizard()) {
					isBlessItem(pc, 90085,  14, 128,3); // 수결지
				}
				if (pc.isIllusionist()) {
					isBlessItem(pc, 90089,  14, 128,3); // 절망
				}
				if (pc.isElf()) {
					isBlessItem(pc, 90088,  14, 128,3); // 격노
				}
				pc.getInventory().removeItem(temp, 1);
		
		}
		break;
		
		
			case 6000049: {//+25 VIP 망토
				if (pc.isWarrior()) {
					isBlessItem(pc, 20049,  25, 128,3); // 거대 여왕 개미의 금빛 날개
                }
				if (pc.isKnight()) {
					isBlessItem(pc, 20049,  25, 128,3); // 거대 여왕 개미의 금빛 날개
				}
				if (pc.isDragonknight()) {
					isBlessItem(pc, 20049,  25, 128,3); // 거대 여왕 개미의 금빛 날개
				}
				if (pc.isCrown()) {
					isBlessItem(pc, 20049,  25, 128,3); // 거대 여왕 개미의 금빛 날개
				}
				if (pc.isDarkelf()) {
					isBlessItem(pc, 20079,  25, 128,3); // 거대 여왕 개미의 은빛 날개
				}
				if (pc.isWizard()) {
					isBlessItem(pc, 20050,  25, 128,3); // 거대 여왕 개미의 은빛 날개
				}
				if (pc.isIllusionist()) {
					isBlessItem(pc, 20050,  25, 128,3); // 거대 여왕 개미의 은빛 날개
				}
				if (pc.isElf()) {
					isBlessItem(pc, 20050,  25, 128,3); // 거대 여왕 개미의 은빛 날개
				}
				pc.getInventory().removeItem(temp, 1);
		
		}
		break;
			case 6000050: {//+25 VIP 갑옷
				if (pc.isWarrior()) {
					isBlessItem(pc, 420104,  25, 128,3); // 거대 여왕 개미의 금빛 날개
                }
				if (pc.isKnight()) {
					isBlessItem(pc, 420104,  25, 128,3); // 거대 여왕 개미의 금빛 날개
				}
				if (pc.isDragonknight()) {
					isBlessItem(pc, 420105,  25, 128,3); // 거대 여왕 개미의 금빛 날개
				}
				if (pc.isCrown()) {
					isBlessItem(pc, 420105,  25, 128,3); // 거대 여왕 개미의 금빛 날개
				}
				if (pc.isDarkelf()) {
					isBlessItem(pc, 420105,  25, 128,3); // 거대 여왕 개미의 은빛 날개
				}
				if (pc.isWizard()) {
					isBlessItem(pc, 420107,  25, 128,3); // 거대 여왕 개미의 은빛 날개
				}
				if (pc.isIllusionist()) {
					isBlessItem(pc, 420107,  25, 128,3); // 거대 여왕 개미의 은빛 날개
				}
				if (pc.isElf()) {
					isBlessItem(pc, 420105,  25, 128,3); // 거대 여왕 개미의 은빛 날개
				}
				pc.getInventory().removeItem(temp, 1);
		
		}
		break;
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	/**
	 * 상자 개봉시 축복 상태 오플 처리 
	 * itemid = SQL 아이템 번호
	 * enchant = 말그대로 인챈트
	 * bless = 129 = 봉인 상태 
	 * blessLevel = 축복 시스템.
	 */
	


	private boolean isBlessItem(L1PcInstance pc, int itemid, int enchant, int bless, int blessLevel) {
		L1ItemInstance item = ItemTable.getInstance().createItem(itemid);
		if (item == null) {
			return false;
		} else if (item.getItem().getType2() == 1 && item.getItem().getType2() == 2) {
			pc.sendPackets(new S_SystemMessage("획득: 지급될 아이템의 조건이 맞지 않습니다."));
			return false;
		}
		
		item.setCount(1);
		item.setEnchantLevel(enchant);
		item.setIdentified(true);

		//-- PC 인벤토리에 아이템 지급 상태에서
		pc.getInventory().storeItem(item);

		//-- 아이템 봉인 상태와, 아이템 축복 인챈트를 주고,
		item.setBless(bless);
		item.setBlessEnchantLevel(blessLevel);
		//-- 업데이트 처리를 다시 한번 한겁니다.
		pc.sendPackets(new S_PacketBox(S_PacketBox.ITEM_, item, 0));
		pc.getInventory().updateItem(item, L1PcInventory.COL_IS_ID);
		pc.getInventory().saveItem(item, L1PcInventory.COL_IS_ID);
		//-- 나중에 수정바람..
		pc.sendPackets(new S_SystemMessage("획득: 아이템을 얻었습니다. -> " + item.getLogName()));
		return true;
	}

	private boolean createNewItem3(L1PcInstance pc, int item_id, int count, int EnchantLevel) {
		return createNewItem3(pc, item_id, count, EnchantLevel, 1, 0);
	}

	private boolean createNewItem3(L1PcInstance pc, int item_id, int count, int EnchantLevel, int bless, int SpiritIn) {

		L1ItemInstance item = ItemTable.getInstance().createItem(item_id);

		item.setCount(count);
		item.setEnchantLevel(EnchantLevel);
		item.setIdentified(true);
		item.setRegistLevel(SpiritIn);
		if (item != null) {
			if (pc.getInventory().checkAddItem(item, count) == L1Inventory.OK) {
				pc.getInventory().storeItem(item);
				item.setBless(bless);
				pc.getInventory().updateItem(item, L1PcInventory.COL_BLESS);
				pc.getInventory().saveItem(item, L1PcInventory.COL_BLESS);
			} else { // 가질 수 없는 경우는 지면에 떨어뜨리는 처리의 캔슬은 하지 않는다(부정 방지)
				L1World.getInstance().getInventory(pc.getX(), pc.getY(), pc.getMapId()).storeItem(item);
			}

			pc.sendPackets(new S_ServerMessage(403, item.getLogName()), true); // %0를 손에 넣었습니다.
			return true;
		} else {
			return false;
		}
	}


}
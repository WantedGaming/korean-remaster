package l1j.server.server.model.item.function;

import l1j.server.server.clientpackets.ClientBasePacket;
import l1j.server.server.datatables.NpcSpawnTable;
import l1j.server.server.model.CharPosUtil;
import l1j.server.server.model.L1Character;
import l1j.server.server.model.L1ItemDelay;
import l1j.server.server.model.L1NpcDeleteTimer;
import l1j.server.server.model.L1Object;
import l1j.server.server.model.L1PcInventory;
import l1j.server.server.model.L1World;
import l1j.server.server.model.Instance.L1ItemInstance;
import l1j.server.server.model.Instance.L1MonsterInstance;
import l1j.server.server.model.Instance.L1NpcInstance;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.serverpackets.S_SystemMessage;
import l1j.server.server.templates.L1EtcItem;
import l1j.server.server.templates.L1Item;


@SuppressWarnings("serial")

public class NpcDeleteWand extends L1ItemInstance {

	public NpcDeleteWand(L1Item item) {
		super(item);
	}

@Override
public void clickItem(L1Character cha, ClientBasePacket packet) {
	if (cha instanceof L1PcInstance) {
		L1PcInstance pc = (L1PcInstance) cha;
		L1ItemInstance useItem = pc.getInventory().getItem(this.getId());
		int spellsc_objid = 0;
		int spellsc_x = 0;
		int spellsc_y = 0;
		spellsc_objid = packet.readD();
		spellsc_x = packet.readH();
		spellsc_y = packet.readH();
		pc.cancelAbsoluteBarrier();
		int itemId = this.getItemId(); 
		int delay_id = 0;
		if (itemId == 6102) { 
			delay_id = ((L1EtcItem) useItem.getItem()).get_delayid();
		}

		if (delay_id != 0) { 
			if (pc.hasItemDelay(delay_id) == true) {
				return;
			}
		}

		L1Object target = L1World.getInstance().findObject(spellsc_objid);
		int heding = CharPosUtil.targetDirection(pc, spellsc_x, spellsc_y);
		pc.getMoveState().setHeading(heding);
		if (target != null) {
			doWandAction(pc, target);
		} else {
		}
		if (itemId == 6102) {
			L1ItemDelay.onItemUse(pc, useItem); 
				}
			}
		}

	private void doWandAction(L1PcInstance user, L1Object target) {
		if (target instanceof L1MonsterInstance) {
			L1MonsterInstance mob = (L1MonsterInstance) target;
			mob.deleteMe();
		} else if (target instanceof L1NpcInstance) {
			L1NpcInstance npc = (L1NpcInstance) target;
			NpcSpawnTable.getInstance().removeSpawn(npc);
			npc.setRespawn(false);
			new L1NpcDeleteTimer(npc, 2 * 1000).begin();
			user.sendPackets(new S_SystemMessage(npc.getNpcId() + npc.getName() + "을(를) 2초 뒤에 삭제 합니다."));
			}
		}
	}
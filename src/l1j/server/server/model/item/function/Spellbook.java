/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */

package l1j.server.server.model.item.function;

import l1j.server.server.clientpackets.ClientBasePacket;
import l1j.server.server.datatables.SkillsTable;
import l1j.server.server.model.Broadcaster;
import l1j.server.server.model.L1Character;
import l1j.server.server.model.L1ItemDelay;
import l1j.server.server.model.Instance.L1ItemInstance;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.model.item.L1ItemId;
import l1j.server.server.serverpackets.S_ACTION_UI;
import l1j.server.server.serverpackets.S_AddSkill;
import l1j.server.server.serverpackets.S_NewUI;
import l1j.server.server.serverpackets.S_OwnCharAttrDef;
import l1j.server.server.serverpackets.S_SPMR;
import l1j.server.server.serverpackets.S_ServerMessage;
import l1j.server.server.serverpackets.S_SkillSound;
import l1j.server.server.serverpackets.S_SystemMessage;
import l1j.server.server.templates.L1EtcItem;
import l1j.server.server.templates.L1Item;
import l1j.server.server.templates.L1Skills;
import l1j.server.server.types.Point;

@SuppressWarnings("serial")
public class Spellbook extends L1ItemInstance {

	public Spellbook(L1Item item) {
		super(item);
	}

	@Override
	public void clickItem(L1Character cha, ClientBasePacket packet) {
		try {
			if (cha instanceof L1PcInstance) {
				L1PcInstance pc = (L1PcInstance) cha;
				L1ItemInstance useItem = pc.getInventory().getItem(this.getId());
				int itemId = useItem.getItemId();
				int delay_id = 0;
				if (useItem.getItem().getType2() == 0) { // 종별：그 외의 아이템
					delay_id = ((L1EtcItem) useItem.getItem()).get_delayid();
				}
				if (delay_id != 0) { // 지연 설정 있어
					if (pc.hasItemDelay(delay_id) == true) {
						return;
					}
				}
				if (itemId > 40169 && itemId < 40226
					|| itemId >= 45000 && itemId <= 45022
					|| (itemId == 140186 || itemId == 140196
					|| itemId == 140198 || itemId == 140204
					|| itemId == 140205 || itemId == 140210 || itemId == 140219)) { // 마법서
					useSpellBook(pc, useItem, itemId);
				} else if ((itemId > 40225 && itemId < 40232) || itemId == 60348 || itemId == 72000 || itemId == 72029
						|| itemId == 300090 || itemId == 300091) {
					if (pc.isCrown() || pc.isGm()) {
						if (itemId == 40226) {
							if (pc.getLevel() >= 50)
								SpellBook4(pc, useItem);
							else
								pc.sendPackets(new S_ServerMessage(3321, "50"), true);
						} else if (itemId == 40228) {
							if (pc.getLevel() >= 55)
								SpellBook4(pc, useItem);
							else
								pc.sendPackets(new S_ServerMessage(3321, "55"), true);
						} else if (itemId == 40227) {
							if (pc.getLevel() >= 60)
								SpellBook4(pc, useItem);
							else
								pc.sendPackets(new S_ServerMessage(3321, "60"), true);
						} else if (itemId == 40231) {
							if (pc.getLevel() >= 65)
								SpellBook4(pc, useItem);
							else
								pc.sendPackets(new S_ServerMessage(3321, "65"), true);
						} else if (itemId == 40230) {
							if (pc.getLevel() >= 70)
								SpellBook4(pc, useItem);
							else
								pc.sendPackets(new S_ServerMessage(3321, "70"), true);
						} else if (itemId == 40229) {
							if (pc.getLevel() >= 75)
								SpellBook4(pc, useItem);
							else
								pc.sendPackets(new S_ServerMessage(3321, "75"), true);
						} else if (itemId == 60348 || itemId == 72029) {
							if (pc.getLevel() >= 80)
								SpellBook4(pc, useItem);
							else
								pc.sendPackets(new S_ServerMessage(3321, "80"), true);
						} else if (itemId == 300090) {
							if (pc.getLevel() >= 80)
								SpellBook4(pc, useItem);
							else
								pc.sendPackets(new S_ServerMessage(3321, "80"), true);
						} else if (itemId == 300091) {
							if (pc.getLevel() >= 80)
								SpellBook4(pc, useItem);
							else
								pc.sendPackets(new S_ServerMessage(3321, "80"), true);
						} else if (itemId == 72000) {
							if (pc.getLevel() >= 85)
								SpellBook4(pc, useItem);
							else
								pc.sendPackets(new S_ServerMessage(3321, "85"), true);
						} else {
							pc.sendPackets(new S_ServerMessage(312), true); // LV가 낮아서
						}
					} else {
						pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."),
								true);
					}
				} else if (itemId >= 40232 && itemId <= 40264 // 정령의 수정
						|| itemId >= 41149 && itemId <= 41153 || itemId == 72002 || itemId == 72028 || (itemId >= 72009 && itemId <= 72011)) {
					useElfSpellBook(pc, useItem, itemId);
				} else if (itemId > 40264 && itemId < 40280 || itemId == 60199 || itemId == 72015 || itemId == 72016 || itemId == 72017) {
					if (pc.isDarkelf() || pc.isGm()) {
						if (itemId >= 40265 && itemId <= 40269) { // 어둠 정령의 수정
							if (pc.getLevel() >= 15)
								SpellBook1(pc, useItem);
							else
								pc.sendPackets(new S_ServerMessage(3321, "15"), true);
						} else if (itemId >= 40270 && itemId <= 40274) { // / 어둠 정령의 수정
							if (pc.getLevel() >= 30)
								SpellBook1(pc, useItem);
							else
								pc.sendPackets(new S_ServerMessage(3321, "30"), true);
						} else if (itemId >= 40275 && itemId <= 40277) {
							if (pc.getLevel() >= 45)
								SpellBook1(pc, useItem);
							else
								pc.sendPackets(new S_ServerMessage(3321, "45"), true);
						} else if (itemId == 40279) {
							if (pc.getLevel() >= 45)
								SpellBook1(pc, useItem);
							else
								pc.sendPackets(new S_ServerMessage(3321, "45"), true);
						} else if (itemId == 60199) {
							if (pc.getLevel() >= 60) { // 아머브레이크
								SpellBook1(pc, useItem);
							} else
								pc.sendPackets(new S_ServerMessage(3321, "60"), true);
						} else if (itemId == 72015) {
							if (pc.getLevel() >= 80) { // 루시퍼
								다엘루시퍼(pc, useItem);
							} else
								pc.sendPackets(new S_ServerMessage(3321, "80"), true);
						} else if (itemId == 72016) {
							if (pc.getLevel() >= 80) { // 더블 데스티니
								다엘패시브(pc, useItem);
							} else
								pc.sendPackets(new S_ServerMessage(3321, "80"), true);
						} else if (itemId == 72017) {
							if (pc.getLevel() >= 80) { // 아머 데스티니
								다엘패시브(pc, useItem);
							} else
								pc.sendPackets(new S_ServerMessage(3321, "80"), true);
						} else if (itemId == 40278) { //파이널 번
							if (pc.getLevel() >= 60)
							    패시브엑티브스킬(pc, useItem, true); //패시브
							else
								pc.sendPackets(new S_ServerMessage(3321, "60"), true);
						} else {
							pc.sendPackets(new S_ServerMessage(312), true);
						}
					} else {
						pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."), true); // (원문:어둠 정령의 수정은 다크 에르프만을 습득할 수 있습니다. )
					}
				} else if (itemId >= 7300 && itemId <= 7311 || itemId == 72007 || itemId == 72022) {
					if (pc.isWarrior() || pc.isGm()) {
						if (itemId == 7302 && pc.getLevel() >= 15) {// 슬레이어 패시브
							전사의인장패시브(pc, useItem);
						} else if (itemId == 7307 && pc.getLevel() >= 30) {// 하울 스킬
							전사의인장(pc, useItem);
						} else if (itemId == 7308 && pc.getLevel() >= 45) {// 토마호크 스킬
							전사의인장(pc, useItem);
						} else if (itemId == 7300 && pc.getLevel() >= 45) {// 크래쉬 패시브
							전사의인장패시브(pc, useItem);
						} else if (itemId == 7303 && pc.getLevel() >= 60) {// 아머가드 패시브
							전사의인장패시브(pc, useItem);
						} else if (itemId == 7309 && pc.getLevel() >= 60) {// 기간틱 스킬
							전사의인장(pc, useItem);
						} else if (itemId == 7301 && pc.getLevel() >= 60) {// 퓨리  패시브
							전사의인장패시브(pc, useItem);
						} else if (itemId == 7304 && pc.getLevel() >= 75) {// 타이탄락 패시브
							전사의인장패시브(pc, useItem);
						} else if (itemId == 7310 && pc.getLevel() >= 80) {// 데스페라도 스킬
							전사의인장(pc, useItem);
						} else if (itemId == 7311 && pc.getLevel() >= 75) {// 파워그립 스킬
							전사의인장(pc, useItem);
						} else if (itemId == 7306 && pc.getLevel() >= 75) {// 타이탄매직 스킬
							전사의인장패시브(pc, useItem);
						} else if (itemId == 7305 && pc.getLevel() >= 80) {// 타이탄블릿 스킬
							전사의인장패시브(pc, useItem);
						} else if (itemId == 72007 && pc.getLevel() >= 80) {// 타이탄 라이징 스킬
							전사의인장(pc, useItem);
						} else if (itemId == 72022 && pc.getLevel() >= 85) {// 타이탄 라이징 스킬
							전사의인장패시브(pc, useItem);
						} else {
							pc.sendPackets(new S_ServerMessage(312));
						}
					} else {
						pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."));
					}

				} else if (itemId >= 40164 && itemId <= 40166 // 기술서
						|| itemId >= 41147 && itemId <= 41148 || itemId == 72001 
						|| itemId == 72012 || itemId == 72013 || itemId == 72014) {
					if (pc.isKnight() || pc.isGm()) {
						if (itemId == 40164) { // 쇼크 스턴
							if (pc.getLevel() >= 60)
								SpellBook3(pc, useItem);
							else
								pc.sendPackets(new S_ServerMessage(3321, "60"), true);
						} else if (itemId == 40165) { // 리덕션 아머
							if (pc.getLevel() >= 50)
									SpellBook3(pc, useItem);
							else
									pc.sendPackets(new S_ServerMessage(3321, "50"), true);
						} else if (itemId == 41147) { // 솔리드 케리지
							if (pc.getLevel() >= 55)
								SpellBook3(pc, useItem);
							else
								pc.sendPackets(new S_ServerMessage(3321, "55"), true);
						} else if (itemId == 41148) { // 카운터 배리어
							if (pc.getLevel() >= 80)
								SpellBook3(pc, useItem);
							else
								pc.sendPackets(new S_ServerMessage(3321, "80"), true);
						} else if (itemId == 40166) { // 바운스 어택
							if (pc.getLevel() >= 65)
								SpellBook3(pc, useItem);
							else
								pc.sendPackets(new S_ServerMessage(3321, "65"), true);
						} else if (itemId == 72001) { // 앱솔루트 블레이드
							if (pc.getLevel() >= 85)
								SpellBook3(pc, useItem);
							else
								pc.sendPackets(new S_ServerMessage(3321, "85"), true);
						} else if (itemId == 72012) { // 프라이드
							if (pc.getLevel() >= 60)
								SpellBook3(pc, useItem);
							else
								pc.sendPackets(new S_ServerMessage(3321, "60"), true);
						} else if (itemId == 72013) { // 블로우 어택
							if (pc.getLevel() >= 75)
								SpellBook3(pc, useItem);
							else
								pc.sendPackets(new S_ServerMessage(3321, "75"), true);
						} else if (itemId == 72014) { // 카운터 배리어:베테랑
							if (pc.getLevel() >= 85)
								기사베테랑(pc, useItem);
							else
								pc.sendPackets(new S_ServerMessage(3321, "85"), true);
						} else {
							pc.sendPackets(new S_ServerMessage(312), true);
						}
					} else {
						pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."), true);
					}
				} else if (itemId >= L1ItemId.DRAGONKNIGHT_SPELLSTART
						&& itemId <= L1ItemId.DRAGONKNIGHT_SPELLEND
						|| itemId == 72018|| itemId == 72019 || itemId == 72020 ) {
					if (pc.isDragonknight() || pc.isGm()) {
						if (itemId >= L1ItemId.DRAGONKNIGHTTABLET_DRAGONSKIN && itemId <= L1ItemId.DRAGONKNIGHTTABLET_AWAKE_ANTHARAS) {
							if (pc.getLevel() >= 20)
								SpellBook5(pc, useItem);
							else
								pc.sendPackets(new S_ServerMessage(3321, "20"), true);
						} else if (itemId >= L1ItemId.DRAGONKNIGHTTABLET_BLOODLUST
								&& itemId <= L1ItemId.DRAGONKNIGHTTABLET_AWAKE_PAPURION) {
							if (pc.getLevel() >= 40)
								SpellBook5(pc, useItem);
							else
								pc.sendPackets(new S_ServerMessage(3321, "40"),
										true);
						} else if (itemId >= L1ItemId.DRAGONKNIGHTTABLET_MOTALBODY
								&& itemId <= L1ItemId.DRAGONKNIGHTTABLET_AWAKE_BALAKAS) {
							if (pc.getLevel() >= 60)
								SpellBook5(pc, useItem);
							else
								pc.sendPackets(new S_ServerMessage(3321, "60"), true);
						} else if (itemId == 72018) {
							if (pc.getLevel() >= 80)
								SpellBook5(pc, useItem);
							else
								pc.sendPackets(new S_ServerMessage(3321, "80"), true);
						} else if (itemId == 72019) {
							if (pc.getLevel() >= 80)
								용기사패시브(pc, useItem);
							else
								pc.sendPackets(new S_ServerMessage(3321, "80"), true);
						} else if (itemId == 72020) {
							if (pc.getLevel() >= 85)
								용기사패시브(pc, useItem);
							else
								pc.sendPackets(new S_ServerMessage(3321, "85"), true);
						/*} else if (itemId == 300092) {
							if (pc.getLevel() >= 80)
								패시브엑티브스킬(pc, useItem, true);
							else
								pc.sendPackets(new S_ServerMessage(3321, "80"), true);*/
						} else {
							pc.sendPackets(new S_ServerMessage(312), true);
						}
					} else {
						pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."),
								true);
					}
				} else if (itemId >= L1ItemId.ILLUSIONIST_SPELLSTART
						&& itemId <= L1ItemId.ILLUSIONIST_SPELLEND || itemId == 72008
						|| itemId == 72021 || itemId == 300093 || itemId == 300094) {
					if (pc.isIllusionist() || pc.isGm()) {
						if (itemId >= L1ItemId.MEMORIALCRYSTAL_MIRRORIMAGE
								&& itemId <= L1ItemId.MEMORIALCRYSTAL_CUBE_IGNITION) {
							if (pc.getLevel() >= 15)
								SpellBook6(pc, useItem);
							else
								pc.sendPackets(new S_ServerMessage(3321, "15"),
										true);
						} else if (itemId >= L1ItemId.MEMORIALCRYSTAL_CONSENTRATION
								&& itemId <= L1ItemId.MEMORIALCRYSTAL_CUBE_QUAKE) {
							if (pc.getLevel() >= 30)
								SpellBook6(pc, useItem);
							else
								pc.sendPackets(new S_ServerMessage(3321, "30"),
										true);
						} else if (itemId >= L1ItemId.MEMORIALCRYSTAL_PATIENCE
								&& itemId <= L1ItemId.MEMORIALCRYSTAL_CUBE_SHOCK) {
							if (pc.getLevel() >= 45)
								SpellBook6(pc, useItem);
							else
								pc.sendPackets(new S_ServerMessage(3321, "45"),
										true);
						} else if (itemId >= L1ItemId.MEMORIALCRYSTAL_INSITE
								&& itemId <= L1ItemId.MEMORIALCRYSTAL_CUBE_BALANCE) {
							if (pc.getLevel() >= 60)
								SpellBook6(pc, useItem);
							else
								pc.sendPackets(new S_ServerMessage(3321, "60"), true);
						} else if (itemId == 72021) {
							if (pc.getLevel() >= 75)
								SpellBook6(pc, useItem);
							else
								pc.sendPackets(new S_ServerMessage(3321, "75"), true);
						} else if (itemId == 72008) {
							if (pc.getLevel() >= 80)
								SpellBook6(pc, useItem);
							else
								pc.sendPackets(new S_ServerMessage(3321, "80"), true);
						} else if (itemId == 300093) {
							if (pc.getLevel() >= 85)
								패시브엑티브스킬(pc, useItem, false);
							else
								pc.sendPackets(new S_ServerMessage(3321, "85"), true);
						} else if (itemId == 300094) {
							if (pc.getLevel() >= 80)
								패시브엑티브스킬(pc, useItem, true);
							else
								pc.sendPackets(new S_ServerMessage(3321, "80"), true);
						} else {
							pc.sendPackets(new S_ServerMessage(312), true);
						}
					} else {
						pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."),
								true);
					}
				}
				L1ItemDelay.onItemUse(pc, useItem); // 아이템 지연 개시
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void 전사의인장(L1PcInstance pc, L1ItemInstance l1iteminstance) {
		String s = "";
		int i = 0;
		int j = 0;
		int k = 0;
		int l = 0;
		int i1 = 0;
		int j1 = 0;
		int k1 = 0;
		int l1 = 0;
		int i2 = 0;
		int j2 = 0;
		int k2 = 0;
		int l2 = 0;
		int i3 = 0;
		int j3 = 0;
		int k3 = 0;
		int l3 = 0;
		int i4 = 0;
		int j4 = 0;
		int k4 = 0;
		int l4 = 0;
		int i5 = 0;
		int j5 = 0;
		int k5 = 0;
		int l5 = 0;
		int i6 = 0;
		int dk3 = 0;
		int bw1 = 0;
		int bw2 = 0;
		int bw3 = 0;

		int warrior1 = 0;
		int warrior2 = 0;
		int warrior3 = 0;
		int warrior4 = 0;
		
		L1Skills l1skills = null;
		for (int j6 = 225; j6 <= 231; j6++) {
			l1skills = SkillsTable.getInstance().getTemplate(j6);
			String s1 = "전사의 인장(" + l1skills.getName() + ")";
			if (l1iteminstance.getItem().getName().equalsIgnoreCase(s1)) {
				int l6 = l1skills.getSkillLevel();
				int i7 = l1skills.getId();
				if (pc.isSkillMastery(i)) {
					pc.sendPackets(new S_SystemMessage("이미 습득한 스킬 입니다."), true);
					return;
				}
				s = l1skills.getName();
				i = l1skills.getSkillId();
				switch (l6) {
				case 1:
					j = i7;
					break;
				case 2:
					k = i7;
					break;
				case 3:
					l = i7;
					break;
				case 4:
					i1 = i7;
					break;
				case 5:
					j1 = i7;
					break;
				case 6:
					k1 = i7;
					break;
				case 7:
					l1 = i7;
					break;
				case 8:
					i2 = i7;
					break;
				case 9:
					j2 = i7;
					break;
				case 10:
					k2 = i7;
					break;
				case 11:
					l2 = i7;
					break;
				case 12:
					i3 = i7;
					break;
				case 13:
					j3 = i7;
					break;
				case 14:
					k3 = i7;
					break;
				case 15:
					l3 = i7;
					break;
				case 16:
					i4 = i7;
					break;
				case 17:
					j4 = i7;
					break;
				case 18:
					k4 = i7;
					break;
				case 19:
					l4 = i7;
					break;
				case 20:
					i5 = i7;
					break;
				case 21:
					j5 = i7;
					break;
				case 22:
					k5 = i7;
					break;
				case 23:
					l5 = i7;
					break;
				case 24:
					i6 = i7;
					break;
				case 25:
					dk3 = i7;
					break;
				case 26:
					bw1 = i7;
					break;
				case 27:
					bw2 = i7;
					break;
				case 28:
					bw3 = i7;
					break;
				case 29:
					warrior1 = i7;
					break;
				case 30:
					warrior2 = i7;
					break;
				case 31:
					warrior3 = i7;
					break;
				case 32:
					warrior4 = i7;
					break;

				}
			}
		}

		int k6 = pc.getId();
		pc.sendPackets(new S_AddSkill(j, k, l, i1, j1, k1, l1, i2, j2, k2, l2,
				i3, j3, k3, l3, i4, j4, k4, l4, i5, j5, k5, l5, i6, dk3, bw1,
				bw2, bw3, warrior1, warrior2, warrior3, warrior4, pc.getElfAttr()));
		S_SkillSound s_skillSound = new S_SkillSound(k6, 224);
		pc.sendPackets(s_skillSound);
		Broadcaster.broadcastPacket(pc, s_skillSound);
		SkillsTable.getInstance().spellMastery(k6, i, s, 0, 0);
		pc.getInventory().removeItem(l1iteminstance, 1);
	}

	private void 전사의인장패시브(L1PcInstance pc, L1ItemInstance l1iteminstance) {
		L1Skills l1skills = null;
		for (int i = 300; i < 314; i++) {
			l1skills = SkillsTable.getInstance().getTemplate(i);
			String s1 = "전사의 인장(" + l1skills.getName() + ")";
			if (l1iteminstance.getItem().getName().equalsIgnoreCase(s1)) {
				int id = l1skills.getId();
				
				if(i == 313){
					if (!pc.isSkillMastery(230)) {
						pc.sendPackets(new S_SystemMessage("선행되는 스킬을 배우지 않았습니다."), true);
						return;
					}
				}
				
				if (pc.isSkillMastery(i)) {
					pc.sendPackets(new S_SystemMessage("이미 습득한 패시브 입니다."), true);
					return;
				}
				switch (id) {
				case 1:
					pc.isCrash = true;
					break;
				case 2:
					pc.isPurry = true;
					break;
				case 3:
					pc.isSlayer = true;
					break;
				case 5:
					pc.isAmorGaurd = true;
					break;
				case 6:
					pc.isTaitanR = true;
					break;
				case 7:
					pc.isTaitanB = true;
					break;
				case 8:
					pc.isTaitanM = true;
					break;
				case 13:
					pc.isDesAbsol = true;
					break;
				}
				pc.sendPackets(new S_NewUI(S_NewUI.패시브추가, id));
				S_SkillSound s_skillSound = new S_SkillSound(pc.getId(), 224);
				pc.sendPackets(s_skillSound);
				Broadcaster.broadcastPacket(pc, s_skillSound);
				SkillsTable.getInstance().spellMastery(pc.getId(), i, l1skills.getName(), 0, 0);
				pc.getInventory().removeItem(l1iteminstance, 1);
			}
		}
	}
	private void 다엘패시브(L1PcInstance pc, L1ItemInstance l1iteminstance) {
		L1Skills l1skills = null;
		for (int i = 309; i < 311; i++) {
			l1skills = SkillsTable.getInstance().getTemplate(i);
			String s1 = "흑정령의 수정 (" + l1skills.getName() + ")";
			if (l1iteminstance.getItem().getName().equalsIgnoreCase(s1)) {
				int id = l1skills.getId();
				
				int baseskill = 0;
				if(i == 309){
					baseskill = 105;
				} else {
					baseskill = 112;
				}
				
				if (!pc.isSkillMastery(baseskill)) {
					pc.sendPackets(new S_SystemMessage("선행되는 스킬을 배우지 않았습니다."), true);
					return;
				}
				
				if (pc.isSkillMastery(i)) {
					pc.sendPackets(new S_SystemMessage("이미 습득한 패시브 입니다."), true);
					return;
				}
				switch (id) {
				case 12:
					pc.isDBDestiny = true;
					break;
				case 11:
					pc.isABDestiny = true;
					break;
				}
				pc.sendPackets(new S_NewUI(S_NewUI.패시브추가, id));
				S_SkillSound s_skillSound = new S_SkillSound(pc.getId(), 224);
				pc.sendPackets(s_skillSound);
				Broadcaster.broadcastPacket(pc, s_skillSound);
				SkillsTable.getInstance().spellMastery(pc.getId(), i, l1skills.getName(), 0, 0);
				pc.getInventory().removeItem(l1iteminstance, 1);
			}
		}
	}

	
	private void 패시브엑티브스킬(L1PcInstance pc, L1ItemInstance item, boolean ispassibe) {
		L1Skills skill = SkillsTable.getInstance().getTemplateByItem(item.getItemId());
		if (skill != null) {
			if (ispassibe) { //패시브
				int id = skill.getId();
				int skillId = skill.getSkillId();
				if (SkillsTable.getInstance().spellCheck(pc.getId(), skillId)) {
					pc.sendPackets(new S_SystemMessage("이미 배운 마법입니다."), true);
					return;
				}
				if (item.getItemId() == 40236){ //레지스트 엘리멘트
					pc.getResistance().addMr(5);
					pc.getResistance().addAllNaturalResistance(5);
					pc.sendPackets(new S_SPMR(pc));
					pc.sendPackets(new S_OwnCharAttrDef(pc));
				}
				pc.sendPackets(new S_ACTION_UI(146, id));
				S_SkillSound s_skillSound = new S_SkillSound(pc.getId(), 224);
				pc.sendPackets(s_skillSound);
				Broadcaster.broadcastPacket(pc, s_skillSound);
				SkillsTable.getInstance().spellMastery(pc.getId(), skillId, skill.getName(), 0, 0);
			} else { //액티브
				int skillLevel = skill.getSkillLevel();
				int id = skill.getId();
				int[] arr = new int[29];
				arr[skillLevel - 1] = id;
				int skillId = skill.getSkillId();
				if (SkillsTable.getInstance().spellCheck(pc.getId(), skillId)) {
					pc.sendPackets(new S_SystemMessage("이미 배운 마법입니다."), true);
					return;
				}
				int objid = pc.getId();
				pc.sendPackets(new S_AddSkill(pc, arr));
				S_SkillSound s_skillSound = new S_SkillSound(objid, 224);
				pc.sendPackets(s_skillSound);
				Broadcaster.broadcastPacket(pc, s_skillSound);
				SkillsTable.getInstance().spellMastery(objid, skillId, skill.getName(), 0, 0);
			}
		}
		pc.getInventory().removeItem(item, 1);
	}
	
	private void 용기사패시브(L1PcInstance pc, L1ItemInstance l1iteminstance) {
		L1Skills l1skills = null;
		for (int i = 311; i < 313; i++) {
			l1skills = SkillsTable.getInstance().getTemplate(i);
			String s1 = "용기사의 서판 (" + l1skills.getName() + ")";
			if (l1iteminstance.getItem().getName().equalsIgnoreCase(s1)) {
				int id = l1skills.getId();
				
				int baseskill = 0;
				if(i == 311){
					baseskill = 192;
				} else {
					baseskill = 187;
				}
				
				if (!pc.isSkillMastery(baseskill)) {
					pc.sendPackets(new S_SystemMessage("선행되는 스킬을 배우지 않았습니다."), true);
					return;
				}
				
				if (pc.isSkillMastery(i)) {
					pc.sendPackets(new S_SystemMessage("이미 습득한 패시브 입니다."), true);
					return;
				}
				switch (id) {
				case 14:
					pc.isGrabBrave = true;
					break;
				case 15:
					pc.isFouBrave = true;
					break;
				}
				pc.sendPackets(new S_NewUI(S_NewUI.패시브추가, id));
				S_SkillSound s_skillSound = new S_SkillSound(pc.getId(), 224);
				pc.sendPackets(s_skillSound);
				Broadcaster.broadcastPacket(pc, s_skillSound);
				SkillsTable.getInstance().spellMastery(pc.getId(), i, l1skills.getName(), 0, 0);
				pc.getInventory().removeItem(l1iteminstance, 1);
			}
		}
	}

	private void 기사베테랑(L1PcInstance pc, L1ItemInstance l1iteminstance) {
		L1Skills l1skills = null;
			l1skills = SkillsTable.getInstance().getTemplate(308);
			String s1 = "기술서 (" + l1skills.getName() + ")";
			if (l1iteminstance.getItem().getName().equalsIgnoreCase(s1)) {
				int id = l1skills.getId();
				
				if (!pc.isSkillMastery(91)) {
					pc.sendPackets(new S_SystemMessage("선행되는 스킬을 배우지 않았습니다.."), true);
					return;
				}
				
				if (pc.isSkillMastery(308)) {
					pc.sendPackets(new S_SystemMessage("이미 습득한 패시브 입니다."), true);
					return;
				}
				
				switch (id) {
				case 10:
					pc.isBetterang = true;
					break;
				}
				pc.sendPackets(new S_NewUI(S_NewUI.패시브추가, id));
				S_SkillSound s_skillSound = new S_SkillSound(pc.getId(), 224);
				pc.sendPackets(s_skillSound);
				Broadcaster.broadcastPacket(pc, s_skillSound);
				SkillsTable.getInstance().spellMastery(pc.getId(), 308, l1skills.getName(), 0, 0);
				pc.getInventory().removeItem(l1iteminstance, 1);
			}
	}
	
	
	private void useSpellBook(L1PcInstance pc, L1ItemInstance item, int itemId) {
		int itemAttr = 0;
		int locAttr = 0; // 0:other 1:law 2:chaos
		boolean isLawful = true;
		int pcX = pc.getX();
		int pcY = pc.getY();
		int mapId = pc.getMapId();
		int level = pc.getLevel();
		if (itemId == 45000 || itemId == 45008 || itemId == 45018
				|| itemId == 45021 || itemId == 40171 || itemId == 40179
				|| itemId == 40180 || itemId == 40182 || itemId == 40194
				|| itemId == 40197 || itemId == 40202 || itemId == 40206
				|| itemId == 40213 || itemId == 40220 || itemId == 40222) {
			itemAttr = 1;
		}
		if (itemId == 45009 || itemId == 45010 || itemId == 45019
				|| itemId == 40172 || itemId == 40173 || itemId == 40178
				|| itemId == 40185 || itemId == 40186 || itemId == 40192
				|| itemId == 40196 || itemId == 40201 || itemId == 40204
				|| itemId == 40211 || itemId == 40221 || itemId == 40225
				|| itemId == 140186 || itemId == 140196 || itemId == 140204) {
			itemAttr = 2;
		}
		if (pcX > 33116 && pcX < 33128 && pcY > 32930 && pcY < 32942
				&& mapId == 4 || pcX > 33135 && pcX < 33147 && pcY > 32235
				&& pcY < 32247 && mapId == 4 || pcX >= 32783 && pcX <= 32803
				&& pcY >= 32831 && pcY <= 32851 && mapId == 77 || pcX >= 33189
				&& pcX <= 33198 && pcY >= 33446 && pcY <= 33456 && mapId == 4) {
			locAttr = 1;
			isLawful = true;
		}
		if (pcX > 32880 && pcX < 32892 && pcY > 32646 && pcY < 32658
				&& mapId == 4 || pcX > 32662 && pcX < 32674 && pcY > 32297
				&& pcY < 32309 && mapId == 4 || pcX >= 33075 && pcX <= 33082
				&& pcY >= 33212 && pcY <= 33220 && mapId == 4) {
			locAttr = 2;
			isLawful = false;
		}
		if (pc.isGm()) {
			SpellBook(pc, item, isLawful);
		} else {// if ((itemAttr == locAttr || itemAttr == 0) && locAttr != 0) {
			if (pc.isKnight()) {
				if (itemId >= 45000 && itemId <= 45007) {
					if (level >= 50)
						SpellBook(pc, item, isLawful);
					else
						pc.sendPackets(new S_ServerMessage(3321, "50"), true);
				} else if (itemId >= 45000 && itemId <= 45007) {
					pc.sendPackets(new S_ServerMessage(312), true);
				} else {
					pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."),
							true);
				}
			} else if (pc.isCrown() || pc.isDarkelf()) {
				if (itemId >= 45000 && itemId <= 45007) {
					if (level >= 15)
						SpellBook(pc, item, isLawful);
					else
						pc.sendPackets(new S_ServerMessage(3321, "15"), true);
				} else if (itemId >= 45008 && itemId <= 45015) {
					if (level >= 30)
						SpellBook(pc, item, isLawful);
					else
						pc.sendPackets(new S_ServerMessage(3321, "30"), true);
				} else if (itemId >= 45008 && itemId <= 45015
						|| itemId >= 45000 && itemId <= 45007) {
					pc.sendPackets(new S_ServerMessage(312), true);
				} else {
					pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."), true);
				}
			} else if (pc.isElf()) {
				if (itemId >= 45000 && itemId <= 45007) {
					if (level >= 10)
						SpellBook(pc, item, isLawful);
					else
						pc.sendPackets(new S_ServerMessage(3321, "10"), true);
				} else if (itemId >= 45008 && itemId <= 45015) {
					if (level >= 20)
						SpellBook(pc, item, isLawful);
					else
						pc.sendPackets(new S_ServerMessage(3321, "20"), true);
				} else if (itemId >= 45016 && itemId <= 45022) {
					if (level >= 30)
						SpellBook(pc, item, isLawful);
					else
						pc.sendPackets(new S_ServerMessage(3321, "30"), true);
				} else if (itemId >= 40170 && itemId <= 40177) {
					if (level >= 40)
						SpellBook(pc, item, isLawful);
					else
						pc.sendPackets(new S_ServerMessage(3321, "40"), true);
				} else if (itemId >= 40178 && itemId <= 40185) {
					if (level >= 50)
						SpellBook(pc, item, isLawful);
					else
						pc.sendPackets(new S_ServerMessage(3321, "50"), true);
				} else if (((itemId >= 40186 && itemId <= 40193) || itemId == 140186)) {
					if (level >= 60)
						SpellBook(pc, item, isLawful);
					else
						pc.sendPackets(new S_ServerMessage(3321, "60"), true);
				} else if (itemId >= 45000 && itemId <= 45022
						|| itemId >= 40170 && itemId <= 40193) {
					pc.sendPackets(new S_ServerMessage(312), true);
				} else {
					pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."),
							true);
				}
			} else if (pc.isWizard()) {
				if (itemId >= 45000 && itemId <= 45007) {
					if (level >= 4)
						SpellBook(pc, item, isLawful);
					else
						pc.sendPackets(new S_ServerMessage(3321, "4"), true);
				} else if (itemId >= 45008 && itemId <= 45015) {
					if (level >= 8)
						SpellBook(pc, item, isLawful);
					else
						pc.sendPackets(new S_ServerMessage(3321, "8"), true);
				} else if (itemId >= 45016 && itemId <= 45022) {
					if (level >= 16)
						SpellBook(pc, item, isLawful);
					else
						pc.sendPackets(new S_ServerMessage(3321, "16"), true);
				} else if (itemId >= 40170 && itemId <= 40177) {
					if (level >= 20)
						SpellBook(pc, item, isLawful);
					else
						pc.sendPackets(new S_ServerMessage(3321, "20"), true);
				} else if (itemId >= 40178 && itemId <= 40185) {
					if (level >= 24)
						SpellBook(pc, item, isLawful);
					else
						pc.sendPackets(new S_ServerMessage(3321, "24"), true);
				} else if (((itemId >= 40186 && itemId <= 40193) || itemId == 140186)) {
					if (level >= 28)
						SpellBook(pc, item, isLawful);
					else
						pc.sendPackets(new S_ServerMessage(3321, "28"), true);
				} else if (((itemId >= 40194 && itemId <= 40201)
						|| itemId == 140196 || itemId == 140198)) {
					if (level >= 32)
						SpellBook(pc, item, isLawful);
					else
						pc.sendPackets(new S_ServerMessage(3321, "32"), true);
				} else if (((itemId >= 40202 && itemId <= 40209)
						|| itemId == 140204 || itemId == 140205)) {
					if (level >= 36)
						SpellBook(pc, item, isLawful);
					else
						pc.sendPackets(new S_ServerMessage(3321, "36"), true);
				} else if (((itemId >= 40210 && itemId <= 40217) || itemId == 140210)) {
					if (level >= 40)
						SpellBook(pc, item, isLawful);
					else
						pc.sendPackets(new S_ServerMessage(3321, "40"), true);
				} else if (((itemId >= 40218 && itemId <= 40225) || itemId == 140219)) {
					if (level >= 44)
						SpellBook(pc, item, isLawful);
					else
						pc.sendPackets(new S_ServerMessage(3321, "44"), true);
				} else {
					pc.sendPackets(new S_ServerMessage(312), true);
				}
			}
		}
	}

	private void useElfSpellBook(L1PcInstance pc, L1ItemInstance item,
			int itemId) {
		int level = pc.getLevel();
		if ((pc.isElf() || pc.isGm())) {
			if (itemId >= 40232 && itemId <= 40234) {
				if (level >= 15)
					SpellBook2(pc, item);
				else
					pc.sendPackets(new S_ServerMessage(3321, "15"), true);
			} else if (itemId == 40235) {
				if (level >= 30)
					SpellBook2(pc, item);
				else
					pc.sendPackets(new S_ServerMessage(3321, "30"), true);
			} else if (itemId >= 40237 && itemId <= 40240) {
				if (level >= 45)
					SpellBook2(pc, item);
				else
					pc.sendPackets(new S_ServerMessage(3321, "45"), true);
			} else if (itemId >= 40241 && itemId <= 40243 || itemId == 72009 || itemId == 72010) {
				if (level >= 60)
					SpellBook2(pc, item);
				else
					pc.sendPackets(new S_ServerMessage(3321, "60"), true);
			} else if (itemId >= 40244 && itemId <= 40246) {
				if (level >= 75)
					SpellBook2(pc, item);
				else
					pc.sendPackets(new S_ServerMessage(3321, "75"), true);
			} else if (itemId >= 40247 && itemId <= 40248) {
				if (level >= 45)
					SpellBook2(pc, item);
				else
					pc.sendPackets(new S_ServerMessage(3321, "45"), true);
			} else if (itemId >= 40249 && itemId <= 40250) {
				if (level >= 60)
					SpellBook2(pc, item);
				else
					pc.sendPackets(new S_ServerMessage(3321, "60"), true);
			} else if (itemId >= 40251 && itemId <= 40252) {
				if (level >= 75)
					SpellBook2(pc, item);
				else
					pc.sendPackets(new S_ServerMessage(3321, "75"), true);
			} else if (itemId == 40253) {
				if (level >= 45)
					SpellBook2(pc, item);
				else
					pc.sendPackets(new S_ServerMessage(3321, "45"), true);
			} else if (itemId == 40254) {
				if (level >= 60)
					SpellBook2(pc, item);
				else
					pc.sendPackets(new S_ServerMessage(3321, "60"), true);
			} else if (itemId == 40255) {
				if (level >= 75)
					SpellBook2(pc, item);
				else
					pc.sendPackets(new S_ServerMessage(3321, "75"), true);
			} else if (itemId == 40256) {
				if (level >= 45)
					SpellBook2(pc, item);
				else
					pc.sendPackets(new S_ServerMessage(3321, "45"), true);
			} else if (itemId == 40257) {
				if (level >= 60)
					SpellBook2(pc, item);
				else
					pc.sendPackets(new S_ServerMessage(3321, "60"), true);
			} else if (itemId >= 40258 && itemId <= 40259) {
				if (level >= 75)
					SpellBook2(pc, item);
				else
					pc.sendPackets(new S_ServerMessage(3321, "75"), true);
			} else if (itemId >= 40260 && itemId <= 40261) {
				if (level >= 45)
					SpellBook2(pc, item);
				else
					pc.sendPackets(new S_ServerMessage(3321, "45"), true);
			} else if (itemId == 40262) {
				if (level >= 60)
					SpellBook2(pc, item);
				else
					pc.sendPackets(new S_ServerMessage(3321, "60"), true);
			} else if (itemId >= 40263 && itemId <= 40264) {
				if (level >= 75)
					SpellBook2(pc, item);
				else
					pc.sendPackets(new S_ServerMessage(3321, "75"), true);
			} else if (itemId >= 41149 && itemId <= 41150) {
				if (level >= 75)
					SpellBook2(pc, item);
				else
					pc.sendPackets(new S_ServerMessage(3321, "75"), true);
			} else if (itemId == 41151) {
				if (level >= 60)
					SpellBook2(pc, item);
				else
					pc.sendPackets(new S_ServerMessage(3321, "60"), true);
			} else if (itemId >= 41152 && itemId <= 41153) {
				if (level >= 75)
					SpellBook2(pc, item);
				else
					pc.sendPackets(new S_ServerMessage(3321, "75"), true);
			} else if (itemId == 72011) {
				if (level >= 75)
					SpellBook2(pc, item);
				else
					pc.sendPackets(new S_ServerMessage(3321, "75"), true);
			} else if (itemId == 72002 || itemId == 72028) {
				if (level >= 80)
					SpellBook2(pc, item);
				else
					pc.sendPackets(new S_ServerMessage(3321, "80"), true);
			} else if (itemId == 40236) { //레지스트 엘리멘트
				if (level >= 30)
					패시브엑티브스킬(pc, item, true); //패시브
				else
					pc.sendPackets(new S_ServerMessage(3321, "75"), true);
			}
		} else {
			pc.sendPackets(new S_ServerMessage(312), true);
		}
	}


	private void SpellBook(L1PcInstance pc, L1ItemInstance item, boolean isLawful) {
		String s = "";
		int i = 0;
		int level1 = 0;
		int level2 = 0;
		int l = 0;
		int i1 = 0;
		int j1 = 0;
		int k1 = 0;
		int l1 = 0;
		int i2 = 0;
		int j2 = 0;
		int k2 = 0;
		int l2 = 0;
		int i3 = 0;
		int j3 = 0;
		int k3 = 0;
		int l3 = 0;
		int i4 = 0;
		int j4 = 0;
		int k4 = 0;
		int l4 = 0;
		int i5 = 0;
		int j5 = 0;
		int k5 = 0;
		int l5 = 0;
		int i6 = 0;
		int dk3 = 0;
		int bw1 = 0;
		int bw2 = 0;
		int bw3 = 0;
		L1Skills l1skills = null;
		for (int skillId = 1; skillId < 81; skillId++) {
			l1skills = SkillsTable.getInstance().getTemplate(skillId);
			String s1 = "마법서 (" + l1skills.getName() + ")";
			if (item.getItem().getName().equalsIgnoreCase(s1)) {
				int skillLevel = l1skills.getSkillLevel();
				int i7 = l1skills.getId();
				s = l1skills.getName();
				i = l1skills.getSkillId();
				switch (skillLevel) {
				case 1:
					level1 = i7;
					break;
				case 2:
					level2 = i7;
					break;
				case 3:
					l = i7;
					break;
				case 4:
					i1 = i7;
					break;
				case 5:
					j1 = i7;
					break;
				case 6:
					k1 = i7;
					break;
				case 7:
					l1 = i7;
					break;
				case 8:
					i2 = i7;
					break;
				case 9:
					j2 = i7;
					break;
				case 10:
					k2 = i7;
					break;
				case 11:
					l2 = i7;
					break;
				case 12:
					i3 = i7;
					break;
				case 13:
					j3 = i7;
					break;
				case 14:
					k3 = i7;
					break;
				case 15:
					l3 = i7;
					break;
				case 16:
					i4 = i7;
					break;
				case 17:
					j4 = i7;
					break;
				case 18:
					k4 = i7;
					break;
				case 19:
					l4 = i7;
					break;
				case 20:
					i5 = i7;
					break;
				case 21:
					j5 = i7;
					break;
				case 22:
					k5 = i7;
					break;
				case 23:
					l5 = i7;
					break;
				case 24:
					i6 = i7;
					break;
				case 25:
					dk3 = i7;
					break;
				case 26:
					bw1 = i7;
					break;
				case 27:
					bw2 = i7;
					break;
				case 28:
					bw3 = i7;
					break;
				}
			}
		}
		if (pc.isSkillMastery(i)) {
			pc.sendPackets(new S_SystemMessage("이미 배운 마법입니다."), true);
			return;
		}
		int objid = pc.getId();
		pc.sendPackets(new S_AddSkill(level1, level2, l, i1, j1, k1, l1, i2,
				j2, k2, l2, i3, j3, k3, l3, i4, j4, k4, l4, i5, j5, k5, l5, i6,
				dk3, bw1, bw2, bw3, 0, 0, 0, 0, pc.getElfAttr()), true);
		S_SkillSound s_skillSound = new S_SkillSound(objid, isLawful ? 224 : 231);
		pc.sendPackets(s_skillSound);
		Broadcaster.broadcastPacket(pc, s_skillSound, true);
		SkillsTable.getInstance().spellMastery(objid, i, s, 0, 0);
		pc.getInventory().removeItem(item, 1);
	}

	private void 다엘루시퍼(L1PcInstance pc, L1ItemInstance l1iteminstance) {
		String s = "";
		int i = 0;
		int j = 0;
		int k = 0;
		int l = 0;
		int i1 = 0;
		int j1 = 0;
		int k1 = 0;
		int l1 = 0;
		int i2 = 0;
		int j2 = 0;
		int k2 = 0;
		int l2 = 0;
		int i3 = 0;
		int j3 = 0;
		int k3 = 0;
		int l3 = 0;
		int i4 = 0;
		int j4 = 0;
		int k4 = 0;
		int l4 = 0;
		int i5 = 0;
		int j5 = 0;
		int k5 = 0;
		int l5 = 0;
		int i6 = 0;
		int dk3 = 0;
		int bw1 = 0;
		int bw2 = 0;
		int bw3 = 0;
		int wr0 = 0;
		int wr1 = 0;
		int wr2 = 0;
		int wr3 = 0;
		
		L1Skills l1skills = null;
		
			l1skills = SkillsTable.getInstance().getTemplate(234);
			String s1 = "흑정령의 수정 (" + l1skills.getName() + ")";
			if (l1iteminstance.getItem().getName().equalsIgnoreCase(s1)) {
				int l6 = l1skills.getSkillLevel();
				int i7 = l1skills.getId();
				s = l1skills.getName();
				i = l1skills.getSkillId();
				switch (l6) {
				case 1:
					j = i7;
					break;
				case 2:
					k = i7;
					break;
				case 3:
					l = i7;
					break;
				case 4:
					i1 = i7;
					break;
				case 5:
					j1 = i7;
					break;
				case 6:
					k1 = i7;
					break;
				case 7:
					l1 = i7;
					break;
				case 8:
					i2 = i7;
					break;
				case 9:
					j2 = i7;
					break;
				case 10:
					k2 = i7;
					break;
				case 11:
					l2 = i7;
					break;
				case 12:
					i3 = i7;
					break;
				case 13:
					j3 = i7;
					break;
				case 14:
					k3 = i7;
					break;
				case 15:
					l3 = i7;
					break;
				case 16:
					i4 = i7;
					break;
				case 17:
					j4 = i7;
					break;
				case 18:
					k4 = i7;
					break;
				case 19:
					l4 = i7;
					break;
				case 20:
					i5 = i7;
					break;
				case 21:
					j5 = i7;
					break;
				case 22:
					k5 = i7;
					break;
				case 23:
					l5 = i7;
					break;
				case 24:
					i6 = i7;
					break;
				case 25:
					dk3 = i7;
					break;
				case 26:
					bw1 = i7;
					break;
				case 27:
					bw2 = i7;
					break;
				case 28:
					bw3 = i7;
					break;
				case 29:
					wr0 = i7;
					break;
				case 30:
					wr1 = i7;
					break;
				case 31:
					wr2 = i7;
					break;
				case 32:
					wr3 = i7;
					break;
				}
			}
		
		if (pc.isSkillMastery(i)) {
			pc.sendPackets(new S_SystemMessage("이미 배운 마법입니다."), true);
			return;
		}
		int k6 = pc.getId();
		pc.sendPackets(new S_AddSkill(j, k, l, i1, j1, k1, l1, i2, j2, k2, l2,
				i3, j3, k3, l3, i4, j4, k4, l4, i5, j5, k5, l5, i6, dk3, bw1,
				bw2, bw3, wr0, wr1, wr2, wr3, pc.getElfAttr()), true);
		S_SkillSound s_skillSound = new S_SkillSound(k6, 224);// 231
		pc.sendPackets(s_skillSound);
		Broadcaster.broadcastPacket(pc, s_skillSound, true);
		SkillsTable.getInstance().spellMastery(k6, i, s, 0, 0);
		pc.getInventory().removeItem(l1iteminstance, 1);
	}
	
	private void SpellBook1(L1PcInstance pc, L1ItemInstance l1iteminstance) {
		String s = "";
		int i = 0;
		int j = 0;
		int k = 0;
		int l = 0;
		int i1 = 0;
		int j1 = 0;
		int k1 = 0;
		int l1 = 0;
		int i2 = 0;
		int j2 = 0;
		int k2 = 0;
		int l2 = 0;
		int i3 = 0;
		int j3 = 0;
		int k3 = 0;
		int l3 = 0;
		int i4 = 0;
		int j4 = 0;
		int k4 = 0;
		int l4 = 0;
		int i5 = 0;
		int j5 = 0;
		int k5 = 0;
		int l5 = 0;
		int i6 = 0;
		int dk3 = 0;
		int bw1 = 0;
		int bw2 = 0;
		int bw3 = 0;
		int wr0 = 0;
		int wr1 = 0;
		int wr2 = 0;
		int wr3 = 0;
		
		L1Skills l1skills = null;
		
		for (int j6 = 97; j6 < 113; j6++) {
			l1skills = SkillsTable.getInstance().getTemplate(j6);
			String s1 = "흑정령의 수정 (" + l1skills.getName() + ")";
			if (l1iteminstance.getItem().getName().equalsIgnoreCase(s1)) {
				int l6 = l1skills.getSkillLevel();
				int i7 = l1skills.getId();
				s = l1skills.getName();
				i = l1skills.getSkillId();
				switch (l6) {
				case 1:
					j = i7;
					break;
				case 2:
					k = i7;
					break;
				case 3:
					l = i7;
					break;
				case 4:
					i1 = i7;
					break;
				case 5:
					j1 = i7;
					break;
				case 6:
					k1 = i7;
					break;
				case 7:
					l1 = i7;
					break;
				case 8:
					i2 = i7;
					break;
				case 9:
					j2 = i7;
					break;
				case 10:
					k2 = i7;
					break;
				case 11:
					l2 = i7;
					break;
				case 12:
					i3 = i7;
					break;
				case 13:
					j3 = i7;
					break;
				case 14:
					k3 = i7;
					break;
				case 15:
					l3 = i7;
					break;
				case 16:
					i4 = i7;
					break;
				case 17:
					j4 = i7;
					break;
				case 18:
					k4 = i7;
					break;
				case 19:
					l4 = i7;
					break;
				case 20:
					i5 = i7;
					break;
				case 21:
					j5 = i7;
					break;
				case 22:
					k5 = i7;
					break;
				case 23:
					l5 = i7;
					break;
				case 24:
					i6 = i7;
					break;
				case 25:
					dk3 = i7;
					break;
				case 26:
					bw1 = i7;
					break;
				case 27:
					bw2 = i7;
					break;
				case 28:
					bw3 = i7;
					break;
				case 29:
					wr0 = i7;
					break;
				case 30:
					wr1 = i7;
					break;
				case 31:
					wr2 = i7;
					break;
				case 32:
					wr3 = i7;
					break;
				}
			}
		}
		if (pc.isSkillMastery(i)) {
			pc.sendPackets(new S_SystemMessage("이미 배운 마법입니다."), true);
			return;
		}
		int k6 = pc.getId();
		pc.sendPackets(new S_AddSkill(j, k, l, i1, j1, k1, l1, i2, j2, k2, l2,
				i3, j3, k3, l3, i4, j4, k4, l4, i5, j5, k5, l5, i6, dk3, bw1,
				bw2, bw3, wr0, wr1, wr2, wr3, pc.getElfAttr()), true);
		S_SkillSound s_skillSound = new S_SkillSound(k6, 224);// 231
		pc.sendPackets(s_skillSound);
		Broadcaster.broadcastPacket(pc, s_skillSound, true);
		SkillsTable.getInstance().spellMastery(k6, i, s, 0, 0);
		pc.getInventory().removeItem(l1iteminstance, 1);
	}

	private void SpellBook2(L1PcInstance pc, L1ItemInstance l1iteminstance) {
		String s = "";
		int i = 0;
		int j = 0;
		int k = 0;
		int l = 0;
		int i1 = 0;
		int j1 = 0;
		int k1 = 0;
		int l1 = 0;
		int i2 = 0;
		int j2 = 0;
		int k2 = 0;
		int l2 = 0;
		int i3 = 0;
		int j3 = 0;
		int k3 = 0;
		int l3 = 0;
		int i4 = 0;
		int j4 = 0;
		int k4 = 0;
		int l4 = 0;
		int i5 = 0;
		int j5 = 0;
		int k5 = 0;
		int l5 = 0;
		int i6 = 0;
		int dk3 = 0;
		int bw1 = 0;
		int bw2 = 0;
		int bw3 = 0;
		L1Skills l1skills = null;
		for (int j6 = 129; j6 <= 179; j6++) {
			l1skills = SkillsTable.getInstance().getTemplate(j6);
			String s1 = "정령의 수정 (" + l1skills.getName() + ")";
			if (l1iteminstance.getItem().getName().equalsIgnoreCase(s1)) {
				if (!pc.isGm() && l1skills.getAttr() != 0 && pc.getElfAttr() != l1skills.getAttr()) {
					if (pc.getElfAttr() == 0 || pc.getElfAttr() == 1 || pc.getElfAttr() == 2 || pc.getElfAttr() == 4 || pc.getElfAttr() == 8) { // 속성치가 이상한 경우는 전속성을 기억할 수 있도록(듯이) 해 둔다
						pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."));
						return;
					}
				}
				int l6 = l1skills.getSkillLevel();
				int i7 = l1skills.getId();
				s = l1skills.getName();
				i = l1skills.getSkillId();
				switch (l6) {
				case 1:
					j = i7;
					break;
				case 2:
					k = i7;
					break;
				case 3:
					l = i7;
					break;
				case 4:
					i1 = i7;
					break;
				case 5:
					j1 = i7;
					break;
				case 6:
					k1 = i7;
					break;
				case 7:
					l1 = i7;
					break;
				case 8:
					i2 = i7;
					break;
				case 9:
					j2 = i7;
					break;
				case 10:
					k2 = i7;
					break;
				case 11:
					l2 = i7;
					break;
				case 12:
					i3 = i7;
					break;
				case 13:
					j3 = i7;
					break;
				case 14:
					k3 = i7;
					break;
				case 15:
					l3 = i7;
					break;
				case 16:
					i4 = i7;
					break;
				case 17:
					j4 = i7;
					break;
				case 18:
					k4 = i7;
					break;
				case 19:
					l4 = i7;
					break;
				case 20:
					i5 = i7;
					break;
				case 21:
					j5 = i7;
					break;
				case 22:
					k5 = i7;
					break;
				case 23:
					l5 = i7;
					break;
				case 24:
					i6 = i7;
					break;
				case 25:
					dk3 = i7;
					break;
				case 26:
					bw1 = i7;
					break;
				case 27:
					bw2 = i7;
					break;
				case 28:
					bw3 = i7;
					break;

				}
			}
		}
		if (pc.isSkillMastery(i)) {
			pc.sendPackets(new S_SystemMessage("이미 배운 마법입니다."), true);
			return;
		}
		int k6 = pc.getId();
		pc.sendPackets(new S_AddSkill(j, k, l, i1, j1, k1, l1, i2, j2, k2, l2,
				i3, j3, k3, l3, i4, j4, k4, l4, i5, j5, k5, l5, i6, dk3, bw1,
				bw2, bw3, 0, 0, 0, 0, pc.getElfAttr()), true);
		S_SkillSound s_skillSound = new S_SkillSound(k6, 224);
		pc.sendPackets(s_skillSound);
		Broadcaster.broadcastPacket(pc, s_skillSound, true);
		SkillsTable.getInstance().spellMastery(k6, i, s, 0, 0);
		pc.getInventory().removeItem(l1iteminstance, 1);
	}

	private void SpellBook3(L1PcInstance pc, L1ItemInstance l1iteminstance) {
		String s = "";
		int i = 0;
		int j = 0;
		int k = 0;
		int l = 0;
		int i1 = 0;
		int j1 = 0;
		int k1 = 0;
		int l1 = 0;
		int i2 = 0;
		int j2 = 0;
		int k2 = 0;
		int l2 = 0;
		int i3 = 0;
		int j3 = 0;
		int k3 = 0;
		int l3 = 0;
		int i4 = 0;
		int j4 = 0;
		int k4 = 0;
		int l4 = 0;
		int i5 = 0;
		int j5 = 0;
		int k5 = 0;
		int l5 = 0;
		int i6 = 0;
		int dk3 = 0;
		int bw1 = 0;
		int bw2 = 0;
		int bw3 = 0;
		L1Skills l1skills = null;
		for (int j6 = 87; j6 <= 94; j6++) {
			l1skills = SkillsTable.getInstance().getTemplate(j6);

			String s1 = (new StringBuilder()).append("기술서 (").append(l1skills.getName()).append(")").toString();
			if (l1iteminstance.getItem().getName().equalsIgnoreCase(s1)) {
				int l6 = l1skills.getSkillLevel();
				int i7 = l1skills.getId();
				s = l1skills.getName();

				i = l1skills.getSkillId();
				switch (l6) {
				case 1:
					j = i7;
					break;
				case 2:
					k = i7;
					break;
				case 3:
					l = i7;
					break;
				case 4:
					i1 = i7;
					break;
				case 5:
					j1 = i7;
					break;
				case 6:
					k1 = i7;
					break;
				case 7:
					l1 = i7;
					break;
				case 8:
					i2 = i7;
					break;
				case 9:
					j2 = i7;
					break;
				case 10:
					k2 = i7;
					break;
				case 11:
					l2 = i7;
					break;
				case 12:
					i3 = i7;
					break;
				case 13:
					j3 = i7;
					break;
				case 14:
					k3 = i7;
					break;
				case 15:
					l3 = i7;
					break;
				case 16:
					i4 = i7;
					break;
				case 17:
					j4 = i7;
					break;
				case 18:
					k4 = i7;
					break;
				case 19:
					l4 = i7;
					break;
				case 20:
					i5 = i7;
					break;
				case 21:
					j5 = i7;
					break;
				case 22:
					k5 = i7;
					break;
				case 23:
					l5 = i7;
					break;
				case 24:
					i6 = i7;
					break;
				case 25:
					dk3 = i7;
					break;
				case 26:
					bw1 = i7;
					break;
				case 27:
					bw2 = i7;
					break;
				case 28:
					bw3 = i7;
					break;

				}
			}
		}
		if (pc.isSkillMastery(i)) {
			pc.sendPackets(new S_SystemMessage("이미 배운 마법입니다."), true);
			return;
		}
		int k6 = pc.getId();
		pc.sendPackets(new S_AddSkill(j, k, l, i1, j1, k1, l1, i2, j2, k2, l2,
				i3, j3, k3, l3, i4, j4, k4, l4, i5, j5, k5, l5, i6, dk3, bw1,
				bw2, bw3, 0, 0, 0, 0, pc.getElfAttr()), true);
		S_SkillSound s_skillSound = new S_SkillSound(k6, 224);
		pc.sendPackets(s_skillSound);
		Broadcaster.broadcastPacket(pc, s_skillSound, true);
		SkillsTable.getInstance().spellMastery(k6, i, s, 0, 0);
		pc.getInventory().removeItem(l1iteminstance, 1);
	}

	private void SpellBook4(L1PcInstance pc, L1ItemInstance l1iteminstance) {
		String s = "";
		int i = 0;
		int j = 0;
		int k = 0;
		int l = 0;
		int i1 = 0;
		int j1 = 0;
		int k1 = 0;
		int l1 = 0;
		int i2 = 0;
		int j2 = 0;
		int k2 = 0;
		int l2 = 0;
		int i3 = 0;
		int j3 = 0;
		int k3 = 0;
		int l3 = 0;
		int i4 = 0;
		int j4 = 0;
		int k4 = 0;
		int l4 = 0;
		int i5 = 0;
		int j5 = 0;
		int k5 = 0;
		int l5 = 0;
		int i6 = 0;
		int dk3 = 0;
		int bw1 = 0;
		int bw2 = 0;
		int bw3 = 0;
		L1Skills l1skills = null;
		for (int j6 = 113; j6 <= 126; j6++) {
			l1skills = SkillsTable.getInstance().getTemplate(j6);
			String s1 = "마법서 (" + l1skills.getName() + ")";
			if (l1iteminstance.getItem().getName().equalsIgnoreCase(s1)) {
				int l6 = l1skills.getSkillLevel();
				int i7 = l1skills.getId();
				s = l1skills.getName();
				i = l1skills.getSkillId();
				switch (l6) {
				case 1:
					j = i7;
					break;
				case 2:
					k = i7;
					break;
				case 3:
					l = i7;
					break;
				case 4:
					i1 = i7;
					break;
				case 5:
					j1 = i7;
					break;
				case 6:
					k1 = i7;
					break;
				case 7:
					l1 = i7;
					break;
				case 8:
					i2 = i7;
					break;
				case 9:
					j2 = i7;
					break;
				case 10:
					k2 = i7;
					break;
				case 11:
					l2 = i7;
					break;
				case 12:
					i3 = i7;
					break;
				case 13:
					j3 = i7;
					break;
				case 14:
					k3 = i7;
					break;
				case 15:
					l3 = i7;
					break;
				case 16:
					i4 = i7;
					break;
				case 17:
					j4 = i7;
					break;
				case 18:
					k4 = i7;
					break;
				case 19:
					l4 = i7;
					break;
				case 20:
					i5 = i7;
					break;
				case 21:
					j5 = i7;
					break;
				case 22:
					k5 = i7;
					break;
				case 23:
					l5 = i7;
					break;
				case 24:
					i6 = i7;
					break;
				case 25:
					dk3 = i7;
					break;
				case 26:
					bw1 = i7;
					break;
				case 27:
					bw2 = i7;
					break;
				case 28:
					bw3 = i7;
					break;

				}
			}
		}
		if (pc.isSkillMastery(i)) {
			pc.sendPackets(new S_SystemMessage("이미 배운 마법입니다."), true);
			return;
		}
		int k6 = pc.getId();
		pc.sendPackets(new S_AddSkill(j, k, l, i1, j1, k1, l1, i2, j2, k2, l2, i3, j3, k3, l3, i4, j4, k4, l4, i5, j5, k5, l5, i6, dk3, bw1, bw2, bw3, 0, 0, 0, 0, pc.getElfAttr()), true);
		S_SkillSound s_skillSound = new S_SkillSound(k6, 224);
		pc.sendPackets(s_skillSound);
		Broadcaster.broadcastPacket(pc, s_skillSound, true);
		SkillsTable.getInstance().spellMastery(k6, i, s, 0, 0);
		pc.getInventory().removeItem(l1iteminstance, 1);
	}

	private void SpellBook5(L1PcInstance pc, L1ItemInstance l1iteminstance) {
		String s = "";
		int i = 0;
		int j = 0;
		int k = 0;
		int l = 0;
		int i1 = 0;
		int j1 = 0;
		int k1 = 0;
		int l1 = 0;
		int i2 = 0;
		int j2 = 0;
		int k2 = 0;
		int l2 = 0;
		int i3 = 0;
		int j3 = 0;
		int k3 = 0;
		int l3 = 0;
		int i4 = 0;
		int j4 = 0;
		int k4 = 0;
		int l4 = 0;
		int i5 = 0;
		int j5 = 0;
		int k5 = 0;
		int l5 = 0;
		int i6 = 0;
		int dk3 = 0;
		int bw1 = 0;
		int bw2 = 0;
		int bw3 = 0;
		L1Skills l1skills = null;
		for (int j6 = 181; j6 < 200; j6++) {
			l1skills = SkillsTable.getInstance().getTemplate(j6);
			String s1 = "용기사의 서판(" + l1skills.getName() + ")";
			if (l1iteminstance.getItem().getName().equalsIgnoreCase(s1)) {
				int l6 = l1skills.getSkillLevel();
				int i7 = l1skills.getId();
				s = l1skills.getName();
				i = l1skills.getSkillId();
				switch (l6) {
				case 1:
					j = i7;
					break;
				case 2:
					k = i7;
					break;
				case 3:
					l = i7;
					break;
				case 4:
					i1 = i7;
					break;
				case 5:
					j1 = i7;
					break;
				case 6:
					k1 = i7;
					break;
				case 7:
					l1 = i7;
					break;
				case 8:
					i2 = i7;
					break;
				case 9:
					j2 = i7;
					break;
				case 10:
					k2 = i7;
					break;
				case 11:
					l2 = i7;
					break;
				case 12:
					i3 = i7;
					break;
				case 13:
					j3 = i7;
					break;
				case 14:
					k3 = i7;
					break;
				case 15:
					l3 = i7;
					break;
				case 16:
					i4 = i7;
					break;
				case 17:
					j4 = i7;
					break;
				case 18:
					k4 = i7;
					break;
				case 19:
					l4 = i7;
					break;
				case 20:
					i5 = i7;
					break;
				case 21:
					j5 = i7;
					break;
				case 22:
					k5 = i7;
					break;
				case 23:
					l5 = i7;
					break;
				case 24:
					i6 = i7;
					break;
				case 25:
					dk3 = i7;
					break;
				case 26:
					bw1 = i7;
					break;
				case 27:
					bw2 = i7;
					break;
				case 28:
					bw3 = i7;
					break;

				}
			}
		}
		if (pc.isSkillMastery(i)) {
			pc.sendPackets(new S_SystemMessage("이미 배운 마법입니다."), true);
			return;
		}
		int k6 = pc.getId();
		pc.sendPackets(new S_AddSkill(j, k, l, i1, j1, k1, l1, i2, j2, k2, l2,
				i3, j3, k3, l3, i4, j4, k4, l4, i5, j5, k5, l5, i6, dk3, bw1,
				bw2, bw3, 0, 0, 0, 0, pc.getElfAttr()), true);
		S_SkillSound s_skillSound = new S_SkillSound(k6, 224);
		pc.sendPackets(s_skillSound);
		Broadcaster.broadcastPacket(pc, s_skillSound, true);
		SkillsTable.getInstance().spellMastery(k6, i, s, 0, 0);
		pc.getInventory().removeItem(l1iteminstance, 1);
	}

	private void SpellBook6(L1PcInstance pc, L1ItemInstance l1iteminstance) {
		String s = "";
		int i = 0;
		int j = 0;
		int k = 0;
		int l = 0;
		int i1 = 0;
		int j1 = 0;
		int k1 = 0;
		int l1 = 0;
		int i2 = 0;
		int j2 = 0;
		int k2 = 0;
		int l2 = 0;
		int i3 = 0;
		int j3 = 0;
		int k3 = 0;
		int l3 = 0;
		int i4 = 0;
		int j4 = 0;
		int k4 = 0;
		int l4 = 0;
		int i5 = 0;
		int j5 = 0;
		int k5 = 0;
		int l5 = 0;
		int i6 = 0;
		int dk3 = 0;
		int bw1 = 0;
		int bw2 = 0;
		int bw3 = 0;
		int wr1 = 0;
		int wr2 = 0;
		int wr3 = 0;
		int wr4 = 0;
		
		for (int j6 = 201; j6 < 225; j6++) {
			L1Skills l1skills = SkillsTable.getInstance().getTemplate(j6);
			String s1 = "기억의 수정(" + l1skills.getName() + ")";
			if (l1iteminstance.getItem().getName().equalsIgnoreCase(s1)) {
				int l6 = l1skills.getSkillLevel();
				int i7 = l1skills.getId();
				s = l1skills.getName();
				i = l1skills.getSkillId();
				switch (l6) {
				case 1:
					j = i7;
					break;
				case 2:
					k = i7;
					break;
				case 3:
					l = i7;
					break;
				case 4:
					i1 = i7;
					break;
				case 5:
					j1 = i7;
					break;
				case 6:
					k1 = i7;
					break;
				case 7:
					l1 = i7;
					break;
				case 8:
					i2 = i7;
					break;
				case 9:
					j2 = i7;
					break;
				case 10:
					k2 = i7;
					break;
				case 11:
					l2 = i7;
					break;
				case 12:
					i3 = i7;
					break;
				case 13:
					j3 = i7;
					break;
				case 14:
					k3 = i7;
					break;
				case 15:
					l3 = i7;
					break;
				case 16:
					i4 = i7;
					break;
				case 17:
					j4 = i7;
					break;
				case 18:
					k4 = i7;
					break;
				case 19:
					l4 = i7;
					break;
				case 20:
					i5 = i7;
					break;
				case 21:
					j5 = i7;
					break;
				case 22:
					k5 = i7;
					break;
				case 23:
					l5 = i7;
					break;
				case 24:
					i6 = i7;
					break;
				case 25:
					dk3 = i7;
					break;
				case 26:
					bw1 = i7;
					break;
				case 27:
					bw2 = i7;
					break;
				case 28:
					bw3 = i7;
					break;
				case 29:
					wr1 = i7;
					break;
				case 30:
					wr2 = i7;
					break;
				case 31:
					wr3 = i7;
					break;
				case 32:
					wr4 = i7;
					break;
				}
			}
		}
		if (pc.isSkillMastery(i)) {
			pc.sendPackets(new S_SystemMessage("이미 배운 마법입니다."), true);
			return;
		}
		int k6 = pc.getId();
		pc.sendPackets(new S_AddSkill(j, k, l, i1, j1, k1, l1, i2, j2, k2, l2,
				i3, j3, k3, l3, i4, j4, k4, l4, i5, j5, k5, l5, i6, dk3, bw1,
				bw2, bw3, wr1, wr2, wr3, wr4, pc.getElfAttr()), true);
		S_SkillSound s_skillSound = new S_SkillSound(k6, 224);
		pc.sendPackets(s_skillSound);
		Broadcaster.broadcastPacket(pc, s_skillSound, true);
		SkillsTable.getInstance().spellMastery(k6, i, s, 0, 0);
		pc.getInventory().removeItem(l1iteminstance, 1);
	}
	
}

package l1j.server.server.model.item.function;

import l1j.server.server.clientpackets.ClientBasePacket;
import l1j.server.server.model.Broadcaster;
import l1j.server.server.model.L1Character;
import l1j.server.server.model.L1Teleport;
import l1j.server.server.model.Instance.L1ItemInstance;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.serverpackets.S_Paralysis;
import l1j.server.server.serverpackets.S_SkillSound;
import l1j.server.server.serverpackets.S_SystemMessage;
import l1j.server.server.templates.L1Item;

@SuppressWarnings("serial")
public class TelBook extends L1ItemInstance {
	public TelBook(L1Item item) {
		super(item);
	}

	private static final int[][] TownAddBook = { { 34060, 32281, 4 }, // 오렌
			{ 33079, 33390, 4 }, // 은기사
			{ 32750, 32439, 4 }, // 오크숲
			{ 32612, 33188, 4 }, // 윈다우드
			{ 33720, 32492, 4 }, // 웰던
			{ 32872, 32912, 304 }, // 침묵의 동굴
			{ 32612, 32781, 4 }, // 글루디오
			{ 33067, 32803, 4 }, // 켄트
			{ 33933, 33358, 4 }, // 아덴
			{ 33601, 33232, 4 }, // 하이네
			{ 32574, 32942, 0 }, // 말하는 섬
			{ 33430, 32815, 4 } }; // 기란

	private static final int[][] DungeonAddBook = { { 32797, 32799, 101 }, // 오만1
			// { 32809, 32723, 7 }, // 본던1
			{ 32764, 32842, 77 }, // 오렌
			{ 32745, 32852, 59 }, // 에바왕국1층
			{ 32776, 32732, 45 }, // 개미굴
			{ 32681, 32877, 450 }, // 고대 거인의 무덤

			// { 32577, 32674, 400 }, // 대공동 저항군
			// { 32920, 32798, 430 }, // 정무
			{ 32929, 32994, 410 }, // 마족
			{ 34266, 32190, 4 }, // 그신
			{ 32758, 33459, 4 }, // 욕망
			{ 32680, 32803, 450 }, // 라던중앙광장
			{ 32843, 32694, 550 } }; // 선박의 무덤 수면

	private static final int[][] OmanTowerAddBook = { { 32731, 32798, 101 }, // 오만1
			{ 32730, 32802, 102 }, // 오만2
			{ 32726, 32802, 103 }, // 오만3
			{ 32620, 32858, 104 }, // 오만4
			{ 32602, 32866, 105 }, // 오만5
			{ 32611, 32862, 106 }, // 오만6
			{ 32618, 32865, 107 }, // 오만7
			{ 32602, 32866, 108 }, // 오만8
			{ 32613, 32866, 109 }, // 오만9
			{ 32730, 32802, 110 }, // 오만10
			{ 32638, 32805, 111 }, // 오만 정상1
			{ 32773, 32810, 111 } }; // 오만 정상2
	
	private static final int[][] OmanTower2AddBook = { { 32731, 32798, 101 }, // 오만1
			{ 32730, 32802, 102 }, // 오만2
			{ 32726, 32802, 103 }, // 오만3
			{ 32620, 32858, 104 }, // 오만4
			{ 32602, 32866, 105 }, // 오만5
			{ 32611, 32862, 106 }, // 오만6
			{ 32618, 32865, 107 }, // 오만7
			{ 32602, 32866, 108 }, // 오만8
			{ 32613, 32866, 109 }, // 오만9
			{ 32730, 32802, 110 }, // 오만10
			{ 32638, 32805, 111 }, // 오만 정상1
			{ 32799, 32963, 111 } }; // 오만 정상2
	
	private static final int[][] JibaeTower2AddBook = { 
			{ 32730, 32800, 12852 }, // 지배1 아.
			{ 32722, 32803, 12853 }, // 지배2
			{ 32727, 32802, 12854 }, // 지배3
			{ 32598, 32863, 12855 }, // 지배4
			{ 32594, 32866, 12856 }, // 지배5
			{ 32602, 32865, 12857 }, // 지배6
			{ 32603, 32866, 12858 }, // 지배7
			{ 32594, 32866, 12859 }, // 지배8
			{ 32602, 32866, 12860 }, // 지배9
			{ 32733, 32802, 12861 }}; // 지배10
	
	private static final int[][] JibaeTower1AddBook = { 
			{ 32819, 32800, 12852 }, // 지배1 아.
			{ 32801, 32798, 12853 }, // 지배2
			{ 32801, 32795, 12854 }, // 지배3
			{ 32672, 32863, 12855 }, // 지배4
			{ 32669, 32868, 12856 }, // 지배5
			{ 32671, 32854, 12857 }, // 지배6
			{ 32672, 32853, 12858 }, // 지배7
			{ 32669, 32843, 12859 }, // 지배8
			{ 32678, 32851, 12860 }, // 지배9
			{ 32801, 32786, 12861 }}; // 지배10

	private static final int[][] JouAddBook = {
	      { 00000, 00000, 0 }, // 저 레벨 추천사냥터
	      { 32680, 32851, 9 }, // 말섬 부쪽섬 
	      { 32492, 32854, 9 }, // 말섬 던전 입구
	      { 32382, 32947, 9 }, // 말섬 오크 망루 지대
	      { 32882, 32652, 4}, //본토 죽음의 폐허
	      { 32886, 32917, 4 }, // 본토 망자의 무덤
	      { 32725, 32925, 4 }, // 글루디오 던전 1층
	      { 32764, 32842, 77 }, // 상아탑 4층 입구
	      { 32707, 33147, 9 }, // 말섬 흑기사 전초 기지
	      { 32630, 32260, 4 }, // 본토 오크 부락
	      { 32904, 33236, 4 }, // 본토 사막(에르자베)
	      { 32724, 33138, 4 }, // 본토 사막(샌드웜)
	      { 32805, 32724, 19 }, // 요정 숲 던전 1층
	      { 32801, 32754, 809 }, // 글루디오 던전 3층
	      { 33429, 32825, 4 }, // 기란 감옥 입구
	      { 32809, 32729, 25 }, // 수련던전 1층
	      { 00000, 00000, 0 }, // 중 레벨 추천 사냥터
	      { 33768, 33408, 4 }, // 본토 거울의 숲
	      { 33796, 32983, 4 }, // 본토 밀림 지대
	      { 32708, 32789, 59 }, // 에바 왕국 1층
	      { 34252, 33453, 4 }, // 오만의 탑 입구
	      { 32891, 32942, 4 }, // 본토 흑기사 출몰 지역
	      { 32766, 32796, 20 }, // 요정 숲 던전 2층
	      { 32727, 32808, 61 }, // 에바 왕국 3층
	      { 32810, 32810, 30 }, // 용의 던전 1층
	      { 32809, 32766, 27 }, // 수련 던전 3층
	      { 00000, 00000, 0 }, // 고 레벨 추천 사냥터
	      
	      { 32709, 32817, 32 }, // 용의 던전 3층
	      { 33432, 33472, 4 }, // 하이네 잊혀진 섬 배표소
	      
	      { 33336, 33007, 4 }, // 본토 암흑용의 상흔
	     
	      { 34124, 32796, 4 }, // 본토 풍룡의 둥지 입구 
	      
	      { 34123, 32191, 4 }, // 본토 얼음 설벽
	      
	      { 33330, 32457, 4 }, // 본토 용의 계곡 입구
	      
	      { 34051, 32519, 4 }, // 본토 엘모어 격전지
	      
	      { 33645, 32421, 4 }, // 본토 화룡의 둥지 입구 

	      }; // 시장

	@Override
	public void clickItem(L1Character cha, ClientBasePacket packet) {
		if (cha instanceof L1PcInstance) {
			L1PcInstance pc = (L1PcInstance) cha;
			L1ItemInstance useItem = pc.getInventory().getItem(this.getId());
			int itemId = useItem.getItemId();
			if (pc.getMapId() == 99 || pc.getMapId() == 6202 || pc.getMapId() == 1708 || pc.getMapId() == 1703 || pc.getMapId() == 1710||pc.getMapId() == 2004) {
				pc.sendPackets(new S_SystemMessage("주위의 마력에의해 순간이동을 사용할 수 없습니다."), true);
				pc.sendPackets(new S_Paralysis(S_Paralysis.TYPE_TELEPORT_UNLOCK, false), true);
				return;
			}
			if(!pc.getInventory().checkItem(39100) &&!pc.getInventory().checkItem(391026) && pc.getMapId() >= 12852 && pc.getMapId() <= 12862){
				pc.sendPackets(new S_SystemMessage("지배의 탑에서는 사용할 수 없습니다."), true);
				pc.sendPackets(new S_Paralysis(S_Paralysis.TYPE_TELEPORT_UNLOCK, false), true);
				return;
			}
			int BookTel = packet.readC();
			switch (itemId) {
			case 560025:
			case 560026:
				int[] TownAddBookList = TownAddBook[BookTel];
				if (TownAddBookList != null) {
					S_SkillSound packe = null;
					packe = new S_SkillSound(pc.getId(), 169);
					pc.sendPackets(packe);
					Broadcaster.broadcastPacket(pc, packe, true);
					L1Teleport.teleport(pc, TownAddBookList[0], TownAddBookList[1], (short) TownAddBookList[2], 3, false, 0);
					pc.getInventory().removeItem(useItem, 1);
				}
				break;
			case 560027:
				int[] DungeonAddBookList = DungeonAddBook[BookTel];
				if (DungeonAddBookList != null) {
					S_SkillSound packe = null;
					packe = new S_SkillSound(pc.getId(), 169);
					pc.sendPackets(packe);
					Broadcaster.broadcastPacket(pc, packe, true);
					L1Teleport.teleport(pc, DungeonAddBookList[0], DungeonAddBookList[1], (short) DungeonAddBookList[2], 3, false, 0);
					pc.getInventory().removeItem(useItem, 1);
				}
				break;
			case 560028:
				int[] OmanTowerAddBookList = OmanTowerAddBook[BookTel];
				if (OmanTowerAddBookList != null) {
					S_SkillSound packe = null;
					packe = new S_SkillSound(pc.getId(), 169);
					pc.sendPackets(packe);
					Broadcaster.broadcastPacket(pc, packe, true);
					L1Teleport.teleport(pc, OmanTowerAddBookList[0], OmanTowerAddBookList[1], (short) OmanTowerAddBookList[2], 3, false, 0);
					pc.getInventory().removeItem(useItem, 1);
				}
				break;
			case 60203:
				int[] OmanAddBookList = OmanTower2AddBook[BookTel];
				if (OmanAddBookList != null) {
					S_SkillSound packe = null;
					packe = new S_SkillSound(pc.getId(), 169);
					pc.sendPackets(packe);
					Broadcaster.broadcastPacket(pc, packe, true);
					L1Teleport.teleport(pc, OmanAddBookList[0],OmanAddBookList[1], (short) OmanAddBookList[2], 3, false, 0);
				}
				break;
			case 60360:// 조우의 이동 기억책
				int[] jouAddBookList = JouAddBook[BookTel];
				if (jouAddBookList != null) {
					S_SkillSound packe = new S_SkillSound(pc.getId(), 169);
					pc.sendPackets(packe);
					Broadcaster.broadcastPacket(pc, packe, true);

					L1Teleport.teleport(pc, jouAddBookList[0],
							jouAddBookList[1], (short) jouAddBookList[2], 3,
							false, 0);
					pc.getInventory().removeItem(useItem, 1);
				}
				break;
				
			
			case 39100:
				int[] JibaeAddBookList = JibaeTower2AddBook[BookTel];
				if (JibaeAddBookList != null) {
					if((pc.getMapId() >= 12852 && pc.getMapId() <= 12862) || pc.isGm()){
					    S_SkillSound packe = null;
					    packe = new S_SkillSound(pc.getId(), 169);
					    pc.sendPackets(packe);
					    Broadcaster.broadcastPacket(pc, packe, true);
					    L1Teleport.teleport(pc, JibaeAddBookList[0],JibaeAddBookList[1], (short) JibaeAddBookList[2], 3, false, 0);
				    } else {
				    	if (!pc.isGm()) {
						    pc.sendPackets(new S_SystemMessage("지배의 탑에서만 사용할 수 있습니다."), true);
				    	}
				    	break;
				
				    	
					}

				}
				break;
				
			//by황인환 PS:라이칸 코더//
			case 391026:
				int[] JibaeAddBookListTEst = JibaeTower1AddBook[BookTel];
				if (JibaeAddBookListTEst != null) {
					if( (pc.getMapId() == 4)|| pc.isGm()){
					    S_SkillSound packe = null;
					    packe = new S_SkillSound(pc.getId(), 169);
					    pc.sendPackets(packe);
					    Broadcaster.broadcastPacket(pc, packe, true);
					    L1Teleport.teleport(pc, JibaeAddBookListTEst[0], JibaeAddBookListTEst[1], (short) JibaeAddBookListTEst[2], 3, true, 0);
				    } else {
				    	/** 지엠이 아니면 메세지 아니라면 그냥 리턴 */
						pc.sendPackets(new S_SystemMessage("마을에서만 사용할 수 있습니다."), true);
					}
				}
				break;
			}
		}
	}
}

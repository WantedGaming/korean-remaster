/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */

package l1j.server.server.model.item.function;

import l1j.server.server.clientpackets.ClientBasePacket;
import l1j.server.server.model.L1Character;
import l1j.server.server.model.Instance.L1ItemInstance;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.serverpackets.S_CharStatElixir;
import l1j.server.server.serverpackets.S_OwnCharStatus2;
import l1j.server.server.serverpackets.S_PacketBox;
import l1j.server.server.serverpackets.S_ServerMessage;
import l1j.server.server.serverpackets.S_SystemMessage;
import l1j.server.server.templates.L1EtcItem;
import l1j.server.server.templates.L1Item;

@SuppressWarnings("serial")
public class Elixir extends L1ItemInstance {

	public Elixir(L1Item item) {
		super(item);
	}

	@Override
	public void clickItem(L1Character cha, ClientBasePacket packet) {
		try {
			if (cha instanceof L1PcInstance) {
				L1PcInstance pc = (L1PcInstance) cha;
				L1ItemInstance useItem = pc.getInventory().getItem(this.getId());
				int itemId = useItem.getItemId();
				int maxstats = 45;
				if(pc.getLevel() >= 90){
					maxstats = 50;
				}
				if(pc.getAbility().getElixirCount() >= 6){
					pc.sendPackets(new S_SystemMessage(pc,"더이상 복용할 수 없습니다."));
					return;
				}
				int levelCount = (pc.getLevel() - 45) / 5;
				
				if(levelCount < 1){
					pc.sendPackets(new S_SystemMessage(pc,"사용할 수 없습니다."));
					return;
				}
				
				if(levelCount <= pc.getAbility().getElixirCount()){
					pc.sendPackets(new S_SystemMessage(pc,"사용할 수 없습니다."));
					return;
				}
				
				switch (itemId) {
				case 40033:
					if (pc.getAbility().getStr() < maxstats) {
						pc.getAbility().addStr((byte) 1);
						pc.getAbility().setElixirCount(pc.getAbility().getElixirCount() + 1);
						pc.getInventory().removeItem(useItem, 1);
						pc.sendPackets(new S_OwnCharStatus2(pc), true);
						pc.save();
						pc.sendPackets(new S_CharStatElixir(pc)); // 엘릭서 표기
					} else {
						pc.sendPackets(new S_ServerMessage(4473), true); 
					}
					break;
				case 40034:
					if (pc.getAbility().getCon() < maxstats) {
						pc.getAbility().addCon((byte) 1);
						pc.getAbility().setElixirCount(pc.getAbility().getElixirCount() + 1);
						pc.getInventory().removeItem(useItem, 1);
						pc.sendPackets(new S_OwnCharStatus2(pc), true);
						pc.save();
						pc.sendPackets(new S_CharStatElixir(pc)); // 엘릭서 표기
					} else {
						pc.sendPackets(new S_ServerMessage(4473), true); 
					}
					break;
				case 40035:
					if (pc.getAbility().getDex() < maxstats) {
						pc.getAbility().addDex((byte) 1);
						pc.resetBaseAc();
						pc.getAbility().setElixirCount(
								pc.getAbility().getElixirCount() + 1);
						pc.getInventory().removeItem(useItem, 1);
						pc.sendPackets(new S_OwnCharStatus2(pc), true);
						pc.sendPackets(
								new S_PacketBox(S_PacketBox.char_ER, pc
										.get_PlusEr()), true);
						pc.save();
						pc.sendPackets(new S_CharStatElixir(pc)); // 엘릭서 표기
					} else {
						pc.sendPackets(new S_ServerMessage(4473), true); 
					}
					break;
				case 40036:
					if (pc.getAbility().getInt() < maxstats) {
						pc.getAbility().addInt((byte) 1);
						pc.getAbility().setElixirCount(
								pc.getAbility().getElixirCount() + 1);
						pc.getInventory().removeItem(useItem, 1);
						pc.sendPackets(new S_OwnCharStatus2(pc), true);
						pc.save();
						pc.sendPackets(new S_CharStatElixir(pc)); // 엘릭서 표기
					} else {
						pc.sendPackets(new S_ServerMessage(4473), true); 
					}
					break;
				case 40037:
					if (pc.getAbility().getWis() < maxstats) {
						pc.getAbility().addWis((byte) 1);
						pc.resetBaseMr();
						pc.getAbility().setElixirCount(
								pc.getAbility().getElixirCount() + 1);
						pc.getInventory().removeItem(useItem, 1);
						pc.sendPackets(new S_OwnCharStatus2(pc), true);
						pc.save();
						pc.sendPackets(new S_CharStatElixir(pc)); // 엘릭서 표기
					} else {
						pc.sendPackets(new S_ServerMessage(4473), true); 
					}
					break;
				case 40038:
					if (pc.getAbility().getCha() < maxstats) {
						pc.getAbility().addCha((byte) 1);
						pc.getAbility().setElixirCount(pc.getAbility().getElixirCount() + 1);
						pc.getInventory().removeItem(useItem, 1);
						pc.sendPackets(new S_OwnCharStatus2(pc), true);
						pc.save();
						pc.sendPackets(new S_CharStatElixir(pc)); // 엘릭서 표기
					} else {
						pc.sendPackets(new S_ServerMessage(4473), true); 
					}
					break;
				}
			}
		} catch (Exception e) {
		}
	}
}

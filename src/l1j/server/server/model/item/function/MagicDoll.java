/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */

package l1j.server.server.model.item.function;

import l1j.server.Config;
import l1j.server.server.clientpackets.ClientBasePacket;
//import l1j.server.server.model.L1CastleLocation;
import l1j.server.server.datatables.NpcTable;
import l1j.server.server.model.Broadcaster;
import l1j.server.server.model.L1Character;
import l1j.server.server.model.L1PcInventory;
import l1j.server.server.model.Instance.L1DollInstance;
import l1j.server.server.model.Instance.L1ItemInstance;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.model.Instance.S_DollIcon;
import l1j.server.server.model.item.L1ItemId;
import l1j.server.server.model.skill.L1SkillId;
import l1j.server.server.serverpackets.S_ACTION_UI;
import l1j.server.server.serverpackets.S_ItemName;
import l1j.server.server.serverpackets.S_OwnCharStatus;
import l1j.server.server.serverpackets.S_PacketBox;
import l1j.server.server.serverpackets.S_ServerMessage;
import l1j.server.server.serverpackets.S_SkillIconGFX;
import l1j.server.server.serverpackets.S_SkillSound;
import l1j.server.server.serverpackets.S_SystemMessage;
import l1j.server.server.templates.L1Item;
import l1j.server.server.templates.L1Npc;

@SuppressWarnings("serial")
public class MagicDoll extends L1ItemInstance {

	public MagicDoll(L1Item item) {
		super(item);
	}

	@Override
	public void clickItem(L1Character cha, ClientBasePacket packet) {
		
		if (cha instanceof L1PcInstance) {
			L1PcInstance pc = (L1PcInstance) cha;
			int itemId = this.getItemId();
			useMagicDoll(pc, itemId, this.getId());
		}
	}

	private void useMagicDoll(L1PcInstance pc, int itemId, int itemObjectId) {
		if (pc.isInvisble()) {
			return;
		}
		
		L1ItemInstance item = pc.getInventory().getItem(itemObjectId);
		
		long curtime = System.currentTimeMillis() / 1000;
		if (pc.getQuizTime3() + 3 > curtime) return;
		
		boolean isAppear = true;
		
		L1DollInstance doll = null;
		for (Object dollObject : pc.getDollList()) {
			doll = (L1DollInstance) dollObject;
			if (doll.getItemObjId() == itemObjectId) { // 이미 꺼내고 있는 매직 실업 수당
				isAppear = false;
				break;
			}
		}

		if (isAppear) {
			int npcId = 0;
			int dollType = 0;
			int consumecount = 0;
			int dollTime = 0;
			switch (itemId) {
			case L1ItemId.DOLL_BUGBEAR:
				npcId = 80106;
				dollType = L1DollInstance.DOLLTYPE_BUGBEAR;
				consumecount = 50;
				dollTime = 1800;
				break;
			case L1ItemId.DOLL_SUCCUBUS:
				npcId = 80107;
				dollType = L1DollInstance.DOLLTYPE_SUCCUBUS;
				consumecount = 50;
				dollTime = 1800;
				break;
			case L1ItemId.DOLL_WAREWOLF:
				npcId = 80108;
				dollType = L1DollInstance.DOLLTYPE_WAREWOLF;
				consumecount = 50;
				dollTime = 1800;
				break;
			case L1ItemId.DOLL_STONEGOLEM:
				npcId = 4500150;
				dollType = L1DollInstance.DOLLTYPE_STONEGOLEM;
				consumecount = 50;
				dollTime = 1800;
				break;
			case L1ItemId.DOLL_ELDER:
				npcId = 4500151;
				dollType = L1DollInstance.DOLLTYPE_ELDER;
				consumecount = 50;
				dollTime = 1800;
				break;
			case L1ItemId.DOLL_CRUSTACEA:
				npcId = 4500152;
				dollType = L1DollInstance.DOLLTYPE_CRUSTACEA;
				consumecount = 50;
				dollTime = 1800;
				break;
			case L1ItemId.DOLL_SEADANCER:
				npcId = 4500153;
				dollType = L1DollInstance.DOLLTYPE_SEADANCER;
				consumecount = 50;
				dollTime = 1800;
				break;
			case L1ItemId.DOLL_SNOWMAN:
				npcId = 4500154;
				dollType = L1DollInstance.DOLLTYPE_SNOWMAN;
				consumecount = 50;
				dollTime = 1800;
				break;
			case L1ItemId.DOLL_COCATRIS:
				npcId = 4500155;
				dollType = L1DollInstance.DOLLTYPE_COCATRIS;
				consumecount = 50;
				dollTime = 1800;
				break;
			case L1ItemId.DOLL_DRAGON_M:
				npcId = 4500156;
				dollType = L1DollInstance.DOLLTYPE_DRAGON_M;
				consumecount = 50;
				dollTime = 18000;
				break;
			case L1ItemId.DOLL_DRAGON_W:
				npcId = 4500157;
				dollType = L1DollInstance.DOLLTYPE_DRAGON_W;
				consumecount = 50;
				dollTime = 18000;
				break;
			case L1ItemId.DOLL_HIGH_DRAGON_M:
				npcId = 4500158;
				dollType = L1DollInstance.DOLLTYPE_HIGH_DRAGON_M;
				consumecount = 50;
				dollTime = 18000;
				break;
			case L1ItemId.DOLL_HIGH_DRAGON_W:
				npcId = 4500159;
				dollType = L1DollInstance.DOLLTYPE_HIGH_DRAGON_W;
				consumecount = 50;
				dollTime = 18000;
				break;
			case L1ItemId.DOLL_LAMIA:
				npcId = 4500160;
				dollType = L1DollInstance.DOLLTYPE_LAMIA;
				consumecount = 50;
				dollTime = 1800;
				break;
			case L1ItemId.DOLL_SPATOI:
				npcId = 4500161;
				dollType = L1DollInstance.DOLLTYPE_SPATOI;
				consumecount = 50;
				dollTime = 1800;
				break;
			case 500202: // 1,800초 동안 근거리 대미지+2,스턴 내성+12,근거리 명중+2
				npcId = 1500202;
				dollType = L1DollInstance.DOLLTYPE_사이클롭스;
				consumecount = 50;
				dollTime = 1800;
				break;
			case 500203: // 1,800초 동안 경험치 보너스+10%, 대미지 리덕션+1
				npcId = 1500203;
				dollType = L1DollInstance.DOLLTYPE_자이언트;
				consumecount = 50;
				dollTime = 1800;
				break;

			case 500204: // 1,800초 동안 64초마다 MP 회복 +15, 공격 시 일정 확률로 콜 라이트닝 발동
				npcId = 1500204;
				dollType = L1DollInstance.DOLLTYPE_흑장로;
				consumecount = 50;
				dollTime = 1800;
				break;
			case 500205: // 1,800초 동안 64초마다 MP 15 회복, SP+1
				npcId = 1500205;
				dollType = L1DollInstance.DOLLTYPE_서큐퀸;
				consumecount = 50;
				dollTime = 1800;
				break;

			case 141919:// 라미아
				npcId = 4500160;
				dollType = L1DollInstance.DOLLTYPE_LAMIA;
				consumecount = 50;
				dollTime = 1800;
				break;
			case 141920:// 스파토이
				npcId = 4500161;
				dollType = L1DollInstance.DOLLTYPE_SPATOI;
				consumecount = 50;
				dollTime = 1800;
				break;
			case 141922:// 에틴
				npcId = 45000161;
				dollType = L1DollInstance.DOLLTYPE_에틴;
				consumecount = 50;
				dollTime = 1800;
				break;
			case 141921:// 허수아비
				npcId = 41915;
				dollType = L1DollInstance.DOLLTYPE_HUSUABI;
				consumecount = 50;
				dollTime = 1800;
				break;
			case 141918:// 시댄서
				npcId = 4500153;
				dollType = L1DollInstance.DOLLTYPE_SEADANCER;
				consumecount = 50;
				dollTime = 1800;
				break;

			case 500108:// 인어
				npcId = 1500108;
				dollType = L1DollInstance.DOLLTYPE_인어;
				consumecount = 50;
				dollTime = 1800;
				break;
			case 500109:// 눈사람
				npcId = 1500110;
				dollType = L1DollInstance.DOLLTYPE_눈사람;
				consumecount = 50;
				dollTime = 1800;
				break;

			case 500110:// 킹버그
				npcId = 1500109;
				dollType = L1DollInstance.DOLLTYPE_킹_버그베어;
				consumecount = 50;
				dollTime = 1800;
				break;

			case 600234:// 킹버그
				npcId = 1600234;
				dollType = L1DollInstance.DOLLTYPE_이벤트인형;
				consumecount = 50;
				dollTime = 1800;
				break;

			case 600241:// 목각
				npcId = 1600241;
				dollType = L1DollInstance.DOLLTYPE_목각;
				consumecount = 50;
				dollTime = 1800;
				break;
			case 600242:// 라바
				npcId = 1600242;
				dollType = L1DollInstance.DOLLTYPE_라바골렘;
				consumecount = 50;
				dollTime = 1800;
				break;
			case 600243:// 다골
				npcId = 1600243;
				dollType = L1DollInstance.DOLLTYPE_다이아몬드골렘;
				consumecount = 50;
				dollTime = 1800;
				break;
			case 600244:// 시어
				npcId = 1600244;
				dollType = L1DollInstance.DOLLTYPE_시어;
				consumecount = 50;
				dollTime = 1800;
				break;
			case 600245:// 나발
				npcId = 1600245;
				dollType = L1DollInstance.DOLLTYPE_나이트발드;
				consumecount = 50;
				dollTime = 1800;
				break;
			case 600246:// 데몬
				npcId = 1600246;
				dollType = L1DollInstance.DOLLTYPE_데몬;
				consumecount = 50;
				dollTime = 1800;
				break;
				
			case 600247:// 데스
				npcId = 1600247;
				dollType = L1DollInstance.DOLLTYPE_데스나이트;
				consumecount = 50;
				dollTime = 1800;
				break;

			case L1ItemId.DOLL_GremRin:
				npcId = 100882;
				dollType = L1DollInstance.DOLLTYPE_그렘린;
				consumecount = 50;
				dollTime = 1800;
				break;

			case L1ItemId.DOLL_ETIN:
				npcId = 45000161;
				dollType = L1DollInstance.DOLLTYPE_에틴;
				consumecount = 50;
				dollTime = 1800;
				break;

			case L1ItemId.DOLL_RICH:
				npcId = 45000162;
				dollType = L1DollInstance.DOLLTYPE_RICH;
				consumecount = 50;
				dollTime = 1800;
				break;

			case L1ItemId.DOLL_PHENIX:
				npcId = 45000163;
				dollType = L1DollInstance.DOLLTYPE_ETIN;
				consumecount = 50;
				dollTime = 1800;
				break;
			case 500144: // 눈사람(A)
				npcId = 700196;
				dollType = L1DollInstance.DOLLTYPE_SNOWMAN_A;
				consumecount = 50;
				dollTime = 36000;
				break;
			case 500145: // 눈사람(B)
				npcId = 700197;
				dollType = L1DollInstance.DOLLTYPE_SNOWMAN_B;
				consumecount = 50;
				dollTime = 1800;
				break;
			case 500146: // 눈사람(C)
				npcId = 700198;
				dollType = L1DollInstance.DOLLTYPE_SNOWMAN_C;
				consumecount = 50;
				dollTime = 1800;
				break;
			case 41915:
				npcId = 41915;
				dollType = L1DollInstance.DOLLTYPE_HUSUABI;
				consumecount = 50;
				dollTime = 1800;
				break;

			case 141915:
				npcId = 141915;
				dollType = L1DollInstance.DOLLTYPE_HW_HUSUABI;
				consumecount = 50;
				dollTime = 1800;
				break;

			case 141916:
				npcId = 101033;
				dollType = L1DollInstance.DOLLTYPE_튼튼한기사;
				consumecount = 50;
				dollTime = 1800;
				break;

			case 437018:
				npcId = 4000009;
				dollType = L1DollInstance.DOLLTYPE_HELPER;
				consumecount = 50;
				dollTime = 300;
				break;
			case 60173:
				npcId = 100320;
				dollType = L1DollInstance.DOLLTYPE_블레그;
				consumecount = 10;
				dollTime = 1800;
				break;
			case 60174:
				npcId = 100321;
				dollType = L1DollInstance.DOLLTYPE_레데그;
				consumecount = 10;
				dollTime = 1800;
				break;
			case 60175:
				npcId = 100322;
				dollType = L1DollInstance.DOLLTYPE_엘레그;
				consumecount = 10;
				dollTime = 1800;
				break;
			case 60176:
				npcId = 100323;
				dollType = L1DollInstance.DOLLTYPE_그레그;
				consumecount = 10;
				dollTime = 1800;
				break;
			case 60261:
				npcId = 100431;
				dollType = L1DollInstance.DOLLTYPE_싸이;
				consumecount = 50;
				dollTime = 1800;
				break;
			case 60262:
				npcId = 100432;
				dollType = L1DollInstance.DOLLTYPE_싸이;
				consumecount = 50;
				dollTime = 1800;
				break;
			case 60263:
				npcId = 100433;
				dollType = L1DollInstance.DOLLTYPE_싸이;
				consumecount = 50;
				dollTime = 1800;
				break;
			case 60309:
				npcId = 100579;
				dollType = L1DollInstance.DOLLTYPE_단디;
				consumecount = 50;
				dollTime = 1800;
				break;
			case 60310:
				npcId = 100580;
				dollType = L1DollInstance.DOLLTYPE_쎄리;
				consumecount = 50;
				dollTime = 1800;
				break;
			case 60324:
				npcId = 100604;
				dollType = L1DollInstance.DOLLTYPE_드레이크;
				consumecount = 50;
				dollTime = 1800;
				break;
			case 60447:
				npcId = 100677;
				dollType = L1DollInstance.DOLLTYPE_남자_여자;
				consumecount = 500;
				dollTime = 18000;
				break;
			case 60448:
				npcId = 100678;
				dollType = L1DollInstance.DOLLTYPE_남자_여자;
				consumecount = 500;
				dollTime = 18000;
				break;
			case 60449:
				npcId = 100679;
				dollType = L1DollInstance.DOLLTYPE_남자_여자;
				consumecount = 500;
				dollTime = 18000;
				break;
			case 60450:
				npcId = 100680;
				dollType = L1DollInstance.DOLLTYPE_남자_여자;
				consumecount = 500;
				dollTime = 18000;
				break;
			case 60451:
				npcId = 100681;
				dollType = L1DollInstance.DOLLTYPE_남자_여자;
				consumecount = 500;
				dollTime = 18000;
				break;
			case 60452:
				npcId = 100682;
				dollType = L1DollInstance.DOLLTYPE_남자_여자;
				consumecount = 500;
				dollTime = 18000;
				break;
			case 60453:
				npcId = 100683;
				dollType = L1DollInstance.DOLLTYPE_남자_여자;
				consumecount = 500;
				dollTime = 18000;
				break;
			case 60454:
				npcId = 100684;
				dollType = L1DollInstance.DOLLTYPE_남자_여자;
				consumecount = 500;
				dollTime = 18000;
				break;
			case 60455:
				npcId = 100685;
				dollType = L1DollInstance.DOLLTYPE_남자_여자;
				consumecount = 500;
				dollTime = 18000;
				break;
			case 60456:
				npcId = 100686;
				dollType = L1DollInstance.DOLLTYPE_남자_여자;
				consumecount = 500;
				dollTime = 18000;
				break;
			case 60457:
				npcId = 100687;
				dollType = L1DollInstance.DOLLTYPE_남자_여자;
				consumecount = 500;
				dollTime = 18000;
				break;
			case 60458:
				npcId = 100688;
				dollType = L1DollInstance.DOLLTYPE_남자_여자;
				consumecount = 500;
				dollTime = 18000;
				break;
			case 60459:
				npcId = 100689;
				dollType = L1DollInstance.DOLLTYPE_남자_여자;
				consumecount = 500;
				dollTime = 18000;
				break;
			case 60460:
				npcId = 100690;
				dollType = L1DollInstance.DOLLTYPE_남자_여자;
				consumecount = 500;
				dollTime = 18000;
				break;
			case 4470150:
				npcId = 4000009;
				dollType = L1DollInstance.DOLLTYPE_HELPER;
				consumecount = 50;
				dollTime = 1800;
				break;
			case 4470160:
				npcId = 4000010;
				dollType = L1DollInstance.DOLLTYPE_HELPER;
				consumecount = 50;
				dollTime = 1800;
				break;
			case 41550:
				npcId = 4000110;
				dollType = L1DollInstance.DOLLTYPE_옥토끼;
				consumecount = 50;
				dollTime = 7200;
				break;
			case 41551:
				npcId = 4000111;
				dollType = L1DollInstance.DOLLTYPE_뱀파이어;
				consumecount = 50;
				dollTime = 1800;
				break;
			case 41552:
				npcId = 4000112;
				dollType = L1DollInstance.DOLLTYPE_아이리스;
				consumecount = 50;
				dollTime = 1800;
				break;
			case 41553:
				npcId = 4000113;
				dollType = L1DollInstance.DOLLTYPE_바란카;
				consumecount = 50;
				dollTime = 1800;
				break;
			case 49000:
				npcId = 4000114;
				dollType = L1DollInstance.DOLLTYPE_몽크;
				consumecount = 50;
				dollTime = 1800;
				break;
			case 19005:
				npcId = 410115;
				dollType = L1DollInstance.DOLLTYPE_타락;
				consumecount = 50;
				dollTime = 7200;
				break;
			case 19006:
				npcId = 410116;
				dollType = L1DollInstance.DOLLTYPE_머미로드;
				consumecount = 50;
				dollTime = 7200;
				break;
			case 19007:
				npcId = 410117;
				dollType = L1DollInstance.DOLLTYPE_바포메트;
				consumecount = 50;
				dollTime = 7200;
				break;
			case 19008:
				npcId = 410118;
				dollType = L1DollInstance.DOLLTYPE_얼음여왕;
				consumecount = 50;
				dollTime = 7200;
				break;
			case 19009:
				npcId = 410119;
				dollType = L1DollInstance.DOLLTYPE_커츠;
				consumecount = 50;
				dollTime = 7200;
				break;
			case 19010:
				npcId = 410155;
				dollType = L1DollInstance.DOLLTYPE_붉은닭;
				consumecount = 50;
				dollTime = 7200;
				break;
			case 73001:
				npcId = 410156;
				dollType = L1DollInstance.DOLLTYPE_안타라스;
				consumecount = 50;
				dollTime = 7200;
				break;
			case 73002:
				npcId = 410157;
				dollType = L1DollInstance.DOLLTYPE_파푸리온;
				consumecount = 50;
				dollTime = 7200;
				break;
			case 73003:
				npcId = 410158;
				dollType = L1DollInstance.DOLLTYPE_린드비오르;
				consumecount = 50;
				dollTime = 7200;
				break;
			case 73004:
				npcId = 410159;
				dollType = L1DollInstance.DOLLTYPE_발라카스;
				consumecount = 50;
				dollTime = 7200;
				break;
			case 73100:
				npcId = 4510161;
				dollType = L1DollInstance.DOLLTYPE_수련자;
				consumecount = 50;
				dollTime = 1800;
				break;
				
			case 950000:
				npcId = 410124;
				dollType = L1DollInstance.DOLLTYPE_축서큐버스;
				consumecount = 50;
				dollTime = 1800;
				break;
			case 950001:
				npcId = 410125;
				dollType = L1DollInstance.DOLLTYPE_축흑장로;
				consumecount = 50;
				dollTime = 1800;
				break;
			case 950002:
				npcId = 410126;
				dollType = L1DollInstance.DOLLTYPE_축자이언트;
				consumecount = 50;
				dollTime = 1800;
				break;
			case 950003:
				npcId = 410127;
				dollType = L1DollInstance.DOLLTYPE_축드레이크;
				consumecount = 50;
				dollTime = 1800;
				break;
			case 950004:
				npcId = 410128;
				dollType = L1DollInstance.DOLLTYPE_축킹버그베어;
				consumecount = 50;
				dollTime = 1800;
				break;
			case 950005:
				npcId = 410129;
				dollType = L1DollInstance.DOLLTYPE_축다이아몬드골렘;
				consumecount = 50;
				dollTime = 1800;
				break;
			case 950006:
				npcId = 410130;
				dollType = L1DollInstance.DOLLTYPE_축사이클롭스;
				consumecount = 50;
				dollTime = 1800;
				break;
			case 950007:
				npcId = 410131;
				dollType = L1DollInstance.DOLLTYPE_축리치;
				consumecount = 50;
				dollTime = 1800;
				break;
			case 950008:
				npcId = 410132;
				dollType = L1DollInstance.DOLLTYPE_축나이트발드;
				consumecount = 50;
				dollTime = 1800;
				break;
			case 950009:
				npcId = 410133;
				dollType = L1DollInstance.DOLLTYPE_축시어;
				consumecount = 50;
				dollTime = 1800;
				break;
			case 950010:
				npcId = 410134;
				dollType = L1DollInstance.DOLLTYPE_축아이리스;
				consumecount = 50;
				dollTime = 1800;
				break;
			case 950011:
				npcId = 410135;
				dollType = L1DollInstance.DOLLTYPE_축뱀파이어;
				consumecount = 50;
				dollTime = 1800;
				break;
			case 950012:
				npcId = 410136;
				dollType = L1DollInstance.DOLLTYPE_축머미로드;
				consumecount = 50;
				dollTime = 1800;
				break;
			case 950013:
				npcId = 410137;
				dollType = L1DollInstance.DOLLTYPE_축데몬;
				consumecount = 50;
				dollTime = 1800;
				break;
			case 950014:
				npcId = 410138;
				dollType = L1DollInstance.DOLLTYPE_축데스나이트;
				consumecount = 50;
				dollTime = 1800;
				break;
			case 950015:
				npcId = 410139;
				dollType = L1DollInstance.DOLLTYPE_축바란카;
				consumecount = 50;
				dollTime = 1800;
				break;
			case 950016:
				npcId = 410140;
				dollType = L1DollInstance.DOLLTYPE_축타락;
				consumecount = 50;
				dollTime = 1800;
				break;
			case 950017:
				npcId = 410141;
				dollType = L1DollInstance.DOLLTYPE_축바포메트;
				consumecount = 50;
				dollTime = 1800;
				break;
			case 950018:
				npcId = 410142;
				dollType = L1DollInstance.DOLLTYPE_축얼음여왕;
				consumecount = 50;
				dollTime = 1800;
				break;
			case 950019:
				npcId = 410143;
				dollType = L1DollInstance.DOLLTYPE_축커츠;
				consumecount = 50;
				dollTime = 1800;
				break;
			}

			if (!pc.getInventory().checkItem(41246, consumecount)) {
				pc.sendPackets(new S_ServerMessage(337, "$5240"), true);
				return;
			}
			if (pc.getDollListSize() >= 1) {
				doll.deleteDoll();
				pc.sendPackets(new S_OwnCharStatus(pc), true);
				pc.sendPackets(new S_PacketBox(S_PacketBox.char_ER, pc.get_PlusEr()), true);
			}
			L1Npc template = NpcTable.getInstance().getTemplate(npcId);
			doll = new L1DollInstance(template, pc, dollType, itemObjectId, dollTime * 1000);
			pc.sendPackets(new S_SkillSound(doll.getId(), 5935), true);
			Broadcaster.broadcastPacket(pc, new S_SkillSound(doll.getId(), 5935), true);
			// pc.sendPackets(new S_SkillIconGFX(56, dollTime, doll.getGfxId().getGfxId(),
			// itemObjectId), true);
			pc.sendPackets(new S_DollIcon(item, 1800));
			pc.sendPackets(new S_OwnCharStatus(pc), true);
			pc.sendPackets(new S_PacketBox(S_PacketBox.char_ER, pc.get_PlusEr()), true);
			pc.sendPackets(new S_ServerMessage(1143), true);
			pc.setQuizTime3(curtime);
			pc.getInventory().consumeItem(41246, consumecount);
		} else {
		
		}
			pc.sendPackets(new S_OwnCharStatus(pc));
		}
	}

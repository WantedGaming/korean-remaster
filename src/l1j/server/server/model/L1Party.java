package l1j.server.server.model;

import java.util.ArrayList;
import java.util.List;

import l1j.server.Config;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.model.skill.L1SkillId;
import l1j.server.server.serverpackets.S_Party;
import l1j.server.server.serverpackets.S_ServerMessage;
import server.controller.PcPartyRefreshThread;

public class L1Party {

	private final List<L1PcInstance> _membersList = new ArrayList<L1PcInstance>();

	private L1PcInstance _leader = null;

	public void addBattleZonePartyMember(L1PcInstance pc) {
		if (pc == null) {
			throw new NullPointerException();
		}

		if (_membersList.isEmpty()) {
			setLeader(pc);
		}

		_membersList.add(pc);
		pc.setParty(this);
		showAddPartyInfo(pc);
		PcPartyRefreshThread.getInstance().addPlayer(pc);
	}

	public void addMember(L1PcInstance pc) {
		if (pc == null) {
			throw new NullPointerException();
		}
		if (_membersList.size() == Config.MAX_PT && !_leader.isGm() || _membersList.contains(pc)) {
			pc.sendPackets(new S_ServerMessage(417));
			return;
		}

		if (_membersList.isEmpty()) {
			setLeader(pc);
		}

		_membersList.add(pc);
		pc.setParty(this);
		pc.sendPackets(new S_Party(S_Party.OPCODE_TYPE_PARTY_MEMBER_LIST, S_Party.PACKET_NONE, pc));
		showAddPartyInfo(pc);
		PcPartyRefreshThread.getInstance().addPlayer(pc);
	}

	private void removeMember(L1PcInstance pc) {
		if (!_membersList.contains(pc)) {
			return;
		}

		if (pc.getParty().isLeader(pc) && getMembers().length > 2) {
			passLeader(_membersList.get(1));
		}

		_membersList.remove(pc);
		pc.setParty(null);

		int[] delete_skill = { L1SkillId.CUBE_AVATAR, L1SkillId.CUBE_GOLEM, L1SkillId.CUBE_OGRE, L1SkillId.CUBE_LICH };
		for (Integer i : delete_skill) {
			if (pc.getSkillEffectTimerSet().hasSkillEffect(i)) {
				pc.getSkillEffectTimerSet().removeSkillEffect(i);
			}
		}
	}

	public boolean isVacancy() {
		return _membersList.size() < Config.MAX_PT;
	}

	public int getVacancy() {
		return Config.MAX_PT - _membersList.size();
	}

	public boolean isMember(L1PcInstance pc) {
		return _membersList.contains(pc);
	}

	private void setLeader(L1PcInstance pc) {
		_leader = pc;
	}

	public L1PcInstance getLeader() {
		return _leader;
	}

	public boolean isLeader(L1PcInstance pc) {
		return pc.getId() == _leader.getId();
	}

	public boolean isAutoDivision(L1PcInstance pc) {
		return pc.getPartyType() == 1 || pc.getPartyType() == 4;
	}

	public String getMembersNameList() {
		String _result = new String("");
		for (L1PcInstance pc : _membersList) {
			_result = _result + pc.getName() + " ";
		}
		return _result;
	}

	private void showAddPartyInfo(L1PcInstance pc) {
		for (L1PcInstance member : getMembers()) {
			if (pc.getId() == member.getId()) 
				continue;
			member.sendPackets(new S_Party(S_Party.OPCODE_TYPE_PARTY_MEMBER_LIST_CHANGE, S_Party.PACKET_TYPE_NEW_MEMBER, pc));
			member.sendPackets(new S_Party(S_Party.OPCODE_TYPE_PARTY_OPERATION_RESULT_NOTI, S_Party.JOIN_YN_OK, pc));
		}
	}

	public void updateMiniHP(L1PcInstance pc) {
		for (L1PcInstance member : getMembers()) {
	/*		if (pc.getId() == member.getId()) 
				continue;*/
			member.sendPackets(new S_Party(S_Party.OPCODE_TYPE_PARTY_MEMBER_STATUS, S_Party.MEMBER_HPMP_CHANGE, pc));
		}
	}

	public void passLeader(L1PcInstance pc) { // 리더위임
		for (L1PcInstance member : getMembers()) {
			member.getParty().setLeader(pc);
			member.sendPackets(new S_Party(S_Party.OPCODE_TYPE_PARTY_MEMBER_LIST_CHANGE, S_Party.PACKET_TYPE_CHANGELEADER, pc));
		}
	}

	public void leaveMember(L1PcInstance pc) {
		if (getMembers().length == 2) {
			for (L1PcInstance member : getMembers()) {
				member.sendPackets(new S_Party(S_Party.OPCODE_TYPE_PARTY_MEMBER_LIST_CHANGE, S_Party.PACKET_TYPE_LEAVE_MEMBER, member));
				member.sendPackets(new S_ServerMessage(418));
				removeMember(member);
			}
		} else {
			for (L1PcInstance member : getMembers()) {
				member.sendPackets(new S_Party(S_Party.OPCODE_TYPE_PARTY_MEMBER_LIST_CHANGE, S_Party.PACKET_TYPE_LEAVE_MEMBER, pc));
			}
			removeMember(pc);
		}
	}

	public void refresh(L1PcInstance pc) {// 파티추가
		pc.sendPackets(new S_Party(S_Party.OPCODE_TYPE_PARTY_SYNC_PERIODIC_INFO, S_Party.PACKET_NONE, pc));
	}

	public void kickMember(L1PcInstance pc) { // 리더추방

		for (L1PcInstance member : getMembers()) {
			member.sendPackets(new S_Party(S_Party.OPCODE_TYPE_PARTY_MEMBER_LIST_CHANGE, S_Party.PACKET_TYPE_LEAVE_MEMBER, pc));
		}

		removeMember(pc);
		pc.sendPackets(new S_ServerMessage(419)); // 파티로부터 추방되었습니다.
	}

	public L1PcInstance[] getMembers() {
		return _membersList.toArray(new L1PcInstance[_membersList.size()]);
	}

	public int getNumOfMembers() {
		return _membersList.size();
	}

	public List<L1PcInstance> getList() {// 하딘 파티 멤버 리스트
		return _membersList;
	}

}

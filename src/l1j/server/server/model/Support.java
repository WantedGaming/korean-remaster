/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */

package l1j.server.server.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.logging.Level;
import java.util.logging.Logger;

import l1j.server.L1DatabaseFactory;
import l1j.server.server.ObjectIdFactory;
import l1j.server.server.datatables.ItemTable;
import l1j.server.server.model.Instance.L1ItemInstance;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.templates.L1Item;
import l1j.server.server.utils.SQLUtil;

// Referenced classes of package l1j.server.server.model:
// L1PcInstance

public class Support {

	private static Logger _log = Logger.getLogger(Support.class.getName());

	private static Support _instance;

	public static Support getInstance() {
		if (_instance == null) {
			_instance = new Support();
		}
		return _instance;
	}

	private Support() {
	}

	public int GiveItem(L1PcInstance pc) {
		Connection con = null;
		PreparedStatement pstm1 = null;
		ResultSet rs = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm1 = con.prepareStatement("SELECT * FROM support WHERE activate IN(?,?)");

			pstm1.setString(1, "A");
			if (pc.isCrown()) {
				pstm1.setString(2, "P");
			} else if (pc.isKnight()) {
				pstm1.setString(2, "K");
			} else if (pc.isElf()) {
				pstm1.setString(2, "E");
			} else if (pc.isWizard()) {
				pstm1.setString(2, "W");
			} else if (pc.isDarkelf()) {
				pstm1.setString(2, "D");
			} else if (pc.isDragonknight()) {
				pstm1.setString(2, "T");
			} else if (pc.isIllusionist()) {
				pstm1.setString(2, "B");
			} else if (pc.isWarrior()) {
				pstm1.setString(2, "R");
			} else {
				pstm1.setString(2, "A");
			}

			rs = pstm1.executeQuery();
			PreparedStatement pstm2 = null;
			while (rs.next()) {
				try {
					int count = rs.getInt("count");
					if (count <= 0)
						continue;
					pstm2 = con.prepareStatement("INSERT INTO character_items SET id=?, item_id=?, char_id=?, item_name=?, count=?, is_equipped=?, enchantlvl=?, is_id=?, durability=?, charge_count=?, remaining_time=?, last_used=?, bless=?, attr_enchantlvl=?, end_time=?");
					pstm2.setInt(1, ObjectIdFactory.getInstance().nextId());
					int itemid = rs.getInt("item_id");
					pstm2.setInt(2, itemid);
					pstm2.setInt(3, pc.getId());
					pstm2.setString(4, rs.getString("item_name"));
					pstm2.setInt(5, count);
					pstm2.setInt(6, 0);
					pstm2.setInt(7, rs.getInt("enchantlvl"));
					pstm2.setInt(8, 1);
					pstm2.setInt(9, 0);
					pstm2.setInt(10, rs.getInt("charge_count"));
					L1Item tempItem = ItemTable.getInstance().getTemplate(itemid);
					if (tempItem.getType2() == 0 && tempItem.getType() == 2) { // light
						pstm2.setInt(11, tempItem.getLightFuel());
					} else
						pstm2.setInt(11, 0);
					pstm2.setTimestamp(12, null);
					pstm2.setInt(13, rs.getInt("bless"));
					pstm2.setInt(14, 0);
					Timestamp deleteTime = null;
					if ((itemid >= 425000 && itemid <= 425002) || // 엘모어 방어구
							(itemid >= 450000 && itemid <= 450007)) { // 엘모어 무기
						deleteTime = new Timestamp(System.currentTimeMillis() + (3600000 * 24 * 3));// 3일
					} else if ((itemid >= 21099 && itemid <= 21112)
							|| itemid == 21254 || itemid == 7 || itemid == 35
							|| itemid == 48 || itemid == 73 || itemid == 105
							|| itemid == 120 || itemid == 147 || itemid == 7232
							|| itemid == 156 || itemid == 174 || itemid == 175
							|| itemid == 224) {
						// System.out.println("1111111111111111");
						deleteTime = new Timestamp(System.currentTimeMillis() + (3600000 * 24 * 7));// 3일
					}
					pstm2.setTimestamp(15, deleteTime);
					pstm2.executeUpdate();
				} catch (SQLException e2) {
					_log.log(Level.SEVERE, e2.getLocalizedMessage(), e2);
				} finally {
					SQLUtil.close(pstm2);
				}
			}
		} catch (SQLException e1) {
			_log.log(Level.SEVERE, e1.getLocalizedMessage(), e1);
		} finally {
			SQLUtil.close(rs);
			SQLUtil.close(pstm1);
			SQLUtil.close(con);
		}
		return 0;
	}

	public void GiveItem(L1PcInstance pc, L1ItemInstance item, boolean eq) {
		// TODO 자동 생성된 메소드 스텁
		Connection con = null;
		PreparedStatement pstm2 = null;
		int itemid = item.getItemId();
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm2 = con.prepareStatement("INSERT INTO character_items SET id=?, item_id=?, char_id=?, char_name=?, item_name=?, count=?, is_equipped=?, enchantlvl=?, is_id=?, durability=?, charge_count=?, remaining_time=?, last_used=?, bless=?, attr_enchantlvl=?,  end_time=?");
			pstm2.setInt(1, ObjectIdFactory.getInstance().nextId());
			pstm2.setInt(2, item.getItemId());
			pstm2.setInt(3, pc.getId());
			pstm2.setInt(3, pc.getId());
			pstm2.setString(4, pc.getName());
			pstm2.setString(5, item.getName());
			pstm2.setInt(6, item.getCount());
			pstm2.setInt(7, 0);// eq ? 1 : 0);
			pstm2.setInt(8, item.getEnchantLevel());
			pstm2.setInt(9, 1);
			pstm2.setInt(10, 0);
			pstm2.setInt(11, item.getChargeCount());

			if (item.getItem().getType2() == 0 && item.getItem().getType() == 2) { // light
				pstm2.setInt(12, item.getItem().getLightFuel());
			} else
				pstm2.setInt(12, 0);

			pstm2.setTimestamp(13, null);
			pstm2.setInt(14, item.getBless());
			pstm2.setInt(15, 0);
			Timestamp deleteTime = null;
			if ((itemid >= 21099 && itemid <= 21112) || itemid == 21254
					|| itemid == 7 || itemid == 35 || itemid == 48
					|| itemid == 73 || itemid == 105 || itemid == 120
					|| itemid == 147 || itemid == 7232 || itemid == 156
					|| itemid == 174 || itemid == 175 || itemid == 224) {
				deleteTime = new Timestamp(System.currentTimeMillis() + 3600000 * 24 * 7);// 3일
			}
			pstm2.setTimestamp(16, deleteTime);
			pstm2.executeUpdate();
		} catch (SQLException e2) {
			_log.log(Level.SEVERE, e2.getLocalizedMessage(), e2);
		} finally {
			SQLUtil.close(pstm2);
			SQLUtil.close(con);
		}
	}



	public int GiveItem2(L1PcInstance pc) {
		Connection con = null;
		PreparedStatement pstm1 = null;
		ResultSet rs = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm1 = con.prepareStatement("SELECT * FROM support WHERE activate IN(?,?)");
			pstm1.setString(1, "A");
			if (pc.isCrown()) {
				pstm1.setString(2, "P");
			} else if (pc.isKnight()) {
				pstm1.setString(2, "K");
			} else if (pc.isElf()) {
				pstm1.setString(2, "E");
			} else if (pc.isWizard()) {
				pstm1.setString(2, "W");
			} else if (pc.isDarkelf()) {
				pstm1.setString(2, "D");
			} else if (pc.isDragonknight()) {
				pstm1.setString(2, "T");
			} else if (pc.isIllusionist()) {
				pstm1.setString(2, "B");
			} else if (pc.isWarrior()) {
				pstm1.setString(2, "R");
			} else {
				pstm1.setString(2, "A");
			}

			rs = pstm1.executeQuery();
			PreparedStatement pstm2 = null;
			while (rs.next()) {
				try {

					int count = rs.getInt("count");
					if (count <= 0)
						continue;
					int itemid = rs.getInt("item_id");

					L1ItemInstance item = ItemTable.getInstance().createItem(itemid);
					item.setCount(count);
					item.setEnchantLevel(rs.getInt("enchantlvl"));
					item.setIdentified(true);
					item.setChargeCount(rs.getInt("charge_count"));

					pc.getInventory().storeItem(item);

				} catch (SQLException e2) {
					_log.log(Level.SEVERE, e2.getLocalizedMessage(), e2);
				} finally {
					SQLUtil.close(pstm2);
				}
			}
		} catch (SQLException e1) {
			_log.log(Level.SEVERE, e1.getLocalizedMessage(), e1);
		} finally {
			SQLUtil.close(rs);
			SQLUtil.close(pstm1);
			SQLUtil.close(con);
		}
		return 0;
	}
}
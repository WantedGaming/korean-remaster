/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */

package l1j.server.server.model.Instance;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Timer;

import l1j.server.Config;
import l1j.server.server.GeneralThreadPool;
import l1j.server.server.clientpackets.ClientBasePacket;
import l1j.server.server.datatables.ArmorSetTable;
import l1j.server.server.datatables.NpcTable;
import l1j.server.server.datatables.PetTable;
import l1j.server.server.model.L1Character;
import l1j.server.server.model.L1EquipmentTimer;
import l1j.server.server.model.L1ItemOwnerTimer;
import l1j.server.server.model.L1Object;
import l1j.server.server.model.L1PcInventory;
import l1j.server.server.model.item.L1ItemId;
import l1j.server.server.model.skill.L1SkillId;
import l1j.server.server.serverpackets.S_OwnCharStatus;
import l1j.server.server.serverpackets.S_PacketBox;
import l1j.server.server.templates.L1Armor;
import l1j.server.server.templates.L1ArmorSets;
import l1j.server.server.templates.L1Item;
import l1j.server.server.templates.L1Npc;
import l1j.server.server.templates.L1Pet;
import l1j.server.server.utils.BinaryOutputStream;
import l1j.server.server.utils.ItemPresentOutStream;

//Referenced classes of package l1j.server.server.model:
//L1Object, L1PcInstance

public class L1ItemInstance extends L1Object {

	private static final long serialVersionUID = 1L;
	private static final SimpleDateFormat sdf = new SimpleDateFormat("MM-dd HH:mm", Locale.KOREA);

	private int _count;
	private int _class = -1;

	private Timer _inntimer;
	public boolean _isSecond = false;
	private int _itemId;
	private L1Item _item;
	private boolean _isEquipped = false;
	private int _enchantLevel;
	private int _attrenchantLevel;
	private int _stepenchantLevel; // 단계강화주문서
	private boolean _isIdentified = false;
	private int _durability;
	private int _chargeCount;
	private int _remainingTime;
	private Timestamp _lastUsed = null;

	private String _encobjid = null;

	private int _blessEnchantLevel; // 축복 주문서 3 단계 시스템 by K

	private int bless;
	private int _lastWeight;
	private final LastStatus _lastStatus = new LastStatus();
	private L1PcInstance _pc;
	public boolean _isRunning = false;
	private EnchantTimer _timer;
	private Timestamp _buyTime = null;
	private Timestamp _endTime = null;
	private boolean _demon_bongin = false;
	private int RingSlotNum = 13;
	private int _pvplevel;

	public int getPvPLevel() {
		return _pvplevel;
	}

	public void setPvPLevel(int level) {
		_pvplevel = level;
	}

	private int _lock;

	public int getLock() {
		return _lock;
	}

	public void setLock(int flag) {
		_lock = flag;
	}

	private int _Key = 0;

	public L1ItemInstance() {
		_count = 1;
		_enchantLevel = 0;
		_blessEnchantLevel = 0;
	}

	public L1ItemInstance(L1Item item, int count) {
		this();
		setItem(item);
		setCount(count);
	}

	public L1ItemInstance(L1Item item) {
		this(item, 1);
	}

	public void clickItem(L1Character cha, ClientBasePacket packet) {
	}

	public int getRSN() {
		return RingSlotNum;
	}

	public void setRSN(int num) {
		RingSlotNum = num;
	}

	public String getEncobjid() {
		return _encobjid;
	}

	public void setEncobjid(String s) {
		_encobjid = s;
	}

	public void setEnchantWA(L1PcInstance p) {
		_pc = p;
	}

	public boolean isIdentified() {
		return _isIdentified;
	}

	public void setIdentified(boolean identified) {
		_isIdentified = identified;
	}

	public String getName() {
		return _item.getName();
	}

	public int getCount() {
		return _count;
	}

	public void setCount(int count) {
		_count = count;
	}

	public int getClassType() {
		return _class;
	}

	public void setClassType(int count) {
		_class = count;
	}

	public boolean isEquipped() {
		return _isEquipped;
	}

	public void setEquipped(boolean equipped) {
		_isEquipped = equipped;
	}

	public boolean isDemonBongin() {
		return _demon_bongin;
	}

	public void setDemonBongin(boolean ck) {
		_demon_bongin = ck;
	}

	public L1Item getItem() {
		return _item;
	}

	public void setItem(L1Item item) {
		_item = item;
		_itemId = item.getItemId();
	}

	public int getItemId() {
		return _itemId;
	}

	public void setItemId(int itemId) {
		_itemId = itemId;
	}

	public boolean isStackable() {
		return _item.isStackable();
	}

	@Override
	public void onAction(L1PcInstance player) {
	}

	public int getEnchantLevel() {
		return _enchantLevel;
	}

	public void setEnchantLevel(int enchantLevel) {
		_enchantLevel = enchantLevel;
	}

	public int getAttrEnchantLevel() {
		return _attrenchantLevel;
	}

	public void setAttrEnchantLevel(int attrenchantLevel) {
		_attrenchantLevel = attrenchantLevel;
	}

	public int getStepEnchantLevel() {
		return _stepenchantLevel;
	}

	public void setStepEnchantLevel(int stepenchantLevel) {
		_stepenchantLevel = stepenchantLevel;
	}

	/** 축복 주문서 3 단계 시스템 by K **/
	public int getBlessEnchantLevel() {
		return _blessEnchantLevel;
	}

	public int getBlessEnchantLevel(int index) {
		return ((_blessEnchantLevel >> (8 * index)) & 0xFF);
	}

	public void setBlessEnchantLevel(int enchant) {
		_blessEnchantLevel = enchant;
	}

	public void setBlessEnchantLevel(int index, int enchant) {
		_blessEnchantLevel |= enchant << (8 * index);
	}

	/** 여기까지 **/

	private int temp_gfx = 0;

	public int get_gfxid() {
		return temp_gfx == 0 ? _item.getGfxId() : temp_gfx;
	}

	public void set_tempGfx(int i) {
		temp_gfx = i;
	}

	public int get_tempGfx() {
		return temp_gfx;
	}

	public int get_durability() {
		return _durability;
	}

	public int getChargeCount() {
		return _chargeCount;
	}

	public void setChargeCount(int i) {
		_chargeCount = i;
	}

	public int getRemainingTime() {
		return _remainingTime;
	}

	public void setRemainingTime(int i) {
		_remainingTime = i;
	}

	public void setLastUsed(Timestamp t) {
		_lastUsed = t;
	}

	public Timestamp getLastUsed() {
		return _lastUsed;
	}

	public int getBless() {
		return bless;
	}

	public void setBless(int i) {
		bless = i;
	}

	public int getLastWeight() {
		return _lastWeight;
	}

	public void setLastWeight(int weight) {
		_lastWeight = weight;
	}

	public Timestamp getBuyTime() {
		return _buyTime;
	}

	public void setBuyTime(Timestamp t) {
		_buyTime = t;
	}

	public int getKey() {
		return _Key;
	}

	public void setKey(int t) {
		_Key = t;
	}

	private String _CreaterName;

	public String getCreaterName() {
		return _CreaterName;
	}

	public void setCreaterName(String name) {
		_CreaterName = name;
	}

	public Timestamp getEndTime() {
		return _endTime;
	}

	public void setEndTime(Timestamp t) {
		_endTime = t;
	}

	private int _registlevel;

	public int getRegistLevel() {
		return _registlevel;
	}

	public void setRegistLevel(int level) {
		_registlevel = level;
	}

	public int getsp() {
		int sp = _item.get_addsp();
		int itemid = getItemId();

		if (itemid == 20107) { // 리롭
			if (getEnchantLevel() >= 3)
				sp += getEnchantLevel() - 2;
		} else if (itemid == 120107) { // 축 리롭
			if (getEnchantLevel() >= 3)
				sp += getEnchantLevel() - 2;
		} else if (itemid == 22112) {
			if (getEnchantLevel() >= 7)
				sp += getEnchantLevel() - 6;
		} else if (itemid == 10101) {
			if (getEnchantLevel() >= 7)
				sp += getEnchantLevel() - 6;
		} else if (getItemId() == 9117) {
			switch (getEnchantLevel()) {
			case 8:
			case 9:
				sp += 1;
				break;
			case 10:
				sp += 2;
				break;
			default:
				break;
			}
		} else if (getItemId() == 91170) {
			switch (getEnchantLevel()) {
			case 7:
			case 8:
			case 9:
				sp += 1;
				break;
			case 10:
				sp += 2;
				break;
			default:
				break;
			}
		}
		int itemtype = _item.getType();
		int itemtype2 = _item.getType2();
		int itemgrade = _item.getGrade();
		if (itemgrade != 3) {
			if (itemtype2 == 2 && (itemtype == 9 || itemtype == 11)) {// 반지
				if (getEnchantLevel() == 7) {
					sp += 1;
				}
				if (getEnchantLevel() == 8) {
					sp += 2;
				}
				if (getEnchantLevel() == 9) {
					sp += 3;
				}
				if (getEnchantLevel() == 10) {
					sp += 4;
				}
				if (getEnchantLevel() == 11) {
					sp += 5;
				}
				if (getEnchantLevel() == 12) {
					sp += 6;
				}
			}
		}
		if (sp < 0)
			sp = 0;
		return sp;
	}

	public int getMr() {
		int mr = _item.get_mdef();
		int itemid = getItemId();
		if (itemid == 20011 || itemid == 20110 || itemid == 120011 || itemid == 9091 || itemid == 490008
				|| itemid == 490017 || itemid == 120194 || itemid == 1020110 || (itemid >= 21169 && itemid <= 21172)) {
			mr += getEnchantLevel();
		}

		if (itemid == 500214 || itemid == 7247 || itemid == 9083 || itemid == 20117) {
			mr += getEnchantLevel();
		}

		if (itemid == 20056 || itemid == 120056 || itemid == 9092 || itemid == 220011 || itemid == 220056
				|| itemid == 425108 || itemid == 9084) {
			mr += getEnchantLevel() * 2;
		}

		if (itemid == 20078 || itemid == 20079 || itemid == 120079 || itemid == 20074 || itemid == 120074
				|| itemid == 21137 || itemid == 20049 || itemid == 20050) {
			mr += getEnchantLevel() * 3;
		}
		if (itemid == 10110) {
			switch (getEnchantLevel()) {
			case 7:
				mr += 2;
				break;
			case 8:
				mr += 3;
				break;
			case 9:
				mr += 4;
				break;
			default:
				break;
			}
		}
		// **룸티스의 보랏빛 귀걸이 마방**//
		if (itemid == 500009) {
			if (getEnchantLevel() == 7)
				mr += 10;
			else if (getEnchantLevel() == 8)
				mr += 13;
			else if (getEnchantLevel() > 0)
				mr += 2 + getEnchantLevel();
		}

		if (itemid == 502009) {
			if (getEnchantLevel() == 7)
				mr += 13;
			else if (getEnchantLevel() == 8)
				mr += 18;
			else if (getEnchantLevel() > 0)
				mr += 3 + getEnchantLevel();
		}
		if (itemid == 21251 || itemid == 9113) {// 스냅퍼마법저항반지 축복
			if (getEnchantLevel() >= 6) {
				mr += getEnchantLevel() - 5;
			}
		}
		if (itemid == 22113) {
			if (getEnchantLevel() >= 5) {
				mr += getEnchantLevel() - 3;
			}
		}
		if (mr < 0)
			mr = 0; // << -마방버그 픽스
		return mr;
	}

	public void set_durability(int i) {
		if (i < 0) {
			i = 0;
		}

		if (i > 127) {
			i = 127;
		}
		_durability = i;
	}

	public int getWeight() {
		if (getItem().getWeight() == 0) {
			return 0;
		} else {
			return Math.max(getCount() * getItem().getWeight() / 1000, 1);
		}
	}

	public class LastStatus {
		public int registLevel;
		public int count;
		public int itemId;
		public boolean isEquipped = false;
		public int enchantLevel;
		public boolean isIdentified = true;
		public int durability;
		public int chargeCount;
		public int remainingTime;
		public Timestamp lastUsed = null;
		public int bless;
		public int attrenchantLevel;
		public int stepenchantLevel;
		public Timestamp endTime = null;
		public boolean demon_bongin;
		public int pvplevel;
		public int lock;
		public int blessEnchantLevel; // 축복 주문서 3 단계 시스템 by K

		public void updateAll() {
			count = getCount();
			itemId = getItemId();
			isEquipped = isEquipped();
			isIdentified = isIdentified();
			enchantLevel = getEnchantLevel();
			durability = get_durability();
			chargeCount = getChargeCount();
			remainingTime = getRemainingTime();
			lastUsed = getLastUsed();
			bless = getBless();
			attrenchantLevel = getAttrEnchantLevel();
			stepenchantLevel = getStepEnchantLevel();
			registLevel = getRegistLevel();
			endTime = getEndTime();
			demon_bongin = isDemonBongin();
			pvplevel = getPvPLevel();
			lock = getLock();
			blessEnchantLevel = getBlessEnchantLevel(); // 축복 주문서 3 단계 시스템 by K

		}

		/** 축복 주문서 3 단계 시스템 by K **/
		public void updateBlessEnchantLevel() {
			blessEnchantLevel = getBlessEnchantLevel();
		}

		/** 여기까지 **/

		public void updateCount() {
			count = getCount();
		}

		public void updateItemId() {
			itemId = getItemId();
		}

		public void updateEquipped() {
			isEquipped = isEquipped();
		}

		public void updateIdentified() {
			isIdentified = isIdentified();
		}

		public void updateEnchantLevel() {
			enchantLevel = getEnchantLevel();
		}

		public void updateDuraility() {
			durability = get_durability();
		}

		public void updateChargeCount() {
			chargeCount = getChargeCount();
		}

		public void updateRemainingTime() {
			remainingTime = getRemainingTime();
		}

		public void updateLastUsed() {
			lastUsed = getLastUsed();
		}

		public void updateBless() {
			bless = getBless();
		}

		public void updateAttrEnchantLevel() {
			attrenchantLevel = getAttrEnchantLevel();
		}

		public void updateRegistLevel() {
			registLevel = getRegistLevel();
		}

		public void updateEndTIme() {
			endTime = getEndTime();
		}

		public void updateDemonBongin() {
			demon_bongin = isDemonBongin();
		}

		public void updatePvPLevel() {
			pvplevel = getPvPLevel();
		}

		public void updateLock() {
			lock = getLock();
		}

		public void updateStepEnchantLevel() {
			stepenchantLevel = getStepEnchantLevel();
		}
	}

	public LastStatus getLastStatus() {
		return _lastStatus;
	}

	public int getRecordingColumns() {
		int column = 0;

		if (getCount() != _lastStatus.count) {
			column += L1PcInventory.COL_COUNT;
		}
		if (getItemId() != _lastStatus.itemId) {
			column += L1PcInventory.COL_ITEMID;
		}
		if (isEquipped() != _lastStatus.isEquipped) {
			column += L1PcInventory.COL_EQUIPPED;
		}
		if (getEnchantLevel() != _lastStatus.enchantLevel) {
			column += L1PcInventory.COL_ENCHANTLVL;
		}
		if (get_durability() != _lastStatus.durability) {
			column += L1PcInventory.COL_DURABILITY;
		}
		if (getChargeCount() != _lastStatus.chargeCount) {
			column += L1PcInventory.COL_CHARGE_COUNT;
		}
		if (getLastUsed() != _lastStatus.lastUsed) {
			column += L1PcInventory.COL_DELAY_EFFECT;
		}
		if (isIdentified() != _lastStatus.isIdentified) {
			column += L1PcInventory.COL_IS_ID;
		}
		if (getRemainingTime() != _lastStatus.remainingTime) {
			column += L1PcInventory.COL_REMAINING_TIME;
		}
		if (getBless() != _lastStatus.bless) {
			column += L1PcInventory.COL_BLESS;
		}
		if (getAttrEnchantLevel() != _lastStatus.attrenchantLevel) {
			column += L1PcInventory.COL_ATTRENCHANTLVL;
		}
		if (getRegistLevel() != _lastStatus.registLevel) {
			column += L1PcInventory.COL_regist;
		}
		if (getEndTime() != _lastStatus.endTime) {
			column += L1PcInventory.COL_ENDTIME;
		}
		if (isDemonBongin() != _lastStatus.demon_bongin) {
			column += L1PcInventory.COL_DEMONBONGIN;
		}
		if (getLock() != _lastStatus.lock) {
			column += L1PcInventory.COL_LOCK;
		}
		/** 축복 주문서 3 단계 시스템 by K **/
		if (getBlessEnchantLevel() != _lastStatus.blessEnchantLevel) {
			column += L1PcInventory.COL_BLESS_ENCHANT;
		} /** 여기까지 **/
		return column;
	}

	public String getNumberedViewName(int count) {
		return getNumberedViewName(count, false);
	}

	public String getNumberedViewName(int count, boolean privateShop) {
		StringBuilder name = new StringBuilder(getNumberedName(count, privateShop));
		int itemType2 = getItem().getType2();
		int itemId = getItem().getItemId();

		/** 팻 목걸이 일시에 체크 */
		if (itemId == 40314 || itemId == 40316) {
			L1Pet pet = PetTable.getTemplate(getId());
			if (pet != null) {
				L1Npc npc = NpcTable.getInstance().getTemplate(pet.getNpcId());
				name.append("(" + npc.get_nameid() + " Lv " + pet.getLevel() + ")");
			}
		}

		if (getItem().getType2() == 0 && getItem().getType() == 2) { // light
			if (isNowLighting()) {
				name.append(" ($10)");
			}
			if (itemId == 40001 || itemId == 40002 || itemId == 60154) {
				if (getRemainingTime() <= 0) {
					name.append(" ($11)");
				}
			}
		}

		if (getLock() != 0) {
			name.append("(각인)");
		}

		if (getItem().getItemId() == L1ItemId.DRAGON_KEY || (getItemId() >= 60350 && getItemId() <= 60352)
				|| getItemId() == 61000) {// 드래곤 키
			name.append(" [" + sdf.format(getEndTime().getTime()) + "]");
		}

		if (getItem().getItemId() == 60285) {// 훈련소 열쇠
			if (getEndTime() != null) {
				int mapid = getKey() - 1399;
				String date = " (" + (mapid > 9 ? mapid : "0" + mapid) + ") [" + sdf.format(getEndTime().getTime())
						+ "]";
				date = date.replace("[0", "[");
				name.append(date);
			}
		}

		if (getItem().getItemId() == 40312 || getItem().getItemId() == 49312) {// 여관열쇠
			if (getEndTime() != null)
				name.append(" [" + sdf.format(getEndTime().getTime()) + "] CHECK : " + getKey());
		}

		if (getItem().getItemId() == 20344 || getItem().getItemId() == 21092 || itemId == 60061 || // 토끼투구,
																									// 요리모자,
																									// 버프코인
				(getItem().getItemId() >= 60009 && getItem().getItemId() <= 60016) || // 계약서
				(itemId >= 425000 && itemId <= 425002) || // 엘모어 방어구
				(itemId >= 450000 && itemId <= 450007) || // 엘모어 무기
				itemId == 21094 || // 위대한 자의 유물
				itemId == 21157 || itemId == 121216 || itemId == 221216 || itemId == 60499 || itemId == 602229
				|| itemId == 60500 || (itemId >= 60173 && itemId <= 60176) || (itemId >= 21113 && itemId <= 21120)
				|| itemId == 430003 || itemId == 430505 || itemId == 430506 || itemId == 41915 || itemId == 5000034// 블레그
																													// 마법
																													// 인형
				|| (itemId >= 21125 && itemId <= 21136) || (itemId >= 21139 && itemId <= 21156)
				|| (itemId >= 427113 && itemId <= 427207) || (itemId >= 427120 && itemId <= 427122)
				|| (itemId >= 21158 && itemId <= 21165) || (itemId >= 267 && itemId <= 274) || itemId == 600234
				|| itemId == 421216 || itemId == 421217 || itemId == 421218 || itemId == 421219 || itemId == 9056
				|| itemId == 19010 || itemId == 621217// 그렘린
				|| itemId == 141915 || itemId == 141916 || itemId == 60319 || itemId == 60233 || itemId == 41550
				|| itemId == 500144 || itemId == 73100 || itemId == 39105 || itemId == 2222384 || itemId == 2222385
				|| itemId == 2222386|| itemId == 222251) {
			if (getEndTime() != null) {
				String date = " [" + sdf.format(getEndTime().getTime()) + "]";
				date = date.replace("[0", "[");
				name.append(date);
			}
		}

		if (getItem().getItemId() == 40309) {
			String cname = getCreaterName();
			name.append(" " + getRoundId() + "-" + (getTicketId() + 1) + " "
					+ (cname == null || cname.equalsIgnoreCase("null") ? "" : cname));
		}

		if (isEquipped()) {
			if (itemType2 == 1) {
				name.append(" ($9)");
			} else if (itemType2 == 2 && !getItem().isUseHighPet()) {
				name.append(" ($117)");
			}
		}
		return name.toString();
	}

	public String getViewName() {
		return getNumberedViewName(_count);
	}

	public String getLogName() {
		return getNumberedName(_count);
	}

	public String getNumberedName(int count) {
		return getNumberedName(count, false);
	}

	public String getNumberedName(int count, boolean privateShop) {
		StringBuilder name = new StringBuilder();
		if (getItemId() == 600240) {
			if (getEnchantLevel() >= 0) {
				name.append("+" + getEnchantLevel() + " ");
			} else if (getEnchantLevel() < 0) {
				name.append(String.valueOf(getEnchantLevel()) + " ");
			}
		}
		if (isIdentified()) {
			if (getItem().getType2() == 1 || getItem().getType2() == 2) {
				switch (getAttrEnchantLevel()) {
				case 1:
					name.append("화령:1단 ");
					break;
				case 2:
					name.append("화령:2단 ");
					break;
				case 3:
					name.append("화령:3단 ");
					break;
				case 4:
					name.append("화령:4단 ");
					break;
				case 5:
					name.append("화령:5단 ");
					break;
				case 6:
					name.append("수령:1단 ");
					break;
				case 7:
					name.append("수령:2단 ");
					break;
				case 8:
					name.append("수령:3단 ");
					break;
				case 9:
					name.append("수령:4단 ");
					break;
				case 10:
					name.append("수령:5단 ");
					break;
				case 11:
					name.append("풍령:1단 ");
					break;
				case 12:
					name.append("풍령:2단 ");
					break;
				case 13:
					name.append("풍령:3단 ");
					break;
				case 14:
					name.append("풍령:4단 ");
					break;
				case 15:
					name.append("풍령:5단 ");
					break;
				case 16:
					name.append("지령:1단 ");
					break;
				case 17:
					name.append("지령:2단 ");
					break;
				case 18:
					name.append("지령:3단 ");
					break;
				case 19:
					name.append("지령:4단 ");
					break;
				case 20:
					name.append("지령:5단 ");
					break;
				default:
					break;
				}
				if (getEnchantLevel() >= 0) {
					name.append("+" + getEnchantLevel() + " ");
				} else if (getEnchantLevel() < 0) {
					name.append(String.valueOf(getEnchantLevel()) + " ");
				}
			}
		}
		name.append(_item.getNameId());

		if (isIdentified()) {
			if (getItem().getType2() == 1 || getItem().getType2() == 2) {
				if (getBlessEnchantLevel() > 0) {
					name.append(" [" + getBlessEnchantLevel() + "단계]");
				}
			}
		}

		if (getItem().getItemId() >= 600251 && getItem().getItemId() <= 600254) {
			if (getItem().getMaxChargeCount() > 0) {
				name.append(" (" + getChargeCount() + ")");
			}
		}
		if (isIdentified()) {
			if (getItem().getItemId() < 600251 || getItem().getItemId() > 600254) {
				if (getItem().getMaxChargeCount() > 0) {
					name.append(" (" + getChargeCount() + ")");
				}
			}
			if (getItem().getMaxUseTime() > 0 && getItem().getType2() != 0) {
				name.append(" [" + getRemainingTime() + "]");
			} else if (getItemId() >= 60173 && getItemId() <= 60176 && getRemainingTime() > 0)
				name.append(" [" + getRemainingTime() + "]");
		}

		if (!privateShop && count > 1) {
			name.append(" (" + count + ")");
		}

		return name.toString();
	}

	@SuppressWarnings("unused")
	public byte[] getStatusBytes() {
		int itemType2 = getItem().getType2();
		int itemId = getItemId();
		@SuppressWarnings("resource")
		// BinaryOutputStream os = new BinaryOutputStream();
		ItemPresentOutStream os = new ItemPresentOutStream();

		if (itemType2 == 0) { // etcitem
			switch (getItem().getType()) {
			case 2: // light
				os.writeC(22);
				os.writeH(getItem().getLightRange());
				break;
			case 7: // food
				os.writeC(21);
				os.writeH(getItem().getFoodVolume());
				break;
			case 0: // arrow
			case 15: // sting
				os.writeC(1);
				os.writeC(getItem().getDmgSmall());
				os.writeC(getItem().getDmgLarge());
				break;
			default:
				os.writeC(23);
				break;
			}

			os.writeC(getItem().getMaterial());
			os.writeD(getWeight());
			if ((itemId >= 90085 && itemId <= 90092) || itemId == 160423 || itemId == 435000 || itemId == 160510
					|| itemId == 160511 || itemId == 21123) {
				os.writeC(61);
				os.writeD(3442346400L);
			}
			if (itemId == 21269 && getEnchantLevel() < 6) {
				os.writeC(61);
				os.writeD(3442346400L);
			}

			if (getItem().getType() == 10) {
				if ((getItem().getItemId() >= 40170 && getItem().getItemId() <= 40225)
						|| (getItem().getItemId() >= 45000 && getItem().getItemId() <= 45022)
						|| getItem().getItemId() == 140186 || getItem().getItemId() == 140196
						|| getItem().getItemId() == 140198 || getItem().getItemId() == 140204
						|| getItem().getItemId() == 140205 || getItem().getItemId() == 140210
						|| getItem().getItemId() == 140219) {
					if (getItem().getskilllv() != 0) {// 법사 단계
						os.writeC(77);// 4D
						os.writeC(getItem().getskilllv() - 1);
					}
					if (getItem().getskilllv() >= 4 && getItem().getskilllv() <= 6) {
						os.writeC(7);
						os.writeC(0x0c);
					} else {
						os.writeC(7);
						os.writeC(0x08);
					}

				} else {
					if ((getItem().getItemId() >= 40232 && getItem().getItemId() <= 40264)
							|| (getItem().getItemId() >= 41149 && getItem().getItemId() <= 41153)) {
						if (getItem().getskilllv() != 0) {// 법사 단계
							os.writeC(77);// 4D
							os.writeC(getItem().getskilllv() - 1);
						}
						os.writeC(7);
						os.writeC(0x014);

					} else {
						if (getItem().getskilllv() != 0) {// 사용레벨
							os.writeC(79);
							os.writeC(getItem().getskilllv());
						}
					}

					if ((getItem().getItemId() >= 40226 && getItem().getItemId() <= 40231)
							|| getItem().getItemId() == 60348) {// 군주
						os.writeC(7);
						os.writeC(0x01);
					}

					if ((getItem().getItemId() >= 40164 && getItem().getItemId() <= 40166)
							|| getItem().getItemId() == 41147 || getItem().getItemId() == 41148) {// 기사
						os.writeC(7);
						os.writeC(0x02);
					}

					if ((getItem().getItemId() >= 40265 && getItem().getItemId() <= 40279)
							|| getItem().getItemId() == 60199) {// 다엘
						os.writeC(7);
						os.writeC(16);
					}
					if ((getItem().getItemId() >= 439100 && getItem().getItemId() <= 439114)) {// 용기사
						os.writeC(7);
						os.writeC(32);
					}
					if ((getItem().getItemId() >= 439000 && getItem().getItemId() <= 439019)) {// 환술
						os.writeC(7);
						os.writeC(64);
					}
					if ((getItem().getItemId() >= 7300 && getItem().getItemId() <= 7311)) {// 전사
						os.writeC(7);
						os.writeC(128);
					}
				}

				if ((getItem().getItemId() >= 40232 && getItem().getItemId() <= 40264)
						|| (getItem().getItemId() >= 41149 && getItem().getItemId() <= 41153)) {
					if (getItem().getskillattr() != 0) {// 속성
						os.writeC(78);// 4b
						os.writeC(getItem().getskillattr() - 1);
					}
				} else {
					if (getItem().getskillattr() != 0) {// 속성
						os.writeC(75);// 4b
						os.writeC(getItem().getskillattr() - 1);
					}
				}

				if (getItem().getskillnum() != 0) {// 스킬번호
					os.writeC(76);// 4c
					os.writeC(getItem().getskillnum() - 1);
				}

			}
			if (itemId == 41550) {
				os.writeC(61);
				os.writeD(3548082400L);
			}
			if (itemId == 500144) {
				os.writeC(61);
				os.writeD(3594738400L);
			}
			if (itemId == 60354) { // 시원한 얼음 조각
				os.writeC(17);
				os.writeC(2);
				os.writeC(47);
				os.writeC(2);
				os.writeC(35);
				os.writeC(2);
				os.writeC(37);
				os.writeC(1);
				os.writeC(38);
				os.writeC(1);
			} else if (itemId == 60080 || itemId == 60082 || itemId == 60084) {
				if (getCreaterName() != null) {
					os.writeC(39);
					os.writeS("제작자: " + getCreaterName());
				}
			} else if (itemId == 75000) {
				if (getCreaterName() != null) {
					os.writeC(39);
					os.writeS("\\aD희생자: [" + getCreaterName() + "]");
					os.writeC(39);
					os.writeS("\\aH사용설명은 더블클릭");
				}
			}
			if (itemId == 8600) { // 사냥꾼의 화살
				os.writeC(35); // 원거리 대미지
				os.writeC(1);
			}
			if (itemId == 8601) { // 사냥꾼의 은 화살
				os.writeC(35); // 원거리 대미지
				os.writeC(3);
			}
			if (itemId == 8602) { // 대정령의 전투 화살
				os.writeC(35); // 원거리 대미지
				os.writeC(3);
				os.writeC(24); // 원거리 명중
				os.writeC(3);
				os.writeC(39);
				os.writeS("\\f9불 속성 대미지:\\aA +3");
				os.writeC(39);
				os.writeS("\\f9바람 속성 대미지:\\aA +3");
				os.writeC(39);
				os.writeS("\\f9물 속성 대미지:\\aA +3");
				os.writeC(39);
				os.writeS("\\f9땅 속성 대미지:\\aA +3");
			}
			if (itemId == 40739) { // 스팅
				os.writeC(35); // 원거리 대미지
				os.writeC(1);
			}
			if (itemId == 40738) { // 실버 스팅
				os.writeC(35); // 원거리 대미지
				os.writeC(3);
			}
			if (itemId == 40740) { // 헤비 스팅
				os.writeC(35); // 원거리 대미지
				os.writeC(3);
				os.writeC(24); // 원거리 명중
				os.writeC(3);
			}
			/** 인형 능력치 표기 **/
			int EN = getEnchantLevel(); // 인형 강화 주문서 by K
			if (itemId == 41248) { // 버그베어
				os.writeAddWeight(500);
			}
			if (itemId == 41249) { // 서큐버스
				os.writeaMPUP(15);
			}
			if (itemId == 41250) { // 늑대인간
				os.writeaPercentDmg(15);
			}
			if (itemId == 430000) { // 돌골렘
				os.writeDMGdown(1);
			}
			if (itemId == 430001) { // 장로
				os.writeaMPUP(15);
			}
			if (itemId == 430002) { // 크러스트시안
				os.writeaPercentDmg(15);
			}
			if (itemId == 430004) { // 에티
				os.writeAddAc(3);

			}
			if (itemId == 430500) { // 코카트리스
				os.writeLongDMG(1);
				os.writeLongHIT(1);
			}
			if (itemId == 500202) { // 사이클롭스
				os.writeShortDMG(2);
				os.writeability_resis(12);
				os.writeShortHIT(2);
			}
			if (itemId == 500203) { // 자이언트
				os.writeAddEXP(10);
				os.writeDMGdown(1);
			}
			if (itemId == 500204) { // 흑장로
				os.writeAddEXP(15);
				try {
					os.writeMagic2("콜 라이트닝");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (itemId == 500205) { // 서큐버스 퀸
				os.writeaMPUP(15);
				os.writeAddSP(1);
			}
			if (itemId == 5000035) { // 리치
				os.writeAddSP(2);
				os.writeAddMaxHP(80);
			}
			if (itemId == 500110) { // 킹버그베어
				os.writeability_resis(8);
				os.writeaMPUP(10);
			}
			if (itemId == 600241) { // 목각
				os.writeAddMaxHP(50);
			}
			if (itemId == 600242) { // 라바 골렘
				os.writeShortDMG(1);
				os.writeDMGdown(1);
			}
			if (itemId == 600243) { // 다이아몬드 골렘
				os.writeDMGdown(2);
			}
			if (itemId == 600244) { // 시어
				os.writeLongDMG(5);
				os.writeaHPUP(30);

			}
			if (itemId == 600245) { // 나이트발드
				os.writeShortDMG(2);
				os.writeShortHIT(2);
				os.writeability_pierce(5);
			}
			if (itemId == 600246) { // 데몬
				os.writeability_resis(12);
				os.writeability_pierce(10);
			}
			if (itemId == 600247) { // 데스나이트
				os.writeDMGdown(5 + EN);
				try {
					os.writeMagic("헬파이어");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				os.writeaBlesssomo(7);
				os.writeAddEXP(20 + EN);
				if (EN == 1) {
					os.writeC(39);
					os.writeS("\\fC헬파이어 대미지 1% 상승");
				} else if (EN == 2) {
					os.writeC(39);
					os.writeS("\\fC헬파이어 대미지 2% 상승");
				} else if (EN == 3) {
					os.writeC(39);
					os.writeS("\\fC헬파이어 대미지 3% 상승");
				} else if (EN == 4) {
					os.writeC(39);
					os.writeS("\\fC헬파이어 대미지 4% 상승");
				} else if (EN == 5) {
					os.writeC(39);
					os.writeS("\\fC헬파이어 대미지 5% 상승");
				}
			}
			if (itemId == 41551) { // 뱀파이어
				os.writeShortDMG(2);
				os.writeShortHIT(2);
			}
			if (itemId == 41552) { // 아이리스
				os.writeaFouslayer(10);
				os.writeDMGdown(3);
			}
			if (itemId == 41553) { // 바란카
				os.writeability_resis(12+ EN);
				os.writeaspirit_pierce(10+ EN);
			}
			if (itemId == 49000 || itemId == 19010) { // 몽크
				os.writeC(36);
				os.writeC(10);
				os.writeC(47); // 근거리 대미지
				os.writeC(2);
				os.writeC(5); // 근거리 명중
				os.writeC(2);
				os.writeC(24); // 원거리 명중
				os.writeC(2);
				os.writeC(35); // 원거리 대미지
				os.writeC(2);
				os.writeC(17); // 스펠
				os.writeC(2);
			}
			if (itemId == 73001) { // 안타라스
				os.writeAddAc(3);
				os.writePVPAddDMG(4);
				os.writePVPAddDMGdown(2);
				os.writeDMGdown(6+ EN);
				os.writeAddEXP(25+ EN);
				os.writeaMPUP(15);
				os.writeaBlesssomo(7);
			}
			if (itemId == 73002) { // 파푸리온
				os.writeAddAc(3);
				os.writePVPAddDMG(4);
				os.writePVPAddDMGdown(2);
				os.writeaAll_resis(8+ EN);
				os.writeaAll_pierce(3+ EN);
				os.writeMagicHIT(8+ EN);
				os.writeAddSP(4+ EN);
				os.writeaMPUP(5);
			}
			if (itemId == 73003) { // 린드비오르
				os.writeAddAc(3);
				os.writePVPAddDMG(4);
				os.writePVPAddDMGdown(2);
				os.writeaAll_resis(8+ EN);
				os.writeaAll_pierce(3+ EN);
				os.writeLongHIT(8+ EN);
				os.writeLongDMG(4+ EN);
				os.writeaMPUP(5);
			}
			if (itemId == 73004) { // 발라카스
				os.writeAddAc(3);
				os.writePVPAddDMG(4);
				os.writePVPAddDMGdown(2);
				os.writeaAll_resis(8+ EN);
				os.writeaAll_pierce(3+ EN);
				os.writeShortHIT(8+ EN);
				os.writeShortDMG(4+ EN);
				os.writeaMPUP(5);
			}
			if (itemId == 19006) { // 머미로드
				os.writeDMGdown(2);
				os.writeaBlesssomo(2);
				os.writeAddEXP(10);
				os.writeAddMPPrecovery(15);
			}
			if (itemId == 73100) { // 수련자
				os.writeC(_add_ac); // 방어구
				os.writeC(-1);
				os.writeC(63); // 대미지 감소
				os.writeC(1);
				os.writeC(88);
				os.writeC(10);
			}
			if (itemId == 19005) { // 타락
				os.writeAddSP(3+ EN);
				os.writeMagicHIT(5+ EN);
				os.writeability_resis(10+ EN);
				os.writeadragonS_pierce(5);
			}
			if (itemId == 19007) { // 바포메트
				os.writeafear_pierce(5);
				os.writeability_resis(10);
			}
			if (itemId == 19008) { // 얼음여왕
				os.writeLongDMG(5+ EN);
				os.writeLongHIT(5+ EN);
				os.writeability_resis(10+ EN);
				os.writeaspirit_pierce(5+ EN);
			}
			if (itemId == 19009) { // 커츠
				os.writeaFouslayer(10+ EN);
				os.writeDMGdown(3+ EN);
				os.writeability_resis(10);
				os.writeadragonS_pierce(5+ EN);
				os.writeAddAc(2);
			}
			if (itemId == 950000) { // 축서큐버스 퀸
				os.writeaMPUP(15);
				os.writeAddAc(2);
				os.writeAddSP(2);

			}
			if (itemId == 950001) { // 축흑장로
				os.writeAddEXP(15);
				os.writeAddAc(2);
				try {
					os.writeMagic2("콜 라이트닝");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (itemId == 950002) { // 축자이언트
				os.writeAddEXP(10);
				os.writeDMGdown(1);
				os.writeAddAc(2);
			}
			if (itemId == 950003) { // 축드레이크
				os.writeaMPUP(6);
				os.writeAddAc(2);
				os.writeLongDMG(2);
			}
			if (itemId == 950004) { // 킹버그베어
				os.writeAddAc(2);
				os.writeability_resis(8);
				os.writeaMPUP(10);
			}
			if (itemId == 950005) { // 다이아몬드 골렘
				os.writeDMGdown(2);
				os.writeAddAc(2);
			}
			if (itemId == 950006) { // 사이클롭스
				os.writeAddAc(2);
				os.writePVPAddDMG(2);
				os.writeShortDMG(2);
				os.writeability_resis(12);
				os.writeShortHIT(2);
			}
			if (itemId == 950007) { // 리치
				os.writeAddAc(2);
				os.writePVPAddDMG(2);
				os.writeAddSP(2);
				os.writeAddMaxHP(80);
			}
			if (itemId == 950008) { // 나이트발드
				os.writeAddAc(2);
				os.writePVPAddDMG(2);
				os.writeShortDMG(2);
				os.writeShortHIT(2);
				os.writeability_pierce(5);
			}
			if (itemId == 950009) { // 시어
				os.writeAddAc(2);
				os.writePVPAddDMG(2);
				os.writeLongDMG(5);
				os.writeaHPUP(30);

			}
			if (itemId == 950010) { // 아이리스
				os.writeAddAc(2);
				os.writePVPAddDMG(2);
				os.writeaFouslayer(10);
				os.writeDMGdown(3);
			}
			if (itemId == 950011) { // 뱀파이어
				os.writeAddAc(2);
				os.writePVPAddDMG(2);
				os.writeShortDMG(2);
				os.writeShortHIT(2);
			}
			if (itemId == 950012) { // 머미로드
				os.writeAddAc(2);
				os.writePVPAddDMG(2);
				os.writeDMGdown(2);
				os.writeaBlesssomo(2);
				os.writeAddEXP(10);
				os.writeAddMPPrecovery(15);
			}

			if (itemId == 950013) { // 데몬
				os.writeAddAc(3);
				os.writePVPAddDMG(2);
				os.writePVPAddDMGdown(4);
				os.writeability_resis(12);
				os.writeability_pierce(10);

			}
			if (itemId == 950014) { // 데스나이트
				os.writeAddAc(3);
				os.writePVPAddDMG(2);
				os.writePVPAddDMGdown(4);
				os.writeDMGdown(5+ EN);
				try {
					os.writeMagic("헬파이어");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				os.writeaBlesssomo(7);
				os.writeAddEXP(20 + EN);
				if (EN == 1) {
					os.writeC(39);
					os.writeS("\\fC헬파이어 대미지 1% 상승");
				} else if (EN == 2) {
					os.writeC(39);
					os.writeS("\\fC헬파이어 대미지 2% 상승");
				} else if (EN == 3) {
					os.writeC(39);
					os.writeS("\\fC헬파이어 대미지 3% 상승");
				} else if (EN == 4) {
					os.writeC(39);
					os.writeS("\\fC헬파이어 대미지 4% 상승");
				} else if (EN == 5) {
					os.writeC(39);
					os.writeS("\\fC헬파이어 대미지 5% 상승");
				}
			}
			if (itemId == 950015) { // 바란카
				os.writeAddAc(3);
				os.writePVPAddDMG(2);
				os.writePVPAddDMGdown(4);
				os.writeability_resis(12);
				os.writeaspirit_pierce(10);
			}
			if (itemId == 950016) { // 타락
				os.writeAddAc(3);
				os.writePVPAddDMG(2);
				os.writePVPAddDMGdown(4);
				os.writeAddSP(3);
				os.writeMagicHIT(5);
				os.writeability_resis(10);
				os.writeadragonS_pierce(5);
			}
			if (itemId == 950017) { // 바포메트
				os.writeAddAc(3);
				os.writePVPAddDMG(2);
				os.writePVPAddDMGdown(4);
				os.writeafear_pierce(5+ EN);
				os.writeability_resis(10+ EN);
			}
			if (itemId == 950018) { // 얼음여왕
				os.writeAddAc(3);
				os.writePVPAddDMG(2);
				os.writePVPAddDMGdown(4);
				os.writeLongDMG(5);
				os.writeLongHIT(5);
				os.writeability_resis(10);
				os.writeaspirit_pierce(5);
			}
			if (itemId == 950019) { // 커츠
				os.writePVPAddDMG(2);
				os.writePVPAddDMGdown(4);
				os.writeaFouslayer(10);
				os.writeDMGdown(3);
				os.writeability_resis(10);
				os.writeadragonS_pierce(5);
				os.writeAddAc(5);
			}

			/** 인형 강화주문서 강화별 표기 by K **/
			if (itemId == 600246 || itemId == 600247 || itemId == 41553 || itemId == 19007 || itemId == 19008
					|| itemId == 19009 || itemId >= 73001 && itemId <= 73004 || itemId >= 950013 && itemId <= 950019
					|| itemId == 19005) {
				if (EN == 1) {
					os.writeC(39);
					os.writeS("\\fH강화 1단계");
				} else if (EN == 2) {
					os.writeC(39);
					os.writeS("\\fH강화 2단계");
				} else if (EN == 3) {
					os.writeC(39);
					os.writeS("\\fH강화 3단계");
				} else if (EN == 4) {
					os.writeC(39);
					os.writeS("\\fH강화 4단계");
				} else if (EN == 5) {
					os.writeC(39);
					os.writeS("\\fH강화 5단계");
					os.writeC(39);
					os.writeS("발동 : \\f3상승데미지 5% ");
				}
			}
			if (getItem().getMinLevel() != 0) {
				os.writeC(111);
				os.writeD(getItem().getMinLevel());
			}
			if (getItem().isTradable()) {
				os.writeC(130);
				os.writeD(1);
			}
		} else if (itemType2 == 1 || itemType2 == 2) { // weapon | armor
			int op_addAc = 0;
			int SafeEnchant = getItem().get_safeenchant();
			os.writeC(39);
			if (SafeEnchant < 0) {
				SafeEnchant = 0;
			}
			os.writeS("\\fT안전인챈: +" + SafeEnchant + "");

			if (itemType2 == 1) { // weapon
				os.writeC(1);
				os.writeC(getItem().getDmgSmall());
				os.writeC(getItem().getDmgLarge());
				os.writeC(getItem().getMaterial());
				os.writeD(getWeight());

			} else if (itemType2 == 2) { // armor
				// AC
				/*
				 * os.writeC(19); int ac = ((L1Armor) getItem()).get_ac(); if (getRegistLevel()
				 * == 14)// 판도라 강철문양 ac -= 1; if (ac < 0) { ac = ac - ac - ac; } else { ac = ac
				 * - ac - ac; } os.writeC(ac); os.writeC(getItem().getMaterial()); if
				 * (getItem().getType() == 8 || getItem().getType() == 12) { os.writeC(0x2b);//
				 * 근성 } else if (getItem().getType() == 9 || getItem().getType() == 11) {
				 * os.writeC(0x2c);// 열정 } else if (getItem().getType() == 10) {
				 * os.writeC(0x2d);// 의지 } else { os.writeC(-1); } os.writeD(getWeight());
				 */
				os.writeC(19);
				int ac = ((L1Armor) getItem()).get_ac();
				if (ac < 0) {
					ac = ac - ac - ac;
				} else {
					ac = ac - ac - ac;
				}
				os.writeC(ac);
				os.writeC(getItem().getMaterial());
				os.writeC(67);
				os.writeC(-1);
				os.writeD(getWeight());
			}
			if (itemType2 == 2) { // 인첸당 방어구 표기
				if (getItem().getGrade() != 3) {
					if (getItem().getType() == 8 || getItem().getType() == 12) {
						switch (getEnchantLevel()) {
						case 5:
							os.writeC(2);
							os.writeC(1);
							break;
						case 6:
							os.writeC(2);
							os.writeC(2);
							break;
						case 7:
							os.writeC(2);
							os.writeC(3);
							break;
						case 8:
							os.writeC(2);
							os.writeC(4);
							break;
						case 9:
							os.writeC(2);
							os.writeC(5);
							break;
						default:
							break;
						}
					} else if (getItemId() == 9501) { // 수호의 투사휘장
						switch (getEnchantLevel()) {
						case 5:
							os.writeC(2); // Ac
							os.writeC(3);
							os.writeC(14); // HP
							os.writeH(30);
							os.writeC(63); // 대미지감소
							os.writeC(1);
							os.writeC(47); // 추타
							os.writeC(1);
							break;
						case 6:
							os.writeC(2); // Ac
							os.writeC(5);
							os.writeC(14); // hp
							os.writeH(35);
							os.writeC(63); // 대미지 감소
							os.writeC(2);
							os.writeC(15); // 마방
							os.writeH(3);
							os.writeC(47); // 근거리 대미지
							os.writeC(2);
							os.writeC(5); // 근거리 명중
							os.writeC(1);
							os.writeC(100);
							os.writeC(1);
							break;
						case 7:
							os.writeC(2); // Ac
							os.writeC(6);
							os.writeC(14); // Hp
							os.writeH(40);
							os.writeC(63); // 대미지 감소
							os.writeC(3);
							os.writeC(15); // 마방
							os.writeH(5);
							os.writeC(47); // 근거리 대미지
							os.writeC(3);
							os.writeC(5); // 근거리 명중
							os.writeC(3);
							os.writeC(60); // pvp대미지 감소
							os.writeC(1);
							os.writeC(100);
							os.writeC(3);
							break;
						case 8:
							os.writeC(2); // Ac
							os.writeC(7);
							os.writeC(14); // HP
							os.writeH(50);
							os.writeC(63); // 대미지 감소
							os.writeC(4);
							os.writeC(15); // 마방
							os.writeH(7);
							os.writeC(47); // 근거리 대미지
							os.writeC(4);
							os.writeC(5); // 근거리 명중
							os.writeC(5);
							os.writeC(60); // pvp대미지 감소
							os.writeC(2);
							os.writeC(100);
							os.writeC(5);
							break;
						default:
							if (getEnchantLevel() > 8) {
								os.writeC(2);
								os.writeC(0);
							}
							break;
						}
					} else if (getItemId() == 9502) { // 수호의 명궁휘장
						switch (getEnchantLevel()) {
						case 5:
							os.writeC(2); // Ac
							os.writeC(3);
							os.writeC(14); // HP
							os.writeH(30);
							os.writeC(63); // 대미지감소
							os.writeC(1);
							os.writeC(35); // 추타
							os.writeC(1);
							break;
						case 6:
							os.writeC(2); // Ac
							os.writeC(5);
							os.writeC(14); // hp
							os.writeH(35);
							os.writeC(63); // 대미지 감소
							os.writeC(2);
							os.writeC(15); // 마방
							os.writeH(3);
							os.writeC(35); // 원거리 대미지
							os.writeC(2);
							os.writeC(24); // 원거리 명중
							os.writeC(1);
							os.writeC(99);
							os.writeC(1);
							break;
						case 7:
							os.writeC(2); // Ac
							os.writeC(6);
							os.writeC(14); // Hp
							os.writeH(40);
							os.writeC(63); // 대미지 감소
							os.writeC(3);
							os.writeC(15); // 마방
							os.writeH(5);
							os.writeC(35); // 원거리 대미지
							os.writeC(3);
							os.writeC(24); // 원거리 명중
							os.writeC(3);
							os.writeC(60); // pvp대미지 감소
							os.writeC(1);
							os.writeC(99);
							os.writeC(3);
							break;
						case 8:
							os.writeC(2); // Ac
							os.writeC(7);
							os.writeC(14); // HP
							os.writeH(50);
							os.writeC(63); // 대미지 감소
							os.writeC(4);
							os.writeC(15); // 마방
							os.writeH(7);
							os.writeC(35); // 원거리 대미지
							os.writeC(4);
							os.writeC(24); // 원거리 명중
							os.writeC(5);
							os.writeC(60); // pvp대미지 감소
							os.writeC(2);
							os.writeC(99);
							os.writeC(5);
							break;
						default:
							if (getEnchantLevel() > 8) {
								os.writeC(2);
								os.writeC(0);
							}
							break;
						}
					} else if (getItemId() == 9503) { // 수호의 현자휘장
						switch (getEnchantLevel()) {
						case 5:
							os.writeC(2); // Ac
							os.writeC(3);
							os.writeC(14); // HP
							os.writeH(30);
							os.writeC(63); // 대미지감소
							os.writeC(1);
							os.writeC(5); // 근거리 명중
							os.writeC(1);
							break;
						case 6:
							os.writeC(2); // Ac
							os.writeC(5);
							os.writeC(14); // hp
							os.writeH(35);
							os.writeC(63); // 대미지 감소
							os.writeC(2);
							os.writeC(15); // 마방
							os.writeH(3);

							os.writeC(40); // 마법 적중
							os.writeC(1);

							os.writeC(5); // 근거리 명중
							os.writeC(2);

							os.writeC(50);
							os.writeH(1);
							break;
						case 7:
							os.writeC(2); // Ac
							os.writeC(6);
							os.writeC(14); // Hp
							os.writeH(40);
							os.writeC(63); // 대미지 감소
							os.writeC(3);
							os.writeC(15); // 마방
							os.writeH(5);

							os.writeC(40); // 마법적중
							os.writeC(3);

							os.writeC(5); // 근거리 명중
							os.writeC(3);

							os.writeC(60); // pvp대미지 감소
							os.writeC(1);
							os.writeC(50);
							os.writeH(2);
							break;
						case 8:
							os.writeC(2); // Ac
							os.writeC(7);
							os.writeC(14); // HP
							os.writeH(50);
							os.writeC(63); // 대미지 감소
							os.writeC(4);
							os.writeC(15); // 마방
							os.writeH(7);
							os.writeC(40); // 마법 적중
							os.writeC(5);
							os.writeC(5); // 근거리 명중
							os.writeC(4);
							os.writeC(60); // pvp대미지 감소
							os.writeC(2);
							os.writeC(50);
							os.writeH(4);
							break;
						default:
							if (getEnchantLevel() > 8) {
								os.writeC(2);
								os.writeC(0);
							}
							break;
						}
					} else if (getItemId() == 32910) { // 수호의 투사 문장
						switch (getEnchantLevel()) {
						case 5:
							os.writeC(47); // 근거리 대미지
							os.writeC(1);
							os.writeC(5); // 근거리 명중
							os.writeC(2);
							break;
						case 6:
							os.writeC(47); // 근거리 대미지
							os.writeC(2);
							os.writeC(5); // 근거리 명중
							os.writeC(3);
							break;
						case 7:
							os.writeC(47); // 근거리 대미지
							os.writeC(3);
							os.writeC(5); // 근거리 명중
							os.writeC(4);
							break;
						case 8:
							os.writeC(47); // 근거리 대미지
							os.writeC(4);
							os.writeC(5); // 근거리 명중
							os.writeC(5);
							break;
						default:
							break;
						}
					} else if (getItemId() == 32911) { // 수호의 명궁 문장
						switch (getEnchantLevel()) {
						case 5:
							os.writeC(35); // 원거리 대미지
							os.writeC(1);
							os.writeC(24); // 원거리 명중
							os.writeC(2);
							break;
						case 6:
							os.writeC(35); // 원거리 대미지
							os.writeC(2);
							os.writeC(24); // 원거리 명중
							os.writeC(3);
							break;
						case 7:
							os.writeC(35); // 원거리 대미지
							os.writeC(3);
							os.writeC(24); // 원거리 명중
							os.writeC(4);
							break;
						case 8:
							os.writeC(35); // 원거리 대미지
							os.writeC(4);
							os.writeC(24); // 원거리 명중
							os.writeC(5);
							break;
						default:
							break;
						}
					} else if (getItemId() == 32912) { // 수호의 현자 문장
						switch (getEnchantLevel()) {
						case 5:
							os.writeC(17);
							os.writeC(1);
							os.writeC(40); // 마법 적중
							os.writeC(2);
							break;
						case 6:
							os.writeC(17);
							os.writeC(2);
							os.writeC(40); // 마법 적중
							os.writeC(3);
							break;
						case 7:
							os.writeC(17);
							os.writeC(3);
							os.writeC(40); // 마법 적중
							os.writeC(4);
							break;
						case 8:
							os.writeC(17);
							os.writeC(4);
							os.writeC(40); // 마법 적중
							os.writeC(5);
							break;
						default:
							break;
						}
					} else if (getItemId() == 31910) { // 투사의 문장
						switch (getEnchantLevel()) {
						case 4:
							os.writeC(5); // 근거리 명중
							os.writeC(1);
							break;
						case 5:
							os.writeC(47); // 근거리 대미지
							os.writeC(1);
							os.writeC(5); // 근거리 명중
							os.writeC(1);
							break;
						case 6:
							os.writeC(47); // 근거리 대미지
							os.writeC(2);
							os.writeC(5); // 근거리 명중
							os.writeC(2);
							break;
						case 7:
							os.writeC(47); // 근거리 대미지
							os.writeC(3);
							os.writeC(5); // 근거리 명중
							os.writeC(3);
							break;
						case 8:
							os.writeC(47); // 근거리 대미지
							os.writeC(4);
							os.writeC(5); // 근거리 명중
							os.writeC(4);
							break;
						default:
							break;
						}
					} else if (getItemId() == 31911) { // 명궁의 문장
						switch (getEnchantLevel()) {
						case 4:
							os.writeC(24); // 원거리 명중
							os.writeC(1);
							break;
						case 5:
							os.writeC(35); // 원거리 대미지
							os.writeC(1);
							os.writeC(24); // 원거리 명중
							os.writeC(1);
							break;
						case 6:
							os.writeC(35); // 원거리 대미지
							os.writeC(2);
							os.writeC(24); // 원거리 명중
							os.writeC(2);
							break;
						case 7:
							os.writeC(35); // 원거리 대미지
							os.writeC(3);
							os.writeC(24); // 원거리 명중
							os.writeC(3);
							break;
						case 8:
							os.writeC(35); // 원거리 대미지
							os.writeC(4);
							os.writeC(24); // 원거리 명중
							os.writeC(4);
							break;
						default:
							break;
						}
					} else if (getItemId() == 31912) { // 현자의 문장
						switch (getEnchantLevel()) {
						case 4:
							os.writeC(40); // 마법 적중
							os.writeC(1);
							break;
						case 5:
							os.writeC(17);
							os.writeC(1);
							os.writeC(40); // 마법 적중
							os.writeC(1);
							break;
						case 6:
							os.writeC(17);
							os.writeC(2);
							os.writeC(40); // 마법 적중
							os.writeC(2);
							break;
						case 7:
							os.writeC(17);
							os.writeC(3);
							os.writeC(40); // 마법 적중
							os.writeC(3);
							break;
						case 8:
							os.writeC(17);
							os.writeC(4);
							os.writeC(40); // 마법 적중
							os.writeC(4);
							break;
						default:
							break;
						}
					} else if (getItemId() == 9300 || getItemId() == 9301 || getItemId() == 9302) {
						switch (getEnchantLevel()) {
						case 0:
							os.writeC(14);
							os.writeH(5);
							break;
						case 1:
							os.writeC(14);
							os.writeH(10);
							break;
						case 2:
							os.writeC(14);
							os.writeH(15);
							break;
						case 3:
							os.writeC(14);
							os.writeH(20);
							break;
						case 4:
							os.writeC(2);
							os.writeC(1);
							os.writeC(14);
							os.writeH(25);
							break;
						case 5:
							os.writeC(2);
							os.writeC(2);
							os.writeC(14);
							os.writeH(30);
							break;
						case 6:
							os.writeC(2);
							os.writeC(3);
							os.writeC(14);
							os.writeH(35);
							break;
						case 7:
							os.writeC(2);
							os.writeC(3);
							os.writeC(14);
							os.writeH(40);
							break;
						case 8:
							os.writeC(2);
							os.writeC(3);
							os.writeC(14);
							os.writeH(50);
							break;
						default:
							if (getEnchantLevel() > 8) {
								os.writeC(2);
								os.writeC(3);
								os.writeC(14);
								os.writeH(50);
							}
							break;
						}
					} else if (getItemId() == 9303) {
						switch (getEnchantLevel()) {
						case 0:
							os.writeC(14);
							os.writeH(5);
							break;
						case 1:
							os.writeC(14);
							os.writeH(10);
							break;
						case 2:
							os.writeC(14);
							os.writeH(15);
							break;
						case 3:
							os.writeC(2);
							os.writeC(1);
							os.writeC(14);
							os.writeH(20);
							break;
						case 4:
							os.writeC(2);
							os.writeC(2);
							os.writeC(14);
							os.writeH(25);
							break;
						case 5:
							os.writeC(2);
							os.writeC(3);
							os.writeC(14);
							os.writeH(30);
							break;
						case 6:
							os.writeC(2);
							os.writeC(5);
							os.writeC(14);
							os.writeH(35);
							break;
						case 7:
							os.writeC(2);
							os.writeC(6);
							os.writeC(14);
							os.writeH(40);
							break;
						case 8:
							os.writeC(2);
							os.writeC(7);
							os.writeC(14);
							os.writeH(50);
							break;
						default:
							if (getEnchantLevel() > 8) {
								os.writeC(2);
								os.writeC(7);
								os.writeC(14);
								os.writeH(50);
							}
							break;
						}
					} else if (getItem().getType() >= 9 && getItem().getType() <= 11 || getItem().getType() == 18) {
						os.writeC(2);
						os.writeC(0);
					} else {
						os.writeC(2);
						os.writeC(getEnchantLevel());
					}
				} else {
					if (getItemId() == 21253) { // 스냅퍼 용사 축복 반지
						switch (getEnchantLevel()) {
						case 0:
							break;
						case 1:
						case 2:
						case 3:
						case 4:
							os.writeC(2);
							os.writeC(getEnchantLevel());
							break;
						case 5:
						case 6:
							os.writeC(2);
							os.writeC(4);
							break;
						case 7:
						case 8:
							os.writeC(2);
							os.writeC(5);
							break;
						}
					} else if (getItemId() == 21249) { // 스냅퍼 용사 반지
						switch (getEnchantLevel()) {
						case 0:
							break;
						case 1:
						case 2:
						case 3:
						case 4:
							os.writeC(2);
							os.writeC(getEnchantLevel());
							break;
						case 5:
						case 6:
						case 7:
						case 8:
							os.writeC(2);
							os.writeC(4);
							break;
						}
					} else if (getItemId() == 21246) {
						switch (getEnchantLevel()) {
						case 0:
						case 1:
							break;
						case 2:
						case 3:
						case 4:
							os.writeC(2);
							os.writeC(getEnchantLevel() - 1);
							break;
						case 5:
							os.writeC(2);
							os.writeC(3);
							break;
						case 6:
						case 7:
							os.writeC(2);
							os.writeC(4);
							break;
						case 8:
							os.writeC(2);
							os.writeC(5);
							break;
						}
					} else if ((getItemId() >= 21247 && getItemId() <= 21248)) { // 스냅퍼반지
						switch (getEnchantLevel()) {
						case 0:
						case 1:
							break;
						case 2:
						case 3:
						case 4:
							os.writeC(2);
							os.writeC(getEnchantLevel() - 1);
							break;
						case 5:
						case 6:
						case 7:
						case 8:
							os.writeC(2);
							os.writeC(4);
							break;
						}
					} else if (getItemId() == 21252) { // 축 스냅퍼 체력
						switch (getEnchantLevel()) {
						case 0:
						case 1:
							break;
						case 2:
						case 3:
						case 4:
							os.writeC(2);
							os.writeC(getEnchantLevel() - 1);
							break;
						case 5:
						case 6:
							os.writeC(2);
							os.writeC(4);
							break;
						case 7:
						case 8:
							os.writeC(2);
							os.writeC(5);
							break;
						}
					} else if (getItemId() == 21250) { // 축 스냅퍼
						switch (getEnchantLevel()) {
						case 0:
						case 1:
							break;
						case 2:
						case 3:
						case 4:
							os.writeC(2);
							os.writeC(getEnchantLevel() - 1);
							break;
						case 5:
						case 6:
							os.writeC(2);
							os.writeC(3);
							break;
						case 7:
						case 8:
							os.writeC(2);
							os.writeC(4);
							break;
						}
					} else if (getItemId() == 21251) { // 축 스냅퍼 마저
						switch (getEnchantLevel()) {
						case 0:
						case 1:
							break;
						case 2:
						case 3:
						case 4:
							os.writeC(2);
							os.writeC(getEnchantLevel() - 1);
							break;
						case 5:
						case 6:
							os.writeC(2);
							os.writeC(4);
							break;
						case 7:
						case 8:
							os.writeC(2);
							os.writeC(5);
							break;
						}
					} else if (getItemId() == 500010 || getItemId() == 502010) {
						int ac = getEnchantLevel();
						if (getBless() == 0 && getEnchantLevel() >= 3) {
							ac += 1;
						}
						if (ac > 0) {
							os.writeC(2);
							os.writeC(ac);
						}
					} else if (getItemId() == 500007) {
						int ac = getEnchantLevel() + 1;
						if (getEnchantLevel() > 5) {
							if (ac > 0) {
								os.writeC(2);
								os.writeC(ac);
							}
						}
					} else if (getItemId() == 502007) {
						int ac = getEnchantLevel() + 1;
						if (getEnchantLevel() >= 5) {
							if (getBless() == 0) {
								ac += 1;
							}
							if (ac > 0) {
								os.writeC(2);
								os.writeC(ac);
							}
						}
					} else if (itemId == 500008) { // 룸티스
						switch (getEnchantLevel()) {
						case 5:
							os.writeC(2);
							os.writeC(1);
							break;
						case 6:
						case 7:
							os.writeC(2);
							os.writeC(2);
							break;
						case 8:
							os.writeC(2);
							os.writeC(3);
							break;
						default:
							os.writeC(2);
							os.writeC(0);
							break;
						}
					} else if (itemId == 500009) { // 룸티스
						switch (getEnchantLevel()) {
						case 6:
							os.writeC(2);
							os.writeC(1);
							break;
						case 7:
							os.writeC(2);
							os.writeC(2);
							break;
						case 8:
							os.writeC(2);
							os.writeC(3);
							break;
						default:
							os.writeC(2);
							os.writeC(0);
							break;
						}
					} else if (itemId == 502008) { // 룸티스
						switch (getEnchantLevel()) {
						case 4:
							os.writeC(2);
							os.writeC(1);
							break;
						case 5:
						case 6:
							os.writeC(2);
							os.writeC(2);
							break;
						case 7:
							os.writeC(2);
							os.writeC(3);
							break;
						case 8:
							os.writeC(2);
							os.writeC(4);
							break;
						default:
							os.writeC(2);
							os.writeC(0);
							break;
						}
					} else if (itemId == 502009) { // 룸티스
						switch (getEnchantLevel()) {
						case 5:
							os.writeC(2);
							os.writeC(1);
							break;
						case 6:
							os.writeC(2);
							os.writeC(2);
							break;
						case 7:
							os.writeC(2);
							os.writeC(3);
							break;
						case 8:
							os.writeC(2);
							os.writeC(4);
							break;
						default:
							os.writeC(2);
							os.writeC(0);
							break;
						}
					} else if (itemId == 502009) { // 룸티스
						switch (getEnchantLevel()) {
						case 5:
							os.writeC(2);
							os.writeC(1);
							break;
						case 6:
							os.writeC(2);
							os.writeC(2);
							break;
						case 7:
							os.writeC(2);
							os.writeC(3);
							break;
						case 8:
							os.writeC(2);
							os.writeC(4);
							break;
						default:
							os.writeC(2);
							os.writeC(0);
							break;
						}
					}
				}
			} else {

				if (itemId == 61 || itemId == 12 || itemId == 86 || itemId == 134 || itemId == 9100 || itemId == 9101
						|| itemId == 9103) {
					if (getEnchantLevel() == 0) {
						os.writeC(2);
						os.writeC(getEnchantLevel());
					} else if (getEnchantLevel() == 1) {
						os.writeC(2);
						os.writeC(2);
					} else if (getEnchantLevel() == 2) {
						os.writeC(2);
						os.writeC(4);
					} else if (getEnchantLevel() == 3) {
						os.writeC(2);
						os.writeC(6);
					} else if (getEnchantLevel() == 4) {
						os.writeC(2);
						os.writeC(8);
					} else if (getEnchantLevel() == 5) {
						os.writeC(2);
						os.writeC(10);
					} else if (getEnchantLevel() == 6) {
						os.writeC(2);
						os.writeC(11);
					} else if (getEnchantLevel() == 7) {
						os.writeC(2);
						os.writeC(12);
					} else if (getEnchantLevel() == 8) {
						os.writeC(2);
						os.writeC(13);
					} else if (getEnchantLevel() == 9) {
						os.writeC(2);
						os.writeC(14);
					} else if (getEnchantLevel() == 10) {
						os.writeC(2);
						os.writeC(15);
					} else if (getEnchantLevel() == 11) {
						os.writeC(2);
						os.writeC(16);
					} else if (getEnchantLevel() == 12) {
						os.writeC(2);
						os.writeC(17);
					} else if (getEnchantLevel() == 13) {
						os.writeC(2);
						os.writeC(18);
					} else if (getEnchantLevel() == 14) {
						os.writeC(2);
						os.writeC(19);
					}
				} else {
					os.writeC(2);
					os.writeC(getEnchantLevel());
				}
			}

			if (get_durability() != 0) {
				os.writeC(3);
				os.writeC(get_durability());
			}
			if (getItem().isTwohandedWeapon()) {
				os.writeC(4);
			}
			if (getItem().getItemId() == 10105) {
				os.writeC(50);
				os.writeH(3);
			}
			if (getItem().getItemId() == 21269) {
				os.writeC(0x28);
				os.writeC(3);
			}

			/** 무기 명중 **/
			if (getItem().getType2() == 1) {
				if (getItem().getType1() == 20 || getItem().getType1() == 62) {
					os.writeC(24); // 원거리 명중
					os.writeC(getItem().getHitModifier());
				} else {
					os.writeC(48); // 근거리 명중
					os.writeC(getItem().getHitModifier());
				}
			}

			/** 방어구 근거리 명중 **/
			else if (getItem().getType2() == 2) {
				if (getItemId() == 30912 || getItemId() == 30195 || getItemId() == 30199) {
					switch (getEnchantLevel()) {
					case 5:
					case 6:
						os.writeC(48);
						os.writeC(1);
						break;
					case 7:
						os.writeC(48);
						os.writeC(2);
						break;
					case 8:
						os.writeC(48);
						os.writeC(3);
						break;
					case 9:
						os.writeC(48);
						os.writeC(4);
						break;
					case 10:
						os.writeC(48);
						os.writeC(5);
						break;
					default:
						break;
					}
				} else if (getItemId() == 22110) {
					if (getEnchantLevel() >= 5) {
						os.writeC(48);
						os.writeC(getEnchantLevel() - 4);
					}
				} else if (getItemId() == 9302) {
					switch (getEnchantLevel()) {
					case 5:
						os.writeC(48);
						os.writeC(1);
						break;
					case 6:
						os.writeC(48);
						os.writeC(2);
						os.writeC(50);
						os.writeH(1);
						break;
					case 7:
						os.writeC(48);
						os.writeC(3);
						os.writeC(50);
						os.writeH(2);
						break;
					case 8:
						os.writeC(48);
						os.writeC(4);
						os.writeC(50);
						os.writeH(4);
						break;
					default:
						if (getEnchantLevel() > 8) {
							os.writeC(48);
							os.writeC(4);
							os.writeC(50);
							os.writeH(4);
						}
						break;
					}
				} else if (getItemId() == 21249) { // 스냅퍼 용사 반지
					switch (getEnchantLevel()) {
					case 0:
					case 1:
					case 2:
					case 3:
					case 4:
						break;
					case 5:
					case 6:
					case 7:
					case 8:
						os.writeC(48);
						os.writeC((getEnchantLevel() - 4));
						break;
					}
				} else if (getItemId() == 21253) { // 스냅퍼 용사 반지
					switch (getEnchantLevel()) {
					case 0:
					case 1:
					case 2:
					case 3:
						break;
					case 4:
					case 5:
					case 6:
					case 7:
					case 8:
						os.writeC(48);
						os.writeC((getEnchantLevel() - 3));
						break;
					}
				} else if (getItemId() == 9200 && getEnchantLevel() >= 5) { // 수호성
																			// 파글
					os.writeC(48);
					os.writeC(getEnchantLevel() - 4);
				} else if (getItemId() == 130220 && getEnchantLevel() >= 7) { // 격분의
																				// 장갑
					os.writeC(48);
					os.writeC(getEnchantLevel() - 3);
				} else if (getItemId() == 500007 && getEnchantLevel() >= 7) {
					switch (getEnchantLevel()) {
					case 7:
						os.writeC(48);
						os.writeC(1);
						break;
					case 8:
						os.writeC(48);
						os.writeC(3);
						break;
					}
				} else if (getItemId() == 502007 && getEnchantLevel() >= 6) {
					switch (getEnchantLevel()) {
					case 6:
						os.writeC(48);
						os.writeC(1);
						break;
					case 7:
						os.writeC(48);
						os.writeC(3);
						break;
					case 8:
						os.writeC(48);
						os.writeC(5);
						break;
					}
				} else if (getItemId() == 500221 && getEnchantLevel() > 4) {
					os.writeC(48);
					os.writeC(getEnchantLevel() - 4);
				} else if (getItemId() == 9115) {
					switch (getEnchantLevel()) {
					case 8:
						os.writeC(48);
						os.writeC(2);
						break;
					case 9:
						os.writeC(48);
						os.writeC(4);
						break;
					case 10:
						os.writeC(48);
						os.writeC(6);
						break;
					default:
						break;
					}
				} else if (getItemId() == 20049||getItemId() == 20079||getItemId() == 20107) { // By.코봉 25인첸 피엠통,리덕작업
					switch (getEnchantLevel()) {
					case 25:
						os.writeC(73);
						os.writeS("HP +100, MP +50");
						os.writeC(74);
						os.writeS("데미지 감소: 13%");
						break;
						
					default:
						break;
					}
				} else if (getItemId() == 91150) {
					switch (getEnchantLevel()) {
					case 7:
						os.writeC(48);
						os.writeC(1);
						break;
					case 8:
						os.writeC(48);
						os.writeC(3);
						break;
					case 9:
						os.writeC(48);
						os.writeC(5);
						break;
					case 10:
						os.writeC(48);
						os.writeC(7);
						break;
					default:
						break;
					}
				} else if (getItem().getHitup() != 0) {
					if (getItemId() == 10102) { // 쿠거 가더 인첸당 명중
						if (getEnchantLevel() > 4) {
							os.writeC(48);
							os.writeC(getItem().getHitup() + getEnchantLevel() - 4);
						} else {
							os.writeC(48);
							os.writeC(getItem().getHitup());
						}
					} else {
						os.writeC(48);
						os.writeC(getItem().getHitup());
					}
				}

				/** 방어구 원거리 명중 **/
				if (getItemId() == 21249) { // 스냅퍼 용사 반지
					switch (getEnchantLevel()) {
					case 5:
					case 6:
					case 7:
					case 8:
						os.writeC(24);
						os.writeC(getEnchantLevel() - 4);
						break;
					}
				} else if (getItemId() == 21253) { // 스냅퍼 용사 반지
					switch (getEnchantLevel()) {
					case 4:
					case 5:
					case 6:
					case 7:
					case 8:
						os.writeC(24);
						os.writeC(getEnchantLevel() - 3);
						break;
					}
				} else if (getItemId() == 30913 || getItemId() == 30196 || getItemId() == 30200) {
					switch (getEnchantLevel()) {
					case 5:
					case 6:
						os.writeC(24);
						os.writeC(1);
						break;
					case 7:
						os.writeC(24);
						os.writeC(2);
						break;
					case 8:
						os.writeC(24);
						os.writeC(3);
						break;
					case 9:
						os.writeC(24);
						os.writeC(4);
						break;
					case 10:
						os.writeC(24);
						os.writeC(5);
						break;
					default:
						break;
					}
				} else if (getItemId() == 9201 && getEnchantLevel() >= 5) { // 수호성
																			// 파글
					os.writeC(24);
					os.writeC(getEnchantLevel() - 4);
				} else if (getItemId() == 22111) {
					if (getEnchantLevel() >= 5) {
						os.writeC(24);
						os.writeC(getEnchantLevel() - 4);
					}
				} else if (getItemId() == 500007 && getEnchantLevel() >= 7) {
					switch (getEnchantLevel()) {
					case 7:
						os.writeC(24);
						os.writeC(1);
						break;
					case 8:
						os.writeC(24);
						os.writeC(3);
						break;
					}
				} else if (getItemId() == 502007 && getEnchantLevel() >= 6) {
					switch (getEnchantLevel()) {
					case 6:
						os.writeC(24);
						os.writeC(1);
						break;
					case 7:
						os.writeC(24);
						os.writeC(3);
						break;
					case 8:
						os.writeC(24);
						os.writeC(5);
						break;
					}
				} else if (getItemId() == 500222 && getEnchantLevel() > 4) {
					os.writeC(24);
					os.writeC(getEnchantLevel() - 4);
				} else if (getItemId() == 9116) {
					switch (getEnchantLevel()) {
					case 8:
						os.writeC(24);
						os.writeC(2);
						break;
					case 9:
						os.writeC(24);
						os.writeC(4);
						break;
					case 10:
						os.writeC(24);
						os.writeC(6);
						break;
					default:
						break;
					}
				} else if (getItemId() == 91160) {
					switch (getEnchantLevel()) {
					case 7:
						os.writeC(24);
						os.writeC(1);
						break;
					case 8:
						os.writeC(24);
						os.writeC(3);
						break;
					case 9:
						os.writeC(24);
						os.writeC(5);
						break;
					case 10:
						os.writeC(24);
						os.writeC(7);
						break;
					default:
						break;
					}

				} else if (getItem().getBowHitup() != 0) {
					if (getItemId() == 10103) { // 우그누스 가더 인첸당 명중
						if (getEnchantLevel() > 4) {
							os.writeC(24);
							os.writeC(getItem().getBowHitup() + getEnchantLevel() - 4);
						} else {
							os.writeC(24);
							os.writeC(getItem().getBowHitup());
						}
					} else {
						os.writeC(24);
						os.writeC(getItem().getBowHitup());
					}
				}
			}

			/** 근거리 대미지 **/
			if (getItem().getType2() == 1) {
				if (getItem().getType() != 7 && getItem().getType() != 17 && getStepEnchantLevel() != 0) {// 검추타주문서이용한...시발
					os.writeC(47);
					os.writeC(getItem().getDmgModifier() + (getStepEnchantLevel() * 2));
				} else if (getItem().getType1() == 20 || getItem().getType1() == 62) {
					os.writeC(35);
					os.writeC(getItem().getDmgModifier());
				} else {
					os.writeC(47);
					os.writeC(getItem().getDmgModifier());
				}
			} else if (getItem().getGrade() != 3 && itemType2 == 2
					&& (getItem().getType() == 9 || getItem().getType() == 11)) { // 반지~
				switch (getEnchantLevel()) {
				case 5:
					os.writeC(47);
					os.writeC(getItem().getDmgup() + 1);
					break;
				case 6:
					os.writeC(47);
					os.writeC(getItem().getDmgup() + 2);
					break;
				case 7:
					os.writeC(47);
					os.writeC(getItem().getDmgup() + 3);
					break;
				case 8:
					os.writeC(47);
					os.writeC(getItem().getDmgup() + 4);
					break;
				case 9:
					os.writeC(47);
					os.writeC(getItem().getDmgup() + 5);
					break;
				case 10:
					os.writeC(47);
					os.writeC(getItem().getDmgup() + 6);
					break;
				case 11:
					os.writeC(47);
					os.writeC(getItem().getDmgup() + 7);
					break;
				case 12:
					os.writeC(47);
					os.writeC(getItem().getDmgup() + 8);
					break;
				default:
					break;
				}

			} else if (getItemId() == 9125 && getEnchantLevel() >= 9) {
				os.writeC(47);
				os.writeC(1);
			} else if (getItemId() == 9115 || getItemId() == 91150) {
				switch (getEnchantLevel()) {
				case 7:
				case 8:
					os.writeC(47);
					os.writeC(1);
					break;
				case 9:
				case 10:
				case 11:
				case 12:
					os.writeC(47);
					os.writeC(2);
					break;
				default:
					break;
				}
			} else if (getItemId() == 9300) {
				switch (getEnchantLevel()) {
				case 5:
					os.writeC(47);
					os.writeC(1);
					break;
				case 6:
					os.writeC(47);
					os.writeC(2);
					os.writeC(100);
					os.writeC(1);
					break;
				case 7:
					os.writeC(47);
					os.writeC(3);
					os.writeC(100);
					os.writeC(3);
					break;
				case 8:
					os.writeC(47);
					os.writeC(4);
					os.writeC(100);
					os.writeC(5);
					break;
				default:
					if (getEnchantLevel() > 8) {
						os.writeC(47);
						os.writeC(4);
						os.writeC(100);
						os.writeC(5);
					}
					break;
				}
			} else if (getItemId() == 30912 || getItemId() == 30195 || getItemId() == 30199) {
				switch (getEnchantLevel()) {
				case 6:
					os.writeC(47);
					os.writeC(1);
					break;
				case 7:
					os.writeC(47);
					os.writeC(2);
					break;
				case 8:
					os.writeC(47);
					os.writeC(3);
					break;
				case 9:
					os.writeC(47);
					os.writeC(4);
					break;
				case 10:
					os.writeC(47);
					os.writeC(5);
					break;
				default:
					break;
				}
			} else if (getItemId() == 500010 || getItemId() == 502010) { // 룸티스검은
				if (getEnchantLevel() >= 3) {
					os.writeC(47);
					int dm = getEnchantLevel() - 2;
					if (getBless() != 0 && getEnchantLevel() >= 4)
						dm -= 1;
					os.writeC(getItem().getDmgup() + dm);
				}
			} else if (getItemId() >= 21247 && getItemId() <= 21249) {
				switch (getEnchantLevel()) {
				case 5:
					os.writeC(47);
					os.writeC(getItem().getDmgup() + 1);
					break;
				case 6:
					os.writeC(47);
					os.writeC(getItem().getDmgup() + 2);
					break;
				case 7:
					os.writeC(47);
					os.writeC(getItem().getDmgup() + 3);
					break;
				case 8:
					os.writeC(47);
					os.writeC(getItem().getDmgup() + 4);
					break;
				default:
					break;
				}
			} else if (getItemId() >= 21251 && getItemId() <= 21253) {
				switch (getEnchantLevel()) {
				case 4:
					os.writeC(47);
					os.writeC(getItem().getDmgup() + 1);
					break;
				case 5:
					os.writeC(47);
					os.writeC(getItem().getDmgup() + 2);
					break;
				case 6:
					os.writeC(47);
					os.writeC(getItem().getDmgup() + 3);
					break;
				case 7:
					os.writeC(47);
					os.writeC(getItem().getDmgup() + 4);
					break;
				case 8:
					os.writeC(47);
					os.writeC(getItem().getDmgup() + 5);
					break;
				default:
					break;
				}
			} else if (getItemId() == 420003) {
				switch (getEnchantLevel()) {
				case 0:
				case 1:
				case 2:
				case 3:
				case 4:
					os.writeC(47);
					os.writeC(getItem().getDmgup());
					break;
				case 5:
				case 6:
					os.writeC(47);
					os.writeC(getItem().getDmgup() + 1);
					break;
				case 7:
				case 8:
					os.writeC(47);
					os.writeC(getItem().getDmgup() + 2);
					break;
				case 9:
					os.writeC(47);
					os.writeC(getItem().getDmgup() + 3);
					break;
				}
			} else if (getItem().getDmgup() != 0) {
				if (getItemId() == 22110 && getEnchantLevel() >= 7) {
					os.writeC(47);
					os.writeC(getItem().getDmgup() + (getEnchantLevel() - 6));
				} else {
					os.writeC(47);
					os.writeC(getItem().getDmgup());
				}
			}
			/** 원거리 대미지 **/
			if (getItem().getType1() == 20 && getItem().getType1() == 62 && getStepEnchantLevel() != 0) {// 검추타주문서이용한...시발
				os.writeC(35);
				os.writeC(getItem().getBowDmgup() + (getStepEnchantLevel() * 2));
			} else if (getItem().getGrade() != 3 && itemType2 == 2
					&& (getItem().getType() == 9 || getItem().getType() == 11)) { // 반지~
				switch (getEnchantLevel()) {
				case 5:
					os.writeC(35);
					os.writeC(getItem().getBowDmgup() + 1);
					break;
				case 6:
					os.writeC(35);
					os.writeC(getItem().getBowDmgup() + 2);
					break;
				case 7:
					os.writeC(35);
					os.writeC(getItem().getBowDmgup() + 3);
					break;
				case 8:
					os.writeC(35);
					os.writeC(getItem().getBowDmgup() + 4);
					break;
				case 9:
					os.writeC(35);
					os.writeC(getItem().getBowDmgup() + 5);
					break;
				case 10:
					os.writeC(35);
					os.writeC(getItem().getBowDmgup() + 6);
					break;
				case 11:
					os.writeC(35);
					os.writeC(getItem().getBowDmgup() + 7);
					break;
				case 12:
					os.writeC(35);
					os.writeC(getItem().getBowDmgup() + 8);
					break;
				default:
					break;
				}
			} else if (getItemId() == 9126 && getEnchantLevel() >= 9) {
				os.writeC(35);
				os.writeC(1);
			} else if (getItemId() == 22111 && getEnchantLevel() >= 7) {
				os.writeC(35);
				os.writeC(getItem().getBowDmgup() + (getEnchantLevel() - 6));
			} else if (getItemId() == 20017 || getItemId() == 120017) {
				switch (getEnchantLevel()) {
				case 7:
					os.writeC(35);
					os.writeC(1);
					break;
				case 8:
					os.writeC(35);
					os.writeC(2);
					break;
				case 9:
				case 10:
				case 11:
				case 12:
					os.writeC(35);
					os.writeC(3);
					break;
				default:
					break;
				}
			} else if (getItemId() == 9116 || getItemId() == 91160) {
				switch (getEnchantLevel()) {
				case 7:
				case 8:
					os.writeC(35);
					os.writeC(1);
					break;
				case 9:
				case 10:
				case 11:
				case 12:
					os.writeC(35);
					os.writeC(2);
					break;
				default:
					break;
				}
			} else if (getItemId() == 9301) {
				switch (getEnchantLevel()) {
				case 5:
					os.writeC(35);
					os.writeC(1);
					break;
				case 6:
					os.writeC(35);
					os.writeC(2);
					os.writeC(99);
					os.writeC(1);
					break;
				case 7:
					os.writeC(35);
					os.writeC(3);
					os.writeC(99);
					os.writeC(3);
					break;
				case 8:
					os.writeC(35);
					os.writeC(4);
					os.writeC(99);
					os.writeC(5);
					break;
				default:
					if (getEnchantLevel() > 8) {
						os.writeC(35);
						os.writeC(4);
						os.writeC(99);
						os.writeC(5);
					}
					break;
				}
			} else if (getItemId() == 30913 || getItemId() == 30196 || getItemId() == 30200) {
				switch (getEnchantLevel()) {
				case 6:
					os.writeC(35);
					os.writeC(1);
					break;
				case 7:
					os.writeC(35);
					os.writeC(2);
					break;
				case 8:
					os.writeC(35);
					os.writeC(3);
					break;
				case 9:
					os.writeC(35);
					os.writeC(4);
					break;
				case 10:
					os.writeC(35);
					os.writeC(5);
					break;
				default:
					break;
				}
			} else if (getItemId() == 420000) { // 고대 명궁의 가더 인첸 추타
				switch (getEnchantLevel()) {
				case 0:
				case 1:
				case 2:
				case 3:
				case 4:
					os.writeC(35);
					os.writeC(getItem().getBowDmgup());
					break;
				case 5:
				case 6:
					os.writeC(35);
					os.writeC(getItem().getBowDmgup() + 1);
					break;
				case 7:
				case 8:
					os.writeC(35);
					os.writeC(getItem().getBowDmgup() + 2);
					break;
				case 9:
					os.writeC(35);
					os.writeC(getItem().getBowDmgup() + 3);
					break;
				}
			} else if (getItemId() == 500010 || getItemId() == 502010) { // 룸티스검은
				if (getEnchantLevel() >= 3) {
					os.writeC(35);
					int dm = getEnchantLevel() - 2;
					if (getBless() != 0 && getEnchantLevel() >= 4)
						dm -= 1;
					os.writeC(dm);

					if (getItemId() == 502010) { // 축
						if (getEnchantLevel() >= 4) {
							os.writeC(95);
							os.writeC(getEnchantLevel() - 2);
							os.writeC(20);
						}
					} else if (getItemId() == 500010) { // 걍
						if (getEnchantLevel() >= 5) {
							os.writeC(95);
							os.writeC(getEnchantLevel() - 3);
							os.writeC(20);
						}
					}

				}
			} else if (getItemId() >= 21247 && getItemId() <= 21249) { // 스냅퍼 용사
																		// 반지
				switch (getEnchantLevel()) {
				case 5:
				case 6:
				case 7:
				case 8:
					os.writeC(35);
					os.writeC(getEnchantLevel() - 4);
					break;
				}
			} else if (getItemId() >= 21251 && getItemId() <= 21253) { // 스냅퍼 용사
																		// 반지
				switch (getEnchantLevel()) {
				case 4:
				case 5:
				case 6:
				case 7:
				case 8:
					os.writeC(35);
					os.writeC(getEnchantLevel() - 3);
					break;
				}
			} else if (getItem().getGrade() == 3) { // 순백 반지
				if ((getItem().getItemId() >= 425109 && getItem().getItemId() <= 425113)) {
					switch (getEnchantLevel()) {
					case 5:
						os.writeC(35);
						os.writeC(getItem().getBowDmgup() + 1);
						break;
					case 6:
						os.writeC(35);
						os.writeC(getItem().getBowDmgup() + 2);
						break;
					case 7:
						os.writeC(35);
						os.writeC(getItem().getBowDmgup() + 3);
						break;
					case 8:
						os.writeC(35);
						os.writeC(getItem().getBowDmgup() + 4);
						break;
					default:
						break;
					}
				}
			} else if (getItem().getBowDmgup() != 0) {
				os.writeC(35);
				os.writeC(getItem().getBowDmgup());
			}

			if (itemId == 126 || itemId == 127 || itemId == 450012 || itemId == 412002 || itemId == 450011
					|| itemId == 450023 || itemId == 450025 || itemId == 450013 || itemId == 413103) {
				os.writeC(16);
			}
			if (itemId == 412001 || itemId == 450009 || itemId == 450008 || itemId == 450010 || itemId == 262
					|| itemId == 12 || itemId == 1412001) {
				os.writeC(34);
			}

			int bit = 0;
			bit |= getItem().isUseRoyal() ? 1 : 0;
			bit |= getItem().isUseKnight() ? 2 : 0;
			bit |= getItem().isUseElf() ? 4 : 0;
			bit |= getItem().isUseMage() ? 8 : 0;
			bit |= getItem().isUseDarkelf() ? 16 : 0;
			bit |= getItem().isUseDragonKnight() ? 32 : 0;
			bit |= getItem().isUseBlackwizard() ? 64 : 0;
			bit |= getItem().isUseWarrior() ? 128 : 0;
			// bit |= getItem().isUseHighPet() ? 128 : 0;
			if (itemType2 != 2 || getItem().getType() != 12 || bit != 127) {
				os.writeC(7);
				os.writeC(bit);
			}

			// STR~CHA
			if (getItem().get_addstr() != 0) {
				os.writeC(8);
				os.writeC(getItem().get_addstr());
			}
			if (getItem().get_adddex() != 0) {
				os.writeC(9);
				os.writeC(getItem().get_adddex());
			}
			if (getItem().get_addcon() != 0) {
				os.writeC(10);
				os.writeC(getItem().get_addcon());
			}
			if (getItem().get_addwis() != 0) {
				os.writeC(11);
				os.writeC(getItem().get_addwis());
			}
			if (getStepEnchantLevel() != 0 && getItem().getType2() == 1 && getItem().getType() == 17) {
				os.writeC(12);
				os.writeC(getItem().get_addint() + getStepEnchantLevel());
			} else if (getItem().get_addint() != 0) {
				os.writeC(12);
				os.writeC(getItem().get_addint());
			}

			if (itemId == 22222) { // 칠흑의 망토 카리 증가
				if (getEnchantLevel() >= 7) {
					os.writeC(13);
					os.writeC(getEnchantLevel() - 5);
				} else {
					os.writeC(13);
					os.writeC(getItem().get_addcha());
				}
			} else if (itemId == 118) { // 칠흑의 수정구 카리 증가
				if (getEnchantLevel() >= 9) {
					os.writeC(13);
					os.writeC(getItem().get_addcha() + 1);
				} else {
					os.writeC(13);
					os.writeC(getItem().get_addcha());
				}
			} else if (itemId == 20112 || itemId == 120112 || itemId == 20016 || itemId == 120016) { // 맘보
																										// 셋
																										// 카리
																										// 증가
				if (getEnchantLevel() >= 7) {
					os.writeC(13);
					os.writeC(getItem().get_addcha() + 1);
				} else {
					os.writeC(13);
					os.writeC(getItem().get_addcha());
				}
			} else if (getItem().get_addcha() != 0) {
				os.writeC(13);
				os.writeC(getItem().get_addcha());
			}

			/** 룸티스 물약회복표시 **/
			/*
			 * if (itemId == 500008 && getEnchantLevel() >= 0){ os.writeC(39);
			 * os.writeS(RootisHealingPotion()); }
			 */
			if (itemId == 293 || itemId == 189 || itemId == 100189) {
				os.writeC(99);
				os.writeC(3);
			}
			if (itemId == 30220) {
				os.writeC(99);
				os.writeC(2);
			}
			if (itemId == 413105) {
				os.writeC(99);
				os.writeC(1);
			}

			/** 타이탄의 분노 **/
			if (itemId == 9103) {
				os.writeC(102);
				os.writeC(5);
			}

			if (itemId == 20050) {
				if (getEnchantLevel() == 5) {
					os.writeC(93);
					os.writeC(1);
				} else if (getEnchantLevel() == 6) {
					os.writeC(93);
					os.writeC(2);
				} else if (getEnchantLevel() == 7) {
					os.writeC(93);
					os.writeC(3);
				} else if (getEnchantLevel() == 8) {
					os.writeC(93);
					os.writeC(4);
				} else if (getEnchantLevel() >= 9) {
					os.writeC(93);
					os.writeC(5);
				}
			}

			if (itemId == 6101) {
				switch (getEnchantLevel()) {
				case 0:
				case 1:
				case 2:
				case 3:
				case 4:
				case 5:
				case 6:
					os.writeC(50);
					os.writeH(1);
					break;
				case 7:
					os.writeC(50);
					os.writeH(2);
					break;
				case 8:
					os.writeC(50);
					os.writeH(3);
					break;
				case 9:
					os.writeC(50);
					os.writeH(4);
					break;
				default:
					if (getEnchantLevel() >= 10) {
						os.writeC(50);
						os.writeH(5);
					}
					break;
				}
			}
			if (itemId == 203019) {
				os.writeC(100);
				os.writeC(10);
			}
			if (itemId == 61 || itemId == 86 || itemId == 134 || itemId == 9100 || itemId == 9102 || itemId == 9101
					|| itemId == 9103) {
				switch (getEnchantLevel()) {
				case 0:
					if (itemId == 61) {
						os.writeC(100);
						os.writeC(1);
					}
					if (itemId == 9100) {
						os.writeC(99);
						os.writeC(1);
						os.writeC(63);
						os.writeC(2);
						os.writeC(97);
						os.writeC(9);
					}
					if (itemId == 9101) {
						os.writeC(100);
						os.writeC(1);
					}
					if (itemId == 9102) {
						os.writeC(50);
						os.writeH(1);
					}
					if (itemId == 9103) {
						os.writeC(100);
						os.writeC(1);
					}
					if (itemId == 134) {
						os.writeC(40);
						os.writeC(2);
					}
					break;
				case 1:
					if (itemId == 61) {
						os.writeC(100);
						os.writeC(2);
					}
					if (itemId == 9100) {
						os.writeC(99);
						os.writeC(2);
						os.writeC(63);
						os.writeC(2);
						os.writeC(97);
						os.writeC(10);
					}
					if (itemId == 9101) {
						os.writeC(100);
						os.writeC(2);
					}
					if (itemId == 9102) {
						os.writeC(50);
						os.writeH(2);
					}
					if (itemId == 9103) {
						os.writeC(100);
						os.writeC(2);
					}
					if (itemId == 134) {
						os.writeC(40);
						os.writeC(3);
					}
					break;
				case 2:
					if (itemId == 61) {
						os.writeC(100);
						os.writeC(3);
					}
					if (itemId == 9100) {
						os.writeC(99);
						os.writeC(3);
						os.writeC(63);
						os.writeC(2);
						os.writeC(97);
						os.writeC(11);
					}
					if (itemId == 9101) {
						os.writeC(100);
						os.writeC(3);
					}
					if (itemId == 9102) {
						os.writeC(50);
						os.writeH(3);
					}
					if (itemId == 9103) {
						os.writeC(100);
						os.writeC(3);
					}
					if (itemId == 134) {
						os.writeC(40);
						os.writeC(4);
					}
					break;
				case 3:
					if (itemId == 61) {
						os.writeC(100);
						os.writeC(4);
					}
					if (itemId == 9100) {
						os.writeC(99);
						os.writeC(4);
						os.writeC(63);
						os.writeC(2);
						os.writeC(97);
						os.writeC(12);
					}
					if (itemId == 9101) {
						os.writeC(100);
						os.writeC(4);
					}
					if (itemId == 9102) {
						os.writeC(50);
						os.writeH(4);
					}
					if (itemId == 9103) {
						os.writeC(100);
						os.writeC(4);
					}
					if (itemId == 134) {
						os.writeC(40);
						os.writeC(5);
					}
					break;
				case 4:
					if (itemId == 61) {
						os.writeC(100);
						os.writeC(5);
					}
					if (itemId == 9100) {
						os.writeC(99);
						os.writeC(5);
						os.writeC(63);
						os.writeC(2);
						os.writeC(97);
						os.writeC(13);
					}
					if (itemId == 9101) {
						os.writeC(100);
						os.writeC(5);
					}
					if (itemId == 9102) {
						os.writeC(50);
						os.writeH(5);
					}
					if (itemId == 9103) {
						os.writeC(100);
						os.writeC(5);
					}
					if (itemId == 134) {
						os.writeC(40);
						os.writeC(6);
					}
					if (itemId == 86) {
					}
					break;
				case 5:
					if (itemId == 61) {
						os.writeC(100);
						os.writeC(6);
					}
					if (itemId == 9100) {
						os.writeC(99);
						os.writeC(6);
						os.writeC(63);
						os.writeC(2);
						os.writeC(97);
						os.writeC(14);
					}
					if (itemId == 9101) {
						os.writeC(100);
						os.writeC(6);
					}
					if (itemId == 9102) {
						os.writeC(50);
						os.writeH(6);
					}
					if (itemId == 9103) {
						os.writeC(100);
						os.writeC(6);
					}
					if (itemId == 134) {
						os.writeC(40);
						os.writeC(7);
					}
					if (itemId == 86) {
					}
					break;
				case 6:
					if (itemId == 61) {
						os.writeC(100);
						os.writeC(7);
					}
					if (itemId == 9100) {
						os.writeC(99);
						os.writeC(7);
						os.writeC(63);
						os.writeC(2);
						os.writeC(97);
						os.writeC(15);
					}
					if (itemId == 9101) {
						os.writeC(100);
						os.writeC(7);
					}
					if (itemId == 9102) {
						os.writeC(50);
						os.writeH(7);
					}
					if (itemId == 9103) {
						os.writeC(100);
						os.writeC(7);
					}
					if (itemId == 134) {
						os.writeC(40);
						os.writeC(8);
					}
					if (itemId == 86) {
					}
					break;
				case 7:
					if (itemId == 61) {
						os.writeC(100);
						os.writeC(8);
					}
					if (itemId == 9100) {
						os.writeC(99);
						os.writeC(8);
						os.writeC(63);
						os.writeC(2);
						os.writeC(97);
						os.writeC(16);
					}
					if (itemId == 9101) {
						os.writeC(100);
						os.writeC(8);
					}
					if (itemId == 9102) {
						os.writeC(50);
						os.writeH(8);
					}
					if (itemId == 9103) {
						os.writeC(100);
						os.writeC(8);
					}
					if (itemId == 134) {
						os.writeC(40);
						os.writeC(9);
					}
					if (itemId == 86) {
					}
					break;
				case 8:
					if (itemId == 61) {
						os.writeC(100);
						os.writeC(9);
					}
					if (itemId == 9100) {
						os.writeC(99);
						os.writeC(9);
						os.writeC(63);
						os.writeC(2);
						os.writeC(97);
						os.writeC(17);
					}
					if (itemId == 9101) {
						os.writeC(100);
						os.writeC(9);
					}
					if (itemId == 9102) {
						os.writeC(50);
						os.writeH(9);
					}
					if (itemId == 9103) {
						os.writeC(100);
						os.writeC(9);
					}
					if (itemId == 134) {
						os.writeC(40);
						os.writeC(10);
					}
					break;
				case 9:
					if (itemId == 61) {
						os.writeC(100);
						os.writeC(10);
					}
					if (itemId == 9100) {
						os.writeC(99);
						os.writeC(10);
						os.writeC(63);
						os.writeC(2);
						os.writeC(97);
						os.writeC(18);
					}
					if (itemId == 9101) {
						os.writeC(100);
						os.writeC(10);
					}
					if (itemId == 9102) {
						os.writeC(50);
						os.writeH(10);
					}
					if (itemId == 9103) {
						os.writeC(100);
						os.writeC(10);
					}
					if (itemId == 134) {
						os.writeC(40);
						os.writeC(11);
					}
					break;
				case 10:
					if (itemId == 61) {
						os.writeC(100);
						os.writeC(11);
					}
					if (itemId == 9100) {
						os.writeC(99);
						os.writeC(11);
						os.writeC(63);
						os.writeC(2);
						os.writeC(97);
						os.writeC(19);
					}
					if (itemId == 9101) {
						os.writeC(100);
						os.writeC(11);
					}
					if (itemId == 9102) {
						os.writeC(50);
						os.writeH(11);
					}
					if (itemId == 9103) {
						os.writeC(100);
						os.writeC(11);
					}
					if (itemId == 134) {
						os.writeC(40);
						os.writeC(12);
					}
					break;
				case 11:
					if (itemId == 61) {
						os.writeC(100);
						os.writeC(12);
					}
					if (itemId == 9100) {
						os.writeC(99);
						os.writeC(12);
						os.writeC(63);
						os.writeC(2);
						os.writeC(97);
						os.writeC(20);
					}
					if (itemId == 9101) {
						os.writeC(100);
						os.writeC(12);
					}
					if (itemId == 9102) {
						os.writeC(50);
						os.writeH(12);
					}
					if (itemId == 9103) {
						os.writeC(100);
						os.writeC(12);
					}
					if (itemId == 134) {
						os.writeC(40);
						os.writeC(13);
					}
					break;
				case 12:
					if (itemId == 61) {
						os.writeC(100);
						os.writeC(13);
					}
					if (itemId == 9100) {
						os.writeC(99);
						os.writeC(13);
						os.writeC(63);
						os.writeC(2);
						os.writeC(97);
						os.writeC(21);
					}
					if (itemId == 9101) {
						os.writeC(100);
						os.writeC(13);
					}
					if (itemId == 9102) {
						os.writeC(50);
						os.writeH(13);
					}
					if (itemId == 9103) {
						os.writeC(100);
						os.writeC(13);
					}
					if (itemId == 134) {
						os.writeC(40);
						os.writeC(14);
					}
					break;
				default:
					if (getEnchantLevel() > 12) {
						if (itemId == 61) {
							os.writeC(100);
							os.writeC(14);
						}
						if (itemId == 9100) {
							os.writeC(99);
							os.writeC(14);
							os.writeC(63);
							os.writeC(2);
							os.writeC(97);
							os.writeC(22);
						}
						if (itemId == 9101) {
							os.writeC(100);
							os.writeC(14);
						}
						if (itemId == 9102) {
							os.writeC(50);
							os.writeH(14);
						}
						if (itemId == 134) {
							os.writeC(40);
							os.writeC(15);
						}
					}
					break;
				}
			}

			if (itemId == 9208) {
				os.writeC(40);
				os.writeC(2);
			}
			if (getItemId() == 20048) {
				switch (getEnchantLevel()) {
				case 12:
				os.writeC(40);
				os.writeC(5);
				break;
			}
			}
			if (itemId >= 9730 && itemId <= 9734) {
				os.writeC(40);
				os.writeC(10);
			}
			if (getItemId() == 500223) { // 쿠거 가더 인첸당 명중
				if (getEnchantLevel() > 4) {
					os.writeC(40);
					os.writeC(getEnchantLevel() - 4);
				}
			}
			if (getItemId() == 9117) {
				switch (getEnchantLevel()) {
				case 8:
					os.writeC(40);
					os.writeC(1);
					break;
				case 9:
					os.writeC(40);
					os.writeC(3);
					break;
				case 10:
					os.writeC(40);
					os.writeC(4);
					break;
				default:
					break;
				}
			}
			if (getItemId() == 91170) {
				switch (getEnchantLevel()) {
				case 8:
					os.writeC(40);
					os.writeC(2);
					break;
				case 9:
					os.writeC(40);
					os.writeC(4);
					break;
				case 10:
					os.writeC(40);
					os.writeC(5);
					break;
				default:
					break;
				}
			}
			if (itemId == 30914 || getItemId() == 30197 || getItemId() == 30201) {
				switch (getEnchantLevel()) {
				case 5:
				case 6:
					os.writeC(40);
					os.writeC(1);
					break;
				case 7:
					os.writeC(40);
					os.writeC(2);
					break;
				case 8:
					os.writeC(40);
					os.writeC(3);
					break;
				case 9:
					os.writeC(40);
					os.writeC(4);
					break;
				case 10:
					os.writeC(40);
					os.writeC(5);
					break;
				default:
					break;
				}
			}

			/** 축복 주문서 3 단계 시스템 by K **/
			if (itemType2 == 1) {
				if (getBlessEnchantLevel() > 0) {
					if (getBless() == 0 || getBless() == 128) {
						switch (getItem().getType()) {
						case 7:
						case 16:
						case 17:
							os.writeC(39);
							os.writeS("축복 스펠 파워 : +" + (1 + (getBlessEnchantLevel() * 2)) + "");
							os.writeC(39);
							os.writeS("축복 마법 적중 : +" + (1 + (getBlessEnchantLevel() * 2)) + "");
							break;
						case 4:
						case 13:
							os.writeC(39);
							os.writeS("축복 원거리 추타 : +" + (1 + (getBlessEnchantLevel() * 2)) + "");// 3/5/7
							os.writeC(39);
							os.writeS("축복 원거리 명중 : +" + (1 + (getBlessEnchantLevel() * 2)) + "");// 3/5/7
							break;
						case 1:
						case 2:
						case 3:
						case 5:
						case 6:
						case 9:
						case 10:
						case 11:
						case 12:
						case 14:
						case 15:
						case 18:
							os.writeC(39);
							os.writeS("축복 근거리 추타 : +" + (1 + (getBlessEnchantLevel() * 2)) + "");// 3/5/7
							os.writeC(39);
							os.writeS("축복 근거리 명중 : +" + (1 + (getBlessEnchantLevel() * 2)) + "");// 3/5/7
							break;
						}
					}
				}
			}

			if (itemType2 == 2) {
				if (getBlessEnchantLevel() > 0) {
					if (!(getItem().getType() >= 8 && getItem().getType() <= 12)) {
						os.writeC(39); // 방어구
						os.writeS("축복 리덕션 : +" + getBlessEnchantLevel() + "");
					}
				}
			}
			/** 여기 까지 **/

			// 보너스 표기
			// if (itemType2 == 1) {
			// if (getBless() == 0 || getBless() == 128) {
			/*
			 * if (getItem().getType() == 7 || getItem().getType() == 16 ||
			 * getItem().getType() == 17) { os.writeC(39);
			 * os.writeS("\\f9축복 보너스 SP:\\aA +1"); os.writeC(115); os.writeD(1); } else {
			 * os.writeC(39); os.writeS("\\f9축복 보너스 대미지:\\aA +1"); os.writeC(115);
			 * os.writeD(1); }
			 */
			if (getItem().getMaterial() == 14 || getItem().getMaterial() == 17 || getItem().getMaterial() == 22) {
				os.writeC(114);
				os.writeD(1);
			}
			/** 무기 속성 타입에 대해서 추뎀 표기 */
			if (getAttrEnchantLevel() != 0) {
				os.writeC(110);
				os.writeC(getAttrEnchantBit(getAttrEnchantLevel()));
			}
			if ((getItemId() >= 277 && getItemId() <= 283) || (getItemId() >= 90085 && getItemId() <= 90092)) {
				os.writeC(59);
				os.writeC(getEnchantLevel());
			} else if (getItemId() >= 4540 && getItemId() <= 4543) {
				os.writeC(59);
				os.writeC(2);
			} else if ((getItemId() >= 21246 && getItemId() <= 21253)) {
				int dmg = 0;
				if (getEnchantLevel() == 7)
					dmg = 1;
				else if (getEnchantLevel() == 8)
					dmg = 2;
				if (dmg != 0) {
					os.writeC(59);
					os.writeC(dmg);
				}
			} else if (getItemId() >= 284 && getItemId() <= 290) {
				os.writeC(59);
				int dmg = 0;
				if (getEnchantLevel() == 7)
					dmg = 3;
				else if (getEnchantLevel() == 8)
					dmg = 5;
				else if (getEnchantLevel() == 9)
					dmg = 7;
				else if (getEnchantLevel() == 10)
					dmg = 10;
				os.writeC(dmg);
			} else if (itemId == 9114 || itemId == 9115 || itemId == 9116 || itemId == 9117 || itemId == 91140
					|| itemId == 91150 || itemId == 91160 || itemId == 91170) {
				if (getEnchantLevel() >= 10) {
					os.writeC(59);
					os.writeC(1);
				}
			} else if (getItem().getGrade() != 3 && itemType2 == 2
					&& (getItem().getType() == 9 || getItem().getType() == 11)) {
				switch (getEnchantLevel()) {
				case 6:
					os.writeC(59);
					os.writeC(getPvPLevel() + 1);
					break;
				case 7:
					os.writeC(59);
					os.writeC(getPvPLevel() + 2);
					break;
				case 8:
					os.writeC(59);
					os.writeC(getPvPLevel() + 3);
					break;
				case 9:
					os.writeC(59);
					os.writeC(getPvPLevel() + 5);
					break;
				case 10:
					os.writeC(59);
					os.writeC(getPvPLevel() + 7);
					break;
				case 11:
					os.writeC(59);
					os.writeC(getPvPLevel() + 9);
					break;
				case 12:
					os.writeC(59);
					os.writeC(getPvPLevel() + 11);
					break;
				default:
					break;
				}
			} else if (getPvPLevel() != 0) {
				os.writeC(59);
				os.writeC(getPvPLevel());
			}
			// PVP 데미지 리덕션
			if (getItemId() == 22002) {
				os.writeC(60);
				os.writeC(2);
			} else if (itemId == 9114 || itemId == 9115 || itemId == 9116 || itemId == 9117 || itemId == 91140
					|| itemId == 91150 || itemId == 91160 || itemId == 91170) {
				if (getEnchantLevel() >= 10) {
					os.writeC(60);
					os.writeC(1);
				}
			} else if (getItem().getGrade() != 3 && itemType2 == 2 && (getItem().getType() == 10)) { // 벨트
				switch (getEnchantLevel()) {
				case 6:
					os.writeC(60);
					os.writeC(1);
					break;
				case 7:
					os.writeC(60);
					os.writeC(3);
					break;
				case 8:
					os.writeC(60);
					os.writeC(5);
					break;
				case 9:
					os.writeC(60);
					os.writeC(7);
					break;
				default:
					break;
				}
			} else if (getItemId() >= 21242 && getItemId() <= 21245) {
				os.writeC(60);
				os.writeC(getEnchantLevel() + 1);
			} else if (getItemId() >= 4640 && getItemId() <= 4643) {
				os.writeC(60);
				os.writeC(2);
			}

			if (getItem().getGrade() != 3 && getItem().getType2() == 2 && (getItem().getType() == 8)
					&& getEnchantLevel() > 0) { // 목걸이 물약회복량
				switch (getEnchantLevel()) {
				case 5:
					os.writeC(65);
					os.writeC(3);
					os.writeC(1);
					os.writeC(96);
					os.writeC(3);
					break;
				case 6:
					os.writeC(65);
					os.writeC(5);
					os.writeC(3);
					os.writeC(96);
					os.writeC(5);
					break;
				case 7:
					os.writeC(65);
					os.writeC(7);
					os.writeC(5);
					os.writeC(96);
					os.writeC(7);
					break;
				case 8:
					os.writeC(65);
					os.writeC(9);
					os.writeC(7);
					os.writeC(96);
					os.writeC(9);
					break;
				case 9:
					os.writeC(65);
					os.writeC(10);
					os.writeC(8);
					os.writeC(96);
					os.writeC(10);
					break;
				default:
					break;
				}
			} else if (getItem().getGrade() != 3 && getItem().getType2() == 2 && (getItem().getType() == 12)
					&& getEnchantLevel() > 0) {
				switch (getEnchantLevel()) {
				case 5:
					os.writeC(65);
					os.writeC(2);
					os.writeC(0);
					os.writeC(96);
					os.writeC(2);
					break;
				case 6:
					os.writeC(65);
					os.writeC(4);
					os.writeC(2);
					os.writeC(96);
					os.writeC(4);
					break;
				case 7:
					os.writeC(65);
					os.writeC(6);
					os.writeC(4);
					os.writeC(96);
					os.writeC(6);
					break;
				case 8:
					os.writeC(65);
					os.writeC(8);
					os.writeC(6);
					os.writeC(96);
					os.writeC(8);
					break;
				case 9:
					os.writeC(65);
					os.writeC(9);
					os.writeC(7);
					os.writeC(96);
					os.writeC(9);
					break;
				default:
					break;
				}
			} else if (itemId == 500008) {
				switch (getEnchantLevel()) {
				case 0:
					os.writeC(65);
					os.writeC(2);
					os.writeC(2);
					os.writeC(96);
					os.writeC(2);
					break;
				case 1:
					os.writeC(65);
					os.writeC(6);
					os.writeC(6);
					os.writeC(96);
					os.writeC(6);
					break;
				case 2:
					os.writeC(65);
					os.writeC(8);
					os.writeC(8);
					os.writeC(96);
					os.writeC(8);
					break;
				case 3:
					os.writeC(65);
					os.writeC(10);
					os.writeC(10);
					os.writeC(96);
					os.writeC(10);
					break;
				case 4:
					os.writeC(65);
					os.writeC(12);
					os.writeC(12);
					os.writeC(96);
					os.writeC(12);
					break;
				case 5:
					os.writeC(65);
					os.writeC(14);
					os.writeC(14);
					os.writeC(96);
					os.writeC(14);
					break;
				case 6:
					os.writeC(65);
					os.writeC(16);
					os.writeC(16);
					os.writeC(96);
					os.writeC(16);
					break;
				case 7:
					os.writeC(65);
					os.writeC(18);
					os.writeC(18);
					os.writeC(96);
					os.writeC(18);
					break;
				case 8:
					os.writeC(65);
					os.writeC(20);
					os.writeC(20);
					os.writeC(96);
					os.writeC(20);
					break;
				default:
					break;
				}
			} else if (itemId == 502008) {
				switch (getEnchantLevel()) {
				case 3:
					os.writeC(65);
					os.writeC(12);
					os.writeC(12);
					os.writeC(96);
					os.writeC(12);
					break;
				case 4:
					os.writeC(65);
					os.writeC(14);
					os.writeC(14);
					os.writeC(96);
					os.writeC(14);
					break;
				case 5:
					os.writeC(65);
					os.writeC(16);
					os.writeC(16);
					os.writeC(96);
					os.writeC(16);
					break;
				case 6:
					os.writeC(65);
					os.writeC(18);
					os.writeC(18);
					os.writeC(96);
					os.writeC(6);
					break;
				case 7:
					os.writeC(65);
					os.writeC(20);
					os.writeC(20);
					os.writeC(96);
					os.writeC(20);
					break;
				case 8:
					os.writeC(65);
					os.writeC(22);
					os.writeC(22);
					os.writeC(96);
					os.writeC(22);
					break;
				default:
					break;
				}
			}

			/** 룸티스 물약회복표시 **/

			if (itemId == 21247 && getEnchantLevel() >= 7) {
				switch (getEnchantLevel()) {
				case 7:
					os.writeC(89);
					os.writeD(1);
					break;
				case 8:
					os.writeC(89);
					os.writeD(3);
					break;
				}
			}

			if (itemId == 21251 && getEnchantLevel() >= 6) {
				switch (getEnchantLevel()) {
				case 6:
					os.writeC(89);
					os.writeD(1);
					break;
				case 7:
					os.writeC(89);
					os.writeD(3);
					break;
				case 8:
					os.writeC(89);
					os.writeD(5);
					break;
				}
			}
			// 피틱엠틱 이자리에서 아래로 이동

			/** 룸티스 데미지감소표시 **/
			if (itemId == 500007 && getEnchantLevel() >= 3) {
				os.writeC(63);
				os.writeC(붉귀리덕());
			}

			if (itemId == 502007 && getEnchantLevel() >= 3) {
				os.writeC(63);
				os.writeC(축붉귀리덕());
			}

			if (itemId == 21248 && getEnchantLevel() >= 7) {
				os.writeC(63);
				os.writeC(체반리덕());
			}

			if (itemId == 21252 && getEnchantLevel() >= 6) {
				os.writeC(63);
				os.writeC(축체반리덕());
			}

			if (getRegistTechnique() != 0) {
				os.writeC(117);
				os.writeC(getRegistTechnique());
			}

			if (getRegistSpirit() != 0) {
				os.writeC(118);
				os.writeC(getRegistSpirit());
			}

			if (getRegistDragonLang() != 0) {
				os.writeC(119);
				os.writeC(getRegistDragonLang());
			}

			if (getRegistFear() != 0) {
				os.writeC(120);
				os.writeC(getRegistFear());
			}

			if (getHitTechnique() != 0) {
				os.writeC(122);
				os.writeC(getHitTechnique());
			}

			if (getHitSpirit() != 0) {
				os.writeC(123);
				os.writeC(getHitSpirit());
			}

			if (getHitDragonLang() != 0) {
				os.writeC(124);
				os.writeC(getHitDragonLang());
			}

			if (getHitFear() != 0) {
				os.writeC(125);
				os.writeC(getHitFear());
			}

			if (getAinBooster() != 0) {
				os.writeC(116);
				os.writeH(getAinBooster());
			}

			/** 룸티스의 푸른빛 귀걸이 **/

			// 무기에 표시부분
			if (itemId == 21097) { // 마법사의 가더
				switch (getEnchantLevel()) {
				case 5:
				case 6:
					os.writeC(17);
					os.writeC(1);
					break;
				case 7:
				case 8:
					os.writeC(17);
					os.writeC(2);
					break;
				default:
					if (getEnchantLevel() >= 9) {
						os.writeC(17);
						os.writeC(3);
					}
					break;
				}
			} else if (getItemId() == 124 || getItemId() == 121 || getItemId() == 100124) {
				switch (getEnchantLevel()) {
				case 7:
					os.writeC(17);
					os.writeC(1);
					break;
				case 8:
					os.writeC(17);
					os.writeC(2);
					break;
				default:
					if (getEnchantLevel() >= 9) {
						os.writeC(17);
						os.writeC(3);
					}
					break;
				}
			} else if (getsp() != 0) {
				if (getItemId() == 134) {
					os.writeC(17);
					os.writeC(getsp() + getEnchantLevel());
				} else if (getItemId() == 9102) {
					os.writeC(17);
					os.writeC(getsp() + getEnchantLevel());

					if (getItemId() == 134) {
						os.writeC(17);
						os.writeC(getsp() + getEnchantLevel());
					} else if (getItemId() == 9102) {
						os.writeC(17);
						os.writeC(getsp() + getEnchantLevel());
					} else {
						os.writeC(17);
						os.writeC(getsp());
					}

				} else {
					os.writeC(17);
					os.writeC(getsp());
				}
			}

			// 스펠파워 지팡이
			// 디락.장신구 업 포함 SP
			// 추타111

			if (getItem().getType2() == 1 && (getItem().getType() == 7 || getItem().getType1() == 17)
					&& getStepEnchantLevel() != 0) {// 검추타주문서이용한...시발
				os.writeC(17);
				os.writeC(getItem().get_addsp() + getStepEnchantLevel());
			} else if (getItemId() == 30914 || getItemId() == 30197 || getItemId() == 30201) {
				switch (getEnchantLevel()) {
				case 6:
					os.writeC(17);
					os.writeC(1);
					break;
				case 7:
					os.writeC(17);
					os.writeC(2);
					break;
				case 8:
					os.writeC(17);
					os.writeC(3);
					break;
				case 9:
					os.writeC(17);
					os.writeC(4);
					break;
				case 10:
					os.writeC(17);
					os.writeC(5);
					break;
				default:
					break;
				}
			} else if (getItemId() == 21246) { // 스냅퍼 지혜 반지
				switch (getEnchantLevel()) {
				case 5:
				case 6:
				case 7:
				case 8:
					os.writeC(17);
					os.writeC(getEnchantLevel() - 4);
					break;
				default:
					break;
				}
			} else if (getItemId() == 21250) { // 스냅퍼 지혜 반지
				switch (getEnchantLevel()) {
				case 4:
					os.writeC(17);
					os.writeC(1);
					break;
				case 5:
					os.writeC(17);
					os.writeC(2);
					break;
				case 6:
					os.writeC(17);
					os.writeC(3);
					break;
				case 7:
				case 8:
					os.writeC(17);
					os.writeC(getEnchantLevel() - 3);
					break;
				default:
					break;
				}
			} else if (itemId == 500009) { // 룸티스 보랏빛
				switch (getEnchantLevel()) {
				case 3:
				case 4:
					os.writeC(17);
					os.writeC(getItem().get_addsp() + 1);
					break;
				case 5:
				case 6:
					os.writeC(17);
					os.writeC(getItem().get_addsp() + 2);
					break;
				case 7:
				case 8:
					os.writeC(17);
					os.writeC(getItem().get_addsp() + 3);
					break;
				}
			} else if (itemId == 502009) { // 룸티스 보랏빛
				switch (getEnchantLevel()) {
				case 3:
					os.writeC(17);
					os.writeC(getItem().get_addsp() + 1);
					break;
				case 4:
				case 5:
					os.writeC(17);
					os.writeC(getItem().get_addsp() + 2);
					break;
				case 6:
				case 7:
					os.writeC(17);
					os.writeC(getItem().get_addsp() + 3);
					break;
				case 8:
					os.writeC(17);
					os.writeC(getItem().get_addsp() + 4);
					break;
				}
			} else if (itemId == 525114) { // 룸티스 보랏빛
				switch (getEnchantLevel()) {
				case 5:
					os.writeC(17);
					os.writeC(1);
					break;
				case 6:
				case 7:
					os.writeC(17);
					os.writeC(2);
					break;
				case 8:
					os.writeC(17);
					os.writeC(3);
					break;
				}
			} else if (itemId == 625114) { // 룸티스 보랏빛
				switch (getEnchantLevel()) {
				case 4:
					os.writeC(17);
					os.writeC(1);
					break;
				case 5:
				case 6:
					os.writeC(17);
					os.writeC(2);
					break;
				case 7:
					os.writeC(17);
					os.writeC(3);
					break;
				case 8:
					os.writeC(17);
					os.writeC(4);
					break;
				}
			} else if (itemId == 9127 && getEnchantLevel() >= 9) {
				os.writeC(17);
				os.writeC(1);
			}

			if (getItem().isHasteItem()) {
				os.writeC(18);
			}

			// 클래스
			// ////////////////////////////////////////////////////////////////////////////////////////////////
			// 디락.장신구업 포함 HP

			if (itemId == 21095) { // 체력의 가더
				os.writeC(14);
				switch (getEnchantLevel()) {
				case 5:
				case 6:
					os.writeH(getItem().get_addhp() + 25);
					break;
				case 7:
				case 8:
					os.writeH(getItem().get_addhp() + 50);
					break;
				default:
					if (getEnchantLevel() >= 9) {
						os.writeH(getItem().get_addhp() + 75);
					} else {
						os.writeH(getItem().get_addhp());
					}
					break;
				}
			} else if (itemId == 9112) {
				os.writeC(14);
				os.writeH(getItem().get_addhp() + (getEnchantLevel() * 5));
			} else if (itemId == 9114 || itemId == 9115 || itemId == 9116 || itemId == 9117 || itemId == 91140
					|| itemId == 91150 || itemId == 91160 || itemId == 91170) {
				if (getEnchantLevel() >= 10) {
					os.writeC(14);
					os.writeH(100);
				}
			} else if (itemId == 30218 || itemId == 21259 || itemId == 21265 || itemId == 21266) {
				os.writeC(14);
				switch (getEnchantLevel()) {
				case 7:
					os.writeH(getItem().get_addhp() + 20);
					break;
				case 8:
					os.writeH(getItem().get_addhp() + 40);
					break;
				default:
					if (getEnchantLevel() >= 9) {
						os.writeH(getItem().get_addhp() + 60);
					} else {
						os.writeH(getItem().get_addhp());
					}
					break;
				}
			} else if (getItem().getGrade() != 3 && itemType2 == 2 && (getItem().getType() == 9
					|| getItem().getType() == 11 || getItem().getType() == 8 || getItem().getType() == 12)) { // 반지~
				switch (getEnchantLevel()) {
				case 0:
					if (getItem().get_addhp() != 0) {
						os.writeC(14);
						os.writeH(getItem().get_addhp());
					}
					break;
				case 1:
					os.writeC(14);
					os.writeH(getItem().get_addhp() + 5);
					break;
				case 2:
					os.writeC(14);
					os.writeH(getItem().get_addhp() + 10);
					break;
				case 3:
					os.writeC(14);
					os.writeH(getItem().get_addhp() + 20);
					break;
				case 4:
					os.writeC(14);
					os.writeH(getItem().get_addhp() + 30);
					break;
				case 5:
					os.writeC(14);
					os.writeH(getItem().get_addhp() + 40);
					break;
				case 6:
					os.writeC(14);
					os.writeH(getItem().get_addhp() + 40);
					break;
				case 7:
					os.writeC(14);
					os.writeH(getItem().get_addhp() + 50);
					break;
				case 8:
					os.writeC(14);
					os.writeH(getItem().get_addhp() + 50);
					break;
				case 9:
					os.writeC(14);
					os.writeH(getItem().get_addhp() + 60);
					break;
				case 10:
					os.writeC(14);
					os.writeH(getItem().get_addhp() + 70);
					break;
				case 11:
					os.writeC(14);
					os.writeH(getItem().get_addhp() + 80);
					break;
				case 12:
					os.writeC(14);
					os.writeH(getItem().get_addhp() + 100);
					break;

				default:
					break;
				}
			} else if (getItem().getGrade() != 3 && itemType2 == 2 && (getItem().getType() == 10)) { // 벨트~
				switch (getEnchantLevel()) {
				case 0:
					if (getItem().get_addhp() != 0) {
						os.writeC(14);
						os.writeH(getItem().get_addhp());
					}
					break;
				case 1:
				case 2:
				case 3:
				case 4:
				case 5:
					os.writeC(14);
					os.writeH(getItem().get_addhp());
					break;
				case 6:
					os.writeC(14);
					os.writeH(getItem().get_addhp() + 20);
					break;
				case 7:
					os.writeC(14);
					os.writeH(getItem().get_addhp() + 30);
					break;
				case 8:
					os.writeC(14);
					os.writeH(getItem().get_addhp() + 40);
					break;
				case 9:
					os.writeC(14);
					os.writeH(getItem().get_addhp() + 50);
					break;
				default:
					break;
				}
			} else if (getItem().getGrade() == 3) {// 순백반지
				if (itemId == 500007) { // 룸티스 붉은빛
					switch (getEnchantLevel()) {
					case 0:
						if (getItem().get_addhp() != 0) {
							os.writeC(14);
							os.writeH(getItem().get_addhp());
						}
						break;
					case 1:
						os.writeC(14);
						os.writeH(getItem().get_addhp() + 20);
						break;
					case 2:
						os.writeC(14);
						os.writeH(getItem().get_addhp() + 30);
						break;
					case 3:
						os.writeC(14);
						os.writeH(getItem().get_addhp() + 40);
						break;
					case 4:
						os.writeC(14);
						os.writeH(getItem().get_addhp() + 50);
						break;
					case 5:
						os.writeC(14);
						os.writeH(getItem().get_addhp() + 60);
						break;
					case 6:
						os.writeC(14);
						os.writeH(getItem().get_addhp() + 70);
						break;
					case 7:
						os.writeC(14);
						os.writeH(getItem().get_addhp() + 80);
						break;
					case 8:
						os.writeC(14);
						os.writeH(getItem().get_addhp() + 90);
						break;
					}
				} else if (itemId == 502007) { // 룸티스 붉은빛
					switch (getEnchantLevel()) {
					case 0:
						if (getItem().get_addhp() != 0) {
							os.writeC(14);
							os.writeH(getItem().get_addhp());
						}
						break;
					case 3:
						os.writeC(14);
						os.writeH(getItem().get_addhp() + 50);
						break;
					case 4:
						os.writeC(14);
						os.writeH(getItem().get_addhp() + 60);
						break;
					case 5:
						os.writeC(14);
						os.writeH(getItem().get_addhp() + 70);
						break;
					case 6:
						os.writeC(14);
						os.writeH(getItem().get_addhp() + 80);
						break;
					case 7:
						os.writeC(14);
						os.writeH(getItem().get_addhp() + 90);
						break;
					case 8:
						os.writeC(14);
						os.writeH(getItem().get_addhp() + 140);
						break;
					}

				} else if ((getItem().getItemId() == 525109 || getItem().getItemId() == 525110
						|| getItem().getItemId() == 525111 || getItem().getItemId() == 525112
						|| getItem().getItemId() == 525113 || getItem().getItemId() == 625109
						|| getItem().getItemId() == 625110 || getItem().getItemId() == 625111
						|| getItem().getItemId() == 625112 || getItem().getItemId() == 625113)) {
					switch (getEnchantLevel()) {
					case 0:
						if (getItem().get_addhp() != 0) {
							os.writeC(14);
							os.writeH(getItem().get_addhp());
						}
						break;//
					case 1:
					case 2:
					case 3:
					case 4:
					case 5:
						os.writeC(14);
						os.writeH(getItem().get_addhp() + (getEnchantLevel() * 5) + 10);
						break;
					case 6:
					case 7:
					case 8:
						if (getItem().getItemId() == 625111) {
							os.writeC(14);
							os.writeH((getItem().get_addhp() + (getEnchantLevel() * 5) + 10)
									+ ((getEnchantLevel() - 5) * 5));
							break;
						} else {
							os.writeC(14);
							os.writeH(getItem().get_addhp() + (getEnchantLevel() * 5) + 10);
							break;
						}
					}

				} else if (getItem().getItemId() == 525115) {
					switch (getEnchantLevel()) {
					case 0:
						if (getItem().get_addhp() != 0) {
							os.writeC(14);
							os.writeH(getItem().get_addhp());
						}
						break;//
					case 3:
					case 4:
					case 5:
					case 6:
					case 7:
					case 8:
						os.writeC(14);
						os.writeH(getItem().get_addhp() + (getEnchantLevel() * 5) - 10);
						break;
					}
				} else if (getItem().getItemId() == 625115) {
					switch (getEnchantLevel()) {
					case 0:
						if (getItem().get_addhp() != 0) {
							os.writeC(14);
							os.writeH(getItem().get_addhp());
						}
						break;//
					case 3:
					case 4:
					case 5:
					case 6:
					case 7:
						os.writeC(14);
						os.writeH(getItem().get_addhp() + (getEnchantLevel() * 5) - 5);
						break;
					case 8:
						os.writeC(14);
						os.writeH(30);
						break;
					}
				} else if (getItem().getItemId() == 525114) {
					switch (getEnchantLevel()) {
					case 0:
						if (getItem().get_addhp() != 0) {
							os.writeC(14);
							os.writeH(getItem().get_addhp());
						}
						break;//
					case 1:
					case 2:
					case 3:
					case 4:
					case 5:
					case 6:
					case 7:
					case 8:
						os.writeC(14);
						os.writeH(getItem().get_addhp() + (getEnchantLevel() * 5));
						break;
					}
				} else if (getItem().getItemId() == 625114) {
					switch (getEnchantLevel()) {
					case 0:
						if (getItem().get_addhp() != 0) {
							os.writeC(14);
							os.writeH(getItem().get_addhp());
						}
						break;//
					case 3:
						os.writeC(14);
						os.writeH(20);
						break;
					case 1:
					case 2:
						os.writeC(14);
						os.writeH(getItem().get_addhp() + (getEnchantLevel() * 5));
						break;
					case 4:
					case 5:
					case 6:
					case 7:
						os.writeC(14);
						os.writeH(getItem().get_addhp() + (getEnchantLevel() * 5) + 5);
						break;
					case 8:
						os.writeC(14);
						os.writeH(getItem().get_addhp() + (getEnchantLevel() * 5) + 10);
						break;
					}
				} else if ((getItem().getItemId() >= 425109 && getItem().getItemId() <= 425113)
						|| (getItem().getItemId() >= 525109 && getItem().getItemId() <= 525113)
						|| (getItem().getItemId() >= 625109 && getItem().getItemId() <= 625113) || getItemId() == 21247
						|| getItemId() == 21248 || getItemId() == 21251 || getItemId() == 21252) {
					switch (getEnchantLevel()) {
					case 0:
						if (getItem().get_addhp() != 0) {
							os.writeC(14);
							os.writeH(getItem().get_addhp());
						}
						break;//
					case 1:
						os.writeC(14);
						os.writeH(getItem().get_addhp() + 15);
						break;
					case 2:
						os.writeC(14);
						os.writeH(getItem().get_addhp() + 20);
						break;
					case 3:
						os.writeC(14);
						os.writeH(getItem().get_addhp() + 25);
						break;
					case 4:
						os.writeC(14);
						os.writeH(getItem().get_addhp() + 30);
						break;
					case 5:
						os.writeC(14);
						os.writeH(getItem().get_addhp() + 35);
						break;
					case 6:
						os.writeC(14);
						os.writeH(getItem().get_addhp() + 40 + (getItemId() == 21252 ? 5 : 0));
						break;
					case 7:
						os.writeC(14);
						os.writeH(getItem().get_addhp() + 45 + (getItemId() == 21252 ? 10 : 0));
						break;
					case 8:
						os.writeC(14);
						os.writeH(getItem().get_addhp() + 50 + (getItemId() == 21252 ? 15 : 0));
						break;
					}
				} else if (getItemId() == 21246) { // 스냅퍼 // 지혜
					if (getEnchantLevel() > 0) {
						os.writeC(14);
						os.writeH(getEnchantLevel() * 5);
					}
				} else if (getItemId() == 21250) { // 축 스냅퍼 // 지혜
					switch (getEnchantLevel()) {
					case 1:
						os.writeC(14);
						os.writeH(5);
						break;
					case 2:
						os.writeC(14);
						os.writeH(10);
						break;
					case 3:
						os.writeC(14);
						os.writeH(20);
						break;
					case 4:
						os.writeC(14);
						os.writeH(25);
						break;
					case 5:
						os.writeC(14);
						os.writeH(30);
						break;
					case 6:
						os.writeC(14);
						os.writeH(35);
						break;
					case 7:
						os.writeC(14);
						os.writeH(40);
						break;
					case 8:
						os.writeC(14);
						os.writeH(50);
						break;
					}
				} else if (getItemId() == 21249 || getItemId() == 21253) { // 스냅퍼
																			// //
																			// 용사
					if (getEnchantLevel() >= 3) {
						os.writeC(14);
						os.writeH((getEnchantLevel() - 2) * 5);
					}
				}
				/**/
			} else if (getItem().get_addhp() != 0) {
				os.writeC(14);
				os.writeH(getItem().get_addhp());
			}

			if (getItem().get_addhpr() != 0) {
				os.writeC(37);
				os.writeC(getItem().get_addhpr());
			}

			// 디락.장신구업 포함 MP
			if (getItem().getGrade() != 3 && itemType2 == 2 && (getItem().getType() == 10)) { // 벨트
																								// ~
				switch (getEnchantLevel()) {
				case 1:
					os.writeC(32);
					os.writeH(getItem().get_addmp() + 5);
					break;
				case 2:
					os.writeC(32);
					os.writeH(getItem().get_addmp() + 10);
					break;
				case 3:
					os.writeC(32);
					os.writeH(getItem().get_addmp() + 20);
					break;
				case 4:
					os.writeC(32);
					os.writeH(getItem().get_addmp() + 30);
					break;
				case 5:
					os.writeC(32);
					os.writeH(getItem().get_addmp() + 40);
					break;
				case 6:
					os.writeC(32);
					os.writeH(getItem().get_addmp() + 40);
					break;
				case 7:
					os.writeC(32);
					os.writeH(getItem().get_addmp() + 50);
					break;
				case 8:
					os.writeC(32);
					os.writeH(getItem().get_addmp() + 50);
					break;
				case 9:
					os.writeC(32);
					os.writeH(getItem().get_addmp() + 60);
					break;
				default:
					break;
				}
			} else if (getItem().getItemId() == 500009) {// 보라빛
				switch (getEnchantLevel()) {
				case 0:
					if (getItem().get_addmp() != 0) {
						os.writeC(32);
						os.writeH(getItem().get_addmp());
					}
					break;
				case 1:
					os.writeC(32);
					os.writeH(getItem().get_addmp() + 10);
					break;
				case 2:
					os.writeC(32);
					os.writeH(getItem().get_addmp() + 15);
					break;
				case 3:
					os.writeC(32);
					os.writeH(getItem().get_addmp() + 30);
					break;
				case 4:
					os.writeC(32);
					os.writeH(getItem().get_addmp() + 35);
					break;
				case 5:
					os.writeC(32);
					os.writeH(getItem().get_addmp() + 50);
					break;
				case 6:
					os.writeC(32);
					os.writeH(getItem().get_addmp() + 55);
					break;
				case 7:
					os.writeC(32);
					os.writeH(getItem().get_addmp() + 70);
					break;
				case 8:
					os.writeC(32);
					os.writeH(getItem().get_addmp() + 95);
					break;
				}
			} else if (getItem().getItemId() == 502009) {// 보라빛
				switch (getEnchantLevel()) {
				case 1:
					os.writeC(32);
					os.writeH(getItem().get_addmp() + 10);
					break;
				case 2:
					os.writeC(32);
					os.writeH(getItem().get_addmp() + 15);
					break;
				case 3:
					os.writeC(32);
					os.writeH(getItem().get_addmp() + 35);
					break;
				case 4:
					os.writeC(32);
					os.writeH(getItem().get_addmp() + 50);
					break;
				case 5:
					os.writeC(32);
					os.writeH(getItem().get_addmp() + 55);
					break;
				case 6:
					os.writeC(32);
					os.writeH(getItem().get_addmp() + 70);
					break;
				case 7:
					os.writeC(32);
					os.writeH(getItem().get_addmp() + 95);
					break;
				case 8:
					os.writeC(32);
					os.writeH(getItem().get_addmp() + 125);
					break;
				}
			} else if (getItemId() == 21246) {
				os.writeC(32);
				if (getEnchantLevel() >= 8) {
					os.writeH(30);
				} else {
					os.writeH(getItem().get_addmp());
				}
			} else if (getItemId() == 21250) {// 스냅퍼의 지혜 축복
				os.writeC(32);
				if (getEnchantLevel() >= 7) {
					if (getEnchantLevel() == 7) {
						os.writeH(30);
					} else {
						os.writeH(35);
					}
				} else {
					os.writeH(getItem().get_addmp());
				}
			} else if (getItem().get_addmp() != 0) {
				os.writeC(32);
				if (getItemId() == 21166)
					os.writeH(getItem().get_addmp() + (getEnchantLevel() * 10));
				else
					os.writeH(getItem().get_addmp());
			}

			if (getItem().get_addmpr() != 0) {
				os.writeC(38);
				int mprr = getItem().get_addmpr();
				if (getItem().getItemId() == 261)
					mprr += getEnchantLevel();
				os.writeC(mprr);
			}

			if (itemId == 121216 || itemId == 421216) {
				os.writeC(36);
				os.writeC(2);
			}
			if (itemId == 31913) {
				switch (getEnchantLevel()) {
				case 1:
					os.writeC(36);
					os.writeC(1);
					break;
				case 2:
					os.writeC(36);
					os.writeC(2);
					break;
				case 3:
					os.writeC(36);
					os.writeC(3);
					break;
				case 4:
					os.writeC(36);
					os.writeC(4);
					break;
				case 5:
					os.writeC(36);
					os.writeC(5);
					break;
				case 6:
					os.writeC(36);
					os.writeC(6);
					break;
				case 7:
					os.writeC(36);
					os.writeC(7);
					break;
				case 8:
					os.writeC(36);
					os.writeC(8);
					break;
				default:
					break;
				}
			}

			if (itemId == 421217 || itemId == 421218 || itemId == 421219) {
				os.writeC(36);
				os.writeC(10);
			}

			if (itemId == 621217) {
				os.writeC(36);
				os.writeC(40);
			}
			if (itemId == 2222384 || itemId == 2222385 || itemId == 2222386) {
				os.writeC(36);
				os.writeC(200);
			}

			if (itemId == 431219) {
				os.writeC(40); // 마법 적중
				os.writeC(2);
				os.writeC(36);
				os.writeC(5);
			}

			if (itemId == 221216 || itemId == 321216) {
				os.writeC(36);
				os.writeC(5);
			}

			if (itemId == 9205) {
				os.writeC(64);
				os.writeC(getEnchantLevel());
				os.writeC(10);
			}

			if (itemId == 21093) {
				os.writeC(64);
				os.writeC(getEnchantLevel() * 2);
				os.writeC(50);
			}
			if (itemId == 420112 || itemId == 420113 || itemId == 420114 || itemId == 420115) {
				switch (getEnchantLevel()) {
				case 0:
				case 1:
				case 2:
				case 3:
				case 4:
				case 5:
				case 6:
					os.writeC(97);
					os.writeC(3);
					break;
				case 7:
					os.writeC(97);
					os.writeC(4);
					break;
				case 8:
					os.writeC(97);
					os.writeC(5);
					break;
				case 9:
					os.writeC(97);
					os.writeC(6);
					break;
				default:
					if (getEnchantLevel() > 9) {
						os.writeC(97);
						os.writeC(6);
					}
					break;
				}
			}

			if (itemId >= 9900 && itemId <= 9974) { // 80렙룬
				os.writeC(135);
				os.writeD(1);
			} else if (itemId >= 9800 && itemId <= 9874) { // 85렙룬
				os.writeC(135);
				os.writeD(2);
			} else if (itemId >= 9700 && itemId <= 9774) { // 90렙룬
				os.writeC(135);
				os.writeD(3);
			}

			if (itemId == 420104 || itemId == 420105 || itemId == 420106 || itemId == 420107) {
				switch (getEnchantLevel()) {
				case 25:
				os.writeC(73);
				os.writeS("HP +100, MP +50");
				os.writeC(74);
				os.writeS("데미지 감소: 13%");
				break;
				default:
					break;
				}
			}
			if (itemId == 420108 || itemId == 420109 || itemId == 420110 || itemId == 420111) {
				os.writeC(74);
				os.writeS("마력의 린드비오르의 가호");
			}
			if (itemId == 420112 || itemId == 420113) {
				os.writeC(74);
				os.writeS("발라카스의 가호: 근거리");
			}
			if (itemId == 420114) {
				os.writeC(74);
				os.writeS("발라카스의 가호: 원거리");
			}
			if (itemId == 420115) {
				os.writeC(74);
				os.writeS("발라카스의 가호: 마법");
			}
			if (itemId == 9204) {
				os.writeC(74);
				os.writeS("페어리의 가호");
			}
			if (itemId == 391034) {
				os.writeC(36);
				os.writeC(10);
			}
			if (itemId == 391035) {
				os.writeC(63);
				os.writeC(2);
				os.writeC(15); 
				os.writeC(10);
				os.writeC(14); 
				os.writeC(50); 
				os.writeC(37); 
				os.writeC(2);
				
			}
			if (itemId == 391036) {
				os.writeC(47);
				os.writeC(3);
				os.writeC(35);
				os.writeC(3);
				os.writeC(17);
				os.writeC(3);
				os.writeC(38);
				os.writeC(2);
			}
			if (itemId == 10110) {
				os.writeC(36);
				os.writeC(20);
			}
			if (itemId == 9114) {
				os.writeC(36);
				int expBonus = 0;
				switch (getEnchantLevel()) {
				case 8:
					expBonus = 2;
					break;
				case 9:
					expBonus = 4;
					break;
				case 10:
					expBonus = 6;
					break;
				default:
					break;
				}
				os.writeC(expBonus);
			}
			if (itemId == 91140) {
				os.writeC(36);
				int expBonus = 0;
				switch (getEnchantLevel()) {
				case 7:
					expBonus = 2;
					break;
				case 8:
					expBonus = 4;
					break;
				case 9:
					expBonus = 6;
					break;
				case 10:
					expBonus = 8;
					break;
				default:
					break;
				}
				os.writeC(expBonus);
			}
			if (itemId == 30910 || itemId == 30195 || itemId == 30196 || itemId == 30197 || itemId == 30198) {
				os.writeC(36);
				int expBonus = 0;
				switch (getEnchantLevel()) {
				case 0:
				case 1:
				case 2:
				case 3:
				case 4:
				case 5:
				case 6:
					expBonus = getEnchantLevel() + 1;
					break;
				case 7:
					expBonus = getEnchantLevel() + 2;
					break;
				case 8:
					expBonus = getEnchantLevel() + 3;
					break;
				case 9:
					expBonus = getEnchantLevel() + 4;
					break;
				case 10:
					expBonus = getEnchantLevel() + 5;
					break;
				}
				os.writeC(expBonus);
			}

			if (itemId == 30910 || itemId == 30912 || itemId == 30913 || itemId == 30914 || itemId == 30195
					|| itemId == 30196 || itemId == 30197) {
				switch (getEnchantLevel()) {
				case 0:
					break;
				case 1:
					os.writeC(96);
					os.writeC(2);
					break;
				case 2:
					os.writeC(96);
					os.writeC(4);
					break;
				case 3:
					os.writeC(96);
					os.writeC(6);
					break;
				case 4:
					os.writeC(96);
					os.writeC(8);
					break;
				case 5:
					os.writeC(96);
					os.writeC(9);
					break;
				case 6:
					os.writeC(96);
					os.writeC(10);
					break;
				case 7:
					os.writeC(96);
					os.writeC(11);
					break;
				case 8:
					os.writeC(96);
					os.writeC(12);
					break;
				case 9:
					os.writeC(96);
					os.writeC(13);
					break;
				case 10:
					os.writeC(96);
					os.writeC(14);
					break;
				}
			}
			if (itemId == 30911 || itemId == 30199 || itemId == 30200 || itemId == 30201 || itemId == 30198) {
				os.writeC(96);
				os.writeC(getEnchantLevel() * 2 + 2);
			}
			if (itemId == 30910 || itemId == 30912 || itemId == 30913 || itemId == 30914 || itemId == 30195
					|| itemId == 30196 || itemId == 30197) {
				switch (getEnchantLevel()) {
				case 0:
					break;
				case 1:
					os.writeC(65);
					os.writeC(2);
					os.writeC(2);
					break;
				case 2:
					os.writeC(65);
					os.writeC(4);
					os.writeC(4);
					break;
				case 3:
					os.writeC(65);
					os.writeC(6);
					os.writeC(6);
					break;
				case 4:
					os.writeC(65);
					os.writeC(8);
					os.writeC(8);
					break;
				case 5:
					os.writeC(65);
					os.writeC(9);
					os.writeC(9);
					break;
				case 6:
					os.writeC(65);
					os.writeC(10);
					os.writeC(10);
					break;
				case 7:
					os.writeC(65);
					os.writeC(11);
					os.writeC(11);
					break;
				case 8:
					os.writeC(65);
					os.writeC(12);
					os.writeC(12);
					break;
				case 9:
					os.writeC(65);
					os.writeC(13);
					os.writeC(13);
					break;
				case 10:
					os.writeC(65);
					os.writeC(14);
					os.writeC(14);
					break;
				}
			}
			if (itemId == 30911 || itemId == 30199 || itemId == 30200 || itemId == 30201 || itemId == 30198) {
				os.writeC(65);
				os.writeC(getEnchantLevel() * 2 + 2);
				os.writeC(getEnchantLevel() * 2 + 2);
			}
			if (itemId == 31910 || itemId == 31911 || itemId == 31912 || itemId == 32910 || itemId == 32911
					|| itemId == 32912) {
				switch (getEnchantLevel()) {
				case 0:
					break;
				case 1:
					os.writeC(96);
					os.writeC(2);
					os.writeC(65);
					os.writeC(2);
					os.writeC(2);
					break;
				case 2:
					os.writeC(96);
					os.writeC(4);
					os.writeC(65);
					os.writeC(4);
					os.writeC(4);
					break;
				case 3:
					os.writeC(96);
					os.writeC(8);
					os.writeC(65);
					os.writeC(8);
					os.writeC(8);
					break;
				case 4:
					os.writeC(96);
					os.writeC(10);
					os.writeC(65);
					os.writeC(10);
					os.writeC(10);
					break;
				case 5:
					os.writeC(96);
					os.writeC(12);
					os.writeC(65);
					os.writeC(12);
					os.writeC(12);
					break;
				case 6:
					os.writeC(96);
					os.writeC(14);
					os.writeC(65);
					os.writeC(14);
					os.writeC(14);
					break;
				case 7:
					os.writeC(96);
					os.writeC(16);
					os.writeC(65);
					os.writeC(16);
					os.writeC(16);
					break;
				case 8:
					os.writeC(96);
					os.writeC(18);
					os.writeC(65);
					os.writeC(18);
					os.writeC(18);
					break;
				default:
					break;
				}
			}
			// 수정

			// MR
			if (getItemId() == 9114 || getItemId() == 91140) {
				if (getEnchantLevel() == 5) {
					os.writeC(15);
					os.writeH(4);
				} else if (getEnchantLevel() == 6) {
					os.writeC(15);
					os.writeH(5);
				} else if (getEnchantLevel() == 7) {
					os.writeC(15);
					os.writeH(6);
				} else if (getEnchantLevel() == 8) {
					os.writeC(15);
					os.writeH(8);
				} else if (getEnchantLevel() == 9) {
					os.writeC(15);
					os.writeH(11);
				} else if (getEnchantLevel() >= 10) {
					os.writeC(15);
					os.writeH(14);
				}
			} else if (getItemId() == 10110) {
				switch (getEnchantLevel()) {
				case 7:
					os.writeC(15);
					os.writeH(2);
					break;
				case 8:
					os.writeC(15);
					os.writeH(3);
					break;
				case 9:
					os.writeC(15);
					os.writeH(4);
					break;
				default:
					break;
				}
			} else if (getItemId() == 31913) {
				if (getEnchantLevel() >= 4) {
					os.writeC(15);
					os.writeH(getEnchantLevel() - 3);
				}
			} else if (itemId == 32910 || itemId == 32911 || itemId == 32912) {
				switch (getEnchantLevel()) {
				case 5:
					os.writeC(15);
					os.writeH(4);
					break;
				case 6:
					os.writeC(15);
					os.writeH(6);
					break;
				case 7:
					os.writeC(15);
					os.writeH(8);
					break;
				case 8:
					os.writeC(15);
					os.writeH(10);
					break;
				default:
					break;
				}
			} else if (getItemId() == 9202 || getItemId() == 9203) { // 신성한 엘름,
																		// 흑기사
																		// 면갑
				switch (getEnchantLevel()) {
				case 5:
					os.writeC(15);
					os.writeH(4);
					break;
				case 6:
					os.writeC(15);
					os.writeH(8);
					break;
				case 7:
					os.writeC(15);
					os.writeH(12);
					break;
				case 8:
					os.writeC(15);
					os.writeH(16);
					break;
				case 9:
					os.writeC(15);
					os.writeH(20);
					break;
				default:
					break;
				}
			} else if (getItem().getGrade() != 3 && getItem().getType2() == 2 && (getItem().getType() == 8)
					&& getEnchantLevel() > 0) { // 목걸이 마방
				switch (getEnchantLevel()) {
				case 5:
					os.writeC(15);
					os.writeH(getMr() + 1);
					break;
				case 6:
					os.writeC(15);
					os.writeH(getMr() + 3);
					break;
				case 7:
					os.writeC(15);
					os.writeH(getMr() + 5);
					break;
				case 8:
					os.writeC(15);
					os.writeH(getMr() + 7);
					break;
				case 9:
					os.writeC(15);
					os.writeH(getMr() + 10);
					break;
				default:
					break;
				}
			} else if (getItem().getGrade() != 3 && itemType2 == 2
					&& (getItem().getType() == 9 || getItem().getType() == 11)) {
				switch (getEnchantLevel()) {
				case 5:
					os.writeC(15);
					os.writeH(getMr());
					break;
				case 6:
					os.writeC(15);
					os.writeH(getMr() + 1);
					break;
				case 7:
					os.writeC(15);
					os.writeH(getMr() + 3);
					break;
				case 8:
					os.writeC(15);
					os.writeH(getMr() + 5);
					break;
				case 9:
					os.writeC(15);
					os.writeH(getMr() + 7);
					break;
				case 10:
					os.writeC(15);
					os.writeH(getMr() + 9);
					break;
				case 11:
					os.writeC(15);
					os.writeH(getMr() + 11);
					break;
				case 12:
					os.writeC(15);
					os.writeH(getMr() + 13);
					break;
				default:
					if (getMr() != 0) {
						os.writeC(15);
						os.writeH(getMr());
					}
					break;
				}
			} else if (getMr() != 0) {
				os.writeC(15);
				os.writeH(getMr());
			}

			// 디락.장신구 포함 속방표시
			if (getItem().get_defense_fire() != 0) {
				os.writeC(27);
				os.writeC(getItem().get_defense_fire());
			}
			if (getItem().get_defense_water() != 0) {
				os.writeC(28);
				os.writeC(getItem().get_defense_water());
			}
			if (getItem().get_defense_wind() != 0) {
				os.writeC(29);
				os.writeC(getItem().get_defense_wind());
			}
			if (getItem().get_defense_earth() != 0) {
				os.writeC(30);
				os.writeC(getItem().get_defense_earth());
			}

			/**
			 * getMr() 추가.디락. 마방과 중복표시 되지 않음 아이템 표시부분 오류 때문에
			 */
			if (getItemId() == 4800) {
				os.writeC(36);
				os.writeC(1);
			} else if (getItemId() == 4801) {
				os.writeC(36);
				os.writeC(2);
			} else if (getItemId() == 4802) {
				os.writeC(36);
				os.writeC(3);
			} else if (getItemId() == 4803) {
				os.writeC(36);
				os.writeC(4);
			} else if (getItemId() == 4804) {
				os.writeC(36);
				os.writeC(5);
			} else if (getItemId() == 4805) {
				os.writeC(36);
				os.writeC(6);
			} else if (getItemId() == 4806) {
				os.writeC(36);
				os.writeC(7);
			} else if (getItemId() == 4807) {
				os.writeC(36);
				os.writeC(8);
			} else if (getItemId() == 4808) {
				os.writeC(36);
				os.writeC(10);
			}
			if (getRegistLevel() != 0 && itemId >= 490000 && itemId <= 490017) {
				os.writeC(39);
				os.writeS(spirit());
			}

			if (getRegistLevel() == 10) {// 판도라 정령문양
				os.writeC(27);
				os.writeC(10);
				os.writeC(28);
				os.writeC(10);
				os.writeC(29);
				os.writeC(10);
				os.writeC(30);
				os.writeC(10);
			} else if (getRegistLevel() == 11) {// 판도라 마나문양
				os.writeC(32);
				os.writeH(30);
			} else if (getRegistLevel() == 12) {// 판도라 체력문양
				os.writeC(14);
				os.writeH(30);
			} else if (getRegistLevel() == 13) {// 판도라 멸마문양
				os.writeC(15);
				os.writeH(10);
			} else if (getRegistLevel() == 15) {// 판도라 회복문양
				os.writeC(37);
				os.writeC(1);
				os.writeC(38);
				os.writeC(1);
			} else if (getRegistLevel() == 16) {// 판도라 석화문양
				os.writeC(33);
				os.writeC(2);
				os.writeC(10);
			} else if (getRegistLevel() == 17) {// 판도라 홀드문양
				os.writeC(33);
				os.writeC(6);
				os.writeC(10);
			} else if (getRegistLevel() == 18) {// 판도라 스턴문양
				os.writeC(33);
				os.writeC(5);
				os.writeC(10);
			}

			if (itemId == 9306) { // 마법 명중
				os.writeC(40);
				os.writeC(1);
			} else if (itemId == 4810) {
				os.writeC(40);
				os.writeC(2);
			} else if (itemId == 421216) {
				os.writeC(40);
				os.writeC(2);
			} else if (itemId == 421219) {
				os.writeC(40);
				os.writeC(6);
			} else if (itemId == 22112) {
				if (getEnchantLevel() >= 5) {
					os.writeC(40);
					os.writeC(getEnchantLevel() - 4);
				}
			} else if (itemId == 21246) {
				switch (getEnchantLevel()) {
				case 7:
					os.writeC(40);
					os.writeC(1);
					break;
				case 8:
					os.writeC(40);
					os.writeC(2);
					break;
				}
			} else if (itemId == 21250) {
				switch (getEnchantLevel()) {
				case 6:
					os.writeC(40);
					os.writeC(1);
					break;
				case 7:
					os.writeC(40);
					os.writeC(2);
					break;
				case 8:
					os.writeC(40);
					os.writeC(3);
					break;
				}
			} else if (itemId == 500009) {
				switch (getEnchantLevel()) {
				case 7:
					os.writeC(40);
					os.writeC(1);
					break;
				case 8:
					os.writeC(40);
					os.writeC(3);
					break;
				}
			} else if (itemId == 502009) {
				switch (getEnchantLevel()) {
				case 6:
					os.writeC(40);
					os.writeC(1);
					break;
				case 7:
					os.writeC(40);
					os.writeC(3);
					break;
				case 8:
					os.writeC(40);
					os.writeC(5);
					break;
				}
			}
			/*
			 * 마법 명중 os.writeC(40); os.writeC(value);
			 */
			if (getItem().getMinLevel() > 0) {
				os.writeC(42);
				os.writeC(getItem().getMinLevel());
			}
			// 45/45 순결한 체력꽃향 요정족 티

			// PVP 추가 데미지

			/**
			 * if ((itemId >= 90085 && itemId <= 90092) || itemId == 160423 || itemId ==
			 * 435000 || itemId == 160510 || itemId == 160511 || itemId == 21123) {
			 * os.writeC(61); os.writeD(3442346400L); }
			 **/
			if (itemId == 160423 || itemId == 435000 || itemId == 160510 || itemId == 160511 || itemId == 21123) {
				os.writeC(61);
				os.writeD(3442346400L);
			}
			if (itemId == 21269 && getEnchantLevel() < 6) {
				os.writeC(61);
				os.writeD(3442346400L);
			}

			if (itemId >= 9075 && itemId <= 9093) {
				os.writeC(61);
				long dd = 3374546400L;
				Calendar cal = (Calendar) Calendar.getInstance().clone();
				cal.setTimeInMillis(dd);
				os.writeD(Config.test + 3374546400L);
			}

			if (itemId == 21096) {// 수호의 가더
				if (수호의가더DamageDown() > 0) {
					os.writeC(63);
					os.writeC(수호의가더DamageDown());
				}
			} else if (itemId == 30218 || itemId == 21259 || itemId == 21265 || itemId == 21266) {
				if (getEnchantLevel() >= 9) {
					os.writeC(63);
					os.writeC(1);
				}
			} else if (getItemId() == 9303) {
				switch (getEnchantLevel()) {
				case 5:
					os.writeC(63);
					os.writeC(1);
					break;
				case 6:
					os.writeC(63);
					os.writeC(2);
					os.writeC(15);
					os.writeH(3);
					break;
				case 7:
					os.writeC(63);
					os.writeC(3);
					os.writeC(15);
					os.writeH(5);
					break;
				case 8:
					os.writeC(63);
					os.writeC(4);
					os.writeC(15);
					os.writeH(7);
					break;
				default:
					if (getEnchantLevel() > 8) {
						os.writeC(63);
						os.writeC(4);
						os.writeC(15);
						os.writeH(7);
					}
					break;
				}
			} else if (getItem().getGrade() != 3 && itemType2 == 2 && (getItem().getType() == 10)) { // 벨트
																										// ~
				os.writeC(63);
				os.writeC(벨트데미지감소());
			} else if (getItem().getDamageReduction() != 0) {
				int reduc = getItem().getDamageReduction();
				if (itemId == 9114 || itemId == 91140) {
					if (getEnchantLevel() >= 9) {
						reduc++;
					}
				}
				if (itemId >= 420100 && itemId <= 420103) {
					if (getEnchantLevel() >= 7) {
						reduc++;
					}
					if (getEnchantLevel() >= 8) {
						reduc++;
					}
					if (getEnchantLevel() >= 9) {
						reduc++;
					}
				}
				if (itemId == 22113) {
					if (getEnchantLevel() >= 7) {
						reduc += getEnchantLevel() - 6;
					}
				}
				if (itemId == 9113) {
					if (getEnchantLevel() >= 6) {
						reduc += getEnchantLevel() - 5;
					}
				}
				os.writeC(63);
				os.writeC(reduc);
			}

			if (itemId == 502007) {
				switch (getEnchantLevel()) {
				case 4:
					os.writeC(64);
					os.writeC(2);
					os.writeC(20);
					break;
				case 5:
					os.writeC(64);
					os.writeC(3);
					os.writeC(20);
					break;
				case 6:
					os.writeC(64);
					os.writeC(4);
					os.writeC(20);
					break;
				case 7:
					os.writeC(64);
					os.writeC(5);
					os.writeC(20);
					break;
				case 8:
					os.writeC(64);
					os.writeC(6);
					os.writeC(20);
					break;
				}
			} else if (itemId == 500007) {
				switch (getEnchantLevel()) {
				case 5:
					os.writeC(64);
					os.writeC(2);
					os.writeC(20);
					break;
				case 6:
					os.writeC(64);
					os.writeC(3);
					os.writeC(20);
					break;
				case 7:
					os.writeC(64);
					os.writeC(4);
					os.writeC(20);
					break;
				case 8:
					os.writeC(64);
					os.writeC(5);
					os.writeC(20);
					break;
				}
			} else if (itemId == 21248) {
				switch (getEnchantLevel()) {
				case 7:
					os.writeC(64);
					os.writeC(1);
					os.writeC(20);
					break;
				case 8:
					os.writeC(64);
					os.writeC(2);
					os.writeC(20);
					break;
				}
			} else if (itemId == 21252) {
				switch (getEnchantLevel()) {
				case 6:
					os.writeC(64);
					os.writeC(1);
					os.writeC(20);
					break;
				case 7:
					os.writeC(64);
					os.writeC(2);
					os.writeC(20);
					break;
				case 8:
					os.writeC(64);
					os.writeC(3);
					os.writeC(20);
					break;
				}
			}

			if (getItem().getWeightReduction() != 0) {
				// os.writeC(68);
				int reduc = getItem().getWeightReduction();
				if (itemId == 7246) {
					if (getEnchantLevel() > 5) {
						int en = getEnchantLevel() - 5;
						reduc += en * 60;
					}
				}

				os.writeC(0x5a);
				os.writeH(reduc);
			}

			if (getItem().getItemId() == 20298 // 제니스의반지
					|| getItem().getItemId() == 120298 // 축제니스반지
					|| getItem().getItemId() == 20117 // 바포메트의갑옷
					|| getItem().getItemId() == 420100 || getItem().getItemId() == 420101
					|| getItem().getItemId() == 420102 || getItem().getItemId() == 420103) {
				os.writeC(70);
				os.writeC(2);// 독내성
			}

			if (getItem().getMagicName() != null) {
				if (getItemId() == 293 || getItemId() == 7227 || getItemId() == 9106 || getItemId() == 90084
						|| getItemId() == 66) {
					if (getEnchantLevel() > 9) {
						os.writeC(74);
						os.writeS(getItem().getMagicName());
					}
				} else {
					os.writeC(74);
					os.writeS(getItem().getMagicName());
				}
			}

			if (getItem().get_canbedmg() != 1) {
				os.writeC(131);
				os.writeD(1);
			}

			if (getItem().getMinLevel() != 0) {
				os.writeC(111);
				os.writeD(getItem().getMinLevel());
			}

			if (getItem().isTradable()) {
				os.writeC(130);
				os.writeD(1);
			}
		}
		return os.getBytes();
	}

	public static final int[] hextable = { 0x80, 0x81, 0x82, 0x83, 0x84, 0x85, 0x86, 0x87, 0x88, 0x89, 0x8a, 0x8b, 0x8c,
			0x8d, 0x8e, 0x8f, 0x90, 0x91, 0x92, 0x93, 0x94, 0x95, 0x96, 0x97, 0x98, 0x99, 0x9a, 0x9b, 0x9c, 0x9d, 0x9e,
			0x9f, 0xa0, 0xa1, 0xa2, 0xa3, 0xa4, 0xa5, 0xa6, 0xa7, 0xa8, 0xa9, 0xaa, 0xab, 0xac, 0xad, 0xae, 0xaf, 0xb0,
			0xb1, 0xb2, 0xb3, 0xb4, 0xb5, 0xb6, 0xb7, 0xb8, 0xb9, 0xba, 0xbb, 0xbc, 0xbd, 0xbe, 0xbf, 0xc0, 0xc1, 0xc2,
			0xc3, 0xc4, 0xc5, 0xc6, 0xc7, 0xc8, 0xc9, 0xca, 0xcb, 0xcc, 0xcd, 0xce, 0xcf, 0xd0, 0xd1, 0xd2, 0xd3, 0xd4,
			0xd5, 0xd6, 0xd7, 0xd8, 0xd9, 0xda, 0xdb, 0xdc, 0xdd, 0xde, 0xdf, 0xe0, 0xe1, 0xe2, 0xe3, 0xe4, 0xe5, 0xe6,
			0xe7, 0xe8, 0xe9, 0xea, 0xeb, 0xec, 0xed, 0xee, 0xef, 0xf0, 0xf1, 0xf2, 0xf3, 0xf4, 0xf5, 0xf6, 0xf7, 0xf8,
			0xf9, 0xfa, 0xfb, 0xfc, 0xfd, 0xfe, 0xff };

	private int 벨트데미지감소() {
		// TODO 자동 생성된 메소드 스텁
		int lvl = getEnchantLevel();
		int reduc = 0;
		switch (lvl) {
		case 5:
			reduc = getItem().getDamageReduction() + 1;
			break;
		case 6:
			reduc = getItem().getDamageReduction() + 2;
			break;
		case 7:
			reduc = getItem().getDamageReduction() + 3;
			break;
		case 8:
			reduc = getItem().getDamageReduction() + 4;
			break;
		case 9:
			reduc = getItem().getDamageReduction() + 5;
			break;
		default:
			if (lvl >= 10) {
				reduc = getItem().getDamageReduction() + 5;
			} else {
				reduc = getItem().getDamageReduction();
			}

			break;
		}
		return reduc;
	}

	private int 수호의가더DamageDown() {
		// TODO 자동 생성된 메소드 스텁
		int lvl = getEnchantLevel();
		int reduc = 0;
		switch (lvl) {
		case 5:
		case 6:
			reduc = 2;
			break;
		case 7:
		case 8:
			reduc = 3;
			break;
		default:
			if (lvl >= 9) {
				reduc = 4;
			} else {
				reduc = 1;
			}
			break;
		}
		return reduc;
	}

	private static final int _hit = 0x05;
	private static final int _dmg = 0x06;
	private static final int _bowhit = 0x18;
	private static final int _bowdmg = 0x23;
	private static final int _str = 0x08;
	private static final int _dex = 0x09;
	private static final int _con = 0x0a;
	private static final int _wis = 0x0b;
	private static final int _int = 0x0c;
	private static final int _cha = 0x0d;

	private static final int _mr = 0x0f;
	private static final int _sp = 0x11;

	private static final int _fire = 0x1B;
	private static final int _water = 0x1C;
	private static final int _wind = 0x1D;
	private static final int _earth = 0x1E;

	private static final int _maxhp = 0x0e;
	private static final int _maxmp = 0x20;
	private static final int _hpr = 0x25;
	private static final int _mpr = 0x26;
	private static final int _add_ac = 0x38;
	private static final int _poly = 0x47;

	public byte[] getStatusBytes(L1PcInstance pc, boolean check) {
		byte[] data = getStatusBytes();
		@SuppressWarnings("resource")
		BinaryOutputStream os = new BinaryOutputStream();
		try {
			os.write(data);

			os.writeC(0x45);

			if (check) {
				os.writeC(1);
			} else {
				os.writeC(2);
			}
			L1ArmorSets set = ArmorSetTable.getInstance().getArmorSets(getItem().getSetId());

			if (set.getAc() != 0) {
				os.writeC(_add_ac);
				os.writeC(set.getAc());
			}

			if (getItem().getItemId() == 20099) {
				os.writeC(_poly);
				os.writeH(1175);// 데몬
			} else if (getItem().getItemId() == 20150) {
				os.writeC(_poly);
				os.writeH(274);// 커츠
				os.writeC(63);
				os.writeC(2);
			} else if (getItem().getItemId() == 20100) {
				os.writeC(_poly);
				os.writeH(18692);// 진데스
			} else if (getItem().getItemId() == 20151) {
				os.writeC(_poly);
				os.writeH(2118);// 케레니스
			} else if (getItem().getItemId() == 20118) {
				os.writeC(_poly);
				os.writeH(2117);// 켄라우헬
			}

			if (set.getShortHitup() != 0) {
				os.writeC(_hit);
				os.writeC(set.getShortHitup());
			}
			if (set.getShortDmgup() != 0) {
				os.writeC(_dmg);
				os.writeC(set.getShortDmgup());
			}

			if (set.getLongHitup() != 0) {
				os.writeC(_bowhit);
				os.writeC(set.getLongHitup());
			}
			if (set.getLongDmgup() != 0) {
				os.writeC(_bowdmg);
				os.writeC(set.getLongDmgup());
			}

			if (set.getHpr() != 0) {
				os.writeC(_hpr);
				os.writeC(set.getHpr());
			}
			if (set.getMpr() != 0) {
				os.writeC(_mpr);
				os.writeC(set.getMpr());
			}

			if (set.getHp() != 0) {
				os.writeC(_maxhp);
				os.writeH(set.getHp());
			}
			if (set.getMp() != 0) {
				os.writeC(_maxmp);
				os.writeH(set.getMp());
			}

			if (set.getMr() != 0) {
				os.writeC(_mr);
				os.writeH(set.getMr());
			}

			if (set.getSp() != 0) {
				os.writeC(_sp);
				os.writeC(set.getSp());
			}

			if (set.getfire() != 0) {
				os.writeC(_fire);
				os.writeC(set.getfire());
			}
			if (set.getwater() != 0) {
				os.writeC(_water);
				os.writeC(set.getwater());
			}
			if (set.getwind() != 0) {
				os.writeC(_wind);
				os.writeC(set.getwind());
			}
			if (set.getearth() != 0) {
				os.writeC(_earth);
				os.writeC(set.getearth());
			}

			if (set.getStr() != 0) {
				os.writeC(_str);
				os.writeC(set.getStr());
			}
			if (set.getDex() != 0) {
				os.writeC(_dex);
				os.writeC(set.getDex());
			}
			if (set.getCon() != 0) {
				os.writeC(_con);
				os.writeC(set.getCon());
			}
			if (set.getWis() != 0) {
				os.writeC(_wis);
				os.writeC(set.getWis());
			}
			if (set.getIntl() != 0) {
				os.writeC(_int);
				os.writeC(set.getIntl());
			}
			if (set.getCha() != 0) {
				os.writeC(_cha);
				os.writeC(set.getCha());
			}
			os.writeC(0x45);
			os.writeC(0);

			if (getItem().getType2() == 2) {
				os.writeC(0);
				os.writeC(-1);
			} else {
				os.writeC(0);
				os.writeC(0);
			}

		} catch (Exception e) {
		}
		return os.getBytes();
	}

	public byte[] getStatusBytes(L1PcInstance pc) {
		byte[] data = getStatusBytes();
		@SuppressWarnings("resource")
		BinaryOutputStream os = new BinaryOutputStream();
		try {
			os.write(data);

			L1ArmorSets set = ArmorSetTable.getInstance().getArmorSets(getItem().getSetId());

			if (set != null && getItem().getMainId() == getItem().getItemId()) {
				os.writeC(0x45);
				os.writeC(2);
				if (set.getAc() != 0) {
					os.writeC(_add_ac);
					os.writeC(set.getAc());
				}
				if (getItem().getItemId() == 20099) {
					os.writeC(_poly);
					os.writeH(1175);// 데몬
				} else if (getItem().getItemId() == 20150) {
					os.writeC(_poly);
					os.writeH(274);// 커츠
					os.writeC(63);
					os.writeC(2);
				} else if (getItem().getItemId() == 20100) {
					os.writeC(_poly);
					os.writeH(18692);// 진데스
				} else if (getItem().getItemId() == 20151) {
					os.writeC(_poly);
					os.writeH(2118);// 케레니스
				} else if (getItem().getItemId() == 20118) {
					os.writeC(_poly);
					os.writeH(2117);// 켄라우헬
				}

				if (set.getShortHitup() != 0) {
					os.writeC(_hit);
					os.writeC(set.getShortHitup());
				}
				if (set.getShortDmgup() != 0) {
					os.writeC(_dmg);
					os.writeC(set.getShortDmgup());
				}

				if (set.getLongHitup() != 0) {
					os.writeC(_bowhit);
					os.writeC(set.getLongHitup());
				}
				if (set.getLongDmgup() != 0) {
					os.writeC(_bowdmg);
					os.writeC(set.getLongDmgup());
				}

				if (set.getHpr() != 0) {
					os.writeC(_hpr);
					os.writeC(set.getHpr());
				}
				if (set.getMpr() != 0) {
					os.writeC(_mpr);
					os.writeC(set.getMpr());
				}

				if (set.getHp() != 0) {
					os.writeC(_maxhp);
					os.writeH(set.getHp());
				}
				if (set.getMp() != 0) {
					os.writeC(_maxmp);
					os.writeH(set.getMp());
				}

				if (set.getMr() != 0) {
					os.writeC(_mr);
					os.writeH(set.getMr());
				}

				if (set.getSp() != 0) {
					os.writeC(_sp);
					os.writeC(set.getSp());
				}

				if (set.getfire() != 0) {
					os.writeC(_fire);
					os.writeC(set.getfire());
				}
				if (set.getwater() != 0) {
					os.writeC(_water);
					os.writeC(set.getwater());
				}
				if (set.getwind() != 0) {
					os.writeC(_wind);
					os.writeC(set.getwind());
				}
				if (set.getearth() != 0) {
					os.writeC(_earth);
					os.writeC(set.getearth());
				}

				if (set.getStr() != 0) {
					os.writeC(_str);
					os.writeC(set.getStr());
				}
				if (set.getDex() != 0) {
					os.writeC(_dex);
					os.writeC(set.getDex());
				}
				if (set.getCon() != 0) {
					os.writeC(_con);
					os.writeC(set.getCon());
				}
				if (set.getWis() != 0) {
					os.writeC(_wis);
					os.writeC(set.getWis());
				}
				if (set.getIntl() != 0) {
					os.writeC(_int);
					os.writeC(set.getIntl());
				}
				if (set.getCha() != 0) {
					os.writeC(_cha);
					os.writeC(set.getCha());
				}
				os.writeC(0x45);
				os.writeC(0);
			}

			if (getItem().getType2() == 2) {
				os.writeC(0);
				os.writeC(-1);
			} else {
				os.writeC(0);
				os.writeC(0);
			}

		} catch (Exception e) {
		}
		return os.getBytes();
	}

	private String spirit() {
		int lvl = getRegistLevel();
		String in = "";
		switch (lvl) {
		case 1:
			in = "정령의 인(I)";
			break;
		case 2:
			in = "정령의 인(II)";
			break;
		case 3:
			in = "정령의 인(III)";
			break;
		case 4:
			in = "정령의 인(IV)";
			break;
		case 5:
			in = "정령의 인(V)";
			break;
		default:
			break;
		}
		return in;
	}

	public EnchantTimer getSkill() {
		return _timer;
	}

	public void getSkillExit() {
		_timer.cancel();
		_isRunning = false;
		_timer = null;
		setAcByMagic(0);
		setDmgByMagic(0);
		setHolyDmgByMagic(0);
		setHitByMagic(0);
	}

	public class EnchantTimer implements Runnable {

		private int skillId = 0;
		private int time = 0;
		private boolean cancel = false;

		public EnchantTimer() {
		}

		public EnchantTimer(int skillid, int _time) {
			skillId = skillid;
			time = _time;
		}

		@Override
		public void run() {
			try {
				if (cancel)
					return;
				time--;
				if (time > 0) {
					GeneralThreadPool.getInstance().schedule(this, 1000);
					return;
				}

				int type = getItem().getType();
				int type2 = getItem().getType2();
				int itemId = getItem().getItemId();
				if (_pc != null && _pc.getInventory().checkItem(itemId)) {
					if (type == 2 && type2 == 2 && isEquipped()) {
						_pc.getAC().addAc(3);
						_pc.sendPackets(new S_OwnCharStatus(_pc), true);
						/*
						 * switch (skillId) { case L1SkillId.BLESSED_ARMOR: if (_pc != null &&
						 * _pc.getInventory().checkItem(getItemId()) && isEquipped()) {
						 * _pc.sendPackets(new S_PacketBox(S_PacketBox.SKILL_WEAPON_ICON, 748, 0, false,
						 * false)); } break; default: break; }
						 */
					}
				}
				// _pc.sendPackets(new S_ServerMessage(308, getLogName()));
				/*
				 * switch (skillId) { case L1SkillId.ENCHANT_WEAPON: if (_pc != null &&
				 * _pc.getInventory().checkItem(itemId) && isEquipped()) { _pc.sendPackets(new
				 * S_PacketBox(S_PacketBox.SKILL_WEAPON_ICON, 747, 0, _isSecond, false)); }
				 * break; case L1SkillId.SHADOW_FANG: if (_pc != null &&
				 * _pc.getInventory().checkItem(itemId) && isEquipped()) { _pc.sendPackets(new
				 * S_PacketBox(S_PacketBox.SKILL_WEAPON_ICON, 2951, 0, false, false)); } break;
				 * default: break; }
				 */
				setAcByMagic(0);
				setDmgByMagic(0);
				setHolyDmgByMagic(0);
				setHitByMagic(0);
				_isRunning = false;
				_timer = null;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		public int getSkillId() {
			return skillId;
		}

		public int getTime() {
			return time;
		}

		public void cancel() {
			cancel = true;
		}

	}

	private int _acByMagic = 0;
	private int _hitByMagic = 0;
	private int _holyDmgByMagic = 0;
	private int _dmgByMagic = 0;

	public int getAcByMagic() {
		return _acByMagic;
	}

	public void setAcByMagic(int i) {
		_acByMagic = i;
	}

	public int getDmgByMagic() {
		return _dmgByMagic;
	}

	public void setDmgByMagic(int i) {
		_dmgByMagic = i;
	}

	public int getHolyDmgByMagic() {
		return _holyDmgByMagic;
	}

	public void setHolyDmgByMagic(int i) {
		_holyDmgByMagic = i;
	}

	public int getHitByMagic() {
		return _hitByMagic;
	}

	public void setHitByMagic(int i) {
		_hitByMagic = i;
	}

	public void setSkillArmorEnchant(L1PcInstance pc, int skillId, int skillTime) {
		int type = getItem().getType();
		int type2 = getItem().getType2();
		if (_isRunning) {
			_timer.cancel();
			int itemId = getItem().getItemId();
			if (pc != null && pc.getInventory().checkItem(itemId)) {
				if (type == 2 && type2 == 2 && isEquipped()) {
					pc.getAC().addAc(3);
					pc.sendPackets(new S_OwnCharStatus(pc), true);
				}
			}
			setAcByMagic(0);
			_isRunning = false;
			_timer = null;
			/*
			 * switch (skillId) { case L1SkillId.BLESSED_ARMOR: if (pc != null &&
			 * pc.getInventory().checkItem(getItemId()) && isEquipped()) {
			 * pc.sendPackets(new S_PacketBox(S_PacketBox.SKILL_WEAPON_ICON, 748, 0, false,
			 * false)); } break; default: break; }
			 */
		}
		if (type == 2 && type2 == 2 && isEquipped()) {
			pc.getAC().addAc(-3);
			pc.sendPackets(new S_OwnCharStatus(pc));
			/*
			 * switch (skillId) { case L1SkillId.BLESSED_ARMOR: if (pc != null &&
			 * pc.getInventory().checkItem(getItem().getItemId()) && isEquipped()) {
			 * pc.sendPackets(new S_PacketBox(S_PacketBox.SKILL_WEAPON_ICON, 748, skillTime
			 * / 1000, false, false)); } break; default: break; }
			 */
		}
		setAcByMagic(3);
		_pc = pc;
		_timer = new EnchantTimer(skillId, skillTime / 1000);
		GeneralThreadPool.getInstance().schedule(_timer, 1000);
		// (new Timer()).schedule(_timer, skillTime);
		_isRunning = true;
	}

	public void setSkillWeaponEnchant(L1PcInstance pc, int skillId, int skillTime) {
		if (getItem().getType2() != 1) {
			return;
		}
		if (_isRunning) {
			_timer.cancel();
			setDmgByMagic(0);
			setHolyDmgByMagic(0);
			setHitByMagic(0);
			_isRunning = false;
			_timer = null;
			/*
			 * switch (skillId) { case L1SkillId.ENCHANT_WEAPON: if (pc != null &&
			 * pc.getInventory().checkItem(getItemId()) && isEquipped()) {
			 * pc.sendPackets(new S_PacketBox(S_PacketBox.SKILL_WEAPON_ICON, 747, 0,
			 * _isSecond, false)); } break; case L1SkillId.BLESS_WEAPON: if (pc != null &&
			 * pc.getInventory().checkItem(getItemId()) && isEquipped()) {
			 * pc.sendPackets(new S_PacketBox(S_PacketBox.SKILL_WEAPON_ICON, 2176, 0,
			 * _isSecond, false)); } break; case L1SkillId.SHADOW_FANG: if (pc != null &&
			 * pc.getInventory().checkItem(getItemId()) && isEquipped()) {
			 * pc.sendPackets(new S_PacketBox(S_PacketBox.SKILL_WEAPON_ICON, 2951, 0, false,
			 * false)); } break; default: break; }
			 */
		}

		/*
		 * switch (skillId) { case L1SkillId.HOLY_WEAPON: setHolyDmgByMagic(1);
		 * setHitByMagic(1); break;
		 * 
		 * case L1SkillId.ENCHANT_WEAPON: if (pc != null &&
		 * pc.getInventory().checkItem(getItem().getItemId()) && isEquipped()) {
		 * pc.sendPackets(new S_PacketBox(S_PacketBox.SKILL_WEAPON_ICON, 747, skillTime
		 * / 1000, _isSecond, false)); } setDmgByMagic(2); break;
		 * 
		 * case L1SkillId.BLESS_WEAPON: if (pc != null &&
		 * pc.getInventory().checkItem(getItem().getItemId()) && isEquipped()) {
		 * pc.sendPackets( new S_PacketBox(S_PacketBox.SKILL_WEAPON_ICON, 2176,
		 * skillTime / 1000, _isSecond, false)); } setDmgByMagic(2); setHitByMagic(2);
		 * break;
		 * 
		 * case L1SkillId.SHADOW_FANG: if (pc != null &&
		 * pc.getInventory().checkItem(getItem().getItemId()) && isEquipped()) {
		 * pc.sendPackets(new S_PacketBox(S_PacketBox.SKILL_WEAPON_ICON, 2951, skillTime
		 * / 1000, false, false)); } setDmgByMagic(5); break;
		 * 
		 * default: break; }
		 */

		_pc = pc;
		_timer = new EnchantTimer(skillId, skillTime / 1000);
		GeneralThreadPool.getInstance().schedule(_timer, 1000);
		// (new Timer()).schedule(_timer, skillTime);
		_isRunning = true;
	}

	private int 붉귀리덕() {
		int lvl = getEnchantLevel();
		int i = 0;
		switch (lvl) {
		case 3:
			i = 1;
			break;
		case 4:
			i = 1;
			break;
		case 5:
			i = 2;
			break;
		case 6:
			i = 3;
			break;
		case 7:
			i = 4;
			break;
		case 8:
			i = 5;
			break;
		default:
			break;
		}
		return i;
	}

	private int 축붉귀리덕() {
		int lvl = getEnchantLevel();
		int i = 0;
		switch (lvl) {
		case 3:
			i = 1;
			break;
		case 4:
			i = 2;
			break;
		case 5:
			i = 3;
			break;
		case 6:
			i = 4;
			break;
		case 7:
			i = 5;
			break;
		case 8:
			i = 6;
			break;
		default:
			break;
		}
		return i;
	}

	private int 체반리덕() {
		int lvl = getEnchantLevel();
		int i = 0;
		switch (lvl) {
		case 7:
			i = 1;
			break;
		case 8:
			i = 2;
			break;
		default:
			break;
		}
		return i;
	}

	private int 축체반리덕() {
		int lvl = getEnchantLevel();
		int i = 0;
		switch (lvl) {
		case 6:
			i = 1;
			break;
		case 7:
			i = 2;
			break;
		case 8:
			i = 3;
			break;
		default:
			break;
		}
		return i;
	}

	// ** 룸티스 악세 정보 추가부분 **/
	public void startItemOwnerTimer(L1PcInstance pc) {
		setItemOwner(pc);
		L1ItemOwnerTimer timer = new L1ItemOwnerTimer(this, 10000);
		timer.begin();
	}

	private L1EquipmentTimer _equipmentTimer;

	public void startEquipmentTimer(L1PcInstance pc) {
		if (getRemainingTime() > 0) {
			_equipmentTimer = new L1EquipmentTimer(pc, this);
			Timer timer = new Timer(true);
			timer.scheduleAtFixedRate(_equipmentTimer, 1000, 1000);
		}
	}

	public void stopEquipmentTimer() {
		try {
			if (getRemainingTime() > 0 && _equipmentTimer != null) {
				_equipmentTimer.cancel();
				_equipmentTimer = null;
			}
		} catch (Exception e) {
		}
	}

	private L1PcInstance _itemOwner;

	public L1PcInstance getItemOwner() {
		return _itemOwner;
	}

	public void setItemOwner(L1PcInstance pc) {
		_itemOwner = pc;
	}

	private boolean _isNowLighting = false;

	public boolean isNowLighting() {
		return _isNowLighting;
	}

	public void setNowLighting(boolean flag) {
		_isNowLighting = flag;
	}

	private int _secondId;

	public int getSecondId() {
		return _secondId;
	}

	public void setSecondId(int i) {
		_secondId = i;
	}

	private int _roundId;

	public int getRoundId() {
		return _roundId;
	}

	public void setRoundId(int i) {
		_roundId = i;
	}

	private int _ticketId = -1; // 티겟 번호

	public int getTicketId() {
		return _ticketId;
	}

	public void setTicketId(int i) {
		_ticketId = i;
	}

	private int _DropMobId = 0;

	public int isDropMobId() {
		return _DropMobId;
	}

	public void setDropMobId(int i) {
		_DropMobId = i;
	}

	private boolean _isWorking = false;

	public boolean isWorking() {
		return _isWorking;
	}

	public void setWorking(boolean flag) {
		_isWorking = flag;
	}

	// 아이템을 분당체크해서 삭제하기 위해서 추가!!
	private int _deleteItemTime = 0;

	public int get_DeleteItemTime() {
		return _deleteItemTime;
	}

	public void add_DeleteItemTime() {
		_deleteItemTime++;
	}

	public void init_DeleteItemTime() {
		_deleteItemTime = 0;
	}

	public int getAttr() {
		int attr = 0;
		switch (getAttrEnchantLevel()) {
		// 화령 1~5
		case 1:
			attr = 17;
			break;
		case 2:
			attr = 33;
			break;
		case 3:
			attr = 49;
			break;
		case 4:
			attr = 65;
			break;
		case 5:
			attr = 81;
			break;
		// 수령 1~5
		case 6:
			attr = 18;
			break;
		case 7:
			attr = 34;
			break;
		case 8:
			attr = 50;
			break;
		case 9:
			attr = 66;
			break;
		case 10:
			attr = 82;
			break;
		// 풍령 1~5
		case 11:
			attr = 19;
			break;
		case 12:
			attr = 35;
			break;
		case 13:
			attr = 51;
			break;
		case 14:
			attr = 67;
			break;
		case 15:
			attr = 83;
			break;
		// 지령 1~5
		case 16:
			attr = 20;
			break;
		case 17:
			attr = 36;
			break;
		case 18:
			attr = 52;
			break;
		case 19:
			attr = 68;
			break;
		case 20:
			attr = 84;
			break;
		}
		return attr;
	}

	public int getAttrEnchantBit(int attr) {
		int attr_bit = 0;
		int result_bit = 0;

		// 1/2/3/33/34 attr_bit = 1;
		// 4/5/6/35/36 attr_bit = 2;
		// 7/8/9/37/38 attr_bit = 3;
		// 10/11/12/39/40 attr_bit = 4;

		switch (attr) {
		case 1:
			attr_bit = 1;
			attr = 1;
			break;
		case 2:
			attr_bit = 1;
			attr = 2;
			break;
		case 3:
			attr_bit = 1;
			attr = 3;
			break;
		case 4:
			attr_bit = 1;
			attr = 4;
			break;
		case 5:
			attr_bit = 1;
			attr = 5;
			break;
		case 6:
			attr_bit = 2;
			attr = 1;
			break;
		case 7:
			attr_bit = 2;
			attr = 2;
		case 8:
			attr_bit = 2;
			attr = 3;
			break;
		case 9:
			attr_bit = 2;
			attr = 4;
			break;
		case 10:
			attr_bit = 2;
			attr = 5;
			break;
		case 11:
			attr_bit = 3;
			attr = 1;
			break;
		case 12:
			attr_bit = 3;
			attr = 2;
			break;
		case 13:
			attr_bit = 3;
			attr = 3;
			break;
		case 14:
			attr_bit = 3;
			attr = 4;
			break;
		case 15:
			attr_bit = 3;
			attr = 5;
			break;
		case 16:
			attr_bit = 4;
			attr = 1;
			break;
		case 17:
			attr_bit = 4;
			attr = 2;
			break;
		case 18:
			attr_bit = 4;
			attr = 3;
			break;
		case 19:
			attr_bit = 4;
			attr = 4;
		case 20:
			attr_bit = 4;
			attr = 5;
			break;
		default:
			break;
		}
		if (attr > 0) {
			result_bit = attr_bit + (16 * attr);
		}
		return result_bit;
	}

	public int getRegistTechnique() { // 기술내성
		int itemType = getItem().getType();
		int itemType2 = getItem().getType2();
		int result = getItem().getRegistTechnique();

		if (getItem().getGrade() != 3 && itemType2 == 2 && (itemType == 8 || itemType == 12)) {
			switch (getEnchantLevel()) {
			case 7:
				result += 2;
				break;
			case 8:
				result += 3;
				break;
			case 9:
				result += 4;
				break;
			default:
				if (getEnchantLevel() > 9) {
					result += 4;
				}
				break;
			}
		}
		if (getItemId() >= 21246 && getItemId() <= 21253) {
			switch (getEnchantLevel()) {
			case 6:
				result += 5;
				break;
			case 7:
				result += 7;
				break;
			case 8:
				result += 9;
				break;
			default:
				if (getEnchantLevel() > 8) {
					result += 9;
				}
				break;
			}
		}

		if (getItemId() >= 22110 && getItemId() <= 22113 || getItemId() == 9113) {
			if (getEnchantLevel() > 5) {
				int result2 = getEnchantLevel() - 5;
				if (result2 > 5) {
					result2 = 5;
				}
				result += result2;
			}
		}
		if (getItemId() == 10100 || getItemId() == 10102 || getItemId() == 10103) {
			if (getEnchantLevel() > 4) {
				result = getEnchantLevel() - 4;
				if (result > 5) {
					result = 5;
				}
			}
		}
		if (getItemId() == 9115 || getItemId() == 9116 || getItemId() == 9117 || getItemId() == 91150
				|| getItemId() == 91160 || getItemId() == 91170) {
			switch (getEnchantLevel()) {
			case 5:
				result = 8;
				break;
			case 6:
				result = 9;
				break;
			case 7:
				result = 10;
				break;
			case 8:
				result = 12;
				break;
			case 9:
				result = 15;
				break;
			case 10:
				result = 18;
				break;
			default:
				if (getEnchantLevel() > 10) {
					result = 18;
				}
				break;
			}
		}
		return result;
	}

	public int getRegistSpirit() { // 정령내성
		int result = getItem().getRegistSpirit();

		if (getItemId() == 21166 || getItemId() == 20049 || getItemId() == 20050 || getItemId() == 22009) {
			if (getEnchantLevel() > 4) {
				int result2 = getEnchantLevel() - 4;
				if (result2 > 7) {
					result2 = 7;
				}
				result += result2;
			}
		}
		if (getItemId() >= 22110 && getItemId() <= 22113 || getItemId() == 9113) {
			if (getEnchantLevel() > 5) {
				int result2 = getEnchantLevel() - 5;
				if (result2 > 5) {
					result2 = 5;
				}
				result += result2;
			}
		}
		if (getItemId() == 10100 || getItemId() == 10102 || getItemId() == 10103) {
			if (getEnchantLevel() > 4) {
				result = getEnchantLevel() - 4;
				if (result > 5) {
					result = 5;
				}
			}
		}
		if (getItemId() == 91150 || getItemId() == 91160 || getItemId() == 91170) {
			switch (getEnchantLevel()) {
			case 7:
				result = 1;
				break;
			case 8:
				result = 2;
				break;
			case 9:
				result = 4;
				break;
			case 10:
				result = 6;
				break;
			default:
				if (getEnchantLevel() > 10) {
					result = 6;
				}
				break;
			}
		}
		return result;
	}

	public int getRegistDragonLang() { // 용언내성
		int result = getItem().getRegistDragonLang();

		if (getItemId() == 20235) {
			if (getEnchantLevel() > 4) {
				int result2 = getEnchantLevel() - 4;
				if (result2 > 7) {
					result2 = 7;
				}
				result += result2;
			}
		}
		if (getItemId() >= 420100 && getItemId() <= 420115) {
			if (getEnchantLevel() > 4) {
				int result2 = getEnchantLevel() - 4;
				if (result2 > 7) {
					result2 = 7;
				}
				result += result2;
			}
		}
		if (getItemId() >= 22110 && getItemId() <= 22113 || getItemId() == 9113) {
			if (getEnchantLevel() > 5) {
				int result2 = getEnchantLevel() - 5;
				if (result2 > 5) {
					result2 = 5;
				}
				result += result2;
			}
		}
		if (getItemId() == 10100 || getItemId() == 10102 || getItemId() == 10103) {
			if (getEnchantLevel() > 4) {
				result = getEnchantLevel() - 4;
				if (result > 5) {
					result = 5;
				}
			}
		}
		if (getItemId() == 91150 || getItemId() == 91160 || getItemId() == 91170) {
			switch (getEnchantLevel()) {
			case 7:
				result = 1;
				break;
			case 8:
				result = 2;
				break;
			case 9:
				result = 4;
				break;
			case 10:
				result = 6;
				break;
			default:
				if (getEnchantLevel() > 10) {
					result = 6;
				}
				break;
			}
		}
		return result;
	}

	public int getRegistFear() { // 공포내성
		int result = getItem().getRegistFear();

		if (getItemId() == 20079) {
			if (getEnchantLevel() > 4) {
				int result2 = getEnchantLevel() - 4;
				if (result2 > 7) {
					result2 = 7;
				}
				result += result2;
			}
		}
		if (getItemId() >= 22110 && getItemId() <= 22113 || getItemId() == 9113) {
			if (getEnchantLevel() > 5) {
				int result2 = getEnchantLevel() - 5;
				if (result2 > 5) {
					result2 = 5;
				}
				result += result2;
			}
		}

		if (getItemId() == 10100 || getItemId() == 10102 || getItemId() == 10103) {
			if (getEnchantLevel() > 4) {
				result = getEnchantLevel() - 4;
				if (result > 5) {
					result = 5;
				}
			}
		}
		if (getItemId() == 91150 || getItemId() == 91160 || getItemId() == 91170) {
			switch (getEnchantLevel()) {
			case 7:
				result = 1;
				break;
			case 8:
				result = 2;
				break;
			case 9:
				result = 4;
				break;
			case 10:
				result = 6;
				break;
			default:
				if (getEnchantLevel() > 10) {
					result = 6;
				}
				break;
			}
		}
		return result;
	}

	public int getHitTechnique() { // 기술적중
		int result = getItem().getTechniqueHit();
		if (getItemId() == 61) {
			result += getEnchantLevel();
		}
		return result;
	}

	public int getHitSpirit() { // 정령적중
		int result = getItem().getSpiritHit();

		if (getItemId() == 9100 || getItemId() == 86) {
			result += getEnchantLevel();
		}
		if (getItemId() == 292) {
			if (getEnchantLevel() == 8) {
				result += 1;
			} else if (getEnchantLevel() == 9) {
				result += 2;
			} else if (getEnchantLevel() >= 10) {
				result += 3;
			}

		}
		return result;
	}

	public int getHitDragonLang() { // 용언적중
		int result = getItem().getDragonLangHit();

		if (getItemId() == 9101 || getItemId() == 9102) {
			result += getEnchantLevel();
		}

		if (getItemId() == 90084 || getItemId() == 6101) {
			if (getEnchantLevel() > 6) {
				int result2 = getEnchantLevel() - 6;
				if (result2 > 4) {
					result2 = 4;
				}
				result += result2;
			}
		}
		return result;
	}

	public int getHitFear() { // 공포적중
		int result = getItem().getFearHit();

		if (getItemId() == 9103) {
			result += getEnchantLevel();
		}

		if (getItemId() == 7227) {
			if (getEnchantLevel() > 7) {
				int result2 = getEnchantLevel() - 7;
				if (result2 > 3) {
					result2 = 3;
				}
				result += result2;
			}
		}

		return result;
	}

	public int getAinBooster() { // 축복소모
		int result = getItem().getAinBooster();
		if (getItemId() == 32910 || getItemId() == 32911 || getItemId() == 32912) {
			switch (getEnchantLevel()) {
			case 5:
				result += 3;
				break;
			case 6:
				result += 5;
				break;
			case 7:
				result += 7;
				break;
			case 8:
				result += 10;
				break;
			default:
				break;
			}
		}
		return result;
	}

	public int getTradeBit() {
		int b = 2;
		if (this.getBless() >= 128)
			b = 3;
		else if (!this.getItem().isTradable())
			b = 3;
		else if (this.getItem().isTradable())
			b = 7;

		return b;
	}

	public int getStatusBit() {
		int b = 0;
		if (!this.getItem().isTradable())
			b += 2;
		if (this.getItem().isCantDelete())
			b += 4;
		if (this.getItem().get_safeenchant() < 0)
			b += 8;

		int bless = this.getBless();
		if (bless >= 128) {
			b = 46;
		}

		if (this.isIdentified())
			b++;

		return b;
	}
}

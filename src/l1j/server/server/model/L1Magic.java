/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */
package l1j.server.server.model;

import static l1j.server.server.model.skill.L1SkillId.ABSOLUTE_BARRIER;
import static l1j.server.server.model.skill.L1SkillId.AM_BREAK;
import static l1j.server.server.model.skill.L1SkillId.AREA_OF_SILENCE;
import static l1j.server.server.model.skill.L1SkillId.ARMOR_BREAK;
import static l1j.server.server.model.skill.L1SkillId.BIRTH_MAAN;
import static l1j.server.server.model.skill.L1SkillId.BONE_BREAK;
import static l1j.server.server.model.skill.L1SkillId.CANCELLATION;
import static l1j.server.server.model.skill.L1SkillId.CONFUSION;
import static l1j.server.server.model.skill.L1SkillId.COUNTER_BARRIER;
import static l1j.server.server.model.skill.L1SkillId.MAJESTY;
import static l1j.server.server.model.skill.L1SkillId.인페르노;
import static l1j.server.server.model.skill.L1SkillId.CURSE_BLIND;
import static l1j.server.server.model.skill.L1SkillId.CURSE_PARALYZE;
import static l1j.server.server.model.skill.L1SkillId.CURSE_POISON;
import static l1j.server.server.model.skill.L1SkillId.DARKNESS;
import static l1j.server.server.model.skill.L1SkillId.DECAY_POTION;
import static l1j.server.server.model.skill.L1SkillId.DISEASE;
import static l1j.server.server.model.skill.L1SkillId.DISINTEGRATE;
import static l1j.server.server.model.skill.L1SkillId.DRAGON_SKIN;
import static l1j.server.server.model.skill.L1SkillId.EARTH_BIND;
import static l1j.server.server.model.skill.L1SkillId.ELEMENTAL_FALL_DOWN;
import static l1j.server.server.model.skill.L1SkillId.ERASE_MAGIC;
import static l1j.server.server.model.skill.L1SkillId.FAFU_MAAN;
import static l1j.server.server.model.skill.L1SkillId.FEAR;
import static l1j.server.server.model.skill.L1SkillId.FEATHER_BUFF_A;
import static l1j.server.server.model.skill.L1SkillId.FEATHER_BUFF_B;
import static l1j.server.server.model.skill.L1SkillId.FIRE_WALL;
import static l1j.server.server.model.skill.L1SkillId.FOG_OF_SLEEPING;
import static l1j.server.server.model.skill.L1SkillId.FOU_SLAYER;
import static l1j.server.server.model.skill.L1SkillId.FREEZING_BREATH;
import static l1j.server.server.model.skill.L1SkillId.GUARD_BREAK;
import static l1j.server.server.model.skill.L1SkillId.HORROR_OF_DEATH;
import static l1j.server.server.model.skill.L1SkillId.ICE_LANCE;
import static l1j.server.server.model.skill.L1SkillId.IMMUNE_TO_HARM;
import static l1j.server.server.model.skill.L1SkillId.IllUSION_AVATAR;
import static l1j.server.server.model.skill.L1SkillId.JOY_OF_PAIN;
import static l1j.server.server.model.skill.L1SkillId.LIFE_MAAN;
import static l1j.server.server.model.skill.L1SkillId.LIND_MAAN;
import static l1j.server.server.model.skill.L1SkillId.MANA_DRAIN;
import static l1j.server.server.model.skill.L1SkillId.MIND_BREAK;
import static l1j.server.server.model.skill.L1SkillId.MOB_BASILL;
import static l1j.server.server.model.skill.L1SkillId.MOB_COCA;
import static l1j.server.server.model.skill.L1SkillId.MORTAL_BODY;
import static l1j.server.server.model.skill.L1SkillId.PANIC;
import static l1j.server.server.model.skill.L1SkillId.PATIENCE;
import static l1j.server.server.model.skill.L1SkillId.PHANTASM;
import static l1j.server.server.model.skill.L1SkillId.POLLUTE_WATER;
import static l1j.server.server.model.skill.L1SkillId.REDUCTION_ARMOR;
import static l1j.server.server.model.skill.L1SkillId.RETURN_TO_NATURE;
import static l1j.server.server.model.skill.L1SkillId.SHAPE_CHANGE;
import static l1j.server.server.model.skill.L1SkillId.SHAPE_MAAN;
import static l1j.server.server.model.skill.L1SkillId.SHOCK_STUN;
import static l1j.server.server.model.skill.L1SkillId.엠파이어;
import static l1j.server.server.model.skill.L1SkillId.SILENCE;
import static l1j.server.server.model.skill.L1SkillId.SLOW;
import static l1j.server.server.model.skill.L1SkillId.SMASH;
import static l1j.server.server.model.skill.L1SkillId.SPECIAL_COOKING;
import static l1j.server.server.model.skill.L1SkillId.STATUS_CURSE_BARLOG;
import static l1j.server.server.model.skill.L1SkillId.STATUS_CURSE_YAHEE;
import static l1j.server.server.model.skill.L1SkillId.STATUS_HOLY_MITHRIL_POWDER;
import static l1j.server.server.model.skill.L1SkillId.STATUS_HOLY_WATER;
import static l1j.server.server.model.skill.L1SkillId.STATUS_HOLY_WATER_OF_EVA;
import static l1j.server.server.model.skill.L1SkillId.STRIKER_GALE;
import static l1j.server.server.model.skill.L1SkillId.TAMING_MONSTER;
import static l1j.server.server.model.skill.L1SkillId.THUNDER_GRAB;
import static l1j.server.server.model.skill.L1SkillId.TRIPLE_ARROW;
import static l1j.server.server.model.skill.L1SkillId.TURN_UNDEAD;
import static l1j.server.server.model.skill.L1SkillId.WEAKNESS;
import static l1j.server.server.model.skill.L1SkillId.DARK_BLIND;
import static l1j.server.server.model.skill.L1SkillId.WEAPON_BREAK;
import static l1j.server.server.model.skill.L1SkillId.WIND_SHACKLE;
import static l1j.server.server.model.skill.L1SkillId.데스페라도;
import static l1j.server.server.model.skill.L1SkillId.파워그립;
import static l1j.server.server.model.skill.L1SkillId.DEATH_HEAL;

import java.util.Random;

import l1j.server.Config;
import l1j.server.GameSystem.Robot.L1RobotInstance;
import l1j.server.server.ActionCodes;
import l1j.server.server.TimeController.WarTimeController;
import l1j.server.server.datatables.CharacterReduc;
import l1j.server.server.datatables.SkillsTable;
import l1j.server.server.model.Instance.L1DollInstance;
import l1j.server.server.model.Instance.L1ItemInstance;
import l1j.server.server.model.Instance.L1MonsterInstance;
import l1j.server.server.model.Instance.L1NpcInstance;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.model.Instance.L1PetInstance;
import l1j.server.server.model.Instance.L1SummonInstance;
import l1j.server.server.model.skill.L1SkillId;
import l1j.server.server.serverpackets.S_DoActionGFX;
import l1j.server.server.serverpackets.S_EffectLocation;
import l1j.server.server.serverpackets.S_SkillHaste;
import l1j.server.server.serverpackets.S_SkillSound;
import l1j.server.server.serverpackets.S_SystemMessage;
import l1j.server.server.templates.L1Skills;
import l1j.server.server.utils.CalcStat;

public class L1Magic {

	private int _calcType;

	private final int PC_PC = 1;

	private final int PC_NPC = 2;

	private final int NPC_PC = 3;

	private final int NPC_NPC = 4;

	private L1PcInstance _pc = null;

	private L1PcInstance _targetPc = null;

	private L1NpcInstance _npc = null;

	private L1NpcInstance _targetNpc = null;

	private int _leverage = 13;

	private L1Skills _skill;

	private static Random _random = new Random(System.nanoTime());

	public void setLeverage(int i) {
		_leverage = i;
	}

	private int getLeverage() {
		return _leverage;
	}

	public L1Magic(L1Character attacker, L1Character target) {
		if (attacker instanceof L1PcInstance) {
			if (target instanceof L1PcInstance) {
				_calcType = PC_PC;
				_pc = (L1PcInstance) attacker;
				_targetPc = (L1PcInstance) target;
			} else {
				_calcType = PC_NPC;
				_pc = (L1PcInstance) attacker;
				_targetNpc = (L1NpcInstance) target;
			}
		} else {
			if (target instanceof L1PcInstance) {
				_calcType = NPC_PC;
				_npc = (L1NpcInstance) attacker;
				_targetPc = (L1PcInstance) target;
			} else {
				_calcType = NPC_NPC;
				_npc = (L1NpcInstance) attacker;
				_targetNpc = (L1NpcInstance) target;
			}
		}
	}

	@SuppressWarnings("unused")
	private int getSpellPower() {
		int spellPower = 0;
		if (_calcType == PC_PC || _calcType == PC_NPC) {
			spellPower = _pc.getAbility().getSp();
		} else if (_calcType == NPC_PC || _calcType == NPC_NPC) {
			spellPower = _npc.getAbility().getSp();
		}
		return spellPower;
	}

	private int getMagicLevel() {
		int magicLevel = 0;
		if (_calcType == PC_PC || _calcType == PC_NPC) {
			magicLevel = _pc.getAbility().getMagicLevel();
		} else if (_calcType == NPC_PC || _calcType == NPC_NPC) {
			magicLevel = _npc.getAbility().getMagicLevel();
		}
		return magicLevel;
	}

	private int getMagicBonus() {
		int magicBonus = 0;
		if (_calcType == PC_PC || _calcType == PC_NPC) {
			magicBonus = _pc.getAbility().getMagicBonus();
		} else if (_calcType == NPC_PC || _calcType == NPC_NPC) {
			magicBonus = _npc.getAbility().getMagicBonus();
		}
		return magicBonus;
	}

	private int getLawful() {
		int lawful = 0;
		if (_calcType == PC_PC || _calcType == PC_NPC) {
			lawful = _pc.getLawful();
		} else if (_calcType == NPC_PC || _calcType == NPC_NPC) {
			lawful = _npc.getLawful();
		}
		return lawful;
	}

	private int getTargetMr() {
		int mr = 0;
		if (_calcType == PC_PC || _calcType == NPC_PC) {
			mr = _targetPc.getResistance().getEffectedMrBySkill();
		} else {
			mr = _targetNpc.getResistance().getEffectedMrBySkill();
		}
		return mr;
	}

	/* ■■■■■■■■■■■■■■ 성공 판정 ■■■■■■■■■■■■■ */
	// ●●●● 확률계 마법의 성공 판정 ●●●●
	// 계산방법
	// 공격측 포인트：LV + ((MagicBonus * 3) * 마법 고유 계수)
	// 방어측 포인트：((LV / 2) + (MR * 3)) / 2
	// 공격 성공율：공격측 포인트 - 방어측 포인트
	public boolean calcProbabilityMagic(int skillId) {
		int probability = 0;
		boolean isSuccess = false;

		if (_pc != null && _pc.isGm()) {
			return true;
		}

		if (_calcType == PC_NPC && _targetNpc != null) {
			int npcId = _targetNpc.getNpcTemplate().get_npcId();
			if (npcId >= 45912
					&& npcId <= 45915
					&& !_pc.getSkillEffectTimerSet().hasSkillEffect(
							STATUS_HOLY_WATER)) {
				return false;
			}
			if (npcId >= 145685 && npcId <= 145686  && !_pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.할파스권속버프)) {
				return false;
			}
			if (npcId == 45916
					&& !_pc.getSkillEffectTimerSet().hasSkillEffect(
							STATUS_HOLY_MITHRIL_POWDER)) {
				return false;
			}
			if (npcId == 45941
					&& !_pc.getSkillEffectTimerSet().hasSkillEffect(
							STATUS_HOLY_WATER_OF_EVA)) {
				return false;
			}
			if (npcId >= 46068 && npcId <= 46091
					&& _pc.getGfxId().getTempCharGfx() == 6035) {
				return false;
			}
			if (npcId >= 46092 && npcId <= 46106
					&& _pc.getGfxId().getTempCharGfx() == 6034) {
				return false;
			}
		}

		if (!checkZone(skillId)) {
			return false;
		}
		
		if (skillId == SHAPE_CHANGE){
			if (_calcType == PC_PC && _pc != null && _targetPc != null) {
				if (_pc.getId() == _targetPc.getId()) {
					return true;
				} else if (_targetPc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.SHAPE_CHANGE_JIBAE)) {
					 _pc.sendPackets(new S_SkillSound(_targetPc.getId(), 15846), true);
					 Broadcaster.broadcastPacket(_pc, new S_SkillSound(_targetPc.getId(), 15846), true);
					 return false;
				}
			if (CharPosUtil.getZoneType(_pc) == 1 || CharPosUtil.getZoneType(_targetPc) == 1) {
				return false;
			}
			if(!CharPosUtil.isAreaAttack(_pc, _targetPc.getX(), _targetPc.getY(), _targetPc.getMapId()) 
				|| !CharPosUtil.isAreaAttack(_targetPc, _pc.getX(), _pc.getY(), _pc.getMapId())) {
				return false;
			}
			}
		}
		
		if (skillId == CANCELLATION) {
			if (_calcType == PC_PC && _pc != null && _targetPc != null) {
				if (_pc.getId() == _targetPc.getId()) {
					return true;
				} else if (_targetPc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.SHAPE_CHANGE_JIBAE)) {
					 _pc.sendPackets(new S_SkillSound(_targetPc.getId(), 15846), true);
					 Broadcaster.broadcastPacket(_pc, new S_SkillSound(_targetPc.getId(), 15846), true);
					 return false;
				}
				if (_pc.getClanid() > 0 && (_pc.getClanid() == _targetPc.getClanid())) {
					/** 2011.04.15 고정수 배틀존 */
					if (_targetPc.get_DuelLine() != 0) {
						return false;
					} else {
						return true;
					}
				}

				if (_pc.isInParty()) {
					if (_pc.getParty().isMember(_targetPc)) {
						return true;
					}
				}

				if (CharPosUtil.getZoneType(_pc) == 1 || CharPosUtil.getZoneType(_targetPc) == 1) {
					return false;
				}
			}
			if (_calcType == PC_NPC || _calcType == NPC_PC || _calcType == NPC_NPC) {
				return true;
			}
		}

		if (_calcType == PC_NPC
				&& _targetNpc.getNpcTemplate().isCantResurrect()

		) { // 50렙 이상 npc 에게 아래 마법 안걸림:즉 보스몬스터에게 사용불가
			if (skillId == WEAPON_BREAK || skillId == CURSE_PARALYZE
					|| skillId == MANA_DRAIN || skillId == WEAKNESS
					|| skillId == SILENCE || skillId == DISEASE
					|| skillId == DECAY_POTION || skillId == ERASE_MAGIC || skillId == AREA_OF_SILENCE
					|| skillId == WIND_SHACKLE || skillId == FOG_OF_SLEEPING 
					|| skillId == ICE_LANCE || skillId == POLLUTE_WATER 
					|| skillId == RETURN_TO_NATURE || skillId == THUNDER_GRAB
					|| skillId == 파워그립) {
				return false;
			}
		}

		if (_calcType == PC_PC) { // 디스중첩 안되게
			if (_targetPc.getSkillEffectTimerSet().hasSkillEffect(DISINTEGRATE)) {
				if (skillId == DISINTEGRATE) {
					return false;
				}
			}
		}
		// 아스바인드중은 WB, 왈가닥 세레이션 이외 무효
		if (_calcType == PC_PC || _calcType == NPC_PC) {
			if (_targetPc.getSkillEffectTimerSet().hasSkillEffect(EARTH_BIND)) {
				_skill = SkillsTable.getInstance().getTemplate(skillId);
				if (skillId != WEAPON_BREAK && skillId != CANCELLATION // 확률계
						&& _skill.getType() != L1Skills.TYPE_HEAL // 힐 계
						&& _skill.getType() != L1Skills.TYPE_CHANGE) { // 버프계
					return false;
				}
			}
		} else {
			if (_targetNpc.getSkillEffectTimerSet().hasSkillEffect(EARTH_BIND)) {
				if (skillId != WEAPON_BREAK && skillId != CANCELLATION) {
					return false;
				}
			}
		}

		// 100% 확률을 가지는 스킬
		if (skillId == SMASH || skillId == MIND_BREAK) {
			return true;
		}
		
		probability = calcProbability(skillId);
		int rnd = 0;

		if ((skillId == EARTH_BIND || skillId == DARKNESS || skillId == CURSE_BLIND || skillId == CURSE_PARALYZE)) {
			if (_calcType == PC_NPC) {
				if (_targetNpc.getLevel() >= 70) {
					return false;
				}
			}
		}

		switch (skillId) {
		case CANCELLATION:
		case SILENCE:
			if (_calcType == PC_PC) {
					rnd = _random.nextInt(100) + 1;
				if (_targetPc.isInvisble()) {
					probability = 0;
				}
			} else if (_calcType == PC_NPC) {
				rnd = 40;
			} else {
				rnd = _random.nextInt(100) + 1;
			}
			break;
		case DECAY_POTION:
		case CURSE_PARALYZE:
		case SLOW:
		case DARKNESS:
		case WEAKNESS:
		case CURSE_POISON:
		case CURSE_BLIND:
		case WEAPON_BREAK:
		case MANA_DRAIN:
			if (_calcType == PC_PC) {
					rnd = _random.nextInt(100) + 1;
			} else if (_calcType == PC_NPC) {
				rnd = 40;
			} else {
			}
			break;
		default:
			rnd = _random.nextInt(100) + 1;
			if (probability > 80)
				probability = 80;
			/** 아랜 -> 얼녀,아이스데몬 마방120이상 대상에게 안들어가게 **/
			if (skillId == ICE_LANCE && _calcType == NPC_PC && (_npc.getNpcId() == 46141 || _npc.getNpcId() == 46142) && _targetPc.getResistance().getEffectedMrBySkill() >= 100)
				probability = 0;
			else if (skillId == ICE_LANCE && _calcType == NPC_PC && _npc.getNpcId() == 100367)
				probability = 20;
			else if (skillId == ICE_LANCE && _calcType == NPC_PC && (_npc.getNpcId() == 46141 || _npc.getNpcId() == 46142)) {
				probability *= 0.5;
			} else if (skillId == TURN_UNDEAD && _calcType == PC_NPC && _pc != null && !_pc.isWizard())
				probability *= 0.5;
			break;
		}

		if (probability >= rnd) {
			isSuccess = true;
		} else {
			isSuccess = false;
			if (skillId == TURN_UNDEAD && _calcType == PC_NPC && _targetNpc != null) {
				if (_random.nextInt(100) + 1 < 20) {
					if (!_targetNpc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.HASTE)) {
						_targetNpc.getSkillEffectTimerSet().setSkillEffect(L1SkillId.HASTE, 9999 * 1000);
						Broadcaster.broadcastPacket(_targetNpc, new S_SkillHaste(_targetNpc.getId(), 1, 0), true);
						_targetNpc.getMoveState().setMoveSpeed(1);
						Broadcaster.broadcastPacket(_targetNpc, new S_EffectLocation(_targetNpc.getX(), _targetNpc.getY(), (short) 8987), true);
					}
				}
			}
		}

		if (!Config.ALT_ATKMSG) {
			return isSuccess;
		}
		if (_targetPc == null && _targetNpc == null)
			return isSuccess;

		String msg2 = "확률:" + probability + "%";
		String msg3 = "";
		if (isSuccess == true) {
			msg3 = "성공";
		} else {
			msg3 = "실패";
		}

		if (_pc != null && _pc.isGm()) {
			_pc.sendPackets(new S_SystemMessage("\\fT["
					+ _pc.getName()
					+ "] ==> ["
					+ (_targetPc == null ? _targetNpc.getName() : _targetPc
							.getName()) + "][== " + msg2 + " ==][" + msg3 + "]"));
		}
		if (_targetPc != null && _targetPc.isGm()) {
			_targetPc.sendPackets(new S_SystemMessage("\\fY["
					+ _targetPc.getName() + "] <== ["
					+ (_pc == null ? _npc.getName() : _pc.getName()) + "][== "
					+ msg2 + " ==][" + msg3 + "]"));
		}
		return isSuccess;
	}

	private boolean checkZone(int skillId) {
		if (_pc != null && _targetPc != null) {
			if (CharPosUtil.getZoneType(_pc) == 1 || CharPosUtil.getZoneType(_targetPc) == 1) {
				if (skillId == WEAPON_BREAK || skillId == SLOW
						|| skillId == CURSE_PARALYZE || skillId == MANA_DRAIN
						|| skillId == DARKNESS || skillId == WEAKNESS
						|| skillId == DISEASE || skillId == DECAY_POTION
						|| skillId == ERASE_MAGIC || skillId == EARTH_BIND
						|| skillId == AREA_OF_SILENCE || skillId == DEATH_HEAL
						|| skillId == WIND_SHACKLE || skillId == STRIKER_GALE
						|| skillId == SHOCK_STUN || skillId == 엠파이어 || skillId == 파워그립
						|| skillId == 데스페라도 || skillId == FOG_OF_SLEEPING
						|| skillId == ICE_LANCE || skillId == HORROR_OF_DEATH
						|| skillId == POLLUTE_WATER || skillId == FEAR
						|| skillId == ELEMENTAL_FALL_DOWN || skillId == SHAPE_CHANGE
						|| skillId == GUARD_BREAK || skillId == SHAPE_CHANGE
						|| skillId == RETURN_TO_NATURE || skillId == PHANTASM
						|| skillId == CONFUSION || skillId == SILENCE) {
					return false;
				}
			}
		}
		return true;
	}

	private int calcProbability(int skillId) {
		L1Skills l1skills = SkillsTable.getInstance().getTemplate(skillId);
		int attackLevel = 0;
		int defenseLevel = 0;
		int probability = 0;
		int attackInt = 0;
		int defenseMr = 0;
		
		if (_calcType == PC_PC || _calcType == PC_NPC) {
			attackLevel = _pc.getLevel();
			attackInt = _pc.getAbility().getTotalInt();
		} else {
			attackLevel = _npc.getLevel();
			attackInt = _npc.getAbility().getTotalInt();
		}

		if (_calcType == PC_PC || _calcType == NPC_PC) {
			defenseLevel = _targetPc.getLevel();
			defenseMr = _targetPc.getResistance().getEffectedMrBySkill();
		} else {
			defenseLevel = _targetNpc.getLevel();
			defenseMr = _targetNpc.getResistance().getEffectedMrBySkill();
			if (skillId == RETURN_TO_NATURE) {
				if (_targetNpc instanceof L1SummonInstance) {
					L1SummonInstance summon = (L1SummonInstance) _targetNpc;
					defenseLevel = summon.getMaster().getLevel();
				}
			}
		}

		switch (skillId) {
		
		case THUNDER_GRAB:
		case GUARD_BREAK:
		case FEAR:
		case HORROR_OF_DEATH:
		case BONE_BREAK:	
		case PHANTASM:
		case CONFUSION:{
			probability = (int) ((attackLevel - defenseLevel) * l1skills.getProbabilityDice()) + l1skills.getProbabilityValue();
			if(_calcType == PC_PC){
				probability -= (int) _targetPc.getResistance().getDragonLang();
				probability += (int) _pc.getResistance().getDragonLangHit();
			} else if (_calcType == PC_NPC){
				probability += (int) _pc.getResistance().getDragonLangHit();
			} else if (_calcType == NPC_PC){
				probability -= (int) _targetPc.getResistance().getDragonLang();
			}
			if (probability > 90) {
				probability = 90;
			}
			if (probability < 10) {
				probability = 10;
			}
		}
			break;
		case 데스페라도:
		case 파워그립:{
			probability = (int) ((attackLevel - defenseLevel) * l1skills.getProbabilityDice()) + l1skills.getProbabilityValue();
			if(_calcType == PC_PC){
				probability -= (int) _targetPc.getResistance().getFear();
				probability += (int) _pc.getResistance().getFearHit();
			} else if (_calcType == PC_NPC){
				probability += (int) _pc.getResistance().getFearHit();
			} else if (_calcType == NPC_PC){
				probability -= (int) _targetPc.getResistance().getFear();
			}
			
			if (probability > 95) {
				probability = 95;
			}

			if (probability < 10)
				probability = 10;
		}
			break;
		case 엠파이어:
		case SHOCK_STUN:{
			probability = (int) ((attackLevel - defenseLevel) * l1skills.getProbabilityDice()) + l1skills.getProbabilityValue();
			if(_calcType == PC_PC){
				probability -= (int) _targetPc.getResistance().getTechnique();;
				probability += (int) _pc.getResistance().getTechniqueHit();
			} else if (_calcType == PC_NPC){
				probability += (int) _pc.getResistance().getTechniqueHit();
			} else if (_calcType == NPC_PC){
				probability -= (int) _targetPc.getResistance().getTechnique();;
			}
		
			if (probability > 90) {
				probability = 90;
				}
			}
			break;
		case DARK_BLIND:
		case ARMOR_BREAK:
		case WIND_SHACKLE:
		case ERASE_MAGIC:
		case STRIKER_GALE:
		case POLLUTE_WATER:
		case AREA_OF_SILENCE:
		case EARTH_BIND:{
			
			probability = (int) ((attackLevel - defenseLevel) * l1skills.getProbabilityDice()) + l1skills.getProbabilityValue();
			
			if(_calcType == PC_PC){
				probability -= (int) _targetPc.getResistance().getSpirit();
				probability += (int) _pc.getResistance().getSpiritHit();;
			} else if (_calcType == PC_NPC){
				probability += (int) _pc.getResistance().getSpiritHit();;
			} else if (_calcType == NPC_PC){
				probability -= (int) _targetPc.getResistance().getSpirit();
			}
			
			if(skillId == ARMOR_BREAK){
				if(_pc.isABDestiny && _pc.getLevel() >= 85){
					probability += (_pc.getLevel() - 84) * 3;
				}
			}
			if (probability > 90) {
				probability = 90;
			}
			
			break;
		}
		case ELEMENTAL_FALL_DOWN:
		case RETURN_TO_NATURE:
			probability = (int) ((attackLevel - defenseLevel) * l1skills.getProbabilityDice()) + l1skills.getProbabilityValue();
			if (_calcType == PC_PC || _calcType == PC_NPC) {
				probability += 2 * _pc.getBaseMagicHitUp();
			}
			break;
		case MORTAL_BODY:
		case COUNTER_BARRIER:
		case 인페르노:
			probability = l1skills.getProbabilityValue();
			break;
		case PANIC:
			probability = (int) ((attackLevel - defenseLevel) * l1skills.getProbabilityDice()) + l1skills.getProbabilityValue();
			if (_calcType == PC_PC || _calcType == PC_NPC) {
				probability += 2 * _pc.getBaseMagicHitUp();
			}
			break;
		case DEATH_HEAL:{

			probability = (int) ((attackInt*2) - (defenseMr / 1.6));
			
			if (_calcType == NPC_PC || _calcType == PC_PC || _calcType == PC_NPC ) {
				if(defenseMr < 50){
					probability = 100;
				}else if(defenseMr >=50 && defenseMr < 70){
					probability = 80;
				}else if(defenseMr >=70 && defenseMr < 80){
					probability = 70;
				}else if(defenseMr >=80 && defenseMr < 90){
					probability = 60;
				}else if(defenseMr >=90 && defenseMr < 100){
					probability = 50;
				}else if(defenseMr >=100 && defenseMr < 110){
					probability = 45;
				}else if(defenseMr >=110 && defenseMr < 120){
					probability = 40;
				}else if(defenseMr >=120 && defenseMr < 130){
					probability = 30;
				}else if(defenseMr >=130 && defenseMr < 140){
					probability = 20;
				}else if(defenseMr >=150 && defenseMr < 160){
					probability = 10;
				}else if(defenseMr >=160){
					probability = 5;
				}
				if(_calcType == PC_PC || _calcType == PC_NPC){
					if(_pc.getSuccMagic() > 0){
					probability += _pc.getSuccMagic()/2;
					}
					if(!_pc.isWizard())
						probability /= 3;
				}
				
				if(defenseMr >= 180){
					probability = 0;
				}
				
				if(probability < 0){
					probability = 0;
				}
				if(probability >= 80){
					probability = 80;
				}
			}
		}
		break;
		case SHAPE_CHANGE:{

			probability = (int) ((attackInt*2) - (defenseMr / 1.6));
			
			if (_calcType == NPC_PC || _calcType == PC_PC || _calcType == PC_NPC ) {
				if(defenseMr < 50){
					probability = 100;
				}else if(defenseMr >=50 && defenseMr < 70){
					probability = 80;
				}else if(defenseMr >=70 && defenseMr < 80){
					probability = 70;
				}else if(defenseMr >=80 && defenseMr < 90){
					probability = 60;
				}else if(defenseMr >=90 && defenseMr < 100){
					probability = 50;
				}else if(defenseMr >=100 && defenseMr < 110){
					probability = 45;
				}else if(defenseMr >=110 && defenseMr < 120){
					probability = 40;
				}else if(defenseMr >=120 && defenseMr < 130){
					probability = 30;
				}else if(defenseMr >=130 && defenseMr < 140){
					probability = 20;
				}else if(defenseMr >=150 && defenseMr < 160){
					probability = 10;
				}else if(defenseMr >=160){
					probability = 5;
				}
				
				if(_calcType == PC_PC || _calcType == PC_NPC){
					if(_pc.getSuccMagic() > 0){
					probability += _pc.getSuccMagic()/2;
					}
					if(!_pc.isWizard())
						probability /= 3;
				}
				if(defenseMr >= 180){
					probability = 0;
				}
				
				if(probability < 0){
					probability = 0;
				}
				if(probability >= 80){
					probability = 80;
				}
			}
		}
			break;
		case FOG_OF_SLEEPING:
		case DARKNESS:
		case WEAKNESS:
		case CURSE_PARALYZE:
		case CURSE_POISON:
		case CURSE_BLIND:
		case WEAPON_BREAK:
		case CANCELLATION:
		case DECAY_POTION:
		case SILENCE:
		case MANA_DRAIN:
		case MOB_COCA:
		case MOB_BASILL:
		case SLOW: {

			probability = (int) ((attackInt*2) - (defenseMr / 1.6));
			
			if (_calcType == NPC_PC || _calcType == PC_PC || _calcType == PC_NPC ) {
				if(defenseMr < 50){
					probability = 100;
				}else if(defenseMr >=50 && defenseMr < 70){
					probability = 80;
				}else if(defenseMr >=70 && defenseMr < 80){
					probability = 70;
				}else if(defenseMr >=80 && defenseMr < 90){
					probability = 60;
				}else if(defenseMr >=90 && defenseMr < 100){
					probability = 50;
				}else if(defenseMr >=100 && defenseMr < 110){
					probability = 45;
				}else if(defenseMr >=110 && defenseMr < 120){
					probability = 40;
				}else if(defenseMr >=120 && defenseMr < 130){
					probability = 30;
				}else if(defenseMr >=130 && defenseMr < 140){
					probability = 20;
				}else if(defenseMr >=150 && defenseMr < 160){
					probability = 10;
				}else if(defenseMr >=160){
					probability = 5;
				}
				if(_calcType == PC_PC || _calcType == PC_NPC){
					if(_pc.getSuccMagic() > 0){
					probability += _pc.getSuccMagic()/2;
					}
					if(!_pc.isWizard())
						probability /= 3;
				}
				
				if(defenseMr >= 170){
					probability = 0;
				}
				
				if(probability < 0){
					probability = 0;
				}
				if(probability >= 80){
					probability = 80;
				}
			}
		}
			break;
		case JOY_OF_PAIN:
			probability = (int) ((attackLevel - defenseLevel) * l1skills.getProbabilityDice()) + l1skills.getProbabilityValue();
			break;
		case TURN_UNDEAD:
			if (_calcType == PC_PC || _calcType == PC_NPC) {
				probability = (int) ((attackInt * 2 + (attackLevel * Config.TURN_UNDEAD) + _pc.getBaseMagicHitUp())
						- (defenseMr + (defenseLevel / 2)) - 84);
				if (!_pc.isWizard()) {
					probability -= 30;
				}
			} else
				probability = (int) ((attackInt * 2 + (attackLevel * Config.TURN_UNDEAD))
						- (defenseMr + (defenseLevel / 2)) - 84);

			if (attackInt >= 35) {
				int addpro = 0;
				if (attackInt == 35)
					addpro += 4;
				if (attackInt == 40)
					addpro += 4;
				if (attackInt == 45)
					addpro += 4;
				if (attackInt == 50)
					addpro += 4;
				if (attackInt == 55)
					addpro += 4;
				if (attackInt == 60)
					addpro += 4;
				probability += addpro;
			}

			if (probability > 0) {
				if(_calcType == PC_NPC){
					if(_pc.getSuccMagic() > 0){
					probability += _pc.getSuccMagic();
					}
				}
			}

			break;
		default: {
			int dice1 = l1skills.getProbabilityDice();
			int diceCount1 = 0;
			if (_calcType == PC_PC || _calcType == PC_NPC) {
				if (_pc.isWizard()) {
					diceCount1 = getMagicBonus() + getMagicLevel() + 1;
				} else if (_pc.isElf()) {
					diceCount1 = getMagicBonus() + getMagicLevel() - 1;
				} else if (_pc.isDragonknight()) {
					diceCount1 = getMagicBonus() + getMagicLevel();
				} else {
					diceCount1 = getMagicBonus() + getMagicLevel() - 1;
				}
			} else {
				diceCount1 = getMagicBonus() + getMagicLevel();
			}
			if (diceCount1 < 1) {
				diceCount1 = 1;
			}
			for (int i = 0; i < diceCount1; i++) {
				probability += (_random.nextInt(dice1) + 1);
			}
			probability = probability * getLeverage() / 10;
			probability -= getTargetMr();

			if (skillId == TAMING_MONSTER) {
				double probabilityRevision = 1;
				if ((_targetNpc.getMaxHp() * 1 / 4) > _targetNpc.getCurrentHp()) {
					probabilityRevision = 1.3;
				} else if ((_targetNpc.getMaxHp() * 2 / 4) > _targetNpc
						.getCurrentHp()) {
					probabilityRevision = 1.2;
				} else if ((_targetNpc.getMaxHp() * 3 / 4) > _targetNpc
						.getCurrentHp()) {
					probabilityRevision = 1.1;
				}
				probability *= probabilityRevision;
			}
		}

			break;
		}
		if (_calcType == PC_PC || _calcType == PC_NPC) {
			if (probability > 0) {
				if (skillId != L1SkillId.SHOCK_STUN && skillId != L1SkillId.엠파이어 && skillId != L1SkillId.데스페라도 && skillId != L1SkillId.파워그립) {
					L1ItemInstance run = _pc.getInventory().getItemEquipped(2, 14);
					if (run != null) {
						if (run.getItemId() == 427115) { // 마법 명중 룬
							probability += 1;
						} else if (run.getItemId() == 427205) { // 장로의 부러진 지팡이
							probability += 2;
						}
					}
					if (_pc.getInventory().checkEquipped(21269)) {
						probability += 3;
					}
				}
			}
		}
		return probability;
	}

	public int calcMagicDamage(int skillId) {
		int damage = 0;
		if (_calcType == PC_PC || _calcType == NPC_PC) {
			damage = calcPcMagicDamage(skillId);
			if (_calcType == PC_PC) {
				if (_pc.getClanid() > 0 && (_pc.getClanid() == _targetPc.getClanid())) {
					if (skillId == 17 || skillId == 22 || skillId == 25 || skillId == 53 || skillId == 53 || skillId == 59 || skillId == 62 || skillId == 65 || skillId == 70 || skillId == 74) { // 미티어 포함한 범위마법들..
						damage = 0;
					}
				}
			}
		} else if (_calcType == PC_NPC || _calcType == NPC_NPC) {
			damage = calcNpcMagicDamage(skillId);
			// ////////혈원에게는 범위마법 데미지 0 디플추가////////////
			// ////////혈원에게는 범위마법 데미지 0 디플추가끝///////////
		}

		if (_calcType == PC_PC || _calcType == PC_NPC) {
			if (skillId == L1SkillId.ENERGY_BOLT && _pc.getInventory().checkEquipped(120)) {
				damage += 10;
			}

			int tempsp = _pc.getAbility().getSp();

			if (tempsp >= 33) {
				int temp2sp = tempsp / 3;
				damage += damage * (temp2sp * 0.01);
			}

			damage += CalcStat.마법대미지(_pc.getAbility().getTotalInt());

			double balance = CalcStat.마법보너스(_pc.getAbility().getTotalInt()) * 0.01;

			damage += damage * balance;
		}
		if (_calcType == PC_PC){
				
		L1ItemInstance 발라카스의완력 = _pc.getInventory().checkEquippedItem(420112);
		L1ItemInstance 발라카스의예지력 = _pc.getInventory().checkEquippedItem(420113);
		L1ItemInstance 발라카스의인내력 = _pc.getInventory().checkEquippedItem(420114);
		L1ItemInstance 발라카스의마력 = _pc.getInventory().checkEquippedItem(420115);
		
		if (발라카스의완력 != null) {
			int ReducLevel = 0;
			int targetReduc = _targetPc.getDamageReductionByArmor();
			switch(발라카스의완력.getEnchantLevel()){
			case 0: case 1: case 2: case 3: case 4: case 5:
				ReducLevel = 3;
				break;	
			case 6:
				ReducLevel = 4;
				break;
			case 7:
				ReducLevel = 5;
				break;
			case 8:
				ReducLevel = 6;
				break;
			case 9:
				ReducLevel = 7;
			break;
			default:
				if(발라카스의완력.getEnchantLevel() > 9){
					ReducLevel = 7;
				}
				break;
			}
			if (targetReduc < ReducLevel){
				ReducLevel = targetReduc;
			}
			if(ReducLevel < 0){
				ReducLevel = 0;
			}
			damage += ReducLevel;
		}
		if (발라카스의예지력 != null) {
			int ReducLevel = 0;
			int targetReduc = _targetPc.getDamageReductionByArmor();
			switch(발라카스의예지력.getEnchantLevel()){
			case 0: case 1: case 2: case 3: case 4: case 5:
				ReducLevel = 3;
				break;	
			case 6:
				ReducLevel = 4;
				break;
			case 7:
				ReducLevel = 5;
				break;
			case 8:
				ReducLevel = 6;
				break;
			case 9:
				ReducLevel = 7;
			break;
			default:
				if(발라카스의예지력.getEnchantLevel() > 9){
					ReducLevel = 7;
				}
				break;
			}
			if (targetReduc < ReducLevel){
				ReducLevel = targetReduc;
			}
			if(ReducLevel < 0){
				ReducLevel = 0;
			}
			damage += ReducLevel;
		}
		if (발라카스의인내력 != null) {
			int ReducLevel = 0;
			int targetReduc = _targetPc.getDamageReductionByArmor();
			switch(발라카스의인내력.getEnchantLevel()){
			case 0: case 1: case 2: case 3: case 4: case 5:
				ReducLevel = 3;
				break;	
			case 6:
				ReducLevel = 4;
				break;
			case 7:
				ReducLevel = 5;
				break;
			case 8:
				ReducLevel = 6;
				break;
			case 9:
				ReducLevel = 7;
			break;
			default:
				if(발라카스의인내력.getEnchantLevel() > 9){
					ReducLevel = 7;
				}
				break;
			}
			if (targetReduc < ReducLevel){
				ReducLevel = targetReduc;
			}
			if(ReducLevel < 0){
				ReducLevel = 0;
			}
			damage += ReducLevel;
		}
		if (발라카스의마력 != null) {
			int ReducLevel = 0;
			int targetReduc = _targetPc.getDamageReductionByArmor();
			switch(발라카스의마력.getEnchantLevel()){
			case 0: case 1: case 2: case 3: case 4: case 5:
				ReducLevel = 3;
				break;	
			case 6:
				ReducLevel = 4;
				break;
			case 7:
				ReducLevel = 5;
				break;
			case 8:
				ReducLevel = 6;
				break;
			case 9:
				ReducLevel = 7;
			break;
			default:
				if(발라카스의마력.getEnchantLevel() > 9){
					ReducLevel = 7;
				}
				break;
			}
			if (targetReduc < ReducLevel){
				ReducLevel = targetReduc;
			}
			if(ReducLevel < 0){
				ReducLevel = 0;
			}
			damage += ReducLevel;
		}
		
		int chance = _random.nextInt(100) + 1;
		if(chance < _targetPc.get확률마법회피()){
			damage = 0;
			}
		}
		
		// 파푸가호
		if (_targetPc != null) {
			// Random random = new Random();
			int chance1 = _random.nextInt(100) + 1;
			if ((_targetPc.getInventory().checkEquipped(420104)
					|| _targetPc.getInventory().checkEquipped(420105)
					|| _targetPc.getInventory().checkEquipped(420106) || _targetPc
					.getInventory().checkEquipped(420107)) && chance1 < 5) {
				// 123456 일때 80~100
				// 파푸 가호 7일때 120~140 / 8일때 140~160 9일때 160~180
				int addhp = _random.nextInt(20) + 1;
				int basehp = 80;

				L1ItemInstance item = null;
				if (_targetPc.getInventory().checkEquipped(420104)) {
					item = _targetPc.getInventory().checkEquippedItem(420104);
				}
				if (_targetPc.getInventory().checkEquipped(420105)) {
					item = _targetPc.getInventory().checkEquippedItem(420105);
				}
				if (_targetPc.getInventory().checkEquipped(420106)) {
					item = _targetPc.getInventory().checkEquippedItem(420106);
				}
				if (_targetPc.getInventory().checkEquipped(420107)) {
					item = _targetPc.getInventory().checkEquippedItem(420107);
				}

				if (item.getEnchantLevel() == 7)
					basehp = 120;
				if (item.getEnchantLevel() == 8)
					basehp = 140;
				if (item.getEnchantLevel() == 9)
					basehp = 160;

				_targetPc.setCurrentHp(_targetPc.getCurrentHp() + basehp
						+ addhp);
				_targetPc
						.sendPackets(new S_SkillSound(_targetPc.getId(), 2187));
				Broadcaster.broadcastPacket(_targetPc, new S_SkillSound(
						_targetPc.getId(), 2187));
			} else if (_targetPc.getInventory().checkEquipped(21255)
					&& chance1 < 4) {
				_targetPc.setCurrentHp(_targetPc.getCurrentHp() + 31);
				_targetPc
						.sendPackets(new S_SkillSound(_targetPc.getId(), 2183));
				Broadcaster.broadcastPacket(_targetPc, new S_SkillSound(
						_targetPc.getId(), 2183));
			}
		}
		// 파푸가호


		damage = calcMrDefense(damage);
		
		return damage;
	}

	private int calcTaitanDamage(int type) {
		int damage = 0;
		L1ItemInstance weapon = null;
		weapon = _targetPc.getWeapon();
		if (weapon != null) {
			// (큰 몬스터 타격치 + 추가 타격 옵션+ 인챈트 수치 ) x 2
			damage = (weapon.getItem().getDmgLarge() + weapon.getEnchantLevel() + weapon
					.getItem().getDmgModifier()) * 2;
		}
		return damage;
	}

	public int calcPcFireWallDamage() {
		int dmg = 0;
		double attrDeffence = calcAttrResistance(L1Skills.ATTR_FIRE);
		L1Skills l1skills = SkillsTable.getInstance().getTemplate(FIRE_WALL);
		dmg = (int) ((1.0 - attrDeffence) * l1skills.getDamageValue());

		if (_targetPc.getSkillEffectTimerSet().hasSkillEffect(ABSOLUTE_BARRIER)
				|| _targetPc.getSkillEffectTimerSet().hasSkillEffect(ICE_LANCE)
				|| _targetPc.getSkillEffectTimerSet().hasSkillEffect(
						FREEZING_BREATH)
				|| _targetPc.getSkillEffectTimerSet()
						.hasSkillEffect(EARTH_BIND)
				|| _targetPc.getSkillEffectTimerSet()
						.hasSkillEffect(MOB_BASILL)
				|| _targetPc.getSkillEffectTimerSet().hasSkillEffect(MOB_COCA)
				|| _targetPc.getSkillEffectTimerSet().hasSkillEffect(
						L1SkillId.STATUS_안전모드)) {
			dmg = 0;
		}

		if (dmg < 0) {
			dmg = 0;
		}

		return dmg;
	}

	public int calcNpcFireWallDamage() {
		int dmg = 0;
		double attrDeffence = calcAttrResistance(L1Skills.ATTR_FIRE);
		L1Skills l1skills = SkillsTable.getInstance().getTemplate(FIRE_WALL);
		dmg = (int) ((1.0 - attrDeffence) * l1skills.getDamageValue());

		if (_targetNpc.getSkillEffectTimerSet().hasSkillEffect(ICE_LANCE)
				|| _targetNpc.getSkillEffectTimerSet().hasSkillEffect(
						FREEZING_BREATH)
				|| _targetNpc.getSkillEffectTimerSet().hasSkillEffect(
						EARTH_BIND)
				|| _targetNpc.getSkillEffectTimerSet().hasSkillEffect(
						MOB_BASILL)
				|| _targetNpc.getSkillEffectTimerSet().hasSkillEffect(MOB_COCA)) {
			dmg = 0;
		}

		if (dmg < 0) {
			dmg = 0;
		}

		return dmg;
	}

	private int calcPcMagicDamage(int skillId) {
		int dmg = 0;

			// if (_calcType == PC_PC) {
		dmg = calcMagicDiceDamage(skillId);
		dmg = (dmg * getLeverage()) / 10;
			// }else if (_calcType == NPC_PC) {
			// dmg = calcMagicDiceDamage(skillId);
			// dmg = (dmg * getLeverage()) / 10;
			// }

		dmg -= _targetPc.getDamageReductionByArmor();
		
		if (_calcType == PC_PC){
			if (_targetPc.getPVPMagicDamageReduction() > 0){
			    dmg -= _targetPc.getPVPMagicDamageReduction();
			}
		}

		if (_targetPc.getSkillEffectTimerSet().hasSkillEffect(SPECIAL_COOKING)) { // 스페셜요리에
																					// 의한
																					// 데미지
																					// 경감
			dmg -= 5;
		}
		if (_targetPc.isAmorGaurd) { // 아머가드에의한 데미지감소
			int d = _targetPc.getAC().getAc() / 20;
			if (d < 0) {
				dmg += d;
			} else {
				dmg -= d;
			}
		}
		if (_targetPc.getSkillEffectTimerSet().hasSkillEffect(REDUCTION_ARMOR)) {
			int targetPcLvl = _targetPc.getLevel();
			if (targetPcLvl < 50) {
				targetPcLvl = 50;
			}
			dmg -= (targetPcLvl - 50) / 5 + 1;
		}
		
		if (_targetPc.getSkillEffectTimerSet().hasSkillEffect(MAJESTY)) {
			if (_targetPc.getLevel() >= 80) {
                dmg -= 2 + ((_targetPc.getLevel() - 80) / 2);
            } else {
                dmg -= 2;
            }
		}

		if (_calcType == NPC_PC) {
			boolean isNowWar = false;
			int castleId = L1CastleLocation.getCastleIdByArea(_targetPc);
			if (castleId > 0) {
				isNowWar = WarTimeController.getInstance().isNowWar(castleId);
			}
			if (!isNowWar) {
				if (_npc instanceof L1PetInstance) {
					dmg /= 8;
				}
				if (_npc instanceof L1SummonInstance) {
					L1SummonInstance summon = (L1SummonInstance) _npc;
					if (summon.isExsistMaster()) {
						dmg /= 8;
					}
				}
			}
			// Object[] dollList = _targetPc.getDollList().values().toArray();
			// // 마법 인형에 의한 추가 방어
			// L1DollInstance doll = null;
			for (L1DollInstance doll : _targetPc.getDollList()) {
				// doll = (L1DollInstance) dollObject;
				dmg -= doll.getDamageReductionByDoll();
			}
			
			if(_targetPc.getAinHasad() < -500000){
				dmg *= 20;
			}
			// dmg -= dmg*0.05;
			// 몹 스킬 데미지 관련
		}

		if (_targetPc.getSkillEffectTimerSet().hasSkillEffect(IllUSION_AVATAR) || _targetPc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.CUBE_AVATAR)) {
			dmg += dmg / 20;
		}
		if (_targetPc.getSkillEffectTimerSet().hasSkillEffect(PATIENCE)) {
			dmg -= 4;
		}
		if (_targetPc.getSkillEffectTimerSet().hasSkillEffect(IMMUNE_TO_HARM)) {
			dmg *= _targetPc.immuneLevel;
		}

		if (_targetPc.getSkillEffectTimerSet().hasSkillEffect(MOB_BASILL)
				|| _targetPc.getSkillEffectTimerSet().hasSkillEffect(MOB_COCA)) {
			dmg = 0;
		}

		if (_targetPc.isTaitanM) {
			if(_targetPc.getWeapon() != null){
			int hpRatio = 100;
			int TitanHp = 41;
			int 라이징 = 5;
			if (0 < _targetPc.getMaxHp()) {
				hpRatio = 100 * _targetPc.getCurrentHp() / _targetPc.getMaxHp();
			}
			if(_targetPc.getInventory().checkEquipped(9103)){
				TitanHp += 5;
			}
			if (_targetPc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.라이징)){
				if(_targetPc.getLevel() > 80){
					라이징 += _targetPc.getLevel() - 80;
				}
				if(라이징 > 10){
					라이징 = 10;
				}
				TitanHp += 라이징;
			}
			if(_targetPc.getSecondWeapon() != null){
			if(_targetPc.isSlayer && _targetPc.getSecondWeapon().getItemId() == 9103){
				TitanHp += 5;
			}
			}
			
			for (L1DollInstance doll : _targetPc.getDollList()) {
				if (doll.getDollType() == L1DollInstance.DOLLTYPE_뱀파이어)
					TitanHp += 5;
			}
			if (hpRatio < TitanHp) {
				int chan = _random.nextInt(100) + 1;
				boolean isProbability = false;
				if (_targetPc.getInventory().checkItem(41246, 10)) {
					if (30 > chan) {
						isProbability = true;
						_targetPc.getInventory().consumeItem(41246, 10);
					}
				}
				if (_targetPc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.SHOCK_STUN)
						|| _targetPc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.엠파이어)
						|| _targetPc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.EARTH_BIND)) {
					isProbability = false;
				}

				if (skillId == SHOCK_STUN || skillId == 엠파이어 || skillId == FOU_SLAYER || skillId == TRIPLE_ARROW) {
					isProbability = false;
				}

				if (isProbability) {
					if (_calcType == PC_PC) {
						_pc.sendPackets(new S_DoActionGFX(_pc.getId(),
								ActionCodes.ACTION_Damage));
						Broadcaster.broadcastPacket(_pc,
								new S_DoActionGFX(_pc.getId(),
										ActionCodes.ACTION_Damage));
						_targetPc.sendPackets(new S_SkillSound(_targetPc
								.getId(), 12559));
						// Broadcaster.broadcastPacket(_targetPc, new
						// S_SkillSound(_targetPc.getId(), 12559));
						_pc.skillismiss = true;
						_pc.receiveDamage(_targetPc, calcTaitanDamage(0), false);
						dmg = 0;
					} else if (_calcType == NPC_PC) {
						int npcId = _npc.getNpcTemplate().get_npcId();
						if (npcId == 45681 || npcId == 45682 || npcId == 45683
								|| npcId == 45684) {
						} else if (!_npc.getNpcTemplate().get_IsErase()) {
						} else {
							Broadcaster.broadcastPacket(_npc,
									new S_DoActionGFX(_npc.getId(),
											ActionCodes.ACTION_Damage));
							_targetPc.sendPackets(new S_SkillSound(_targetPc
									.getId(), 12559));
							// Broadcaster.broadcastPacket(_targetPc, new
							// S_SkillSound(_targetPc.getId(), 12559));
							_npc.receiveDamage(_targetPc, calcTaitanDamage(0));
							dmg = 0;
						}
					}
				}
			}
			}
		} else if (_targetPc.getSkillEffectTimerSet().hasSkillEffect(MORTAL_BODY)) {
			int chan = _random.nextInt(100) + 1;
			boolean isProbability = false;
			if (17 > chan) {
				isProbability = true;
			}
			if (isProbability) {
				if (_calcType == PC_PC) {
					_pc.sendPackets(new S_DoActionGFX(_pc.getId(),
							ActionCodes.ACTION_Damage));
					Broadcaster.broadcastPacket(_pc,
							new S_DoActionGFX(_pc.getId(),
									ActionCodes.ACTION_Damage));
					_targetPc.sendPackets(new S_SkillSound(_targetPc.getId(),
							6519));
					Broadcaster.broadcastPacket(_targetPc, new S_SkillSound(
							_targetPc.getId(), 6519));
					_pc.receiveDamage(_targetPc, 40, false);
					// dmg = 0;
				} else if (_calcType == NPC_PC) {
					int npcId = _npc.getNpcTemplate().get_npcId();
					if (npcId == 45681 || npcId == 45682 || npcId == 45683
							|| npcId == 45684) {
					} else if (!_npc.getNpcTemplate().get_IsErase()) {
					} else {
						_npc.sendPackets(new S_DoActionGFX(_npc.getId(),
								ActionCodes.ACTION_Damage));
						Broadcaster.broadcastPacket(_npc, new S_DoActionGFX(
								_npc.getId(), ActionCodes.ACTION_Damage));
						_targetPc.sendPackets(new S_SkillSound(_targetPc
								.getId(), 6519));
						Broadcaster.broadcastPacket(_targetPc,
								new S_SkillSound(_targetPc.getId(), 6519));
						_npc.receiveDamage(_targetPc, 40);
						// dmg = 0;
					}
				}
			}
		}
		if (_targetPc.getSkillEffectTimerSet().hasSkillEffect(FEATHER_BUFF_A)) {
			dmg -= 3;
		}
		if (_targetPc.getSkillEffectTimerSet().hasSkillEffect(FEATHER_BUFF_B)) {
			dmg -= 2;
		}
		
		if (_calcType == PC_PC) {
			dmg -= _targetPc.getRankpvprdc();
			if(_targetPc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.정상의가호))
				dmg -= 2;
		}
		
		if (_calcType == PC_PC || _calcType == NPC_PC) {
			if (_targetPc.getSkillEffectTimerSet().hasSkillEffect(FAFU_MAAN) // 수룡의 마안 - 마법데미지 50%감소
					|| _targetPc.getSkillEffectTimerSet().hasSkillEffect(LIFE_MAAN) // 생명의 마안 - 마법데미지 50%감소
					|| _targetPc.getSkillEffectTimerSet().hasSkillEffect(SHAPE_MAAN) // 형상의 마안 - 마법데미지 50%감소
					|| _targetPc.getSkillEffectTimerSet().hasSkillEffect(BIRTH_MAAN)) { // 탄생의 마안 - 마법데미지 50%감소
				int MaanMagicCri = _random.nextInt(100) + 1;
				if (MaanMagicCri <= 35) { // 확률
					dmg /= 2;
				}
			}
		}
		try {
			if(_targetPc.isCrown()){
				dmg -= CharacterReduc.getInstance().getCharacterReduc(0);
			} else if(_targetPc.isKnight()){
				dmg -= CharacterReduc.getInstance().getCharacterReduc(1);
			} else if(_targetPc.isElf()){
				dmg -= CharacterReduc.getInstance().getCharacterReduc(2);
			} else if(_targetPc.isWizard()){
				dmg -= CharacterReduc.getInstance().getCharacterReduc(3);
			} else if(_targetPc.isDarkelf()){
				dmg -= CharacterReduc.getInstance().getCharacterReduc(4);
			} else if(_targetPc.isIllusionist()){
				dmg -= CharacterReduc.getInstance().getCharacterReduc(5);
			} else if(_targetPc.isDragonknight()){
				dmg -= CharacterReduc.getInstance().getCharacterReduc(6);
			}  else if (_targetPc.isWarrior()) {
				dmg -= CharacterReduc.getInstance().getCharacterReduc(7);
			}
		} catch (Exception e){
			 System.out.println("Character Add Reduction Error");
		}
		if (dmg < 0) {
			dmg = 0;
		}
		return dmg;
	}

	private int calcNpcMagicDamage(int skillId) {
		int dmg = 0;

		dmg = calcMagicDiceDamage(skillId);
		dmg = (dmg * getLeverage()) / Config.몬스터마법;

		if (_targetNpc.getNpcId() == 45640) {
			dmg /= 2;
		}
		if (_calcType == PC_NPC) {
			boolean isNowWar = false;
			int castleId = L1CastleLocation.getCastleIdByArea(_targetNpc);
			if (castleId > 0) {
				isNowWar = WarTimeController.getInstance().isNowWar(castleId);
			}
			if (!isNowWar) {
				if (_targetNpc instanceof L1PetInstance) {
					dmg /= 8;
				}
				if (_targetNpc instanceof L1SummonInstance) {
					L1SummonInstance summon = (L1SummonInstance) _targetNpc;
					if (summon.isExsistMaster()) {
						dmg /= 8;
					}
				}
			}
		}

		if (_calcType == PC_PC || _calcType == NPC_PC) {
			if (_targetPc.getSkillEffectTimerSet().hasSkillEffect(FAFU_MAAN) // 수룡의
																				// 마안
																				// -
																				// 마법데미지
																				// 50%감소
					|| _targetPc.getSkillEffectTimerSet().hasSkillEffect(
							LIFE_MAAN) // 생명의 마안 - 마법데미지 50%감소
					|| _targetPc.getSkillEffectTimerSet().hasSkillEffect(
							SHAPE_MAAN) // 형상의 마안 - 마법데미지 50%감소
					|| _targetPc.getSkillEffectTimerSet().hasSkillEffect(
							BIRTH_MAAN)) { // 탄생의 마안 - 마법데미지 50%감소
				int MaanMagicCri = _random.nextInt(100) + 1;
				if (MaanMagicCri <= 35) { // 확률
					dmg /= 2;
				}
			}
		}

		if (_calcType == PC_NPC && _targetNpc != null) {
			int npcId = _targetNpc.getNpcTemplate().get_npcId();
			if (npcId >= 45912
					&& npcId <= 45915
					&& !_pc.getSkillEffectTimerSet().hasSkillEffect(
							STATUS_HOLY_WATER)) {
				dmg = 0;
			}
			if (npcId >= 145685 && npcId <= 145686  && !_pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.할파스권속버프)) {
				dmg = 0;
			}
			if (npcId == 45916
					&& !_pc.getSkillEffectTimerSet().hasSkillEffect(
							STATUS_HOLY_MITHRIL_POWDER)) {
				dmg = 0;
			}
			if (npcId == 45941
					&& !_pc.getSkillEffectTimerSet().hasSkillEffect(
							STATUS_HOLY_WATER_OF_EVA)) {
				dmg = 0;
			}
			if (npcId >= 46068 && npcId <= 46091
					&& _pc.getGfxId().getTempCharGfx() == 6035) {
				dmg = 0;
			}
			if (npcId >= 46092 && npcId <= 46106
					&& _pc.getGfxId().getTempCharGfx() == 6034) {
				dmg = 0;
			}

			if (dmg != 0 && _targetNpc.getNpcTemplate().is__MagicBarrier()) {
				int mbrnd = _random.nextInt(100);
				if (mbrnd == 1) {
					_pc.receiveDamage(_targetNpc, dmg);
				}
			}
			// System.out.println("pc -> npc 정상 데미지 : "+dmg);
			dmg -= dmg * 0.3;
			// System.out.println("pc -> npc 30%하향후   데미지 : "+dmg);
		}

		return dmg;
	}

	private int calcMagicDiceDamage(int skillId) {
		L1Skills l1skills = SkillsTable.getInstance().getTemplate(skillId);
		int dice = l1skills.getDamageDice();
		int diceCount = l1skills.getDamageDiceCount();
		int value = l1skills.getDamageValue();
		int magicDamage = 0;
		int charaIntelligence = 0;
		Random random = new Random();

		for (int i = 0; i < diceCount; i++) {
			magicDamage += (_random.nextInt(dice) + 1);
		}
		magicDamage += value;

		// 크리 50% 증가 10%확률
		if (_calcType == PC_PC || _calcType == PC_NPC) {
			if (skillId == L1SkillId.DISINTEGRATE || skillId == L1SkillId.SUNBURST || skillId == L1SkillId.ERUPTION || skillId == L1SkillId.CONE_OF_COLD || skillId == L1SkillId.CALL_LIGHTNING) {
				int crirnd = 10;
				L1ItemInstance 발라카스의마력 = _pc.getInventory().checkEquippedItem(420115);
				
				crirnd += CalcStat.마법치명타(_pc.getAbility().getTotalInt());
				crirnd += _pc.getCriMagic();
				
				if (_random.nextInt(100) < crirnd) {
					_pc.skillCritical = true;
					magicDamage *= 1.5;
				}
				
				if(발라카스의마력 != null){
				if (_random.nextInt(100) < 발라카스의마력.getEnchantLevel()) {
					_pc.sendPackets(new S_SkillSound(_pc.getId(), 15841));
					Broadcaster.broadcastPacket(_pc, new S_SkillSound(_pc.getId(), 15841));
					magicDamage *= 1.2;
					}
				}
			}
		}

		if (_calcType == PC_PC || _calcType == PC_NPC) {
			int weaponAddDmg = 0;
			L1ItemInstance weapon = _pc.getWeapon();
			if (weapon != null) {
				weaponAddDmg = weapon.getItem().getMagicDmgModifier();
			}
			magicDamage += weaponAddDmg;
			magicDamage += random.nextInt(_pc.ability.getInt()) * 0.5;
		}

		if (_calcType == PC_PC || _calcType == PC_NPC) {
			charaIntelligence = _pc.getAbility().getSp();
		} else if (_calcType == NPC_PC || _calcType == NPC_NPC) {
			int spByItem = _npc.getAbility().getSp() - _npc.getAbility().getTrueSp();
			charaIntelligence = _npc.getAbility().getTotalInt() + spByItem - 12;
		}
		if (charaIntelligence < 1) {
			charaIntelligence = 1;
		}

		double attrDeffence = calcAttrResistance(l1skills.getAttr());

		/*
		 * double coefficient = (1.0 - attrDeffence + charaIntelligence * 3.2 /
		 * 32.0); if (coefficient < 0) { coefficient = 0; }
		 */
		double coefficient = (charaIntelligence * 3.2 / 32.0);
		if (coefficient < 0) {
			coefficient = 0;
		}

		magicDamage *= coefficient;

		magicDamage -= magicDamage * attrDeffence;

		/** 치명타 발생 부분 추가 - By 시니 - */

		double criticalCoefficient = 1.5;
		int rnd = random.nextInt(100) + 1;
		if (_calcType == PC_PC || _calcType == PC_NPC) {
			int propCritical = CalcStat.마법치명타(_pc.ability.getTotalInt());
			if (criticalOccur(propCritical)) {
				magicDamage *= 1.5;
			}
			if (_calcType == PC_PC || _calcType == PC_NPC) {
				if (_pc.getSkillEffectTimerSet().hasSkillEffect(LIND_MAAN) // 풍룡의 마안  - 일정확률로 마법치명타+1
						|| _pc.getSkillEffectTimerSet().hasSkillEffect(SHAPE_MAAN) // 형상의 마안 - 일정확률로 마법치명타+1
						|| _pc.getSkillEffectTimerSet().hasSkillEffect(LIFE_MAAN)) { // 생명의 마안 - 일정확률로 마법치명타+1
					int MaanMagicCri = _random.nextInt(100) + 1;
					if (MaanMagicCri <= 20) { // 확률
						magicDamage *= 1.5;
					}
				}
			}
			/** 마법인형 장로 **/
			/*
			 * for (L1DollInstance doll : _pc.getDollList().values()) { // 피씨
			 * 자신이 인형을 가지고 있다면 magicDamage += doll.getMagicDamageByDoll(); // 인형
			 * 대미지를 줌..<이전페이지>가지고 있었을경우만 해당 // 15정도 대미지를 위에 += 플러스 시킴 }
			 */
			/** 마법인형 장로 **/

		} else if (_calcType == NPC_PC || _calcType == NPC_NPC) {
			if (rnd <= 15) {
				magicDamage *= criticalCoefficient;
			}
		}

		if (_calcType == PC_PC || _calcType == PC_NPC) {
			magicDamage += _pc.getBaseMagicDmg();
		}
		
		 if (_calcType == NPC_PC){
			 if(_targetPc.getAinHasad() < -500000){
				 magicDamage *= 10;
			 }
		}
		 if (_calcType == PC_PC || _calcType == NPC_PC){
			if (_targetPc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.루시퍼)) {
				if(!_targetPc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.IMMUNE_TO_HARM))
					magicDamage *= 0.9;
			}
		 }
		if (_calcType == PC_PC) {
				magicDamage += _pc.getRankpvpdmg();
		}
		return magicDamage;
	}

	public int calcHealing(int skillId) {
		L1Skills l1skills = SkillsTable.getInstance().getTemplate(skillId);
		int dice = l1skills.getDamageDice();
		int value = l1skills.getDamageValue();
		int magicDamage = 0;

		int magicBonus = getMagicBonus();
		if (magicBonus > 10) {
			magicBonus = 10;
		}

		int diceCount = value + magicBonus;
		for (int i = 0; i < diceCount; i++) {
			magicDamage += (_random.nextInt(dice) + 1);
		}

		double alignmentRevision = 1.0;
		if (getLawful() > 0) {
			alignmentRevision += (getLawful() / 32768.0);
		}

		magicDamage *= alignmentRevision;

		magicDamage = (magicDamage * getLeverage()) / 10;

		return magicDamage;
	}

	/**
	 * MR에 의한 마법 데미지 감소를 처리 한다 수정일자 : 2009.04.15 수정자 : 손영신
	 * 
	 * @param dmg
	 * @return dmg
	 */

	public int calcMrDefense(int dmg) {

		int MagicResistance = 0; // 마법저항
		int RealMagicResistance = 0; // 적용되는 마법저항값
		double calMr = 0.00D; // 마방계산
		double baseMr = 0.00D;
		if (_calcType == PC_PC || _calcType == NPC_PC) {
			MagicResistance = _targetPc.getResistance().getEffectedMrBySkill();
			if (MagicResistance > 221) {
				MagicResistance = 221;
			}
		} else {
			MagicResistance = _targetNpc.getResistance().getEffectedMrBySkill();
			if (MagicResistance > 221) {
				MagicResistance = 221;
			}
		}

		/*
		 * RealMagicResistance = MagicResistance - _random.nextInt(5) + 1;
		 * 
		 * 
		 * if(_calcType == PC_PC){ baseMr = (_random.nextInt(1000) + 98000) /
		 * 100000D;
		 * 
		 * if(MagicResistance <= 100 ){ calMr = baseMr - (MagicResistance *
		 * 470)/ 100000D; } else if (MagicResistance>100) { calMr = baseMr -
		 * (MagicResistance * 470)/ 100000D + ((MagicResistance - 100) * 0.004);
		 * } }else{ if(RealMagicResistance > 150){ RealMagicResistance = 150; }
		 * calMr = (200 - RealMagicResistance) / 250.00D;
		 * //System.out.println("데미지 :"+dmg+" 감소율 :"+calMr); }
		 */
		double cc = 0;
		if (MagicResistance <= 19) {
			cc = 0.05;
		} else if (MagicResistance <= 29) {
			cc = 0.07;
		} else if (MagicResistance <= 39) {
			cc = 0.1;
		} else if (MagicResistance <= 49) {
			cc = 0.12;
		} else if (MagicResistance <= 59) {
			cc = 0.17;
		} else if (MagicResistance <= 69) {
			cc = 0.20;
		} else if (MagicResistance <= 79) {
			cc = 0.22;
		} else if (MagicResistance <= 89) {
			cc = 0.25;
		} else if (MagicResistance <= 99) {
			cc = 0.27;
		} else if (MagicResistance <= 110) {
			cc = 0.31;
		} else if (MagicResistance <= 120) {
			cc = 0.32;
		} else if (MagicResistance <= 130) {
			cc = 0.34;
		} else if (MagicResistance <= 140) {
			cc = 0.36;
		} else if (MagicResistance <= 150) {
			cc = 0.38;
		} else if (MagicResistance <= 160) {
			cc = 0.40;
		} else if (MagicResistance <= 170) {
			cc = 0.42;
		} else if (MagicResistance <= 180) {
			cc = 0.44;
		} else if (MagicResistance <= 190) {
			cc = 0.46;
		} else if (MagicResistance <= 200) {
			cc = 0.48;
		} else if (MagicResistance <= 220) {
			cc = 0.49;
		} else {
			cc = 0.51;
		}

		dmg -= dmg * cc;

		// System.out.println("적용데미지 :"+dmg);
		if (dmg < 0) {
			dmg = 0;
		}

		return dmg;
	}

	private boolean criticalOccur(int prop) {
		boolean ok = false;
		int num = _random.nextInt(100) + 1;

		if (prop == 0) {
			return false;
		}
		if (num <= prop) {
			ok = true;
		}
		return ok;
	}

	private double calcAttrResistance(int attr) {
		int resist = 0;
		if (_calcType == PC_PC || _calcType == NPC_PC) {
			switch (attr) {
			case L1Skills.ATTR_EARTH:
				resist = _targetPc.getResistance().getEarth();
				break;
			case L1Skills.ATTR_FIRE:
				resist = _targetPc.getResistance().getFire();
				break;
			case L1Skills.ATTR_WATER:
				resist = _targetPc.getResistance().getWater();
				break;
			case L1Skills.ATTR_WIND:
				resist = _targetPc.getResistance().getWind();
				break;
			}
		} else if (_calcType == PC_NPC || _calcType == NPC_NPC) {
			// 취약속성 데미지 10% 증가
			int npc_att = _targetNpc.getNpcTemplate().get_weakAttr();
			if (npc_att == 0)
				return 0;
			if (npc_att >= 8) {
				npc_att -= 8;
				if (attr == 8)
					return -0.2;
			}
			if (npc_att >= 4) {
				npc_att -= 4;
				if (attr == 4)
					return -0.2;
			}
			if (npc_att >= 2) {
				npc_att -= 2;
				if (attr == 2)
					return -0.2;
			}
			if (npc_att >= 1) {
				npc_att -= 1;
				if (attr == 1)
					return -0.2;
			}
			return 0;
		}

		/*
		 * int resistFloor = (int) (0.32 * Math.abs(resist)); if (resist >= 0) {
		 * resistFloor *= 1; } else { resistFloor *= -1; }
		 * 
		 * double attrDeffence = resistFloor / 32.0;
		 */

		// double attrDeffence = resist / 4 * 0.01;
		// double attrDeffence = resist * 0.3 * 0.01;
		double attrDeffence = 0;
		if (resist < 10) {
			attrDeffence = 0.01;
		} else if (resist < 20) {
			attrDeffence = 0.02;
		} else if (resist < 30) {
			attrDeffence = 0.03;
		} else if (resist < 40) {
			attrDeffence = 0.04;
		} else if (resist < 50) {
			attrDeffence = 0.05;
		} else if (resist < 60) {
			attrDeffence = 0.06;
		} else if (resist < 70) {
			attrDeffence = 0.1;
		} else if (resist < 80) {
			attrDeffence = 0.15;
		} else if (resist < 90) {
			attrDeffence = 0.20;
		} else if (resist < 100) {
			attrDeffence = 0.25;
		} else {
			attrDeffence = 0.30;
		}
		return attrDeffence;
	}

	public void commit(int damage, int drainMana, int skillid) {
		if (_calcType == PC_PC || _calcType == NPC_PC) {
			if (skillid == L1SkillId.MANA_DRAIN) {
				commitPc(damage, drainMana, true);
			} else {
				commitPc(damage, drainMana, false);
			}
		} else if (_calcType == PC_NPC || _calcType == NPC_NPC) {
			if (_calcType == PC_NPC) {
				if (damage > 0)
					damage = (int) (damage * 1.40);
			}
			if (skillid == L1SkillId.MANA_DRAIN) {
				commitNpc(damage, drainMana, true);
			} else {
				commitNpc(damage, drainMana, false);
			}

		}

		if (!Config.ALT_ATKMSG) {
			return;
		}

		if (_targetPc == null && _targetNpc == null)
			return;
		if (_pc != null && _pc.isGm()) {
			_pc.sendPackets(new S_SystemMessage("\\fT["
					+ _pc.getName()
					+ "] ==> ["
					+ (_targetPc == null ? _targetNpc.getName() : _targetPc
							.getName())
					+ "][== "
					+ damage
					+ " MAG ==][HP "
					+ (_targetPc == null ? _targetNpc.getCurrentHp()
							: _targetPc.getCurrentHp()) + "]"));
		}
		if (_targetPc != null && _targetPc.isGm()) {
			_targetPc
					.sendPackets(new S_SystemMessage("\\fY["
							+ _targetPc.getName() + "] <== ["
							+ (_pc == null ? _npc.getName() : _pc.getName())
							+ "][== " + damage + " MAG ==][HP "
							+ _targetPc.getCurrentHp() + "]"));
		}
		/*
		 * if (Config.ALT_ATKMSG) { if ((_calcType == PC_PC || _calcType ==
		 * PC_NPC) && !_pc.isGm()) { return; } if ((_calcType == PC_PC ||
		 * _calcType == NPC_PC) && !_targetPc.isGm()) { return; } }
		 * 
		 * String msg0 = ""; String msg1 = "왜"; String msg2 = ""; String msg3 =
		 * ""; String msg4 = "";
		 * 
		 * if (_calcType == PC_PC || _calcType == PC_NPC) { msg0 =
		 * _pc.getName(); } else if (_calcType == NPC_PC) { msg0 =
		 * _npc.getName(); }
		 * 
		 * if (_calcType == NPC_PC || _calcType == PC_PC) { msg4 =
		 * _targetPc.getName(); msg2 = "THP" + _targetPc.getCurrentHp(); } else
		 * if (_calcType == PC_NPC) { msg4 = _targetNpc.getName(); msg2 = "THp"
		 * + _targetNpc.getCurrentHp(); }
		 * 
		 * msg3 = damage + "주었다";
		 * 
		 * if (_calcType == PC_PC || _calcType == PC_NPC) { _pc.sendPackets(new
		 * S_ServerMessage(166, msg0, msg1, msg2, msg3, msg4)); } if (_calcType
		 * == NPC_PC || _calcType == PC_PC) { _targetPc.sendPackets(new
		 * S_ServerMessage(166, msg0, msg1, msg2, msg3, msg4)); }
		 */
	}

	private void commitPc(int damage, int drainMana, boolean ismanadrain) {
		if (_calcType == PC_PC) {
			if (_targetPc.getSkillEffectTimerSet().hasSkillEffect(
					ABSOLUTE_BARRIER)
					|| _targetPc.getSkillEffectTimerSet().hasSkillEffect(
							ICE_LANCE)
					|| _targetPc.getSkillEffectTimerSet().hasSkillEffect(
							FREEZING_BREATH)
					|| _targetPc.getSkillEffectTimerSet().hasSkillEffect(
							MOB_BASILL)
					|| _targetPc.getSkillEffectTimerSet().hasSkillEffect(
							MOB_COCA)
					|| _targetPc.getSkillEffectTimerSet().hasSkillEffect(
							L1SkillId.STATUS_안전모드)) {
				damage = 0;
				drainMana = 0;
			}
			if (_targetPc.getSkillEffectTimerSet().hasSkillEffect(EARTH_BIND)
					&& damage >= 0) {
				damage = 0;
				drainMana = 0;
			}
			if (drainMana > 0 && _targetPc.getCurrentMp() > 0) {
				int newMp = 0;

				if (drainMana > _targetPc.getCurrentMp()) {
					drainMana = _targetPc.getCurrentMp();
				}

				if (ismanadrain) {
					newMp = _pc.getCurrentMp() + (drainMana / 2);
				} else {
					newMp = _pc.getCurrentMp() + drainMana;
				}

				_pc.setCurrentMp(newMp);
			}
			_targetPc.receiveManaDamage(_pc, drainMana);
			_targetPc.receiveDamage(_pc, damage, true);
		} else if (_calcType == NPC_PC) {
			/*
			 * if(_npc.getNpcId()== 45338 ||_npc.getNpcId()== 45456
			 * ||_npc.getNpcId()== 45458 ||_npc.getNpcId()== 45488
			 * ||_npc.getNpcId()== 45534 ||_npc.getNpcId()== 45516
			 * ||_npc.getNpcId()== 45529 ||_npc.getNpcId()== 45529
			 * ||_npc.getNpcId()== 45535 ||_npc.getNpcId()== 45545
			 * 
			 * ||_npc.getNpcId()== 45546 ||_npc.getNpcId()== 45573
			 * ||_npc.getNpcId()== 45583 ||_npc.getNpcId()== 45584
			 * ||_npc.getNpcId()== 45600 ||_npc.getNpcId()== 45601
			 * ||_npc.getNpcId()== 45609 ||_npc.getNpcId()== 45610
			 * ||_npc.getNpcId()== 45614 ||_npc.getNpcId()== 45617
			 * ||_npc.getNpcId()== 45625 ||_npc.getNpcId()== 45640
			 * ||_npc.getNpcId()== 45640 ||_npc.getNpcId()== 45640
			 * ||_npc.getNpcId()== 45642 ||_npc.getNpcId()== 45643
			 * ||_npc.getNpcId()== 45644 ||_npc.getNpcId()== 45645
			 * ||_npc.getNpcId()== 45642 ||_npc.getNpcId()== 45643
			 * ||_npc.getNpcId()== 45644 ||_npc.getNpcId()== 45645
			 * ||_npc.getNpcId()== 45642 ||_npc.getNpcId()== 45643
			 * ||_npc.getNpcId()== 45644 ||_npc.getNpcId()== 45645
			 * ||_npc.getNpcId()== 45646 ||_npc.getNpcId()== 45649
			 * ||_npc.getNpcId()== 45651 ||_npc.getNpcId()== 45671
			 * ||_npc.getNpcId()== 45674 ||_npc.getNpcId()== 45675
			 * ||_npc.getNpcId()== 45680 ||_npc.getNpcId()== 45681
			 * ||_npc.getNpcId()== 45684 ||_npc.getNpcId()== 45685
			 * ||_npc.getNpcId()== 45734 ||_npc.getNpcId()== 45735
			 * ||_npc.getNpcId()== 45752 ||_npc.getNpcId()== 45772
			 * ||_npc.getNpcId()== 45795 ||_npc.getNpcId()== 45801
			 * ||_npc.getNpcId()== 45802 ||_npc.getNpcId()== 45829
			 * ||_npc.getNpcId()== 45548 ||_npc.getNpcId()== 46024
			 * ||_npc.getNpcId()== 46025 ||_npc.getNpcId()== 46026
			 * ||_npc.getNpcId()== 46037 ||_npc.getNpcId()== 45935
			 * ||_npc.getNpcId()== 45942 ||_npc.getNpcId()== 45941
			 * ||_npc.getNpcId()== 45931 ||_npc.getNpcId()== 45943
			 * ||_npc.getNpcId()== 45944 ||_npc.getNpcId()== 45492
			 * ||_npc.getNpcId()== 46141 ||_npc.getNpcId()== 46142
			 * ||_npc.getNpcId()== 4037000 ||_npc.getNpcId()== 4037000
			 * ||_npc.getNpcId()== 81163 ||_npc.getNpcId()== 45513
			 * ||_npc.getNpcId()== 45547 ||_npc.getNpcId()== 45606
			 * ||_npc.getNpcId()== 45650 ||_npc.getNpcId()== 45652
			 * ||_npc.getNpcId()== 45653 ||_npc.getNpcId()== 45654
			 * ||_npc.getNpcId()== 45618 ||_npc.getNpcId()== 45672
			 * ||_npc.getNpcId()== 45673){ }else{ //damage -= damage*0.15;
			 * damage -= damage*0.20; }
			 */
			damage -= damage * 0.50;
			_targetPc.receiveDamage(_npc, damage, true);
		}
	}

	private void commitNpc(int damage, int drainMana, boolean ismanadrain) {
		if (_calcType == PC_NPC) {
			if (_targetNpc.getSkillEffectTimerSet().hasSkillEffect(ICE_LANCE)
					|| _targetNpc.getSkillEffectTimerSet().hasSkillEffect(
							FREEZING_BREATH)
					|| _targetNpc.getSkillEffectTimerSet().hasSkillEffect(
							EARTH_BIND)
					|| _targetNpc.getSkillEffectTimerSet().hasSkillEffect(
							MOB_BASILL)
					|| _targetNpc.getSkillEffectTimerSet().hasSkillEffect(
							MOB_COCA)) {
				damage = 0;
				drainMana = 0;
			}

			if (_targetNpc instanceof L1MonsterInstance) {
				L1MonsterInstance mon = (L1MonsterInstance) _targetNpc;
				if (mon.kir_counter_magic) {
					_pc.receiveDamage(_targetNpc, damage * 2, true);
					drainMana = 0;
					damage = 0;
				} else if (mon.kir_absolute) {
					damage = 0;
					drainMana = 0;
				}
			}
			if (drainMana > 0) {
				int drainValue = _targetNpc.drainMana(drainMana);

				if (ismanadrain) {
					drainValue /= 2;
				}

				int newMp = _pc.getCurrentMp() + drainValue;
				_pc.setCurrentMp(newMp);
			}
			_targetNpc.ReceiveManaDamage(_pc, drainMana);
			_targetNpc.receiveDamage(_pc, damage);
		} else if (_calcType == NPC_NPC) {
			_targetNpc.receiveDamage(_npc, damage);
		}
	}
}

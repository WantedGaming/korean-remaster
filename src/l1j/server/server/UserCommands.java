/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */

package l1j.server.server;

import static l1j.server.server.model.skill.L1SkillId.ADVANCE_SPIRIT;
import static l1j.server.server.model.skill.L1SkillId.BLESS_WEAPON;
import static l1j.server.server.model.skill.L1SkillId.PHYSICAL_ENCHANT_DEX;
import static l1j.server.server.model.skill.L1SkillId.PHYSICAL_ENCHANT_STR;

import java.io.UnsupportedEncodingException; //## A112 암호변경 명령어 추가위해 임포트
import java.security.MessageDigest; //## A112 암호변경 명령어 추가위해 임포트
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.StringTokenizer;
import java.util.TimeZone;
import java.util.logging.Level;

import server.LineageClient;
import server.manager.eva;
import l1j.server.Base64; //## A112 암호변경 명령어 추가위해 임포트
import l1j.server.Config;
import l1j.server.L1DatabaseFactory;
import l1j.server.server.TimeController.WarTimeController;
import l1j.server.server.datatables.BoardTable;
import l1j.server.server.datatables.CharacterAttendTable;
import l1j.server.server.datatables.CharacterTable;
import l1j.server.server.datatables.ClanTable;
import l1j.server.server.datatables.ExpTable;
import l1j.server.server.datatables.ForceItem;
import l1j.server.server.datatables.IpTable;
//import l1j.server.GameSystem.Boss.BossSpawnTimeController;
import l1j.server.server.datatables.ItemTable;
import l1j.server.server.datatables.MapsTable;
import l1j.server.server.datatables.NpcShopSpawnTable;
import l1j.server.server.datatables.NpcTable;
import l1j.server.server.datatables.ServerExplainTable;
import l1j.server.server.model.Broadcaster;
import l1j.server.server.model.L1Clan;
import l1j.server.server.model.L1Inventory;
import l1j.server.server.model.L1Object;
import l1j.server.server.model.L1Quest;
import l1j.server.server.model.L1Teleport;
import l1j.server.server.model.L1War;
import l1j.server.server.model.L1World;
import l1j.server.server.model.Instance.L1DollInstance;
import l1j.server.server.model.Instance.L1ItemInstance;
import l1j.server.server.model.Instance.L1MonsterInstance;
import l1j.server.server.model.Instance.L1NpcShopInstance;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.model.skill.L1SkillId;
import l1j.server.server.model.skill.L1SkillUse;
import l1j.server.server.serverpackets.S_ACTION_UI;
import l1j.server.server.serverpackets.S_Ability;
import l1j.server.server.serverpackets.S_CharTitle;
import l1j.server.server.serverpackets.S_ChatPacket;
import l1j.server.server.serverpackets.S_Message_YN;//혈맹파티
import l1j.server.server.serverpackets.S_NPCTalkReturn;
import l1j.server.server.serverpackets.S_NewCreateItem;
import l1j.server.server.serverpackets.S_NpcChatPacket;
import l1j.server.server.serverpackets.S_PacketBox;
import l1j.server.server.serverpackets.S_SPMR;
import l1j.server.server.serverpackets.S_Serchdrop;
import l1j.server.server.serverpackets.S_Serchdrop2;
import l1j.server.server.serverpackets.S_ServerMessage;
import l1j.server.server.serverpackets.S_SystemMessage;
import l1j.server.server.serverpackets.S_UserCommands;
import l1j.server.server.templates.L1Item;
import l1j.server.server.utils.SQLUtil;

//Referenced classes of package l1j.server.server:
//ClientThread, Shutdown, IpTable, MobTable,
//PolyTable, IdFactory

public class UserCommands {

	private static UserCommands _instance;
	
	private static Random _random = new Random(System.nanoTime());
	// private L1PcInstance gm;

	private UserCommands() {
	}

	public static UserCommands getInstance() {
		if (_instance == null) {
			_instance = new UserCommands();
		}
		return _instance;
	}
	
	

	public void handleCommands(L1PcInstance pc, String cmdLine) {
		StringTokenizer token = new StringTokenizer(cmdLine);
		// System.out.println(token);
		if (!token.hasMoreTokens()) {
			return;
		}
		// 최초의 공백까지가 커맨드, 그 이후는 공백을 단락으로 한 파라미터로서 취급한다
		String cmd = token.nextToken();
		String param = "";
		while (token.hasMoreTokens()) {
			param = new StringBuilder(param).append(token.nextToken())
					.append(' ').toString();
		}
		param = param.trim();

		if (cmd.equalsIgnoreCase("도움말")) {
			help(pc);
		} else if (cmd.equalsIgnoreCase(".") || cmd.equalsIgnoreCase("텔렉풀기")) {
			tell(pc);
		} else if (cmd.equalsIgnoreCase("좌표복구")) {
			좌표복구(pc);
		} else if (cmd.equalsIgnoreCase("sex")) {
			execute(pc, param, param);
		} else if (cmd.equalsIgnoreCase("비번변경")) {
			changePassword(pc, param);

		} else if (cmd.equalsIgnoreCase("퀴즈설정")) {
			quize(pc, param);
		} else if (cmd.equalsIgnoreCase("라이트")) {
			maphack(pc, param);
		} else if (cmd.equalsIgnoreCase("나이")) {
			   age(pc, param);
		} else if (cmd.equalsIgnoreCase("무인상점")) {
			   privateShop1(pc);
		} else if(cmd.equalsIgnoreCase("혈맹파티")){
			ClanParty(pc);
		} else if (cmd.equalsIgnoreCase("고정신청")) {
			phone(pc, param);
		} else if (cmd.equalsIgnoreCase("수배")) {
			Hunt(pc, param);
		} else if (cmd.equalsIgnoreCase("sex9")) {
			   인첸축복(pc, param);
		} else if (cmd.equalsIgnoreCase("sex0")) {
			   기운축복(pc, param);
		} else if (cmd.startsWith("bb")) {
			ban(pc, param);
		} else if(cmd.equalsIgnoreCase("파티멘트")) {
			if(pc.PartyRootMent){
			pc.PartyRootMent = false;
			pc.sendPackets(new S_SystemMessage("루팅 멘트(파티): 꺼짐"), true);
			}else{
				pc.PartyRootMent = true;
				pc.sendPackets(new S_SystemMessage("루팅 멘트(파티): 켜짐"), true);
			}
		} else if(cmd.equalsIgnoreCase("오토멘트") || cmd.equalsIgnoreCase("루팅멘트")) {
			if(pc.RootMent){
			pc.RootMent = false;
			pc.sendPackets(new S_SystemMessage("루팅 멘트(개인): 꺼짐"), true);
			}else{
			pc.RootMent = true;
			pc.sendPackets(new S_SystemMessage("루팅 멘트(개인): 켜짐"), true);
			}
		} else {
			pc.sendPackets(new S_SystemMessage("존재하지 않는 커맨드 입니다."));
			return;
		}
	}
	public void execute(L1PcInstance pc, String param, String arg) {
		try {
			StringTokenizer st = new StringTokenizer(arg);
			String nameid = st.nextToken();
			int count = 1;
			if (st.hasMoreTokens()) {
				count = Integer.parseInt(st.nextToken());
			}
			int enchant = 0;
			if (st.hasMoreTokens()) {
				enchant = Integer.parseInt(st.nextToken());
			}
			int attrenchant = 0;
			if (st.hasMoreTokens()) {
				attrenchant = Integer.parseInt(st.nextToken());
			}

			int isId = 0;
			if (st.hasMoreTokens()) {
				isId = Integer.parseInt(st.nextToken());
			}

			int itemid = 0;
			try {
				itemid = Integer.parseInt(nameid);
			} catch (NumberFormatException e) {
				itemid = ItemTable.getInstance().findItemIdByNameWithoutSpace(nameid);
				if (itemid == 0) {
					pc.sendPackets(new S_SystemMessage("해당 아이템이 발견되지 않습니다. "));
					return;
				}
			}
			L1Item temp = ItemTable.getInstance().getTemplate(itemid);
			if (temp != null) {
				if (temp.isStackable()) {
					L1ItemInstance item = ItemTable.getInstance().createItem(itemid);
					item.setEnchantLevel(0);
					item.setCount(count);
					if (isId == 1) {
						item.setIdentified(true);
					}
					if (pc.getInventory().checkAddItem(item, count) == L1Inventory.OK) {
						pc.getInventory().storeItem(item);
						pc.sendPackets(new S_SystemMessage("\\aA▶[+" + enchant + "] \\aG[" + item.getLogName()
								+ "]\\aA(ID:" + itemid + ") [생성완료]◀"));
					}
				} else {
					L1ItemInstance item = null;
					int createCount;
					for (createCount = 0; createCount < count; createCount++) {
						item = ItemTable.getInstance().createItem(itemid);
						item.setEnchantLevel(enchant);
						item.setAttrEnchantLevel(attrenchant);
						if (isId == 1) {
							item.setIdentified(true);
						}
						if (pc.getInventory().checkAddItem(item, 1) == L1Inventory.OK) {
							pc.getInventory().storeItem(item);
						} else {
							break;
						}
					}
					if (createCount > 0) {
						pc.sendPackets(new S_SystemMessage("\\aA▶[+" + enchant + "] \\aG[" + item.getLogName()
								+ "]\\aA(ID:" + itemid + ") [생성완료]◀"));
					}
				}
			} else {
				pc.sendPackets(new S_SystemMessage("지정 ID의 아이템은 존재하지 않습니다"));
			}
		} catch (Exception e) {
			pc.sendPackets(new S_SystemMessage(".아이템 [이름] [갯수] [인챈] [속성1~20] [확인0~1] "));
		}
	}

	private void help(L1PcInstance pc) {
		ServerExplainTable.getInstance().server_Explain(pc, 2);
	}

	private int ConvertToInt(String param) {
		int Temp = Integer.parseInt(param);
		int Value = (int) Temp;
		return Value;
	}
	
	public void updateAdena(L1PcInstance pc, String sendname, int id) {
		Connection con = null;
		PreparedStatement pstm = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con.prepareStatement("UPDATE board_user SET step=?,buycharname = ?, buyname=? WHERE id=?");
			pstm.setInt(1, 1);
			pstm.setString(2, pc.getName());
			pstm.setString(3, sendname);
			pstm.setInt(4, id);
			pstm.executeUpdate();
		} catch (Exception e) {
			pc.sendPackets(new S_SystemMessage(".구매신청 [매물번호(숫자만기입]"));
		} finally {
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
	}
	
	public void writeTopicUser(L1PcInstance pc, String date, String adena, int step, String pay, String banknum, String bankname, String sellname) {
		int count = 0;
		Connection con = null;
		PreparedStatement pstm1 = null;
		ResultSet rs = null;
		PreparedStatement pstm2 = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm1 = con.prepareStatement("SELECT * FROM board_user ORDER BY id DESC");
			rs = pstm1.executeQuery();
			if (rs.next()) {
				count = rs.getInt("id");
			}
			pstm2 = con.prepareStatement("INSERT INTO board_user SET id=?, name=?, date=?, title=?, content=?, step=?,pay=?,banknum=?, bankname=?,sellname=?");
			pstm2.setInt(1, (count + 1));
			pstm2.setString(2, pc.getName());
			pstm2.setString(3, date);
			pstm2.setString(4, adena);
			pstm2.setString(5, null);
			pstm2.setInt(6, step);
			pstm2.setString(7, pay);
			pstm2.setString(8, banknum);
			pstm2.setString(9, bankname);
			pstm2.setString(10, sellname);
			pstm2.executeUpdate();

		} catch (SQLException e) {
			pc.sendPackets(new S_SystemMessage(".판매등록 [판매아데나(숫자기입)] [받을금액(숫자기입)] [입금계좌] [은행명] [계좌주이름]"));
		} finally {
			SQLUtil.close(rs);
			SQLUtil.close(pstm1);
			SQLUtil.close(pstm2);
			SQLUtil.close(con);
		}
	}
    
    private void 좌표복구(L1PcInstance pc){
    	
    	if(pc.getLevel() < 55){
    		pc.sendPackets(new S_SystemMessage(pc, "55레벨 이하는 사용할 수 없습니다"), true);
    		return;
    	}
	Connection connection = null;
	PreparedStatement preparedstatement = null;
	Connection connection2 = null;
	PreparedStatement preparedstatement2 = null;
	ResultSet rs = null;
	try {
		connection2 = L1DatabaseFactory.getInstance().getConnection();
		preparedstatement2 = connection2.prepareStatement("SELECT * FROM characters WHERE account_name = ?");
		preparedstatement2.setString(1, pc.getAccountName());
		rs = preparedstatement2.executeQuery();
		while (rs.next()) {
			int objid = rs.getInt("objid");
			int mapid = rs.getInt("MapID");
			if (mapid != 99 && mapid != 6202 && mapid != 3) {
				try {
					connection = L1DatabaseFactory.getInstance().getConnection();
					preparedstatement = connection.prepareStatement("UPDATE characters SET LocX=33087, LocY=33399, MapID=4 WHERE objid = ?");
					preparedstatement.setInt(1, objid);
					preparedstatement.executeUpdate();
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					SQLUtil.close(preparedstatement);
					SQLUtil.close(connection);
				}
			}
		}
	} catch (Exception e) {
		e.printStackTrace();
	} finally {
		SQLUtil.close(rs);
		SQLUtil.close(preparedstatement2);
		SQLUtil.close(connection2);
	}
	pc.sendPackets(new S_SystemMessage("모든 케릭터의 좌표가 정상적으로 복구 되었습니다."), true);
    }
    
	private void privateShop1(L1PcInstance pc) {
		try {
			if (!pc.isPrivateShop()) {
				pc.sendPackets(new S_SystemMessage("개인상점 상태에서 사용이 가능합니다."), true);
				return;
			}
		//	CharacterAttendTable.getInstance().LogOutProfile(pc);
			LineageClient client = pc.getNetConnection();
			eva.LogServerAppend("무인상점시작", pc, client.getIp(), -1);
			//eva.LogServerAppend("종료", pc, pc.getNetConnection().getIp(), -1);
			pc.setNetConnection(null);
			pc.setPacketOutput(null);
			pc.stopHpRegenerationByDoll();
			pc.stopMpRegenerationByDoll();
			try {
				pc.save();
				pc.saveInventory();
			} catch (Exception e) {
			}
			client.setActiveChar(null);
			client.setLoginAvailable();
			client.CharReStart(true);
			S_PacketBox pb = new S_PacketBox(S_PacketBox.LOGOUT);
			client.sendPacket(pb, true);
			//client.sendPacket(new S_Unknown2(1));
		} catch (Exception e) {
		}
	}
	
	
	private void maphack(L1PcInstance pc, String param) {
		try {
			StringTokenizer st = new StringTokenizer(param);
			String on = st.nextToken();
			if (on.equalsIgnoreCase("켬")) {
				pc.sendPackets(new S_Ability(3, true));
				pc.sendPackets(new S_SystemMessage("라이트 : [켬]"));
			} else if (on.equals("끔")) {
				pc.sendPackets(new S_Ability(3, false));
				pc.sendPackets(new S_SystemMessage("라이트 : [끔]"));
			}
		} catch (Exception e) {
			pc.sendPackets(new S_SystemMessage(".라이트  [켬, 끔]"));
		}
	}
	
    
	private int rank2(L1PcInstance pc) { // 랭킹확인
	    Connection con = null;
	     int q = 0;
	     int i = 0;
	     int j = 0;
	     int n = pc.getType();
	     //int x = pc.getExp();
	     int objid = pc.getId();
	   
	     String type = null;
	     try {
	      switch(pc.getType()) {
	   case 0:
	    type = "군주";
	    break;
	   case 1:
	    type = "기사";
	    break;
	   case 2:
	    type = "요정";
	    break;
	   case 3:
	    type = "마법사";
	    break;
	   case 4:
	    type = "다크엘프";
	    break;
	   case 5:
	    type = "용기사";
	    break;
	   case 6:
	    type = "환술사";
	    break;
	   case 7:
		type = "전사";
		break;
	   }
	      con = L1DatabaseFactory.getInstance().getConnection();
	      Statement pstm = con.createStatement();
	      ResultSet rs = pstm.executeQuery("SELECT objid FROM characters WHERE AccessLevel = 0 order by Exp desc");
	      Statement pstm2 = con.createStatement();
	      ResultSet rs2 = pstm2.executeQuery("SELECT `Exp`,`char_name` FROM `characters` WHERE AccessLevel = 0 ORDER BY `Exp` DESC limit 1");
	     
	      if(pc.getType()==0){
	      Statement pstm3 = con.createStatement();  
	      ResultSet rs3 = pstm3.executeQuery("SELECT objid FROM characters WHERE type = 0 and AccessLevel = 0 order by Exp desc");
	      while(rs3.next()){
	       j++;
	    if(objid == rs3.getInt(1))
	     break;
	   }
	      rs3.close(); 
	      pstm3.close();
	      }else if(pc.getType()==1){
	       Statement pstm3 = con.createStatement();  
	       ResultSet rs3 = pstm3.executeQuery("SELECT objid FROM characters WHERE type = 1 and AccessLevel = 0 order by Exp desc");
	       while(rs3.next()){
	        j++;
	     if(objid == rs3.getInt(1))
	      break;
	    }
	       rs3.close(); 
	       pstm3.close();
	      }else if(pc.getType()==2){
	       Statement pstm3 = con.createStatement();  
	       ResultSet rs3 = pstm3.executeQuery("SELECT objid FROM characters WHERE type = 2 and AccessLevel = 0 order by Exp desc");
	       while(rs3.next()){
	        j++;
	     if(objid == rs3.getInt(1))
	      break;
	    }
	       rs3.close(); 
	       pstm3.close();
	      }else if(pc.getType()==3){
	       Statement pstm3 = con.createStatement();  
	       ResultSet rs3 = pstm3.executeQuery("SELECT objid FROM characters WHERE type = 3 and AccessLevel = 0 order by Exp desc");
	       while(rs3.next()){
	        j++;
	     if(objid == rs3.getInt(1))
	      break;
	    }
	       rs3.close(); 
	       pstm3.close();
	      }else if(pc.getType()==4){
	       Statement pstm3 = con.createStatement();  
	       ResultSet rs3 = pstm3.executeQuery("SELECT objid FROM characters WHERE type = 4 and AccessLevel = 0 order by Exp desc");
	       while(rs3.next()){
	        j++;
	     if(objid == rs3.getInt(1))
	      break;
	    }
	       rs3.close(); 
	       pstm3.close();
	      }else if(pc.getType()==5){
	       Statement pstm3 = con.createStatement();  
	       ResultSet rs3 = pstm3.executeQuery("SELECT objid FROM characters WHERE type = 5 and AccessLevel = 0 order by Exp desc");
	       while(rs3.next()){
	        j++;
	     if(objid == rs3.getInt(1))
	      break;
	       }
	       rs3.close(); 
	       pstm3.close();
	       
	      }else if(pc.getType()==6){
	       Statement pstm3 = con.createStatement();  
	       ResultSet rs3 = pstm3.executeQuery("SELECT objid FROM characters WHERE type = 6 and AccessLevel = 0 order by Exp desc");
	       while(rs3.next()){
	        j++;
	     if(objid == rs3.getInt(1))
	      break;
	    }
	       rs3.close(); 
	       pstm3.close();
	      }else if(pc.getType()==7){
		       Statement pstm3 = con.createStatement();  
		       ResultSet rs3 = pstm3.executeQuery("SELECT objid FROM characters WHERE type = 7 and AccessLevel = 0 order by Exp desc");
		       while(rs3.next()){
		        j++;
		     if(objid == rs3.getInt(1))
		      break;
		    }
		       rs3.close(); 
		       pstm3.close();
		      }

	      rs.close(); 
	      pstm.close();
	      rs2.close(); 
	      pstm2.close();
	      con.close();
	     } catch (Exception e) {
	      // TODO: handle exception
	   }
	     return j;
	  }
   	
 	private int rankchar(L1PcInstance pc) { // 랭킹확인
	    Connection con = null;
	     int q = 0;
	     int objid = pc.getId();
	     String type = null;
	     try {
	      con = L1DatabaseFactory.getInstance().getConnection();
	      Statement pstm = con.createStatement();
	      ResultSet rs = pstm.executeQuery("SELECT objid FROM characters WHERE AccessLevel = 0 order by Exp desc");
	      Statement pstm2 = con.createStatement();
	      ResultSet rs2 = pstm2.executeQuery("SELECT `Exp`,`char_name` FROM `characters` WHERE AccessLevel = 0 ORDER BY `Exp` DESC limit 1");
	      while(rs.next()){
	    q++;
	    if(objid == rs.getInt(1))
	     break;
	   }
	      rs.close(); 
	      pstm.close();
	      rs2.close(); 
	      pstm2.close();
	      con.close();
	     } catch (Exception e) {
	      // TODO: handle exception
	   }
		return q;
	  }
	
	private void wardate(L1PcInstance pc) {
		// TODO 자동 생성된 메소드 스텁
		S_SystemMessage sm = new S_SystemMessage(
				"=======================================================");
		pc.sendPackets(sm, true);
		sm = new S_SystemMessage(WarTimeController.getInstance().WarTimeString(
				1)
				+ " [켄트성] 공성!");
		pc.sendPackets(sm, true);
		sm = new S_SystemMessage(
				"=======================================================");
		pc.sendPackets(sm, true);
		sm = new S_SystemMessage(WarTimeController.getInstance().WarTimeString(
				4)
				+ " [기란성] 공성!");
		pc.sendPackets(sm, true);
		sm = new S_SystemMessage(
				"=======================================================");
		pc.sendPackets(sm, true);
	}

	private void bossTime(L1PcInstance pc) {
		// TODO Auto-generated method stub
		try {
			S_SystemMessage sm = new S_SystemMessage(
					"================== 2시간 젠타임 보스 ==================");
			pc.sendPackets(sm, true);
			sm = new S_SystemMessage(" ");
			pc.sendPackets(sm, true);
			sm = new S_SystemMessage("[12시 기준, 2시간 마다 젠되며 50분~정각 사이 랜덤 출현]");
			pc.sendPackets(sm, true);
			sm = new S_SystemMessage(
					"[ex : 12시 타임 보스 : 11:50 ~ 12:00 사이 출현 됩니다.]");
			pc.sendPackets(sm, true);
			sm = new S_SystemMessage(" ");
			pc.sendPackets(sm, true);
			sm = new S_SystemMessage("크로커다일,드레이크선장,맘보토끼,이프리트,드레이크,쿠만");
			pc.sendPackets(sm, true);
			sm = new S_SystemMessage("흑장로,도펠갱어,그레이트 미노타우르스,커츠,자이언트 웜,");
			pc.sendPackets(sm, true);
			sm = new S_SystemMessage("에이션트자이언트,피닉스,우두머리 반어인,오염된 오크투사");
			pc.sendPackets(sm, true);
			sm = new S_SystemMessage("대왕오징어,스피리드,카스파 패밀리,유니콘,몽환의섬대정령");
			pc.sendPackets(sm, true);
			sm = new S_SystemMessage("저주받은 물의 대정령,저주받은 무녀 사엘,물의 정령,카푸,");
			pc.sendPackets(sm, true);
			sm = new S_SystemMessage("심연의주인,쿠만,네크로맨서,쉘맨,정예 흑기사 대장");
			pc.sendPackets(sm, true);
			sm = new S_SystemMessage(" ");
			pc.sendPackets(sm, true);
			sm = new S_SystemMessage(
					"=======================================================");
			pc.sendPackets(sm, true);
			sm = new S_SystemMessage(" ");
			pc.sendPackets(sm, true);
			sm = new S_SystemMessage(
					"================== 4시간 젠타임 보스 ==================");
			pc.sendPackets(sm, true);
			sm = new S_SystemMessage(" ");
			pc.sendPackets(sm, true);
			sm = new S_SystemMessage("[03시 기준, 4시간 마다 젠되며 30분~정각 사이 랜덤 출현]");
			pc.sendPackets(sm, true);
			sm = new S_SystemMessage(
					"[ex : 03시 타임 보스 : 02:30 ~ 03:00 사이 출현 됩니다.]");
			pc.sendPackets(sm, true);
			sm = new S_SystemMessage(" ");
			pc.sendPackets(sm, true);
			sm = new S_SystemMessage("바포메트,베레스,아이스데몬,데몬,아리오크,호세,얼음여왕");
			pc.sendPackets(sm, true);
			sm = new S_SystemMessage("산적두목 클라인,네크로스,아라니아,케팔레");
			pc.sendPackets(sm, true);
			sm = new S_SystemMessage("오르쿠스,리카온,부식된 해골기사");
			pc.sendPackets(sm, true);
			sm = new S_SystemMessage(" ");
			pc.sendPackets(sm, true);
			sm = new S_SystemMessage("[12시 기준, 4시간 마다 젠되며 30분~정각 사이 랜덤 출현]");
			pc.sendPackets(sm, true);
			sm = new S_SystemMessage(
					"[ex : 12시 타임 보스 : 11:30 ~ 12:00 사이 출현 됩니다.]");
			pc.sendPackets(sm, true);
			sm = new S_SystemMessage(" ");
			pc.sendPackets(sm, true);
			sm = new S_SystemMessage("데스나이트");
			pc.sendPackets(sm, true);
			sm = new S_SystemMessage(" ");
			pc.sendPackets(sm, true);
			sm = new S_SystemMessage("[02시 기준, 4시간 마다 젠되며 30분~정각 사이 랜덤 출현]");
			pc.sendPackets(sm, true);
			sm = new S_SystemMessage(
					"[ex : 02시 타임 보스 : 01:30 ~ 02:00 사이 출현 됩니다.]");
			pc.sendPackets(sm, true);
			sm = new S_SystemMessage(" ");
			pc.sendPackets(sm, true);
			sm = new S_SystemMessage("켄라우헬,케레니스,마이노 샤먼,");
			pc.sendPackets(sm, true);
			sm = new S_SystemMessage("마이노 샤먼의 다이아몬드 골렘,발바도스,백작 친위대장,");
			pc.sendPackets(sm, true);
			sm = new S_SystemMessage("타로스 백작,맘몬,흑마법사 마야, 버모스");
			pc.sendPackets(sm, true);
			sm = new S_SystemMessage(" ");
			pc.sendPackets(sm, true);
			sm = new S_SystemMessage(
					"=======================================================");
			pc.sendPackets(sm, true);
			sm = new S_SystemMessage(" ");
			pc.sendPackets(sm, true);
			sm = new S_SystemMessage(
					"================== 6시간 젠타임 보스 ==================");
			pc.sendPackets(sm, true);
			sm = new S_SystemMessage(" ");
			pc.sendPackets(sm, true);
			sm = new S_SystemMessage(
					"[01시 07시 13시 19시 6시간에 한번 젠되며 50분~정각 사이 랜덤 출현]");
			pc.sendPackets(sm, true);
			sm = new S_SystemMessage("[ex : 오만 보스 : 12시 50분 ~ 01시 사이 출현 됩니다.]");
			pc.sendPackets(sm, true);
			sm = new S_SystemMessage(" ");
			pc.sendPackets(sm, true);
			sm = new S_SystemMessage("오만의 탑 10층 ~ 100층 보스들");
			pc.sendPackets(sm, true);
			sm = new S_SystemMessage(" ");
			pc.sendPackets(sm, true);
			sm = new S_SystemMessage(
					"[06시 12시 18시 24시 6시간에 한번 젠되며 50분~정각 사이 랜덤 출현]");
			pc.sendPackets(sm, true);
			sm = new S_SystemMessage("[ex : 우호도 보스 : 23시 50분 ~ 24시 사이 출현 됩니다.]");
			pc.sendPackets(sm, true);
			sm = new S_SystemMessage(" ");
			pc.sendPackets(sm, true);
			sm = new S_SystemMessage("발록, 타락, 야히, 죽음, 혼돈");
			pc.sendPackets(sm, true);
			sm = new S_SystemMessage(" ");
			pc.sendPackets(sm, true);
			sm = new S_SystemMessage(
					"=======================================================");
			pc.sendPackets(sm, true);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/*
	 * private void Sealedoff(L1PcInstance pc, String param) { try {
	 * StringTokenizer tok = new StringTokenizer(param); int sealCount =
	 * Integer.parseInt(tok.nextToken(), 10); Account account =
	 * Account.load(pc.getAccountName()); //추가 if(account.getquize() != null){
	 * pc.sendPackets(new S_SystemMessage(".퀴즈인증 [설정한퀴즈] \\fY인증후 다시입력하세요."));
	 * return; } // 봉인해제시 퀴즈가 설정되어 있지 않다면 신청불가능 하도록
	 * 
	 * if (pc.getInventory().getWeight240() == 240){ pc.sendPackets(new
	 * S_SystemMessage("아이템이 너무 무거워, 더 이상 가질 수 없습니다.")); return; } if(sealCount
	 * > 15) { pc.sendPackets(new
	 * S_SystemMessage("봉인해제주문서 15개 이상 신청 할 수 없습니다.")); return; } if
	 * (pc.getInventory().checkItem(50021, 1)) { pc.sendPackets(new
	 * S_SystemMessage("봉인해제주문서를 모두다 사용후 신청하세요.")); return; } createNewItem(pc,
	 * 50021, sealCount); } catch (Exception e) { pc.sendPackets(new
	 * S_SystemMessage(".봉인해제신청 [신청할 장수] 을 입력하세요.")); } }
	 */
	private void Sealedoff(L1PcInstance pc, String param) {
		try {
			StringTokenizer tok = new StringTokenizer(param);
			String tem = tok.nextToken();
			Account account = Account.load(pc.getAccountName()); // 추가
			if (account.getquize() == null) {
				pc.sendPackets(new S_SystemMessage(".현제 퀴즈가 설정 되어 있지 않습니다."));
				return;
				// 봉인해제시 퀴즈가 설정되어 있지 않다면 신청불가능 하도록
			}
			if (!account.getquize().equalsIgnoreCase(tem)) {
				pc.sendPackets(new S_SystemMessage(".설정하신 퀴즈와 동일하지 않습니다."));
				return;
			}
			if (pc.getInventory().calcWeightpercent() >= 100) {
				pc.sendPackets(new S_SystemMessage(
						"아이템이 너무 무거워, 더 이상 가질 수 없습니다."));
				return;
			}
			if (pc.getInventory().checkItem(50021, 1)) {
				pc.sendPackets(new S_SystemMessage("봉인해제주문서를 모두다 사용후 신청하세요."));
				return;
			}
			createNewItem(pc, 50021, 20);
		} catch (Exception e) {
			pc.sendPackets(new S_SystemMessage(".봉인해제신청  [본인퀴즈] 을 입력하세요."));

		}
	}

	private void StartWar(L1PcInstance pc, String param) {
		try {
			String clan_name = pc.getClanname();
			L1Clan clan = L1World.getInstance().getClan(clan_name);

			if (clan == null)
				return;
			if (pc.getId() != clan.getLeaderId()) { // 상대가 혈맹주 이외
				pc.sendPackets(new S_ServerMessage(92, pc.getName())); // \f1%0은
																		// 프린스나
																		// 프린세스가
																		// 아닙니다.
				return;
			}

			StringTokenizer tok = new StringTokenizer(param);
			String clan_name1 = tok.nextToken();
			String clan_name2 = tok.nextToken();
			L1Clan clan1 = L1World.getInstance().getClan(clan_name1);
			L1Clan clan2 = L1World.getInstance().getClan(clan_name2);

			if (clan1 == null) {

				pc.sendPackets(new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "["
						+ clan_name1 + "]혈맹이 존재하지 않습니다."));
				return;
			}
			if (clan2 == null) {
				pc.sendPackets(new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "["
						+ clan_name2 + "]혈맹이 존재하지 않습니다."));
				return;
			}

			if (clan_name1.equalsIgnoreCase("루피왕") || clan_name2.equalsIgnoreCase("루피왕")) {
				pc.sendPackets(new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
						"선포 할 수 없는 혈맹입니다."));
				return;
			}

			for (L1War war : L1World.getInstance().getWarList()) {
				if (war.CheckClanInSameWar(clan_name1, clan_name2) == true) {

					pc.sendPackets(new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
							"[" + clan_name1 + "]혈맹과 [" + clan_name2
									+ "]혈맹은 이미 전쟁 중 입니다."));
					return;
				}

			}
			if (WarTimeController.getInstance().isNowWar(clan.getCastleId())) {
				pc.sendPackets(new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
						"공성중에는 전쟁을 선포할수 없습니다."));
				return;
			}

			L1War war = new L1War();
			war.handleCommands(2, clan_name1, clan_name2); // 모의전 개시

			L1World.getInstance().broadcastPacketToAll(
					new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "[" + clan_name1
							+ "혈맹] VS [" + clan_name2 + "]혈맹의 전쟁을 시작 합니다."));

		} catch (Exception e) {
			pc.sendPackets(new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
					".혈전시작 [혈맹이름] [혈맹이름] 을 적어주세요."));

		}
	}

	private void toRanking(L1PcInstance pc, String str) {
		L1PcInstance topc = null;
		try {
			StringTokenizer tok = new StringTokenizer(str);
			String charname = tok.nextToken();
			topc = L1World.getInstance().getPlayer(charname);
			if (topc == null) {
				L1NpcShopInstance npc = null;
				npc = L1World.getInstance().getNpcShop(charname);
				if (npc != null) {
					int allRank = 0;
					int classRank = 0;
					Random _rnd = new Random(System.nanoTime());
					Connection con = null;
					PreparedStatement pstm = null;
					ResultSet rs = null;
					PreparedStatement pstm1 = null;
					ResultSet rs1 = null;
					try {
						con = L1DatabaseFactory.getInstance().getConnection();
						pstm = con
								.prepareStatement("SELECT Exp FROM characters WHERE AccessLevel = 0 order by Exp desc");
						rs = pstm.executeQuery();
						while (rs.next()) {
							if (76007362 + _rnd.nextInt(30000000) >= rs
									.getInt(1))
								break;
							allRank++;
						}
						pstm1 = con
								.prepareStatement("SELECT Exp FROM characters WHERE Type = ? AND AccessLevel = 0 order by Exp desc");
						pstm1.setInt(1, 1);
						rs1 = pstm1.executeQuery();
						while (rs1.next()) {
							if (76007362 + _rnd.nextInt(30000000) >= rs1
									.getInt(1))
								break;
							classRank++;
						}
					} catch (SQLException e) {
					} finally {
						SQLUtil.close(rs);
						SQLUtil.close(pstm);
						SQLUtil.close(rs1);
						SQLUtil.close(pstm1);
						SQLUtil.close(con);
					}
					pc.sendPackets(new S_SystemMessage(charname + "님 전체:"
							+ allRank + "위 클래스:" + classRank + "위 "));
					return;
				} else {
					pc.sendPackets(new S_SystemMessage(charname
							+ "님 은 접속 중인 케릭터가 아닙니다."));
					return;
				}
			}
		} catch (Exception e) {
			return;
		}
		Connection con = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		PreparedStatement pstm1 = null;
		ResultSet rs1 = null;
		int allRank = 0;
		int classRank = 0;
		if (topc.noPlayerCK) { // 봇
			try {
				con = L1DatabaseFactory.getInstance().getConnection();
				pstm = con
						.prepareStatement("SELECT Exp FROM characters WHERE AccessLevel = 0 order by Exp desc");
				rs = pstm.executeQuery();
				while (rs.next()) {
					if (topc.getExp() >= rs.getInt(1))
						break;
					allRank++;
				}
				pstm1 = con
						.prepareStatement("SELECT Exp FROM characters WHERE Type = ? AND AccessLevel = 0 order by Exp desc");
				pstm1.setInt(1, topc.getType());
				rs1 = pstm1.executeQuery();
				while (rs1.next()) {
					if (topc.getExp() >= rs1.getInt(1))
						break;
					classRank++;
				}
			} catch (SQLException e) {
			} finally {
				SQLUtil.close(rs);
				SQLUtil.close(pstm);
				SQLUtil.close(rs1);
				SQLUtil.close(pstm1);
				SQLUtil.close(con);
			}
			pc.sendPackets(new S_SystemMessage(topc.getName() + "님 전체:"
					+ allRank + "위 클래스:" + classRank + "위 "));
			return;
		}
		/*
		 * if (!(pc.getInventory().checkItem(40308, 300000))){
		 * pc.sendPackets(new
		 * S_SystemMessage("상대방의 랭킹 을 검색 하기위해서는 1만 아데나가 필요합니다.")); return; }
		 */
		try {// 이프와동일
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con
					.prepareStatement("SELECT * FROM ( SELECT @RNUM:=@RNUM+1 AS ROWNUM , C.Exp,C.char_name,c.objid,c.type  FROM (SELECT @RNUM:=0) R, characters c  WHERE C.AccessLevel = 0  ORDER BY C.Exp DESC ) A  WHERE objid = ?");
			pstm.setInt(1, topc.getId());
			rs = pstm.executeQuery();
			if (rs.next()) {
				allRank = rs.getInt(1);
			}
			pstm1 = con
					.prepareStatement("SELECT * FROM ( SELECT @RNUM:=@RNUM+1 AS ROWNUM , C.Exp,C.char_name,c.objid,c.type  FROM (SELECT @RNUM:=0) R, characters c  WHERE C.AccessLevel = 0  and c.type =? ORDER BY C.Exp DESC ) A  WHERE objid = ?");
			pstm1.setInt(1, topc.getType());
			pstm1.setInt(2, topc.getId());
			rs1 = pstm1.executeQuery();

			if (rs1.next()) {
				classRank = rs1.getInt(1);
			}
			// pc.getInventory().consumeItem(40308, 10000);
			// pc.sendPackets(new
			// S_SystemMessage("랭킹검색으로 아데나(10000)이 소요되었습니다."));
		} catch (SQLException e) {// 엘즈 실패와동일 아닐시에는 다음행동
		} finally {
			SQLUtil.close(rs);
			SQLUtil.close(pstm);// 이건ㅁ...db열었던거 닫는 최적화를위한 작업
			SQLUtil.close(rs1);
			SQLUtil.close(pstm1);
			SQLUtil.close(con);
		}
		if (allRank > 20) {
			if (allRank < 100)
				allRank = allRank / 4 + allRank;
			else
				allRank = allRank / 2 + allRank;
		}
		if (classRank > 20) {
			if (classRank < 100)
				classRank = classRank / 4 + classRank;
			else
				classRank = classRank / 2 + classRank;
		}
		pc.sendPackets(new S_SystemMessage(topc.getName() + "님 전체:" + allRank
				+ "위 클래스:" + classRank + "위 "));

	}

	private void infoRanking(L1PcInstance pc) {
		Connection con = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		PreparedStatement pstm1 = null;
		ResultSet rs1 = null;
		int allRank = 0;
		int classRank = 0;
		/*
		 * if (!(pc.getInventory().checkItem(40308, 100000))) {
		 * pc.sendPackets(new S_SystemMessage("랭킹 을 조회 하기위해서는 1만 아데나가 필요합니다."));
		 * return;
		 * 
		 * }
		 */

		// 전체
		// SELECT count(exp)+1 as cnt FROM (`characters`) WHERE `Exp` > ? AND
		// `AccessLevel` = 0 ORDER BY `objid` asc
		//
		// 클래스
		// SELECT count(exp)+1 as cnt FROM (`characters`) WHERE `Type` = ? AND
		// `Exp` > ? AND `AccessLevel` = 0 ORDER BY `objid` asc
		try {// 이프와동일
			con = L1DatabaseFactory.getInstance().getConnection();
			// pstm =
			// con.prepareStatement("SELECT * FROM ( SELECT @RNUM:=@RNUM+1 AS ROWNUM , C.Exp,C.char_name,c.objid,c.type  FROM (SELECT @RNUM:=0) R, characters c  WHERE C.AccessLevel = 0  ORDER BY C.Exp DESC ) A  WHERE objid = ?");
			pstm = con
					.prepareStatement("SELECT count(exp)+1 as cnt FROM (`characters`) WHERE `Exp` > ? AND `AccessLevel` = 0 ORDER BY `objid` asc");
			pstm.setInt(1, pc.getExp());
			rs = pstm.executeQuery();
			if (rs.next()) {
				allRank = rs.getInt(1);
			}
			pstm1 = con
					.prepareStatement("SELECT count(exp)+1 as cnt FROM (`characters`) WHERE `Type` = ? AND `Exp` > ? AND `AccessLevel` = 0 ORDER BY `objid` asc");
			// pstm1 =
			// con.prepareStatement("SELECT * FROM ( SELECT @RNUM:=@RNUM+1 AS ROWNUM , C.Exp,C.char_name,c.objid,c.type  FROM (SELECT @RNUM:=0) R, characters c  WHERE C.AccessLevel = 0  and c.type =? ORDER BY C.Exp DESC ) A  WHERE objid = ?");
			pstm1.setInt(1, pc.getType());
			pstm1.setInt(2, pc.getExp());
			rs1 = pstm1.executeQuery();

			if (rs1.next()) {
				classRank = rs1.getInt(1);
			}
			// pc.getInventory().consumeItem(40308, 10000);
			// pc.sendPackets(new
			// S_SystemMessage("랭킹조회로 아데나(10000)이 소요되었습니다."));
		} catch (SQLException e) {// 엘즈 실패와동일 아닐시에는 다음행동
		} finally {
			SQLUtil.close(rs);
			SQLUtil.close(pstm);// 이건ㅁ...db열었던거 닫는 최적화를위한 작업
			SQLUtil.close(rs1);
			SQLUtil.close(pstm1);
			SQLUtil.close(con);
		}
		if (allRank > 20) {
			if (allRank < 100)
				allRank = allRank / 4 + allRank;
			else
				allRank = allRank / 2 + allRank;
		}

		if (classRank > 20) {
			if (classRank < 100)
				classRank = classRank / 4 + classRank;
			else
				classRank = classRank / 2 + classRank;
		}

		pc.sendPackets(new S_SystemMessage("전체:" + allRank + "위 클래스:"
				+ classRank + "위 "));

	}

	private void StopWar(L1PcInstance pc, String param) {
		try {
			String clan_name = pc.getClanname();
			L1Clan clan = L1World.getInstance().getClan(clan_name);

			if (clan == null)
				return;
			if (pc.getId() != clan.getLeaderId()) { // 상대가 혈맹주 이외
				pc.sendPackets(new S_ServerMessage(92, pc.getName())); // \f1%0은
																		// 프린스나
																		// 프린세스가
																		// 아닙니다.
				return;
			}
			StringTokenizer tok = new StringTokenizer(param);
			String clan_name1 = tok.nextToken();
			String clan_name2 = tok.nextToken();
			L1Clan clan1 = L1World.getInstance().getClan(clan_name1);
			L1Clan clan2 = L1World.getInstance().getClan(clan_name2);

			if (clan1 == null) {
				pc.sendPackets(new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "["
						+ clan_name1 + "]혈맹이 존재하지 않습니다."));
				return;
			}
			if (clan2 == null) {
				pc.sendPackets(new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "["
						+ clan_name2 + "]혈맹이 존재하지 않습니다."));
				return;
			}

			if (clan.getCastleId() != 0) {
				for (L1War war : L1World.getInstance().getWarList()) {
					if (war != null && war.GetWarType() == 1) {
						if (war.GetDefenceClanName().equals(clan.getClanName())) {
							return;
						}
					}
				}
			}

			for (L1War war : L1World.getInstance().getWarList()) {
				if (war.CheckClanInSameWar(clan_name1, clan_name2) == true) {
					war.CeaseWar(clan_name1, clan_name2);
					L1World.getInstance().broadcastPacketToAll(
							new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "["
									+ clan_name1 + "혈맹] VS [" + clan_name2
									+ "]혈맹의 전쟁을 종료 합니다."));
					return;
				}
			}
			pc.sendPackets(new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "["
					+ clan_name1 + "]혈맹과 [" + clan_name2
					+ "]혈맹은 현재 전쟁중이지 않습니다."));

		} catch (Exception e) {
			pc.sendPackets(new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
					".혈전종료 [혈맹이름] [혈맹이름] 을 적어주세요."));
		}
	}

	private void serchdroplist(L1PcInstance gm, String param) {
		try {
			StringTokenizer tok = new StringTokenizer(param);
			String nameid = tok.nextToken();

			int itemid = 0;
			try {
				itemid = Integer.parseInt(nameid);
			} catch (NumberFormatException e) {
				itemid = ItemTable.getInstance().findItemIdByNameWithoutSpace(
						nameid);
				if (itemid == 0) {
					gm.sendPackets(new S_SystemMessage("해당 아이템이 발견되지 않았습니다."));
					return;
				}
			}
			gm.sendPackets(new S_Serchdrop(itemid));
		} catch (Exception e) {
			// _log.log(Level.SEVERE, e.getLocalizedMessage(), e);
			gm.sendPackets(new S_SystemMessage(
					".드랍리스트 [itemid 또는 name]를 입력해 주세요."));
			gm.sendPackets(new S_SystemMessage("아이템 name을 공백없이 정확히 입력해야 합니다."));
			gm.sendPackets(new S_SystemMessage(
					"ex) .드랍 마법서(디스인티그레이트) -- > 검색 O"));
			gm.sendPackets(new S_SystemMessage("ex) .드랍 디스 -- > 검색 X"));
		}
	} // 추가

	private void age(L1PcInstance pc, String cmd) {
		try {
			StringTokenizer tok = new StringTokenizer(cmd);
			String AGE = tok.nextToken();
			int AGEint = Integer.parseInt(AGE);

			if (AGEint > 99) {
				pc.sendPackets(new S_SystemMessage("입력하신 나이는 올바른 값이 아닙니다."));
				return;
			}

			pc.setAge(AGEint);
			pc.save();
			pc.sendPackets(new S_SystemMessage(pc.getName() + " 님의 나이 ("
					+ AGEint + ")가 설정되었습니다."));
		} catch (Exception e) {
			pc.sendPackets(new S_SystemMessage("  사용 예) .나이 28"));
		}
	}

	/*
	 * private void age2(L1PcInstance pc, String cmd) { try { StringTokenizer
	 * tok = new StringTokenizer(cmd); String AGE2 = tok.nextToken(); int
	 * AGEint2 = Integer.parseInt(AGE2);
	 * 
	 * switch (AGEint2) { case 1: pc.sendPackets(new S_Age(pc , 1)); break; case
	 * 2: pc.sendPackets(new S_Age(pc , 2)); break; case 3: pc.sendPackets(new
	 * S_Age(pc , 3)); break; case 4: pc.sendPackets(new S_Age(pc , 4)); break;
	 * default: pc.sendPackets(new S_SystemMessage("  .족보 1~4 만 가능합니다."));
	 * 
	 * }
	 * 
	 * 
	 * } catch (Exception e) { pc.sendPackets(new
	 * S_SystemMessage("  사용 예) .족보 2")); } }
	 */
	/** 혈맹 파티 신청 명령어 **/
	public void ClanParty(L1PcInstance pc) {
		int ClanId = pc.getClanid();
		if (ClanId != 0
				&& (pc.getClanRank() == L1Clan.CLAN_RANK_GUARDIAN || pc
						.isCrown())) { // Clan[O] [군주,수호기사]
			for (L1PcInstance SearchBlood : L1World.getInstance()
					.getAllPlayers()) {
				if (SearchBlood.getClanid() != ClanId
						|| SearchBlood.isPrivateShop()
						|| SearchBlood.isInParty()) { // 클랜이 같지않다면[X],
														// 이미파티중이면[X], 상점중[X]
					continue; // 포문탈출
				} else if (SearchBlood.getName() != pc.getName()) {
					pc.setPartyType(1); // 파티타입 설정
					SearchBlood.setPartyID(pc.getId()); // 파티아이디 설정
					SearchBlood
							.sendPackets(new S_Message_YN(954, pc.getName())); // 분패파티
																				// 신청
					pc.sendPackets(new S_SystemMessage("당신은 ["
							+ SearchBlood.getName() + "]에게 파티를 신청했습니다."));
				}
			}
		} else { // 클랜이 없거나 군주 또는 수호기사 [X]
			pc.sendPackets(new S_SystemMessage("혈맹의 군주, 수호기사만 사용할수 있습니다."));
		}
	}

	/*
	 * private void hope(L1PcInstance pc, String cmd) { //드래곤볼 for (L1Object obj
	 * : L1World.getInstance().getVisibleObjects(pc, 10)) { // 5 범위 내에 오브젝트를 찾아서
	 * if (obj instanceof L1MonsterInstance){ // 몬스터라면 L1MonsterInstance mon =
	 * (L1MonsterInstance) obj; if(mon.getNpcTemplate().get_npcId() == 45000172)
	 * if (mon.getCurrentHp() > 0) try { StringTokenizer st = new
	 * StringTokenizer(cmd); String nameid = st.nextToken(); int count = 1; int
	 * enchant = 0; int itemid = 0; try { itemid = Integer.parseInt(nameid); }
	 * catch (NumberFormatException e) { itemid = ItemTable.getInstance().
	 * findItemIdByNameWithoutSpace(nameid); if (itemid == 217 ||itemid == 41148
	 * ||itemid == 40222 ||itemid == 40219 || itemid == 213 || itemid == 20270
	 * || itemid == 61 || itemid == 41153 || itemid == 40249 || itemid == 41152
	 * || itemid == 40249 || itemid == 41149 || itemid == 20321 || itemid ==
	 * 420104 || itemid == 420105 || itemid == 420106 || itemid == 420107 ||
	 * itemid == 430110 || itemid == 430109 || itemid == 430108 || itemid ==
	 * 430107 || itemid == 430106 || itemid == 430105 || itemid == 86 || itemid
	 * == 191 || itemid == 134 || itemid == 12 || itemid == 450006 || itemid ==
	 * 430506 || itemid == 20270) { //안나오게할 아이템.
	 * L1World.getInstance().broadcastPacketToAll(new
	 * S_PacketBox(S_PacketBox.GREEN_MESSAGE,
	 * "그런 아이템은 이 세상에 없다...소원을 빌때는 이름을 붙여 써라..서둘러라..시간이 없..다")); return; } }
	 * L1Item temp = ItemTable.getInstance(). getTemplate(itemid); if (temp !=
	 * null) { L1ItemInstance item = null; int createCount; for (createCount =
	 * 0; createCount < count; createCount++) { item = ItemTable.getInstance().
	 * createItem(itemid); item.setEnchantLevel(enchant); if (pc.getInventory().
	 * checkAddItem(item, 1) == L1Inventory.OK) { pc.sendPackets(new
	 * S_ChatPacket(pc, ""+nameid+" 을(를) 저에게 만들어 주세요!!" ,
	 * Opcodes.S_SAY, 2)); Thread.sleep(5000); pc.getInventory().
	 * storeItem(item); L1World.getInstance().broadcastPacketToAll(new
	 * S_PacketBox(S_PacketBox.GREEN_MESSAGE,
	 * ""+pc.getName()+"에게.."+nameid+"을!.... 너의 소원은 이루어졌다..그럼 이만....")); for
	 * (L1Object obj1 : L1World.getInstance().getVisibleObjects(pc, 20)) { // 20
	 * 범위 내에 오브젝트를 찾아서 if (obj1 instanceof L1MonsterInstance){ // 몬스터라면
	 * L1MonsterInstance mon1 = (L1MonsterInstance) obj;
	 * if(mon1.getNpcTemplate().get_npcId() == 45000172 ){
	 * mon1.receiveDamage(gm, 90000); // 데미지 } } }
	 * 
	 * } else { break; } } } else {
	 * L1World.getInstance().broadcastPacketToAll(new
	 * S_PacketBox(S_PacketBox.GREEN_MESSAGE,
	 * " 그런 아이템은 이 세상에 없다...소원을 빌때는 이름을 붙여써라..서둘러라..시간이 없..다")); } } catch
	 * (Exception e) { pc.sendPackets(new S_SystemMessage(
	 * ".소원 [아이템이름] 이라고 입력. ex).소원 싸울아비장검")); } }
	 * 
	 * } }
	 */
	/*
	 * private void checktime(L1PcInstance pc) { try { long nowtime =
	 * System.currentTimeMillis(); Timestamp nowstamp = new Timestamp(nowtime);
	 * int girantime = 0; int girantemp = 18000; int giranh = 0; int girans = 0;
	 * int giranm = 0;
	 * 
	 * int ivorytime = 0; int ivorytemp = 3600; int ivoryh = 0; int ivorys = 0;
	 * int ivorym = 0;
	 * 
	 * if(pc.getgiranday() != null && pc.getgiranday().getDate() ==
	 * nowstamp.getDate() && pc.getgirantime() != 0){ girantime =
	 * pc.getgirantime(); girans = (girantemp - girantime) % 60; giranm =
	 * (girantemp - girantime) / 60 % 60; giranh = (girantemp - girantime) /60
	 * /60;//시간 }else{ giranh = 5; giranm = 0; girans = 0; } if(pc.getivoryday()
	 * != null && pc.getivoryday().getDate() == nowstamp.getDate() &&
	 * pc.getivorytime() != 0){ ivorytime = pc.getivorytime(); ivorys =
	 * (ivorytemp - ivorytime) % 60; ivorym = (ivorytemp - ivorytime) / 60 % 60;
	 * ivoryh = (ivorytemp - ivorytime) /60 /60;//시간 }else{ ivoryh = 1; ivorym =
	 * 0; ivorys = 0; } pc.sendPackets(new
	 * S_SystemMessage("기란감옥 : "+giranh+"시간 "+giranm+"분 "+girans+"초"));
	 * pc.sendPackets(new
	 * S_SystemMessage("상아탑 : "+ivoryh+"시간 "+ivorym+"분 "+ivorys+"초")); }catch
	 * (Exception e) { } }
	 */
	private void check(L1PcInstance pc) {
		try {
			long curtime = System.currentTimeMillis() / 1000;
			if (pc.getQuizTime() + 20 > curtime) {
				pc.sendPackets(new S_SystemMessage("20초간의 지연시간이 필요합니다."));
				return;
			}
			double Exp = 1.0;
			double foodBonus = 1.0;
			double ainhasadBonus = 1.0;
			double dollBonus = 1.0;
			double expposion = 1.0;

			for (L1DollInstance doll : pc.getDollList()) {
				int dollType = doll.getDollType();
				if (dollType == L1DollInstance.DOLLTYPE_SNOWMAN_B
						|| dollType == L1DollInstance.DOLLTYPE_SNOWMAN_C) {
					dollBonus = 1.1;
				}
			}

			if (pc.getSkillEffectTimerSet().hasSkillEffect(
					L1SkillId.COOKING_1_7_N)
					|| pc.getSkillEffectTimerSet().hasSkillEffect(
							L1SkillId.COOKING_1_7_S)) {
				foodBonus = 1.05;
			} else if (pc.getSkillEffectTimerSet().hasSkillEffect(
					L1SkillId.COOKING_1_15_N)
					|| pc.getSkillEffectTimerSet().hasSkillEffect(
							L1SkillId.COOKING_1_15_S)) {
				foodBonus = 1.07;
			} else if (pc.getSkillEffectTimerSet().hasSkillEffect(
					L1SkillId.COOKING_1_23_N)
					|| pc.getSkillEffectTimerSet().hasSkillEffect(
							L1SkillId.COOKING_1_23_S)) {
				foodBonus = 1.09;
			}
			if (pc.getSkillEffectTimerSet()
					.hasSkillEffect(L1SkillId.EXP_POTION)
					|| pc.getSkillEffectTimerSet().hasSkillEffect(
							L1SkillId.STATUS_COMA_5)) {
				expposion = 1.2;
			}

			if (pc.getAinHasad() > 10000) {
				ainhasadBonus = 2;
				pc.sendPackets(new S_ACTION_UI(S_ACTION_UI.EINHASAD, pc, pc.getAinHasad()));
			}

			int exp1 = (int) (Exp * foodBonus * expposion * ainhasadBonus * dollBonus);

			int hpr = pc.getHpr() + pc.getInventory().hpRegenPerTick();
			int mpr = pc.getMpr() + pc.getInventory().mpRegenPerTick();

			pc.sendPackets(new S_SystemMessage(
					"===================( 나의 정보 )===================="));
			pc.sendPackets(new S_SystemMessage("\\fY(경험치:" + exp1
					+ "배)\\fY(피틱:" + hpr + ')' + "(엠틱:" + mpr + ')' + "(PK횟수:"
					+ pc.get_PKcount() + ")(엘릭:"
					+ pc.getAbility().getElixirCount() + "개)"));
			pc.sendPackets(new S_SystemMessage(
					"===================================================="));
			pc.setQuizTime(curtime);
		} catch (Exception e) {
		}
	}

	/*
	 * private void showHelp(L1PcInstance pc) { pc.sendPackets(new
	 * S_UserCommands(1)); }
	 * 
	 * private void showHelp1(L1PcInstance pc) { pc.sendPackets(new
	 * S_UserCommands3(1)); } private void showHelp2(L1PcInstance pc) {
	 * //pc.sendPackets(new S_ServerMessage(790)); //pc.sendPackets(new
	 * S_ServerMessage(755)); pc.sendPackets(new S_SystemMessage("\\fD이이이이"));
	 * 
	 * } private void supportHelp(L1PcInstance pc) { pc.sendPackets(new
	 * S_SystemMessage("\\fU상점을 켜신후 리스하시면 무인상점이 됩니다.")); }
	 */

	private void tell(L1PcInstance pc) {
		try {
			if (pc.isPinkName()) {
				pc.sendPackets(new S_SystemMessage("전투중이라 사용할 수 없습니다."));
				return;
			}
			if (pc.getLevel() < 60) {
				pc.sendPackets(new S_SystemMessage("60레벨 미만은 사용할 수 없습니다."));
				return;
			}
			if (pc.isFishing() || pc.isFishingReady() || pc.isPrivateShop()) {
				return;
			}
			long curtime = System.currentTimeMillis() / 1000;
			if (pc.getQuizTime() + 20 > curtime) {
				pc.sendPackets(new S_SystemMessage("20초간의 지연시간이 필요합니다."));
				return;
			}
			L1Teleport.teleport(pc, pc.getX(), pc.getY(), pc.getMapId(), pc.getMoveState().getHeading(), false);
			pc.sendPackets(new S_SystemMessage("주변 오브젝트를 재로딩 하였습니다."));
			pc.setQuizTime(curtime);
		} catch (Exception exception35) {
		}
	}

	private void charname(L1PcInstance pc, String cmd) {
		try {
			StringTokenizer tok = new StringTokenizer(cmd);
			String chaName = tok.nextToken();
			if (pc.getClanid() > 0) {
				pc.sendPackets(new S_SystemMessage("\\fU혈맹탈퇴후 캐릭명을 변경할수 있습니다."));
				return;
			}
			if (!pc.getInventory().checkItem(467009, 1)) { // 있나 체크
				pc.sendPackets(new S_SystemMessage(
						"\\fU케릭명 변경 비법서를 소지하셔야 가능합니다."));
				return;
			}
			for (int i = 0; i < chaName.length(); i++) {
				if (chaName.charAt(i) == 'ㄱ'
						|| chaName.charAt(i) == 'ㄲ'
						|| chaName.charAt(i) == 'ㄴ'
						|| chaName.charAt(i) == 'ㄷ'
						|| // 한문자(char)단위로 비교.
						chaName.charAt(i) == 'ㄸ'
						|| chaName.charAt(i) == 'ㄹ'
						|| chaName.charAt(i) == 'ㅁ'
						|| chaName.charAt(i) == 'ㅂ'
						|| // 한문자(char)단위로 비교
						chaName.charAt(i) == 'ㅃ'
						|| chaName.charAt(i) == 'ㅅ'
						|| chaName.charAt(i) == 'ㅆ'
						|| chaName.charAt(i) == 'ㅇ'
						|| // 한문자(char)단위로 비교
						chaName.charAt(i) == 'ㅈ'
						|| chaName.charAt(i) == 'ㅉ'
						|| chaName.charAt(i) == 'ㅊ'
						|| chaName.charAt(i) == 'ㅋ'
						|| // 한문자(char)단위로 비교.
						chaName.charAt(i) == 'ㅌ'
						|| chaName.charAt(i) == 'ㅍ'
						|| chaName.charAt(i) == 'ㅎ'
						|| chaName.charAt(i) == 'ㅛ'
						|| // 한문자(char)단위로 비교.
						chaName.charAt(i) == 'ㅕ'
						|| chaName.charAt(i) == 'ㅑ'
						|| chaName.charAt(i) == 'ㅐ'
						|| chaName.charAt(i) == 'ㅔ'
						|| // 한문자(char)단위로 비교.
						chaName.charAt(i) == 'ㅗ'
						|| chaName.charAt(i) == 'ㅓ'
						|| chaName.charAt(i) == 'ㅏ'
						|| chaName.charAt(i) == 'ㅣ'
						|| // 한문자(char)단위로 비교.
						chaName.charAt(i) == 'ㅠ'
						|| chaName.charAt(i) == 'ㅜ'
						|| chaName.charAt(i) == 'ㅡ'
						|| chaName.charAt(i) == 'ㅒ'
						|| // 한문자(char)단위로 비교.
						chaName.charAt(i) == 'ㅖ'
						|| chaName.charAt(i) == 'ㅢ'
						|| chaName.charAt(i) == 'ㅟ'
						|| chaName.charAt(i) == 'ㅝ'
						|| // 한문자(char)단위로 비교.
						chaName.charAt(i) == 'ㅞ' || chaName.charAt(i) == 'ㅙ'
						|| chaName.charAt(i) == 'ㅚ'
						|| chaName.charAt(i) == 'ㅘ'
						|| // 한문자(char)단위로 비교.
						chaName.charAt(i) == '씹' || chaName.charAt(i) == '좃'
						|| chaName.charAt(i) == '좆' || chaName.charAt(i) == 'ㅤ') {
					pc.sendPackets(new S_SystemMessage("사용할수없는 케릭명입니다."));
					return;
				}
			}
			if (chaName.getBytes().length > 12) {
				pc.sendPackets(new S_SystemMessage("이름이 너무 깁니다."));
				return;
			}
			if (chaName.length() == 0) {
				pc.sendPackets(new S_SystemMessage("변경할 케릭명을 입력하세요."));
				return;
			}
			if (BadNamesList.getInstance().isBadName(chaName)) {
				pc.sendPackets(new S_SystemMessage("사용할 수 없는 케릭명입니다."));
				return;
			}
			if (isInvalidName(chaName)) {
				pc.sendPackets(new S_SystemMessage("사용할 수 없는 케릭명입니다."));
				return;
			}
			if (CharacterTable.doesCharNameExist(chaName)) {
				pc.sendPackets(new S_SystemMessage("동일한 케릭명이 존재합니다."));
				return;
			}

			if (CharacterTable.RobotNameExist(chaName)) {
				pc.sendPackets(new S_SystemMessage("동일한 케릭명이 존재합니다."));
				return;
			}
			if (CharacterTable.RobotCrownNameExist(chaName)) {
				pc.sendPackets(new S_SystemMessage("동일한 케릭명이 존재합니다."));
				return;
			}
			if (NpcShopSpawnTable.getInstance().getNpc(chaName)
					|| npcshopNameCk(chaName)) {
				pc.sendPackets(new S_SystemMessage("동일한 케릭명이 존재합니다."));
				return;
			}
			if (CharacterTable.somakname(chaName)) {
				pc.sendPackets(new S_SystemMessage("동일한 케릭명이 존재합니다."));
				return;
			}
			String oldname = pc.getName();

			chaname(chaName, oldname);

			long sysTime = System.currentTimeMillis();
			logchangename(chaName, oldname, new Timestamp(sysTime));

			pc.sendPackets(new S_SystemMessage(chaName + " 아이디로 변경 하셨습니다."));
			pc.sendPackets(new S_SystemMessage(
					"\\fU계정 입력창으로 이동후 다시 접속하시면 적용됩니다."));
			pc.getInventory().consumeItem(467009, 1); // 소모
		} catch (Exception e) {
			pc.sendPackets(new S_SystemMessage(
					".케릭명변경 [바꾸실아이디] []는 제외하고 입력해주세요."));
		}
	}

	private boolean npcshopNameCk(String name) {
		return NpcTable.getInstance().findNpcShopName(name);
	}

	/** 변경 가능한지 검사한다 시작 **/
	private static boolean isAlphaNumeric(String s) {
		boolean flag = true;
		char ac[] = s.toCharArray();
		int i = 0;
		do {
			if (i >= ac.length) {
				break;
			}
			if (!Character.isLetterOrDigit(ac[i])) {
				flag = false;
				break;
			}
			i++;
		} while (true);
		return flag;
	}

	private static boolean isInvalidName(String name) {
		int numOfNameBytes = 0;
		try {
			numOfNameBytes = name.getBytes("EUC-KR").length;
		} catch (UnsupportedEncodingException e) {
			// _log.log(Level.SEVERE, e.getLocalizedMessage(), e);
			return false;
		}

		if (isAlphaNumeric(name)) {
			return false;
		}
		if (5 < (numOfNameBytes - name.length()) || 12 < numOfNameBytes) {
			return false;
		}

		if (BadNamesList.getInstance().isBadName(name)) {
			return false;
		}
		return true;
	}

	private void chaname(String chaName, String oldname) {
		Connection con = null;
		PreparedStatement pstm = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con
					.prepareStatement("UPDATE characters SET char_name=? WHERE char_name=?");
			pstm.setString(1, chaName);
			pstm.setString(2, oldname);
			pstm.executeUpdate();
		} catch (Exception e) {

		} finally {
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
	}

	private void logchangename(String chaName, String oldname,
			Timestamp datetime) {
		Connection con = null;
		PreparedStatement pstm = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			String sqlstr = "INSERT INTO Log_Change_name SET Old_Name=?,New_Name=?, Time=?";
			pstm = con.prepareStatement(sqlstr);
			pstm.setString(1, oldname);
			pstm.setString(2, chaName);
			pstm.setTimestamp(3, datetime);
			pstm.executeUpdate();
		} catch (SQLException e) {
		} finally {
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
	}

	public void autoroot(L1PcInstance pc, String cmd, String param) {
		if (param.equalsIgnoreCase("끔")) { // by사부 오토루팅 켬끔 명령어
			pc.getSkillEffectTimerSet().setSkillEffect(
					L1SkillId.STATUS_AUTOROOT, 0);
			pc.sendPackets(new S_SystemMessage("\\fY오토루팅을 해제합니다. "));

		} else if (param.equalsIgnoreCase("켬")) { // by사부 오토루팅 켬끔 명령어
			pc.getSkillEffectTimerSet().removeSkillEffect(
					L1SkillId.STATUS_AUTOROOT);
			pc.sendPackets(new S_SystemMessage("\\fY오토루팅을 활성화합니다. "));

		} else { // by사부 오토루팅 켬끔 명령어
			pc.sendPackets(new S_SystemMessage(cmd + " [켬,끔] 라고 입력해 주세요. "));
		}
	}

	public void ment(L1PcInstance pc, String cmd, String param) { // by사부 멘트
		if (param.equalsIgnoreCase("끔")) {
			pc.getSkillEffectTimerSet()
					.setSkillEffect(L1SkillId.STATUS_MENT, 0);
			pc.sendPackets(new S_SystemMessage("\\fY오토루팅 멘트를 끕니다."));
		} else if (param.equalsIgnoreCase("켬")) {
			pc.getSkillEffectTimerSet()
					.removeSkillEffect(L1SkillId.STATUS_MENT);
			pc.sendPackets(new S_SystemMessage("\\fY오토루팅 멘트를 켭니다."));

		} else {
			pc.sendPackets(new S_SystemMessage(cmd + " [켬,끔] 라고 입력해 주세요. "));
		}
	}

	public void killment(L1PcInstance pc, String cmd, String param) { // by사부 멘트
		if (param.equalsIgnoreCase("끔")) {
			pc.킬멘트 = false;
			pc.sendPackets(new S_SystemMessage("킬멘트 를 표시하지 않습니다."));
		} else if (param.equalsIgnoreCase("켬")) {
			pc.킬멘트 = true;
			pc.sendPackets(new S_SystemMessage("킬멘트 를 표시 합니다."));
		} else {
			pc.sendPackets(new S_SystemMessage(".킬멘트 [켬/끔] 으로 입력해 주세요. "));
		}
	}

	private void serchdroplist2(L1PcInstance gm, String param) {
		try {
			StringTokenizer tok = new StringTokenizer(param);
			String nameid = tok.nextToken();

			int npcid = 0;
			try {
				npcid = Integer.parseInt(nameid);
			} catch (NumberFormatException e) {
				if (nameid.equalsIgnoreCase("흑장로")) {
					npcid = 45545;
				} else
					npcid = NpcTable.getInstance().findNpcIdByName(nameid);
				if (npcid == 0) {
					gm.sendPackets(new S_SystemMessage("해당 몬스터가 발견되지 않았습니다."));
					return;
				}
			}
			gm.sendPackets(new S_Serchdrop2(npcid));
		} catch (Exception e) {
			// _log.log(Level.SEVERE, e.getLocalizedMessage(), e);
			gm.sendPackets(new S_SystemMessage(".몹드랍 [몬스터이름]을 입력해 주세요."));
			gm.sendPackets(new S_SystemMessage(".몹이름입력시 공백없이 정확히 입력해야 합니다."));
		}
	}

	private static boolean isDisitAlpha(String str) {
		boolean check = true;
		for (int i = 0; i < str.length(); i++) {
			if (!Character.isDigit(str.charAt(i)) // 숫자가 아니라면
					&& Character.isLetterOrDigit(str.charAt(i)) // 특수문자라면
					&& !Character.isUpperCase(str.charAt(i)) // 대문자가 아니라면
					&& !Character.isLowerCase(str.charAt(i))) { // 소문자가 아니라면
				check = false;
				break;
			}
		}
		return check;
	}

	private static String encodePassword(String rawPassword)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		byte buf[] = rawPassword.getBytes("UTF-8");
		buf = MessageDigest.getInstance("SHA").digest(buf);

		return Base64.encodeBytes(buf);
	}

	public static String checkPassword(String accountName) {
		String _inputPwd = null;
		Connection con = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con.prepareStatement("select password from accounts where login = ?");
			pstm.setString(1, accountName);// 이부분 에러뜸ㄴ
			rs = pstm.executeQuery();
			if (rs.next()) {
				_inputPwd = rs.getString("password");
			}
			return _inputPwd;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			SQLUtil.close(rs);
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
		return _inputPwd;
	}

	public static boolean checkPassword(String accountName, String _pwd, String rawPassword) {
		String _inputPwd = null;
		Connection con = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con.prepareStatement("SELECT password(?) as pwd ");

			pstm.setString(1, rawPassword);// 이부분 에러뜸ㄴ
			rs = pstm.executeQuery();
			if (rs.next()) {
				_inputPwd = rs.getString("pwd");
			}
			SQLUtil.close(rs);
			SQLUtil.close(pstm);
			SQLUtil.close(con);
			if (_pwd.equals(_inputPwd)) { // 동일하다면
				return true;
			} else
				return false;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			SQLUtil.close(rs);
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
		return false;
	}

	private void to_Change_Passwd(L1PcInstance pc, String passwd) {
		PreparedStatement statement = null;
		PreparedStatement pstm = null;
		java.sql.Connection con = null;
		ResultSet rs = null;
		try {
			String login = null;
			con = L1DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("select account_name from characters where char_name Like '" + pc.getName() + "'");
			rs = statement.executeQuery();
			while (rs.next()) {
				login = rs.getString(1);
				pstm = con.prepareStatement("UPDATE accounts SET password=? WHERE login Like '" + login + "'");
				pstm.setString(1, passwd);
				pstm.executeUpdate();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			SQLUtil.close(rs);
			SQLUtil.close(pstm);
			SQLUtil.close(statement);
			SQLUtil.close(con);
		}
	}

	private void changePassword(L1PcInstance pc, String param) {
		try {
			StringTokenizer tok = new StringTokenizer(param);
			String 키워드 = tok.nextToken();
			String 새로운passwd = tok.nextToken();
			Account account = Account.load(pc.getAccountName()); // 추가
			if (account.getquize() != null) {
				if (account.getquize().equalsIgnoreCase(키워드)) {
				} else {
					pc.sendPackets(new S_SystemMessage("퀴즈가 일치하지 않습니다."));
					return;
				}
			} else {
				pc.sendPackets(new S_SystemMessage(".퀴즈설정을 이용해 퀴즈를 먼저 등록해 주세요."));
				return;
			}
			if (새로운passwd.length() < 4) {
				pc.sendPackets(new S_SystemMessage("최소 4자 이상 입력해 주십시오."));
				return;
			}

			if (새로운passwd.length() > 10) {
				pc.sendPackets(new S_SystemMessage("최대 10자 이하로 입력해 주십시오."));
				return;
			}

			if (isDisitAlpha(새로운passwd) == false) {
				pc.sendPackets(new S_SystemMessage("암호에 허용되지 않는 문자가 포함 되어 있습니다."));
				return;
			}
			to_Change_Passwd(pc, 새로운passwd);
			pc.sendPackets(new S_SystemMessage("비밀번호가 [" + 새로운passwd + "]로 변경되었습니다."));
		} catch (Exception e) {
			pc.sendPackets(new S_SystemMessage(".비번변경 퀴즈 바꿀비번 으로 입력해 주세요."));
		}
	}
	public void 인첸축복(L1PcInstance pc, String arg) {
		   try {
			   StringTokenizer tok = new StringTokenizer(arg);
			   String user = tok.nextToken();
			   L1PcInstance target = L1World.getInstance().getPlayer(user);
			   target.인첸축복 = true;
		   	}catch (Exception e) {
		   		pc.sendPackets(new S_SystemMessage(".인첸축복 [케릭명]"));
		   	}
		}
	private void ban(L1PcInstance pc, String param) {
		try {
			StringTokenizer st = new StringTokenizer(param);
			int type = Integer.parseInt(st.nextToken(), 10);
			String account = st.nextToken();
			IpTable iptable = IpTable.getInstance();
			if (account.equalsIgnoreCase("")) {
				pc.sendPackets(new S_SystemMessage(".벤 [1~4] [IP or 계정]"), true);
				pc.sendPackets(new S_SystemMessage("1 = 계정벤 / 3 = 계정벤 해제"),
						true);
				pc.sendPackets(new S_SystemMessage("2 = IP벤 / 4 = IP벤 해제"),
						true);
				pc.sendPackets(new S_SystemMessage("5 = 광역IP벤 / 6 = 광역IP벤 해제"),
						true);
				pc.sendPackets(new S_SystemMessage(
						"7 = 광역IP벤+계정 / 8 = 광역IP벤+계정 해제"), true);
			}
			switch (type) {
			case 1: {// 계정벤
				Account.ban(account);
				pc.sendPackets(new S_SystemMessage(account + " 계정을 압류시켰습니다."),
						true);
			}
				break;
			case 2: {// 아이피벤
				iptable.banIp(account); // BAN 리스트에 IP를 추가한다.
				pc.sendPackets(new S_SystemMessage(account + " IP를 차단 시킵니다."),
						true);
				// iptable.reload();
			}
				break;
			case 3: {// 계정벤풀기
				Connection con = null;
				PreparedStatement pstm = null;
				try {
					con = L1DatabaseFactory.getInstance().getConnection();
					String sqlstr = "UPDATE accounts SET banned=0 WHERE login=?";
					pstm = con.prepareStatement(sqlstr);
					pstm.setString(1, account);
					pstm.executeUpdate();
				} catch (SQLException e) {
		
				} finally {
					SQLUtil.close(pstm);
					SQLUtil.close(con);
				}
				pc.sendPackets(
						new S_SystemMessage(account + " 계정의 벤을 해제 시킵니다."), true);
			}
				break;
			case 4: {// 아이피벤풀기
				iptable.liftBanIp(account);
				// iptable.reload();
			}
				break;
			case 5: {// 광역아이피벤
				if (account.lastIndexOf(".") + 1 != account.length()) {
					pc.sendPackets(new S_SystemMessage(
							"123.456.789.  <-- 형식으로 입력하여주세요"), true);
					return;
				}
				for (int i = 1; i <= 255; i++) {
					iptable.banIp(account + i); // BAN 리스트에 IP를 추가한다.
				}
				pc.sendPackets(new S_SystemMessage(account
						+ "1~255 IP를 차단 시킵니다."), true);
			}
				break;
			case 6: {// 광역아이피벤 풀기
				if (account.lastIndexOf(".") + 1 != account.length()) {
					pc.sendPackets(new S_SystemMessage(
							"123.456.789.  <-- 형식으로 입력하여주세요"), true);
					return;
				}
				for (int i = 1; i <= 255; i++) {
					iptable.liftBanIp(account + i); // BAN 리스트에 IP를 삭제한다.
				}
				pc.sendPackets(new S_SystemMessage(account
						+ "1~255 IP를 해제 시킵니다."), true);
			}
				break;
			case 7: {// 광역아이피+계정 벤
				if (account.lastIndexOf(".") + 1 != account.length()) {
					pc.sendPackets(new S_SystemMessage(
							"123.456.789.  <-- 형식으로 입력하여주세요"), true);
					return;
				}
				Connection con = null;
				PreparedStatement pstm = null;
				try {
					con = L1DatabaseFactory.getInstance().getConnection();
					for (int i = 1; i <= 255; i++) {
						iptable.banIp(account + i); // BAN 리스트에 IP를 추가한다.
						String sqlstr = "UPDATE accounts SET banned=1 WHERE ip=?";
						pstm = con.prepareStatement(sqlstr);
						pstm.setString(1, account + i);
						pstm.executeUpdate();
						SQLUtil.close(pstm);
					}
				} catch (SQLException e) {
				
				} finally {
					SQLUtil.close(pstm);
					SQLUtil.close(con);
				}
				pc.sendPackets(new S_SystemMessage(account
						+ "1~255 IP 차단과 해당 계정을 압류시킵니다."), true);
			}
				break;
			case 8: {// 광역아이피+계정 벤풀기
				if (account.lastIndexOf(".") + 1 != account.length()) {
					pc.sendPackets(new S_SystemMessage(
							"123.456.789.  <-- 형식으로 입력하여주세요"), true);
					return;
				}
				Connection con = null;
				PreparedStatement pstm = null;
				try {
					con = L1DatabaseFactory.getInstance().getConnection();
					for (int i = 1; i <= 255; i++) {
						iptable.liftBanIp(account + i); // BAN 리스트에 IP를 삭제한다.
						String sqlstr = "UPDATE accounts SET banned=0 WHERE ip=?";
						pstm = con.prepareStatement(sqlstr);
						pstm.setString(1, account + i);
						pstm.executeUpdate();
						SQLUtil.close(pstm);
					}
				} catch (SQLException e) {
					
				} finally {
					SQLUtil.close(pstm);
					SQLUtil.close(con);
				}
				pc.sendPackets(new S_SystemMessage(account
						+ "1~255 IP와 계정을 해제 시킵니다."), true);
			}
				break;
			}
		} catch (Exception e) {
			pc.sendPackets(new S_SystemMessage(".벤 [1~4] [IP or 계정]"), true);
			pc.sendPackets(new S_SystemMessage("1 = 계정벤 / 3 = 계정벤 해제"), true);
			pc.sendPackets(new S_SystemMessage("2 = IP벤 / 4 = IP벤 해제"), true);
			pc.sendPackets(new S_SystemMessage("5 = 광역IP벤 / 6 = 광역IP벤 해제"),
					true);
			pc.sendPackets(
					new S_SystemMessage("7 = 광역IP벤+계정 / 8 = 광역IP벤+계정 해제"), true);
		}
	}
	public void 기운축복(L1PcInstance pc, String arg) {
		   try {
			   StringTokenizer tok = new StringTokenizer(arg);
			   String user = tok.nextToken();
			   L1PcInstance target = L1World.getInstance().getPlayer(user);
			   target.기운축복 = true;
		   	}catch (Exception e) {
		   		pc.sendPackets(new S_SystemMessage(".기운축복 [케릭명]"));
		   	}
		}
	private void phone(L1PcInstance pc, String param) {
		try {
			StringTokenizer tok = new StringTokenizer(param);
			String name = tok.nextToken();
			String phone = tok.nextToken();

			if (name.length() > 12 || phone.length() > 12) {
				pc.sendPackets(new S_SystemMessage("잘못된 길이 입니다."));
				return;
			}
			Account account = Account.load(pc.getAccountName());
			if(account.getPrcount() == 0){
				pc.sendPackets(new S_ACTION_UI(S_ACTION_UI.신스킬, 6073), true);
				pc.sendPackets(new S_ACTION_UI(6073, 172800, 4827, 5072), true);
				pc.addDamageReductionByArmor(1);
				pc.getSkillEffectTimerSet().setSkillEffect(L1SkillId.고정인증버프, 172800 * 1000);
				Account.repr(pc.getAccountName());
				phonenumber(name, phone, pc.getName());
				pc.sendPackets(new S_SystemMessage(pc, "고정인증이 완료되었습니다. 고정 버프가 지급되었습니다."), true);
			}else{
				phonenumber(name, phone, pc.getName());
				pc.sendPackets(new S_SystemMessage("\\fT(" + name + ") 님 " + phone + " 번호로 변경 되셨습니다. 감사합니다."));
			}
		} catch (Exception e) {
			pc.sendPackets(new S_SystemMessage(".고정신청 [성함 폰번호] 로 입력해주세요.[]문자는 제외"));
			pc.sendPackets(new S_SystemMessage("EX).고정신청 홍길동 0000000000 (번호에 스페이스가 들어가면 안됩니다.)"));
		}
	}

	private void quize(L1PcInstance pc, String param) {
		try {
			StringTokenizer tok = new StringTokenizer(param);
			// String user = tok.nextToken();
			String quize = tok.nextToken();
			Account account = Account.load(pc.getAccountName());

			if (quize.length() < 6) {
				pc.sendPackets(new S_SystemMessage("최소 6자 이상 입력해 주십시오."));
				return;
			}

			if (quize.length() > 12) {
				pc.sendPackets(new S_SystemMessage("최대 12자 이하로 입력해 주십시오."));
				return;
			}
			if (isDisitAlpha(quize) == false) {
				pc.sendPackets(new S_SystemMessage("허용되지 않는 문자가 포함되었습니다."));
				return;
			}

			if (account.getquize() != null) {
				pc.sendPackets(new S_SystemMessage("이미 퀴즈가 설정되어 있습니다."));
				return;
			}
			account.setquize(quize);
			Account.updateQuize(account);
			pc.sendPackets(new S_SystemMessage("퀴즈가 [" + quize
					+ "]으로 입력되었습니다. 퀴즈는 다시 확인과 변경이 불가능하니 유의하시기 바랍니다."));
		} catch (Exception e) {
			pc.sendPackets(new S_SystemMessage(
					".설정하실 퀴즈를 입력해주세요.\n최소 6자 이상 입력하시길 바랍니다."));
		}
	}

	public void phonenumber(String name, String phone, String chaname) {
		Connection con = null;
		PreparedStatement pstm = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			String sqlstr = "INSERT INTO UserPhone SET name=?,pnumber=?, chaname=?";
			pstm = con.prepareStatement(sqlstr);
			pstm.setString(1, name);
			pstm.setString(2, phone);
			pstm.setString(3, chaname);
			pstm.executeUpdate();
		} catch (SQLException e) {
		} finally {
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
	}

	private void quize1(L1PcInstance pc, String cmd) {
		try {
			StringTokenizer tok = new StringTokenizer(cmd);
			String quize2 = tok.nextToken();
			Account account = Account.load(pc.getAccountName());

			if (quize2.length() < 4) {
				pc.sendPackets(new S_SystemMessage("최소 4자 이상 입력해 주십시오."));
				return;
			}

			if (quize2.length() > 12) {
				pc.sendPackets(new S_SystemMessage("최대 12자 이하로 입력해 주십시오."));
				return;
			}

			if (account.getquize() == null || account.getquize() == "") {
				pc.sendPackets(new S_SystemMessage(
						"키워드가 설정되어 있지 않습니다.\\fY [.키워드] [암호]"));
				return;
			}
			if (!quize2.equals(account.getquize())) {
				pc.sendPackets(new S_SystemMessage(
						"키워드가 일치하지 않습니다.\\fY잊었나요?? 건의게시판 문의"));
				return;
			}
			if (isDisitAlpha(quize2) == false) {
				pc.sendPackets(new S_SystemMessage("키워드에 허용되지 않는 문자가 포함되었습니다."));
				return;
			}
			account.setquize(null);
			Account.updateQuize(account);
			pc.sendPackets(new S_SystemMessage(
					"키워드인증이 완료 되었습니다.\\fY새로운 키워드를 설정하세요"));
		} catch (Exception e) {
			pc.sendPackets(new S_SystemMessage(".키워드인증 [설정하신 키워드]를 입력하세요."));
		}
	}
	private void Hunt(L1PcInstance pc, String cmd) {
		try {
			StringTokenizer st = new StringTokenizer(cmd);
			String char_name = st.nextToken();
			int price = Integer.parseInt(st.nextToken());
			
			L1PcInstance target = L1World.getInstance().getPlayer(char_name);
			
			if (target != null) {
				if (target.isGm()) {
					return;
				}
				
				int 현재단계 = target.getHuntCount();
				
				if (현재단계 >= 3) {
					pc.sendPackets(new S_SystemMessage("해당 캐릭터는 이미 3단계 수배중입니다."));
					return;
				}
				
				switch (현재단계) {
					case 0:
						if (price != 15000000) {
							pc.sendPackets(new S_SystemMessage("현재 수배 가능한 금액은 "+ (15000000 / 10000) +" 만 아데나입니다"));
							return;
						}
						break;
						
					case 1:
						if (price != 25000000) {
							pc.sendPackets(new S_SystemMessage("현재 수배 가능한 금액은 "+ (25000000 / 10000) +" 만 아데나입니다"));
							return;
						}
						break;
						
					case 2:
						if (price != 40000000) {
							pc.sendPackets(new S_SystemMessage("현재 수배 가능한 금액은 "+ (40000000 / 10000) +" 만 아데나입니다"));
							return;
						}
						break;
				}
				
				if (!(pc.getInventory().consumeItem(40308, price))) {
					pc.sendPackets(new S_SystemMessage("아데나가 부족합니다"));
					return;
				}
				
				target.initBeWanted();
				
				현재단계++;
				
				target.setHuntCount(현재단계);
				target.setHuntPrice(target.getHuntPrice() + price);
				target.save();
				
				target.addBeWanted();
				L1Teleport.teleport(target, target.getX(), target.getY(), (short) target.getMapId(), target.getMoveState().getHeading(), false);//테베
				
				
				L1World.getInstance().broadcastServerMessage("\\aD[" + target.getName() + "] 님의 목에 현상금이 걸렸습니다.");
				L1World.getInstance().broadcastPacketToAll(new S_SystemMessage("\\aD[ 수배자 ] :  " + target.getName()));
			} else {
				pc.sendPackets(new S_SystemMessage("접속중인 캐릭터가 아닙니다."));
			}
		} catch (Exception e) {
			pc.sendPackets(new S_SystemMessage(".수배 [캐릭터명] [금액]"));
			pc.sendPackets(new S_SystemMessage("수배된 캐릭터는 단계별로 [추타,리덕,SP] 효과가 증가됩니다."));
		}
	}
	
	private void changeTitle(L1PcInstance pc, String title) {
		int objectId = pc.getId();
		pc.setTitle(title);
		pc.sendPackets(new S_CharTitle(objectId, title));
		Broadcaster.broadcastPacket(pc, new S_CharTitle(objectId, title));
		try {
			pc.save(); // DB에 캐릭터 정보를 써 우
		} catch (Exception e) {
		}
	}
	
	/*private void joinlotto(L1PcInstance pc){
		if(pc.getLevel() < Config.LOTTO_LEVEL){
			pc.sendPackets(new S_SystemMessage("로또게임은 "+Config.LOTTO_LEVEL+"레벨 부터 참여하실 수 있습니다."));
			return;
		}
		if(LottoGameController.getInstance().getLottoStart() == true){
		if(pc.getLottoGame()){
			if(pc.getInventory().checkItem(40308, Config.LOTTO_BATTING)){
			int lottonumber = 0;
			pc.system = -1;
			lottonumber = _random.nextInt(Config.LOTTO_SIZE)+1;
			pc.sendPackets(new S_SystemMessage("\\aG"+pc.getName()+"님의 로또 응모번호는 \\aD"+lottonumber+"번 \\aG입니다."));
			pc.getInventory().consumeItem(40308, Config.LOTTO_BATTING);
			LottoGameController.getInstance().addLottoAden(Config.LOTTO_BATTING);
			pc.sendPackets(new S_SystemMessage("로또게임 응모: "+Config.LOTTO_BATTING+"아데나 지출."));
			pc.setLotto(lottonumber);
			pc.setLottoGame(false);
			}else{
			pc.sendPackets(new S_SystemMessage("로또게임 응모금액이 부족합니다."));
			}
		} else {
			pc.sendPackets(new S_SystemMessage("이미 응모 하였습니다."));
		}
		}else{
			pc.sendPackets(new S_SystemMessage("지금은 로또게임중이 아니므로 사용할 수 없습니다."));
		}
		}*/
	
	private boolean createNewItem(L1PcInstance pc, int item_id, int count) {
		L1ItemInstance item = ItemTable.getInstance().createItem(item_id);
		if (item != null) {
			item.setCount(count);
			if (pc.getInventory().checkAddItem(item, count) == L1Inventory.OK) {
				pc.getInventory().storeItem(item);
			} else { // 가질 수 없는 경우는 지면에 떨어뜨리는 처리의 캔슬은 하지 않는다(부정 방지)
				L1World.getInstance()
						.getInventory(pc.getX(), pc.getY(), pc.getMapId())
						.storeItem(item);
			}
			pc.sendPackets(new S_ServerMessage(403, item.getLogName())); // %0를
																			// 손에
																			// 넣었습니다.
			return true;
		} else {
			return false;
		}
	}

	private static final int[] allBuffSkill = { L1SkillId.파이어실드,
			PHYSICAL_ENCHANT_DEX, PHYSICAL_ENCHANT_STR, BLESS_WEAPON,
			ADVANCE_SPIRIT };

	private void buff(L1PcInstance pc) {

		if (pc.isDead())
			return;

		long curtime = System.currentTimeMillis() / 1000;
		if (pc.getQuizTime() + 20 > curtime) {
			pc.sendPackets(new S_SystemMessage("20초간의 지연시간이 필요합니다."));
			return;
		}
		if (pc.getLevel() <= 59) {
			try {
				L1SkillUse l1skilluse = new L1SkillUse();
				for (int i = 0; i < allBuffSkill.length; i++) {
					l1skilluse.handleCommands(pc, allBuffSkill[i], pc.getId(),
							pc.getX(), pc.getY(), null, 0,
							L1SkillUse.TYPE_GMBUFF);
					pc.setQuizTime(curtime);
				}
			} catch (Exception e) {
			}
		} else {
			pc.sendPackets(new S_SystemMessage("59레벨 이후는 버프를 받을수 없습니다."));
		}
	}
	private static String currentTime() {
		TimeZone tz = TimeZone.getTimeZone(Config.TIME_ZONE);
		Calendar cal = Calendar.getInstance(tz);
		int year = cal.get(Calendar.YEAR) - 2000;
		String year2;
		if (year < 10) {
			year2 = "0" + year;
		} else {
			year2 = Integer.toString(year);
		}
		int Month = cal.get(Calendar.MONTH) + 1;
		String Month2 = null;
		if (Month < 10) {
			Month2 = "0" + Month;
		} else {
			Month2 = Integer.toString(Month);
		}
		int date = cal.get(Calendar.DATE);
		String date2 = null;
		if (date < 10) {
			date2 = "0" + date;
		} else {
			date2 = Integer.toString(date);
		}
		return year2 + "/" + Month2 + "/" + date2;
	}
}

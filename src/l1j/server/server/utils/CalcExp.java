/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */

package l1j.server.server.utils;

import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Logger;

import kr.PeterMonk.GameSystem.SupportSystem.L1SupportMap;
import kr.PeterMonk.GameSystem.SupportSystem.SupportMapTable;
import l1j.server.Config;
import l1j.server.GameSystem.EventShop.EventShop_아인_따스한_시선;
import l1j.server.GameSystem.EventShop.EventShop_할로윈;
import l1j.server.server.Account;
import l1j.server.server.GidunBlessController;
import l1j.server.server.GodeBlessController;
import l1j.server.server.LadunBlessController;
import l1j.server.server.MaldunBlessController;
import l1j.server.server.OmanBlessController;
import l1j.server.server.Opcodes;
import l1j.server.server.datatables.ExpTable;
import l1j.server.server.datatables.PetTable;
import l1j.server.server.model.L1Character;
import l1j.server.server.model.L1Clan;
import l1j.server.server.model.L1Object;
import l1j.server.server.model.L1World;
import l1j.server.server.model.Instance.L1DollInstance;
import l1j.server.server.model.Instance.L1ItemInstance;
import l1j.server.server.model.Instance.L1MonsterInstance;
import l1j.server.server.model.Instance.L1NpcInstance;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.model.Instance.L1PetInstance;
import l1j.server.server.model.Instance.L1SummonInstance;
import l1j.server.server.model.skill.L1SkillId;
import l1j.server.server.serverpackets.S_ACTION_UI;
import l1j.server.server.serverpackets.S_ChatPacket;
import l1j.server.server.serverpackets.S_ItemName;
//import l1j.server.server.serverpackets.S_Disconnect;
import l1j.server.server.serverpackets.S_PacketBox;
import l1j.server.server.serverpackets.S_PetPack;
import l1j.server.server.serverpackets.S_PetWindow;
import l1j.server.server.serverpackets.S_ServerMessage;
import l1j.server.server.serverpackets.S_SystemMessage;
import l1j.server.server.templates.L1Pet;
import server.GameServer;

// Referenced classes of package l1j.server.server.utils:
// CalcStat

public class CalcExp {

	// private static final long serialVersionUID = 1L;

	private static Logger _log = Logger.getLogger(CalcExp.class.getName());

	public static final int MAX_EXP = ExpTable.getExpByLevel(100) - 1;

	private static L1NpcInstance npc = null;

	private static Random _random = new Random(System.nanoTime()); // 추가해주세요.

	private CalcExp() {
	}

	public static void calcExp(L1PcInstance l1pcinstance, int targetid, ArrayList<?> acquisitorList, ArrayList<?> hateList, int exp) {
		try {
			int i = 0;
			double party_level = 0;
			double dist = 0;
			int member_exp = 0;
			int member_lawful = 0;
			L1Object l1object = L1World.getInstance().findObject(targetid);
			npc = (L1NpcInstance) l1object;

			// 헤이트의 합계를 취득
			L1Character acquisitor;
			int hate = 0;
			int acquire_exp = 0;
			int acquire_lawful = 0;
			int party_exp = 0;
			int party_lawful = 0;
			int totalHateExp = 0;
			int totalHateLawful = 0;
			int partyHateExp = 0;
			int partyHateLawful = 0;
			int ownHateExp = 0;

			if (acquisitorList.size() != hateList.size()) {
				return;
			}
			for (i = hateList.size() - 1; i >= 0; i--) {
				acquisitor = (L1Character) acquisitorList.get(i);
				hate = (Integer) hateList.get(i);
				if (acquisitor != null && !acquisitor.isDead()) {
					totalHateExp += hate;
					if (acquisitor instanceof L1PcInstance) {
						totalHateLawful += hate;
					}
				} else { // null였거나 죽어 있으면(자) 배제
					acquisitorList.remove(i);
					hateList.remove(i);
				}
			}
			if (totalHateExp == 0) { // 취득자가 없는 경우
				return;
			}

			if (l1object != null && !(npc instanceof L1PetInstance) && !(npc instanceof L1SummonInstance)) {
				// int exp = npc.get_exp();
				if (!L1World.getInstance().isProcessingContributionTotal() && l1pcinstance.getHomeTownId() > 0) {
					int contribution = npc.getLevel() / 10;
					l1pcinstance.addContribution(contribution);
				}
				int lawful = npc.getLawful();

				if (l1pcinstance.isInParty()) { // 파티중
					// 파티의 헤이트의 합계를 산출
					// 파티 멤버 이외에는 그대로 배분
					partyHateExp = 0;
					partyHateLawful = 0;
					for (i = hateList.size() - 1; i >= 0; i--) {
						acquisitor = (L1Character) acquisitorList.get(i);
						hate = (Integer) hateList.get(i);
						if (acquisitor instanceof L1PcInstance) {
							L1PcInstance pc = (L1PcInstance) acquisitor;
							if (pc == l1pcinstance) {
								partyHateExp += hate;
								partyHateLawful += hate;
							} else if (l1pcinstance.getParty().isMember(pc)) {
								partyHateExp += hate;
								partyHateLawful += hate;
							} else {
								if (totalHateExp > 0) {
									acquire_exp = (exp * hate / totalHateExp);
								}
								if (totalHateLawful > 0) {
									acquire_lawful = (lawful * hate / totalHateLawful);
								}
								AddExp(pc, acquire_exp, acquire_lawful);
							}
						} else if (acquisitor instanceof L1PetInstance) {
							L1PetInstance pet = (L1PetInstance) acquisitor;
							L1PcInstance master = (L1PcInstance) pet.getMaster();
							if (master == l1pcinstance) {
								partyHateExp += hate;
							} else if (l1pcinstance.getParty().isMember(master)) {
								partyHateExp += hate;
							} else {
								if (totalHateExp > 0) {
									acquire_exp = (exp * hate / totalHateExp);
								}
								AddExpPet(pet, acquire_exp);
							}
						} else if (acquisitor instanceof L1SummonInstance) {
							L1SummonInstance summon = (L1SummonInstance) acquisitor;
							L1PcInstance master = (L1PcInstance) summon.getMaster();
							if (master == l1pcinstance) {
								partyHateExp += hate;
							} else if (l1pcinstance.getParty().isMember(master)) {
								partyHateExp += hate;
							} else {
							}
						}
					}
					if (totalHateExp > 0) {
						party_exp = (exp * partyHateExp / totalHateExp);
					}
					if (totalHateLawful > 0) {
						party_lawful = (lawful * partyHateLawful / totalHateLawful);
					}

					// EXP, 로우훌 배분

					// 프리보나스
					double pri_bonus = 0;
					L1PcInstance leader = l1pcinstance.getParty().getLeader();
					if (leader.isCrown() && (l1pcinstance.getNearObjects().knownsObject(leader) || l1pcinstance.equals(leader))) {
						pri_bonus = 0.059;
					}

					// PT경험치의 계산
					L1PcInstance[] ptMembers = l1pcinstance.getParty().getMembers();
					double pt_bonus = 0;
					for (L1PcInstance each : l1pcinstance.getParty().getMembers()) {
						if (l1pcinstance.getNearObjects().knownsObject(each) || l1pcinstance.equals(each)) {
							party_level += each.getLevel() * each.getLevel();
						}
						if (l1pcinstance.getNearObjects().knownsObject(each)) {
							pt_bonus += 0.04;

						}
					}

					party_exp = (int) (party_exp * (1 + pt_bonus + pri_bonus));
					
					if(hateList.size() >= 2 && l1pcinstance.isInParty() && (l1pcinstance.getMapId() == 12862)){
						party_exp *= (hateList.size() * Config.PT_EXP);
					}
					
					// 자캐릭터와 그 애완동물·사몬의 헤이트의 합계를 산출
					if (party_level > 0) {
						dist = ((l1pcinstance.getLevel() * l1pcinstance.getLevel()) / party_level);
					}
					member_exp = (int) (party_exp * dist);
					member_lawful = (int) (party_lawful * dist);

					ownHateExp = 0;
					for (i = hateList.size() - 1; i >= 0; i--) {
						acquisitor = (L1Character) acquisitorList.get(i);
						hate = (Integer) hateList.get(i);
						if (acquisitor instanceof L1PcInstance) {
							L1PcInstance pc = (L1PcInstance) acquisitor;
							if (pc == l1pcinstance) {
								ownHateExp += hate;
							}
						} else if (acquisitor instanceof L1PetInstance) {
							L1PetInstance pet = (L1PetInstance) acquisitor;
							L1PcInstance master = (L1PcInstance) pet
									.getMaster();
							if (master == l1pcinstance) {
								ownHateExp += hate;
							}
						} else if (acquisitor instanceof L1SummonInstance) {
							L1SummonInstance summon = (L1SummonInstance) acquisitor;
							L1PcInstance master = (L1PcInstance) summon.getMaster();
							if (master == l1pcinstance) {
								ownHateExp += hate;
							}
						}
					}
					// 자캐릭터와 그 애완동물·사몬에 분배
					if (ownHateExp != 0) { // 공격에 참가하고 있었다
						for (i = hateList.size() - 1; i >= 0; i--) {
							acquisitor = (L1Character) acquisitorList.get(i);
							hate = (Integer) hateList.get(i);
							if (acquisitor instanceof L1PcInstance) {
								L1PcInstance pc = (L1PcInstance) acquisitor;
								if (pc == l1pcinstance) {
									if (ownHateExp > 0) {
										acquire_exp = (member_exp * hate / ownHateExp);
									}
									AddExp(pc, acquire_exp, member_lawful);
								}
							} else if (acquisitor instanceof L1PetInstance) {
								L1PetInstance pet = (L1PetInstance) acquisitor;
								L1PcInstance master = (L1PcInstance) pet
										.getMaster();
								if (master == l1pcinstance) {
									if (ownHateExp > 0) {
										acquire_exp = (member_exp * hate / ownHateExp);
									}
									AddExpPet(pet, acquire_exp);
								}
							} else if (acquisitor instanceof L1SummonInstance) {
							}
						}
					} else { // 공격에 참가하고 있지 않았다
						// 자캐릭터에만 분배
						AddExp(l1pcinstance, member_exp, member_lawful);
					}

					// 파티 멤버와 그 애완동물·사몬의 헤이트의 합계를 산출
					for (int cnt = 0; cnt < ptMembers.length; cnt++) {
						if (l1pcinstance.getNearObjects().knownsObject(ptMembers[cnt])) {
							if (party_level > 0) {
								dist = ((ptMembers[cnt].getLevel() * ptMembers[cnt]
										.getLevel()) / party_level);
							}
							member_exp = (int) (party_exp * dist);
							member_lawful = (int) (party_lawful * dist);
							ownHateExp = 0;
							for (i = hateList.size() - 1; i >= 0; i--) {
								acquisitor = (L1Character) acquisitorList.get(i);
								hate = (Integer) hateList.get(i);
								if (acquisitor instanceof L1PcInstance) {
									L1PcInstance pc = (L1PcInstance) acquisitor;
									if (pc == ptMembers[cnt]) {
										ownHateExp += hate;
									}
								} else if (acquisitor instanceof L1PetInstance) {
									L1PetInstance pet = (L1PetInstance) acquisitor;
									L1PcInstance master = (L1PcInstance) pet
											.getMaster();
									if (master == ptMembers[cnt]) {
										ownHateExp += hate;
									}
								} else if (acquisitor instanceof L1SummonInstance) {
									L1SummonInstance summon = (L1SummonInstance) acquisitor;
									L1PcInstance master = (L1PcInstance) summon
											.getMaster();
									if (master == ptMembers[cnt]) {
										ownHateExp += hate;
									}
								}
							}
							// 파티 멤버와 그 애완동물·사몬에 분배
							if (ownHateExp != 0) { // 공격에 참가하고 있었다
								for (i = hateList.size() - 1; i >= 0; i--) {
									acquisitor = (L1Character) acquisitorList
											.get(i);
									hate = (Integer) hateList.get(i);
									if (acquisitor instanceof L1PcInstance) {
										L1PcInstance pc = (L1PcInstance) acquisitor;
										if (pc == ptMembers[cnt]) {
											if (ownHateExp > 0) {
												acquire_exp = (member_exp
														* hate / ownHateExp);
											}
											AddExp(pc, acquire_exp,
													member_lawful);
										}
									} else if (acquisitor instanceof L1PetInstance) {
										L1PetInstance pet = (L1PetInstance) acquisitor;
										L1PcInstance master = (L1PcInstance) pet
												.getMaster();
										if (master == ptMembers[cnt]) {
											if (ownHateExp > 0) {
												acquire_exp = (member_exp * hate / ownHateExp);
											}
											AddExpPet(pet, acquire_exp);
										}
									} else if (acquisitor instanceof L1SummonInstance) {
									}
								}
							} else { // 공격에 참가하고 있지 않았다
								// 파티 멤버에만 분배
								AddExp(ptMembers[cnt], member_exp, member_lawful);
							}
						}
					}
				} else { // 파티를 짜지 않았다
					// EXP, 로우훌의 분배
					for (i = hateList.size() - 1; i >= 0; i--) {
						acquisitor = (L1Character) acquisitorList.get(i);
						hate = (Integer) hateList.get(i);
						acquire_exp = (exp * hate / totalHateExp);
						if (acquisitor instanceof L1PcInstance) {
							if (totalHateLawful > 0) {
								acquire_lawful = (lawful * hate / totalHateLawful);
							}
						}

						if (acquisitor instanceof L1PcInstance) {
							L1PcInstance pc = (L1PcInstance) acquisitor;
							AddExp(pc, acquire_exp, acquire_lawful);
						} else if (acquisitor instanceof L1PetInstance) {
							L1PetInstance pet = (L1PetInstance) acquisitor;
							AddExpPet(pet, acquire_exp);
						} else if (acquisitor instanceof L1SummonInstance) {
						}
					}
				}
			}

		} catch (Exception e) {
		}
	}

	private static void AddExp(L1PcInstance pc, int exp, int lawful) {
		/** 서버 오픈 대기 */
		if (Config.STANDBY_SERVER) {
			return;
		}

		if (pc.getLevel() > Config.MAXLEVEL) {
			return;
		}
		if (pc.isDead())
			return;

		int pclevel = pc.getLevel();

		if (pclevel >= 45) {
			if (npc instanceof L1MonsterInstance) {
				L1MonsterInstance mon = (L1MonsterInstance) npc;
				if ((mon.getNpcId() >= 100225 && mon.getNpcId() <= 100231)
						|| (mon.getNpcId() >= 100236 && mon.getNpcId() <= 100241)) {
					return;
				}
			}
		}

		int add_lawful = (int) (lawful * Config.RATE_LAWFUL) * -1;
		pc.addLawful(add_lawful);

		if (npc instanceof L1MonsterInstance) {
			L1MonsterInstance mon = (L1MonsterInstance) npc;
			if (mon.getUbId() != 0) {
				int ubexp = (exp / 10);
				pc.setUbScore(pc.getUbScore() + ubexp);
			}
		}

		double exppenalty = ExpTable.getPenaltyRate(pclevel);
		double foodBonus = 1;
		double expposion = 1;
		double levelBonus = 1;
		// double ainhasadBonus = 1;
		double clanBonus = 1;
		double castleBonus = 1;
		double dollBonus = 1.0;
		double gereng = 1;
		double dragoneme = 1;
		double etcBonus = 1;
		double 진귀한 = 1;
		double clan20Bonus = 1;
		double levelupBonus = 1;
		double 신규지원 = 1;
		double bjbuff = 1;
		double MonkeyBonus = 1;
		double comboBonus = 1;
		double expItemBonusByBadge = 1;
		double dragonBooster = 1;
		double winnersbuff = 1;
		double newyear = 1;
		double dragonexp = 1;
		double 고정인증 = 1;

		if(Config.주말버프){
			newyear += 0.2;
		}
		if (pc.getDessertId() == L1SkillId.COOKING_NEW_닭고기 || pc.getDessertId() == L1SkillId.COOKING_축복_닭고기) {
			foodBonus = 1.04;
		} else if (pc.getDessertId() == L1SkillId.메티스정성스프 || pc.getDessertId() == L1SkillId.싸이시원한음료) {
			foodBonus = 1.05;
		} else if (pc.getDessertId() == L1SkillId.천하장사버프) {
			foodBonus = 1.2;
		}
		if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.고정인증버프)){
			고정인증 += 0.1;
		}
		if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.드래곤의성장버프)){
			if(pc.getLevel() <= 79)
				dragonexp += 0.8;
			else if(pc.getLevel() >= 80 && pc.getLevel() <= 81)
				dragonexp += 0.7;
			else if(pc.getLevel() >= 82 && pc.getLevel() <= 83)
				dragonexp += 0.6;
			else if(pc.getLevel() >= 84 && pc.getLevel() <= 85)
				dragonexp += 0.5;
			else if(pc.getLevel() == 86)
				dragonexp += 0.4;
			else if(pc.getLevel() == 87)
				dragonexp += 0.3;
			else if(pc.getLevel() == 88)
				dragonexp += 0.2;
			else if(pc.getLevel() >= 89)
				dragonexp += 0.1;
		}

		if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.EXP_POTION)
				||pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.웃픈버프1)
				|| pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.STATUS_COMA_5)
				|| pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.EXP_POTION_cash)
				|| pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.EXP_POTION)
				||pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.웃픈버프1)) {
		if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.EXP_POTION_cash)
				|| pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.EXP_POTION)
				||pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.웃픈버프1)) {
				if (pc.PC방_버프) {
					expposion = 1.3;
				} else {
					expposion = 1.2;
				}
			} else {
				expposion = 1.2;
			}
		}
		if(pc.isrankexp()){
			dragonexp += 0.1;
		}
		if(pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.BJBUFF)){
			bjbuff = 1.2;
		}
		if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.EXP_POTION2)) {
			gereng = 1.4;
		}
		if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.EXP_POTION3)) {
			gereng = 1.3;
		}
		if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.별풍선3단)
				||pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.별풍선4단)
				||pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.별풍선5단)) {
			winnersbuff = 1.2;
		}
		
		if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.DRAGON_EME_2) && pc.getAinHasad() > 10000) {
			if((GidunBlessController.getInstance().getGidunStart() && pc.getMapId() >= 53 && pc.getMapId() <= 56)||
				(GodeBlessController.getInstance().getGodeStart() &&  pc.getMapId() == 12862) || 
				(MaldunBlessController.getInstance().getMaldunStart() && pc.getMapId() >= 32 && pc.getMapId() <= 38) || 
				(OmanBlessController.getInstance().getOmanStart() && pc.getMapId() >= 101 && pc.getMapId() <= 111) ||
				(LadunBlessController.getInstance().getLadunStart() &&  (pc.getMapId() == 1708 || pc.getMapId() == 1703))){
				dragoneme = 4.2;
			} else {
				dragoneme = 2.8;
			}
		} else if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.DRAGON_PUPLE) && pc.getAinHasad() > 10000) {
			if (pc.getLevel() >= 49 && pc.getLevel() <= 54)
				dragoneme = 1.53;
			else if (pc.getLevel() >= 55 && pc.getLevel() <= 59)
				dragoneme = 1.43;
			else if (pc.getLevel() >= 60 && pc.getLevel() <= 64)
				dragoneme = 1.33;
			else if (pc.getLevel() >= 65)
				dragoneme = 1.23;
			if (pc.getAinHasad() <= 10000) {
				pc.getSkillEffectTimerSet().removeSkillEffect(
						L1SkillId.DRAGON_PUPLE);
			}
		} else if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.DRAGON_TOPAZ) && pc.getAinHasad() > 10000) {
			if((GidunBlessController.getInstance().getGidunStart() && pc.getMapId() >= 53 && pc.getMapId() <= 56)||
					(GodeBlessController.getInstance().getGodeStart() &&  pc.getMapId() == 12862) || 
					(MaldunBlessController.getInstance().getMaldunStart() && pc.getMapId() >= 32 && pc.getMapId() <= 38) || 
					(OmanBlessController.getInstance().getOmanStart() && pc.getMapId() >= 101 && pc.getMapId() <= 111) ||
					(LadunBlessController.getInstance().getLadunStart() &&  (pc.getMapId() == 1708 || pc.getMapId() == 1703))){
			dragoneme = 4.2;
			} else {
			dragoneme = 2.8;
			}
			if (pc.getAinHasad() <= 10000) {pc.getSkillEffectTimerSet().removeSkillEffect(L1SkillId.DRAGON_TOPAZ);
			}
		}
		if (pc.getInventory().checkEquipped(121216) || pc.getInventory().checkEquipped(421216) ) {
			etcBonus += 0.02;
		}
		if (pc.getInventory().checkEquipped(421217) || pc.getInventory().checkEquipped(421218) || pc.getInventory().checkEquipped(421219)) {
			etcBonus += 0.1;
		}
		
		if (pc.getInventory().checkEquipped(10110)) {
			etcBonus += 0.2;
		}
		
		if (pc.getInventory().checkEquipped(621217)) {
			etcBonus += 0.4;
		}
		if (pc.getInventory().checkEquipped(2222384)||pc.getInventory().checkEquipped(2222385)||pc.getInventory().checkEquipped(2222386)) {
			etcBonus += 2.0;
		}
		
		if (pc.getInventory().checkEquipped(221216) || pc.getInventory().checkEquipped(431219)) {
			etcBonus += 0.05;
		}
		if (pc.getInventory().checkEquipped(321216)) {
			etcBonus += 0.05;
		}
		if (pc.getInventory().checkEquipped(4800)) {
			MonkeyBonus = 1.01;
		}
		if (pc.getInventory().checkEquipped(4801)) {
			MonkeyBonus = 1.02;
		}
		if (pc.getInventory().checkEquipped(4801)) {
			MonkeyBonus = 1.03;
		}
		if (pc.getInventory().checkEquipped(4803)) {
			MonkeyBonus = 1.04;
		}
		if (pc.getInventory().checkEquipped(4804)) {
			MonkeyBonus = 1.05;
		}
		if (pc.getInventory().checkEquipped(4805)) {
			MonkeyBonus = 1.06;
		}
		if (pc.getInventory().checkEquipped(4806)) {
			MonkeyBonus = 1.07;
		}
		if (pc.getInventory().checkEquipped(4807)) {
			MonkeyBonus = 1.08;
		}
		if (pc.getInventory().checkEquipped(4808)) {
			MonkeyBonus = 1.10;
		}
		if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.STATUS_진귀한식량)) {
			진귀한 = 1.25;
		}
		if (pc.getInventory().checkEquipped(31913)){
			L1ItemInstance item = pc.getInventory().checkEquippedItem(31913);
			int enchant = item.getEnchantLevel();
			double bonus = 0;
			switch (enchant){
				case 1:
					bonus = 0.01;
					break;
				case 2:
					bonus = 0.02;
					break;
				case 3:
					bonus = 0.03;
					break;
				case 4:
					bonus = 0.04;
					break;
				case 5:
					bonus = 0.05;
					break;
				case 6:
					bonus = 0.06;
					break;
				case 7:
					bonus = 0.07;
					break;
				case 8:
					bonus = 0.08;
					break;
				default:
					bonus = 0;
					break;
			}
			expItemBonusByBadge += bonus;
		}
		
		if (pc.getInventory().checkEquipped(9114)){
			L1ItemInstance item = pc.getInventory().checkEquippedItem(9114);
			int enchant = item.getEnchantLevel();
			double bonus = 0;
			switch (enchant){
				case 8:
					bonus = 0.02;
					break;
				case 9:
					bonus = 0.04;
					break;
				case 10:
					bonus = 0.06;
					break;
				default:
					bonus = 0;
					break;
			}
			expItemBonusByBadge += bonus;
		}
		
		if (pc.getInventory().checkEquipped(91140)){
			L1ItemInstance item = pc.getInventory().checkEquippedItem(91140);
			int enchant = item.getEnchantLevel();
			double bonus = 0;
			switch (enchant){
				case 7:
					bonus = 0.02;
					break;
				case 8:
					bonus = 0.04;
					break;
				case 9:
					bonus = 0.06;
					break;
				case 10:
					bonus = 0.08;
					break;
				default:
					bonus = 0;
					break;
			}
			expItemBonusByBadge += bonus;
		}
		
		if(pc.getResistance().getAinBooster() != 0){
			dragonBooster += pc.getResistance().getAinBooster()/100;
		}
		
		if (GameServer.신규지원_경험치지급단 && pc.getLevel() <= 60){
			신규지원 = 1.50;
		}
		if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.레벨업보너스)){
			levelupBonus = 2.23;
		}
		if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.이스마엘의가호)){
			castleBonus *= 1.2;
		}
		if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.노나메의가호)){
			castleBonus *= 1.2;
		}
		
		if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.Tam_Fruit5)){
			etcBonus += 0.05;
		}

		if (pc.getAinHasad() > 10000) {
			if (pc.getAinHasad() > 2000000) {
				gereng += 1.3;
			} else {
				gereng += 1;
			}

			if (EventShop_할로윈.진행()) {
				gereng += 0.77;
			}
			if (EventShop_아인_따스한_시선.진행()) {
				if (EventShop_아인_따스한_시선.EventZone(pc)) {
					gereng += 0.50;
				}
			}
			if (pc.PC방_버프) {
				gereng += 0.20;
			}
		}
		// }
		for (L1DollInstance doll : pc.getDollList()) {
			dollBonus = doll.getAddExpByDoll();
		} // 추가

		L1Clan clan = L1World.getInstance().getClan(pc.getClanname());
		if (clan != null) {
			if (pc.혈맹버프) {
				clan20Bonus = 1.1;
			}
		}
		
		int newchar = 1;

		int settingEXP = (int) Config.RATE_XP;

		if (Config.룸티스_Event) {
			if (Config.룸티스드랍진행중) {
				if (dragoneme != 1) {
					dragoneme *= 2;
				}
			}
		}
		
		if(pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.COMBO_BUFF)) {
			if(pc.getComboCount() > 0 && pc.getComboCount() <= 30){
				comboBonus = 1 + (pc.getComboCount() * 0.1);
			} else if(pc.getComboCount() > 30){
				comboBonus = 3;
			}
		}
				
		int add_exp = (int) (exp * settingEXP * foodBonus * expposion * bjbuff
				* levelBonus * exppenalty * newchar * clanBonus * castleBonus * dragonBooster
				* dollBonus * gereng * dragoneme * etcBonus * 진귀한 * clan20Bonus * dragonexp * 고정인증
				* levelupBonus * MonkeyBonus * comboBonus * expItemBonusByBadge * winnersbuff * 신규지원 * newyear);
		
		if(pc.getMapId() >= 12852 && pc.getMapId() <= 12862){
			add_exp *= 1.3;
		}
		
		pc.calAinHasad(-(int)(exp * comboBonus* dragonBooster * dragoneme * dragonexp));
		pc.sendPackets(new S_ACTION_UI(S_ACTION_UI.EINHASAD, pc));
		
		if(pc.getAinHasad() < -500000){
			pc.sendPackets(new S_SystemMessage(pc,"[그랑카인의 저주] 몬스터들의 공격력이 20배 증가합니다."), true);
			pc.sendPackets(new S_SystemMessage(pc,"[그랑카인의 저주] 아인하사드의 축복이 필요합니다."), true);
		}
		
		if (pc.isAutoPlay()) {
			if (SupportMapTable.getInstance().isSupportMap(pc.getMapId())) {
				L1SupportMap SM = SupportMapTable.getInstance().getSupportMap(pc.getMapId());
				if (SM != null) {
					add_exp *= SM.getExpRate();
				}
			}
		}
		
		if (add_exp < 0) {
			return;
		}

		if (ExpTable.getExpByLevel(Config.MAXLEVEL + 1) <= pc.getExp() + add_exp) {
			pc.setExp(ExpTable.getExpByLevel(Config.MAXLEVEL + 1) - 1);
		} else {
			pc.addExp(add_exp);
		}

	}
	
	
	public static void AddExpPet(L1PetInstance pet, int exp) {
		L1PcInstance pc = (L1PcInstance) pet.getMaster();
		int levelBefore = pet.getLevel();

		
		int BonusExp = exp * 10;
		if(pet.SkillCheck(L1SkillId.GrowthFoliage)) BonusExp *= 1.3;
		int totalExp = (int)BonusExp + pet.getExp();
		if (totalExp >= ExpTable.getExpByLevel(100)) {
			totalExp = ExpTable.getExpByLevel(100) - 1;
		}
		pet.setExp(totalExp);
		pet.setLevel(ExpTable.getLevelByExp(totalExp));
		pc.sendPackets(new S_PetWindow(S_PetWindow.PatExp, pet), true);
		
		
		int FightingBonuseExp = (int)Math.round((exp)/100.0) * 100;
		if(FightingBonuseExp < 5000) FightingBonuseExp = 5000;
		if(FightingBonuseExp >= 20000) FightingBonuseExp = 20000;
		pet.Fighting(FightingBonuseExp);

		int gap = pet.getLevel() - levelBefore;
		for (int i = 1; i <= gap; i++) {
			IntRange hpUpRange = pet.getPetType().getHpUpRange();
			pet.addMaxHp(hpUpRange.randomValue());
			pet.setCurrentHp(pet.getMaxHp());
		}
		try {
			pc.sendPackets(new S_PetPack(pet, pc), true);
		} catch (Exception e) {}
		
		if (gap != 0) {
			
			pet.getPetAc();
			pet.getPetMr();
			
			int BonusPoint = pet.getHunt() + pet.getSurvival() + pet.getSacred();
			int BonusPointTemp = 0;
			if(pet.getLevel() > 50){
				BonusPointTemp = (5 + (pet.getLevel() - 50)) - BonusPoint;
			}else BonusPointTemp = (pet.getLevel() / 10) - BonusPoint;
			if(BonusPointTemp > 0) pet.setBonusPoint(BonusPointTemp);
			L1ItemInstance item = pc.getInventory().getItem(pet.getItemObjId());
			PetTable.UpDatePet(pet); 
			pc.sendPackets(new S_ItemName(item), true);;
			pc.sendPackets(new S_PetWindow(S_PetWindow.PatLevel, pet), true);
			pc.sendPackets(new S_PetWindow(S_PetWindow.PatStatUpDate, pet), true);
			try {
				pc.sendPackets(new S_ServerMessage(320, pet.getName()), true);
			} catch (Exception e) {}
		}
	}

	private static void CheckQuize(L1PcInstance pc) {
		Account account = Account.load(pc.getAccountName());

		int rnd = 0;
		rnd = _random.nextInt(100);

		if (pc.getLevel() >= 70 && rnd < 10
				&& pc.getMap().isNormalZone(pc.getLocation())) {

			if (account.getquize() == null || account.getquize() == "") {
				String chatText = "퀴즈가 설정되지 않았습니다 해킹 방지를 위해 퀴즈를 설정해 주세요.     예).퀴즈설정 1234";
				S_ChatPacket s_chatpacket = new S_ChatPacket(pc, chatText,
						Opcodes.S_SAY, 2);
				if (!pc.getExcludingList().contains(pc.getName())) {
					pc.sendPackets(s_chatpacket);
				}
				pc.sendPackets(new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
						chatText), true);
			}
		}
	}

}

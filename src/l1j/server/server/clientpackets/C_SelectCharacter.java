/* This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */

package l1j.server.server.clientpackets;

import static l1j.server.server.model.skill.L1SkillId.ADDITIONAL_FIRE;
import static l1j.server.server.model.skill.L1SkillId.ANTA_MAAN;
import static l1j.server.server.model.skill.L1SkillId.BERSERKERS;
import static l1j.server.server.model.skill.L1SkillId.BIRTH_MAAN;
import static l1j.server.server.model.skill.L1SkillId.CLEAR_MIND;
import static l1j.server.server.model.skill.L1SkillId.CONCENTRATION;
import static l1j.server.server.model.skill.L1SkillId.COOKING_1_0_N;
import static l1j.server.server.model.skill.L1SkillId.COOKING_1_0_S;
import static l1j.server.server.model.skill.L1SkillId.COOKING_1_14_N;
import static l1j.server.server.model.skill.L1SkillId.COOKING_1_14_S;
import static l1j.server.server.model.skill.L1SkillId.COOKING_1_16_N;
import static l1j.server.server.model.skill.L1SkillId.COOKING_1_16_S;
import static l1j.server.server.model.skill.L1SkillId.COOKING_1_22_N;
import static l1j.server.server.model.skill.L1SkillId.COOKING_1_22_S;
import static l1j.server.server.model.skill.L1SkillId.COOKING_1_6_N;
import static l1j.server.server.model.skill.L1SkillId.COOKING_1_6_S;
import static l1j.server.server.model.skill.L1SkillId.COOKING_1_8_N;
import static l1j.server.server.model.skill.L1SkillId.COOKING_1_8_S;
import static l1j.server.server.model.skill.L1SkillId.COOKING_NEW_닭고기;
import static l1j.server.server.model.skill.L1SkillId.COOKING_NEW_한우;
import static l1j.server.server.model.skill.L1SkillId.COOKING_축복_닭고기;
import static l1j.server.server.model.skill.L1SkillId.COOKING_축복_연어;
import static l1j.server.server.model.skill.L1SkillId.COOKING_축복_칠면조;
import static l1j.server.server.model.skill.L1SkillId.COOKING_축복_한우;
import static l1j.server.server.model.skill.L1SkillId.DECAY_POTION;
import static l1j.server.server.model.skill.L1SkillId.DECREASE_WEIGHT;
import static l1j.server.server.model.skill.L1SkillId.DISEASE;
import static l1j.server.server.model.skill.L1SkillId.DRAGON_SKIN;
import static l1j.server.server.model.skill.L1SkillId.DRESS_EVASION;
import static l1j.server.server.model.skill.L1SkillId.EARTH_BIND;
import static l1j.server.server.model.skill.L1SkillId.ELEMENTAL_FALL_DOWN;
import static l1j.server.server.model.skill.L1SkillId.ELEMENTAL_FIRE;
import static l1j.server.server.model.skill.L1SkillId.ELEMENTAL_PROTECTION;
import static l1j.server.server.model.skill.L1SkillId.ERASE_MAGIC;
import static l1j.server.server.model.skill.L1SkillId.EXP_POTION;
import static l1j.server.server.model.skill.L1SkillId.EXP_POTION2;
import static l1j.server.server.model.skill.L1SkillId.EXP_POTION3;
import static l1j.server.server.model.skill.L1SkillId.FAFU_MAAN;
import static l1j.server.server.model.skill.L1SkillId.FEAR;
import static l1j.server.server.model.skill.L1SkillId.FEATHER_BUFF_A;
import static l1j.server.server.model.skill.L1SkillId.FEATHER_BUFF_B;
import static l1j.server.server.model.skill.L1SkillId.FEATHER_BUFF_C;
import static l1j.server.server.model.skill.L1SkillId.FEATHER_BUFF_D;
import static l1j.server.server.model.skill.L1SkillId.GUARD_BREAK;
import static l1j.server.server.model.skill.L1SkillId.HORROR_OF_DEATH;
import static l1j.server.server.model.skill.L1SkillId.INSIGHT;
import static l1j.server.server.model.skill.L1SkillId.LIFE_MAAN;
import static l1j.server.server.model.skill.L1SkillId.LIND_MAAN;
import static l1j.server.server.model.skill.L1SkillId.MORTAL_BODY;
import static l1j.server.server.model.skill.L1SkillId.NATURES_TOUCH;
import static l1j.server.server.model.skill.L1SkillId.PANIC;
import static l1j.server.server.model.skill.L1SkillId.PATIENCE;
import static l1j.server.server.model.skill.L1SkillId.POLLUTE_WATER;
import static l1j.server.server.model.skill.L1SkillId.RESIST_ELEMENTAL;
import static l1j.server.server.model.skill.L1SkillId.RESIST_MAGIC;
import static l1j.server.server.model.skill.L1SkillId.SHAPE_CHANGE;
import static l1j.server.server.model.skill.L1SkillId.SHAPE_MAAN;
import static l1j.server.server.model.skill.L1SkillId.SILENCE;
import static l1j.server.server.model.skill.L1SkillId.SOUL_OF_FLAME;
import static l1j.server.server.model.skill.L1SkillId.SPECIAL_COOKING;
import static l1j.server.server.model.skill.L1SkillId.STATUS_BLUE_POTION;
import static l1j.server.server.model.skill.L1SkillId.STATUS_BLUE_POTION2;
import static l1j.server.server.model.skill.L1SkillId.STATUS_BLUE_POTION3;
import static l1j.server.server.model.skill.L1SkillId.STATUS_BRAVE;

import static l1j.server.server.model.skill.L1SkillId.STATUS_CASHSCROLL;
import static l1j.server.server.model.skill.L1SkillId.STATUS_CASHSCROLL2;
import static l1j.server.server.model.skill.L1SkillId.STATUS_CASHSCROLL3;
import static l1j.server.server.model.skill.L1SkillId.STATUS_CHAT_PROHIBITED;
import static l1j.server.server.model.skill.L1SkillId.STATUS_COMA_3;
import static l1j.server.server.model.skill.L1SkillId.STATUS_COMA_5;
import static l1j.server.server.model.skill.L1SkillId.STATUS_ELFBRAVE;
import static l1j.server.server.model.skill.L1SkillId.STATUS_FRUIT;
import static l1j.server.server.model.skill.L1SkillId.STATUS_HASTE;
import static l1j.server.server.model.skill.L1SkillId.STATUS_TIKAL_BOSSDIE;
import static l1j.server.server.model.skill.L1SkillId.STRIKER_GALE;
import static l1j.server.server.model.skill.L1SkillId.Tam_Fruit1;
import static l1j.server.server.model.skill.L1SkillId.Tam_Fruit2;
import static l1j.server.server.model.skill.L1SkillId.Tam_Fruit3;
import static l1j.server.server.model.skill.L1SkillId.Tam_Fruit4;
import static l1j.server.server.model.skill.L1SkillId.Tam_Fruit5;
import static l1j.server.server.model.skill.L1SkillId.VALA_MAAN;
import static l1j.server.server.model.skill.L1SkillId.VENOM_RESIST;
import static l1j.server.server.model.skill.L1SkillId.WEAKNESS;
import static l1j.server.server.model.skill.L1SkillId.싸이클론;
import static l1j.server.server.model.skill.L1SkillId.WIND_SHACKLE;
import static l1j.server.server.model.skill.L1SkillId.노나메의가호;
import static l1j.server.server.model.skill.L1SkillId.별풍선1단;
import static l1j.server.server.model.skill.L1SkillId.별풍선2단;
import static l1j.server.server.model.skill.L1SkillId.별풍선3단;
import static l1j.server.server.model.skill.L1SkillId.별풍선4단;
import static l1j.server.server.model.skill.L1SkillId.별풍선5단;
import static l1j.server.server.model.skill.L1SkillId.싸이매콤한라면;
import static l1j.server.server.model.skill.L1SkillId.이스마엘의가호;
import static l1j.server.server.model.skill.L1SkillId.천하장사버프;

import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javolution.util.FastTable;
import l1j.server.Config;
import l1j.server.L1DatabaseFactory;
import l1j.server.SpecialEventHandler;
import l1j.server.GameSystem.BossTimer;
import l1j.server.GameSystem.UserRanking.UserRankingController;
import l1j.server.server.Account;
import l1j.server.server.ActionCodes;
import l1j.server.server.GMCommands;
import l1j.server.server.GeneralThreadPool;
import l1j.server.server.SkillCheck;
import l1j.server.server.TimeController.InvSwapController;
import l1j.server.server.TimeController.WarTimeController;
import l1j.server.server.datatables.CharacterAttendTable;
import l1j.server.server.datatables.CharacterTable;
import l1j.server.server.datatables.ExcludeLetterTable;
import l1j.server.server.datatables.ExcludeTable;
import l1j.server.server.datatables.GetBackRestartTable;
import l1j.server.server.datatables.IpCheckTable;
import l1j.server.server.datatables.LetterTable;
import l1j.server.server.datatables.MonsterBookTable;
import l1j.server.server.datatables.SkillsTable;
import l1j.server.server.model.Broadcaster;
import l1j.server.server.model.Getback;
import l1j.server.server.model.L1CastleLocation;
import l1j.server.server.model.L1Character;
import l1j.server.server.model.L1Clan;
import l1j.server.server.model.L1Cooking;
import l1j.server.server.model.L1Location;
import l1j.server.server.model.L1Object;
import l1j.server.server.model.L1PolyMorph;
import l1j.server.server.model.L1War;
import l1j.server.server.model.L1World;
import l1j.server.server.model.Instance.L1DollInstance;
import l1j.server.server.model.Instance.L1ItemInstance;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.model.Instance.L1SummonInstance;
import l1j.server.server.model.map.L1Map;
import l1j.server.server.model.map.L1WorldMap;
import l1j.server.server.model.skill.L1SkillId;
import l1j.server.server.model.skill.L1SkillUse;
import l1j.server.server.serverpackets.S_ACTION_UI;
import l1j.server.server.serverpackets.S_AddSkill;
import l1j.server.server.serverpackets.S_AttenDance;
import l1j.server.server.serverpackets.S_CharStatElixir;
import l1j.server.server.serverpackets.S_CharVisualUpdate;
import l1j.server.server.serverpackets.S_CharacterConfig;
import l1j.server.server.serverpackets.S_ClanWindow;
import l1j.server.server.serverpackets.S_DRAGONPERL;
import l1j.server.server.serverpackets.S_Dexup;
import l1j.server.server.serverpackets.S_Disconnect;
import l1j.server.server.serverpackets.S_ElfIcon;
import l1j.server.server.serverpackets.S_EventNotice;
import l1j.server.server.serverpackets.S_HPUpdate;
import l1j.server.server.serverpackets.S_MPUpdate;
import l1j.server.server.serverpackets.S_MapID;
import l1j.server.server.serverpackets.S_MatizAlarm;
import l1j.server.server.serverpackets.S_MonsterUi;
import l1j.server.server.serverpackets.S_NewCreateItem;
import l1j.server.server.serverpackets.S_NewUI;
import l1j.server.server.serverpackets.S_OwnCharAttrDef;
import l1j.server.server.serverpackets.S_OwnCharStatus;
import l1j.server.server.serverpackets.S_OwnCharStatus2;
import l1j.server.server.serverpackets.S_PacketBox;
import l1j.server.server.serverpackets.S_Paralysis;
import l1j.server.server.serverpackets.S_Poison;
import l1j.server.server.serverpackets.S_ReturnedStat;
import l1j.server.server.serverpackets.S_SPMR;
import l1j.server.server.serverpackets.S_ServerMessage;
import l1j.server.server.serverpackets.S_SkillBrave;
import l1j.server.server.serverpackets.S_SkillHaste;
import l1j.server.server.serverpackets.S_SkillIconBlessOfEva;
import l1j.server.server.serverpackets.S_SkillIconGFX;
import l1j.server.server.serverpackets.S_SkillSound;
import l1j.server.server.serverpackets.S_Strup;
import l1j.server.server.serverpackets.S_SummonPack;
import l1j.server.server.serverpackets.S_SurvivalCry;
import l1j.server.server.serverpackets.S_SystemMessage;
import l1j.server.server.serverpackets.S_UnityIcon;
import l1j.server.server.serverpackets.S_Unknown1;
import l1j.server.server.serverpackets.S_War;
import l1j.server.server.serverpackets.S_Weather;
import l1j.server.server.serverpackets.S_WorldPutObject;
import l1j.server.server.serverpackets.S_bonusstats;
import l1j.server.server.serverpackets.S_문장주시;
import l1j.server.server.clientpackets.S_웃픈버프;
import l1j.server.server.templates.L1BookMark;
import l1j.server.server.templates.L1GetBackRestart;
import l1j.server.server.templates.L1Skills;
import l1j.server.server.utils.CheckInitStat;
import l1j.server.server.utils.SQLUtil;
import server.GameServer;
import server.LineageClient;
import server.manager.eva;
import server.system.autoshop.AutoShop;
import server.system.autoshop.AutoShopManager;
import server.threads.pc.AutoCheckThread;

//Referenced classes of package l1j.server.server.clientpackets:
//ClientBasePacket

public class C_SelectCharacter extends ClientBasePacket {
	private static final String C_LOGIN_TO_SERVER = "[C] C_LoginToServer";
	private static Logger _log = Logger.getLogger(C_SelectCharacter.class.getName());

	private static final int[] omanLocX = { 32781, 32818, 32818, 32818 };
	private static final int[] omanLocY = { 32781, 32781, 32818, 32781 };

	private static final Random ran = new Random();
	public static final SimpleDateFormat ss = new SimpleDateFormat("MM-dd HH:mm", Locale.KOREA);
	public static FastTable<String> nameList = new FastTable<String>();
	public static FastTable<String> nameINOUTList = new FastTable<String>();
	private static final int 수련자[] = { 21099, 21100, 21101, 21102, 21103, 21104, 21105, 21106, 21107, 21108, 21109,
			21110, 21111, 21112, 21254, 7, 35, 48, 73, 105, 120, 147, 156, 174, 175, 224, 7232 };

	@SuppressWarnings("deprecation")
	public C_SelectCharacter(byte abyte0[], LineageClient client) throws FileNotFoundException, Exception {
		super(abyte0);

		String charname = readS();
		/*
		 * int i = Config.이름확인(charname.toCharArray()); if(i < 0){
		 * System.out.println("한글 영어 숫자가 아닌 문자입니다."); }
		 */

		// System.out.println("접속자 이름 :"+charname);
		try {
			// client.sendPacket(new S_Test(true, true, true), true);
			// client.selectCharName = charName;
			GeneralThreadPool.getInstance().schedule(new login(charname, client), ran.nextInt(500));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static String HexToDex(int data, int digits) {
		String number = Integer.toHexString(data);
		for (int i = number.length(); i < digits; i++)
			number = "0" + number;
		return number;
	}

	public static String DataToPacket(byte[] data, int len) {
		StringBuffer result = new StringBuffer();
		int counter = 0;
		for (int i = 0; i < len; i++) {
			if (counter % 16 == 0) {
				result.append(HexToDex(i, 4) + ": ");
			}
			result.append(HexToDex(data[i] & 0xff, 2) + " ");
			counter++;
			if (counter == 16) {
				result.append("   ");
				int charpoint = i - 15;
				for (int a = 0; a < 16; a++) {
					int t1 = data[charpoint++];
					if (t1 > 0x1f && t1 < 0x80) {
						result.append((char) t1);
					} else {
						result.append('.');
					}
				}
				result.append("\n");
				counter = 0;
			}
		}
		int rest = data.length % 16;
		if (rest > 0) {
			for (int i = 0; i < 17 - rest; i++) {
				result.append("   ");
			}
			int charpoint = data.length - rest;
			for (int a = 0; a < rest; a++) {
				int t1 = data[charpoint++];
				if (t1 > 0x1f && t1 < 0x80) {
					result.append((char) t1);
				} else {
					result.append('.');
				}
			}
			result.append("\n");
		}
		return result.toString();
	}

	/*
	 * public C_SelectCharacter(byte abyte0[]){ super(abyte0); } public void
	 * clogin(String charName, LineageClient client){
	 * GeneralThreadPool.getInstance().schedule(new login(charName, client),
	 * ran.nextInt(500)); }
	 */

	class login implements Runnable {
		String name = "";
		LineageClient client = null;

		public login(String _name, LineageClient _client) {
			name = _name;
			client = _client;
		}

		@Override
		public void run() {
			try {
				if (name.equalsIgnoreCase("") || client == null /*
																 * || client.close
																 */)
					return;
				login_server(name, client);
			} catch (Exception e) {
			}
		}
	}

	public void commit(L1PcInstance pc) {
		Connection con = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con.prepareStatement("SELECT * FROM _Auto_Check WHERE Name=?");
			pstm.setString(1, pc.getName());
			rs = pstm.executeQuery();
			if (rs.next()) {
				pc.Auto_check = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			SQLUtil.close(rs);
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
	}

	private void login_server(String charName, LineageClient client) {
		try {
			if (client.getAccount() == null) {
				// System.out.println("계정 Null 접속 시도 " + charName);
				eva.LogFailServerAppend("Null 계정 접속 시도: " + charName);
				client.kick();
				client.close();
				return;
			}

			if (client.getActiveChar() != null) { // restart 상태가 아니란 소린가?
				// System.out.println("1계정 2케릭 버그 시도 : " + charName + " 감지코드
				// 1");
				eva.LogFailServerAppend("동일계정 2케릭 버그 시도: " + charName + "[감지코드 1]");
				client.kick();
				client.close();
				return;
			}

			if (GMCommands.접속이름체크) {
				if (nameList.contains(charName)) {
					if (L1World.getInstance().getPlayer(charName) == null) {
						GeneralThreadPool.getInstance().schedule(new Runnable() {
							@Override
							public void run() {
								GMCommands.connectCharNameReset();
							}
						}, 3000);
					} else
						GameServer.DuplicationLoginCheck(charName, "2초내 연속 접속");
					client.kick();
					client.close();
					return;
				}
				nameList.add(charName);
				GeneralThreadPool.getInstance().schedule(new charNameDelete(charName), 2000);
			}

			AutoShopManager shopManager = AutoShopManager.getInstance();
			AutoShop shopPlayer = shopManager.getShopPlayer(charName);
			if (shopPlayer != null) {
				L1PcInstance pc = L1World.getInstance().getPlayer(charName);
				shopPlayer.logout();
				shopManager.remove(shopPlayer);
				shopPlayer = null;
				pc.zombie = false;
			}
			if (GMCommands.접속이름체크) {
				if (nameINOUTList.contains(charName)) {
					if (L1World.getInstance().getPlayer(charName) == null) {
						GeneralThreadPool.getInstance().schedule(new Runnable() {
							@Override
							public void run() {
								GMCommands.connectCharNameReset();
							}
						}, 3000);
					} else
						GameServer.DuplicationLoginCheck(charName, "다중 접속");
					client.kick();
					client.close();
					return;
				}
				nameINOUTList.add(charName);
			}

			L1PcInstance pc = L1PcInstance.load(charName);
			String accountName = client.getAccountName();
			if (pc == null || !accountName.equals(pc.getAccountName())) {
				// System.out.println("현제 계정에 없는 케릭 접속시도 : " + charName + " 계정 :
				// "+ client.getAccountName());
				eva.LogFailServerAppend("무계정 접속 시도: " + charName + " " + accountName);
				client.kick();
				client.close();
				return;
			}

			// ///////////////// 스텟버그방지소스 /////////////////////
			L1PcInstance ckpc = L1World.getInstance().getPlayer(charName);
			if (ckpc != null && !ckpc.zombie && !ckpc.isPrivateShop()) {
				// System.out.println("1계정 2케릭 버그 시도 : " + charName + " 감지코드
				// 2");
				eva.LogFailServerAppend("동일계정 2케릭 버그 시도: " + charName + "[감지코드 2]");
				client.kick();
				client.close();
				ckpc.logout();
				if (ckpc.getNetConnection() != null)
					ckpc.getNetConnection().kick();
				return;
			}
			for (L1PcInstance _client2 : L1World.getInstance().getAllPlayers()) {
				if (_client2 == null || _client2.zombie || _client2.샌드백 || _client2.isPrivateShop()) {
					continue;
				}
				if (client.getAccountName().equalsIgnoreCase(_client2.getAccountName())) {
					// System.out.println("1계정 2케릭 버그 시도 : " + charName + " 감지코드
					// 3");
					eva.LogFailServerAppend("동일계정 2케릭 버그 시도: " + charName + "[감지코드 3]");
					client.kick();
					client.close();
					if (_client2.getNetConnection() != null) {
						_client2.getNetConnection().kick();
						_client2.getNetConnection().close();
					}
					return;
				}
			}

			if (pc.isGm()) {
				Config.add버그(pc);
				Config.add접속(pc);
			}

			if (pc.getLevel() > pc.getHighLevel()) {
				// System.out.println("정상 레벨보다 높은레벨 절단 : " + charName);
				eva.LogFailServerAppend("레벨 버그 접속 절단: " + charName);
				client.kick();
				client.close();
				return;
			}
			if (pc.getAbility().getCon() > 65 || pc.getAbility().getStr() > 65 || pc.getAbility().getDex() > 65
					|| pc.getAbility().getCha() > 65 || pc.getAbility().getInt() > 65
					|| pc.getAbility().getWis() > 65) {
				// System.out.println("한가지 스탯 65이상 절단 : " + charName);
				eva.LogFailServerAppend("스텟(65)오버 접속 절단: " + charName);
				client.kick();
				client.close();
				return;
			}

			// ///////////////// 스텟버그방지소스 /////////////////////

			String ip = client.getIp();
			try {
				// _log.info("캐릭터 로그인: char=" + charName + " account=" +
				// accountName + " host=" + client.getHostname());
				eva.LogServerAppend("접속", pc, client.getIp(), 1);
			} catch (Exception e) {
				// _log.info("캐릭터 로그인: char=" + charName + " account=" +
				// accountName + " host= 호스트 에러");
				eva.LogServerAppend("접속[호스트 에러]", pc, client.getIp(), 1);
			}
			pc.setPacketOutput(client);

			// pc.setPacketOutput(client);

			// 0319eva.LogServerAppend("접속", pc, client.getIp(), 1);
			// ChatMonitorChannel.getInstance().sendMsg(ChatMonitorChannel.CHAT_MONITOR_GM,
			// time +" [접속] "+pc.getName()+" - "+ client.getIp(), null);

			int currentHpAtLoad = pc.getCurrentHp();
			int currentMpAtLoad = pc.getCurrentMp();

			pc.clearSkillMastery();
			pc.setOnlineStatus(1);
			CharacterTable.updateOnlineStatus(pc);
			L1World.getInstance().storeObject(pc);
			pc.set_delete(false); // 허공버그픽스 추가

			pc.setNetConnection(client);

			client.setActiveChar(pc);

			if (GameServer.getInstance().checkip(ip)) {
				client.패킷로그 = true;
			}

			pc.상점아이템삭제(pc.getId());

			// if(pc.isGm())ChatMonitorChannel.getInstance().join(pc);

			/*
			 * ############### Point System ############### int at =
			 * client.getAccount().getAccountTime() * 60000; if(at > 0) { long la =
			 * client.getAccount().getLastActive().getTime(); long sum = la + at;
			 * pc.setLimitPointTime(sum/1000); pc.setPointUser(true); } ###############
			 * Point System ###############
			 */

			pc.sendPackets(new S_Unknown1(), true);

			// pc.sendPackets(new S_CharacterConfig());
			if (Config.CHARACTER_CONFIG_IN_SERVER_SIDE) {
				if (pc.getLevel() == 1) {
					pc.sendPackets(new S_CharacterConfig(pc.getId(), 1), true);
				} else {
					pc.sendPackets(new S_CharacterConfig(pc.getId()), true);
				}

			}

			pc.sendPackets(new S_PacketBox(S_PacketBox.char_ER, pc.getbase_Er()), true);
			// 0000: 76 84 03 v..

			pc.sendPackets(new S_PacketBox(S_PacketBox.char_ER, pc.get_Er()), true);
			// 0000: 76 84 0b

			items(pc);

			pc.sendPackets(new S_ReturnedStat(S_ReturnedStat.Unknown_LOGIN2, 0, 0), true);
			pc.sendPackets(new S_ReturnedStat(pc, S_ReturnedStat.LOGIN_EQUIP));

			// 현재 피 디비에서 불러오기
			pc.setCurrentHp(pc.get_loadHp());
			pc.setCurrentMp(pc.get_loadMp());

			pc.sendPackets(new S_PacketBox(S_PacketBox.WORLDMAP_UNKNOWN1), true);

			bookmarks(pc);

			pc.sendPackets(new S_ReturnedStat(pc, S_ReturnedStat.BOOKMARK), true);
			// pc.sendPackets(new S_PacketBox(pc, S_PacketBox.DG_TIME_RESTART),
			// true);

			skills(pc);
			loadSkills(pc);

			// pc.sendPackets(new S_NewCreateItem(0x91), true); //모름...

			// restart처가 getback_restart 테이블로 지정되고 있으면(자) 이동시킨다
			GetBackRestartTable gbrTable = GetBackRestartTable.getInstance();
			L1GetBackRestart[] gbrList = gbrTable.getGetBackRestartTableList();
			if (gbrList != null) {
				for (L1GetBackRestart gbr : gbrList) {
					if (pc.getMapId() == gbr.getArea()) {
						if (pc.getMapId() == 248 || pc.getMapId() == 249 || pc.getMapId() == 250
								|| pc.getMapId() == 251) {
							L1Clan clan = L1World.getInstance().getClan(pc.getClanname());
							if (clan != null) {
								if (clan.getCastleId() == 4) {
									break;
								}
							}
							pc.setX(33429);
							pc.setY(32814);
							pc.setMap((short) 4);
						}
						if (pc.getMapId() == 240 || pc.getMapId() == 241 || pc.getMapId() == 242
								|| pc.getMapId() == 243) {
							L1Clan clan = L1World.getInstance().getClan(pc.getClanname());
							if (clan != null) {
								if (clan.getCastleId() == 1) {
									break;
								}
							}
							pc.setX(33429);
							pc.setY(32814);
							pc.setMap((short) 4);
						}
						if (pc.getMapId() == 15) {
							L1Clan clan = L1World.getInstance().getClan(pc.getClanname());
							if (clan != null) {
								if (clan.getCastleId() == 1) {
									break;
								}
							}
						} else if (pc.getMapId() == 29) {
							L1Clan clan = L1World.getInstance().getClan(pc.getClanname());
							if (clan != null) {
								if (clan.getCastleId() == 3) {
									break;
								}
							}
						} else if (pc.getMapId() == 260) {
							L1Clan clan = L1World.getInstance().getClan(pc.getClanname());
							if (clan != null) {
								if (clan.getCastleId() == 2) {
									break;
								}
							}
						} else if (pc.getMapId() == 52) {
							L1Clan clan = L1World.getInstance().getClan(pc.getClanname());
							if (clan != null) {
								if (clan.getCastleId() == 4) {
									break;
								}
							}
						} else if (pc.getMapId() == 64) {
							L1Clan clan = L1World.getInstance().getClan(pc.getClanname());
							if (clan != null) {
								if (clan.getCastleId() == 5) {
									break;
								}
							}
						} else if (pc.getMapId() == 300) {
							L1Clan clan = L1World.getInstance().getClan(pc.getClanname());
							if (clan != null) {
								if (clan.getCastleId() == 7) {
									break;
								}
							}
						}
						pc.setX(gbr.getLocX());
						pc.setY(gbr.getLocY());
						pc.setMap(gbr.getMapId());
						break;
					}
				}
			}

			gbrList = null;

			if ((pc.getMapId() >= 271 && pc.getMapId() <= 278) || (pc.getMapId() >= 2100 && pc.getMapId() <= 2151)) {
				pc.setX(34060);
				pc.setY(32282);
				pc.setMap((short) 4);
			}

			if (pc.getMapId() == 4 && ((pc.getX() >= 33332 && pc.getX() <= 33338 && pc.getY() >= 32433
					&& pc.getY() <= 32439)
					|| (pc.getX() >= 33259 && pc.getX() <= 33265 && pc.getY() >= 32399 && pc.getY() <= 32405)
					|| (pc.getX() >= 33389 && pc.getX() <= 33395 && pc.getY() >= 32341 && pc.getY() <= 32347))) {
				L1Location newLocation = pc.getLocation().randomLocation(30, true);
				pc.setX(newLocation.getX());
				pc.setY(newLocation.getY());
			}

			if (pc.getMapId() == 814) {
				pc.setX(33616);
				pc.setY(33244);
				pc.setMap((short) 4);
			}

			if (pc.getMapId() == 2010 || (pc.getMapId() >= 2221 && pc.getMapId() <= 2235)) {
				pc.setX(33085);
				pc.setY(33391);
				pc.setMap((short) 4);
			}
			if (pc.getMapId() >= 2600 && pc.getMapId() <= 2699) {
				pc.setX(33702);
				pc.setY(32502);
				pc.setMap((short) 4);
			}
			if (pc.getMapId() >= 1400 && pc.getMapId() <= 1499) {
				pc.setX(33489);
				pc.setY(32764);
				pc.setMap((short) 4);
			}
			if (pc.getMapId() == 623) {
				pc.setX(33429);
				pc.setY(32831);
				pc.setMap((short) 4);
			}
			if (pc.getMapId() >= 2301 && pc.getMapId() <= 2350) {
				pc.setX(33438);
				pc.setY(32799);
				pc.setMap((short) 4);
			}
			if (pc.getMapId() >= 1936 && pc.getMapId() <= 1940) {
				pc.setX(33438);
				pc.setY(32799);
				pc.setMap((short) 4);
			}

			L1Map map = L1WorldMap.getInstance().getMap(pc.getMapId());
			// altsettings.properties로 GetBack가 true라면 거리에 이동시킨다
			int tile = map.getTile(pc.getX(), pc.getY());
			if (Config.GET_BACK || !map.isInMap(pc.getX(), pc.getY()) || tile == 0 || tile == 4 || tile == 12) {
				if (pc.getMapId() != 1703 && pc.getMapId() != 1708) {
					int[] loc = Getback.GetBack_Location(pc, true);
					try {
						pc.setX(loc[0]);
						pc.setY(loc[1]);
						pc.setMap((short) loc[2]);
					} catch (Exception e) {
						System.out.println("캐릭터 Get_Back Error: char=" + charName + " X: " + pc.getX() + " Y: "
								+ pc.getY() + " Tile: " + tile);
						_log.info("캐릭터 Get_Back Error: char=" + charName + " X: " + pc.getX() + " Y: " + pc.getY()
								+ " Tile: " + tile);
						e.printStackTrace();
					}
				}
			}

			if (pc.getMapId() == 101) {// 오만의 탑 귀환 설정일경우 1층으로 세팅되어있음
				int rnd = ran.nextInt(omanLocX.length);
				pc.setX(omanLocX[rnd]);
				pc.setY(omanLocY[rnd]);
				pc.setMap((short) 101);
			}

			if (pc.getMapId() == 410) {
				pc.setX(32929);
				pc.setY(32995);
				pc.setMap((short) 410);
			}
			if (pc.getMapId() == 5153 || pc.getMapId() == 5125 || pc.getMapId() == 5140 || pc.getMapId() == 5143) {
				pc.setX(33429);
				pc.setY(32814);
				pc.setMap((short) 4);
			}
			if ((pc.getMapId() >= 1005 && pc.getMapId() <= 1010) || (pc.getMapId() >= 1011 && pc.getMapId() <= 1016)
					|| (pc.getMapId() >= 10000 && pc.getMapId() <= 10005)
					|| (pc.getMapId() >= 11000 && pc.getMapId() <= 11005)) {
				pc.setX(33719);
				pc.setY(32506);
				pc.setMap((short) 4);
			}
			if (pc.getMapId() >= 9101 && pc.getMapId() <= 9199) {
				pc.setX(33442);
				pc.setY(32799);
				pc.setMap((short) 4);
			}
			if (pc.getMapId() >= 653 && pc.getMapId() <= 656) { // 수상한감옥 -> 수상한
																// 하늘정원
				pc.setX(32764);
				pc.setY(32836);
				pc.setMap((short) 622);
			}
			if (pc.getMapId() == 785 || pc.getMapId() == 788 || pc.getMapId() == 789) {
				pc.setX(33442);
				pc.setY(32799);
				pc.setMap((short) 4);
			}
			if (pc.getReturnStat() == 0 && pc.getMapId() == 5166) {
				pc.setX(32612);
				pc.setY(32734);
				pc.setMap((short) 4);
			}
			if (!pc.isGm() && (pc.getMapId() == 610 || pc.getMapId() == 612 || pc.getMapId() == 5554)) {
				pc.setX(33442);
				pc.setY(32799);
				pc.setMap((short) 4);
			}

			if ((pc.getMapId() == 420)) {
				pc.setX(32693);
				pc.setY(32800);
				pc.setMap((short) 450);
			}

			if (!pc.isGm() && (pc.getMapId() == 430)) {
				pc.setX(33442);
				pc.setY(32799);
				pc.setMap((short) 4);
			}
			if ((pc.getMapId() >= 9103 && pc.getMapId() <= 9200)) {
				pc.setX(32573);
				pc.setY(32940);
				pc.setMap((short) 0);
			}

			if (GMCommands.autocheck_Tellist.size() > 0) {
				if (GMCommands.autocheck_Tellist.contains(client.getAccountName()) && pc.getMapId() != 6202) {
					pc.setX(32928);
					pc.setY(32864);
					pc.setMap((short) 6202);
				}
			}

			if (!pc.isGm() && Config.LEVEL_DOWN_RANGE != 0) {
				if (pc.getHighLevel() - pc.getLevel() >= Config.LEVEL_DOWN_RANGE) {
					S_ServerMessage sm = new S_ServerMessage(64);
					pc.sendPackets(sm, true);
					S_Disconnect dis = new S_Disconnect();
					pc.sendPackets(dis, true);
					_log.info(String.format("[%s]: 렙따 범위를 넘었기 때문에 강제 절단 했습니다.", pc.getName()));
				}
			}

			// 전쟁중의 기내에 있었을 경우, 성주 혈맹이 아닌 경우는 귀환시킨다.
			int castle_id = L1CastleLocation.getCastleIdByArea(pc);
			if (pc.getMapId() == 66) {
				castle_id = 6;
			}
			if (0 < castle_id) {
				if (WarTimeController.getInstance().isNowWar(castle_id)) {
					L1Clan clan = L1World.getInstance().getClan(pc.getClanname());
					if (clan != null && clan.getCastleId() != castle_id) {
						int[] loc = new int[3];
						loc = L1CastleLocation.getGetBackLoc(castle_id);
						pc.setX(loc[0]);
						pc.setY(loc[1]);
						pc.setMap((short) loc[2]);
						loc = null;
					} else if (pc.getMapId() == 4) {
						int[] loc = new int[3];
						loc = L1CastleLocation.getGetBackLoc(castle_id);
						pc.setX(loc[0]);
						pc.setY(loc[1]);
						pc.setMap((short) loc[2]);
						loc = null;
					}
				}
			}

			L1World.getInstance().addVisibleObject(pc);
			// S_ActiveSpells s_activespells = new S_ActiveSpells(pc);
			// pc.sendPackets(s_activespells);

			pc.beginGameTimeCarrier();

			pc.sendPackets(new S_OwnCharStatus(pc), true);
			pc.sendPackets(new S_MapID(pc.getMapId(), pc.getMap().isUnderwater()), true);
			// pc.sendPackets(new S_OwnCharPack(pc), true);
			pc.sendPackets(new S_WorldPutObject(pc), true);

			buff(client, pc);

			pc.sendPackets(new S_Weather(L1World.getInstance().getWeather()), true);

			// 본섭은 위 아래 패킷 사이에 S_OPCODE_CASTLEMASTER 패킷이 온다 (총 9개)
			pc.sendCastleMaster();

			pc.sendPackets(new S_OwnCharStatus2(pc), true);
			pc.sendPackets(new S_SPMR(pc), true);

			pc.sendPackets(new S_ReturnedStat(pc, 4), true);

			// XXX 타이틀 정보는 S_OwnCharPack에 포함되므로 아마 불요
			// S_CharTitle s_charTitle = new S_CharTitle(pc.getId(),
			// pc.getTitle());
			// pc.sendPackets(s_charTitle);
			// Broadcaster.broadcastPacket(pc, s_charTitle);

			pc.sendVisualEffectAtLogin(); // 독, 수중 등의 시각 효과를 표시

			pc.sendPackets(new S_PacketBox(pc, S_PacketBox.KARMA), true);
			pc.sendPackets(new S_PacketBox(S_PacketBox.INIT_DG, 0x00));
			// pc.sendPackets(new S_PacketBox(S_PacketBox.UPDATE_DG, pc.getDg()));

			pc.sendPackets(new S_SurvivalCry(6000), true);

			pc.getLight().turnOnOffLight();

			// 존재버그 관련 추가
			L1PcInstance jonje = L1World.getInstance().getPlayer(pc.getName());
			if (jonje == null) {
				S_SystemMessage sm = new S_SystemMessage("존재버그 강제종료! 재접속하세요");
				pc.sendPackets(sm, true);
				client.kick();
				return;
			}

			if (pc.getGfxId().getTempCharGfx() == 11326 || pc.getGfxId().getTempCharGfx() == 11427
					|| pc.getGfxId().getTempCharGfx() == 10047 || pc.getGfxId().getTempCharGfx() == 9688
					|| pc.getGfxId().getTempCharGfx() == 11322 || pc.getGfxId().getTempCharGfx() == 10069
					|| pc.getGfxId().getTempCharGfx() == 10034 || pc.getGfxId().getTempCharGfx() == 10032) {
				pc.getSkillEffectTimerSet().killSkillEffectTimer(L1SkillId.SHAPE_CHANGE);
				L1PolyMorph.undoPoly(pc);
			}

			if (pc.getCurrentHp() > 0) {
				pc.setDead(false);
				pc.setActionStatus(0);
			} else {
				pc.setDead(true);
				pc.setActionStatus(ActionCodes.ACTION_Die);
			}

			if (pc.getLevel() >= 51 && pc.getLevel() - 50 > pc.getAbility().getBonusAbility()
					&& pc.getAbility().getAmount() < 150) {
				int temp = (pc.getLevel() - 50) - pc.getAbility().getBonusAbility();
				pc.sendPackets(new S_bonusstats(pc.getId(), temp), true);
			}

			if (pc.getReturnStat() != 0) {
				SpecialEventHandler.getInstance().ReturnStats(pc);
			}

			pc.sendPackets(new S_PacketBox(S_PacketBox.LOGIN_UNKNOWN3));
			pc.sendPackets(new S_EventNotice()); // 이벤트알람
			pc.sendPackets(new S_PacketBox(86, 0), true);// 모름1
			pc.sendPackets(new S_PacketBox(86, 1), true);// 모름2

			pc.sendPackets(new S_PacketBox(S_PacketBox.char_ER, pc.get_Er()), true);

			hasadbuff(pc);
			pc.sendPackets(new S_EventNotice()); // 이벤트알람
			pc.사망패널티(true);
			/** 잊섬 리뉴얼 패킷들 **/
			// pc.sendPackets(new S_NewCreateItem(S_NewCreateItem.신규패킷2, true),
			// true);
			pc.sendPackets(new S_NewCreateItem(S_NewCreateItem.신규패킷11, true), true); // 알람x
			pc.sendPackets(new S_NewCreateItem(S_NewCreateItem.신규패킷12, true), true); // 알람x
			pc.sendPackets(new S_NewCreateItem(S_NewCreateItem.신규패킷3, true), true); // 알람x
			pc.sendPackets(new S_NewCreateItem(S_NewCreateItem.신규패킷4, true), true); // 알람x
			pc.sendPackets(new S_NewCreateItem(S_NewCreateItem.신규패킷5, true), true); // 알람x
			pc.sendPackets(new S_NewCreateItem(S_NewCreateItem.신규패킷7, true), true); // 알람X
			pc.sendPackets(new S_NewCreateItem(S_NewCreateItem.신규패킷6, true), true); // 알람x
			pc.sendPackets(new S_NewCreateItem(S_NewCreateItem.신규패킷8, true), true);
			pc.sendPackets(new S_NewCreateItem(S_NewCreateItem.신규패킷10, true), true); // 알람X
			pc.sendPackets(new S_NewCreateItem(S_NewCreateItem.신규패킷13, true), true); // 알람X

			// 몬스터 도감 패킷이 변경됫나봐요 이거 보내니 팅기네요
			// pc.sendPackets(new S_MonsterUi());

			HashMap<Integer, Integer> mbq = MonsterBookTable.getInstace().getMonQuest(pc.getId());
			if (mbq != null)
				pc.sendPackets(new S_MonsterUi(S_MonsterUi.MONSTER_BOOK, mbq));
			else
				pc.sendPackets(new S_MonsterUi(S_MonsterUi.MONSTER_BOOK, null));

			HashMap<Integer, Integer> mbl = MonsterBookTable.getInstace().getMonBookList(pc.getId());
			if (mbl != null)
				pc.sendPackets(new S_MonsterUi(S_MonsterUi.MONSTER_LOAD, mbl));
			else
				pc.sendPackets(new S_MonsterUi(S_MonsterUi.MONSTER_LOAD, null));

			북(pc);
			/** 잊섬 리뉴얼 패킷들 **/
			String s = "";

			pc.sendPackets(new S_NewCreateItem(0x01e3, 1, pc.getAbility().getTotalStr(), pc.getAbility().getTotalCon(),
					"힘", pc.getType(), pc));
			pc.sendPackets(new S_NewCreateItem(0x01e3, 1, pc.getAbility().getTotalDex(), 0, "덱", pc.getType(), pc));
			pc.sendPackets(new S_NewCreateItem(0x01e3, 1, pc.getAbility().getTotalCon(), pc.getAbility().getTotalStr(),
					"콘", pc.getType(), pc));
			pc.sendPackets(new S_NewCreateItem(0x01e3, 1, pc.getAbility().getTotalInt(), 0, "인트", pc.getType(), pc));
			pc.sendPackets(new S_NewCreateItem(0x01e3, 1, pc.getAbility().getTotalWis(), 0, "위즈", pc.getType(), pc));
			pc.sendPackets(new S_NewCreateItem(0x01e3, 1, pc.getAbility().getTotalCha(), 0, "카리", pc.getType(), pc));
			pc.sendPackets(new S_NewCreateItem("무게", pc));

			pc.sendPackets(new S_NewCreateItem(0x01e7, "툴팁1", pc));
			pc.sendPackets(new S_NewCreateItem(0x01e7, "툴팁2", pc));
			pc.sendPackets(new S_NewCreateItem(0x01e7, "툴팁3", pc));

			pc.sendPackets(new S_NewCreateItem(0x01ea, "스탯툴팁", pc));

			pc.sendPackets(new S_NewCreateItem(0x01e9, "08 00 c9 c9"));// 변신이벤트?

			pc.sendPackets(new S_NewCreateItem(0x7E, "08 00 10 01 63 d7"));// 변신이벤트?
			pc.sendPackets(new S_PacketBox(S_PacketBox.ACTION_GUIDE_2));

			UserRankingController.getInstance().setBuffSetting(pc);
			serchSummon(pc);

			WarTimeController.getInstance().checkCastleWar(pc);

			if (pc.getClanid() != 0) { // 크란 소속중

				L1Clan clan = L1World.getInstance().getClan(pc.getClanname());
				if (clan != null) {
					if (pc.getClanid() == clan.getClanId() && // 크란을 해산해, 재차, 동명의 크란이 창설되었을 때의 대책
							pc.getClanname().toLowerCase().equals(clan.getClanName().toLowerCase())) {
						clan.addOnlineClanMember(pc.getName(), pc);
						S_ServerMessage sm = new S_ServerMessage(843, pc.getName());
						// S_ServerMessage sm = new S_ServerMessage(843, pc.getName());
						for (L1PcInstance clanMember : clan.getOnlineClanMember()) {
							if (clanMember.getId() != pc.getId()) {
								clanMember.sendPackets(sm);
								// 지금, 혈맹원의%0%s가 게임에 접속했습니다.
								// clanMember.sendPackets(new S_SystemMessage(clanMember,"혈맹원
								// "+pc.getName()+"님께서 방금 게임에 접속하셨습니다."), true);
							}
						}
						sm = null;
						// 전전쟁 리스트를 취득
						for (L1War war : L1World.getInstance().getWarList()) {
							boolean ret = war.CheckClanInWar(pc.getClanname());
							if (ret) { // 전쟁에 참가중
								String enemy_clan_name = war.GetEnemyClanName(pc.getClanname());
								if (enemy_clan_name != null) {
									// 당신의 혈맹이 현재_혈맹과 교전중입니다.
									pc.sendPackets(new S_War(8, pc.getClanname(), enemy_clan_name), true);
								}
								break;
							}
						}
					} else {
						pc.setClanid(0);
						pc.setClanname("");
						pc.setClanRank(0);
						pc.save(); // DB에 캐릭터 정보를 기입한다
					}
				}
			}

			if (pc.getPartnerId() != 0) { // 결혼중
				L1PcInstance partner = (L1PcInstance) L1World.getInstance().findObject(pc.getPartnerId());
				if (partner != null && partner.getPartnerId() != 0) {
					if (pc.getPartnerId() == partner.getId() && partner.getPartnerId() == pc.getId()) {
						pc.sendPackets(new S_ServerMessage(548), true); // 당신의
																		// 파트너는
																		// 지금
						// 게임중입니다.
						partner.sendPackets(new S_ServerMessage(549), true); // 당신의
																				// 파트너는
						// 방금
						// 로그인했습니다.
					}
				}
			}
			if (currentHpAtLoad > pc.getCurrentHp()) {
				pc.setCurrentHp(currentHpAtLoad);
			}
			if (currentMpAtLoad > pc.getCurrentMp()) {
				pc.setCurrentMp(currentMpAtLoad);
			}

			pc.startObjectAutoUpdate();
			client.CharReStart(false);
			pc.beginExpMonitor();

			pc.tempx = pc.getX();
			pc.tempy = pc.getY();
			pc.tempm = pc.getMapId();
			pc.temph = pc.getMoveState().getHeading();
			long sysTime = System.currentTimeMillis();

			// System.out.println(pc.getDETime2());
			if (pc.getDETime2() != null) {
				if (sysTime <= pc.getDETime2().getTime()) {
					long DETIME = pc.getDETime2().getTime() - sysTime;
					pc.getSkillEffectTimerSet().removeSkillEffect(L1SkillId.DRAGON_EME_1);
					pc.getSkillEffectTimerSet().setSkillEffect(L1SkillId.DRAGON_EME_1, (int) DETIME);
					pc.sendPackets(new S_PacketBox(S_PacketBox.DRAGON_EME, 0x01, (int) DETIME / 1000), true);
				}
			}
			// System.out.println(pc.getDETime());
			if (pc.getDETime() != null) {
				if (sysTime <= pc.getDETime().getTime()) {
					long DETIME = pc.getDETime().getTime() - sysTime;
					pc.getSkillEffectTimerSet().removeSkillEffect(L1SkillId.DRAGON_EME_2);
					pc.getSkillEffectTimerSet().setSkillEffect(L1SkillId.DRAGON_EME_2, (int) DETIME);
					pc.sendPackets(new S_PacketBox(S_PacketBox.DRAGON_EME, 0x02, (int) DETIME / 1000), true);
				}
			}
			if (pc.getPUPLETime() != null) {
				if (sysTime <= pc.getPUPLETime().getTime()) {
					long DETIME = pc.getPUPLETime().getTime() - sysTime;
					pc.getSkillEffectTimerSet().removeSkillEffect(L1SkillId.DRAGON_PUPLE);
					pc.getSkillEffectTimerSet().setSkillEffect(L1SkillId.DRAGON_PUPLE, (int) DETIME);
					pc.sendPackets(new S_PacketBox((int) DETIME / 1000, 1, true, true), true);
				}
			}

			if (pc.getTOPAZTime() != null) {
				if (sysTime <= pc.getTOPAZTime().getTime()) {
					long DETIME = pc.getTOPAZTime().getTime() - sysTime;
					pc.getSkillEffectTimerSet().removeSkillEffect(L1SkillId.DRAGON_TOPAZ);
					pc.getSkillEffectTimerSet().setSkillEffect(L1SkillId.DRAGON_TOPAZ, (int) DETIME);
					pc.sendPackets(new S_PacketBox((int) DETIME / 1000, 2, true, true), true);
				}
			}

			int tamcount = pc.tamcount();
			if (tamcount > 0) {
				long tamtime = pc.TamTime();

				int aftertamtime = (int) tamtime;

				pc.sendPackets(new S_NewCreateItem(S_NewCreateItem.버프창, tamtime, tamcount, true), true);

				if (aftertamtime < 0) {
					aftertamtime = 0;
				}

				if (tamcount == 1) {
					pc.getSkillEffectTimerSet().setSkillEffect(Tam_Fruit1, aftertamtime);
					pc.getAC().addAc(-1);
				} else if (tamcount == 2) {
					pc.getSkillEffectTimerSet().setSkillEffect(Tam_Fruit2, aftertamtime);
					pc.getAC().addAc(-2);
				} else if (tamcount == 3) {
					pc.getSkillEffectTimerSet().setSkillEffect(Tam_Fruit3, aftertamtime);
					pc.getAC().addAc(-3);
					pc.addDamageReductionByArmor(2);
				} else if (tamcount == 4) {
					pc.getSkillEffectTimerSet().setSkillEffect(Tam_Fruit4, aftertamtime);
					pc.getAC().addAc(-4);
					pc.addDamageReductionByArmor(2);
				} else if (tamcount == 5) {
					pc.getSkillEffectTimerSet().setSkillEffect(Tam_Fruit5, aftertamtime);
					pc.getAC().addAc(-5);
					pc.addDamageReductionByArmor(2);
				}

				pc.sendPackets(new S_OwnCharStatus(pc));
			}

			if (pc.getNetConnection().getAccount().getBuff_WindDEFENCE() != null) {
				if (sysTime <= pc.getNetConnection().getAccount().getBuff_WindDEFENCE().getTime()) {
					long bufftime = pc.getNetConnection().getAccount().getBuff_WindDEFENCE().getTime() - sysTime;
					pc.getSkillEffectTimerSet().removeSkillEffect(L1SkillId.강화버프_풍방);
					pc.getSkillEffectTimerSet().setSkillEffect(L1SkillId.강화버프_풍방, (int) bufftime);
					pc.sendPackets(new S_NewUI("풍방", (long) bufftime), true);
					pc.getResistance().addWind(10);
				}
			}
			if (pc.getNetConnection().getAccount().getBuff_WaterDEFENCE() != null) {
				if (sysTime <= pc.getNetConnection().getAccount().getBuff_WaterDEFENCE().getTime()) {
					long bufftime = pc.getNetConnection().getAccount().getBuff_WaterDEFENCE().getTime() - sysTime;
					pc.getSkillEffectTimerSet().removeSkillEffect(L1SkillId.강화버프_수방);
					pc.getSkillEffectTimerSet().setSkillEffect(L1SkillId.강화버프_수방, (int) bufftime);
					pc.sendPackets(new S_NewUI("수방", (long) bufftime), true);
					pc.getResistance().addWater(10);
				}
			}

			if (pc.getNetConnection().getAccount().getBuff_EarthDEFENCE() != null) {
				if (sysTime <= pc.getNetConnection().getAccount().getBuff_EarthDEFENCE().getTime()) {
					long bufftime = pc.getNetConnection().getAccount().getBuff_EarthDEFENCE().getTime() - sysTime;
					pc.getSkillEffectTimerSet().removeSkillEffect(L1SkillId.강화버프_지방);
					pc.getSkillEffectTimerSet().setSkillEffect(L1SkillId.강화버프_지방, (int) bufftime);
					pc.sendPackets(new S_NewUI("지방", (long) bufftime), true);
					pc.getResistance().addEarth(10);
				}
			}
			if (pc.getNetConnection().getAccount().getBuff_FireDEFENCE() != null) {
				if (sysTime <= pc.getNetConnection().getAccount().getBuff_FireDEFENCE().getTime()) {
					long bufftime = pc.getNetConnection().getAccount().getBuff_FireDEFENCE().getTime() - sysTime;
					pc.getSkillEffectTimerSet().removeSkillEffect(L1SkillId.강화버프_화방);
					pc.getSkillEffectTimerSet().setSkillEffect(L1SkillId.강화버프_화방, (int) bufftime);
					pc.sendPackets(new S_NewUI("화방", (long) bufftime), true);
					pc.getResistance().addFire(10);
				}
			}
			if (pc.getNetConnection().getAccount().getBuff_SoulDEFENCE() != null) {
				if (sysTime <= pc.getNetConnection().getAccount().getBuff_SoulDEFENCE().getTime()) {
					long bufftime = pc.getNetConnection().getAccount().getBuff_SoulDEFENCE().getTime() - sysTime;
					pc.getSkillEffectTimerSet().removeSkillEffect(L1SkillId.강화버프_정방);
					pc.getSkillEffectTimerSet().setSkillEffect(L1SkillId.강화버프_정방, (int) bufftime);
					pc.sendPackets(new S_NewUI("정방", (long) bufftime), true);
					pc.getResistance().addFire(5);
					pc.getResistance().addEarth(5);
					pc.getResistance().addWater(5);
					pc.getResistance().addWind(5);
				}
			}

			if (pc.getNetConnection().getAccount().getBuff_WindATTACK() != null) {
				if (sysTime <= pc.getNetConnection().getAccount().getBuff_WindATTACK().getTime()) {
					long bufftime = pc.getNetConnection().getAccount().getBuff_WindATTACK().getTime() - sysTime;
					pc.getSkillEffectTimerSet().removeSkillEffect(L1SkillId.강화버프_풍공);
					pc.getSkillEffectTimerSet().setSkillEffect(L1SkillId.강화버프_풍공, (int) bufftime);
					pc.sendPackets(new S_NewUI("풍공", (long) bufftime), true);
				}
			}
			if (pc.getNetConnection().getAccount().getBuff_WaterATTACK() != null) {
				if (sysTime <= pc.getNetConnection().getAccount().getBuff_WaterATTACK().getTime()) {
					long bufftime = pc.getNetConnection().getAccount().getBuff_WaterATTACK().getTime() - sysTime;
					pc.getSkillEffectTimerSet().removeSkillEffect(L1SkillId.강화버프_수공);
					pc.getSkillEffectTimerSet().setSkillEffect(L1SkillId.강화버프_수공, (int) bufftime);
					pc.sendPackets(new S_NewUI("수공", (long) bufftime), true);
				}
			}
			if (pc.getNetConnection().getAccount().getBuff_EarthATTACK() != null) {
				if (sysTime <= pc.getNetConnection().getAccount().getBuff_EarthATTACK().getTime()) {
					long bufftime = pc.getNetConnection().getAccount().getBuff_EarthATTACK().getTime() - sysTime;
					pc.getSkillEffectTimerSet().removeSkillEffect(L1SkillId.강화버프_지공);
					pc.getSkillEffectTimerSet().setSkillEffect(L1SkillId.강화버프_지공, (int) bufftime);
					pc.sendPackets(new S_NewUI("지공", (long) bufftime), true);
				}
			}
			if (pc.getNetConnection().getAccount().getBuff_FireATTACK() != null) {
				if (sysTime <= pc.getNetConnection().getAccount().getBuff_FireATTACK().getTime()) {
					long bufftime = pc.getNetConnection().getAccount().getBuff_FireATTACK().getTime() - sysTime;
					pc.getSkillEffectTimerSet().removeSkillEffect(L1SkillId.강화버프_화공);
					pc.getSkillEffectTimerSet().setSkillEffect(L1SkillId.강화버프_화공, (int) bufftime);
					pc.sendPackets(new S_NewUI("화공", (long) bufftime), true);
				}
			}

			if (pc.getNetConnection().getAccount().getBuff_WIS() != null) {
				if (sysTime <= pc.getNetConnection().getAccount().getBuff_WIS().getTime()) {
					long bufftime = pc.getNetConnection().getAccount().getBuff_WIS().getTime() - sysTime;
					pc.getSkillEffectTimerSet().removeSkillEffect(L1SkillId.강화버프_지혜);
					pc.getSkillEffectTimerSet().setSkillEffect(L1SkillId.강화버프_지혜, (int) bufftime);
					pc.sendPackets(new S_NewUI("지혜", (long) bufftime), true);
					pc.getAbility().addAddedWis((byte) 1);
					pc.sendPackets(new S_SPMR(pc));
					pc.sendPackets(new S_OwnCharStatus(pc));
				}
			}

			if (pc.getNetConnection().getAccount().getBuff_INT() != null) {
				if (sysTime <= pc.getNetConnection().getAccount().getBuff_INT().getTime()) {
					long bufftime = pc.getNetConnection().getAccount().getBuff_INT().getTime() - sysTime;
					pc.getSkillEffectTimerSet().removeSkillEffect(L1SkillId.강화버프_지식);
					pc.getSkillEffectTimerSet().setSkillEffect(L1SkillId.강화버프_지식, (int) bufftime);
					pc.sendPackets(new S_NewUI("지식", (long) bufftime), true);
					pc.getAbility().addAddedInt((byte) 1);
					pc.sendPackets(new S_SPMR(pc));
					pc.sendPackets(new S_OwnCharStatus(pc));
				}
			}

			if (pc.getNetConnection().getAccount().getBuff_DEX() != null) {
				if (sysTime <= pc.getNetConnection().getAccount().getBuff_DEX().getTime()) {
					long bufftime = pc.getNetConnection().getAccount().getBuff_DEX().getTime() - sysTime;
					pc.getSkillEffectTimerSet().removeSkillEffect(L1SkillId.강화버프_민첩);
					pc.getSkillEffectTimerSet().setSkillEffect(L1SkillId.강화버프_민첩, (int) bufftime);
					pc.sendPackets(new S_NewUI("민첩", (long) bufftime), true);
					pc.getAbility().addAddedDex((byte) 1);
					pc.sendPackets(new S_OwnCharStatus(pc));
				}
			}

			if (pc.getNetConnection().getAccount().getBuff_STR() != null) {
				if (sysTime <= pc.getNetConnection().getAccount().getBuff_STR().getTime()) {
					long bufftime = pc.getNetConnection().getAccount().getBuff_STR().getTime() - sysTime;
					pc.getSkillEffectTimerSet().removeSkillEffect(L1SkillId.강화버프_완력);
					pc.getSkillEffectTimerSet().setSkillEffect(L1SkillId.강화버프_완력, (int) bufftime);
					pc.sendPackets(new S_NewUI("완력", (long) bufftime), true);
					pc.getAbility().addAddedStr((byte) 1);
					pc.sendPackets(new S_OwnCharStatus(pc));
				}
			}

			if (pc.getNetConnection().getAccount().getBuff_PVP() != null) {
				if (sysTime <= pc.getNetConnection().getAccount().getBuff_PVP().getTime()) {
					long bufftime = pc.getNetConnection().getAccount().getBuff_PVP().getTime() - sysTime;
					pc.getSkillEffectTimerSet().removeSkillEffect(L1SkillId.강화버프_PVP);
					pc.getSkillEffectTimerSet().setSkillEffect(L1SkillId.강화버프_PVP, (int) bufftime);
					pc.sendPackets(new S_NewUI("PVP", (long) bufftime), true);
					pc.addPvPDmgByArmor(1);
					pc.addPvPReductionByArmor(1);
				}
			}

			if (pc.getNetConnection().getAccount().getBuff_HPMP() != null) {
				if (sysTime <= pc.getNetConnection().getAccount().getBuff_HPMP().getTime()) {
					long bufftime = pc.getNetConnection().getAccount().getBuff_HPMP().getTime() - sysTime;
					pc.getSkillEffectTimerSet().removeSkillEffect(L1SkillId.강화버프_활력);
					pc.getSkillEffectTimerSet().setSkillEffect(L1SkillId.강화버프_활력, (int) bufftime);
					pc.sendPackets(new S_NewUI("활력", (long) bufftime), true);
					pc.addMaxHp(50);
					pc.addMaxMp(50);
					pc.addWeightReduction(3);
					pc.sendPackets(new S_HPUpdate(pc));
					pc.sendPackets(new S_MPUpdate(pc.getCurrentMp(), pc.getMaxMp()));
				}
			}

			if (pc.getNetConnection().getAccount().getBuff_DMG() != null) {
				if (sysTime <= pc.getNetConnection().getAccount().getBuff_DMG().getTime()) {
					long bufftime = pc.getNetConnection().getAccount().getBuff_DMG().getTime() - sysTime;
					pc.getSkillEffectTimerSet().removeSkillEffect(L1SkillId.강화버프_공격);
					pc.getSkillEffectTimerSet().setSkillEffect(L1SkillId.강화버프_공격, (int) bufftime);
					pc.sendPackets(new S_NewUI("공격", (long) bufftime), true);
					pc.addDmgup(1);
					pc.addBowDmgup(1);
				}
			}

			if (pc.getNetConnection().getAccount().getBuff_REDUC() != null) {
				if (sysTime <= pc.getNetConnection().getAccount().getBuff_REDUC().getTime()) {
					long bufftime = pc.getNetConnection().getAccount().getBuff_REDUC().getTime() - sysTime;
					pc.getSkillEffectTimerSet().removeSkillEffect(L1SkillId.강화버프_방어);
					pc.getSkillEffectTimerSet().setSkillEffect(L1SkillId.강화버프_방어, (int) bufftime);
					pc.sendPackets(new S_NewUI("방어", (long) bufftime), true);
					pc.addDamageReductionByArmor(1);
				}
			}

			if (pc.getNetConnection().getAccount().getBuff_MAGIC() != null) {
				if (sysTime <= pc.getNetConnection().getAccount().getBuff_MAGIC().getTime()) {
					long bufftime = pc.getNetConnection().getAccount().getBuff_MAGIC().getTime() - sysTime;
					pc.getSkillEffectTimerSet().removeSkillEffect(L1SkillId.강화버프_마법);
					pc.getSkillEffectTimerSet().setSkillEffect(L1SkillId.강화버프_마법, (int) bufftime);
					pc.sendPackets(new S_NewUI("마법", (long) bufftime), true);
					pc.getAbility().addSp(1);
					pc.sendPackets(new S_SPMR(pc));
				}
			}

			if (pc.getNetConnection().getAccount().getBuff_STUN() != null) {
				if (sysTime <= pc.getNetConnection().getAccount().getBuff_STUN().getTime()) {
					long bufftime = pc.getNetConnection().getAccount().getBuff_STUN().getTime() - sysTime;
					pc.getSkillEffectTimerSet().removeSkillEffect(L1SkillId.강화버프_스턴);
					pc.getSkillEffectTimerSet().setSkillEffect(L1SkillId.강화버프_스턴, (int) bufftime);
					pc.sendPackets(new S_NewUI("스턴", (long) bufftime), true);
					pc.getResistance().addTechnique(2);
				}
			}

			if (pc.getNetConnection().getAccount().getBuff_HOLD() != null) {
				if (sysTime <= pc.getNetConnection().getAccount().getBuff_HOLD().getTime()) {
					long bufftime = pc.getNetConnection().getAccount().getBuff_HOLD().getTime() - sysTime;
					pc.getSkillEffectTimerSet().removeSkillEffect(L1SkillId.강화버프_홀드);
					pc.getSkillEffectTimerSet().setSkillEffect(L1SkillId.강화버프_홀드, (int) bufftime);
					pc.sendPackets(new S_NewUI("홀드", (long) bufftime), true);
					pc.getResistance().addSpirit(2);
				}
			}

			if (pc.getNetConnection().getAccount().getBuff_PC방() != null) {
				if (sysTime <= pc.getNetConnection().getAccount().getBuff_PC방().getTime()) {
					long 피씨타임 = pc.getNetConnection().getAccount().getBuff_PC방().getTime() - sysTime;

					TimeZone seoul = TimeZone.getTimeZone("UTC");
					Calendar calendar = Calendar.getInstance(seoul);
					calendar.setTimeInMillis(피씨타임);
					int d = calendar.get(Calendar.DATE) - 1;
					int h = calendar.get(Calendar.HOUR_OF_DAY);
					int m = calendar.get(Calendar.MINUTE);
					int sc = calendar.get(Calendar.SECOND);

					if (d > 0) {
						pc.sendPackets(new S_SystemMessage(pc,
								"[PC방 이용 시간] " + d + "일 " + h + "시간 " + m + "분 " + sc + "초 남았습니다."));
					} else if (h > 0) {
						pc.sendPackets(
								new S_SystemMessage(pc, "[PC방 이용 시간] " + h + "시간 " + m + "분 " + sc + "초 남았습니다."));
					} else if (m > 0) {
						pc.sendPackets(new S_SystemMessage(pc, "[PC방 이용 시간] " + m + "분 " + sc + "초 남았습니다."));
					} else {
						pc.sendPackets(new S_SystemMessage(pc, "[PC방 이용 시간] " + sc + "초 남았습니다."));
					}

					PC버프(pc);
				}
			}

			if (pc.PC방_버프) {
				s = "08 01 10 01 f1 d5";// 피씨방..
				// pc.sendPackets(new S_NewCreateItem(s ));
				pc.sendPackets(new S_NewCreateItem(126, s));
			} else {
				s = "08 00 10 01 e7 6d";// 피씨방..
				// pc.sendPackets(new S_NewCreateItem(s ));
				pc.sendPackets(new S_NewCreateItem(126, s));
			}

			if (pc.getNetConnection().getAccount().getDragonRaid() != null) {
				if (sysTime <= pc.getNetConnection().getAccount().getDragonRaid().getTime()) {
					long BloodTime = pc.getNetConnection().getAccount().getDragonRaid().getTime() - sysTime;
					pc.getSkillEffectTimerSet().removeSkillEffect(L1SkillId.DRAGONRAID_BUFF);
					pc.getSkillEffectTimerSet().setSkillEffect(L1SkillId.DRAGONRAID_BUFF, (int) BloodTime);
					pc.sendPackets(new S_PacketBox(S_PacketBox.드래곤레이드버프, (int) BloodTime / 1000), true);
					// pc.sendPackets(new
					// S_SystemMessage("드래곤 레이드 마법이 걸려있으므로
					// "+ss.format(pc.getNetConnection().getAccount().getDragonRaid())+"
					// 이후에 드래곤 포탈 입장이 가능합니다."),
					// true);
				}
			}

			try {
				pc.save(); // DB에 캐릭터 정보를 기입한다
			} catch (Exception e) {
				e.printStackTrace();
			}

			S_SystemMessage smpc = new S_SystemMessage(
					"\\fY" + pc.getName() + " 님이 접속. \\fVIP:" + ip + " 계정:" + client.getAccountName());
			if (Config.접속채팅모니터() > 0) {
				for (L1PcInstance gm : Config.toArray접속채팅모니터()) {
					if (gm.getNetConnection() == null) {
						Config.remove접속(gm);
						continue;
					}
					if (gm == pc) {
						continue;
					}
					gm.sendPackets(smpc);
					if (GMCommands.주시아이피체크) {
						FastTable<String> iplist = IpCheckTable.getInstance().list();
						if (iplist != null && iplist.size() > 0) {
							for (String ipl : iplist) {
								if (ip.startsWith(ipl)) {
									pc.sendPackets(new S_SystemMessage(
											"\\fY >> " + pc.getName() + " = " + ipl + " << 주시 아이피 케릭터 체크바람"));
								}
							}
						}
					}
				}
			}

			if (pc.getMoveState().getHeading() < 0 || pc.getMoveState().getHeading() > 7) {
				pc.getMoveState().setHeading(0);
			}

			pc.sendPackets(new S_CharStatElixir(pc));
			pc.sendPackets(new S_ReturnedStat(S_ReturnedStat.RING_RUNE_SLOT, S_ReturnedStat.SUBTYPE_RING,
					pc.getRingSlotLevel()));

			if (pc.getEarringSlotLevel() > 0) {
				pc.sendPackets(new S_ReturnedStat(S_ReturnedStat.RING_RUNE_SLOT, S_ReturnedStat.SUBTYPE_RING, 16));
			}
			if (pc.getShoulder70SlotLevel() > 0) {
				pc.sendPackets(new S_ReturnedStat(S_ReturnedStat.RING_RUNE_SLOT, S_ReturnedStat.SUBTYPE_RING, 128));
			}

			if (pc.getShoulder83SlotLevel() > 0) {
				pc.sendPackets(new S_ReturnedStat(S_ReturnedStat.RING_RUNE_SLOT, S_ReturnedStat.SUBTYPE_RING, 64));
			}

			pc.sendPackets(new S_ReturnedStat(S_ReturnedStat.RING_RUNE_SLOT, S_ReturnedStat.SUBTYPE_RUNE, 1));

			if (pc.getHellTime() > 0) {
				pc.beginHell(false);
			}
			보안버프(pc);

			/*
			 * if (pc.getKills() >= 20 && pc.getKills() < 40) { pc.sendPackets(new
			 * S_ACTION_UI(S_ACTION_UI.신스킬, 15419), true); pc.sendPackets(new
			 * S_ACTION_UI(15419, 172800, 7237, 5213), true); pc.addPvPDmgByArmor(1);
			 * pc.addPvPReductionByArmor(1); } else if (pc.getKills() >= 40 && pc.getKills()
			 * < 60) { pc.sendPackets(new S_ACTION_UI(S_ACTION_UI.신스킬, 15420), true);
			 * pc.sendPackets(new S_ACTION_UI(15420, 172800, 7237, 5214), true);
			 * pc.addPvPDmgByArmor(2); pc.addPvPReductionByArmor(2); } else if
			 * (pc.getKills() >= 60 && pc.getKills() < 80) { pc.sendPackets(new
			 * S_ACTION_UI(S_ACTION_UI.신스킬, 15421), true); pc.sendPackets(new
			 * S_ACTION_UI(15421, 172800, 7237, 5215), true); pc.addPvPDmgByArmor(3);
			 * pc.addPvPReductionByArmor(3); } else if (pc.getKills() >= 80 && pc.getKills()
			 * < 100) { pc.sendPackets(new S_ACTION_UI(S_ACTION_UI.신스킬, 15422), true);
			 * pc.sendPackets(new S_ACTION_UI(15422, 172800, 7237, 5216), true);
			 * pc.addPvPDmgByArmor(4); pc.addPvPReductionByArmor(4); } else if
			 * (pc.getKills() >= 100) { pc.sendPackets(new S_ACTION_UI(S_ACTION_UI.신스킬,
			 * 15423), true); pc.sendPackets(new S_ACTION_UI(15423, 172800, 7237, 5217),
			 * true); pc.addPvPDmgByArmor(5); pc.addPvPReductionByArmor(5); }
			 */

			if (Config.주말버프) {
				pc.sendPackets(new S_ACTION_UI(S_ACTION_UI.신스킬, 6070), true);
				pc.sendPackets(new S_ACTION_UI(6070, 172800, 8258, 5026), true);
				pc.sendPackets(new S_ServerMessage(5026), true);
			}
			Account account = Account.load(pc.getAccountName());
			if (Config.근하신년) {
				pc.sendPackets(new S_ACTION_UI(S_ACTION_UI.신스킬, 16402), true);
				pc.sendPackets(new S_ACTION_UI(16402, 172800, 9007, 5384), true);
				if (account.getDayQuest() == 1) {
					pc.sendPackets(new S_ACTION_UI(S_ACTION_UI.신스킬, 16401), true);
					pc.sendPackets(new S_ACTION_UI(16401, 172800, 9005, 5385), true);
				}
			}

			if (account.getPrcount() > 0) {
				pc.sendPackets(new S_ACTION_UI(S_ACTION_UI.신스킬, 6073), true);
				pc.sendPackets(new S_ACTION_UI(6073, 172800, 4827, 5072), true);
				pc.getSkillEffectTimerSet().setSkillEffect(L1SkillId.고정인증버프, 172800 * 1000);
				pc.addDamageReductionByArmor(1);
			}

			pc.sendPackets(new S_OwnCharStatus(pc), true);

			if (pc.getClanid() != 0) { // 크란 소속중
				pc.sendPackets(new S_ClanWindow(S_ClanWindow.혈마크띄우기, pc.getClan().getmarkon()), true);
				pc.sendPackets(new S_문장주시(pc.getClan(), 2), true);
				pc.sendPackets(new S_NewUI(0x19, pc.getClan().getClanName(), pc), true);
			}

			if (pc.getReturnStat() != 0) {
				L1SkillUse l1skilluse = new L1SkillUse();
				l1skilluse.handleCommands(pc, L1SkillId.CANCELLATION, pc.getId(), pc.getX(), pc.getY(), null, 0,
						L1SkillUse.TYPE_LOGIN);
				if (pc.getSecondWeapon() != null) {
					pc.getInventory().setEquipped(pc.getSecondWeapon(), false, false, false, true);
				}
				if (pc.getWeapon() != null) {
					pc.getInventory().setEquipped(pc.getWeapon(), false, false, false, false);
				}

				pc.sendPackets(new S_CharVisualUpdate(pc));
				pc.sendPackets(new S_OwnCharStatus(pc));

				for (L1ItemInstance armor : pc.getInventory().getItems()) {
					for (int type = 0; type <= 19; type++) {
						if (armor != null) {
							pc.getInventory().setEquipped(armor, false, false, false);
						}
					}
				}
				pc.sendPackets(new S_SPMR(pc));
				pc.sendPackets(new S_OwnCharAttrDef(pc));
				pc.sendPackets(new S_OwnCharStatus(pc));
				pc.setReturnStatus(1);
				pc.sendPackets(new S_ReturnedStat(pc, S_ReturnedStat.START));

			} else if (!pc.LoadCheckStatus()) {
				if (!CheckInitStat.CheckPcStat(pc)) {
					pc.setReturnStat(pc.getExp());
					pc.setReturnStatus(1);
					pc.sendPackets(new S_ReturnedStat(pc, S_ReturnedStat.START));
					return;
				}
			}

			/** 레지스트 엘리멘트 패시브 스킬 **/
			if (SkillsTable.getInstance().spellCheck(pc.getId(), L1SkillId.RESIST_ELEMENTAL)) {
				pc.getResistance().addMr(5);
				pc.getResistance().addAllNaturalResistance(5);
				pc.sendPackets(new S_SPMR(pc));
				pc.sendPackets(new S_OwnCharAttrDef(pc));
			}

			if (pc.getGfxId().getTempCharGfx() == 11120 || pc.getGfxId().getTempCharGfx() == 11121
					|| pc.getGfxId().getTempCharGfx() == 11122 || pc.getGfxId().getTempCharGfx() == 11123
					|| pc.getGfxId().getTempCharGfx() == 11124 || pc.getGfxId().getTempCharGfx() == 11125
					|| pc.getGfxId().getTempCharGfx() == 11126 || pc.getGfxId().getTempCharGfx() == 11127) {
				pc.getInventory().takeoffEquip(pc.getGfxId().getTempCharGfx());
			}

			if (LetterTable.getInstance().CheckNoReadLetter(pc.getName())) {
				S_SkillSound ss = new S_SkillSound(pc.getId(), 1091);
				pc.sendPackets(ss, true);
				S_ServerMessage sm = new S_ServerMessage(428);
				pc.sendPackets(sm, true);
			}

			ExcludeTable.getInstance().load(pc);
			ExcludeLetterTable.getInstance().load(pc);
			pc.setSurvivalCry(sysTime);
			pc.sendPackets(new S_SPMR(pc), true);

			// 12_09
			for (L1Object obj : L1World.getInstance().getObject()) {
				if (obj == null)
					continue;
				if (obj instanceof L1DollInstance) {
					L1DollInstance doll = (L1DollInstance) obj;
					L1Character cha = doll.getMaster();
					if (cha == null || ((L1PcInstance) cha).getNetConnection() == null || cha.isDead())
						doll.deleteDoll();
					else if (doll.getMaster().getName().equalsIgnoreCase(charName))
						doll.deleteDoll();
				}
			}

			if (!pc.isGm()) {
				if (pc.getLevel() == 1 && pc.getExp() == 0 && pc.getMapId() == 2005 && GMCommands.자동생성방지) {
					AutoCheckThread.getInstance().createCharAutoCheck(pc);
				}
			}

			pc.encobjid = byteWrite(pc.getId());// 표식설정을위해

			if (GMCommands.autocheck_accountlist.contains(client.getAccountName())
					|| GMCommands.autocheck_iplist.contains(ip)) {
				String q = Config.자동방지_퀴즈;
				String d = Config.자동방지_답;
				pc.getNetConnection().AutoQuiz = q;
				pc.getNetConnection().AutoAnswer = d;
				GMCommands.getInstance().AutoCheckStart(client);
			}

			// TODO 출석체크 부분
			/*
			 * CharacterAttendTable.getInstance().LoginAttendProfile(pc); pc.sendPackets(new
			 * S_AttenDance(S_AttenDance.WatchCreate)); pc.sendPackets(new
			 * S_AttenDance(S_AttenDance.WatchItemList, 0)); pc.sendPackets(new
			 * S_AttenDance(S_AttenDance.WatchItemList, 1)); pc.sendPackets(new
			 * S_AttenDance(pc, S_AttenDance.WatchPcPorfile, 0));
			 */

			InvSwapController.getInstance().toWorldJoin(pc);

			if (BossTimer.getInstance().nowErzarbe) {
				pc.sendPackets(new S_MatizAlarm(1, 0, 3600, true));
			}
			if (BossTimer.getInstance().nowsandworm) {
				pc.sendPackets(new S_MatizAlarm(2, 0, 3600, true));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			clear();
		}
	}

	public static final int[] hextable = { 0x80, 0x81, 0x82, 0x83, 0x84, 0x85, 0x86, 0x87, 0x88, 0x89, 0x8a, 0x8b, 0x8c,
			0x8d, 0x8e, 0x8f, 0x90, 0x91, 0x92, 0x93, 0x94, 0x95, 0x96, 0x97, 0x98, 0x99, 0x9a, 0x9b, 0x9c, 0x9d, 0x9e,
			0x9f, 0xa0, 0xa1, 0xa2, 0xa3, 0xa4, 0xa5, 0xa6, 0xa7, 0xa8, 0xa9, 0xaa, 0xab, 0xac, 0xad, 0xae, 0xaf, 0xb0,
			0xb1, 0xb2, 0xb3, 0xb4, 0xb5, 0xb6, 0xb7, 0xb8, 0xb9, 0xba, 0xbb, 0xbc, 0xbd, 0xbe, 0xbf, 0xc0, 0xc1, 0xc2,
			0xc3, 0xc4, 0xc5, 0xc6, 0xc7, 0xc8, 0xc9, 0xca, 0xcb, 0xcc, 0xcd, 0xce, 0xcf, 0xd0, 0xd1, 0xd2, 0xd3, 0xd4,
			0xd5, 0xd6, 0xd7, 0xd8, 0xd9, 0xda, 0xdb, 0xdc, 0xdd, 0xde, 0xdf, 0xe0, 0xe1, 0xe2, 0xe3, 0xe4, 0xe5, 0xe6,
			0xe7, 0xe8, 0xe9, 0xea, 0xeb, 0xec, 0xed, 0xee, 0xef, 0xf0, 0xf1, 0xf2, 0xf3, 0xf4, 0xf5, 0xf6, 0xf7, 0xf8,
			0xf9, 0xfa, 0xfb, 0xfc, 0xfd, 0xfe, 0xff };

	private String byteWrite(long value) {
		long temp = value / 128;
		StringBuffer sb = new StringBuffer();
		if (temp > 0) {
			sb.append((byte) hextable[(int) value % 128]);
			while (temp >= 128) {
				sb.append((byte) hextable[(int) temp % 128]);
				temp = temp / 128;
			}
			if (temp > 0)
				sb.append((int) temp);
		} else {
			if (value == 0) {
				sb.append(0);
			} else {
				sb.append((byte) hextable[(int) value]);
				sb.append(0);
			}
		}
		return sb.toString();
	}

	private void PC버프(L1PcInstance pc) {
		// TODO 자동 생성된 메소드 스텁
		pc.sendPackets(new S_PacketBox(S_PacketBox.PC방버프, 1), true);
		pc.PC방_버프 = true;
	}

	class earthCheck implements Runnable {

		private L1PcInstance pc = null;

		public earthCheck(L1PcInstance _pc) {
			pc = _pc;
		}

		@Override
		public void run() {
			// TODO 자동 생성된 메소드 스텁
			try {
				if (pc == null || pc.getNetConnection() == null)
					return;
				if (pc.getMapId() != 6202) {
					GeneralThreadPool.getInstance().schedule(this, 1000);
					return;
				}
				if (!pc.getSkillEffectTimerSet().hasSkillEffect(EARTH_BIND)) {
					pc.getSkillEffectTimerSet().setSkillEffect(EARTH_BIND, 9999 * 1000);
					pc.sendPackets(new S_Poison(pc.getId(), 2), true);
					Broadcaster.broadcastPacket(pc, new S_Poison(pc.getId(), 2), true);
					pc.sendPackets(new S_Paralysis(S_Paralysis.TYPE_FREEZE, true), true);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			GeneralThreadPool.getInstance().schedule(this, 1000);
		}

	}

	private static final String[] LoginMessage = { "라우풀 성향은 방어계열 보너스를 받게 됩니다.", "마을 게시판에는 도움이 되는 정보가 있을 수 있습니다.",
			"PK시 경비병의 제재를 받지 않습니다.", "공성이 가능한 곳은, [켄트], [윈다우드] 입니다.", "카오틱 대상을 공격해도, 보라색이 됩니다.",
			"카오틱 성향은 공격계열 보너스를 받게 됩니다.", "성향치가 극단으로 갈수록 성향 보너스도 커집니다.", "성향에 따른 보너스가 제공됩니다.",
			"카오틱 보너스는 -10000 이하부터 받게 됩니다.", "다수 혈맹의 시스템적 동맹이 불가능합니다." };

	private void 보안버프(L1PcInstance pc) {
		// TODO 자동 생성된 메소드 스텁
		pc.sendPackets(new S_PacketBox(S_PacketBox.보안버프), true);
		pc.getAC().addAc(-1);
		pc.sendPackets(new S_OwnCharAttrDef(pc), true);
	}

	private void hasadbuff(L1PcInstance pc) {
		try {
			if (pc.getAinHasad() >= 50000000) {
				pc.setAinHasad(50000000);
				pc.sendPackets(new S_ACTION_UI(S_ACTION_UI.EINHASAD, pc), true);
				return;
			}

			if (pc.getAinHasad() >= 50000000) {
				pc.sendPackets(new S_ACTION_UI(S_ACTION_UI.EINHASAD, pc), true);
				return;
			}
			int temp = 0;

			if (pc.getLogOutTime() != null) {
				temp = (int) ((System.currentTimeMillis() - pc.getLogOutTime().getTime()) / 900000);
			}

			int sum = pc.getAinHasad() + (temp * 10000);
			if (sum >= 50000000)
				pc.setAinHasad(50000000);
			else
				pc.setAinHasad(sum);

			pc.sendPackets(new S_ACTION_UI(S_ACTION_UI.EINHASAD, pc), true);
		} catch (Exception e) {
		}
	}

	private void items(L1PcInstance pc) {// DB로부터 캐릭터와 창고의 아이템을 읽어들인다
		CharacterTable.getInstance().restoreInventory(pc);

	}

	private void 북(L1PcInstance pc) {
		try {

			HashMap<Integer, Integer> mbl = MonsterBookTable.getInstace().getMonBookList(pc.getId());

			if (mbl != null) {
				mbl.forEach((monNum, value) -> {
					int monquest1 = MonsterBookTable.getInstace().getQuest1(monNum);
					int monquest2 = MonsterBookTable.getInstace().getQuest2(monNum);
					int monquest3 = MonsterBookTable.getInstace().getQuest3(monNum);

					if (monNum >= 568) {
						monNum += 10;
					}
					if (value >= monquest1) {
						pc.sendPackets(new S_MonsterUi(S_MonsterUi.MONSTER_CLEAR, (monNum * 3) - 2));
					}
					if (value >= monquest2) {
						pc.sendPackets(new S_MonsterUi(S_MonsterUi.MONSTER_CLEAR, (monNum * 3) - 1));
					}
					if (value >= monquest3) {
						pc.sendPackets(new S_MonsterUi(S_MonsterUi.MONSTER_CLEAR, monNum * 3));
					}
				});
			}

		} catch (Exception e) {
		}
	}

	private void bookmarks(L1PcInstance pc) {

		Connection con = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			// pstm =
			// con.prepareStatement("SELECT * FROM character_teleport WHERE
			// char_id=? ORDER BY name ASC");
			pstm = con.prepareStatement("SELECT * FROM character_teleport WHERE char_id=? ORDER BY id ASC");
			pstm.setInt(1, pc.getId());
			rs = pstm.executeQuery();

			L1BookMark bookmark = null;
			while (rs.next()) {
				bookmark = new L1BookMark();
				bookmark.setId(rs.getInt("id"));
				bookmark.setCharId(rs.getInt("char_id"));
				bookmark.setName(rs.getString("name"));
				bookmark.setLocX(rs.getInt("locx"));
				bookmark.setLocY(rs.getInt("locy"));
				bookmark.setMapId(rs.getShort("mapid"));
				bookmark.setRandomX(rs.getShort("randomX"));
				bookmark.setRandomY(rs.getShort("randomY"));
				bookmark.set_fast(rs.getShort("fast"));
				pc.addBookMark(bookmark);
			}
		} catch (SQLException e) {
			_log.log(Level.SEVERE, e.getLocalizedMessage(), e);
		} finally {
			SQLUtil.close(rs);
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
	}

	// 패시브 스킬을 위해 따로 처리

	public static int[] loadSkills(L1PcInstance pc) {
		int[] skillList = new int[30];

		for (int i = 0; i < 30; ++i) {
			skillList[i] = 0;
		}

		Connection con = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		try {

			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con.prepareStatement("SELECT * FROM character_skills WHERE char_obj_id=?");
			pstm.setInt(1, pc.getId());
			rs = pstm.executeQuery();

			L1Skills l1skills = null;

			List<Integer> skillIdList = new ArrayList<Integer>();
			while (rs.next()) {
				int skillId = rs.getInt("skill_id");

				l1skills = SkillsTable.getInstance().getTemplate(skillId);

				if (l1skills != null && l1skills.getSkillLevel() > 0 && l1skills.getSkillLevel() <= 30) {
					skillList[l1skills.getSkillLevel() - 1] |= l1skills.getId();
				}

				// 패시브 스킬 로드
				if (l1skills != null && l1skills.getSkillLevel() == 99) {
					pc.sendPackets(new S_ACTION_UI(145, l1skills.getId()));
				}

				if (skillId == L1SkillId.AURAKIA) {
					pc.toSkill().setAura(true);
				}

				skillIdList.add(skillId);
			}
			SkillCheck.getInstance().AddSkill(pc.getId(), skillIdList);

		} catch (SQLException e) {
			_log.log(Level.SEVERE, e.getLocalizedMessage(), e);
		} finally {
			SQLUtil.close(rs);
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}

		return skillList;
	}

	private void skills(L1PcInstance pc) {
		Connection con = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con.prepareStatement("SELECT * FROM character_skills WHERE char_obj_id=?");
			pstm.setInt(1, pc.getId());
			rs = pstm.executeQuery();

			int i = 0;
			int lv1 = 0;
			int lv2 = 0;
			int lv3 = 0;
			int lv4 = 0;
			int lv5 = 0;
			int lv6 = 0;
			int lv7 = 0;
			int lv8 = 0;
			int lv9 = 0;
			int lv10 = 0;
			int lv11 = 0;
			int lv12 = 0;
			int lv13 = 0;
			int lv14 = 0;
			int lv15 = 0;
			int lv16 = 0;
			int lv17 = 0;
			int lv18 = 0;
			int lv19 = 0;
			int lv20 = 0;
			int lv21 = 0;
			int lv22 = 0;
			int lv23 = 0;
			int lv24 = 0;
			int lv25 = 0;
			int lv26 = 0;
			int lv27 = 0;
			int lv28 = 0;
			int lv29 = 0;
			int lv30 = 0;
			int lv31 = 0;
			int lv32 = 0;
			boolean passive = false;
			boolean 크래쉬 = false;
			boolean 퓨리 = false;
			boolean 슬레이어 = false;
			boolean 아머가드 = false;
			boolean 타이탄락 = false;
			boolean 타이탄블릿 = false;
			boolean 타이탄매직 = false;
			boolean 스피릿츠 = false;
			boolean 베테랑 = false;
			boolean 더블데스티니 = false;
			boolean 아머데스티니 = false;
			boolean 그랩브레이브 = false;
			boolean 포우브레이브 = false;
			boolean 데스페라도앱솔 = false;

			L1Skills l1skills = null;
			while (rs.next()) {

				int skillId = rs.getInt("skill_id");
				try {
					l1skills = SkillsTable.getInstance().getTemplate(skillId);
					if (l1skills.getSkillLevel() == 1) {
						lv1 |= l1skills.getId();
					}
					if (l1skills.getSkillLevel() == 2) {
						lv2 |= l1skills.getId();
					}
					if (l1skills.getSkillLevel() == 3) {
						lv3 |= l1skills.getId();
					}
					if (l1skills.getSkillLevel() == 4) {
						lv4 |= l1skills.getId();
					}
					if (l1skills.getSkillLevel() == 5) {
						lv5 |= l1skills.getId();
					}
					if (l1skills.getSkillLevel() == 6) {
						lv6 |= l1skills.getId();
					}
					if (l1skills.getSkillLevel() == 7) {
						lv7 |= l1skills.getId();
					}
					if (l1skills.getSkillLevel() == 8) {
						lv8 |= l1skills.getId();
					}
					if (l1skills.getSkillLevel() == 9) {
						lv9 |= l1skills.getId();
					}
					if (l1skills.getSkillLevel() == 10) {
						lv10 |= l1skills.getId();
					}
					if (l1skills.getSkillLevel() == 11) {
						lv11 |= l1skills.getId();
					}
					if (l1skills.getSkillLevel() == 12) {
						lv12 |= l1skills.getId();
					}
					if (l1skills.getSkillLevel() == 13) {
						lv13 |= l1skills.getId();
					}
					if (l1skills.getSkillLevel() == 14) {
						lv14 |= l1skills.getId();
					}
					if (l1skills.getSkillLevel() == 15) {
						lv15 |= l1skills.getId();
					}
					if (l1skills.getSkillLevel() == 16) {
						lv16 |= l1skills.getId();
					}
					if (l1skills.getSkillLevel() == 17) {
						lv17 |= l1skills.getId();
					}
					if (l1skills.getSkillLevel() == 18) {
						lv18 |= l1skills.getId();
					}
					if (l1skills.getSkillLevel() == 19) {
						lv19 |= l1skills.getId();
					}
					if (l1skills.getSkillLevel() == 20) {
						lv20 |= l1skills.getId();
					}
					if (l1skills.getSkillLevel() == 21) {
						lv21 |= l1skills.getId();
					}
					if (l1skills.getSkillLevel() == 22) {
						lv22 |= l1skills.getId();
					}
					if (l1skills.getSkillLevel() == 23) {
						lv23 |= l1skills.getId();
					}
					if (l1skills.getSkillLevel() == 24) {
						lv24 |= l1skills.getId();
					}
					if (l1skills.getSkillLevel() == 25) {
						lv25 |= l1skills.getId();
					}
					if (l1skills.getSkillLevel() == 26) {
						lv26 |= l1skills.getId();
					}
					if (l1skills.getSkillLevel() == 27) {
						lv27 |= l1skills.getId();
					}
					if (l1skills.getSkillLevel() == 28) {
						lv28 |= l1skills.getId();
					}
					if (l1skills.getSkillLevel() == 29) {
						lv29 |= l1skills.getId();
					}
					if (l1skills.getSkillLevel() == 30) {
						lv30 |= l1skills.getId();
					}
					if (l1skills.getSkillLevel() == 31) {
						lv31 |= l1skills.getId();
					}
					if (l1skills.getSkillLevel() == 32) {
						lv32 |= l1skills.getId();
					}

					if (l1skills.getSkillLevel() == 99) {
						switch (l1skills.getId()) {
						case 1:
							크래쉬 = true;
							pc.isCrash = true;
							break;
						case 2:
							퓨리 = true;
							pc.isPurry = true;
							break;
						case 3:
							슬레이어 = true;
							pc.isSlayer = true;
							break;
						case 5:
							아머가드 = true;
							pc.isAmorGaurd = true;
							break;
						case 6:
							타이탄락 = true;
							pc.isTaitanR = true;
							break;
						case 7:
							타이탄블릿 = true;
							pc.isTaitanB = true;
							break;
						case 8:
							타이탄매직 = true;
							pc.isTaitanM = true;
							break;
						case 9:
							스피릿츠 = true;
							pc.isSprits = true;
							break;
						case 10:
							베테랑 = true;
							pc.isBetterang = true;
							break;
						case 12:
							더블데스티니 = true;
							pc.isDBDestiny = true;
							break;
						case 11:
							아머데스티니 = true;
							pc.isABDestiny = true;
							break;
						case 13:
							데스페라도앱솔 = true;
							pc.isDesAbsol = true;
							break;
						case 14:
							그랩브레이브 = true;
							pc.isGrabBrave = true;
							break;
						case 15:
							포우브레이브 = true;
							pc.isFouBrave = true;
							break;
						}
						passive = true;
					}

					i = lv1 + lv2 + lv3 + lv4 + lv5 + lv6 + lv7 + lv8 + lv9 + lv10 + lv11 + lv12 + lv13 + lv14 + lv15
							+ lv16 + lv17 + lv18 + lv19 + lv20 + lv21 + lv22 + lv23 + lv24 + lv25 + lv26 + lv27 + lv28
							+ lv29 + lv30 + lv31 + lv32;

					pc.setSkillMastery(skillId);
				} catch (Exception e) {
					// System.out.println(skillId);
				}
			}

			if (베테랑)
				pc.sendPackets(new S_NewUI(S_NewUI.패시브추가, 10));
			if (더블데스티니)
				pc.sendPackets(new S_NewUI(S_NewUI.패시브추가, 12));
			if (아머데스티니)
				pc.sendPackets(new S_NewUI(S_NewUI.패시브추가, 11));
			if (그랩브레이브)
				pc.sendPackets(new S_NewUI(S_NewUI.패시브추가, 14));
			if (포우브레이브)
				pc.sendPackets(new S_NewUI(S_NewUI.패시브추가, 15));
			if (데스페라도앱솔)
				pc.sendPackets(new S_NewUI(S_NewUI.패시브추가, 13));
			if (passive) {
				S_NewUI sn1 = new S_NewUI(크래쉬, 퓨리, 슬레이어, 아머가드, 타이탄락, 타이탄블릿, 타이탄매직, 스피릿츠);
				pc.sendPackets(sn1);
				sn1 = null;
			}

			/** 패시브 스킬 로드 **/
			if (l1skills != null && l1skills.getSkillLevel() == 99) {
				pc.sendPackets(new S_ACTION_UI(145, l1skills.getId()));
			}

			if (i > 0) {
				S_AddSkill as = new S_AddSkill(lv1, lv2, lv3, lv4, lv5, lv6, lv7, lv8, lv9, lv10, lv11, lv12, lv13,
						lv14, lv15, lv16, lv17, lv18, lv19, lv20, lv21, lv22, lv23, lv24, lv25, lv26, lv27, lv28, lv29,
						lv30, lv31, lv32, pc.getElfAttr());
				pc.sendPackets(as, true);
			}
		} catch (SQLException e) {
			_log.log(Level.SEVERE, e.getLocalizedMessage(), e);
		} finally {
			SQLUtil.close(rs);
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
	}

	private void serchSummon(L1PcInstance pc) {
		for (L1SummonInstance summon : L1World.getInstance().getAllSummons()) {
			if (summon.getMaster().getId() == pc.getId()) {
				summon.setMaster(pc);
				pc.addPet(summon);
				for (L1PcInstance visiblePc : L1World.getInstance().getVisiblePlayer(summon)) {
					S_SummonPack sp = new S_SummonPack(summon, visiblePc);
					visiblePc.sendPackets(sp, true);
				}
			}
		}
	}

	private void buff(LineageClient clientthread, L1PcInstance pc) {
		Connection con = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		try {

			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con.prepareStatement("SELECT * FROM character_buff WHERE char_obj_id=?");
			pstm.setInt(1, pc.getId());
			rs = pstm.executeQuery();
			int icon[] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
					0, 0, 0, 0, 0, 0, 0, 0 };

			while (rs.next()) {
				int skillid = rs.getInt("skill_id");
				int remaining_time = rs.getInt("remaining_time");
				if (skillid >= COOKING_1_0_N && skillid <= COOKING_1_6_N
						|| skillid >= COOKING_1_8_N && skillid <= COOKING_1_14_N
						|| skillid >= COOKING_1_16_N && skillid <= COOKING_1_22_N
						|| skillid >= COOKING_1_0_S && skillid <= COOKING_1_6_S
						|| skillid >= COOKING_1_8_S && skillid <= COOKING_1_14_S
						|| skillid >= COOKING_1_16_S && skillid <= COOKING_1_22_S) { // 요리(디저트는 제외하다)
					L1Cooking.eatCooking(pc, skillid, remaining_time);
					continue;
				}
				if (skillid >= COOKING_축복_한우 && skillid <= COOKING_축복_닭고기) {
					L1Cooking.newEatCooking(pc, skillid, remaining_time);
					continue;
				}
				if (skillid >= COOKING_NEW_한우 && skillid <= COOKING_NEW_닭고기) {
					L1Cooking.newEatCooking(pc, skillid, remaining_time);
					continue;
				}
				if (skillid >= 싸이매콤한라면 && skillid <= L1SkillId.싸이시원한음료) {
					L1Cooking.newEatCooking(pc, skillid, remaining_time);
					continue;
				}
				if (skillid == 천하장사버프) {
					L1Cooking.newEatCooking(pc, skillid, remaining_time);
					continue;
				}
				switch (skillid) {
				case LIND_MAAN: {
					if (pc.getSkillEffectTimerSet().hasSkillEffect(LIND_MAAN)) {
						pc.getSkillEffectTimerSet().removeSkillEffect(LIND_MAAN);
					}
					pc.Add_Er(10);
					pc.sendPackets(new S_PacketBox(S_PacketBox.char_ER, pc.get_PlusEr()), true);
					pc.getResistance().addFear(7); // 수면내성3
					icon[35] = (remaining_time + 16) / 32;
					icon[36] = 48;
				}
					break;
				case FAFU_MAAN: {
					if (pc.getSkillEffectTimerSet().hasSkillEffect(FAFU_MAAN)) {
						pc.getSkillEffectTimerSet().removeSkillEffect(FAFU_MAAN);
					}
					pc.getResistance().addSpirit(7); // 동빙내성3
					icon[35] = (remaining_time + 16) / 32;
					icon[36] = 47;
				}
					break;
				case ANTA_MAAN: {
					if (pc.getSkillEffectTimerSet().hasSkillEffect(ANTA_MAAN)) {
						pc.getSkillEffectTimerSet().removeSkillEffect(ANTA_MAAN);
					}
					pc.getResistance().addDragonLang(7);
					icon[35] = (remaining_time + 16) / 32;
					icon[36] = 46;
				}
					break;
				case VALA_MAAN: {
					if (pc.getSkillEffectTimerSet().hasSkillEffect(VALA_MAAN)) {
						pc.getSkillEffectTimerSet().removeSkillEffect(VALA_MAAN);
					}
					icon[35] = (remaining_time + 16) / 32;
					icon[36] = 49;
					pc.getResistance().addTechnique(7); // 스턴내성3
				}
					break;
				case BIRTH_MAAN: {
					if (pc.getSkillEffectTimerSet().hasSkillEffect(BIRTH_MAAN)) {
						pc.getSkillEffectTimerSet().removeSkillEffect(BIRTH_MAAN);
					}
					icon[35] = (remaining_time + 16) / 32;
					icon[36] = 50;
					pc.getResistance().addSpirit(7); // 스턴내성3
					pc.getResistance().addDragonLang(7); // 스턴내성3
				}
					break;
				case SHAPE_MAAN: {
					if (pc.getSkillEffectTimerSet().hasSkillEffect(SHAPE_MAAN)) {
						pc.getSkillEffectTimerSet().removeSkillEffect(SHAPE_MAAN);
					}
					icon[35] = (remaining_time + 16) / 32;
					icon[36] = 51;
					pc.getResistance().addSpirit(7); // 스턴내성3
					pc.getResistance().addDragonLang(7); // 스턴내성3
					pc.getResistance().addFear(7); // 스턴내성3
				}
					break;
				case LIFE_MAAN: {
					if (pc.getSkillEffectTimerSet().hasSkillEffect(LIFE_MAAN)) {
						pc.getSkillEffectTimerSet().removeSkillEffect(LIFE_MAAN);
					}
					icon[35] = (remaining_time + 16) / 32;
					icon[36] = 52;
					pc.getResistance().addAllRegist(7);
					pc.getResistance().addAllHit(3);
				}
					break;
				case L1SkillId.STATUS_DRAGONPERL:// 드진 스킬아이디
					pc.sendPackets(new S_DRAGONPERL(pc.getId(), 8));
					pc.broadcastPacket(new S_DRAGONPERL(pc.getId(), 8));
					pc.sendPackets(new S_PacketBox(S_PacketBox.DRAGONPERL, 8, remaining_time));
					pc.set진주속도(1);
					break;
				case L1SkillId.STATUS_WISDOM_POTION:
					icon[38] = (remaining_time + 8) / 16;
					break;
				case DECREASE_WEIGHT:
					icon[0] = remaining_time / 16;
					break;
				case WEAKNESS:// 위크니스 //
					icon[4] = remaining_time / 4;
					pc.addDmgup(-5);
					pc.addHitup(-1);
					break;
				case BERSERKERS:// 버서커스 //
					icon[7] = remaining_time / 4;
					pc.getAC().addAc(10);
					pc.addDmgup(2);
					pc.addHitup(8);
					break;
				case DISEASE:// 디지즈 //
					icon[5] = remaining_time / 4;
					pc.addDmgup(-6);
					pc.getAC().addAc(12);
					break;
				case SILENCE:
					icon[2] = remaining_time / 4;
					break;
				case SHAPE_CHANGE:
					int poly_id = rs.getInt("poly_id");
					// pc.getGfxId().setTempCharGfx(poly_id);
					L1PolyMorph.doPoly(pc, poly_id, remaining_time, L1PolyMorph.MORPH_BY_ITEMMAGIC);
					continue;
				case L1SkillId.SHAPE_CHANGE_JIBAE:
					int poly_id1 = rs.getInt("poly_id");
					L1PolyMorph.doPoly(pc, poly_id1, remaining_time, L1PolyMorph.MORPH_BY_JIBAE);
					continue;
				case DECAY_POTION:
					icon[1] = remaining_time / 4;
					break;
				case VENOM_RESIST:// 베놈 레지스트 //
					icon[3] = remaining_time / 4;
					break;
				case DRESS_EVASION:// 드레스 이베이젼 //
					// icon[6] = remaining_time / 4;
					pc.Add_Er(18);
					pc.sendPackets(new S_PacketBox(S_PacketBox.char_ER, pc.get_PlusEr()), true);
					pc.sendPackets(new S_OwnCharAttrDef(pc));
					pc.sendPackets(new S_ACTION_UI(L1SkillId.DRESS_EVASION, remaining_time, 5285, 970));
					break;
				case RESIST_MAGIC:// 레지스트 매직
					pc.getResistance().addMr(10);
					pc.sendPackets(new S_ElfIcon(remaining_time / 16, 0, 0, 0), true);
					break;
				case ELEMENTAL_FALL_DOWN:
					icon[12] = remaining_time / 4;
					int playerAttr = pc.getElfAttr();
					int i = -50;
					switch (playerAttr) {
					case 0:
						pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."), true);
						break;
					case 1:
						pc.getResistance().addEarth(i);
						pc.setAddAttrKind(1);
						break;
					case 2:
						pc.getResistance().addFire(i);
						pc.setAddAttrKind(2);
						break;
					case 4:
						pc.getResistance().addWater(i);
						pc.setAddAttrKind(4);
						break;
					case 8:
						pc.getResistance().addWind(i);
						pc.setAddAttrKind(8);
						break;
					default:
						break;
					}
					break;
				case CLEAR_MIND:// 클리어 마인드
					pc.getAbility().addAddedStr((byte) 1);
					pc.getAbility().addAddedDex((byte) 1);
					pc.getAbility().addAddedInt((byte) 1);
					pc.sendPackets(new S_ACTION_UI(L1SkillId.CLEAR_MIND, remaining_time, 5279, 861));
					break;

				case ELEMENTAL_PROTECTION:// 프로텍션 프롬 엘리멘탈
					int attr = pc.getElfAttr();
					if (attr == 1) {
						pc.getResistance().addEarth(50);
					} else if (attr == 2) {
						pc.getResistance().addFire(50);
					} else if (attr == 4) {
						pc.getResistance().addWater(50);
					} else if (attr == 8) {
						pc.getResistance().addWind(50);
					}
					pc.sendPackets(new S_ElfIcon(0, 0, 0, remaining_time / 16), true);
					break;
				case ERASE_MAGIC:
					icon[10] = remaining_time / 4;
					break;
				case NATURES_TOUCH:// 네이쳐스 터치 //
					icon[8] = remaining_time / 4;
					break;
				case WIND_SHACKLE:
					icon[9] = remaining_time / 4;
					break;
				case ELEMENTAL_FIRE:
					icon[13] = remaining_time / 4;
					break;
				case POLLUTE_WATER:// 폴루트 워터 //
					icon[16] = remaining_time / 4;
					break;
				case STRIKER_GALE:// 스트라이커 게일 //
					icon[14] = remaining_time / 4;
					break;
				case SOUL_OF_FLAME:// 소울 오브 프레임 //
					icon[15] = remaining_time / 4;
					break;
				case ADDITIONAL_FIRE:
					icon[11] = remaining_time / 16;
					break;
				case DRAGON_SKIN:// 드래곤 스킨 //
					if (pc.getLevel() >= 84) {
						pc.드래곤스킨 = 8;
					} else if (pc.getLevel() >= 82) {
						pc.드래곤스킨 = 7;
					} else if (pc.getLevel() >= 80) {
						pc.드래곤스킨 = 6;
					} else {
						pc.드래곤스킨 = 5;
					}
					pc.addDamageReductionByArmor(pc.드래곤스킨);
					icon[29] = remaining_time / 16;
					break;
				case GUARD_BREAK:// 가드 브레이크 //
					icon[28] = remaining_time / 4;
					pc.getAC().addAc(10);
					break;
				case FEAR:// 피어 //
					icon[26] = remaining_time / 4;
					break;
				case MORTAL_BODY:// 모탈바디 //
					icon[24] = remaining_time / 4;
					break;
				case HORROR_OF_DEATH:// 호러 오브 데스 //
					icon[25] = remaining_time / 4;
					pc.getAbility().addAddedStr((byte) -3);
					pc.getAbility().addAddedInt((byte) -3);
					break;
				case CONCENTRATION:
					icon[21] = remaining_time / 16;
					break;
				case PATIENCE:// 페이션스 //
					icon[27] = remaining_time / 4;
					break;
				case INSIGHT:
					icon[22] = remaining_time / 16;
					pc.getAbility().addAddedStr((byte) 1);
					pc.getAbility().addAddedDex((byte) 1);
					pc.getAbility().addAddedCon((byte) 1);
					pc.getAbility().addAddedInt((byte) 1);
					pc.getAbility().addAddedWis((byte) 1);
					pc.getAbility().addAddedCha((byte) 1);
					pc.resetBaseMr();
					break;
				case PANIC:
					icon[23] = remaining_time / 16;
					pc.getAbility().addAddedStr((byte) -1);
					pc.getAbility().addAddedDex((byte) -1);
					pc.getAbility().addAddedCon((byte) -1);
					pc.getAbility().addAddedInt((byte) -1);
					pc.getAbility().addAddedWis((byte) -1);
					pc.getAbility().addAddedCha((byte) -1);
					pc.resetBaseMr();
					break;
				case L1SkillId.BLOOD_LUST:
					pc.sendPackets(new S_SkillBrave(pc.getId(), 1, remaining_time), true);
					Broadcaster.broadcastPacket(pc, new S_SkillBrave(pc.getId(), 1, 0), true);
					pc.getMoveState().setBraveSpeed(1);
					break;
				case STATUS_BRAVE:
					pc.sendPackets(new S_SkillBrave(pc.getId(), 1, remaining_time), true);
					Broadcaster.broadcastPacket(pc, new S_SkillBrave(pc.getId(), 1, 0), true);
					pc.getMoveState().setBraveSpeed(1);
					break;
				case STATUS_HASTE:
					pc.sendPackets(new S_SkillHaste(pc.getId(), 1, remaining_time), true);
					Broadcaster.broadcastPacket(pc, new S_SkillHaste(pc.getId(), 1, 0), true);
					pc.getMoveState().setMoveSpeed(1);
					break;
				case STATUS_BLUE_POTION:
				case STATUS_BLUE_POTION2:
				case STATUS_BLUE_POTION3:
					pc.sendPackets(new S_SkillIconGFX(34, remaining_time), true);
					break;
				case STATUS_ELFBRAVE:
					pc.sendPackets(new S_SkillBrave(pc.getId(), 3, remaining_time), true);
					Broadcaster.broadcastPacket(pc, new S_SkillBrave(pc.getId(), 3, 0), true);
					pc.getMoveState().setBraveSpeed(1);
					break;
				case STATUS_CHAT_PROHIBITED:
					pc.sendPackets(new S_SkillIconGFX(36, remaining_time), true);
					break;
				case STATUS_TIKAL_BOSSDIE:
					icon[20] = (remaining_time + 8) / 16;
					L1SkillUse sk1 = new L1SkillUse();
					sk1.handleCommands(clientthread.getActiveChar(), skillid, pc.getId(), pc.getX(), pc.getY(), null,
							remaining_time, L1SkillUse.TYPE_LOGIN);
					sk1 = null;
					break;
				case STATUS_COMA_3:// 코마 3
					icon[31] = (remaining_time + 16) / 32;
					icon[32] = 40;
					L1SkillUse sk2 = new L1SkillUse();
					sk2.handleCommands(clientthread.getActiveChar(), skillid, pc.getId(), pc.getX(), pc.getY(), null,
							remaining_time, L1SkillUse.TYPE_LOGIN);
					sk2 = null;
					break;
				case STATUS_COMA_5:// 코마 5
					icon[31] = (remaining_time + 16) / 32;
					icon[32] = 41;
					L1SkillUse sk3 = new L1SkillUse();
					sk3.handleCommands(clientthread.getActiveChar(), skillid, pc.getId(), pc.getX(), pc.getY(), null,
							remaining_time, L1SkillUse.TYPE_LOGIN);
					sk3 = null;
					break;
				case SPECIAL_COOKING:
					if (pc.getSkillEffectTimerSet().hasSkillEffect(SPECIAL_COOKING)) {
						if (pc.getSkillEffectTimerSet().getSkillEffectTimeSec(SPECIAL_COOKING) < remaining_time) {
						}
					}
					continue;
				case L1SkillId.STATUS_시원한얼음조각:
					icon[18] = (remaining_time + 8) / 16;
					icon[19] = 0x4C;
					pc.addDmgup(2);
					pc.getAbility().addSp(2);
					pc.addHpr(1);
					pc.addMpr(1);
					pc.sendPackets(new S_SPMR(pc), true);
					break;
				case STATUS_CASHSCROLL:// 체력증강주문서 //
					icon[18] = (remaining_time + 8) / 16;
					pc.addHpr(4);
					pc.addMaxHp(50);
					pc.sendPackets(new S_HPUpdate(pc.getCurrentHp(), pc.getMaxHp()), true);
					if (pc.isInParty()) {
						pc.getParty().updateMiniHP(pc);
					}
					pc.sendPackets(new S_MPUpdate(pc.getCurrentMp(), pc.getMaxMp()), true);
					break;
				case L1SkillId.웃픈버프1:
					icon[18] = (remaining_time + 8) / 16;
					icon[19] = 0x4C;
					pc.sendPackets(new S_웃픈버프(1, 1800L));
					break;

				case L1SkillId.웃픈버프2:
					icon[18] = (remaining_time + 8) / 16;
					icon[19] = 0x4C;
					pc.getResistance().addMr(10);
					pc.addDamageReductionByArmor(2);
					pc.addMaxHp(100);
					pc.addHpr(2);
					pc.sendPackets(new S_SPMR(pc));
					pc.sendPackets(new S_HPUpdate(pc));
					pc.sendPackets(new S_MPUpdate(pc.getCurrentMp(), pc.getMaxMp()));
					break;

				case L1SkillId.웃픈버프3:
					icon[18] = (remaining_time + 8) / 16;
					icon[19] = 0x4C;
					pc.addDmgup(3);
					pc.addBowDmgup(3);
					pc.getAbility().addSp(3);
					pc.addMaxMp(50);
					pc.addMpr(2);
					pc.sendPackets(new S_SPMR(pc));
					pc.sendPackets(new S_HPUpdate(pc));
					pc.sendPackets(new S_MPUpdate(pc.getCurrentMp(), pc.getMaxMp()));
					break;

				case STATUS_CASHSCROLL2:// 마력증강주문서 //
					icon[18] = (remaining_time + 8) / 16;
					icon[19] = 1;
					pc.addMpr(4);
					pc.addMaxMp(40);
					pc.sendPackets(new S_MPUpdate(pc.getCurrentMp(), pc.getMaxMp()), true);
					break;
				case STATUS_CASHSCROLL3:
					icon[18] = (remaining_time + 8) / 16;
					icon[19] = 2;
					pc.addDmgup(3);
					pc.addHitup(3);
					pc.getAbility().addSp(3);
					pc.sendPackets(new S_SPMR(pc), true);
					break;
				case L1SkillId.STATUS_투사증강:
					pc.addDmgup(3);
					pc.addHitup(5);
					pc.addPvPReductionByArmor(3);
					pc.getSkillEffectTimerSet().setSkillEffect(8486, remaining_time * 1000);
					pc.sendPackets(new S_ACTION_UI(S_ACTION_UI.신스킬, 8486), true);
					pc.sendPackets(new S_ACTION_UI(8486, remaining_time, 2430, 5180), true);
					break;
				case L1SkillId.STATUS_명궁증강:
					pc.addBowDmgup(3);
					pc.addBowHitup(5);
					pc.addPvPReductionByArmor(3);
					pc.getSkillEffectTimerSet().setSkillEffect(8487, remaining_time * 1000);
					pc.sendPackets(new S_ACTION_UI(S_ACTION_UI.신스킬, 8487), true);
					pc.sendPackets(new S_ACTION_UI(8487, remaining_time, 2430, 5181), true);
					break;
				case L1SkillId.STATUS_현자증강:
					pc.getAbility().addSp(3);
					pc.addSuccMagic(5);
					pc.addPvPReductionByArmor(3);
					pc.getSkillEffectTimerSet().setSkillEffect(8488, remaining_time * 1000);
					pc.sendPackets(new S_ACTION_UI(S_ACTION_UI.신스킬, 8488), true);
					pc.sendPackets(new S_ACTION_UI(8488, remaining_time, 2430, 5182), true);
					break;
				case STATUS_FRUIT:// 유그드라 //
					icon[30] = remaining_time / 4;
					if (SkillsTable.getInstance().spellCheck(pc.getId(), L1SkillId.DARKHORSE)) {
						Broadcaster.broadcastPacket(pc, new S_SkillBrave(pc.getId(), 1, 0), true);
						pc.sendPackets(new S_SkillBrave(pc.getId(), 1, remaining_time), true);
						pc.getMoveState().setBraveSpeed(1);
					} else {
						Broadcaster.broadcastPacket(pc, new S_SkillBrave(pc.getId(), 4, 0), true);
						pc.sendPackets(new S_SkillBrave(pc.getId(), 4, remaining_time), true);
						pc.getMoveState().setBraveSpeed(4);
					}
					break;
				case EXP_POTION:
				case L1SkillId.EXP_POTION_cash:
					pc.sendPackets(new S_ACTION_UI(S_ACTION_UI.신스킬, 30080), true);
					pc.sendPackets(new S_ACTION_UI(30080, remaining_time, 3069, 4358), true);
					break;
				case 싸이클론:
					pc.sendPackets(new S_ACTION_UI(S_ACTION_UI.신스킬, 167), true);
					pc.sendPackets(new S_ACTION_UI(167, remaining_time, 9190, 5446), true);
					break;
				case EXP_POTION2:
					icon[17] = remaining_time / 16;
					break;
				case EXP_POTION3:
					icon[17] = remaining_time / 16;
					break;
				case FEATHER_BUFF_A:// 운세에 따른 깃털 버프 // 매우좋은
					icon[33] = remaining_time / 16;
					icon[34] = 70;
					pc.addDmgup(2);
					pc.addHitup(2);
					pc.getAbility().addSp(2);
					pc.sendPackets(new S_SPMR(pc), true);
					pc.addHpr(3);
					pc.addMaxHp(50);
					pc.sendPackets(new S_HPUpdate(pc.getCurrentHp(), pc.getMaxHp()), true);
					if (pc.isInParty()) {
						pc.getParty().updateMiniHP(pc);
					}
					pc.addMpr(3);
					pc.addMaxMp(30);
					pc.sendPackets(new S_MPUpdate(pc.getCurrentMp(), pc.getMaxMp()), true);
					break;
				case FEATHER_BUFF_B:// 운세에 따른 깃털 버프 // 좋은
					icon[33] = remaining_time / 16;
					icon[34] = 71;
					pc.addHitup(2);
					pc.getAbility().addSp(1);
					pc.sendPackets(new S_SPMR(pc), true);
					pc.addMaxHp(50);
					pc.sendPackets(new S_HPUpdate(pc.getCurrentHp(), pc.getMaxHp()), true);
					if (pc.isInParty()) {
						pc.getParty().updateMiniHP(pc);
					}
					pc.addMaxMp(30);
					pc.sendPackets(new S_MPUpdate(pc.getCurrentMp(), pc.getMaxMp()), true);
					break;
				case FEATHER_BUFF_C:// 운세에 따른 깃털 버프 // 보통
					icon[33] = remaining_time / 16;
					icon[34] = 72;
					pc.addMaxHp(50);
					pc.sendPackets(new S_HPUpdate(pc.getCurrentHp(), pc.getMaxHp()), true);
					if (pc.isInParty()) {
						pc.getParty().updateMiniHP(pc);
					}
					pc.addMaxMp(30);
					pc.sendPackets(new S_MPUpdate(pc.getCurrentMp(), pc.getMaxMp()), true);
					pc.getAC().addAc(-2);
					break;
				case L1SkillId.STATUS_UNDERWATER_BREATH:
					pc.sendPackets(new S_SkillIconBlessOfEva(pc.getId(), remaining_time), true);
					break;
				case FEATHER_BUFF_D:// 운세에 따른 깃털 버프 // 나쁜
					icon[33] = remaining_time / 16;
					icon[34] = 73;
					pc.getAC().addAc(-1);
					break;
				case L1SkillId.레벨업보너스://
					pc.sendPackets(new S_PacketBox(remaining_time, true, true), true);
					break;
				case L1SkillId.승리의기운:
					pc.sendPackets(new S_ACTION_UI(S_ACTION_UI.신스킬, 15411), true);
					pc.sendPackets(new S_ACTION_UI(15411, remaining_time, 7240, 4748), true);
					break;
				case 노나메의가호:
					pc.sendPackets(new S_ACTION_UI(S_ACTION_UI.신스킬, 15412), true);
					pc.sendPackets(new S_ACTION_UI(15412, remaining_time, 7235, 4732), true);
					break;
				case 이스마엘의가호:
					pc.sendPackets(new S_ACTION_UI(S_ACTION_UI.신스킬, 15413), true);
					pc.sendPackets(new S_ACTION_UI(15413, remaining_time, 7233, 4733), true);
					break;
				case 별풍선1단:
					pc.sendPackets(new S_ACTION_UI(S_ACTION_UI.신스킬, 15414), true);
					pc.sendPackets(new S_ACTION_UI(15414, remaining_time, 4909, 5021), true);
					pc.addDmgup(1);
					pc.addBowDmgup(1);
					pc.addHitup(1);
					pc.addBowHitup(1);
					break;
				case L1SkillId.드래곤의성장버프:
					pc.sendPackets(new S_ACTION_UI(S_ACTION_UI.신스킬, 6072), true);
					pc.sendPackets(new S_ACTION_UI(6072, remaining_time, 8382, 5087), true);
					break;
				case 별풍선2단:
					pc.sendPackets(new S_ACTION_UI(S_ACTION_UI.신스킬, 15415), true);
					pc.sendPackets(new S_ACTION_UI(15415, remaining_time, 4909, 5022), true);
					pc.addDmgup(1);
					pc.addBowDmgup(1);
					pc.addHitup(1);
					pc.addBowHitup(1);
					pc.addDamageReductionByArmor(1);
					break;
				case 별풍선3단:
					pc.sendPackets(new S_ACTION_UI(S_ACTION_UI.신스킬, 15416), true);
					pc.sendPackets(new S_ACTION_UI(15416, remaining_time, 4909, 5023), true);
					pc.addDmgup(1);
					pc.addBowDmgup(1);
					pc.addHitup(1);
					pc.addBowHitup(1);
					pc.addDamageReductionByArmor(1);
					break;
				case 별풍선4단:
					pc.sendPackets(new S_ACTION_UI(S_ACTION_UI.신스킬, 15417), true);
					pc.sendPackets(new S_ACTION_UI(15417, remaining_time, 4909, 5024), true);
					pc.addDmgup(3);
					pc.addBowDmgup(3);
					pc.addHitup(3);
					pc.addBowHitup(3);
					pc.addDamageReductionByArmor(1);
					break;
				case 별풍선5단:
					pc.sendPackets(new S_ACTION_UI(S_ACTION_UI.신스킬, 15418), true);
					pc.sendPackets(new S_ACTION_UI(15418, remaining_time, 4909, 5025), true);
					pc.addDmgup(3);
					pc.addBowDmgup(3);
					pc.addHitup(3);
					pc.addBowHitup(3);
					pc.addDamageReductionByArmor(3);
					break;
				case L1SkillId.PHYSICAL_ENCHANT_STR:
					pc.getAbility().addAddedStr((byte) 5);
					pc.sendPackets(new S_Strup(pc, 5, remaining_time), true);
					break;
				case L1SkillId.PHYSICAL_ENCHANT_DEX:
					pc.getAbility().addAddedDex((byte) 5);
					pc.sendPackets(new S_PacketBox(S_PacketBox.char_ER, pc.get_PlusEr()), true);
					pc.sendPackets(new S_Dexup(pc, 5, remaining_time), true);
					pc.sendPackets(new S_OwnCharAttrDef(pc));
					break;
				case L1SkillId.흑사의기운:
					pc.getAC().addAc(-2);
					pc.addMaxHp(20);
					pc.addMaxMp(13);
					pc.sendPackets(new S_HPUpdate(pc.getCurrentHp(), pc.getMaxHp()));
					pc.sendPackets(new S_MPUpdate(pc.getCurrentMp(), pc.getMaxMp()));
					pc.sendPackets(new S_OwnCharStatus(pc));
					icon[37] = remaining_time / 16;
					pc.getSkillEffectTimerSet().setSkillEffect(L1SkillId.흑사의기운, remaining_time * 1000);
					break;

				case L1SkillId.SHINING_ARMOR:
					pc.Add_Er(10);
					pc.sendPackets(new S_ACTION_UI(L1SkillId.SHINING_ARMOR, remaining_time, 9483, 5892));
					pc.sendPackets(new S_OwnCharAttrDef(pc));
					break;
				case L1SkillId.MAJESTY:
					pc.sendPackets(new S_ACTION_UI(L1SkillId.MAJESTY, remaining_time, 9518, 5893));
					break;
				case L1SkillId.FREEZING_ARMOR:
					pc.Add_Er(5);
					pc.sendPackets(new S_ACTION_UI(L1SkillId.FREEZING_ARMOR, remaining_time, 9490, 5889));
					pc.sendPackets(new S_OwnCharAttrDef(pc));
					break;
				case L1SkillId.ENCHANT_ACCURACY:
					pc.addHitup(5);
					pc.sendPackets(new S_ACTION_UI(L1SkillId.ENCHANT_ACCURACY, remaining_time, 9487, 5888));
					break;

				default:
					L1SkillUse sk111 = new L1SkillUse();
					sk111.handleCommands(clientthread.getActiveChar(), skillid, pc.getId(), pc.getX(), pc.getY(), null,
							remaining_time, L1SkillUse.TYPE_LOGIN);
					sk111 = null;
					continue;
				}
				pc.getSkillEffectTimerSet().setSkillEffect(skillid, remaining_time * 1000);
			}
			S_UnityIcon uni = new S_UnityIcon(icon[0], icon[1], icon[2], icon[3], icon[4], icon[5], icon[6], icon[7],
					icon[8], icon[9], icon[10], icon[11], icon[12], icon[13], icon[14], icon[15], icon[16], icon[17],
					icon[18], icon[19], icon[20], icon[21], icon[22], icon[23], icon[24], icon[25], icon[26], icon[27],
					icon[28], icon[29], icon[30], icon[31], icon[32], icon[33], icon[34], icon[35], icon[36], icon[37],
					icon[38]);
			pc.sendPackets(uni, true);
			icon = null;
		} catch (SQLException e) {
			_log.log(Level.SEVERE, e.getLocalizedMessage(), e);
		} finally {
			SQLUtil.close(rs);
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}

	}

	class charNameDelete implements Runnable {
		private String name = null;

		public charNameDelete(String _name) {
			name = _name;
		}

		@Override
		public void run() {
			// TODO 자동 생성된 메소드 스텁
			try {
				if (nameList.contains(name))
					nameList.remove(name);
			} catch (Exception e) {
				System.out.println("접속 이름 리스트에서 삭제 에러 : " + name);
			}
		}

	}

	@Override
	public String getType() {
		return C_LOGIN_TO_SERVER;
	}
}
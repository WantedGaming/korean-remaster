/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */
package l1j.server.server.clientpackets;

import java.util.logging.Logger;

import l1j.server.Config;
import l1j.server.GameSystem.MiniGame.DeathMatch;
import l1j.server.server.TimeController.InvSwapController;
import l1j.server.server.datatables.PetTable;
import l1j.server.server.model.Instance.L1NpcInstance;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.model.Instance.L1PetInstance;
import l1j.server.server.model.Instance.L1SummonInstance;
import l1j.server.server.model.skill.L1SkillId;
import l1j.server.server.serverpackets.S_CharPass;
import l1j.server.server.serverpackets.S_PacketBox;
import l1j.server.server.serverpackets.S_SystemMessage;
import server.LineageClient;
import server.manager.eva;
import server.system.autoshop.AutoShop;
import server.system.autoshop.AutoShopManager;

public class C_Restart extends ClientBasePacket {
	private static final String C_OPCODE_RESTART = "[C] C_Restart";
	private static Logger _log = Logger.getLogger(C_Restart.class.getName());

	public C_Restart(byte[] decrypt, LineageClient client) throws Exception {
		super(decrypt);
		try {
			if (client.getActiveChar() != null) {
				if (DeathMatch.getInstance().isPlayerMember(client.getActiveChar())) {
					client.sendPacket(new S_SystemMessage("데스매치에서는 리스 할 수 없습니다."), true);
					return;
				} else if (client.getActiveChar().getMapId() >= 10000 && client.getActiveChar().getMapId() <= 10005) {
					client.sendPacket(new S_SystemMessage("린드비오르 레이드 에서는 리스 할 수 없습니다."), true);
					return;
				}
			}
			
			L1PcInstance pc = client.getActiveChar();
			pc.restart = true;

			if (pc.isGm()) {
				Config.remove전체(pc);
			}
		//	InvSwapController.getInstance().toSaveSet(pc, code);
			
			try {
				L1PetInstance pet = null;
				// L1SummonInstance summon = null;
				for (Object petObject : pc.getPetList()) {
					if (petObject instanceof L1PetInstance) {
						pet = (L1PetInstance) petObject;
						pet.getMap().setPassable(pet.getLocation(), true);
						pet.dropItem();
						//getPetList().remove(pet.getId());
						//pet.deleteMe();
						pet.deletePet();
					}
					// 서먼
					if (petObject instanceof L1SummonInstance) {
						L1SummonInstance summon = (L1SummonInstance) petObject;
						summon.Death(null);
						/*
						 * for (L1PcInstance visiblePc :
						 * L1World.getInstance().getVisiblePlayer(summon)) {
						 * visiblePc.sendPackets(new S_SummonPack(summon, visiblePc,
						 * false)); // summon.deleteMe(); summon.deleteMe2(); }
						 */
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			client.CharReStart(true);
			S_PacketBox pb = new S_PacketBox(S_PacketBox.LOGOUT);
			client.sendPacket(pb, true);

			if (client.getActiveChar() != null) {
				pc.setadFeature(1);
				try {
					pc.save();
				} catch (Exception e) {
				}
				try {
					pc.saveInventory();
				} catch (Exception e) {
				}
				try {
					pc.getNetConnection().getAccount().updateDGTime();
				} catch (Exception e) {
				}

				try {
					pc.getNetConnection().getAccount().updateTam();
				} catch (Exception e) {
				}
				try {
					pc.getNetConnection().getAccount().updateNcoin();
				} catch (Exception e) {
				}

				/** 2011.04.07 고정수 배틀존 */
					if(pc.isPrivateShop()/* && AutoShopManager.getInstance().isAutoShop()*/){
							 synchronized(pc) { 
								 AutoShopManager shopManager = AutoShopManager.getInstance();
								 AutoShop autoshop = shopManager.makeAutoShop(pc);
								 shopManager.register(autoshop);
								// eva.LogServerAppend("무인상점시작", pc, client.getIp(), -1);
								 client.setActiveChar(null);
								 pc.zombie=true;
							 }
					} else { // 무인PC 만들기(쿠우)
						synchronized (pc) {
							client.quitGame(pc);
							pc.logout();
							client.setActiveChar(null);
							pc.setPacketOutput(null);
						}
						}
			} else {
				_log.fine("Disconnect Request from Account : "
						+ client.getAccountName());
			}
		} catch (Exception e) {

		} finally {
			clear();
		}
	}

	@Override
	public String getType() {
		return C_OPCODE_RESTART;
	}
}

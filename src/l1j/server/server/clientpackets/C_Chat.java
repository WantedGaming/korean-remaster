/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */

package l1j.server.server.clientpackets;

import static l1j.server.server.model.skill.L1SkillId.EARTH_BIND;

import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.TimeZone;

import l1j.server.Config;
//import l1j.server.channel.ChatMonitorChannel;
import l1j.server.L1DatabaseFactory;
import l1j.server.GameSystem.Gamble.GambleInstance;
import l1j.server.server.BadNamesList;
import l1j.server.server.GMCommands;
import l1j.server.server.Opcodes;
import l1j.server.server.UserCommands;
import l1j.server.server.datatables.CharacterTable;
import l1j.server.server.datatables.NpcShopSpawnTable;
import l1j.server.server.datatables.NpcTable;
import l1j.server.server.model.Broadcaster;
import l1j.server.server.model.L1Clan;
import l1j.server.server.model.L1Object;
import l1j.server.server.model.L1Teleport;
import l1j.server.server.model.L1World;
import l1j.server.server.model.Instance.L1MonsterInstance;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.model.skill.L1SkillId;
import l1j.server.server.serverpackets.S_ChatPacket;
import l1j.server.server.serverpackets.S_NewUI;
import l1j.server.server.serverpackets.S_NpcChatPacket;
import l1j.server.server.serverpackets.S_PacketBox;
import l1j.server.server.serverpackets.S_ServerMessage;
import l1j.server.server.serverpackets.S_SystemMessage;
import l1j.server.server.templates.L1Npc;
import l1j.server.server.utils.SQLUtil;
import server.LineageClient;
import server.manager.eva;

//Referenced classes of package l1j.server.server.clientpackets:
//ClientBasePacket

//chat opecode type
//통상 0x44 0x00
//절규(! ) 0x44 0x00
//속삭임(") 0x56 charname
//전체(&) 0x72 0x03
//트레이드($) 0x44 0x00
//PT(#) 0x44 0x0b
//혈맹(@) 0x44 0x04
//연합(%) 0x44 0x0d
//CPT(*) 0x44 0x0e

public class C_Chat extends ClientBasePacket {

	private static final String C_CHAT = "[C] C_Chat";

	private static final String[] textFilter = {"시발"};
	
	private int chatCount = 0;
	
	private boolean npcshopNameCk(String name) {
		return NpcTable.getInstance().findNpcShopName(name);
	}

	/** 변경 가능한지 검사한다 시작 **/

	private void chaname(String chaName, String oldname) {
		Connection con = null;
		PreparedStatement pstm = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con
					.prepareStatement("UPDATE characters SET char_name=? WHERE char_name=?");
			pstm.setString(1, chaName);
			pstm.setString(2, oldname);
			pstm.executeUpdate();
		} catch (Exception e) {

		} finally {
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
	}

	private void logchangename(String chaName, String oldname,
			Timestamp datetime) {
		Connection con = null;
		PreparedStatement pstm = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			String sqlstr = "INSERT INTO Log_Change_name SET Old_Name=?,New_Name=?, Time=?";
			pstm = con.prepareStatement(sqlstr);
			pstm.setString(1, oldname);
			pstm.setString(2, chaName);
			pstm.setTimestamp(3, datetime);
			pstm.executeUpdate();
		} catch (SQLException e) {
		} finally {
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
	}

	private static boolean isAlphaNumeric(String s) {
		boolean flag = true;
		char ac[] = s.toCharArray();
		int i = 0;
		do {
			if (i >= ac.length) {
				break;
			}
			if (!Character.isLetterOrDigit(ac[i])) {
				flag = false;
				break;
			}
			i++;
		} while (true);
		return flag;
	}

	private static boolean isInvalidName(String name) {
		int numOfNameBytes = 0;
		try {
			numOfNameBytes = name.getBytes("EUC-KR").length;
		} catch (UnsupportedEncodingException e) {
			// _log.log(Level.SEVERE, e.getLocalizedMessage(), e);
			return false;
		}

		if (isAlphaNumeric(name)) {
			return false;
		}
		if (5 < (numOfNameBytes - name.length()) || 12 < numOfNameBytes) {
			return false;
		}

		if (BadNamesList.getInstance().isBadName(name)) {
			return false;
		}
		return true;
	}

	public C_Chat(byte abyte0[], LineageClient clientthread) {
		super(abyte0);
		try {
			L1PcInstance pc = clientthread.getActiveChar();
			int chatType = readC();
			String chatText = readS();

			// System.out.println("오만79층.add(new Robot_Location_bean("+pc.getX()+", "+pc.getY()+", 179));");
			/*
			 * if(pc.get_autogo()==1&&chatText.equals(pc.get_autocode()))
			 * //오토입력을 대기중&&입력한코드와 오토코드가동일할때 { pc.sendPackets(new
			 * S_SystemMessage("오토 방지 코드가 인증되었습니다. ")); pc.set_autook(0);
			 * pc.set_autogo(0);
			 * 
			 * }else if (pc.get_autogo()==1){ // 오토입력을 대기중 입력한코드와 오토코드가 불일치할때
			 * pc.sendPackets(new
			 * S_SystemMessage("오토 방지 코드 입력 실패! 코드:"+pc.get_autocode
			 * ()+"를 다시입력해주세요. ")); pc.set_autook(1);
			 * pc.set_autoct(pc.get_autoct()+1); pc.set_autogo(1); }
			 */
			/*
			 * if(PhoneCheck.get(pc.getAccountName())){ int phon = 0; try { phon
			 * = Integer.valueOf(chatText); if(PhoneCheck.폰등록갯수(chatText)>=2){
			 * pc.sendPackets(new
			 * S_SystemMessage("이미 타인에 의해 폰인증이 완료된 번호 입니다.")); return; }
			 * PhoneCheck.remove(pc.getAccountName());
			 * PhoneCheck.removenocheck(pc.getAccountName());
			 * PhoneCheck.폰등록(chatText, pc.getName(), pc.getAccountName(), 0);
			 * pc.sendPackets(new
			 * S_SystemMessage("번호가 정상적으로 전달 되었습니다. 확인 전화에 협조해 주시기 바랍니다.")); }
			 * catch (NumberFormatException e) { pc.sendPackets(new
			 * S_SystemMessage("자신의 핸드폰 번호를 숫자로만 입력해주세요(-,공백없이)"));
			 * pc.sendPackets(new
			 * S_SystemMessage("예) 010-1234-5678(X) / 01012345678(O)")); } catch
			 * (Exception e) { e.printStackTrace(); } return; }
			 */

			if (pc.캐릭명변경) {
				try {
					String chaName = chatText;
					if (pc.getClanid() > 0) {
						pc.sendPackets(new S_SystemMessage(
								"혈맹탈퇴후 캐릭명을 변경할수 있습니다."));
						pc.캐릭명변경 = false;
						return;
					}
					if (!pc.getInventory().checkItem(467009, 1)) { // 있나 체크
						pc.sendPackets(new S_SystemMessage(
								"케릭명 변경 비법서를 소지하셔야 가능합니다."));
						pc.캐릭명변경 = false;
						return;
					}
					for (int i = 0; i < chaName.length(); i++) {
						if (chaName.charAt(i) == 'ㄱ'
								|| chaName.charAt(i) == 'ㄲ'
								|| chaName.charAt(i) == 'ㄴ'
								|| chaName.charAt(i) == 'ㄷ'
								|| // 한문자(char)단위로 비교.
								chaName.charAt(i) == 'ㄸ'
								|| chaName.charAt(i) == 'ㄹ'
								|| chaName.charAt(i) == 'ㅁ'
								|| chaName.charAt(i) == 'ㅂ'
								|| // 한문자(char)단위로 비교
								chaName.charAt(i) == 'ㅃ'
								|| chaName.charAt(i) == 'ㅅ'
								|| chaName.charAt(i) == 'ㅆ'
								|| chaName.charAt(i) == 'ㅇ'
								|| // 한문자(char)단위로 비교
								chaName.charAt(i) == 'ㅈ'
								|| chaName.charAt(i) == 'ㅉ'
								|| chaName.charAt(i) == 'ㅊ'
								|| chaName.charAt(i) == 'ㅋ'
								|| // 한문자(char)단위로 비교.
								chaName.charAt(i) == 'ㅌ'
								|| chaName.charAt(i) == 'ㅍ'
								|| chaName.charAt(i) == 'ㅎ'
								|| chaName.charAt(i) == 'ㅛ'
								|| // 한문자(char)단위로 비교.
								chaName.charAt(i) == 'ㅕ'
								|| chaName.charAt(i) == 'ㅑ'
								|| chaName.charAt(i) == 'ㅐ'
								|| chaName.charAt(i) == 'ㅔ'
								|| // 한문자(char)단위로 비교.
								chaName.charAt(i) == 'ㅗ'
								|| chaName.charAt(i) == 'ㅓ'
								|| chaName.charAt(i) == 'ㅏ'
								|| chaName.charAt(i) == 'ㅣ'
								|| // 한문자(char)단위로 비교.
								chaName.charAt(i) == 'ㅠ'
								|| chaName.charAt(i) == 'ㅜ'
								|| chaName.charAt(i) == 'ㅡ'
								|| chaName.charAt(i) == 'ㅒ'
								|| // 한문자(char)단위로 비교.
								chaName.charAt(i) == 'ㅖ'
								|| chaName.charAt(i) == 'ㅢ'
								|| chaName.charAt(i) == 'ㅟ'
								|| chaName.charAt(i) == 'ㅝ'
								|| // 한문자(char)단위로 비교.
								chaName.charAt(i) == 'ㅞ'
								|| chaName.charAt(i) == 'ㅙ'
								|| chaName.charAt(i) == 'ㅚ'
								|| chaName.charAt(i) == 'ㅘ'
								|| // 한문자(char)단위로 비교.
								chaName.charAt(i) == '씹'
								|| chaName.charAt(i) == '좃'
								|| chaName.charAt(i) == '좆'
								|| chaName.charAt(i) == 'ㅤ') {
							pc.sendPackets(new S_SystemMessage("사용할수없는 케릭명입니다."));
							pc.sendPackets(new S_SystemMessage(
									"캐릭명 변경 비법서를 다시 클릭후 이용해 주세요."));
							pc.캐릭명변경 = false;
							return;
						}
					}
					if (chaName.getBytes().length > 12) {
						pc.sendPackets(new S_SystemMessage("이름이 너무 깁니다."));
						pc.sendPackets(new S_SystemMessage(
								"캐릭명 변경 비법서를 다시 클릭후 이용해 주세요."));
						pc.캐릭명변경 = false;
						return;
					}
					if (chaName.length() == 0) {
						pc.sendPackets(new S_SystemMessage("변경할 케릭명을 입력하세요."));
						pc.sendPackets(new S_SystemMessage(
								"캐릭명 변경 비법서를 다시 클릭후 이용해 주세요."));
						pc.캐릭명변경 = false;
						return;
					}
					if (BadNamesList.getInstance().isBadName(chaName)) {
						pc.sendPackets(new S_SystemMessage("사용할 수 없는 케릭명입니다."));
						pc.sendPackets(new S_SystemMessage(
								"캐릭명 변경 비법서를 다시 클릭후 이용해 주세요."));
						pc.캐릭명변경 = false;
						return;
					}
					if (isInvalidName(chaName)) {
						pc.sendPackets(new S_SystemMessage("사용할 수 없는 케릭명입니다."));
						pc.sendPackets(new S_SystemMessage(
								"캐릭명 변경 비법서를 다시 클릭후 이용해 주세요."));
						pc.캐릭명변경 = false;
						return;
					}
					if (CharacterTable.doesCharNameExist(chaName)) {
						pc.sendPackets(new S_SystemMessage("동일한 케릭명이 존재합니다."));
						pc.sendPackets(new S_SystemMessage(
								"캐릭명 변경 비법서를 다시 클릭후 이용해 주세요."));
						pc.캐릭명변경 = false;
						return;
					}
					if (CharacterTable.RobotNameExist(chaName)) {
						pc.sendPackets(new S_SystemMessage("동일한 케릭명이 존재합니다."));
						pc.sendPackets(new S_SystemMessage(
								"캐릭명 변경 비법서를 다시 클릭후 이용해 주세요."));
						pc.캐릭명변경 = false;
						return;
					}
					if (CharacterTable.RobotCrownNameExist(chaName)) {
						pc.sendPackets(new S_SystemMessage("동일한 케릭명이 존재합니다."));
						pc.sendPackets(new S_SystemMessage(
								"캐릭명 변경 비법서를 다시 클릭후 이용해 주세요."));
						pc.캐릭명변경 = false;
						return;
					}
					if (NpcShopSpawnTable.getInstance().getNpc(chaName)
							|| npcshopNameCk(chaName)) {
						pc.sendPackets(new S_SystemMessage("동일한 케릭명이 존재합니다."));
						pc.sendPackets(new S_SystemMessage(
								"캐릭명 변경 비법서를 다시 클릭후 이용해 주세요."));
						pc.캐릭명변경 = false;
						return;
					}
					if (CharacterTable.somakname(chaName)) {
						pc.sendPackets(new S_SystemMessage("동일한 케릭명이 존재합니다."));
						pc.sendPackets(new S_SystemMessage(
								"캐릭명 변경 비법서를 다시 클릭후 이용해 주세요."));
						pc.캐릭명변경 = false;
						return;
					}

					pc.getInventory().consumeItem(467009, 1); // 소모

					String oldname = pc.getName();

					chaname(chaName, oldname);

					long sysTime = System.currentTimeMillis();
					logchangename(chaName, oldname, new Timestamp(sysTime));

					pc.sendPackets(new S_SystemMessage(chaName
							+ " 아이디로 변경 하셨습니다."));
					pc.sendPackets(new S_SystemMessage(
							"원할한  이용을 위해 클라이언트가 강제로 종료 됩니다."));

					Thread.sleep(1000);
					clientthread.kick();
				} catch (Exception e) {
				}
				return;
			}
			if (clientthread.AutoCheck) {
				if (chatText.equalsIgnoreCase(clientthread.AutoAnswer)) {
					pc.sendPackets(new S_SystemMessage(
							"자동 방지 답을 성공적으로 입력하였습니다."), true);
					while (pc.isTeleport() || pc.텔대기()) {
						Thread.sleep(100);
					}
					if (pc.getMapId() == 6202 || pc.getMapId() == 2005) {
						if (pc.getSkillEffectTimerSet().hasSkillEffect(
								EARTH_BIND)) {
							pc.getSkillEffectTimerSet().removeSkillEffect(
									EARTH_BIND);
						}
					}
					if (pc.getMapId() == 6202) {
						// L1Teleport.teleport(pc, 32778, 32832, (short) 622, 5,
						// true);
						L1Teleport.teleport(pc, 33442, 32797, (short) 4, 5,
								true);
					}
					if (GMCommands.autocheck_iplist.contains(clientthread
							.getIp())) {
						GMCommands.autocheck_iplist
								.remove(clientthread.getIp());
					}
					if (GMCommands.autocheck_accountlist.contains(clientthread
							.getAccountName())) {
						GMCommands.autocheck_accountlist.remove(clientthread
								.getAccountName());
					}
				} else {
					if (clientthread.AutoCheckCount++ >= 2) {
						pc.sendPackets(new S_SystemMessage(
								"자동 방지 답을 잘못 입력하였습니다."), true);
						while (pc.isTeleport() || pc.텔대기()) {
							Thread.sleep(100);
						}

						if (!GMCommands.autocheck_Tellist.contains(clientthread
								.getAccountName())) {
							GMCommands.autocheck_Tellist.add(clientthread
									.getAccountName());
						}

						L1Teleport.teleport(pc, 32928, 32864, (short) 6202, 5,
								true);

					} else {
						pc.sendPackets(new S_SystemMessage(
								"자동 방지 답을 잘못 입력하였습니다. 기회는 총3번입니다."), true);
						// pc.sendPackets(new
						// S_PacketBox(S_PacketBox.GREEN_MESSAGE,
						// "자동 방지 : [ "+pc.getNetConnection().AutoQuiz+" ] 답을 채팅창에 입력해주세요."),
						// true);
						// pc.sendPackets(new
						// S_SystemMessage("자동 방지 : [ "+pc.getNetConnection().AutoQuiz+" ] 답을 채팅창에 입력해주세요."),
						// true);
						pc.sendPackets(
								new S_PacketBox(
										S_PacketBox.GREEN_MESSAGE,
										"자동 방지 : "
												+ pc.getNetConnection().AutoQuiz),
								true);
						pc.sendPackets(
								new S_SystemMessage("자동 방지 : "
										+ pc.getNetConnection().AutoQuiz), true);
						return;
					}
					/*
					 * if(clientthread.AutoCheckCount >= 2){
					 * clientthread.kick(); return; } pc.sendPackets(new
					 * S_SystemMessage("오토 방지 코드를 잘못 입력하셨습니다."), true);
					 * clientthread.AutoCheckCount++; Random _rnd = new
					 * Random(System.nanoTime()); int x = _rnd.nextInt(30); int
					 * y = _rnd.nextInt(30); clientthread.AutoAnswer = ""+(x+y);
					 * pc.sendPackets(new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
					 * "오토 방지 코드 [ "+x+" + "+y+" = ? ] 답을 입력해주세요."), true);
					 * pc.sendPackets(new
					 * S_SystemMessage("오토 방지 코드 [ "+x+" + "+y
					 * +" = ? ] 답을 입력해주세요."), true);
					 */
				}
				clientthread.AutoCheck = false;
				clientthread.AutoCheckCount = 0;
				clientthread.AutoQuiz = "";
				clientthread.AutoAnswer = "";
				return;
			}

			if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.SILENCE)
					|| pc.getSkillEffectTimerSet().hasSkillEffect(
							L1SkillId.AREA_OF_SILENCE)
					|| pc.getSkillEffectTimerSet().hasSkillEffect(
							L1SkillId.STATUS_POISON_SILENCE)) {
				return;
			}
			if (pc.getSkillEffectTimerSet().hasSkillEffect(
					L1SkillId.STATUS_CHAT_PROHIBITED)) { // 채팅 금지중
				S_ServerMessage sm = new S_ServerMessage(242);
				pc.sendPackets(sm); // 현재 채팅 금지중입니다.
				sm = null;
				return;
			}

			if (pc.isDeathMatch() && !pc.isGhost() && !pc.isGm()) {
				S_ServerMessage sm = new S_ServerMessage(912);
				pc.sendPackets(sm); // 채팅을 할 수 없습니다.
				sm = null;
				return;
			}

			if (!pc.isGm()) {
				for (String tt : textFilter) {
					int indexof = chatText.indexOf(tt);
					if (indexof != -1) {
						int count = 100;
						while ((indexof = chatText.indexOf(tt)) != -1) {
							if (count-- <= 0)
								break;
							char[] dd = chatText.toCharArray();
							chatText = "";
							for (int i = 0; i < dd.length; i++) {
								if (i >= indexof
										&& i <= (indexof + tt.length() - 1)) {
									chatText = chatText + "  ";
								} else
									chatText = chatText + dd[i];
							}
						}
					}
				}
			}
			chatCount++;
			switch (chatType) {
			case 0: {
				if (pc.isGhost() && !(pc.isGm() || pc.isMonitor())) {
					return;
				}
				if (chatText.startsWith(".시각")) {
					StringBuilder sb = null;
					sb = new StringBuilder();
					TimeZone kst = TimeZone.getTimeZone("GMT+9");
					Calendar cal = Calendar.getInstance(kst);
					sb.append("[Server Time]" + cal.get(Calendar.YEAR) + "년 "
							+ (cal.get(Calendar.MONTH) + 1) + "월 "
							+ cal.get(Calendar.DATE) + "일 "
							+ cal.get(Calendar.HOUR_OF_DAY) + ":"
							+ cal.get(Calendar.MINUTE) + ":"
							+ cal.get(Calendar.SECOND));
					S_SystemMessage sm = new S_SystemMessage(sb.toString());
					pc.sendPackets(sm, true);
					sb = null;
					return;
				}
				// GM커멘드
				if (chatText.startsWith(".")
						&& (pc.getAccessLevel() == Config.GMCODE || pc
								.getAccessLevel() == 7777)) {
					String cmd = chatText.substring(1);
					GMCommands.getInstance().handleCommands(pc, cmd);
					return;
				}

				if (chatText.startsWith("$")) {
					String text = chatText.substring(1);
					chatWorld(pc, text, 12);
					if (!pc.isGm()) {
						pc.checkChatInterval();
					}
					return;
				}

				Gamble(pc, chatText);
				if (chatText.startsWith(".")) { // 유저코멘트
					String cmd = chatText.substring(1);
					if (cmd == null) {
						return;
					}
					UserCommands.getInstance().handleCommands(pc, cmd);
					return;
				}

				if (chatText.startsWith("$")) { // 월드채팅
					String text = chatText.substring(1);

					chatWorld(pc, text, 12);
					if (!pc.isGm()) {
						pc.checkChatInterval();
					}
					return;
				}

				/** 텔렉 풀기 **/
				/*
				 * if (chatText.startsWith("119")) { try {
				 * L1Teleport.teleport(pc, pc.getX(), pc.getY(), pc.getMapId(),
				 * pc.getMoveState().getHeading(), false); } catch (Exception
				 * exception35) {} }
				 */
				S_ChatPacket s_chatpacket = new S_ChatPacket(pc, chatText,
						Opcodes.S_SAY, 0);
				if (!pc.getExcludingList().contains(pc.getName())) {
					if (pc.getMapId() != 2699) {
						pc.sendPackets(s_chatpacket);
					}
				}
				for (L1PcInstance listner : L1World.getInstance()
						.getRecognizePlayer(pc)) {
					if (!listner.getExcludingList().contains(pc.getName())) {
						if (listner.getMapId() == 2699) {
							continue;
						}
						listner.sendPackets(s_chatpacket);
					}
				}
				// 돕펠 처리
				L1MonsterInstance mob = null;
				for (L1Object obj : pc.getNearObjects().getKnownObjects()) {
					if (obj instanceof L1MonsterInstance) {
						mob = (L1MonsterInstance) obj;
						if (mob.getNpcTemplate().is_doppel()
								&& mob.getName().equals(pc.getName())) {
							Broadcaster
									.broadcastPacket(mob, new S_NpcChatPacket(
											mob, chatText, 0), true);
						}
					}
				}
				// 0319eva.LogChatNormalAppend("[일반]", pc.getName(), chatText);
			}
				break;
			case 2: {
				if (pc.isGhost()) {
					return;
				}
				S_ChatPacket s_chatpacket = new S_ChatPacket(pc, chatText,
						Opcodes.S_SAY, 2);
				if (!pc.getExcludingList().contains(pc.getName())) {
					pc.sendPackets(s_chatpacket);
				}
				for (L1PcInstance listner : L1World.getInstance()
						.getVisiblePlayer(pc, 50)) {
					if (!listner.getExcludingList().contains(pc.getName())) {
						listner.sendPackets(s_chatpacket);
					}
				}
				// 0319eva.LogChatNormalAppend("[일반]", pc.getName(), chatText);
				// 돕펠 처리
				L1MonsterInstance mob = null;
				for (L1Object obj : pc.getNearObjects().getKnownObjects()) {
					if (obj instanceof L1MonsterInstance) {
						mob = (L1MonsterInstance) obj;
						if (mob.getNpcTemplate().is_doppel()
								&& mob.getName().equals(pc.getName())) {
							for (L1PcInstance listner : L1World.getInstance()
									.getVisiblePlayer(mob, 30)) {
								listner.sendPackets(new S_NpcChatPacket(mob,
										chatText, 2), true);
							}
						}
					}
				}
			}
				break;
			case 3: {
				chatWorld(pc, chatText, chatType);
			}
				break;
			case 4: {
				if (pc.getClanid() != 0) { // 크란 소속중
					L1Clan clan = L1World.getInstance().getClan(
							pc.getClanname());
					int rank = pc.getClanRank();
					// if (clan != null && (rank == L1Clan.CLAN_RANK_PUBLIC ||
					// rank == L1Clan.CLAN_RANK_정예 || rank ==
					// L1Clan.CLAN_RANK_GUARDIAN || rank ==
					// L1Clan.CLAN_RANK_PRINCE)) {
					S_ChatPacket s_chatpacket = new S_ChatPacket(pc, chatText,
							Opcodes.S_MESSAGE, 4);

					// monitoring
					// ChatMonitorChannel.getInstance().sendMsg(ChatMonitorChannel.CHAT_MONITOR_CLAN,
					// chatText, pc);

					if (Config.혈맹채팅모니터() > 0) {
						for (L1PcInstance gm : Config.toArray혈맹채팅모니터()) {
							if (gm.getNetConnection() == null) {
								Config.remove혈맹(gm);
								continue;
							}
							if (gm == pc) {
								continue;
							}
							gm.sendPackets(new S_ChatPacket(pc, chatText,
									Opcodes.S_MESSAGE, 40));
						}
					}
					// 0319eva.LogChatClanAppend("[혈맹]", pc.getName(),
					// pc.getClanname(), chatText);
					for (L1PcInstance listner : clan.getOnlineClanMember()) {
						if (!listner.getExcludingList().contains(pc.getName())) {
							listner.sendPackets(s_chatpacket);
						}
					}
					// }
				}
			}
				break;
			case 11: {
				if (pc.isInParty()) { // 파티중
					S_ChatPacket s_chatpacket = new S_ChatPacket(pc, chatText, Opcodes.S_MESSAGE, 11);
					for (L1PcInstance listner : pc.getParty().getMembers()) {
						if (!listner.getExcludingList().contains(pc.getName())) {
							listner.sendPackets(s_chatpacket);
						}
					}
					// monitoring
					// ChatMonitorChannel.getInstance().sendMsg(ChatMonitorChannel.CHAT_MONITOR_PARTY,
					// chatText, pc);
				}
				if (Config.파티채팅모니터() > 0) {
					for (L1PcInstance gm : Config.toArray파티채팅모니터()) {
						if (gm.getNetConnection() == null) {
							Config.remove파티(gm);
							continue;
						}
						if (gm == pc) {
							continue;
						}
						S_ChatPacket cp = new S_ChatPacket(pc, chatText,
								Opcodes.S_MESSAGE, 110);
						gm.sendPackets(cp, true);
					}
				}
				// 0319eva.LogChatPartyAppend("[파티]", pc.getName(), chatText);
			}
				break;
			case 12 :
				if (pc.getMapId() == 39 || pc.getMapId() == 5113){
					pc.sendPackets(new S_SystemMessage("해당 지역에선 일반 채팅외엔 사용하실 수 없습니다."));
					return;
				}
				if(pc.isGm()){
					L1World.getInstance().broadcastPacketToAll(new S_SystemMessage("[******] "+chatText));
				} else{
				chatWorld(pc, chatText, 3);
				}
				break;
			case 13: { // 수호기사 채팅
				if (pc.getClanid() != 0) { // 혈맹 소속중
					L1Clan clan = L1World.getInstance().getClan(
							pc.getClanname());
					int rank = pc.getClanRank();
					if (clan != null
							&& (rank == L1Clan.CLAN_RANK_GUARDIAN
									|| rank == L1Clan.CLAN_RANK_SUBPRINCE || rank == L1Clan.CLAN_RANK_PRINCE)) {
						// S_ChatPacket s_chatpacket = new S_ChatPacket(pc,
						// chatText, Opcodes.S_MESSAGE, 4);
						S_ChatPacket s_chatpacket = new S_ChatPacket(pc,
								chatText, Opcodes.S_SAY, 15);
						for (L1PcInstance listner : clan.getOnlineClanMember()) {
							int listnerRank = listner.getClanRank();
							if (!listner.getExcludingList().contains(
									pc.getName())
									&& (listnerRank == L1Clan.CLAN_RANK_GUARDIAN
											|| rank == L1Clan.CLAN_RANK_SUBPRINCE || listnerRank == L1Clan.CLAN_RANK_PRINCE)) {
								listner.sendPackets(s_chatpacket);
							}
						}
					}
					// monitoring
					// ChatMonitorChannel.getInstance().sendMsg(ChatMonitorChannel.CHAT_MONITOR_CLAN,
					// chatText, pc);
				}
				if (Config.혈맹채팅모니터() > 0) {
					for (L1PcInstance gm : Config.toArray혈맹채팅모니터()) {
						if (gm.getNetConnection() == null) {
							Config.remove혈맹(gm);
							continue;
						}
						if (gm == pc) {
							continue;
						}
						S_ChatPacket cp = new S_ChatPacket(pc, chatText,
								Opcodes.S_MESSAGE, 40);
						gm.sendPackets(cp, true);
					}
				}
				// 0319eva.LogChatPartyAppend("[연합]", pc.getName(), chatText);
			}
				break;
			case 14: { // 채팅 파티
				if (pc.isInChatParty()) { // 채팅 파티중
					S_ChatPacket s_chatpacket = new S_ChatPacket(pc, chatText,
							Opcodes.S_SAY, 14);
					for (L1PcInstance listner : pc.getChatParty().getMembers()) {
						if (!listner.getExcludingList().contains(pc.getName())) {
							listner.sendPackets(s_chatpacket);
						}
					}
				}
				if (Config.파티채팅모니터() > 0) {
					for (L1PcInstance gm : Config.toArray파티채팅모니터()) {
						if (gm.getNetConnection() == null) {
							Config.remove파티(gm);
							continue;
						}
						if (gm == pc) {
							continue;
						}
						S_ChatPacket cp = new S_ChatPacket(pc, chatText,
								Opcodes.S_MESSAGE, 110);
						gm.sendPackets(cp, true);
					}
				}
				// monitoring
				// ChatMonitorChannel.getInstance().sendMsg(ChatMonitorChannel.CHAT_MONITOR_PARTY,
				// chatText, pc);
				// 0319eva.LogChatPartyAppend("[파티]", pc.getName(), chatText);
			}
				break;
			case 15: { // 동맹채팅
				if (pc.getClanid() != 0) { // 혈맹 소속중
					L1Clan clan = L1World.getInstance().getClan(
							pc.getClanname());

					if (clan != null) {
						Integer allianceids[] = clan.Alliance();
						if (allianceids.length > 0) {
							String TargetClanName = null;
							L1Clan TargegClan = null;

							S_ChatPacket s_chatpacket = new S_ChatPacket(pc,
									chatText, Opcodes.S_SAY, 13);
							// S_ChatPacket s_chatpacket = new S_ChatPacket(pc,
							// chatText, Opcodes.S_MESSAGE, 15);
							// 원래는 온라인중인 자기의 혈원과 온라인중인 동맹의 혈원한테 쏘아주어야함. (현재는
							// 대처용)
							for (L1PcInstance listner : clan
									.getOnlineClanMember()) {
								int AllianceClan = listner.getClanid();
								if (pc.getClanid() == AllianceClan) {
									listner.sendPackets(s_chatpacket);
								}
							} // 자기혈맹 전송용

							for (int j = 0; j < allianceids.length; j++) {
								TargegClan = clan.getAlliance(allianceids[j]);
								if (TargegClan != null) {
									TargetClanName = TargegClan.getClanName();
									if (TargetClanName != null) {
										for (L1PcInstance alliancelistner : TargegClan
												.getOnlineClanMember()) {
											alliancelistner
													.sendPackets(s_chatpacket);
										} // 동맹혈맹 전송용
									}
								}

							}
						}

					}
				}
				break;
			}
			case 17:
				if (pc.getClanid() != 0) { // 혈맹 소속중
					L1Clan clan = L1World.getInstance().getClan(
							pc.getClanname());
					if (clan != null
							&& (pc.isCrown() && pc.getId() == clan
									.getLeaderId())) {
						S_ChatPacket s_chatpacket = new S_ChatPacket(pc,
								chatText, Opcodes.S_MESSAGE, 17);
						for (L1PcInstance listner : clan.getOnlineClanMember()) {
							if (!listner.getExcludingList().contains(
									pc.getName())) {
								listner.sendPackets(s_chatpacket);
							}
						}
					}
				}
				break;

			}
			if (!pc.isGm()) {
				pc.checkChatInterval();
			}
		} catch (Exception e) {

		} finally {
			clear();
		}
	}

	private void Gamble(L1PcInstance pc, String chatText) {
		// TODO Auto-generated method stub
		if (pc.Gamble_Somak) { // 소막
			for (int i : GambleInstance.mobArray) {
				L1Npc npck = NpcTable.getInstance().getTemplate(i);
				String name = npck.get_name().replace(" ", "");
				if (name.equalsIgnoreCase(chatText)
						|| npck.get_name().equalsIgnoreCase(chatText)
				/*
				 * || chatText.startsWith(npck.get_name())||
				 * chatText.startsWith(name)
				 */) {
					pc.Gamble_Text = npck.get_name();
				}
			}
		}
	}

	private void chatWorld(L1PcInstance pc, String chatText, int chatType) {
		if (pc.getLevel() >= Config.GLOBAL_CHAT_LEVEL) {
			if (pc.isGm() || L1World.getInstance().isWorldChatElabled()) {
				if (pc.get_food() >= 12) { // 5%겟지?
					S_PacketBox pb = new S_PacketBox(S_PacketBox.FOOD, pc.get_food());
					pc.sendPackets(pb, true);
					if (chatType == 3) {
						S_PacketBox pb2 = new S_PacketBox(S_PacketBox.FOOD, pc.get_food());
						pc.sendPackets(pb2, true);
					} else if (chatType == 12) {
						S_PacketBox pb3 = new S_PacketBox(S_PacketBox.FOOD, pc.get_food());
						pc.sendPackets(pb3, true);
					}
					pc.sendPackets(new S_NewUI(S_NewUI.CHAT_OWNER, chatCount, chatType, chatText, pc, ""));
					//eva.WorldChatAppend("[전체]", pc.getName(), chatText);
					for (L1PcInstance listner : L1World.getInstance().getAllPlayers()) {
						if (pc.isGm()) {
							if (chatType == 12) {
								S_NewUI cp = new S_NewUI(S_NewUI.CHAT_MESSAGE, chatCount, chatType, chatText, pc, "");
								//S_ChatPacket cp = new S_ChatPacket(pc, chatText, Opcodes.S_MESSAGE, chatType);
								
								listner.sendPackets(cp, true);
							} else if (chatType == 3) {
								S_NewUI cp = new S_NewUI(S_NewUI.CHAT_MESSAGE, chatCount, chatType, chatText, pc, "");
								//S_ChatPacket cp = new S_ChatPacket(pc, chatText, Opcodes.S_MESSAGE,	chatType);
							//	eva.WorldChatAppend("[전체]", pc.getName(), chatText);
								listner.sendPackets(cp, true);
							}
						} else {
							if (!listner.getExcludingList().contains(pc.getName())) {
								if (listner.isShowTradeChat() && chatType == 12) {
									S_NewUI cp = new S_NewUI(S_NewUI.CHAT_MESSAGE, chatCount, 3, chatText, pc, "");
									//S_ChatPacket cp = new S_ChatPacket(pc, chatText, Opcodes.S_MESSAGE, chatType);
								//	eva.WorldChatAppend("[전체]", pc.getName(), chatText);
									listner.sendPackets(cp, true);
								} else if (listner.isShowWorldChat() && chatType == 3) {
									S_NewUI cp = new S_NewUI(S_NewUI.CHAT_MESSAGE, chatCount, chatType, chatText, pc, "");
									//S_ChatPacket cp = new S_ChatPacket(pc, chatText, Opcodes.S_MESSAGE,	chatType);
								//	eva.WorldChatAppend("[전체]", pc.getName(), chatText);
									listner.sendPackets(cp, true);
								}
							}
						}
					}
				} else {
					S_ServerMessage sm = new S_ServerMessage(462);
					pc.sendPackets(sm, true);
				}
			} else {
				S_ServerMessage sm = new S_ServerMessage(510);
				pc.sendPackets(sm, true);
			}
		} else {
			S_ServerMessage sm = new S_ServerMessage(195, String.valueOf(Config.GLOBAL_CHAT_LEVEL));
			pc.sendPackets(sm, true);
		}
	}

	@Override
	public String getType() {
		return C_CHAT;
	}
}

/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */

package l1j.server.server.clientpackets;

import java.io.File;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import l1j.server.L1DatabaseFactory;
import l1j.server.server.ObjectIdFactory;
import l1j.server.server.datatables.ClanTable;
import l1j.server.server.model.Broadcaster;
import l1j.server.server.model.L1Clan;
import l1j.server.server.model.L1World;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.serverpackets.S_ReturnedStat;
import l1j.server.server.utils.SQLUtil;
import server.LineageClient;

// Referenced classes of package l1j.server.server.clientpackets:
// ClientBasePacket

public class C_Emblem extends ClientBasePacket {

	private static final String C_EMBLEM = "[C] C_Emblem";
	private static Logger _log = Logger.getLogger(C_Emblem.class.getName());

	public C_Emblem(byte abyte0[], LineageClient clientthread) throws Exception {
		super(abyte0);
		L1PcInstance player = clientthread.getActiveChar();
		if (player == null) {
			return;
		} else if (player.getClanRank() != 4 && player.getClanRank() != 10) {
			return;
		}
		if (player.getClanid() != 0) {
			
			FileOutputStream fos = null;
			try {
				byte[] buff = new byte[384];
				for (short cnt = 0; cnt < 384; cnt++) {
					buff[cnt] = (byte)(readC() & 0xff);
				}
				int newEmblemdId = ObjectIdFactory.getInstance().nextId();
				String emblem_file = String.valueOf(newEmblemdId);
				fos = new FileOutputStream("emblem/" + emblem_file);
				fos.write(buff,  0, 384);
				L1Clan clan = ClanTable.getInstance().getTemplate(player.getClanid());
				clan.setmarkon(newEmblemdId);
				ClanTable.getInstance().updateClan(clan);
				for(L1PcInstance pc : clan.getOnlineClanMember()){
					pc.sendPackets(new S_ReturnedStat(pc, S_ReturnedStat.CLAN_JOIN_LEAVE), true);
					Broadcaster.broadcastPacket(pc, new S_ReturnedStat(pc, S_ReturnedStat.CLAN_JOIN_LEAVE), true);
				}
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println(String.format("%s 엠블럼 등록 오류", player.getName()));
			} finally {
				if (null != fos) {
					fos.close();
				}
				fos = null;
			}
		}
	}

	@Override
	public String getType() {
		return C_EMBLEM;
	}
}

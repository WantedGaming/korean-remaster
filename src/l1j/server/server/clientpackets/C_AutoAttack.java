package l1j.server.server.clientpackets;

import static l1j.server.server.model.skill.L1SkillId.PHANTASM;
import static l1j.server.server.model.skill.L1SkillId.DARK_BLIND;
import l1j.server.server.model.L1Object;
import l1j.server.server.model.L1World;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.model.skill.L1SkillId;
import server.LineageClient;

public class C_AutoAttack extends ClientBasePacket {
	public C_AutoAttack(byte[] decrypt, LineageClient client) {
		super(decrypt);
		L1PcInstance pc = client.getActiveChar();
		if (client.getActiveChar() == null)
			return;
		try {
			int targetId = readD();

			if (targetId == 0 || targetId == 0xFFFFFFFF) {
				client.getActiveChar().run = false;
			} else {
				L1Object target = L1World.getInstance().findObject(targetId);
				if (target == null)
					return;
				client.getActiveChar().target = target;
				if (client.getActiveChar().run)
					return;
				if(pc.getSkillEffectTimerSet().hasSkillEffect(PHANTASM) || pc.getSkillEffectTimerSet().hasSkillEffect(DARK_BLIND)){
					return;
				}

				client.getActiveChar().run = true;
				client.getActiveChar().startatat();
			}
		} catch (Exception e) {
			client.getActiveChar().run = false;
		}
	}
}

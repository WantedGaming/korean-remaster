/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */

package l1j.server.server.clientpackets;

import static l1j.server.server.model.skill.L1SkillId.ABSOLUTE_BARRIER;
import static l1j.server.server.model.skill.L1SkillId.BONE_BREAK;
import static l1j.server.server.model.skill.L1SkillId.CALL_CLAN;
import static l1j.server.server.model.skill.L1SkillId.CURSE_PARALYZE;
import static l1j.server.server.model.skill.L1SkillId.CURSE_PARALYZE2;
import static l1j.server.server.model.skill.L1SkillId.EARTH_BIND;
import static l1j.server.server.model.skill.L1SkillId.FIRE_WALL;
import static l1j.server.server.model.skill.L1SkillId.FOG_OF_SLEEPING;
import static l1j.server.server.model.skill.L1SkillId.FREEZING_BREATH;
import static l1j.server.server.model.skill.L1SkillId.ICE_LANCE;
import static l1j.server.server.model.skill.L1SkillId.INVISIBILITY;
import static l1j.server.server.model.skill.L1SkillId.LIFE_STREAM;
import static l1j.server.server.model.skill.L1SkillId.MASS_TELEPORT;
import static l1j.server.server.model.skill.L1SkillId.MEDITATION;
import static l1j.server.server.model.skill.L1SkillId.MOB_BASILL;
import static l1j.server.server.model.skill.L1SkillId.MOB_COCA;
import static l1j.server.server.model.skill.L1SkillId.MOB_RANGESTUN_18;
import static l1j.server.server.model.skill.L1SkillId.MOB_RANGESTUN_19;
import static l1j.server.server.model.skill.L1SkillId.MOB_RANGESTUN_30;
import static l1j.server.server.model.skill.L1SkillId.MOB_SHOCKSTUN_30;
import static l1j.server.server.model.skill.L1SkillId.PHANTASM;
import static l1j.server.server.model.skill.L1SkillId.RUN_CLAN;
import static l1j.server.server.model.skill.L1SkillId.SCALES_EARTH_DRAGON;
import static l1j.server.server.model.skill.L1SkillId.SCALES_FIRE_DRAGON;
import static l1j.server.server.model.skill.L1SkillId.SCALES_WATER_DRAGON;
import static l1j.server.server.model.skill.L1SkillId.SHAPE_CHANGE;
import static l1j.server.server.model.skill.L1SkillId.SHOCK_STUN;
import static l1j.server.server.model.skill.L1SkillId.엠파이어;
import static l1j.server.server.model.skill.L1SkillId.TELEPORT;
import static l1j.server.server.model.skill.L1SkillId.TRUE_TARGET;
import static l1j.server.server.model.skill.L1SkillId.ABSOLUTE_BLADE;
import static l1j.server.server.model.skill.L1SkillId.어쌔신;
import l1j.server.Config;
import l1j.server.server.ActionCodes;
import l1j.server.server.GMCommands;
import l1j.server.server.datatables.PhoneCheck;
import l1j.server.server.datatables.SkillsTable;
import l1j.server.server.model.AcceleratorChecker;
import l1j.server.server.model.Broadcaster;
import l1j.server.server.model.CharPosUtil;
import l1j.server.server.model.L1Object;
import l1j.server.server.model.L1Teleport;
import l1j.server.server.model.L1World;
import l1j.server.server.model.Instance.L1ItemInstance;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.model.skill.L1SkillId;
import l1j.server.server.model.skill.L1SkillUse;
import l1j.server.server.serverpackets.S_ACTION_UI;
import l1j.server.server.serverpackets.S_DoActionGFX;
import l1j.server.server.serverpackets.S_Paralysis;
import l1j.server.server.serverpackets.S_ServerMessage;
import l1j.server.server.serverpackets.S_SystemMessage;
import server.LineageClient;

//Referenced classes of package l1j.server.server.clientpackets:
//ClientBasePacket

public class C_UseSkill extends ClientBasePacket {

	public C_UseSkill(byte abyte0[], LineageClient client) throws Exception {
		super(abyte0);
		try {
			int row = readC();
			int column = readC();
			int skillId = (row * 8) + column + 1;
			String charName = null;
			String message = null;
			int targetId = 0;
			int targetX = 0;
			int targetY = 0;
			L1PcInstance pc = client.getActiveChar();
			if (GMCommands.마법속도체크 && pc.isGm()) {
				long time = System.currentTimeMillis();
				long eve = 0;
				if (pc._skilltime != 0) {
					eve = time - pc._skilltime;
					if (eve != 0) {
						pc.sendPackets(new S_SystemMessage("클라 마법 평균 속도 : "
								+ eve));
					}
				}
				pc._skilltime = time;
			}

			if (pc.isTeleport() || pc.isDead() || pc.isGhost()) {
				return;
			}
			if (!pc.isSkillMastery(skillId)) {
				return;
			}
			if (!pc.getMap().isUsableSkill()) {
				S_ServerMessage sm = new S_ServerMessage(563);
				pc.sendPackets(sm, true);
				return;
			}
			/*
			 * if(PhoneCheck.getnocheck(pc.getAccountName())){
			 * pc.sendPackets(new S_SystemMessage("폰인증 미입력으로 감옥에 있어야 합니다."));
			 * return; }
			 */

			if (Config.폰인증) {
				if (PhoneCheck.getnocheck(pc.getAccountName())) {
					pc.sendPackets(new S_SystemMessage(
							"폰인증을위해 대기중 입니다. 자동으로 텔이 안될경우 운영자에게 문의하십시오."));
					return;
				}
			}
			/** SPR체크 **/
			if (pc.magicSpeedCheck >= 1) {
				if (pc.magicSpeedCheck == 1) {
					pc.magicSpeed = System.currentTimeMillis();
					pc.sendPackets(new S_SystemMessage(pc,"[체크시작]"), true);
				}
				pc.magicSpeedCheck++;
				if (pc.magicSpeedCheck >= 12) {
					pc.magicSpeedCheck = 0;
					double k = (System.currentTimeMillis() - pc.magicSpeed) / 10D;
					String s = String.format("%.0f", k);
					pc.magicSpeed = 0;
					pc.sendPackets(new S_SystemMessage(pc,"-----------------------------------------"), true);
					pc.sendPackets(new S_SystemMessage(pc,"해당변신은 " + s + "이 마법딜로 적절한값입니다."), true);
					pc.sendPackets(new S_SystemMessage(pc,"-----------------------------------------"), true);
				}
			}
			/** SPR체크 **/
			/** 제어 스킬 중에 임의로 아이템 사용 못하게 **/
			if (pc.getSkillEffectTimerSet().hasSkillEffect(SHOCK_STUN)
					|| pc.getSkillEffectTimerSet().hasSkillEffect(엠파이어)
					|| pc.getSkillEffectTimerSet().hasSkillEffect(MOB_SHOCKSTUN_30)
					|| pc.getSkillEffectTimerSet().hasSkillEffect(MOB_RANGESTUN_19)
					|| pc.getSkillEffectTimerSet().hasSkillEffect(MOB_RANGESTUN_18)
					|| pc.getSkillEffectTimerSet().hasSkillEffect(MOB_RANGESTUN_30)
					|| pc.getSkillEffectTimerSet().hasSkillEffect(EARTH_BIND)
					|| pc.getSkillEffectTimerSet().hasSkillEffect(MOB_COCA)
					|| pc.getSkillEffectTimerSet().hasSkillEffect(MOB_BASILL)
					|| pc.getSkillEffectTimerSet().hasSkillEffect(ICE_LANCE)
					|| pc.getSkillEffectTimerSet().hasSkillEffect(FREEZING_BREATH)
					|| pc.getSkillEffectTimerSet().hasSkillEffect(BONE_BREAK)
					|| pc.getSkillEffectTimerSet().hasSkillEffect(PHANTASM)
					|| pc.getSkillEffectTimerSet().hasSkillEffect(FOG_OF_SLEEPING)
					|| pc.getSkillEffectTimerSet().hasSkillEffect(CURSE_PARALYZE)
					|| pc.getSkillEffectTimerSet().hasSkillEffect(CURSE_PARALYZE2)) {
				return;
			}

			if (GMCommands.트리플포우스핵 && (skillId == L1SkillId.TRIPLE_ARROW || skillId == L1SkillId.FOU_SLAYER)) {
			} else {
				int result;
				result = pc.getAcceleratorChecker().checkSkillInterval(skillId);
				if (result != AcceleratorChecker.R_OK) {
					return;
				} else {
					// pc.radd_hc();
				}
			}

			if (pc.getMapId() == 1931) {
				if (pc.getInventory().getSize() >= 180) {
					pc.sendPackets(
							new S_SystemMessage("인벤토리를 갯수를 비운 후 입장해주세요."), true);
					L1Teleport.teleport(pc, 33443, 32797, (short) 4, 5, true);
					return;
				}
			}

			if (skillId == L1SkillId.CANCELLATION) {
				if (pc.isInvisble()) {
					pc.delInvis();
				}
			}
			if (skillId == L1SkillId.IMMUNE_TO_HARM){
				if (pc.isInvisble()) {
					pc.sendPackets(new S_SystemMessage("사용 실패: 투명상태에서 사용 불가"), true);
					return;
				}
			}
			if (skillId == L1SkillId.HOLY_WEAPON) {
				if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.ENCHANT_WEAPON)) {
					pc.getSkillEffectTimerSet().removeSkillEffect(L1SkillId.ENCHANT_WEAPON);
					pc.sendPackets(new S_ACTION_UI(S_ACTION_UI.신스킬, L1SkillId.ENCHANT_WEAPON));
				} else if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.BLESS_WEAPON)) {
					pc.getSkillEffectTimerSet().removeSkillEffect(L1SkillId.BLESS_WEAPON);
					pc.sendPackets(new S_ACTION_UI(S_ACTION_UI.신스킬, L1SkillId.BLESS_WEAPON));
				} else if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.SHADOW_FANG)) {
					pc.getSkillEffectTimerSet().removeSkillEffect(L1SkillId.SHADOW_FANG);
					pc.sendPackets(new S_ACTION_UI(S_ACTION_UI.신스킬, L1SkillId.SHADOW_FANG));
				} 
			}
			
			if (skillId == L1SkillId.ENCHANT_WEAPON) {
				if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.HOLY_WEAPON)) {
					pc.getSkillEffectTimerSet().removeSkillEffect(L1SkillId.HOLY_WEAPON);
					pc.sendPackets(new S_ACTION_UI(S_ACTION_UI.신스킬, L1SkillId.HOLY_WEAPON));
				} else if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.BLESS_WEAPON)) {
					pc.getSkillEffectTimerSet().removeSkillEffect(L1SkillId.BLESS_WEAPON);
					pc.sendPackets(new S_ACTION_UI(S_ACTION_UI.신스킬, L1SkillId.BLESS_WEAPON));
				} else if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.SHADOW_FANG)) {
					pc.getSkillEffectTimerSet().removeSkillEffect(L1SkillId.SHADOW_FANG);
					pc.sendPackets(new S_ACTION_UI(S_ACTION_UI.신스킬, L1SkillId.SHADOW_FANG));
				} 
			}
			
			if (skillId == L1SkillId.BLESS_WEAPON) {
				if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.HOLY_WEAPON)) {
					pc.getSkillEffectTimerSet().removeSkillEffect(L1SkillId.HOLY_WEAPON);
					pc.sendPackets(new S_ACTION_UI(S_ACTION_UI.신스킬, L1SkillId.HOLY_WEAPON));
				} else if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.ENCHANT_WEAPON)) {
					pc.getSkillEffectTimerSet().removeSkillEffect(L1SkillId.ENCHANT_WEAPON);
					pc.sendPackets(new S_ACTION_UI(S_ACTION_UI.신스킬, L1SkillId.ENCHANT_WEAPON));
				} else if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.SHADOW_FANG)) {
					pc.getSkillEffectTimerSet().removeSkillEffect(L1SkillId.SHADOW_FANG);
					pc.sendPackets(new S_ACTION_UI(S_ACTION_UI.신스킬, L1SkillId.SHADOW_FANG));
				} 
			}
			
			if (skillId == L1SkillId.SHADOW_FANG) {
				if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.HOLY_WEAPON)) {
					pc.getSkillEffectTimerSet().removeSkillEffect(L1SkillId.HOLY_WEAPON);
					pc.sendPackets(new S_ACTION_UI(S_ACTION_UI.신스킬, L1SkillId.HOLY_WEAPON));
				} else if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.ENCHANT_WEAPON)) {
					pc.getSkillEffectTimerSet().removeSkillEffect(L1SkillId.ENCHANT_WEAPON);
					pc.sendPackets(new S_ACTION_UI(S_ACTION_UI.신스킬, L1SkillId.ENCHANT_WEAPON));
				} else if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.BLESS_WEAPON)) {
					pc.getSkillEffectTimerSet().removeSkillEffect(L1SkillId.BLESS_WEAPON);
					pc.sendPackets(new S_ACTION_UI(S_ACTION_UI.신스킬, L1SkillId.BLESS_WEAPON));
				} 
			}
			
			if(skillId == L1SkillId.파워그립){
				S_DoActionGFX gfx = new S_DoActionGFX(pc.getId(), ActionCodes.ACTION_SkillBuff);
				pc.sendPackets(gfx);
				Broadcaster.broadcastPacket(pc, gfx, true);
			}
			
			if(skillId == ABSOLUTE_BLADE){
				 L1ItemInstance weapon = pc.getWeapon();
				 if(weapon.getItem().getType1() == 20 || weapon.getItem().getType1() == 62){
					 pc.sendPackets(new S_SystemMessage("마법을 사용할 수 없습니다."), true);
					 return;
				 }
			}
			
			if(skillId == L1SkillId.인페르노){
				 L1ItemInstance weapon = pc.getWeapon();
				 if(weapon.getItem().getType1() != 4){
					 pc.sendPackets(new S_SystemMessage("마법 사용에 필요한 장비를 착용하지 않아 사용이 불가능합니다."), true);
					 return;
				 }
			}
			if(skillId == L1SkillId.엠파이어){
			if(pc.getInventory().getTypeEquipped(2, 7) < 1){
				 pc.sendPackets(new S_SystemMessage("마법 사용에 필요한 장비를 착용하지 않아 사용이 불가능합니다."), true);
				 return;
				}
			}
			
			if (abyte0.length > 4) {
				try {
					switch (skillId) {
					case CALL_CLAN:
					case RUN_CLAN:
						charName = readS();
						break;
					case TRUE_TARGET:
						targetId = readD();
						targetX = readH();
						targetY = readH();
						// message = readS();
						break;
					case TELEPORT:
					case MASS_TELEPORT:
						targetId = readH(); // MapID
						// targetId = readD(); // Bookmark ID
						targetX = readH();
						targetY = readH();
						break;
					case FIRE_WALL:
					case LIFE_STREAM:
						targetX = readH();
						targetY = readH();
						break;
					case L1SkillId.SUMMON_MONSTER:
						targetX = readC();
						break;
					default: {
						targetId = readD();
						targetX = readH();
						targetY = readH();
					}
						break;
					}
				} catch (Exception e) {
					// _log.log(Level.SEVERE, "", e);
				}
			}

			if (skillId != TELEPORT
					&& pc.getSkillEffectTimerSet().hasSkillEffect(
							ABSOLUTE_BARRIER)) { // 아브소르트바리아의 해제
				pc.getSkillEffectTimerSet().killSkillEffectTimer(
						ABSOLUTE_BARRIER);
				pc.startHpRegenerationByDoll();
				pc.startMpRegenerationByDoll();
			}
			
			if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.MOEBIUS)) { // 뫼비우스의 해제
				pc.getSkillEffectTimerSet().removeSkillEffect(L1SkillId.MOEBIUS);
			}

			if (pc.getSkillEffectTimerSet().hasSkillEffect(
					L1SkillId.STATUS_안전모드)) {
				pc.getSkillEffectTimerSet().killSkillEffectTimer(
						L1SkillId.STATUS_안전모드);
			}

			pc.getSkillEffectTimerSet().killSkillEffectTimer(MEDITATION);

			try {
				L1Object target2 = L1World.getInstance().findObject(targetId);
				if (target2 != null
						&& SkillsTable.getInstance().getTemplate(skillId)
								.getTarget().equalsIgnoreCase("attack")) {
					if (skillId != L1SkillId.FIRE_WALL) {
						int calcx = (int) pc.getX()
								- target2.getLocation().getX();
						int calcy = (int) pc.getY()
								- target2.getLocation().getY();
						int range = SkillsTable.getInstance()
								.getTemplate(skillId).getRanged();
						if (range != -1) {
							if (Math.abs(calcx) > range
									|| Math.abs(calcy) > range) {
								return;
							}
						}
					}
				}

				if (!pc.isGm() && target2 != null
						&& target2 instanceof L1PcInstance
						&& skillId == L1SkillId.CANCELLATION) {
					L1PcInstance tpc = (L1PcInstance) target2;
					if (tpc.getMapId() == 2005)
						return;
				}

				
				if (skillId == CALL_CLAN || skillId == RUN_CLAN) {
					if (charName.isEmpty()) {
						return;
					}

					StringBuffer sb = new StringBuffer();
					for (int i = 0; i < charName.length(); i++) {
						if (charName.charAt(i) == '[') {
							break;
						}
						sb.append(charName.charAt(i));
					}

					L1PcInstance target = L1World.getInstance().getPlayer(sb.toString());
					sb = null;

					if (target == null) {
						pc.sendPackets(new S_ServerMessage(73, charName), true);
						return;
					}
					if(target.getLevel() <= 55){
						pc.sendPackets(new S_SystemMessage("대상의 레벨이 너무 낮습니다."), true);
						return;
					}
					if (pc.isPinkName() || target.isPinkName()) {
						pc.sendPackets(new S_SystemMessage("시전자 또는 대상이 전투 중입니다."), true);
						return;
					}

					if (pc.isGhost()) {
						pc.sendPackets(new S_SystemMessage(
								"현재 시전 할 수 없는 대상입니다."), true);
						return;
					}

					if (pc.getClan() == null || pc.getClanid() == 0) {
						pc.sendPackets(new S_SystemMessage(
								"혈맹을 창설해야 사용 가능 합니다."), true);
						return;
					}

					if (pc.getClanid() != target.getClanid()) {
						pc.sendPackets(new S_ServerMessage(414), true);
						return;
					}

					targetId = target.getId();
					if (skillId == CALL_CLAN) {
						int callClanId = pc.getCallClanId();
						if (callClanId == 0 || callClanId != targetId) {
							pc.setCallClanId(targetId);
							pc.setCallClanHeading(pc.getMoveState().getHeading());
						}
					}
				}
				if (skillId == MASS_TELEPORT){
					if(pc.getMapId() == 5490){
						pc.sendPackets(new S_SystemMessage("이곳에서는 사용할 수 없습니다."), true);
						pc.sendPackets(new S_Paralysis(S_Paralysis.TYPE_TELEPORT_UNLOCK, false), true);
						return;
					}
				}
				if (skillId == L1SkillId.블로우어택) {
					if(pc.getWeapon().getItem().getType() != 1 && pc.getWeapon().getItem().getType() != 2){
						pc.sendPackets(new S_SystemMessage("기술을 사용할 수 없습니다."), true);
						return;
					}
				}
				if (skillId == INVISIBILITY) {
					if (pc.getMapId() == 5153) {
						pc.sendPackets(new S_ServerMessage(563), true); // \f1  여기에서는 사용할 수 없습니다.
						return;
					}
				}

				pc.플레이어상태 = pc.공격_상태;
				pc.상태시간 = System.currentTimeMillis() + 2000;
				L1SkillUse l1skilluse = new L1SkillUse();
				l1skilluse.handleCommands(pc, skillId, targetId, targetX, targetY, message, 0, L1SkillUse.TYPE_NORMAL);
				l1skilluse = null;
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (Exception e) {

		} finally {
			clear();
		}
	}

	class areaSilence implements Runnable {

		private L1PcInstance pc;

		public areaSilence(L1PcInstance _pc) {
			pc = _pc;
		}

		@Override
		public void run() {
			// TODO 자동 생성된 메소드 스텁
			int i = 10;
			try {
				while (i > 0) {
					if (pc == null || pc.getNetConnection() == null)
						return;
					if (i <= 3) {
						pc.sendPackets(new S_SystemMessage(i + "초 - 에어리어 오브 사일런스 쿨타임"), true);
					}
					i--;
					Thread.sleep(1000);
				}
				pc.sendPackets(new S_SystemMessage("에어리어 오브 사일런스 쿨타임 종료"), true);
			} catch (Exception e) {
			}
		}

	}
}

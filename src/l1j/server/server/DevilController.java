package l1j.server.server;

import java.util.Calendar;
import java.util.Locale;
import java.text.SimpleDateFormat;

import l1j.server.Config;
import l1j.server.server.model.L1Teleport;
import l1j.server.server.model.L1World;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.serverpackets.S_PacketBox;
import l1j.server.server.serverpackets.S_SystemMessage;
import l1j.server.server.utils.L1SpawnUtil;

public class DevilController extends Thread {
	
		private static DevilController _instance;

		private boolean _DevilStart;
		public boolean getDevilStart() {
			return _DevilStart;
		}
		public void setDevilStart(boolean Devil) {
			_DevilStart = Devil;
		}
		private static long sTime = 0;	
		
		public boolean isGmOpen = false;

		private static final SimpleDateFormat s = new SimpleDateFormat("HH", Locale.KOREA);

		private static final SimpleDateFormat ss = new SimpleDateFormat("MM-dd HH:mm", Locale.KOREA);

		public static DevilController getInstance() {
			if(_instance == null) {
				_instance = new DevilController();
			}
			return _instance;
		}
		
		@Override
			public void run() {
			try	{
					while (true) {
						Thread.sleep(1000); 
						if(!Config.DEVIL_KING)
							continue;
						/** 오픈 **/
						if(!isGmOpen && !isOpen())
							continue;
						if(L1World.getInstance().getAllPlayers().size() <= 0)
							continue;
						
						isGmOpen = false;

						/** 오픈 메세지 **/
						L1SpawnUtil.spawn2(33445, 32805, (short) 4, 777017, 0, 3600*1000, 0);
						L1World.getInstance().broadcastServerMessage("\\aG[악마왕의 영토] 텔레포터가 나타났습니다.");
						L1World.getInstance().broadcastPacketToAll(new S_PacketBox(S_PacketBox.GREEN_MESSAGE, 
								"[악마왕의 영토] 텔레포터가 나타났습니다."));

						/** 악마왕영토 시작**/
						setDevilStart(true);

						/** 실행 1시간 시작**/

						Thread.sleep(2400000L); //3800000L 1시간 10분정도
						Boss();
						Thread.sleep(1200000L); //3800000L 1시간 10분정도
						
						L1World.getInstance().broadcastServerMessage("\\aG악마왕의 영토가 닫혔습니다.");
						L1World.getInstance().broadcastPacketToAll(new S_PacketBox(S_PacketBox.GREEN_MESSAGE, 
								"악마왕의 영토가 닫혔습니다."));
						 
						/** 1시간뒤에 영토맵에 있는 유저 마을로 **/
						for(L1PcInstance pc : L1World.getInstance().getAllPlayers()) {
						if (pc.getMapId() == 5167){
						L1Teleport.teleport(pc, 33970, 33246, (short) 4, 4, true);
						setDevilStart(false);
						}
						
						/** 5초 뒤에 다시한번 텔레포트 마을로 **/
						Thread.sleep(5000L);
						TelePort();
						setDevilStart(false);
						
						/** 5초 뒤에 다시한번 텔레포트 마을로 **/
						Thread.sleep(5000L);
						TelePort2();
						setDevilStart(false);

						/** 종료메세지 출력 **/
						End();
					}
					}
				} catch(Exception e){
					e.printStackTrace();
				}
			}

			/**
			 *오픈 시각을 가져온다
			 *
			 *@return (Strind) 오픈 시각(MM-dd HH:mm)
			 */
			 public String OpenTime() {
				 Calendar c = Calendar.getInstance();
				 c.setTimeInMillis(sTime);
				 return ss.format(c.getTime());
			 }
			 
			 private void Boss(){
				 L1SpawnUtil.spawn2(32681, 32893, (short) 5167, 7000062, 50, 1200*1000, 0);
					L1World.getInstance().broadcastServerMessage("\\aD악마왕 영토의 결계를 풀고 악마왕이 나타났습니다.");
					L1World.getInstance().broadcastPacketToAll(new S_PacketBox(S_PacketBox.GREEN_MESSAGE, 
							"\\f=악마왕 영토의 결계를 풀고 \\f3악마왕\\f=이 나타났습니다.."));
				 }
			 /**
			 *영토가 열려있는지 확인
			 *
			 *@return (boolean) 열려있다면 true 닫혀있다면 false
			 */

			 private boolean isOpen() {
				  Calendar calender = Calendar.getInstance();
				  int hour, minute;
				  hour = calender.get(Calendar.HOUR_OF_DAY);
				  minute = calender.get(Calendar.MINUTE);
				  if ((hour == 16 && minute == 01) || (hour == 20 && minute == 01) 
					|| (hour == 00 && minute == 01)|| (hour == 04 && minute == 01)
					|| (hour == 8 && minute == 01)|| (hour == 12 && minute == 01)){
				   return true;
				  }
				  return false;
				 }


			 /**
			 *실제 현재시각을 가져온다
			 *
			 *@return (String) 현재 시각(HH:mm)
			 */
			 private String getTime() {
				 return s.format(Calendar.getInstance().getTime());
			 }

			 /**아덴마을로 팅기게**/
			 private void TelePort() {
				 for(L1PcInstance c : L1World.getInstance().getAllPlayers()) {
					 switch(c.getMap().getId()) {
						 case 5167:
						 c.stopHpRegenerationByDoll();
						 c.stopMpRegenerationByDoll();
						 L1Teleport.teleport(c, 33970, 33246, (short) 4, 4, true);
						//c.sendPackets(new S_SystemMessage("악마의 영토가 닫혔습니다."));
						 break;
						 default:
						 break;
					 }
				 }
			 }
			 
			 /**아덴마을로 팅기게**/
			 private void TelePort2() {
				 for(L1PcInstance c : L1World.getInstance().getAllPlayers()) {
					 switch(c.getMap().getId()) {
						 case 5167:
						 c.stopHpRegenerationByDoll();
						 c.stopMpRegenerationByDoll();
						 L1Teleport.teleport(c, 33970, 33246, (short) 4, 4, true);
						//c.sendPackets(new S_SystemMessage("악마의 영토가 닫혔습니다."));
						 break;
						 default:
						 break;
					 }
				 }
			 }

			 /** 종료 **/
			 private void End() {
				// L1World.getInstance().broadcastServerMessage("악마의 영토가 사라졌습니다. 3시간뒤에 다시 열립니다.");
				/* for(L1PcInstance pc : L1World.getInstance().getAllPlayers()) {
			     if (pc.getMapId() == 5167){
				 L1Teleport.teleport(pc, 33970, 33246, (short) 4, 4, true);*/
				 setDevilStart(false);
			 }
}
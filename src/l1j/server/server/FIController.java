package l1j.server.server;

import java.util.Calendar;
import java.util.Locale;
import java.text.SimpleDateFormat;

import l1j.server.Config;
import l1j.server.server.model.L1Object;
import l1j.server.server.model.L1Teleport;
import l1j.server.server.model.L1World;
import l1j.server.server.model.Instance.L1MonsterInstance;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.serverpackets.S_ChatPacket;
import l1j.server.server.serverpackets.S_PacketBox;
import l1j.server.server.serverpackets.S_SystemMessage;
import l1j.server.server.utils.L1SpawnUtil;

public class FIController extends Thread {
	
		private static FIController _instance;

		private boolean _F_IStart;
		public boolean getF_IStart() {
			return _F_IStart;
		}
		public void setF_IStart(boolean F_I) {
			_F_IStart = F_I;
		}
		private static long sTime = 0;	
		
		public boolean isGmOpen = false;

		private static final SimpleDateFormat s = new SimpleDateFormat("HH", Locale.KOREA);

		private static final SimpleDateFormat ss = new SimpleDateFormat("MM-dd HH:mm", Locale.KOREA);

		public static FIController getInstance() {
			if(_instance == null) {
				_instance = new FIController();
			}
			return _instance;
		}
		
		@Override
			public void run() {
			try{
					while (true) {
						Thread.sleep(1000); 
						/** 오픈 **/
						
						isFIOpen();
						Thread.sleep(60000);
						
						if(isClose()){
							End();
							Thread.sleep(10000);
						}
					}
				} catch(Exception e){
					e.printStackTrace();
				}
			}


			 private boolean isOpen() {
				  Calendar calender = Calendar.getInstance();
				  int hour, minute;
				  hour = calender.get(Calendar.HOUR_OF_DAY);
				  minute = calender.get(Calendar.MINUTE);	 
			   if ((hour == 16 && minute == 00)) {
				  return true;
				  }
				  return false;
				 }
			 
			 private void isFIOpen(){
				 try{
					 String[] weekDay = { "일요일", "월요일", "화요일", "수요일", "목요일", "금요일", "토요일" };     
					   Calendar cal = Calendar.getInstance(); 
					      int num = cal.get(Calendar.DAY_OF_WEEK)-1; 
					      String today = weekDay[num];
					      int hour, minute;
						  hour = cal.get(Calendar.HOUR_OF_DAY);
						  minute = cal.get(Calendar.MINUTE);
					      if(today.equalsIgnoreCase("금요일") || today.equalsIgnoreCase("토요일") || today.equalsIgnoreCase("일요일")){
					    	  if (hour == 18 && minute == 00) {
						/**		L1World.getInstance().broadcastServerMessage("\\aD[잊혀진 섬]: 잊혀진 섬이 개방되었습니다.");
								L1World.getInstance().broadcastPacketToAll(
								new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "\\fO[잊혀진 섬]: 잊혀진 섬이 개방되었습니다."), true);**/
								setF_IStart(true);
					    	  }
					      }
				 } catch(Exception e2){
						e2.printStackTrace();
				 }
				}
			 
			 private boolean isClose() {
				  Calendar calender = Calendar.getInstance();
				  int hour, minute;
				  hour = calender.get(Calendar.HOUR_OF_DAY);
				  minute = calender.get(Calendar.MINUTE);	 
			   if ((hour == 02 && minute == 00)) {
				  return true;
				  }
				  return false;
				 }
			 
			 /** 종료 **/
			 public void End() {
				 setF_IStart(false);
				 for(L1PcInstance pc : L1World.getInstance().getAllPlayers()) {
			     if (pc.getMapId() == 1700 || pc.getMapId() == 1708 || pc.getMapId() == 1710 || pc.getMapId() == 1703){
				 L1Teleport.teleport(pc, 33970, 33246, (short) 4, 4, true);
				 pc.sendPackets(new S_ChatPacket(pc, "잊혀진 섬 사용시간이 종료되었습니다.", Opcodes.S_MESSAGE, 17));
			     }
				}
				for (L1Object ob : L1World.getInstance().getVisibleObjects(1708).values()) {
					if (ob instanceof L1MonsterInstance) {
						L1MonsterInstance npc = (L1MonsterInstance) ob;
						if (npc == null || npc._destroyed || npc.isDead())
							continue;
							npc.deleteMe();
						}
					}
				for (L1Object ob : L1World.getInstance().getVisibleObjects(1703).values()) {
					if (ob instanceof L1MonsterInstance) {
						L1MonsterInstance npc = (L1MonsterInstance) ob;
						if (npc == null || npc._destroyed || npc.isDead())
							continue;
							npc.deleteMe();
						}
					}
			 	}
		}
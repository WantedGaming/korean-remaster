package l1j.server.server;

import java.util.Calendar;
import java.util.Locale;
import java.text.SimpleDateFormat;

import l1j.server.Config;
import l1j.server.server.model.L1Teleport;
import l1j.server.server.model.L1World;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.model.skill.L1SkillId;
import l1j.server.server.serverpackets.S_PacketBox;
import l1j.server.server.serverpackets.S_SystemMessage;
import l1j.server.server.utils.L1SpawnUtil;

public class ConnectGiftController extends Thread {
	
		private static ConnectGiftController _instance;

		private static long sTime = 0;	
		
		public boolean isGmOpen = false;

		private static final SimpleDateFormat s = new SimpleDateFormat("HH", Locale.KOREA);

		private static final SimpleDateFormat ss = new SimpleDateFormat("MM-dd HH:mm", Locale.KOREA);

		public static ConnectGiftController getInstance() {
			if(_instance == null) {
				_instance = new ConnectGiftController();
			}
			return _instance;
		}
		
		@Override
			public void run() {
					while (true) {
						try	{
						if(isOpen()){
						GiveMarble();
						Thread.sleep(120000);
					}
				} catch(Exception e){
					e.printStackTrace();
				} finally {
					try{
						Thread.sleep(1000L);
					} catch(Exception e){
						e.printStackTrace();
					}
				}
				}
			}

		
			private void GiveMarble(){
				for(L1PcInstance pc : L1World.getInstance().getAllPlayers()) {
				if(pc.getMapId()!= 800){
					if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.BJBUFF)){
					pc.getInventory().storeItem(6104, 2);
					pc.sendPackets(new S_SystemMessage("접속 유지 보상: 접속 보상 스티커 상자(2)"));
					} else {
					pc.getInventory().storeItem(6104, 1);
					pc.sendPackets(new S_SystemMessage("접속 유지 보상: 접속 보상 스티커 상자(1)"));
					}
				} else {
					pc.sendPackets(new S_SystemMessage("접속 보상 실패: 시장 안에서는 접속 유지 보상 불가"));
				}
				}
			}
				
			/**
			 *오픈 시각을 가져온다
			 *
			 *@return (Strind) 오픈 시각(MM-dd HH:mm)
			 */
			 public String OpenTime() {
				 Calendar c = Calendar.getInstance();
				 c.setTimeInMillis(sTime);
				 return ss.format(c.getTime());
			 }

			 /**
			 *영토가 열려있는지 확인
			 *
			 *@return (boolean) 열려있다면 true 닫혀있다면 false
			 */

			 private boolean isOpen() {
				  Calendar calender = Calendar.getInstance();
				  int minute;
				  minute = calender.get(Calendar.MINUTE);
				  if (minute == 01 || minute == 11 || minute == 21 || minute == 31 || minute == 41 ||  minute == 51){
				   return true;
				  }
				  return false;
				 }

}
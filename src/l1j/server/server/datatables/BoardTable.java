/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */
package l1j.server.server.datatables;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import l1j.server.L1DatabaseFactory;
import l1j.server.server.model.Instance.L1ItemInstance;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.utils.SQLUtil;

// Referenced classes of package l1j.server.server:
// IdFactory

public class BoardTable {

	private static Logger _log = Logger.getLogger(BoardTable.class.getName());

	private static BoardTable _instance;
	
	private final Map<Integer, String> _stepnum = new HashMap<Integer, String>();
	private final Map<Integer, String> _buyname = new HashMap<Integer, String>();
	private final Map<Integer, String> _selladena = new HashMap<Integer, String>();
	private final Map<Integer, String> _name = new HashMap<Integer, String>();
	private final Map<Integer, String> _sendname = new HashMap<Integer, String>();
	
	private BoardTable() {
		stepNum();
		buyName();
		sellAdena();
		sellName();
		SendName();
	}
	
	
	public static BoardTable getInstance() {
		if (_instance == null) {
			_instance = new BoardTable();
		}
		return _instance;
	}
	
	public static void reload() { // Gn.67
		BoardTable oldInstance = _instance;
		_instance = new BoardTable();
		oldInstance._stepnum.clear();
		oldInstance._buyname.clear();
		oldInstance._selladena.clear();
		oldInstance._name.clear();
		oldInstance._sendname.clear();
	}
	
	private void stepNum() {
		Connection con = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con.prepareStatement("SELECT * FROM board_user");
			for (rs = pstm.executeQuery(); rs.next();) {
				int id = rs.getInt("id");
				String step = rs.getString("step");
				_stepnum.put(new Integer(id), step);
			}
			_log.config("step " + _stepnum.size());
		} catch (SQLException e) {
			_log.log(Level.SEVERE, e.getLocalizedMessage(), e);
		} finally {
			SQLUtil.close(rs);
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
	}

	private void sellAdena() {
		Connection con = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con.prepareStatement("SELECT * FROM board_user");
			for (rs = pstm.executeQuery(); rs.next();) {
				int id = rs.getInt("id");
				String adena = rs.getString("title");
				_selladena.put(new Integer(id), adena);
			}
			_log.config("selladena " + _selladena.size());
		} catch (SQLException e) {
			_log.log(Level.SEVERE, e.getLocalizedMessage(), e);
		} finally {
			SQLUtil.close(rs);
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
	}
	

	private void sellName() {
		Connection con = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con.prepareStatement("SELECT * FROM board_user");
			for (rs = pstm.executeQuery(); rs.next();) {
				int id = rs.getInt("id");
				String sellName = rs.getString("name");
				_name.put(new Integer(id), sellName);
			}
			_log.config("sellname " + _name.size());
		} catch (SQLException e) {
			_log.log(Level.SEVERE, e.getLocalizedMessage(), e);
		} finally {
			SQLUtil.close(rs);
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
	}
	
	private void buyName() {
		Connection con = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con.prepareStatement("SELECT * FROM board_user");
			for (rs = pstm.executeQuery(); rs.next();) {
				int id = rs.getInt("id");
				String name = rs.getString("buyname");
				_buyname.put(new Integer(id), name);
			}
			_log.config("buyname " + _buyname.size());
		} catch (SQLException e) {
			_log.log(Level.SEVERE, e.getLocalizedMessage(), e);
		} finally {
			SQLUtil.close(rs);
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
	}
	
	private void SendName() {
		Connection con = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con.prepareStatement("SELECT * FROM board_user");
			for (rs = pstm.executeQuery(); rs.next();) {
				int id = rs.getInt("id");
				String sendname = rs.getString("buycharname");
				_sendname.put(new Integer(id), sendname);
			}
			_log.config("buycharname" + _sendname.size());
		} catch (SQLException e) {
			_log.log(Level.SEVERE, e.getLocalizedMessage(), e);
		} finally {
			SQLUtil.close(rs);
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
	}
	
	public String getStep(int id) {
		String sitem = null;
		if (_stepnum.containsKey(id)) {
			sitem = _stepnum.get(id);
		}
		return sitem;
	}
	
	public String getBuyname(int id) {
		String sitem = null;
		if (_buyname.containsKey(id)) {
			sitem = _buyname.get(id);
		}
		return sitem;
	}
	
	public String getAdena(int id) {
		String sitem = null;
		if (_selladena.containsKey(id)) {
			sitem = _selladena.get(id);
		}
		return sitem;
	}
	
	public String getName(int id) {
		String sitem = null;
		if (_name.containsKey(id)) {
			sitem = _name.get(id);
		}
		return sitem;
	}
	
	public String getSendName(int id) {
		String sitem = null;
		if (_sendname.containsKey(id)) {
			sitem = _sendname.get(id);
		}
		return sitem;
	}

	public void writeTopicfree(L1PcInstance pc, String date, String title, String content, int id) {
		int count = 0;
		Connection con = null;
		PreparedStatement pstm1 = null;
		ResultSet rs = null;
		PreparedStatement pstm2 = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm1 = con.prepareStatement("SELECT * FROM board_free ORDER BY board_id DESC");
			rs = pstm1.executeQuery();
			while (rs.next()) {
				if (rs.getInt(6) == id) {
					if (count < rs.getInt(1))
						;
					{
						count = rs.getInt(1);
					}
				}
			}
			pstm2 = con.prepareStatement("INSERT INTO board_free SET id=?, name=?, date=?, title=?, content=?, board_id=?");
			pstm2.setInt(1, (count + 1));
			pstm2.setString(2, pc.getName());
			pstm2.setString(3, date);
			pstm2.setString(4, title);
			pstm2.setString(5, content);
			pstm2.setInt(6, id);
			pstm2.executeUpdate();
		} catch (SQLException e) {
			_log.log(Level.SEVERE, e.getLocalizedMessage(), e);
		} finally {
			SQLUtil.close(rs);
			SQLUtil.close(pstm1);
			SQLUtil.close(pstm2);
			SQLUtil.close(con);
		}
	}

	public void writeTopic(L1PcInstance pc, String date, String title, String content, int id) {
		int count = 0;
		Connection con = null;
		PreparedStatement pstm1 = null;
		ResultSet rs = null;
		PreparedStatement pstm2 = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm1 = con.prepareStatement("SELECT * FROM board ORDER BY board_id DESC");
			rs = pstm1.executeQuery();
			while (rs.next()) {
				if (rs.getInt(6) == id) {
					if (count < rs.getInt(1));
					{
					count = rs.getInt(1);
					}
				}
			}
			pstm2 = con.prepareStatement("INSERT INTO board SET id=?, name=?, date=?, title=?, content=?, board_id=?");
			pstm2.setInt(1, (count + 1));
			pstm2.setString(2, pc.getName());
			pstm2.setString(3, date);
			pstm2.setString(4, title);
			pstm2.setString(5, content);
			pstm2.setInt(6, id);
			pstm2.executeUpdate();
		} catch (SQLException e) {
			_log.log(Level.SEVERE, e.getLocalizedMessage(), e);
		} finally {
			SQLUtil.close(rs);
			SQLUtil.close(pstm1);
			SQLUtil.close(pstm2);
			SQLUtil.close(con);
		}
	}

	public void writeTopicUser(L1PcInstance pc, String date, String title, String content, String step) {
		int count = 0;
		Connection con = null;
		PreparedStatement pstm1 = null;
		ResultSet rs = null;
		PreparedStatement pstm2 = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm1 = con.prepareStatement("SELECT * FROM board_user ORDER BY id DESC");
			rs = pstm1.executeQuery();
			if (rs.next()) {
				count = rs.getInt("id");
			}
			pstm2 = con.prepareStatement("INSERT INTO board_user SET id=?, name=?, step=?, title=?, date=?, content=?");
			pstm2.setInt(1, (count + 1));
			pstm2.setString(2, pc.getName());
			pstm2.setString(3, step);
			pstm2.setString(4, title);
			pstm2.setString(5, date);
			pstm2.setString(6, content);
			pstm2.executeUpdate();

		} catch (SQLException e) {
			_log.log(Level.SEVERE, e.getLocalizedMessage(), e);
		} finally {
			SQLUtil.close(rs);
			SQLUtil.close(pstm1);
			SQLUtil.close(pstm2);
			SQLUtil.close(con);
		}
	}

	public void writeDragonKey(L1PcInstance pc, L1ItemInstance key, String date, int id) {
		int count = 0;

		Connection con = null;
		PreparedStatement pstm1 = null;
		ResultSet rs = null;
		PreparedStatement pstm2 = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm1 = con
					.prepareStatement("SELECT * FROM board ORDER BY board_id DESC");
			rs = pstm1.executeQuery();
			while (rs.next()) {
				if (rs.getInt(6) == id) {
					if (count < rs.getInt(1))
						;
					{
						count = rs.getInt(1);
					}
				}
			}
			pstm2 = con.prepareStatement("INSERT INTO board SET id=?, name=?, date=?, board_id=?, remaining_time=?, item_id=?");
			pstm2.setInt(1, (count + 1));
			pstm2.setString(2, pc.getName());
			pstm2.setString(3, date);
			pstm2.setInt(4, id);
			pstm2.setTimestamp(5, key.getEndTime());
			pstm2.setInt(6, key.getId());
			pstm2.executeUpdate();
		} catch (SQLException e) {
			_log.log(Level.SEVERE, e.getLocalizedMessage(), e);
		} finally {
			SQLUtil.close(rs);
			SQLUtil.close(pstm1);
			SQLUtil.close(pstm2);
			SQLUtil.close(con);
		}
	}

	public void deleteTopicfree(int number) {
		Connection con = null;
		PreparedStatement pstm = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con.prepareStatement("DELETE FROM board_free WHERE id=?");
			pstm.setInt(1, number);
			pstm.executeUpdate();
		} catch (SQLException e) {
			_log.log(Level.SEVERE, e.getLocalizedMessage(), e);
		} finally {
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
	}

	public void deleteTopic(int number) {
		Connection con = null;
		PreparedStatement pstm = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con.prepareStatement("DELETE FROM board WHERE id=?");
			pstm.setInt(1, number);
			pstm.executeUpdate();
		} catch (SQLException e) {
			_log.log(Level.SEVERE, e.getLocalizedMessage(), e);
		} finally {
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
	}
	
	public void deleteAdena(int number) {
		Connection con = null;
		PreparedStatement pstm = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con.prepareStatement("DELETE FROM board_user WHERE id=?");
			pstm.setInt(1, number);
			pstm.executeUpdate();
		} catch (SQLException e) {
			_log.log(Level.SEVERE, e.getLocalizedMessage(), e);
		} finally {
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
	}


	/**
	 * 등록된 사람이 있나 찾는다 (드래곤 키)
	 * 
	 * @param name
	 * @param npcid
	 * @return
	 */
	public boolean checkExistName(String name, int npcid) {
		boolean result = true;
		java.sql.Connection con = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con.prepareStatement("SELECT name FROM board WHERE board_id=? AND name=?");
			pstm.setInt(1, npcid);
			pstm.setString(2, name);
			rs = pstm.executeQuery();
			result = rs.next();
		} catch (SQLException e) {
			_log.warning("could not check existing charname:" + e.getMessage());
		} finally {
			SQLUtil.close(rs);
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
		return result;
	}

	/**
	 * 키가 등록되었는지 찾는다 (드래곤 키)
	 * 
	 * @param name
	 * @param key
	 * @return
	 */
	public boolean checkExistkey(int key, int npcid) {
		boolean result = true;
		java.sql.Connection con = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con.prepareStatement("SELECT name FROM board WHERE board_id=? AND item_id=?");
			pstm.setInt(1, npcid);
			pstm.setInt(2, key);
			rs = pstm.executeQuery();
			result = rs.next();
		} catch (SQLException e) {
			_log.warning("could not check existing charname:" + e.getMessage());
		} finally {
			SQLUtil.close(rs);
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
		return result;
	}

	/**
	 * 게시판에서 해당 키 내용을 삭제한다
	 * 
	 * @param id
	 *            (아이템 id)
	 */
	public void delDayExpire(int id) {
		Connection con = null;
		PreparedStatement pstm = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con.prepareStatement("DELETE FROM board WHERE item_id=?");
			pstm.setInt(1, id);
			pstm.executeUpdate();

		} catch (SQLException e) {
			_log.log(Level.SEVERE, e.getLocalizedMessage(), e);
		} finally {
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}

	}
}

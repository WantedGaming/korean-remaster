package l1j.server.server;

import java.util.Calendar;

import l1j.server.server.model.L1World;
import l1j.server.server.serverpackets.S_PacketBox;
import l1j.server.server.utils.L1SpawnUtil;
import server.manager.eva;

public class FieldBossController2 extends Thread {
	
		private static FieldBossController2 _instance;

		private boolean _FieldBossStart;
		public boolean getFieldBossStart() {
			return _FieldBossStart;
		}
		public void setFieldBossStart(boolean FieldBoss) {
			_FieldBossStart = FieldBoss;
		}

		public boolean isGmOpen = false;
		
		public static FieldBossController2 getInstance() {
			if(_instance == null) {
				_instance = new FieldBossController2();
			}
			return _instance;
		}
		
		@Override
			public void run() {
			while (true) {
			try	{		
						if(isOpen()){
						Spawn1();
						Thread.sleep(120000);
						End();
					}
				} catch(Exception e){
					e.printStackTrace();
				}finally{
					try	{
						Thread.sleep(1000);
					}catch(Exception e){
					}
				}
			}
		}
			
			 private void Spawn1(){
				 try{
					 L1SpawnUtil.spawn2(32731, 32803, (short) 812, 45601, 0, 3600*1000, 0);//데스나이트
						/**L1World.getInstance().broadcastServerMessage("\\aI[보스 알림]: 데스나이트 (글루디오 던전6층)");
						L1World.getInstance().broadcastPacketToAll(new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "\\fO[보스 알림]: 데스나이트 (글루디오 던전6층)"), true);
					 eva.BossLogAppend("[보스스폰] 데스나이트 (글루디오 던전6층)");	**/
				 }catch(Exception e2){
						e2.printStackTrace();
				 }
				}
			 
			 private boolean isOpen() {
				  Calendar calender = Calendar.getInstance();
				  int hour, minute;
				  hour = calender.get(Calendar.HOUR_OF_DAY);
				  minute = calender.get(Calendar.MINUTE);	 
			   if ((hour == 1 && minute == 05)
					   || (hour == 6 && minute == 05)
					   || (hour == 13 && minute == 05)
					   || (hour == 18 && minute == 05)) {
				  return true;
				  }
				  return false;
				 }
			 
			 /** 종료 **/
			 public void End() {
				 setFieldBossStart(false);
			 }
}
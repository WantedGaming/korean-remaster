package l1j.server.server;

import java.util.Calendar;
import java.util.Locale;
import java.text.SimpleDateFormat;

import l1j.server.Config;
import l1j.server.server.model.L1Teleport;
import l1j.server.server.model.L1World;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.serverpackets.S_ChatPacket;
import l1j.server.server.serverpackets.S_PacketBox;
import l1j.server.server.serverpackets.S_SystemMessage;
import l1j.server.server.utils.L1SpawnUtil;

public class GidunBlessController extends Thread {
	
		private static GidunBlessController _instance;

		private boolean _GidunStart;
		public boolean getGidunStart() {
			return _GidunStart;
		}
		public void setGidunStart(boolean Gidun) {
			_GidunStart = Gidun;
		}
		private static long sTime = 0;	
		
		public boolean isGmOpen = false;

		private static final SimpleDateFormat s = new SimpleDateFormat("HH", Locale.KOREA);

		private static final SimpleDateFormat ss = new SimpleDateFormat("MM-dd HH:mm", Locale.KOREA);

		public static GidunBlessController getInstance() {
			if(_instance == null) {
				_instance = new GidunBlessController();
			}
			return _instance;
		}
		
		@Override
			public void run() {
			try	{
					while (true) {
						Thread.sleep(1000); 
						/** 오픈 **/
						if(!isGmOpen)
							continue;
						if(L1World.getInstance().getAllPlayers().size() <= 0)
							continue;
						
						isGmOpen = false;

						/** 오픈 메세지 **/
						L1World.getInstance().broadcastServerMessage("\\aG기란 감옥에 아인하사드의 축복이 충만합니다.");
						L1World.getInstance().broadcastServerMessage("\\aG앞으로 1시간 동안 기란감옥 사냥시 경험치");
						L1World.getInstance().broadcastServerMessage("\\aG획득이 50% 추가됩니다.");
						L1World.getInstance().broadcastPacketToAll(new S_PacketBox(S_PacketBox.GREEN_MESSAGE, 
								"기란감옥에 아인하사드의 축복이 충만합니다. 이 축복은 1시간 동안 적용됩니다."));

						/** 기감 축복 시작**/
						setGidunStart(true);
						
						Thread.sleep(900000L);
						MSG1();
						Thread.sleep(900000L);
						MSG1();
						Thread.sleep(900000L);
						MSG1();
						Thread.sleep(900000L);
						L1World.getInstance().broadcastServerMessage("\\aG기란 감옥에 아인하사드의 축복이 사라집니다."); 	
						/** 5초 뒤에 다시한번 텔레포트 마을로 **/
						Thread.sleep(5000L);
						setGidunStart(false);
						
						/** 5초 뒤에 다시한번 텔레포트 마을로 **/
						Thread.sleep(5000L);
						setGidunStart(false);

						/** 종료메세지 출력 **/
						End();
					}
				} catch(Exception e){
					e.printStackTrace();
				}
			}

		private void MSG1(){
			 for(L1PcInstance pc : L1World.getInstance().getAllPlayers()) {
				 pc.sendPackets(new S_ChatPacket(pc, "기란감옥에 아인하사드의 축복이 충만합니다.", Opcodes.S_MESSAGE, 17));
				 }
			}
			/**
			 *오픈 시각을 가져온다
			 *
			 *@return (Strind) 오픈 시각(MM-dd HH:mm)
			 */
			 public String OpenTime() {
				 Calendar c = Calendar.getInstance();
				 c.setTimeInMillis(sTime);
				 return ss.format(c.getTime());
			 }

			 /**
			 *실제 현재시각을 가져온다
			 *
			 *@return (String) 현재 시각(HH:mm)
			 */
			 private String getTime() {
				 return s.format(Calendar.getInstance().getTime());
			 }


			 /** 종료 **/
			 private void End() {
				 setGidunStart(false);
			 }
}
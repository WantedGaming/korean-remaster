/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */

package l1j.server.server;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Constructor;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.StringTokenizer;
import java.util.concurrent.ScheduledFuture;
import java.util.logging.Level;
import java.util.logging.Logger;

import javolution.util.FastMap;
import javolution.util.FastTable;
import kr.PeterMonk.GameSystem.SupportSystem.SupportMapTable;
import l1j.server.Base64;
import l1j.server.Config;
import l1j.server.L1DatabaseFactory;
import l1j.server.SpecialEventHandler;
import l1j.server.Database.DB;
import l1j.server.GameSystem.BossTimer;
import l1j.server.GameSystem.NpcShopSystem;
import l1j.server.GameSystem.Advertisement.BadukiChatController;
import l1j.server.GameSystem.Advertisement.ItemBayChatController;
import l1j.server.GameSystem.Advertisement.NurtureChatController;
import l1j.server.GameSystem.EventShop.EventShopList;
import l1j.server.GameSystem.EventShop.EventShopTable;
import l1j.server.GameSystem.Gamble.Gamble;
import l1j.server.GameSystem.NpcBuyShop.NpcBuyShop;
import l1j.server.GameSystem.NpcTradeShop.NpcTradeShop;
import l1j.server.GameSystem.Robot.L1RobotInstance;
import l1j.server.GameSystem.Robot.Robot;
import l1j.server.GameSystem.Robot.Robot_Bugbear;
import l1j.server.GameSystem.Robot.Robot_ConnectAndRestart;
import l1j.server.GameSystem.Robot.Robot_Crown;
import l1j.server.GameSystem.Robot.Robot_Fish;
import l1j.server.GameSystem.Robot.Robot_Hunt;
import l1j.server.GameSystem.UserRanking.UserRankingController;
import l1j.server.server.AzmodanSystem;
import l1j.server.Warehouse.ClanWarehouse;
import l1j.server.Warehouse.PrivateWarehouse;
import l1j.server.Warehouse.SupplementaryService;
import l1j.server.Warehouse.WarehouseManager;
import l1j.server.server.TimeController.WarTimeController;
import l1j.server.server.clientpackets.C_AuthLogin;
import l1j.server.server.clientpackets.C_SelectCharacter;
import l1j.server.server.command.L1Commands;
import l1j.server.server.command.executor.L1CommandExecutor;
import l1j.server.server.datatables.AutoLoot;
import l1j.server.server.datatables.BoardTable;
import l1j.server.server.datatables.CastleTable;
import l1j.server.server.datatables.CharacterTable;
import l1j.server.server.datatables.ClanTable;
import l1j.server.server.datatables.DoorSpawnTable;
import l1j.server.server.datatables.DropTable;
import l1j.server.server.datatables.ExpTable;
import l1j.server.server.datatables.HouseTable;
import l1j.server.server.datatables.IpPhoneCertificationTable;
import l1j.server.server.datatables.IpTable;
import l1j.server.server.datatables.ItemTable;
import l1j.server.server.datatables.LogTable;
import l1j.server.server.datatables.MapsTable;
import l1j.server.server.datatables.ModelSpawnTable;
import l1j.server.server.datatables.NpcShopSpawnTable;
import l1j.server.server.datatables.NpcTable;
import l1j.server.server.datatables.PetTable;
import l1j.server.server.datatables.PetsSkillsTable;
import l1j.server.server.datatables.PhoneCheck;
import l1j.server.server.datatables.SprTable;
import l1j.server.server.datatables.UBTable;
import l1j.server.server.model.Broadcaster;
import l1j.server.server.model.CharPosUtil;
import l1j.server.server.model.L1BugBearRace;
import l1j.server.server.model.L1Clan;
import l1j.server.server.model.L1Cooking;
import l1j.server.server.model.L1EffectSpawn;
import l1j.server.server.model.L1Inventory;
import l1j.server.server.model.L1NpcDeleteTimer;
import l1j.server.server.model.L1Object;
import l1j.server.server.model.L1PcInventory;
import l1j.server.server.model.L1Teleport;
import l1j.server.server.model.L1UltimateBattle;
import l1j.server.server.model.L1War;
import l1j.server.server.model.L1World;
import l1j.server.server.model.Support;
import l1j.server.server.model.Instance.L1DollInstance;
import l1j.server.server.model.Instance.L1DoorInstance;
import l1j.server.server.model.Instance.L1ItemInstance;
import l1j.server.server.model.Instance.L1MerchantInstance;
import l1j.server.server.model.Instance.L1MonsterInstance;
import l1j.server.server.model.Instance.L1NpcInstance;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.model.Instance.L1PetInstance;
import l1j.server.server.model.item.function.PetSummons;
import l1j.server.server.model.skill.L1SkillId;
import l1j.server.server.model.skill.L1SkillUse;
import l1j.server.server.serverpackets.S_ACTION_UI;
import l1j.server.server.serverpackets.S_Ability;
import l1j.server.server.serverpackets.S_AttackPacket;
import l1j.server.server.serverpackets.S_Chainfo;
import l1j.server.server.serverpackets.S_CharTitle;
import l1j.server.server.serverpackets.S_ChatPacket;
import l1j.server.server.serverpackets.S_ClanJoinLeaveStatus;
import l1j.server.server.serverpackets.S_ClanWindow;
import l1j.server.server.serverpackets.S_DRAGONPERL;
import l1j.server.server.serverpackets.S_DeleteInventoryItem;
import l1j.server.server.serverpackets.S_Disconnect;
import l1j.server.server.serverpackets.S_DoActionGFX;
import l1j.server.server.serverpackets.S_Door;
import l1j.server.server.serverpackets.S_EffectLocation;
import l1j.server.server.serverpackets.S_Emblem;
import l1j.server.server.serverpackets.S_GMCommands;
import l1j.server.server.serverpackets.S_InvCheck;
import l1j.server.server.serverpackets.S_ItemName;
import l1j.server.server.serverpackets.S_MatizAlarm;
import l1j.server.server.serverpackets.S_NPCTalkReturn;
import l1j.server.server.serverpackets.S_NewCreateItem;
import l1j.server.server.serverpackets.S_PacketBox;
import l1j.server.server.serverpackets.S_PetPack;
import l1j.server.server.serverpackets.S_PetWindow;
import l1j.server.server.serverpackets.S_RemoveObject;
import l1j.server.server.serverpackets.S_ReturnedStat;
import l1j.server.server.serverpackets.S_SabuTell;
import l1j.server.server.serverpackets.S_ServerMessage;
import l1j.server.server.serverpackets.S_SkillBrave;
import l1j.server.server.serverpackets.S_SkillIconGFX;
import l1j.server.server.serverpackets.S_SkillSound;
import l1j.server.server.serverpackets.S_Sound;
import l1j.server.server.serverpackets.S_SupportSystem;
import l1j.server.server.serverpackets.S_SystemMessage;
import l1j.server.server.serverpackets.S_TempInv;
import l1j.server.server.serverpackets.S_Weather;
import l1j.server.server.serverpackets.S_문장주시;
import l1j.server.server.storage.mysql.MySqlCharacterStorage;
import l1j.server.server.templates.L1Command;
import l1j.server.server.templates.L1House;
import l1j.server.server.templates.L1Item;
import l1j.server.server.templates.L1Npc;
import l1j.server.server.utils.CommonUtil;
import l1j.server.server.utils.DeadLockDetector;
import l1j.server.server.utils.IntRange;
import l1j.server.server.utils.L1SpawnUtil;
import l1j.server.server.utils.SQLUtil;
import server.GameServer;
import server.LineageClient;
import server.manager.eva;
import server.system.autoshop.AutoShop;
import server.system.autoshop.AutoShopManager;
import server.threads.pc.AutoCheckThread;

public class GMCommands {

	private static Logger _log = Logger.getLogger(GMCommands.class.getName());

	public static boolean sellShopNotice = false;
	public static boolean restartBot = false;
	public static boolean bugbearBot = false;
	public static boolean clanBot = false;
	public static boolean fishBot = false;
	public static boolean huntBot = false;
	// public static boolean 매입상점 = false;
	public static boolean 자동생성방지 = false;
	public static boolean 주시아이피체크 = false;
	public static boolean 무인상점구매체크 = true;
	public static boolean 케릭인증영자방 = false;
	public static boolean 트리플포우스핵 = false;
	public static boolean 용해로그 = false;
	public static boolean 접속이름체크 = false;
	public static boolean 아덴교환체크 = false;
	public static boolean 엔진체크 = true;
//    public static boolean 웹인증코드 = false;

	public static boolean 범위로그 = false;
	public static boolean 마법속도체크 = false;
	private static GMCommands _instance;

	private GMCommands() {
	}

	public static GMCommands getInstance() {
		if (_instance == null) {
			_instance = new GMCommands();
		}
		return _instance;
	}

	private String complementClassName(String className) {
		if (className.contains(".")) {
			return className;
		}
		return "l1j.server.server.command.executor." + className;
	}

	private boolean executeDatabaseCommand(L1PcInstance pc, String name, String arg) {
		try {
			L1Command command = L1Commands.get(name);
			if (command == null) {
				return false;
			}
			if (pc.getAccessLevel() < command.getLevel()) {
				pc.sendPackets(new S_ServerMessage(74, "커멘드 " + name), true); // \f1%0은 사용할 수 없습니다.
				return true;
			}

			Class<?> cls = Class.forName(complementClassName(command.getExecutorClassName()));
			L1CommandExecutor exe = (L1CommandExecutor) cls.getMethod("getInstance").invoke(null);
			
			exe.execute(pc, name, arg);
			eva.LogCommandAppend(pc.getName(), name, arg);
			return true;
		} catch (Exception e) {
			// _log.log(Level.SEVERE, "error gm command", e);
		}
		return false;
	}

	public static final int[] hextable = { 0x80, 0x81, 0x82, 0x83, 0x84, 0x85,
			0x86, 0x87, 0x88, 0x89, 0x8a, 0x8b, 0x8c, 0x8d, 0x8e, 0x8f, 0x90,
			0x91, 0x92, 0x93, 0x94, 0x95, 0x96, 0x97, 0x98, 0x99, 0x9a, 0x9b,
			0x9c, 0x9d, 0x9e, 0x9f, 0xa0, 0xa1, 0xa2, 0xa3, 0xa4, 0xa5, 0xa6,
			0xa7, 0xa8, 0xa9, 0xaa, 0xab, 0xac, 0xad, 0xae, 0xaf, 0xb0, 0xb1,
			0xb2, 0xb3, 0xb4, 0xb5, 0xb6, 0xb7, 0xb8, 0xb9, 0xba, 0xbb, 0xbc,
			0xbd, 0xbe, 0xbf, 0xc0, 0xc1, 0xc2, 0xc3, 0xc4, 0xc5, 0xc6, 0xc7,
			0xc8, 0xc9, 0xca, 0xcb, 0xcc, 0xcd, 0xce, 0xcf, 0xd0, 0xd1, 0xd2,
			0xd3, 0xd4, 0xd5, 0xd6, 0xd7, 0xd8, 0xd9, 0xda, 0xdb, 0xdc, 0xdd,
			0xde, 0xdf, 0xe0, 0xe1, 0xe2, 0xe3, 0xe4, 0xe5, 0xe6, 0xe7, 0xe8,
			0xe9, 0xea, 0xeb, 0xec, 0xed, 0xee, 0xef, 0xf0, 0xf1, 0xf2, 0xf3,
			0xf4, 0xf5, 0xf6, 0xf7, 0xf8, 0xf9, 0xfa, 0xfb, 0xfc, 0xfd, 0xfe,
			0xff };

	private static void Delete(String id_name) {
		Connection con = null;
		PreparedStatement pstm = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con
					.prepareStatement("delete FROM character_items WHERE item_id IN ("
							+ id_name + ")");
			pstm.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			SQLUtil.close(pstm);
		}
		try {
			pstm = con
					.prepareStatement("delete FROM character_warehouse WHERE item_id in ("
							+ id_name + ")");
			pstm.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			SQLUtil.close(pstm);
		}
		try {
			pstm = con
					.prepareStatement("delete FROM clan_warehouse WHERE item_id in ("
							+ id_name + ")");
			pstm.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			SQLUtil.close(pstm);
		}
		try {
			pstm = con
					.prepareStatement("delete FROM character_elf_warehouse WHERE item_id in ("
							+ id_name + ")");
			pstm.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
	}

	private static void Delete21269() {
		Connection con = null;
		PreparedStatement pstm = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con
					.prepareStatement("delete FROM character_items WHERE item_id=21269 AND enchantlvl < 6");
			pstm.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			SQLUtil.close(pstm);
		}
		try {
			pstm = con
					.prepareStatement("delete FROM character_warehouse WHERE item_id=21269 AND enchantlvl < 6");
			pstm.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			SQLUtil.close(pstm);
		}
		try {
			pstm = con
					.prepareStatement("delete FROM clan_warehouse WHERE item_id=21269 AND enchantlvl < 6");
			pstm.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			SQLUtil.close(pstm);
		}
		try {
			pstm = con
					.prepareStatement("delete FROM character_elf_warehouse WHERE item_id=21269 AND enchantlvl < 6");
			pstm.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
	}

	private static int 할로윈리스트[] = { 90085, 90086, 90087, 90088, 90089, 90090,
			90091, 90092, 160423, 435000, 160510, 160511, 21123, 21269 };
	private static int 할로윈리스트2[] = { 90085, 90086, 90087, 90088, 90089, 90090,
			90091, 90092, 160423, 435000, 160510, 160511, 21123 };

	public synchronized static void EventItemDelete() {
		try {
			for (L1PcInstance tempPc : L1World.getInstance().getAllPlayers()) {
				if (tempPc == null)
					continue;
				for (int i = 0; i < 할로윈리스트.length; i++) {
					L1ItemInstance[] item = tempPc.getInventory().findItemsId(
							할로윈리스트[i]);

					if (item != null && item.length > 0) {
						for (int o = 0; o < item.length; o++) {
							if (item[o].getItemId() == 21269) {
								if (item[o].getEnchantLevel() >= 6) {
									if (item[o].getBless() >= 128)
										item[o].setBless(128);
									else
										item[o].setBless(0);
									tempPc.getInventory().updateItem(item[o],
											L1PcInventory.COL_BLESS);
									tempPc.getInventory().saveItem(item[o],
											L1PcInventory.COL_BLESS);
									continue;
								}
							}
							if (item[o].isEquipped()) {
								tempPc.getInventory().setEquipped(item[o],
										false); // 추가해주세요.
							}
							tempPc.getInventory().removeItem(item[o]);
						}
					}
					try {
						PrivateWarehouse pw = WarehouseManager.getInstance()
								.getPrivateWarehouse(tempPc.getAccountName());
						L1ItemInstance[] item2 = pw.findItemsId(할로윈리스트[i]);
						if (item2 != null && item2.length > 0) {
							for (int o = 0; o < item2.length; o++) {
								if (item[o].getItemId() == 21269) {
									if (item[o].getEnchantLevel() >= 6) {
										if (item[o].getBless() >= 128)
											item[o].setBless(128);
										else
											item[o].setBless(0);
										tempPc.getInventory().updateItem(
												item[o],
												L1PcInventory.COL_BLESS);
										tempPc.getInventory().saveItem(item[o],
												L1PcInventory.COL_BLESS);
										continue;
									}
								}
								pw.removeItem(item2[o]);
							}
						}
					} catch (Exception e) {
					}
					try {
						if (tempPc.getClanid() > 0) {
							ClanWarehouse cw = WarehouseManager.getInstance()
									.getClanWarehouse(tempPc.getClanname());
							L1ItemInstance[] item3 = cw.findItemsId(할로윈리스트[i]);
							if (item3 != null && item3.length > 0) {
								for (int o = 0; o < item3.length; o++) {
									if (item3[o].getItemId() == 21269) {
										if (item3[o].getEnchantLevel() >= 6) {
											if (item3[o].getBless() >= 128)
												item3[o].setBless(128);
											else
												item3[o].setBless(0);
											tempPc.getInventory().updateItem(
													item3[o],
													L1PcInventory.COL_BLESS);
											tempPc.getInventory().saveItem(
													item3[o],
													L1PcInventory.COL_BLESS);
											continue;
										}
									}
									cw.removeItem(item3[o]);
								}
							}
						}
					} catch (Exception e) {
					}
					try {
						Collection<L1NpcInstance> pList = tempPc.getPetList();
						if (pList.size() > 0) {
							for (L1NpcInstance npc : pList) {
								L1ItemInstance[] pitem = npc.getInventory()
										.findItemsId(할로윈리스트[i]);
								if (pitem != null && pitem.length > 0) {
									for (int o = 0; o < pitem.length; o++) {
										if (pitem[o].getItemId() == 21269) {
											if (pitem[o].getEnchantLevel() >= 6) {
												if (pitem[o].getBless() >= 128)
													pitem[o].setBless(128);
												else
													pitem[o].setBless(0);
												tempPc.getInventory()
														.updateItem(
																pitem[o],
																L1PcInventory.COL_BLESS);
												tempPc.getInventory()
														.saveItem(
																pitem[o],
																L1PcInventory.COL_BLESS);
												continue;
											}
										}
										npc.getInventory().removeItem(pitem[o]);
									}
								}
							}
						}
					} catch (Exception e) {
					}
				}
			}
			try {
				for (L1Object obj : L1World.getInstance().getAllItem()) {
					if (!(obj instanceof L1ItemInstance))
						continue;
					L1ItemInstance temp_item = (L1ItemInstance) obj;
					if (temp_item.getItemOwner() == null
							|| !(temp_item.getItemOwner() instanceof L1RobotInstance)) {
						if (temp_item.getX() == 0 && temp_item.getY() == 0)
							continue;
					}
					for (int ii = 0; ii < 할로윈리스트.length; ii++) {
						if (할로윈리스트[ii] == temp_item.getItemId()) {
							if (temp_item.getItemId() == 21269) {
								if (temp_item.getEnchantLevel() >= 6) {
									if (temp_item.getBless() >= 128)
										temp_item.setBless(128);
									else
										temp_item.setBless(0);
									continue;
								}
							}
							L1Inventory groundInventory = L1World.getInstance()
									.getInventory(temp_item.getX(),
											temp_item.getY(),
											temp_item.getMapId());
							groundInventory.removeItem(temp_item);
							break;
						}
					}

				}
			} catch (Exception e) {
			}
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < 할로윈리스트2.length; i++) {
				sb.append(+할로윈리스트2[i]);
				if (i < 할로윈리스트2.length - 1) {
					sb.append(",");
				}
			}
			Delete(sb.toString());
			Delete21269();
			BlessUpdate(21269);
		} catch (Exception e) {
		}
	}
	
	
    
	public static void BlessUpdate(int itemid) {
		Connection con = null;
		Connection con2 = null;
		PreparedStatement pstm = null;
		PreparedStatement pstm2 = null;
		ResultSet rs = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con.prepareStatement("SELECT id, bless, enchantlvl FROM character_items WHERE item_id=?");
			// pstm =
			// con.prepareStatement("SELECT * FROM character_items WHERE item_id=?");
			pstm.setInt(1, itemid);
			rs = pstm.executeQuery();
			while (rs.next()) {
				int id = rs.getInt("id");
				int bless = rs.getInt("bless");
				int ent = rs.getInt("enchantlvl");
				if (ent < 6)
					continue;
				try {
					con2 = L1DatabaseFactory.getInstance().getConnection();
					pstm2 = con2
							.prepareStatement("UPDATE character_items SET bless =? WHERE id=?");
					pstm2.setInt(1, bless > 128 ? 128 : 0);
					pstm2.setInt(2, id);
					pstm2.executeUpdate();
				} finally {
					SQLUtil.close(pstm2);
					SQLUtil.close(con2);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			SQLUtil.close(rs);
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
	}

	public void handleCommands(L1PcInstance gm, String cmdLine) {
		StringTokenizer token = new StringTokenizer(cmdLine);
		// 최초의 공백까지가 커맨드, 그 이후는 공백을 단락으로 한 파라미터로서 취급한다
		if (!token.hasMoreTokens()) {
			return;
		}
		String cmd = token.nextToken();
		String param = "";
		while (token.hasMoreTokens()) {
			param = new StringBuilder(param).append(token.nextToken()).append(' ').toString();
		}
		param = param.trim();

		// 데이타베이스화 된 커멘드
		if (executeDatabaseCommand(gm, cmd, param)) {
			if (!cmd.equalsIgnoreCase("재실행")) {
				_lastCommands.put(gm.getId(), cmdLine);
			}
			return;
		}

		if (gm.getAccessLevel() < 200) {
			gm.sendPackets(new S_ServerMessage(74, "커맨드 " + cmd), true);
			return;
		}

		if (gm.isSGm() && !cmd.equalsIgnoreCase("압류해제") && !cmd.equalsIgnoreCase("감옥") && !cmd.equalsIgnoreCase("마을") && !cmd.equalsIgnoreCase("맵누구")) {
			gm.sendPackets(new S_ServerMessage(74, "커맨드 " + cmd), true);
			return;
		}
		eva.LogCommandAppend(gm.getName(), cmdLine);
		// GM에 개방하는 커맨드는 여기에 쓴다
		if (cmd.equalsIgnoreCase("도움말")) {
			gm.sendPackets(new S_SupportSystem(gm, S_SupportSystem.SC_FINISH_PLAY_SUPPORT_ACK), true);
			showHelp(gm);
		} else if (cmd.equalsIgnoreCase("텔렉")) {
			L1Teleport.teleport(gm, gm.getX(), gm.getY(), (short) gm.getMapId(), gm.getMoveState().getHeading(), true);
		} else if (cmd.equalsIgnoreCase("펫스킬")) {
			FSkill(gm, param);
		} else if (cmd.equalsIgnoreCase("펫레벨")) {
			Petlevelup(gm, param);
		} else if (cmd.equalsIgnoreCase("펫우정")) {
			Fpoint(gm, param);
		} else if (cmd.equalsIgnoreCase("랭킹갱신")) {
			UserRankingController.isRenewal = true;
			gm.sendPackets(new S_SystemMessage("랭킹이 갱신 되었습니다."));
		} else if (cmd.equalsIgnoreCase("킬멘트")) {
			killment(gm, param); // by 사부 킬 멘트
		} else if (cmd.equalsIgnoreCase("자동압류")) {
			자동압류(gm, param);
		} else if (cmd.equalsIgnoreCase("판매상점")) {
			sellshop(gm);
		} else if (cmd.equalsIgnoreCase("악마왕")) {
			DevilController.getInstance().isGmOpen = true;
		} else if (cmd.equalsIgnoreCase("쿵")){
			태클버그(gm, param);
		} else if (cmd.equalsIgnoreCase("잊섬개방")) {
			FIController.getInstance().setF_IStart(true);
		} else if (cmd.equalsIgnoreCase("잊섬종료")) {
			FIController.getInstance().End();
			
		} else if (cmd.equalsIgnoreCase("고라스오픈")) {
			Goras1Controller.getInstance().setG_RStart(true);
		} else if (cmd.equalsIgnoreCase("고라스종료")) {
			Goras1Controller.getInstance().End();
		} else if (cmd.equalsIgnoreCase("뜨거운오픈")) {
			DGController.getInstance().setD_GStart(true);
		} else if (cmd.equalsIgnoreCase("뜨거운종료")) {
			DGController.getInstance().End();
		} else if (cmd.equalsIgnoreCase("지배정상오픈")) {
			Jibae11.getInstance().setJ_B11Start(true);
		} else if (cmd.equalsIgnoreCase("지배정상종료")) {
			Jibae11.getInstance().End();
		} else if (cmd.equalsIgnoreCase("결계오픈")) {
			JibaeGG.getInstance().setG_GStart(true);
		} else if (cmd.equalsIgnoreCase("결계종료")) {
			JibaeGG.getInstance().End();

			
        } else if (cmd.equals("아지트")) { 
            GiveHouse(gm, param);
		} else if (cmd.equalsIgnoreCase("일퀘초기화")) {
			if(!DayQuestController.getInstance().getDayQuestStart()){
		    DayQuestController.getInstance().isGmOpen = true;
			}else{
				gm.sendPackets(new S_SystemMessage("초기화 진행중입니다. 잠시 기다려주세요."), true);
			}
		} else if (cmd.equalsIgnoreCase("매물")){
			매입선물(gm, param);
		} else if (cmd.equalsIgnoreCase("노나메")){
			노나메의가호(gm, param);
		} else if (cmd.equalsIgnoreCase("이스마엘")){
			이스마엘의가호(gm, param);
		} else if (cmd.equalsIgnoreCase("선물상자")) {
		    GiftBoxController.getInstance().isGmOpen = true;
		} else if (cmd.equalsIgnoreCase("방송인증")) {
			   bjbuff(gm, param);
		} else if (cmd.equalsIgnoreCase("방송중단")) {
			   bjbuffstop(gm, param);
		} else if (cmd.equalsIgnoreCase("온라인")) {
			   AllPlayerList(gm, param);
		} else if (cmd.equalsIgnoreCase("인첸축복")) {
			   인첸축복(gm, param);
		} else if (cmd.equalsIgnoreCase("기운축복")) {
			   기운축복(gm, param);
		} else if (cmd.equalsIgnoreCase("폰인증시작")) {
			폰인증시작(gm);
		} else if (cmd.equalsIgnoreCase("데페작업")) {
			jakjak(gm);
		} else if (cmd.equalsIgnoreCase("아머작업")) {
			jakjak2(gm);
		} else if (cmd.equalsIgnoreCase("카배작업")) {
			jakjak3(gm);
		} else if (cmd.equalsIgnoreCase("에르카배")) {
			에르카배(gm, param);
		} else if (cmd.equalsIgnoreCase("샌드아머")) {
			샌드아머(gm, param);
		} else if (cmd.equalsIgnoreCase("샌드데페")) {
			샌드데페(gm, param);
		} else if (cmd.equalsIgnoreCase("룬스톤데페")) {
			룬스톤데페(gm);
		} else if (cmd.equalsIgnoreCase("룬스톤카배")) {
			룬스톤카배(gm);
		} else if (cmd.equalsIgnoreCase("룬스톤디스")) {
			룬스톤디스(gm);
		} else if (cmd.equalsIgnoreCase("룬스톤아머")) {
			룬스톤아머(gm);
		} else if (cmd.equalsIgnoreCase("발라작업")) {
			dollvala(gm);
		} else if (cmd.equalsIgnoreCase("파푸작업")) {
			dollfafu(gm);
		} else if (cmd.equalsIgnoreCase("안타작업")) {
			dollanta(gm);
		} else if (cmd.equalsIgnoreCase("린드작업")) {
			dolllind(gm);
		} else if (cmd.equalsIgnoreCase("데몬작업")) {
			dolldemon(gm);
		} else if (cmd.equalsIgnoreCase("데스작업")) {
			dolldeath(gm);
		} else if (cmd.equalsIgnoreCase("커츠작업")) {
			dollkurts(gm);
		} else if (cmd.equalsIgnoreCase("바포작업")) {
			dollbapho(gm);
		} else if (cmd.equalsIgnoreCase("얼녀작업")) {
			dollice(gm);
		} else if (cmd.equalsIgnoreCase("타락작업")) {
			dolltarak(gm);
		} else if (cmd.equalsIgnoreCase("폰인증종료")) {
			폰인증종료(gm);
		} else if (cmd.equalsIgnoreCase("버경시작")) {
			FieldBossController3.getInstance().setFieldBossStart(true);
			 L1BugBearRace.getInstance();
			 L1BugBearRace.getInstance().loadingGame();
		} else if (cmd.equalsIgnoreCase("버경종료")) {
			FieldBossController3.getInstance().setFieldBossStart(false);
		} else if (cmd.equalsIgnoreCase("폰인증초기화")) {
			폰인증초기화(gm);
		} else if (cmd.equalsIgnoreCase("0")){
	        skillpacket(gm, param);
		} else if (cmd.equalsIgnoreCase("장사")){
	        gm.sendPackets(new S_PacketBox(S_PacketBox.ICON_COOKING, 187, 1800));
		} else if (cmd.equalsIgnoreCase("매입상점")) {
			buyshop(gm);
		/*} else if (cmd.equalsIgnoreCase("로또게임")){
		    LottoGameController.getInstance().isGmOpen = true;*/
		} else if (cmd.equalsIgnoreCase("보스")) {
			보스출동(gm, param);
		} else if (cmd.equalsIgnoreCase("계정검사")) {
		      AccountCheck(gm, param);
		} else if (cmd.equalsIgnoreCase("계정확인")) {
		      AccountCheck1(gm, param);
		} else if (cmd.equalsIgnoreCase("지하통로")){
			AzmodanSystem.getInstance().startAzmodan(191919, gm);
			L1SpawnUtil.spawn2(33430, 32806, (short) 4, 14913, 0, 3600000, 0);
			 L1World.getInstance().broadcastServerMessage("\\aG지하통로 던전이 개방되었습니다.");
		     L1World.getInstance().broadcastPacketToAll(new S_PacketBox(84, "지하통로 던전이 개방되었습니다. 기란마을 자이로스를 통해 이동하세요."));
        } else if (cmd.equalsIgnoreCase("공속체크")) {gm.AttackSpeedCheck2 = 1;
    	gm.sendPackets(new S_SystemMessage("\\fY허수아비를 10회공격해주세요."));            
        } else if (cmd.equalsIgnoreCase("이속체크")) {gm.MoveSpeedCheck = 1;
    	gm.sendPackets(new S_SystemMessage("\\fY한 방향으로 10회무빙해주세요."));
        } else if (cmd.equalsIgnoreCase("마법체크")) {gm.magicSpeedCheck = 1;
    	gm.sendPackets(new S_SystemMessage("\\fY원하는마법을 10회사용해주세요."));
		} else if (cmd.equalsIgnoreCase("오토루팅")) {
			autoloot(gm, param);
		} else if (cmd.equalsIgnoreCase("에르자베")){
			BossTimer.getInstance().isOpen2 = true;
		} else if (cmd.equalsIgnoreCase("샌드웜")){
			BossTimer.getInstance().isOpen = true;
		} else if (cmd.equalsIgnoreCase("장비검사")){
		      WeaponSearch(gm, param);
		} else if (cmd.equalsIgnoreCase("밸런스")){
		      CharacterBalance(gm, param);
		} else if (cmd.equalsIgnoreCase("유저매입상점")) {
			buyshop_user(gm);
		} else if (cmd.equalsIgnoreCase("전체정리")) {
			전체정리(gm);
		} else if (cmd.equalsIgnoreCase("맵정리")) {
			맵정리(gm);
		} else if (cmd.equalsIgnoreCase("인형청소")) {
			인형청소(gm);
		} else if (cmd.equalsIgnoreCase("메모리반환")) {
			메모리반환(gm);
		} else if (cmd.equalsIgnoreCase("버그")) {
			버그(gm, param);
		} else if (cmd.equalsIgnoreCase("산타")){
			GoldDragon(gm);
		} else if (cmd.equalsIgnoreCase("손오공")){
			GoldDragon2(gm);
		} else if (cmd.equalsIgnoreCase("멘트")) {
			멘트(gm, param);
		} else if (cmd.equalsIgnoreCase("메세지")) {
			메세지(gm, param);
		} else if (cmd.equalsIgnoreCase("버그2")) {
			버그2(gm, param);
		} else if (cmd.equalsIgnoreCase("판매완료")) {
			판매완료(gm, param);
		} else if (cmd.equalsIgnoreCase("던전초기화")) {
			던전초기화(gm, param);
		} else if (cmd.equalsIgnoreCase("전원초기화")) {
			전원초기화(gm, param);
		} else if (cmd.equalsIgnoreCase("방송리스트")) {
			BJList(gm, param);
		} else if (cmd.equalsIgnoreCase("서버종료")) {
			서버종료(gm, param);
		} else if (cmd.equalsIgnoreCase("html")) {
			html(gm, param);
		} else if (cmd.equalsIgnoreCase("축복")) {
			던전축복(gm, param);
		} else if (cmd.equalsIgnoreCase("기감초기화")) {
			기감(gm);
		} else if (cmd.equalsIgnoreCase("로그")) {
			standBy8(gm, param);
		} else if (cmd.equalsIgnoreCase("액숀")) {
			시바(gm, param);
		} else if (cmd.equalsIgnoreCase("오픈대기")) {//(by 마트무사) [0062] 서버 오픈대기 스탠바이 상태 켬/끔
			standBy(gm, param);
		} else if (cmd.equalsIgnoreCase("주말버프")) {
			주말버프(gm, param);
		} else if (cmd.equalsIgnoreCase("근하신년")) {
			근하신년(gm, param);
		} else if (cmd.equalsIgnoreCase("배세호상점")) {
			세호상점(gm, param);

			// }else if (cmd.equalsIgnoreCase("엔샵샵")) {
			// 엔샵(gm);
		} else if (cmd.equalsIgnoreCase("채팅로그")) {
			채팅(gm, param);
		} else if (cmd.equalsIgnoreCase("옵")) {
			옵코드(gm, param);

		} else if (cmd.equalsIgnoreCase("버경")) {
			버경(gm, param);
		} else if (cmd.equalsIgnoreCase("마법속도체크")) {
			마법속도(gm);
		} else if (cmd.equalsIgnoreCase("입장시간")) {
			checktime(gm);
		} else if (cmd.equalsIgnoreCase("무대테스트")) {
			텟트(gm);
		} else if (cmd.equalsIgnoreCase("패킷박스")) {
			packetbox(gm, param);
		} else if (cmd.equalsIgnoreCase("계정추가")) {
			addaccount(gm, param);
		} else if (cmd.equalsIgnoreCase("화면버프")) {
			SpecialEventHandler.getInstance().doScreenBuf(gm);
		} else if (cmd.equalsIgnoreCase("화면버프2")) {
			SpecialEventHandler.getInstance().doscreenbuftest(gm);
		} else if (cmd.equalsIgnoreCase("전체버프")) {
			SpecialEventHandler.getInstance().doAllBuf();
		} else if (cmd.equalsIgnoreCase("코마버프")) {
			SpecialEventHandler.getInstance().doAllComaBuf();

		} else if (cmd.equalsIgnoreCase("코마")) {
			SpecialEventHandler.getInstance().doAllComa();

		} else if (cmd.equalsIgnoreCase("화면코마")) {
			SpecialEventHandler.getInstance().doScreenComaBuf(gm);

		} else if (cmd.equalsIgnoreCase("무인상점")) {
			autoshop(gm, param);
		} else if (cmd.equalsIgnoreCase("비번변경")) {
			changepassword(gm, param);
		} else if (cmd.equalsIgnoreCase("아")) {
			CodeTest(gm, param);
		} else if (cmd.equalsIgnoreCase("정리")) {
			Clear(gm);
		} else if (cmd.equalsIgnoreCase("불")) {
			spawnmodel(gm, param);
		} else if (cmd.equalsIgnoreCase("서버저장")) {
			serversave(gm);// 요고 추가
		} else if (cmd.equalsIgnoreCase("가라")) {
			nocall(gm, param);
		} else if (cmd.equalsIgnoreCase("감옥")) {
			hellcall(gm, param);
		} else if (cmd.startsWith("압류해제")) {
			accountdel(gm, param);
		} else if (cmd.startsWith("압류")) {
			kick(gm, param);
		} else if (cmd.startsWith("벤")) {
			ban(gm, param);
		} else if (cmd.startsWith("버그")) {
			standBy7(gm, param);
		} else if (cmd.startsWith("케릭삭제")) {
			standBy77(gm, param);
		} else if (cmd.startsWith("이벤상점")) {
			standBy79(gm, param);
		} else if (cmd.startsWith("환수")) {
			환수(gm, param);
		} else if (cmd.startsWith("이벤트")) {
			이벤(gm, param);
		} else if (cmd.startsWith("컨피그로드")) {
			standBy80(gm);
		} else if (cmd.startsWith("분신")) {
			standBy81(gm, param);
		} else if (cmd.startsWith("화면분신")) {
			standBy82(gm, param);
		} else if (cmd.startsWith("샌드백정리")) {
			standBy82(gm);
		} else if (cmd.equalsIgnoreCase("방송사고")){
			bjbuffAllstop(gm);
		} else if (cmd.equalsIgnoreCase("채금풀기")) {
			chatx(gm, param);
		} else if (cmd.equalsIgnoreCase("전체선물")) {
			allpresent(gm, param);
		} else if (cmd.equalsIgnoreCase("배율설정")) { // / 추가
			SetRates(gm, param);
		} else if (cmd.equalsIgnoreCase("인챈설정")) {
			SetEnchantRates(gm, param);
		} else if (cmd.equalsIgnoreCase("배율조회")) {
			CheckAllRates(gm);
		} else if (cmd.equalsIgnoreCase("케릭검사")) { // #### 케릭검사
			chainfo(gm, param);
		} else if (cmd.equalsIgnoreCase("라우풀")) { // #### 케릭검사
			giveLawful(gm, param);
		} else if (cmd.equalsIgnoreCase("지원")){
			지원(gm, param);
		} else if (cmd.equalsIgnoreCase("판매체크")) {
			판매체크(gm);
		} else if (cmd.equals("공성시작")) {
			castleWarStart(gm, param);
		} else if (cmd.equals("공성종료")) {
			castleWarExit(gm, param);
		} else if (cmd.equals("고정인증")) {
			고정인증(gm, param);
		} else if (cmd.startsWith("겸치복구")) {
			returnEXP(gm, param);
		} else if (cmd.equalsIgnoreCase("영자채팅")) { // by판도라 영자채팅
			if (Config.isGmchat) {
				Config.isGmchat = false;
				gm.sendPackets(new S_SystemMessage("영자채팅 OFF"), true);
			} else {
				Config.isGmchat = true;
				gm.sendPackets(new S_SystemMessage("영자채팅 ON"), true);
			}
		} else if (cmd.equalsIgnoreCase("혈전시작")) {
			StartWar(gm, param);
		} else if (cmd.equalsIgnoreCase("혈전종료")) {
			StopWar(gm, param);
		} else if (cmd.equalsIgnoreCase("검색")) { // ########## 검색 추가 ##########
			searchDatabase(gm, param);
		} else if (cmd.equalsIgnoreCase("겜블")) {
			gamble(gm);
		} else if (cmd.equalsIgnoreCase("데드락")) {
			GeneralThreadPool.getInstance().execute(new DeadLockDetector(gm));
		} else if (cmd.equalsIgnoreCase("디풀")) {
			gm.sendPackets(new S_SystemMessage("DBCP Active Connection Count >> " + DB.active_count_check), true);
		} else if (cmd.equalsIgnoreCase("맵핵")) {
			maphack(gm, param);
		} else if (cmd.equalsIgnoreCase("마을")) {
			UserGiranTel(gm, param);
		} else if (cmd.equalsIgnoreCase("맵버프")) {
			mapBuff(gm);
		} else if (cmd.equalsIgnoreCase("포트변경")) {
			포트변경(gm, param);
		} else if (cmd.equalsIgnoreCase("재실행")) {
			if (!_lastCommands.containsKey(gm.getId())) {
				gm.sendPackets(new S_ServerMessage(74, "커맨드 " + cmd), true); // \f1%0은 사용할 수 없습니다.
				return;
			}
			redo(gm, param);
			return;
		} else if (cmd.equalsIgnoreCase("화면스폰")) {
			화면스폰(gm, param);
		} else if (cmd.equalsIgnoreCase("액션속도")) {
			SprSpeed(gm, param);
		} else if (cmd.equalsIgnoreCase("혈맹마크")) {
			clanMark(gm, param);
		} else if (cmd.equalsIgnoreCase("혈맹가입")) {
			강제가입(gm, param);
		} else if (cmd.equalsIgnoreCase("혈맹정보")) {
			search_Clan(gm, param);
		} else if (cmd.equalsIgnoreCase("로그기록")) {
			logSwitch(gm, param);
		} else if (cmd.equalsIgnoreCase("자동생성")) {
			autobot(gm, param);
		} else if (cmd.equalsIgnoreCase("하루상점리셋")) {
			L1MerchantInstance.resetOneDayBuy();
			gm.sendPackets(new S_SystemMessage("하루상점 리셋"), true);
		} else if (cmd.equalsIgnoreCase("길팅체크")) {
			길팅체크 = !길팅체크;
			gm.sendPackets(new S_SystemMessage("길팅체크 : " + 길팅체크), true);
		} else if (cmd.equalsIgnoreCase("루팅체크")) {
			루팅체크 = !루팅체크;
			gm.sendPackets(new S_SystemMessage("루팅체크 : " + 루팅체크), true);
		} else if (cmd.equalsIgnoreCase("맵누구")) {
			mapwho(gm, param);
		} else if (cmd.equalsIgnoreCase("케릭명변경")
				|| cmd.equalsIgnoreCase("캐릭명변경")) {
			charname(gm, param);
		} else if (cmd.equalsIgnoreCase("자동체크")) {
			autocheck2(gm, param);
		} else if (cmd.equalsIgnoreCase("스턴축복")) {
			스턴축복(gm, param);
		} else if (cmd.equalsIgnoreCase("스턴내성")) {
			스턴내성(gm, param);
		} else if (cmd.equalsIgnoreCase("상점검사")) {
			상점검사(gm);
		} else if (cmd.equalsIgnoreCase("알람")){
			이벤트알람(gm, param);
		} else if (cmd.equalsIgnoreCase("드다")) {
			드다(gm, param);
		} else if (cmd.equalsIgnoreCase("도감버프")) {
			도감버프(gm, param);
		} else if (cmd.equalsIgnoreCase("자동방지")) {
			autoCheck(gm, param);
		} else if (cmd.equalsIgnoreCase("자동아이피")) {
			autoIpcheck(gm, param);
		} else if (cmd.equalsIgnoreCase("봇")) {
			봇(gm, param);
		} else if (cmd.equalsIgnoreCase("엔진체크")) {
			엔진체크 = !엔진체크;
			gm.sendPackets(new S_SystemMessage("엔진체크 : " + 엔진체크), true);
		} else if (cmd.equalsIgnoreCase("무인상점구매체크")) {
			무인상점구매체크 = !무인상점구매체크;
			gm.sendPackets(new S_SystemMessage("무인상점구매체크 : " + 무인상점구매체크), true);
		} else if (cmd.equalsIgnoreCase("아이피체크")) {
			주시아이피체크 = !주시아이피체크;
			gm.sendPackets(new S_SystemMessage("아이피체크 : " + 주시아이피체크), true);
		} else if (cmd.equalsIgnoreCase("영자인증")) {
			케릭인증영자방 = !케릭인증영자방;
			gm.sendPackets(new S_SystemMessage("영자인증 : " + 케릭인증영자방), true);
		} else if (cmd.equalsIgnoreCase("트리플스핵")) {
			트리플포우스핵 = !트리플포우스핵;
			gm.sendPackets(new S_SystemMessage("트리플스핵 : " + 트리플포우스핵), true);
		} else if (cmd.equalsIgnoreCase("자동생성방지")) {
			자동생성방지 = !자동생성방지;
			gm.sendPackets(new S_SystemMessage("자동생성방지 : " + 자동생성방지), true);
		} else if (cmd.equalsIgnoreCase("용해로그")) {
			용해로그 = !용해로그;
			gm.sendPackets(new S_SystemMessage("용해로그 : " + 용해로그), true);
		} else if (cmd.equalsIgnoreCase("인증")) {
			ipphone_certification(gm, param);
		} else if (cmd.equalsIgnoreCase("대화창")) {
			try {
				StringTokenizer st = new StringTokenizer(param);
				String s = st.nextToken();
				gm.sendPackets(new S_NPCTalkReturn(gm.getId(), s), true);
			} catch (Exception e) {
			}
		} else if (cmd.equalsIgnoreCase("접속이름체크")) {
			접속이름체크 = !접속이름체크;
			gm.sendPackets(new S_SystemMessage("접속이름체크 : " + 접속이름체크), true);
		} else if (cmd.equalsIgnoreCase("접속이름체크리셋")) {
			connectCharNameReset();
		} else if (cmd.equalsIgnoreCase("아덴교환체크")) {
			아덴교환체크 = !아덴교환체크;
			gm.sendPackets(new S_SystemMessage("아덴교환체크 : " + 아덴교환체크), true);
		} else if (cmd.equalsIgnoreCase("인벤삭제")) {
			try {
				List<L1ItemInstance> list = gm.getInventory().getItems();
				L1ItemInstance[] ll = list.toArray(new L1ItemInstance[list
						.size()]);
				for (L1ItemInstance item : ll) {
					gm.getInventory().removeItem(item);
				}
			} catch (Exception e) {
			}
		} else if (cmd.equalsIgnoreCase("혈문장")) {
			혈문장(gm, param);

		} else if (cmd.equalsIgnoreCase("드래곤상점")) {
			if (NpcShopSystem._dragon_power) {
				gm.sendPackets(new S_SystemMessage("이미 실행중입니다."), true);
				return;
			}
		//	NpcShopSystem.getInstance().npcDragonShopStart();
			//} else if (cmd.equalsIgnoreCase("이체크")){
			 이미지체크(gm);
		} else if (cmd.equalsIgnoreCase("베이")) {
			베이(gm, param);
		} else if (cmd.equalsIgnoreCase("육성")) {
			육성(gm, param);
		} else if (cmd.equalsIgnoreCase("바둑이")) {
			바둑이(gm, param);
		} else if (cmd.equalsIgnoreCase("탐주기")) {
			탐주기(gm, param);
		} else if (cmd.equalsIgnoreCase("사냥아덴로그")) {
			사냥아덴(gm, param);
		} else if (cmd.equalsIgnoreCase("상점아덴로그")) {
			상점아덴(gm, param);
		} else if (cmd.equalsIgnoreCase("화면로그")) {
			if (fifi == null) {
				fifi = GeneralThreadPool.getInstance().schedule(new sdf(gm),
						1000);
				gm.sendPackets(new S_SystemMessage("----- 화면 로그 시작 -----"),
						true);
			}
		} else if (cmd.equalsIgnoreCase("화면로그종료")) {
			if (fifi != null) {
				fifi.cancel(true);
				fifi = null;
				for (L1PcInstance pc : L1World.getInstance().getAllPlayersToArray()) {
					if (pc.getNetConnection() == null)
						continue;
					if (pc.getNetConnection().패킷로그 || GameServer.getInstance().checkip(pc.getNetConnection().getIp())) {
						pc.getNetConnection().행동로그저장(true);
						pc.getNetConnection().패킷로그 = false;
						GameServer.getInstance().removeip(
								pc.getNetConnection().getIp());
					}
				}
				gm.sendPackets(new S_SystemMessage("----- 화면 로그 종료 -----"),
						true);
			}
		} else if (cmd.equalsIgnoreCase("광역압류")) {
			광역압류(gm, param);
		} else if (cmd.equalsIgnoreCase("드")) {
			드랍추가(gm, param);
		} else if (cmd.equalsIgnoreCase("노")) {
			노트(gm, param);
		} else if (cmd.equals("테")) {
			toMapMove(gm, param);
		}
		else {
			gm.sendPackets(
					new S_SystemMessage("커멘드 " + cmd + " 는 존재하지 않습니다. "), true);
		}
		token = null;
	}
	
	private void toMapMove(L1PcInstance pc, String s) {
		try {
			StringTokenizer st = new StringTokenizer(s);
			int map = Integer.parseInt(st.nextToken());
 
			int startX = 0, startY = 0, endX = 0, endY = 0;
			int locX = 0, locY = 0;
			
			startX = MapsTable.getInstance().getStartX(map);
			startY = MapsTable.getInstance().getStartY(map);
			endX = MapsTable.getInstance().getEndX(map);
			endY = MapsTable.getInstance().getEndY(map);

			if (startX != 0 && startY != 0 && endX != 0 && endY != 0) {
				locX = CommonUtil.random(startX, startY);
				locY = CommonUtil.random(endX, endY);
				L1Teleport.teleport(pc, locX, locY, (short) map, 0);
			}
			pc.sendPackets(new S_SystemMessage("테: '" + locX + "' / '" + locY + "' / '" + map + "'"));
		} catch (Exception e) {
			pc.sendPackets(new S_SystemMessage("테: '맵' 번호로 입력 바랍니다."));
		}

	}
	
	 private void 주말버프(L1PcInstance gm, String param){
		  try{
		  StringTokenizer st = new StringTokenizer(param);
		  String status = st.nextToken();
		  if (status.equalsIgnoreCase("켬")) {
			  
		   Config.주말버프 = true;
		   for (L1PcInstance pc : L1World.getInstance().getAllPlayers()) {
			pc.sendPackets(new S_ACTION_UI(S_ACTION_UI.신스킬, 6070), true);
			pc.sendPackets(new S_ACTION_UI(6070, 172800, 8258, 5026), true);
			pc.sendPackets(new S_ServerMessage(5026), true);
		   }
		   gm.sendPackets(new S_SystemMessage("주말버프 On."));
		   
		   
		  } else if(status.equalsIgnoreCase("끔")) {
			  
		   Config.주말버프 = false;
		   for (L1PcInstance pc : L1World.getInstance().getAllPlayers()) {
			pc.sendPackets(new S_ACTION_UI(S_ACTION_UI.신스킬, 6070), true);
			pc.sendPackets(new S_ACTION_UI(6070, 0, 8258, 5026), true);
		   }
		   gm.sendPackets(new S_SystemMessage("주말버프 Off"));
		   
		  }
		  }catch (Exception eee){
		   gm.sendPackets(new S_SystemMessage(".주말버프 [켬/끔] 으로 입력하세요."));
		  }
	}
	 
	 private void 이벤트알람(L1PcInstance gm, String param){
		 try{
		 StringTokenizer st = new StringTokenizer(param);
		 int id = Integer.parseInt(st.nextToken());
		 int id2 = Integer.parseInt(st.nextToken());
		 boolean flag = false; 
		 if(id2 == 1)
			 flag = true;
		 if(id2 == 2)
			 flag = false;
		 gm.sendPackets(new S_MatizAlarm(id, 1, 3600, flag));
		 }catch (Exception eee){
			   gm.sendPackets(new S_SystemMessage(".알람 [id] [1켬 2끔] (1 에르자베, 2 샌드웜, 3 켄트성공성)"));
		 }
	 	}
		 
	 private void 근하신년(L1PcInstance gm, String param){
		  try{
		  StringTokenizer st = new StringTokenizer(param);
		  String status = st.nextToken();
		  if (status.equalsIgnoreCase("켬")) {
			  
		   Config.근하신년 = true;
		   for (L1PcInstance pc : L1World.getInstance().getAllPlayers()) {
				pc.sendPackets(new S_ACTION_UI(S_ACTION_UI.신스킬, 16402), true);
				pc.sendPackets(new S_ACTION_UI(16402, 172800, 9007, 5384), true);
		   }
		   gm.sendPackets(new S_SystemMessage("근하신년 On."));
		   
		   
		  } else if(status.equalsIgnoreCase("끔")) {
			  
		   Config.근하신년 = false;
		   for (L1PcInstance pc : L1World.getInstance().getAllPlayers()) {
				pc.sendPackets(new S_ACTION_UI(S_ACTION_UI.신스킬, 16402), true);
				pc.sendPackets(new S_ACTION_UI(16402, 0, 9007, 5384), true);
		   }
		   gm.sendPackets(new S_SystemMessage("근하신년 Off"));
		   
		  }
		  }catch (Exception eee){
		   gm.sendPackets(new S_SystemMessage(".근하신년 [켬/끔] 으로 입력하세요."));
		  }
	}
	 
	
	 private void standBy(L1PcInstance gm, String param){
		  try{
		  StringTokenizer st = new StringTokenizer(param);
		  String status = st.nextToken();
		  if (status.equalsIgnoreCase("켬")) {
		   Config.STANDBY_SERVER = true;
		   gm.sendPackets(new S_SystemMessage("오픈대기 상태로 돌입합니다. 일부 패킷이 차단 됩니다."));
		  } else if(status.equalsIgnoreCase("끔")) {
		   Config.STANDBY_SERVER = false;
		   gm.sendPackets(new S_SystemMessage("오픈대기 상태가 해지되고 정상적인 플레이가 가능합니다."));
		  }
		  }catch (Exception eee){
		   gm.sendPackets(new S_SystemMessage(".오픈대기 [켬/끔] 으로 입력하세요."));
		   gm.sendPackets(new S_SystemMessage("켬 - 오픈대기 상태로 전환 | 끔 - 일반모드로 게임시작"));
		  }
	}
	 
	private void 지원(L1PcInstance gm, String param){
		try{
			
			StringTokenizer st = new StringTokenizer(param);
			String name = st.nextToken();
			
			L1PcInstance pc = L1World.getInstance().getPlayer(name);
			
			Support.getInstance().GiveItem2(pc);
			
			pc.sendPackets(new S_SystemMessage(pc, "운영자의 작은 배려입니다. 최선을 다해주시기 바랍니다."));
			gm.sendPackets(new S_SystemMessage(gm, ""+name+"님에게 지원품 지급 완료"));
		} catch (Exception eee){
			gm.sendPackets(new S_SystemMessage(".지원 [캐릭명]"));
		}
	}
	 
	private void 노트(L1PcInstance gm, String param) {
		StringTokenizer st = new StringTokenizer(param);
		String _노트 = st.nextToken();
		gm._note = _노트;
		gm.sendPackets(new S_SystemMessage("노트값등록 :" + _노트));
	}
	
	private void 강제가입(L1PcInstance gm, String param){
		  try{
		StringTokenizer st = new StringTokenizer(param);
		String clanPname = st.nextToken();
		String targetname = st.nextToken();
		
		L1PcInstance pc = L1World.getInstance().getPlayer(clanPname);
		
		String clanName = pc.getClanname();
		
		L1Clan clan = L1World.getInstance().getClan(clanName);
		
		L1PcInstance targetPc = L1World.getInstance().getPlayer(targetname);

		if (clan != null) {
			혈맹가입(pc, targetPc, clan);
		}
	}catch (Exception eee){
		   gm.sendPackets(new S_SystemMessage(".혈맹가입 [혈맹원캐릭명] [가입자캐릭명]"));
		  }
	}
	
	public static void 혈맹가입(L1PcInstance pc, L1PcInstance joinPc, L1Clan clan) {
		 try{
			 if (pc == null)
			return;
			 int maxMember = Config.CLAN_MEMBER_SIZE;
			 if (joinPc.getClanid() == 0) { // 크란미가입

			if (maxMember <= clan.getClanMemberList().size()) { // 빈 곳이 없다
				if (pc != null)
					joinPc.sendPackets(new S_ServerMessage(188, pc.getName())); // %0는 당신을 혈맹원으로서 받아들일 수가 없습니다.
					joinPc.sendPackets(new S_SystemMessage("[" + clan.getClanName() + "] 혈맹은\n현재 가입할 수 없습니다."), true);
				return;
			}
			for (L1PcInstance clanMembers : clan.getOnlineClanMember()) {
				clanMembers.sendPackets(new S_ServerMessage(94, joinPc.getName())); // \f1%0이 혈맹의 일원으로서 받아들여졌습니다.
			}
			joinPc.setClanid(clan.getClanId());
			joinPc.setClanname(clan.getClanName());
			joinPc.setClanRank(L1Clan.CLAN_RANK_PROBATION);
			joinPc.setTitle("");
			joinPc.sendPackets(new S_CharTitle(joinPc.getId(), ""));
			Broadcaster.broadcastPacket(joinPc, new S_CharTitle(joinPc.getId(),
					""));
			joinPc.setClanJoinDate(new Timestamp(System.currentTimeMillis()));
			try {
				joinPc.save();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} // DB에 캐릭터 정보를 기입한다
			clan.addClanMember(joinPc.getName(), joinPc.getClanRank(),
					joinPc.getLevel(), joinPc.getType(), joinPc.getMemo(),
					joinPc.getOnlineStatus(), joinPc);

			joinPc.sendPackets(new S_PacketBox(S_PacketBox.MSG_RANK_CHANGED,
					0x07, joinPc.getName()), true);

			joinPc.sendPackets(new S_PacketBox(S_PacketBox.WORLDMAP_UNKNOWN1),
					true);

			joinPc.sendPackets(new S_ClanJoinLeaveStatus(joinPc), true);
			Broadcaster.broadcastPacket(joinPc, new S_ClanJoinLeaveStatus(
					joinPc));

			joinPc.sendPackets(new S_ReturnedStat(joinPc, S_ReturnedStat.CLAN_JOIN_LEAVE), true);
			Broadcaster.broadcastPacket(joinPc, new S_ReturnedStat(joinPc, S_ReturnedStat.CLAN_JOIN_LEAVE));
			joinPc.sendPackets(new S_ClanWindow(S_ClanWindow.혈마크띄우기, joinPc.getClan().getmarkon()), true);
			joinPc.sendPackets(new S_문장주시(joinPc.getClan(), 2), true);

			ClanTable.getInstance().updateClan(joinPc.getClan());
			if (pc != null) {
				pc.sendPackets(new S_PacketBox(pc,
						S_PacketBox.PLEDGE_REFRESH_PLUS));
				joinPc.sendPackets(new S_ServerMessage(95, clan.getClanName())); // \f1%0 혈맹에 가입했습니다.
			}
		} else { // 크란 가입이 끝난 상태(크란 연합)
			if (Config.CLAN_ALLIANCE) {
				if (pc != null)
					changeClan(pc.getNetConnection(), pc, joinPc, maxMember);
			} else {
				joinPc.sendPackets(new S_SystemMessage("이미 혈맹에 가입했습니다.")); // \f1당신은 벌써 혈맹에 가입하고 있습니다.
			}
		}
		 } catch (Exception e) {
				_log.log(Level.SEVERE, e.getLocalizedMessage(), e);
			}
	}
	private static void changeClan(LineageClient clientthread, L1PcInstance pc, L1PcInstance joinPc, int maxMember) {
		int clanId = pc.getClanid();
		String clanName = pc.getClanname();
		L1Clan clan = L1World.getInstance().getClan(clanName);
		int clanNum = clan.getClanMemberList().size();

		int oldClanId = joinPc.getClanid();
		String oldClanName = joinPc.getClanname();
		L1Clan oldClan = L1World.getInstance().getClan(oldClanName);
		int oldClanNum = oldClan.getClanMemberList().size();
		if (clan != null && oldClan != null && joinPc.isCrown()
				&& joinPc.getId() == oldClan.getLeaderId()) {
			if (maxMember < clanNum + oldClanNum) { // 빈 곳이 없다
				joinPc.sendPackets(new S_ServerMessage(188, pc.getName())); // %0는
																			// 당신을
																			// 혈맹원으로서
																			// 받아들일
																			// 수가
																			// 없습니다.
				return;
			}
			L1PcInstance clanMember[] = clan.getOnlineClanMember();
			for (int cnt = 0; cnt < clanMember.length; cnt++) {
				clanMember[cnt].sendPackets(new S_ServerMessage(94, joinPc
						.getName())); // \f1%0이 혈맹의 일원으로서 받아들여졌습니다.
			}

			for (int i = 0; i < oldClan.getClanMemberList().size(); i++) {
				L1PcInstance oldClanMember = L1World.getInstance().getPlayer(
						oldClan.getClanMemberList().get(i).name);
				if (oldClanMember != null) { // 온라인중의 구크란 멤버
					oldClanMember.setClanid(clanId);
					oldClanMember.setClanname(clanName);
					// 혈맹 연합에 가입한 군주는 가디안
					// 군주가 데려 온 혈맹원은 본받아
					if (oldClanMember.getId() == joinPc.getId()) {
						// oldClanMember.setClanRank(L1Clan.CLAN_RANK_GUARDIAN);
						oldClanMember.setClanRank(L1Clan.CLAN_RANK_SUBPRINCE);
					} else {
						oldClanMember.setClanRank(L1Clan.CLAN_RANK_PROBATION);
					}
					try {
						// DB에 캐릭터 정보를 기입한다
						oldClanMember.save();
					} catch (Exception e) {
						_log.log(Level.SEVERE, e.getLocalizedMessage(), e);
					}
					clan.addClanMember(oldClanMember.getName(),
							oldClanMember.getClanRank(),
							oldClanMember.getLevel(), oldClanMember.getType(),
							oldClanMember.getMemo(),
							oldClanMember.getOnlineStatus(), oldClanMember);
					oldClanMember
							.sendPackets(new S_ServerMessage(95, clanName)); // \f1%0
																				// 혈맹에
																				// 가입했습니다.
				} else { // 오프 라인중의 구크란 멤버
					try {
						L1PcInstance offClanMember = CharacterTable
								.getInstance()
								.restoreCharacter(
										oldClan.getClanMemberList().get(i).name);
						offClanMember.setClanid(clanId);
						offClanMember.setClanname(clanName);
						offClanMember.setClanRank(L1Clan.CLAN_RANK_PROBATION);
						offClanMember.save();
						clan.addClanMember(offClanMember.getName(),
								offClanMember.getClanRank(),
								offClanMember.getLevel(),
								offClanMember.getType(),
								offClanMember.getMemo(),
								offClanMember.getOnlineStatus(), offClanMember);
					} catch (Exception e) {
						_log.log(Level.SEVERE, e.getLocalizedMessage(), e);
					}
				}
			}
			// 이전혈맹 삭제
			String emblem_file = String.valueOf(oldClanId);
			File file = new File("emblem/" + emblem_file);
			file.delete();
			ClanTable.getInstance().deleteClan(oldClanName);
		}
	}
	private static ClanLeave_joinNameCK leaveNameCK = null;

	class ClanLeave_joinNameCK implements Runnable {
		private final FastMap<String, Long> list;

		public ClanLeave_joinNameCK() {
			list = new FastMap<String, Long>();
		}

		@Override
		public void run() {
			while (true) {
				try {
					if (list.size() > 0) {
						FastTable<String> sl = new FastTable<String>();
						for (FastMap.Entry<String, Long> e = list.head(), mapEnd = list
								.tail(); (e = e.getNext()) != mapEnd;) {
							if (e.getValue() < System.currentTimeMillis()) {
								sl.add(e.getKey());
							}
						}
						for (String name : sl) {
							list.remove(name);
						}
						Thread.sleep(1000);
					} else {
						Thread.sleep(5000);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		public void add(String name, long time) {
			list.put(name, time);
		}

		public long ck(String name) {
			long d = 0;
			try {
				d = list.get(name);
			} catch (Exception e) {
			}
			return d;
		}
	}
	
	
	public void killment(L1PcInstance pc, String param) { // by사부 멘트
		if (param.equalsIgnoreCase("끔")) {
			pc.킬멘트 = false;
			pc.sendPackets(new S_SystemMessage("킬멘트 를 표시하지 않습니다."));
		} else if (param.equalsIgnoreCase("켬")) {
			pc.킬멘트 = true;
			pc.sendPackets(new S_SystemMessage("킬멘트 를 표시 합니다."));

		} else {
			pc.sendPackets(new S_SystemMessage(".킬멘트 [켬/끔] 으로 입력해 주세요. "));
		}
	}
	private void search_Clan(L1PcInstance gm, String param) {
		try {
			StringTokenizer tok = new StringTokenizer(param);
			String name = tok.nextToken();
			search_Clan2(gm, name);
		} catch (Exception e) {
			gm.sendPackets(new S_SystemMessage(".혈맹정보 [혈맹이름]"));
		}
	}	
	
	private void search_Clan2(L1PcInstance gm, String param) {
			Connection c = null;
			String s_account = null;
			String s_name = param;
			String s_level = null;
			String s_clan = param;
			String s_hp = null;
			String s_mp = null;
			String s_kill = null;
			String s_death = null;
			int online = 0;
			PreparedStatement statement0 = null;
			PreparedStatement statement1 = null;
			ResultSet rs0 = null;
			ResultSet rs1 = null;
			int count = 0;
			int count0 = 0;
			try {
				if (param == null) {
					gm.sendPackets(new S_SystemMessage(".혈맹정보 [혈맹이름]"));
					return;
				}
				c = L1DatabaseFactory.getInstance().getConnection();
				statement0 = c.prepareStatement("select account_name, clanname from characters where char_name = '" + s_name + "'");
				rs0 = statement0.executeQuery();

				while (rs0.next()) {
					s_account = rs0.getString(1);
					s_clan = rs0.getString(2);
					if (s_clan.length() == 0) {
						gm.sendPackets(new S_SystemMessage("혈맹에 가입하지 않은 캐릭입니다."));
						return;
					}
					gm.sendPackets(new S_SystemMessage("===================( 혈맹 정보 )===================="));
					gm.sendPackets(new S_SystemMessage("\\fY(캐릭명:" + param + ')' + "(계정:" + s_account + ')' + "(클랜명:" + s_clan + ')'));
					gm.sendPackets(new S_SystemMessage("===================================================="));
				}
				statement1 = c.prepareStatement("select " + "char_name," + "level," + "clanname," + "OnlineStatus," + "MaxHp," + "MaxMp," + "PC_Kill," + "PC_Death " + "from characters where Clanname = '" + s_clan + "' ORDER BY 'level' DESC, 'Exp' LIMIT 150");
				rs1 = statement1.executeQuery();

				while (rs1.next()) {
					s_name = rs1.getString(1);
					s_level = rs1.getString(2);
					s_clan = rs1.getString(3);
					online = rs1.getInt(4);
					s_hp = rs1.getString(5);
					s_mp = rs1.getString(6);
					s_kill = rs1.getString(7);
					s_death = rs1.getString(8);

					if (online == 1) {
						gm.sendPackets(new S_SystemMessage("\\fU" + s_name + " Lv:" + s_level + " Kill:" + s_kill + " Death:" + s_death + " Hp:" + s_hp + " Mp:" + s_mp));
						count++;
					}
					count0++;
				}
				gm.sendPackets(new S_SystemMessage("\\fY(클랜명: " + s_clan + ") (총 인원: " + count0 + "명) (접속 인원: " + count + "명)"));
			} catch (Exception e) {
				gm.sendPackets(new S_SystemMessage(".혈맹정보 [혈맹이름]"));
			} finally {
				SQLUtil.close(rs1);
				SQLUtil.close(statement1);
				SQLUtil.close(rs0);
				SQLUtil.close(statement0);
				SQLUtil.close(c);
			}
		}
	
	private void AccountCheck(L1PcInstance pc, String param) {
		try {
			StringTokenizer st = new StringTokenizer(param);
			String name = st.nextToken();
			CharacterTable.getInstance().CharacterAccountCheck(pc, name);
		} catch (Exception e) {
			pc.sendPackets(new S_SystemMessage(".계정검사 [캐릭터명]"));
		}
	}
	
	private void AccountCheck1(L1PcInstance pc, String param) {
		try {
			StringTokenizer st = new StringTokenizer(param);
			String name = st.nextToken();
			CharacterTable.getInstance().CharacterAccountCheck1(pc, name);
		} catch (Exception e) {
			pc.sendPackets(new S_SystemMessage(".계정확인 [캐릭터명]"));
		}
	}
	
	private void charname(L1PcInstance pc, String cmd) {
		try {
			StringTokenizer tok = new StringTokenizer(cmd);
			String chaName = tok.nextToken();
			if (pc.getClanid() > 0) {
				pc.sendPackets(new S_SystemMessage("\\fU혈맹탈퇴후 캐릭명을 변경할수 있습니다."));
				return;
			}
			if (!pc.getInventory().checkItem(467009, 1)) { // 있나 체크
				pc.sendPackets(new S_SystemMessage("\\fU케릭명 변경 비법서를 소지하셔야 가능합니다."));
				return;
			}
			for (int i = 0; i < chaName.length(); i++) {
				if (chaName.charAt(i) == 'ㄱ'
						|| chaName.charAt(i) == 'ㄲ'
						|| chaName.charAt(i) == 'ㄴ'
						|| chaName.charAt(i) == 'ㄷ'
						|| // 한문자(char)단위로 비교.
						chaName.charAt(i) == 'ㄸ'
						|| chaName.charAt(i) == 'ㄹ'
						|| chaName.charAt(i) == 'ㅁ'
						|| chaName.charAt(i) == 'ㅂ'
						|| // 한문자(char)단위로 비교
						chaName.charAt(i) == 'ㅃ'
						|| chaName.charAt(i) == 'ㅅ'
						|| chaName.charAt(i) == 'ㅆ'
						|| chaName.charAt(i) == 'ㅇ'
						|| // 한문자(char)단위로 비교
						chaName.charAt(i) == 'ㅈ'
						|| chaName.charAt(i) == 'ㅉ'
						|| chaName.charAt(i) == 'ㅊ'
						|| chaName.charAt(i) == 'ㅋ'
						|| // 한문자(char)단위로 비교.
						chaName.charAt(i) == 'ㅌ'
						|| chaName.charAt(i) == 'ㅍ'
						|| chaName.charAt(i) == 'ㅎ'
						|| chaName.charAt(i) == 'ㅛ'
						|| // 한문자(char)단위로 비교.
						chaName.charAt(i) == 'ㅕ'
						|| chaName.charAt(i) == 'ㅑ'
						|| chaName.charAt(i) == 'ㅐ'
						|| chaName.charAt(i) == 'ㅔ'
						|| // 한문자(char)단위로 비교.
						chaName.charAt(i) == 'ㅗ'
						|| chaName.charAt(i) == 'ㅓ'
						|| chaName.charAt(i) == 'ㅏ'
						|| chaName.charAt(i) == 'ㅣ'
						|| // 한문자(char)단위로 비교.
						chaName.charAt(i) == 'ㅠ'
						|| chaName.charAt(i) == 'ㅜ'
						|| chaName.charAt(i) == 'ㅡ'
						|| chaName.charAt(i) == 'ㅒ'
						|| // 한문자(char)단위로 비교.
						chaName.charAt(i) == 'ㅖ'
						|| chaName.charAt(i) == 'ㅢ'
						|| chaName.charAt(i) == 'ㅟ'
						|| chaName.charAt(i) == 'ㅝ'
						|| // 한문자(char)단위로 비교.
						chaName.charAt(i) == 'ㅞ' || chaName.charAt(i) == 'ㅙ'
						|| chaName.charAt(i) == 'ㅚ'
						|| chaName.charAt(i) == 'ㅘ'
						|| // 한문자(char)단위로 비교.
						chaName.charAt(i) == '씹' || chaName.charAt(i) == '좃'
						|| chaName.charAt(i) == '좆' || chaName.charAt(i) == 'ㅤ') {
					pc.sendPackets(new S_SystemMessage("사용할수없는 케릭명입니다."));
					return;
				}
			}
			if (chaName.getBytes().length > 12) {
				pc.sendPackets(new S_SystemMessage("이름이 너무 깁니다."));
				return;
			}
			if (chaName.length() == 0) {
				pc.sendPackets(new S_SystemMessage("변경할 케릭명을 입력하세요."));
				return;
			}
			if (BadNamesList.getInstance().isBadName(chaName)) {
				pc.sendPackets(new S_SystemMessage("사용할 수 없는 케릭명입니다."));
				return;
			}
			if (isInvalidName(chaName)) {
				pc.sendPackets(new S_SystemMessage("사용할 수 없는 케릭명입니다."));
				return;
			}
			if (CharacterTable.doesCharNameExist(chaName)) {
				pc.sendPackets(new S_SystemMessage("동일한 케릭명이 존재합니다."));
				return;
			}

			if (CharacterTable.RobotNameExist(chaName)) {
				pc.sendPackets(new S_SystemMessage("동일한 케릭명이 존재합니다."));
				return;
			}
			if (CharacterTable.RobotCrownNameExist(chaName)) {
				pc.sendPackets(new S_SystemMessage("동일한 케릭명이 존재합니다."));
				return;
			}
			if (NpcShopSpawnTable.getInstance().getNpc(chaName)
					|| npcshopNameCk(chaName)) {
				pc.sendPackets(new S_SystemMessage("동일한 케릭명이 존재합니다."));
				return;
			}
			if (CharacterTable.somakname(chaName)) {
				pc.sendPackets(new S_SystemMessage("동일한 케릭명이 존재합니다."));
				return;
			}
			String oldname = pc.getName();

			chaname(chaName, oldname);

			long sysTime = System.currentTimeMillis();
			logchangename(chaName, oldname, new Timestamp(sysTime));

			pc.sendPackets(new S_SystemMessage(chaName + " 아이디로 변경 하셨습니다."));
			pc.sendPackets(new S_SystemMessage(
					"\\fU계정 입력창으로 이동후 다시 접속하시면 적용됩니다."));
			pc.getInventory().consumeItem(467009, 1); // 소모
		} catch (Exception e) {
			pc.sendPackets(new S_SystemMessage(
					".케릭명변경 [바꾸실아이디] []는 제외하고 입력해주세요."));
		}
	}

	private void logchangename(String chaName, String oldname,
			Timestamp datetime) {
		Connection con = null;
		PreparedStatement pstm = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			String sqlstr = "INSERT INTO Log_Change_name SET Old_Name=?,New_Name=?, Time=?";
			pstm = con.prepareStatement(sqlstr);
			pstm.setString(1, oldname);
			pstm.setString(2, chaName);
			pstm.setTimestamp(3, datetime);
			pstm.executeUpdate();
		} catch (SQLException e) {
		} finally {
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
	}

	private boolean npcshopNameCk(String name) {
		return NpcTable.getInstance().findNpcShopName(name);
	}

	/** 변경 가능한지 검사한다 시작 **/
	private static boolean isAlphaNumeric(String s) {
		boolean flag = true;
		char ac[] = s.toCharArray();
		int i = 0;
		do {
			if (i >= ac.length) {
				break;
			}
			if (!Character.isLetterOrDigit(ac[i])) {
				flag = false;
				break;
			}
			i++;
		} while (true);
		return flag;
	}

	private static boolean isInvalidName(String name) {
		int numOfNameBytes = 0;
		try {
			numOfNameBytes = name.getBytes("EUC-KR").length;
		} catch (UnsupportedEncodingException e) {
			// _log.log(Level.SEVERE, e.getLocalizedMessage(), e);
			return false;
		}

		if (isAlphaNumeric(name)) {
			return false;
		}
		if (5 < (numOfNameBytes - name.length()) || 12 < numOfNameBytes) {
			return false;
		}

		if (BadNamesList.getInstance().isBadName(name)) {
			return false;
		}
		return true;
	}

	private void chaname(String chaName, String oldname) {
		Connection con = null;
		PreparedStatement pstm = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con
					.prepareStatement("UPDATE characters SET char_name=? WHERE char_name=?");
			pstm.setString(1, chaName);
			pstm.setString(2, oldname);
			pstm.executeUpdate();
		} catch (Exception e) {

		} finally {
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
	}

	private void 드랍추가(L1PcInstance gm, String param) {
		StringTokenizer st = new StringTokenizer(param);
		int itemid = Integer.parseInt(st.nextToken(), 10);
		// int max = Integer.parseInt(st.nextToken(), 10);
		L1Item temp = ItemTable.getInstance().getTemplate(itemid);
		if (temp == null) {
			gm.sendPackets(new S_SystemMessage("아이템을 찾을수 없습니다."));
			return;
		}
		L1Object npc = NpcTable.getInstance().getTemplate(gm._npcnum);
		if (npc == null) {
			gm.sendPackets(new S_SystemMessage("엔피씨를 찾을수 없습니다."));
			return;
		}
		@SuppressWarnings("static-access")
		boolean ck = DropTable.getInstance().SabuDrop(gm._npcnum, itemid, 1,
				1000000, gm._npcname, temp.getName(), gm._note);
		if (ck) {
			gm.sendPackets(new S_SystemMessage("드랍추가 : npc-" + gm._npcnum
					+ " / item(" + itemid + ") / max " + 1 + "개"));
		} else {
			gm.sendPackets(new S_SystemMessage("드랍추가 : 이미 등록된 드랍 리스트"));
		}
	}

	private void 광역압류(L1PcInstance gm, String param) {
		// TODO 자동 생성된 메소드 스텁
		try {
			StringTokenizer st = new StringTokenizer(param);
			String 이름 = st.nextToken();
			L1PcInstance pc = L1World.getInstance().getPlayer(이름);
			if (pc == null) {
				gm.sendPackets(new S_SystemMessage(이름 + " 이란 케릭터는 미 접속 중입니다."),
						true);
				return;
			}
			if (pc.getNetConnection() == null || pc.getNetConnection().getIp() == null) {
				gm.sendPackets(new S_SystemMessage(이름 + " 이란 케릭터는 무인상점이거나 로봇 케릭터입니다."), true);
				return;
			}
			String ip = pc.getNetConnection().getIp();
			ip = ip.substring(0, ip.lastIndexOf(".") + 1);

			for (L1PcInstance tempPc : L1World.getInstance().getAllPlayers()) {
				if (tempPc == null || tempPc.getNetConnection() == null || tempPc.getNetConnection().getIp() == null)
					continue;
				if (tempPc.getNetConnection().getIp().indexOf(ip) >= 0) {
					tempPc.getNetConnection().kick();
					tempPc.getNetConnection().close();
				}
			}

			Connection con = null;
			PreparedStatement pstm = null;
			try {
				con = L1DatabaseFactory.getInstance().getConnection();
				for (int i = 1; i <= 255; i++) {
					IpTable.getInstance().banIp(ip + i); // BAN 리스트에 IP를 추가한다.
					String sqlstr = "UPDATE accounts SET banned=1 WHERE ip=?";
					pstm = con.prepareStatement(sqlstr);
					pstm.setString(1, ip + i);
					pstm.executeUpdate();
					SQLUtil.close(pstm);
				}
			} catch (SQLException e) {
				_log.log(Level.SEVERE, e.getLocalizedMessage(), e);
			} finally {
				SQLUtil.close(pstm);
				SQLUtil.close(con);
			}
			gm.sendPackets(new S_SystemMessage(이름 + " 케릭터 관련 [" + ip
					+ " 대역] IP 차단과 해당 계정을 압류시킵니다."), true);

		} catch (Exception e) {
		}
	}

	private void 사냥아덴(L1PcInstance gm, String param) {
		// TODO 자동 생성된 메소드 스텁
		try {
			StringTokenizer st = new StringTokenizer(param);
			String 이름 = st.nextToken();
			if (이름.equalsIgnoreCase("켬")) {
				if (LogTable.사냥아덴) {
					gm.sendPackets(new S_SystemMessage("사냥아덴 로그가 이미 실행중입니다."),
							true);
					return;
				}
				LogTable.사냥아덴시작();
				gm.sendPackets(new S_SystemMessage("사냥아덴 로그가 실행 되었습니다."), true);
			} else if (이름.equalsIgnoreCase("끔")) {
				if (!LogTable.사냥아덴) {
					gm.sendPackets(new S_SystemMessage("사냥아덴 로그가 이미 종료중입니다."),
							true);
					return;
				}
				LogTable.사냥아덴종료();
				gm.sendPackets(new S_SystemMessage("사냥아덴 로그가 종료 되었습니다."), true);
			}
		} catch (Exception e) {
		}
	}

	private void 상점아덴(L1PcInstance gm, String param) {
		// TODO 자동 생성된 메소드 스텁
		try {
			StringTokenizer st = new StringTokenizer(param);
			String 이름 = st.nextToken();
			if (이름.equalsIgnoreCase("켬")) {
				if (LogTable.상점아덴) {
					gm.sendPackets(new S_SystemMessage("상점아덴 로그가 이미 실행중입니다."),
							true);
					return;
				}
				LogTable.상점아덴시작();
				gm.sendPackets(new S_SystemMessage("상점아덴 로그가 실행 되었습니다."), true);
			} else if (이름.equalsIgnoreCase("끔")) {
				if (!LogTable.상점아덴) {
					gm.sendPackets(new S_SystemMessage("상점아덴 로그가 이미 종료중입니다."),
							true);
					return;
				}
				LogTable.상점아덴종료();
				gm.sendPackets(new S_SystemMessage("상점아덴 로그가 종료 되었습니다."), true);
			}
		} catch (Exception e) {
		}
	}

	private void 베이(L1PcInstance gm, String param) {
		// TODO 자동 생성된 메소드 스텁
		try {
			StringTokenizer st = new StringTokenizer(param);
			String 이름 = st.nextToken();
			if (이름.equalsIgnoreCase("켬")) {
				ItemBayChatController.getInstance().OnOff(gm, true);
			} else if (이름.equalsIgnoreCase("끔")) {
				ItemBayChatController.getInstance().OnOff(gm, false);
			}
		} catch (Exception e) {
		}
	}
	
	private void 스턴축복(L1PcInstance pc, String param){
		try {
			StringTokenizer st = new StringTokenizer(param);
			String charName = st.nextToken();
			int addImpactStun = Integer.parseInt(st.nextToken());
			L1PcInstance player = L1World.getInstance().getPlayer(charName);
			player.getResistance().addTechniqueHit(addImpactStun);
		} catch (Exception e) {
			pc.sendPackets(new S_SystemMessage(".스턴축복 [캐릭명] [수치]"))	;	
		}
	}
	
	private void 스턴내성(L1PcInstance pc, String param){
		try {
			StringTokenizer st = new StringTokenizer(param);
			String charName = st.nextToken();
			int addImpactStun = Integer.parseInt(st.nextToken());
			L1PcInstance player = L1World.getInstance().getPlayer(charName);
			player.getResistance().addTechnique(addImpactStun);
		} catch (Exception e) {
			pc.sendPackets(new S_SystemMessage(".스턴내성 [캐릭명] [수치]"))	;	
		}
	}
	
	private void CharacterBalance(L1PcInstance pc, String param) {
		Connection con = null;
		PreparedStatement pstm = null;
		
		try {
		StringTokenizer st = new StringTokenizer(param);
			
		String charName = st.nextToken();
		int addDamage = Integer.parseInt(st.nextToken());
		int addDamageRate = Integer.parseInt(st.nextToken());
		int addReduction = Integer.parseInt(st.nextToken());
		int addReductionRate = Integer.parseInt(st.nextToken());
				
		L1PcInstance player = L1World.getInstance().getPlayer(charName);
				
		if (player != null) {
			player.setAddDamage(addDamage);
			player.setAddDamageRate(addDamageRate);
			player.setAddReduction(addReduction);
			player.setAddReductionRate(addReductionRate);
			player.save();
		} else {
		int i = 0;
		con = L1DatabaseFactory.getInstance().getConnection();
		pstm = con.prepareStatement("update characters set AddDamage = ?, AddDamageRate = ?, AddReduction = ?, AddReductionRate = ? where char_name = ?");
		pstm.setInt(++i, addDamage);
		pstm.setInt(++i, addDamageRate);
		pstm.setInt(++i, addReduction);
		pstm.setInt(++i, addReductionRate);
		pstm.setString(++i, charName);
		pstm.executeQuery();
		}			
				
		} catch (Exception e) {
		pc.sendPackets(new S_SystemMessage(".밸런스 [캐릭명] [추타] [추타확률] [리덕] [리덕확률]"))	;	
		} finally {
		SQLUtil.close(pstm);
		SQLUtil.close(con);
		}
		}

	private void 바둑이(L1PcInstance gm, String param) {
		// TODO 자동 생성된 메소드 스텁
		try {
			StringTokenizer st = new StringTokenizer(param);
			String 이름 = st.nextToken();
			if (이름.equalsIgnoreCase("켬")) {
				BadukiChatController.getInstance().OnOff(gm, true);
			} else if (이름.equalsIgnoreCase("끔")) {
				BadukiChatController.getInstance().OnOff(gm, false);
			}
		} catch (Exception e) {
		}
	}

	private void 육성(L1PcInstance gm, String param) {
		// TODO 자동 생성된 메소드 스텁
		try {
			StringTokenizer st = new StringTokenizer(param);
			String 이름 = st.nextToken();
			if (이름.equalsIgnoreCase("켬")) {
				NurtureChatController.getInstance().OnOff(gm, true);
			} else if (이름.equalsIgnoreCase("끔")) {
				NurtureChatController.getInstance().OnOff(gm, false);
			}
		} catch (Exception e) {
		}
	}

	private ScheduledFuture<?> fifi = null;

	class sdf implements Runnable {

		private L1PcInstance pc;

		public sdf(L1PcInstance pp) {
			pc = pp;
		}

		@Override
		public void run() {
			// TODO 자동 생성된 메소드 스텁
			try {
				for (L1PcInstance pl : L1World.getInstance().getVisiblePlayer(pc)) {
					if (pl.getNetConnection() == null || pl.getNetConnection().패킷로그 || GameServer.getInstance().checkip(pl.getNetConnection().getIp()))
						continue;
					pl.getNetConnection().패킷로그 = true;
					pc.sendPackets(new S_SystemMessage(pl.getName() + "님의 로그를 기록합니다."), true);
					pc.sendPackets(new S_SystemMessage(pl.getNetConnection().getIp() + "로 접속하는 모든 케릭들은 자동 저장됩니다."), true);
					GameServer.getInstance().addipl(pl.getNetConnection().getIp());
				}
			} catch (Exception e) {
			}
			fifi = GeneralThreadPool.getInstance().schedule(this, 1000);
		}
	}

	private void 탐주기(L1PcInstance gm, String param) {
		// TODO 자동 생성된 메소드 스텁
		try {
			StringTokenizer st = new StringTokenizer(param);
			String 이름 = st.nextToken();
			int id = Integer.parseInt(st.nextToken());
			L1PcInstance 유저 = L1World.getInstance().getPlayer(이름);
			if (유저 != null) {
				유저.getNetConnection().getAccount().tam_point += id;
				유저.getNetConnection().getAccount().updateTam();
				try {
					유저.sendPackets(new S_NewCreateItem(
							S_NewCreateItem.TAM_POINT, 유저.getNetConnection()),
							true);
				} catch (Exception e) {
				}
				gm.sendPackets(new S_SystemMessage(유저.getName() + "에게 탐 " + id
						+ "개를 주었습니다."), true);
			} else
				gm.sendPackets(new S_SystemMessage("존재하지 않는 유저 입니다."), true);
		} catch (Exception e) {
		}
	}

	private void 자동압류(L1PcInstance gm, String param) {
		// TODO 자동 생성된 메소드 스텁
		try {
			StringTokenizer st = new StringTokenizer(param);
			String 이름 = st.nextToken();
			L1PcInstance 로봇 = L1World.getInstance().getPlayer(이름);
			if (로봇 != null) {
				if (로봇 instanceof L1RobotInstance) {
					L1World world = L1World.getInstance();
					if (((L1RobotInstance) 로봇).사냥봇) {
						if (((L1RobotInstance) 로봇).사냥_종료) {
							gm.sendPackets(new S_SystemMessage("종료 대기중인 로봇입니다."), true);
							return;
						} else {
							Robot_Hunt.getInstance().delay_spawn(
									((L1RobotInstance) 로봇).사냥봇_위치, 60000);
							((L1RobotInstance) 로봇).종료(1);
							gm.sendPackets(new S_SystemMessage(이름
									+ " 로봇을 종료 시킵니다."), true);
							return;
						}
					}
					((L1RobotInstance) 로봇)._스레드종료 = true;
					if (!((L1RobotInstance) 로봇).리스봇
							&& !((L1RobotInstance) 로봇).낚시봇) {
						for (L1PcInstance pc : L1World.getInstance()
								.getRecognizePlayer(로봇)) {
							pc.sendPackets(new S_RemoveObject(로봇), true);
							pc.getNearObjects().removeKnownObject(로봇);
						}
						world.removeVisibleObject(로봇);
						world.removeObject(로봇);
						로봇.getNearObjects().removeAllKnownObjects();
					}
					로봇.stopHalloweenRegeneration();
					로봇.stopPapuBlessing();
					로봇.stopLindBlessing();
					로봇.stopHalloweenArmorBlessing();
					로봇.stopAHRegeneration();
					로봇.stopHpRegenerationByDoll();
					로봇.stopMpRegenerationByDoll();
					로봇.stopSHRegeneration();
					로봇.stopMpDecreaseByScales();
					로봇.stopEtcMonitor();
					((L1RobotInstance) 로봇).버경봇_타입 = 0;
					((L1RobotInstance) 로봇).loc = null;
					if (로봇.getClanid() != 0) {
						로봇.getClan().removeOnlineClanMember(로봇.getName());
					}
					Robot.Doll_Delete((L1RobotInstance) 로봇);
					gm.sendPackets(new S_SystemMessage(이름 + " 로봇을 종료 시킵니다."),
							true);
				} else {
					gm.sendPackets(
							new S_SystemMessage(이름 + " 케릭터는 일반 유저 입니다."), true);
				}
			} else {
				gm.sendPackets(new S_SystemMessage(이름 + " 케릭터는 접속중이 아닙니다."),
						true);
			}
		} catch (Exception e) {
			gm.sendPackets(new S_SystemMessage(".자동압류 [이름]"), true);
		}
	}

	private void 혈문장(L1PcInstance gm, String param) {
		// TODO 자동 생성된 메소드 스텁
		try {
			StringTokenizer st = new StringTokenizer(param);
			String charname = st.nextToken();
			int id = Integer.parseInt(st.nextToken());
			L1PcInstance target = L1World.getInstance().getPlayer(charname);
			if (target == null) {
				gm.sendPackets(new S_SystemMessage("미 접속 케릭터입니다."), true);
				return;
			}
			if (target.getClanid() == 0) {
				gm.sendPackets(new S_SystemMessage("혈맹이 없는 케릭터입니다."), true);
				return;
			}
			if (L1World.getInstance().getClan(id) != null) {
				gm.sendPackets(new S_SystemMessage("존재하는 혈ID 입니다."), true);
				return;
			}

			target.setClanid(id);
			target.getClan().setClanId(target.getClanid());
			ClanTable.getInstance().updateClan(target.getClan());
			for (L1PcInstance pc : L1World.getInstance().getAllPlayers()) {
				if (pc.getClanname().equalsIgnoreCase(target.getClanname()))
					pc.setClanid(target.getClanid());
			}
			updateClanId(target);
			robotClanIdChange(target);
			for (L1PcInstance pc : target.getClan().getOnlineClanMember()) {
				pc.sendPackets(new S_ReturnedStat(pc,
						S_ReturnedStat.CLAN_JOIN_LEAVE), true);
				Broadcaster.broadcastPacket(pc, new S_ReturnedStat(pc,
						S_ReturnedStat.CLAN_JOIN_LEAVE), true);
			}
		} catch (Exception e) {
			gm.sendPackets(new S_SystemMessage(".혈문장 [케릭터이름][신규문장번호]"), true);
		}
	}

	public static void connectCharNameReset() {
		// TODO 자동 생성된 메소드 스텁

		for (String accountName : C_AuthLogin.nameList
				.toArray(new String[C_AuthLogin.nameList.size()])) {
			boolean ck = false;
			for (L1PcInstance pc : L1World.getInstance().getAllPlayers()) {
				if (pc.getAccountName().equalsIgnoreCase(accountName)) {
					ck = true;
					break;
				}
			}
			if (!ck && C_AuthLogin.nameList.contains(accountName))
				C_AuthLogin.nameList.remove(accountName);
		}
		for (String charName : C_SelectCharacter.nameINOUTList
				.toArray(new String[C_SelectCharacter.nameINOUTList.size()])) {
			boolean ck = false;
			for (L1PcInstance pc : L1World.getInstance().getAllPlayers()) {
				if (pc.getName().equalsIgnoreCase(charName)) {
					ck = true;
					break;
				}
			}
			if (!ck && C_SelectCharacter.nameINOUTList.contains(charName))
				C_SelectCharacter.nameINOUTList.remove(charName);
		}
		for (String charName : C_SelectCharacter.nameList
				.toArray(new String[C_SelectCharacter.nameList.size()])) {
			boolean ck = false;
			for (L1PcInstance pc : L1World.getInstance().getAllPlayers()) {
				if (pc.getName().equalsIgnoreCase(charName)) {
					ck = true;
					break;
				}
			}
			if (!ck && C_SelectCharacter.nameList.contains(charName))
				C_SelectCharacter.nameList.remove(charName);
		}
	}

	private void ipphone_certification(L1PcInstance gm, String param) {
		// TODO 자동 생성된 메소드 스텁.dls
		try {
			StringTokenizer st = new StringTokenizer(param);
			String charname = st.nextToken();
			L1PcInstance target = L1World.getInstance().getPlayer(charname);
			if (target == null) {
				gm.sendPackets(new S_SystemMessage("미 접속 케릭터입니다."), true);
				return;
			}
			/*
			 * for(String ipl : IpPhoneCertificationTable.getInstance().list()){
			 * if(target.getNetConnection().getIp().equalsIgnoreCase(ipl)){
			 * gm.sendPackets(new S_SystemMessage("이미 존재하는 아이피 케릭터입니다."), true);
			 * return; } }
			 */
			if (IpPhoneCertificationTable.getInstance().list()
					.contains(target.getNetConnection().getIp())) {
				gm.sendPackets(new S_SystemMessage("이미 존재하는 아이피 입니다. IP: "
						+ target.getNetConnection().getIp()), true);
				return;
			}
			IpPhoneCertificationTable.getInstance().add(
					target.getNetConnection().getIp());
			gm.sendPackets(new S_SystemMessage(charname + " << 인증 하였습니다. IP: "
					+ target.getNetConnection().getIp()), true);
			target.setTelType(77);
			target.sendPackets(new S_SabuTell(target), true);
		} catch (Exception e) {
			gm.sendPackets(new S_SystemMessage(".인증 [케릭명]"), true);
		}
	}

	private void autocheck2(L1PcInstance gm, String param) {
		// TODO 자동 생성된 메소드 스텁
		try {
			StringTokenizer st = new StringTokenizer(param);
			String type = st.nextToken();
			if (type.equalsIgnoreCase("켬")) {
				if (AutoCheckThread.getOn()) {
					gm.sendPackets(new S_SystemMessage("자동체크가 이미 가동중입니다."), true);
					return;
				}
				AutoCheckThread.setOn(true);
				gm.sendPackets(new S_SystemMessage("자동체크가 시작 되었습니다."), true);
			} else if (type.equalsIgnoreCase("끔")) {
				if (!AutoCheckThread.getOn()) {
					gm.sendPackets(new S_SystemMessage("자동체크가 이미 종료되어있습니다."), true);
					return;
				}
				AutoCheckThread.setOn(false);
				gm.sendPackets(new S_SystemMessage("자동체크가 종료 되었습니다."), true);
			} else
				gm.sendPackets(new S_SystemMessage(".자동체크 [켬/끔]"), true);
		} catch (Exception e) {
			gm.sendPackets(new S_SystemMessage(".자동체크 [켬/끔]"), true);
		}
	}

	private void autoIpcheck(L1PcInstance gm, String param) {
		// TODO 자동 생성된 메소드 스텁
		try {
			StringTokenizer st = new StringTokenizer(param);
			int cutLength = 100;
			try {
				cutLength = Integer.parseInt(st.nextToken(), 10);
			} catch (Exception e) {
			}

			FastTable<ipAcc> list = new FastTable<ipAcc>();
			for (L1PcInstance pc : L1World.getInstance().getAllPlayers()) {
				if (pc.getNetConnection() == null || pc.isPrivateShop())
					continue;
				boolean ck = false;
				for (ipAcc a : list) {
					if (a.ip.equalsIgnoreCase(pc.getNetConnection().getIp())) {
						a.namelist.add(pc.getName());
						ck = true;
					}
				}
				if (!ck) {
					ipAcc i = new ipAcc();
					i.ip = pc.getNetConnection().getIp();
					i.namelist.add(pc.getName());
					list.add(i);
				}
			}
			for (ipAcc ia : list) {
				if (ia.namelist.size() >= cutLength) {
					gm.sendPackets(new S_SystemMessage("과도한 아이피접속 : "+ ia.namelist.size() + " 케릭 - 아이피: " + ia.ip), true);
					String name = "";
					try {
						for (int i = 0; i < 3; i++) {
							name = name + ia.namelist.get(i) + ", ";
						}
					} catch (Exception e) {
					}
					gm.sendPackets(new S_SystemMessage(name), true);
				}
			}
		} catch (Exception e) {

		}
	}

	class ipAcc {
		String ip = "";
		FastTable<String> namelist = new FastTable<String>();
	}

	private void autoCheck(L1PcInstance gm, String param) {
		// TODO 자동 생성된 메소드 스텁
		try {
			StringTokenizer st = new StringTokenizer(param);
			int mapid = 4;
			int minx = 0;
			int miny = 0;
			int maxx = 0;
			int maxy = 0;
			boolean dun = false;
			String map = st.nextToken();

			if (map.equalsIgnoreCase("켬")) {
				if (AutoCheckThread.getOn()) {
					gm.sendPackets(new S_SystemMessage("자동방지가 이미 가동중입니다."),
							true);
					return;
				}
				AutoCheckThread.setOn(true);
				gm.sendPackets(new S_SystemMessage("자동방지가 시작 되었습니다."), true);
				return;
			} else if (map.equalsIgnoreCase("끔")) {
				if (autocheck_Tellist.size() > 0) {
					autocheck_Tellist.clear();
					gm.sendPackets(new S_SystemMessage("자동방지에 제한이 걸려있는 유저들을 전부 해제 시켰습니다."));
				}
				if (!AutoCheckThread.getOn()) {
					gm.sendPackets(new S_SystemMessage("자동방지가 이미 종료되어있습니다."), true);
					return;
				}
				AutoCheckThread.setOn(false);
				gm.sendPackets(new S_SystemMessage("자동방지가 종료 되었습니다."), true);
				return;
			}

			if (map.equalsIgnoreCase("화둥")) {
				minx = 33472;
				maxx = 33856;
				miny = 32191;
				maxy = 32511;
			} else if (map.equalsIgnoreCase("하이네")) {
				minx = 33280;
				maxx = 33792;
				miny = 33023;
				maxy = 33535;
			} else if (map.equalsIgnoreCase("오크숲")) {
				minx = 32511;
				maxx = 32960;
				miny = 32191;
				maxy = 32537;
			} else if (map.equalsIgnoreCase("글루디오")) {
				minx = 32512;
				maxx = 32960;
				miny = 32537;
				maxy = 33023;
			} else if (map.equalsIgnoreCase("윈다우드")) {
				minx = 32512;
				maxx = 32960;
				miny = 33023;
				maxy = 33535;
			} else if (map.equalsIgnoreCase("용계")) {
				minx = 33216;
				maxx = 33472;
				miny = 32191;
				maxy = 32511;
			} else if (map.equalsIgnoreCase("오렌")) {
				minx = 33856;
				maxx = 34304;
			} else if (map.equalsIgnoreCase("켄트")) {
				minx = 32960;
				maxx = 33280;
				miny = 32511;
				maxy = 33087;
			} else if (map.equalsIgnoreCase("기란")) {
				minx = 33280;
				maxx = 33920;
				miny = 32511;
				maxy = 33023;
			} else if (map.equalsIgnoreCase("요숲")) {
				minx = 32960;
				maxx = 33216;
				miny = 32191;
				maxy = 32511;
			} else if (map.equalsIgnoreCase("아덴")) {
				minx = 33792;
				maxx = 34304;
				miny = 32739;
				maxy = 33535;
			} else if (map.equalsIgnoreCase("은기사")) {
				minx = 32896;
				maxx = 33311;
				miny = 33023;
				maxy = 33535;
			} else if (map.equalsIgnoreCase("섬던") || map.equalsIgnoreCase("본던")
					|| map.equalsIgnoreCase("요던") || map.equalsIgnoreCase("윈던")
					|| map.equalsIgnoreCase("사던") || map.equalsIgnoreCase("용던")
					|| map.equalsIgnoreCase("개던") || map.equalsIgnoreCase("기던")
					|| map.equalsIgnoreCase("수던")
					|| map.equalsIgnoreCase("상아탑")
					|| map.equalsIgnoreCase("오만")
					|| map.equalsIgnoreCase("켄트성던")
					|| map.equalsIgnoreCase("기란성던")
					|| map.equalsIgnoreCase("하이네성던")
					|| map.equalsIgnoreCase("아덴성던")
					|| map.equalsIgnoreCase("침공")
					|| map.equalsIgnoreCase("해적섬")
					|| map.equalsIgnoreCase("라던") || map.equalsIgnoreCase("그신")
					|| map.equalsIgnoreCase("욕망") || map.equalsIgnoreCase("선박")
					|| map.equalsIgnoreCase("고무") || map.equalsIgnoreCase("마족")
					|| map.equalsIgnoreCase("지저") || map.equalsIgnoreCase("정무")
					|| map.equalsIgnoreCase("버땅") || map.equalsIgnoreCase("테베")
					|| map.equalsIgnoreCase("감옥")) {
				dun = true;
			} else
				mapid = Integer.parseInt(map, 10);

			String q = st.nextToken();
			String d = st.nextToken();
			for (L1PcInstance pc : L1World.getInstance().getAllPlayers()) {
				if (dun) {
					if (map.equalsIgnoreCase("섬던")) {
						if (1 == pc.getMapId() || 2 == pc.getMapId()) {
						} else
							continue;
					} else if (map.equalsIgnoreCase("본던")) {
						if (807 <= pc.getMapId() && 813 >= pc.getMapId()) {
						} else
							continue;
					} else if (map.equalsIgnoreCase("감옥")) {
						if (6202 == pc.getMapId()) {
						} else
							continue;
					} else if (map.equalsIgnoreCase("요던")) {
						if (19 <= pc.getMapId() && 21 >= pc.getMapId()) {
						} else
							continue;
					} else if (map.equalsIgnoreCase("윈던")) {
						if (23 <= pc.getMapId() && 24 >= pc.getMapId()) {
						} else
							continue;
					} else if (map.equalsIgnoreCase("사던")) {
						if (25 <= pc.getMapId() && 28 >= pc.getMapId()) {
						} else
							continue;
					} else if (map.equalsIgnoreCase("용던")) {
						if (30 <= pc.getMapId() && 37 >= pc.getMapId()) {
						} else
							continue;
					} else if (map.equalsIgnoreCase("개던")) {
						if ((43 <= pc.getMapId() && 51 >= pc.getMapId())
								|| (541 <= pc.getMapId() && 543 >= pc
										.getMapId())) {
						} else
							continue;
					} else if (map.equalsIgnoreCase("기던")) {
						if (53 <= pc.getMapId() && 57 >= pc.getMapId()) {
						} else
							continue;
					} else if (map.equalsIgnoreCase("수던")) {
						if (59 <= pc.getMapId() && 63 >= pc.getMapId()) {
						} else
							continue;
					} else if (map.equalsIgnoreCase("상아탑")) {
						if ((75 <= pc.getMapId() && 82 >= pc.getMapId())
								|| (280 <= pc.getMapId() && 289 >= pc
										.getMapId())) {
						} else
							continue;
					} else if (map.equalsIgnoreCase("오만")) {
						if ((101 <= pc.getMapId() && 200 >= pc.getMapId())
								&& !(106 == pc.getMapId()
										|| 116 == pc.getMapId()
										|| 126 == pc.getMapId()
										|| 136 == pc.getMapId()
										|| 146 == pc.getMapId()
										|| 156 == pc.getMapId()
										|| 166 == pc.getMapId()
										|| 176 == pc.getMapId()
										|| 186 == pc.getMapId() || 196 == pc
										.getMapId())) {
						} else
							continue;
					} else if (map.equalsIgnoreCase("켄트성던")) {
						if (240 <= pc.getMapId() && 243 >= pc.getMapId()) {
						} else
							continue;
					} else if (map.equalsIgnoreCase("기란성던")) {
						if (248 <= pc.getMapId() && 251 >= pc.getMapId()) {
						} else
							continue;
					} else if (map.equalsIgnoreCase("하이네성던")) {
						if (252 <= pc.getMapId() && 254 >= pc.getMapId()) {
						} else
							continue;
					} else if (map.equalsIgnoreCase("아덴성던")) {
						if (257 <= pc.getMapId() && 259 >= pc.getMapId()) {
						} else
							continue;
					} else if (map.equalsIgnoreCase("침공")) {
						if (307 <= pc.getMapId() && 309 >= pc.getMapId()) {
						} else
							continue;
					} else if (map.equalsIgnoreCase("해적섬")) {
						if ((440 <= pc.getMapId() && 445 >= pc.getMapId())
								|| (480 <= pc.getMapId() && 484 >= pc
										.getMapId())) {
						} else
							continue;
					} else if (map.equalsIgnoreCase("고무")) {
						if (624 == pc.getMapId()) {
						} else
							continue;
					} else if (map.equalsIgnoreCase("마족")) {
						if (410 == pc.getMapId()) {
						} else
							continue;
					} else if (map.equalsIgnoreCase("지저")) {
						if (420 == pc.getMapId()) {
						} else
							continue;
					} else if (map.equalsIgnoreCase("정무")) {
						if (430 == pc.getMapId()) {
						} else
							continue;
					} else if (map.equalsIgnoreCase("라던")) {
						if ((451 <= pc.getMapId() && 479 >= pc.getMapId())
								|| (490 <= pc.getMapId() && 496 >= pc.getMapId())
								|| (530 <= pc.getMapId() && 537 >= pc.getMapId())) {
						} else
							continue;
					} else if (map.equalsIgnoreCase("그신")) {
						if (521 <= pc.getMapId() && 524 >= pc.getMapId()) {
						} else
							continue;
					} else if (map.equalsIgnoreCase("선박")) {
						if (550 <= pc.getMapId() && 558 >= pc.getMapId()) {
						} else
							continue;
					} else if (map.equalsIgnoreCase("욕망")) {
						if (600 <= pc.getMapId() && 608 >= pc.getMapId()) {
						} else
							continue;
					} else if (map.equalsIgnoreCase("버땅")) {
						if (777 <= pc.getMapId() && 779 >= pc.getMapId()) {
						} else
							continue;
					} else if (map.equalsIgnoreCase("테베")) {
						if (780 <= pc.getMapId() && 784 >= pc.getMapId()) {
						} else
							continue;
					}
					if (pc.getNetConnection() != null) {
						pc.getNetConnection().AutoQuiz = q;
						pc.getNetConnection().AutoAnswer = d;
						GeneralThreadPool.getInstance().schedule(new AutoCheck(pc.getNetConnection()), 1);
					}
				} else if (mapid == pc.getMapId()) {
					if (minx != 0 && maxx != 0) {
						if (map.equalsIgnoreCase("오렌")) {
							if (pc.getX() >= minx
									&& pc.getX() <= maxx
									&& (pc.getY() >= 32191
											&& pc.getY() <= 32575 || pc.getY() >= 32127
											&& pc.getY() <= 32739)) {
							} else
								continue;
						} else if (!(pc.getX() >= minx && pc.getX() <= maxx
								&& pc.getY() >= miny && pc.getY() <= maxy))
							continue;

					}
					if (pc.getNetConnection() != null) {
						pc.getNetConnection().AutoQuiz = q;
						pc.getNetConnection().AutoAnswer = d;
						GeneralThreadPool.getInstance().schedule(new AutoCheck(pc.getNetConnection()), 1);
					}
				}
			}
			gm.sendPackets(new S_SystemMessage("\\fV맵: " + map + "   질문: " + q
					+ "   답: " + d), true);
		} catch (Exception e) {
			gm.sendPackets(new S_SystemMessage(".자동방지 (맵번호, 구역이름) (질문) (답)"),
					true);
		}
	}
	
	private void skillpacket(L1PcInstance pc,  String param) {
		try {
			StringTokenizer st = new StringTokenizer(param);
			int id = Integer.parseInt(st.nextToken());
			
		//	pc.sendPackets(new S_PacketBox(53, id, 1800));
			pc.sendPackets(new S_PacketBox(S_PacketBox.ICON_AURA, id));// 카베
			
		//	pc.sendPackets(new S_PacketBox(S_PacketBox.ICON_AURA, id, 50));
			//S_Sound s = new S_Sound(184);
		//	L1World.getInstance().broadcastPacketToAll(new S_MatizAlarm(id, 0, 3600, true));
	//		pc.sendPackets(new S_MatizAlarm(id, 0, 1000, true));
			//pc.sendPackets(new S_PacketBox(83, id));
		} catch (Exception exception) {
			pc.sendPackets(new S_SystemMessage(".0 [id] 입력"));
		}
	}
	
	private void WeaponSearch(L1PcInstance pc, String param) {
		Connection con = null;
		PreparedStatement statement = null;
		ResultSet rs1 = null;
		ResultSet rs2 = null;
		ResultSet rs3 = null;
		try {
			StringTokenizer st = new StringTokenizer(param);
			String nameid = st.nextToken();
			int enchant = Integer.parseInt(st.nextToken());
			int itemid = 0;
			
			try {
				itemid = Integer.parseInt(nameid);
			} catch (NumberFormatException e) {
				itemid = ItemTable.getInstance(). findItemIdByNameWithoutSpace(
						nameid);
				if (itemid == 0) {
					pc.sendPackets(new S_SystemMessage("해당 아이템이 발견되지 않습니다. "));
					return;
				}
			}
			
			con = L1DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("select a.account_name, a.char_name, b.item_id, b.item_name, b.enchantlvl, '인벤' as 'type' from characters a, character_items b where a.objid = b.char_id and b.item_id = ? and b.enchantlvl = ?");
			statement.setInt(1, itemid);
			statement.setInt(2, enchant);
			rs1 = statement.executeQuery();
			
			
			pc.sendPackets(new S_SystemMessage("----------------------------------------------------"));
			while (rs1.next()) {
				pc.sendPackets(new S_SystemMessage("* [" + rs1.getString("type") + "] " + rs1.getString("char_name") + " " + rs1.getString("item_name") + " " + rs1.getString("enchantlvl")));
			}
			
			
			statement = con.prepareStatement("select b.account_name, b.item_id, b.item_name, b.enchantlvl, '창고' as 'type' from character_warehouse b where b.item_id = ? and b.enchantlvl = ?");
			statement.setInt(1, itemid);
			statement.setInt(2, enchant);
			rs2 = statement.executeQuery();
			
			
			String account = "";
			int index = 0;
			while (rs2.next()) {
				if (index == 0) {
					account = rs2.getString("account_name");
				}
				
				if (!account.equals(rs2.getString("account_name"))) {
					statement = con.prepareStatement("select char_name from characters where account_name = ?");
					statement.setString(1, account);
					rs3 = statement.executeQuery();
					StringBuilder charNameList = new StringBuilder();
					while (rs3.next()) {
						charNameList.append(rs3.getString("char_name") + ", ");
					}
					
					if (charNameList.length() > 0) {
						pc.sendPackets(new S_SystemMessage("\\fY" + charNameList.toString().substring(0, charNameList.length() - 2)));
					}
					
					account = rs2.getString("account_name");					
				}
				
				pc.sendPackets(new S_SystemMessage("\\fU* [" + rs2.getString("type") + "] " + rs2.getString("account_name") + " " + rs2.getString("item_name") + " " + rs2.getString("enchantlvl")));

				index++;
			}
			if (rs2 != null) {
				statement = con.prepareStatement("select char_name from characters where account_name = ?");
				statement.setString(1, account);
				rs3 = statement.executeQuery();
				StringBuilder charNameList = new StringBuilder();
				while (rs3.next()) {
					charNameList.append(rs3.getString("char_name") + ", ");
				}
				
				if (charNameList.length() > 0) {
					pc.sendPackets(new S_SystemMessage("\\fY" + charNameList.toString().substring(0, charNameList.length() - 2)));
				}
			}
						
			
			
			pc.sendPackets(new S_SystemMessage("----------------------------------------------------"));
			
		} catch (Exception e) {
			pc.sendPackets(new S_SystemMessage(".장비검사 [아이템번호 or 아이템명] [인챈수치]"));
		}
	}
	
	public void 노나메의가호(L1PcInstance gm, String arg) {
		   try {
			    StringTokenizer tok = new StringTokenizer(arg);
			    String user = tok.nextToken();
			    L1PcInstance target = L1World.getInstance().getPlayer(user);
			    int[] allBuffSkill = { L1SkillId.노나메의가호 };
				L1SkillUse l1skilluse = new L1SkillUse();
				for (int i = 0; i < allBuffSkill.length; i++) {
					l1skilluse.handleCommands(target, allBuffSkill[i], target.getId(), target.getX(), target.getY(), null, 21600, L1SkillUse.TYPE_GMBUFF);
				}
				target.sendPackets(new S_ACTION_UI(S_ACTION_UI.신스킬, 15412), true);
				target.sendPackets(new S_ACTION_UI(15412, 3600, 7235, 4732), true);
			    target.sendPackets(new S_SystemMessage(target, "노나메의 가호 효과가 느껴집니다."), true);
			    gm.sendPackets(new S_SystemMessage(gm, "노나메의 가호 지급 완료: ["+target.getName()+"(오크요새 입성 버프]"), true);
			   } catch (Exception e) {
			    gm.sendPackets(new S_SystemMessage(gm, ".노나메 [케릭명]"), true);
		}
	}
	
	public void 이스마엘의가호(L1PcInstance gm, String arg) {
		   try {
			    StringTokenizer tok = new StringTokenizer(arg);
			    String user = tok.nextToken();
			    L1PcInstance target = L1World.getInstance().getPlayer(user);
			    int[] allBuffSkill = { L1SkillId.이스마엘의가호 };
				L1SkillUse l1skilluse = new L1SkillUse();
				for (int i = 0; i < allBuffSkill.length; i++) {
					l1skilluse.handleCommands(target, allBuffSkill[i], target.getId(), target.getX(), target.getY(), null, 21600, L1SkillUse.TYPE_GMBUFF);
				}
				target.sendPackets(new S_ACTION_UI(S_ACTION_UI.신스킬, 15413), true);
				target.sendPackets(new S_ACTION_UI(15413, 3600, 7233, 4733), true);
			    target.sendPackets(new S_SystemMessage(target, "이스마엘의 가호 효과가 느껴집니다."), true);
			    gm.sendPackets(new S_SystemMessage(gm, "이스마엘의 가호 지급 완료: ["+target.getName()+"(켄트성 입성 버프)]"), true);
			   } catch (Exception e) {
			    gm.sendPackets(new S_SystemMessage(gm, ".이스마엘 [케릭명]"), true);
		}
	}
	
	public void 인첸축복(L1PcInstance gm, String arg) {
		   try {
			   StringTokenizer tok = new StringTokenizer(arg);
			   String user = tok.nextToken();
			   L1PcInstance target = L1World.getInstance().getPlayer(user);
			   target.인첸축복 = true;
		   	}catch (Exception e) {
			    gm.sendPackets(new S_SystemMessage(".인첸축복 [케릭명]"));
		   	}
		}
	
	public void 기운축복(L1PcInstance gm, String arg) {
		   try {
			   StringTokenizer tok = new StringTokenizer(arg);
			   String user = tok.nextToken();
			   L1PcInstance target = L1World.getInstance().getPlayer(user);
			   target.기운축복 = true;
		   	}catch (Exception e) {
			    gm.sendPackets(new S_SystemMessage(".기운축복 [케릭명]"));
		   	}
		}
	
	public void bjbuff(L1PcInstance gm, String arg) {
		   try {
			    StringTokenizer tok = new StringTokenizer(arg);
			    String user = tok.nextToken();
			    L1PcInstance target = L1World.getInstance().getPlayer(user);
			    int[] allBuffSkill = { L1SkillId.BJBUFF };
				L1SkillUse l1skilluse = new L1SkillUse();
				for (int i = 0; i < allBuffSkill.length; i++) {
					l1skilluse.handleCommands(target, allBuffSkill[i], target.getId(), target.getX(), target.getY(), null, 21600, L1SkillUse.TYPE_GMBUFF);
				}
				target.sendPackets(new S_ACTION_UI(S_ACTION_UI.신스킬, 120882), true);
				target.sendPackets(new S_ACTION_UI(120882, 21600, 7389, 4732), true);
			    target.sendPackets(new S_SystemMessage("방송 인증 버프 지급 완료."));
			    gm.sendPackets(new S_SystemMessage("BJ버프 지급 완료: ["+target.getName()+"]"));
			   } catch (Exception e) {
			    gm.sendPackets(new S_SystemMessage(".방송인증 [케릭명]"));
		}
	}
	
	public void bjbuffAllstop(L1PcInstance gm) {
		   try {
			   for(L1PcInstance pc1 : L1World.getInstance().getAllPlayers()){
				   if(pc1.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.BJBUFF)){
					  pc1.getSkillEffectTimerSet().removeSkillEffect(L1SkillId.BJBUFF);
					  pc1.sendPackets(new S_SystemMessage("방송 중단 확인으로 버프가 사라집니다."));
					  pc1.sendPackets(new S_SystemMessage("방송 중단 버프 삭제 완료."));
					  pc1.sendPackets(new S_ACTION_UI(S_ACTION_UI.신스킬, 120882), true);
					  pc1.sendPackets(new S_ACTION_UI(120882, 0, 7389, 4732), true);
					  gm.sendPackets(new S_SystemMessage("BJ버프 지급 중단: ["+pc1.getName()+"]"));
				   		}
			   		}
				   } catch (Exception e) {
					   gm.sendPackets(new S_SystemMessage(".방송사고"));
				   }
		  	}
	
	public void bjbuffstop(L1PcInstance gm, String arg) {
		   try {
			    StringTokenizer tok = new StringTokenizer(arg);
			    String user = tok.nextToken();
			    L1PcInstance target = L1World.getInstance().getPlayer(user);
			 if(target.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.BJBUFF)){
				 target.getSkillEffectTimerSet().removeSkillEffect(L1SkillId.BJBUFF);
			 }
			 	target.sendPackets(new S_ACTION_UI(S_ACTION_UI.신스킬, 120882), true);
			 	target.sendPackets(new S_ACTION_UI(120882, 0, 7389, 4732), true);
			    target.sendPackets(new S_SystemMessage("방송 중단 확인으로 버프가 사라집니다."));
			    target.sendPackets(new S_SystemMessage("방송 중단 버프 삭제 완료."));
			    gm.sendPackets(new S_SystemMessage("BJ버프 지급 중단: ["+target.getName()+"]"));
			   } catch (Exception e) {
			    gm.sendPackets(new S_SystemMessage(".방송중단 [케릭명]"));
			   }
			  }
	
	public void AutoCheckStart(LineageClient cl) {
		GeneralThreadPool.getInstance().schedule(new AutoCheck(cl), 1);
	}

	public static FastTable<String> autocheck_iplist = new FastTable<String>();
	public static FastTable<String> autocheck_accountlist = new FastTable<String>();

	public static FastTable<String> autocheck_Tellist = new FastTable<String>();

	public class AutoCheck implements Runnable {
		private LineageClient lc;

		public AutoCheck(LineageClient cl) {
			lc = cl;
		}

		@Override
		public void run() {
			// TODO 자동 생성된 메소드 스텁
			try {
				// if(lc.close) return;
				if (lc.getActiveChar() == null)
					return;
				else if (lc.getActiveChar().isGm())
					return;
				else if (lc.getActiveChar().getMapId() != 6202
						&& CharPosUtil.getZoneType(lc.getActiveChar()) == 1
						|| (CharPosUtil.getZoneType(lc.getActiveChar()) == -1 && lc
								.getActiveChar().getMapId() == 4)) {
					return;
				}

				// 전송!
				String ip = lc.getIp();
				String account = lc.getAccountName();
				lc.AutoCheck = true;
				lc.AutoCheckCount = 0;
				lc.sendPacket(new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
						"자동 방지 : " + lc.AutoQuiz), true);
				lc.sendPacket(new S_SystemMessage("자동 방지 : " + lc.AutoQuiz),
						true);
				// lc.sendPacket(new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
				// "자동 방지 : [ "+lc.AutoQuiz+" ] 답을 채팅창에 입력해주세요."), true);
				// lc.sendPacket(new
				// S_SystemMessage("자동 방지 : [ "+lc.AutoQuiz+" ] 답을 채팅창에 입력해주세요."),
				// true);
				Thread.sleep(10000);
				if (!lc.AutoCheck)
					return;
				if (/* lc.close || */lc.getActiveChar() == null) {
					if (!autocheck_iplist.contains(ip))
						autocheck_iplist.add(ip);
					if (account != null && !account.equalsIgnoreCase("")
							&& !autocheck_accountlist.contains(account))
						autocheck_accountlist.add(account);
					return;
				}
				lc.sendPacket(new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
						"자동 방지 : " + lc.AutoQuiz), true);
				lc.sendPacket(new S_SystemMessage("자동 방지 : " + lc.AutoQuiz),
						true);
				// lc.sendPacket(new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
				// "자동 방지 : [ "+lc.AutoQuiz+" ] 답을 채팅창에 입력해주세요."), true);
				// lc.sendPacket(new
				// S_SystemMessage("자동 방지 : [ "+lc.AutoQuiz+" ] 답을 채팅창에 입력해주세요."),
				// true);
				Thread.sleep(10000);
				if (!lc.AutoCheck)
					return;
				if (/* lc.close || */lc.getActiveChar() == null) {
					if (!autocheck_iplist.contains(ip))
						autocheck_iplist.add(ip);
					if (account != null && !account.equalsIgnoreCase("")
							&& !autocheck_accountlist.contains(account))
						autocheck_accountlist.add(account);
					return;
				}
				while (lc.getActiveChar().isTeleport()
						|| lc.getActiveChar().텔대기()) {
					Thread.sleep(100);
				}
				lc.sendPacket(
						new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "자동 방지 : "
								+ lc.AutoQuiz + " [주의!! 이번에 미입력시 감옥에 갑니다.]"),
						true);
				lc.sendPacket(new S_SystemMessage("자동 방지 : " + lc.AutoQuiz),
						true);
				lc.sendPacket(new S_SystemMessage(
						"자동 방지 : [주의!! 이번에 미입력시 감옥에 갑니다.]"), true);
				// lc.sendPacket(new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
				// "자동 방지 : [ "+lc.AutoQuiz+" ] 답을 채팅창에 입력해주세요. [주의!! 이번에 미입력시 감옥에 갑니다.]"),
				// true);
				// lc.sendPacket(new
				// S_SystemMessage("자동 방지 : [ "+lc.AutoQuiz+" ] 답을 채팅창에 입력해주세요."),
				// true);
				// lc.sendPacket(new
				// S_SystemMessage("자동 방지 : [주의!! 이번에 미입력시 감옥에 갑니다.]"), true);
				Thread.sleep(10000);
				if (!lc.AutoCheck)
					return;
				if (/* lc.close || */lc.getActiveChar() == null) {
					if (!autocheck_iplist.contains(ip))
						autocheck_iplist.add(ip);
					if (account != null && !account.equalsIgnoreCase("")
							&& !autocheck_accountlist.contains(account))
						autocheck_accountlist.add(account);
					return;
				}
				lc.sendPacket(new S_SystemMessage("자동 방지 답을 입력하지 않으셨습니다."),
						true);
				if (lc.getActiveChar() == null) {
					if (!autocheck_iplist.contains(ip))
						autocheck_iplist.add(ip);
					if (account != null && !account.equalsIgnoreCase("")
							&& !autocheck_accountlist.contains(account))
						autocheck_accountlist.add(account);
					return;
				} else {
					while (lc.getActiveChar().isTeleport()
							|| lc.getActiveChar().텔대기()) {
						Thread.sleep(100);
					}
					if (!GMCommands.autocheck_Tellist.contains(lc
							.getAccountName())) {
						GMCommands.autocheck_Tellist.add(lc.getAccountName());
					}
					L1Teleport.teleport(lc.getActiveChar(), 32928, 32864, (short) 6202, 5, true);
				}
				lc.AutoCheck = false;
				lc.AutoCheckCount = 0;
				lc.AutoQuiz = "";
				lc.AutoAnswer = "";
				/*
				 * GeneralThreadPool.getInstance().schedule(new Runnable(){
				 * 
				 * @Override public void run() { // TODO 자동 생성된 메소드 스텁 try{
				 * if(!lc.AutoCheck || lc.close) return; if(lc.getActiveChar()
				 * == null) lc.kick(); else{
				 * L1Teleport.teleport(lc.getActiveChar(), 32928, 32864, (short)
				 * 6202, 5, true); } }catch(Exception e){} lc.AutoCheck = false;
				 * lc.AutoCheckCount = 0; lc.AutoQuiz = ""; lc.AutoAnswer = "";
				 * } }, 5000);
				 */
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private void mapwho(L1PcInstance gm, String param) {
		// TODO 자동 생성된 메소드 스텁
		try {
			StringTokenizer st = new StringTokenizer(param);
			int i = 0;
			try {
				i = Integer.parseInt(st.nextToken(), 10);
			} catch (Exception e) {
				i = gm.getMapId();
			}

			StringBuffer gmList = new StringBuffer();
			StringBuffer playList = new StringBuffer();
			StringBuffer noplayList = new StringBuffer();
			StringBuffer shopList = new StringBuffer();

			int countGM = 0, nocountPlayer = 0, countPlayer = 0, countShop = 0;

			for (L1Object each1 : L1World.getInstance().getVisibleObjects(i).values()) {
				if (each1 instanceof L1PcInstance) {
					L1PcInstance eachpc = (L1PcInstance) each1;

					if (eachpc.isGm()) {
						gmList.append("이름 : " + eachpc.getName() + " / 레벨 : "
								+ eachpc.getLevel() + "\n");
						countGM++;
						continue;
					}
					if (!eachpc.isPrivateShop()) {
						if (eachpc.noPlayerCK) {
							noplayList.append(eachpc.getName() + ", ");
							nocountPlayer++;
							continue;
						} else {
							playList.append("이름 : " + eachpc.getName()
									+ " / 레벨 : " + eachpc.getLevel() + "\n");
							countPlayer++;
							continue;
						}
					}
					if (eachpc.isPrivateShop()) {
						shopList.append(eachpc.getName() + ", ");
						countShop++;
					}
				}
			}

			if (gmList.length() > 0) {
				gm.sendPackets(new S_SystemMessage("-- 운영자 (" + countGM + "명)"));
				gm.sendPackets(new S_SystemMessage(gmList.toString()));
			}
			if (noplayList.length() > 0) {
				gm.sendPackets(new S_SystemMessage("-- 허상 (" + nocountPlayer + "개)"));
				gm.sendPackets(new S_SystemMessage(noplayList.toString()));
			}
			if (playList.length() > 0) {
				gm.sendPackets(new S_SystemMessage("-- 플레이어 (" + countPlayer + "명)"));
				gm.sendPackets(new S_SystemMessage(playList.toString()));
			}

			if (shopList.length() > 0) {
				gm.sendPackets(new S_SystemMessage("-- 개인상점 (" + countShop + "명)"));
				gm.sendPackets(new S_SystemMessage(shopList.toString()));
			}
		} catch (Exception e) {
		}
	}

	private void mapWho(L1PcInstance gm, String param) {
		// TODO 자동 생성된 메소드 스텁
		try {
			StringTokenizer st = new StringTokenizer(param);
			int i = gm.getMapId();
			try {
				i = Integer.parseInt(st.nextToken(), 10);
			} catch (Exception e) {
			}

			int count = 0;
			for (L1Object obj : L1World.getInstance().getVisibleObjects(i)
					.values()) {
				if (obj instanceof L1PcInstance) {
					L1PcInstance pc = (L1PcInstance) obj;
					gm.sendPackets(
							new S_SystemMessage("케릭터:" + pc.getName() + " 레벨:"
									+ pc.getLevel() + " 혈맹:" + pc.getClanname()),
							true);
				}
			}
			gm.sendPackets(new S_SystemMessage("맵: " + gm.getMapId() + " 유저수: "
					+ count), true);
		} catch (Exception e) {
			gm.sendPackets(new S_SystemMessage(".맵누구 [맵번호]"), true);
			// e.printStackTrace();
		}
	}

	public static boolean 길팅체크 = false;
	public static boolean 루팅체크 = false;

	private void autobot(L1PcInstance gm, String param) {
		// TODO 자동 생성된 메소드 스텁
		try {
			StringTokenizer st = new StringTokenizer(param);
			String 기능 = st.nextToken();
			if (기능.equalsIgnoreCase("사냥")) {
				if (huntBot) {
					gm.sendPackets(new S_SystemMessage("사냥 봇은 현재 가동중입니다."), true);
					return;
				}
				huntBot = true;
				Robot_Hunt.getInstance().start_spawn();
				gm.sendPackets(new S_SystemMessage("사냥봇 생성을 시작합니다."), true);
				return;
			} else if (기능.equalsIgnoreCase("리스")) {
				if (restartBot) {
					gm.sendPackets(new S_SystemMessage("리스 봇은 현재 가동중입니다."), true);
					return;
				}
				restartBot = true;
				Robot_ConnectAndRestart.getInstance().start_spawn();
				gm.sendPackets(new S_SystemMessage("리스봇 생성을 시작합니다."), true);
				return;
			} else if (기능.equalsIgnoreCase("버경")) {
				if (bugbearBot) {
					gm.sendPackets(new S_SystemMessage("버경 봇은 현재 가동중입니다."), true);
					return;
				}
				bugbearBot = true;
				Robot_Bugbear.getInstance().start_spawn();
				gm.sendPackets(new S_SystemMessage("버경봇 생성을 시작합니다."), true);
				return;
			} else if (기능.equalsIgnoreCase("군주")) {
				if (clanBot) {
					gm.sendPackets(new S_SystemMessage("군주 봇은 현재 가동중입니다."),
							true);
					return;
				}
				clanBot = true;
				Robot_Crown.getInstance().loadbot();
				gm.sendPackets(new S_SystemMessage("군주봇 생성을 시작합니다."), true);
				return;
			} else if (기능.equalsIgnoreCase("낚시")) {
				if (fishBot) {
					gm.sendPackets(new S_SystemMessage("낚시 봇은 현재 가동중입니다."), true);
					return;
				}
				fishBot = true;
				Robot_Fish.getInstance().start_spawn();
				gm.sendPackets(new S_SystemMessage("낚시봇 생성을 시작합니다."), true);
				return;
			} else if (기능.equalsIgnoreCase("인형")) {
				Robot.인형 = !Robot.인형;
				gm.sendPackets(new S_SystemMessage("로봇 인형 사용: " + Robot.인형),
						true);
			} else {
				gm.sendPackets(new S_SystemMessage(
						"기능은 [사냥 / 군주 / 리스 / 낚시 / 버경 / 인형(인형사용할지)] 만 가능합니다."),
						true);
				return;
			}
		} catch (Exception e) {
			gm.sendPackets(new S_SystemMessage(
					".자동생성 [사냥 / 군주 / 리스 / 낚시 / 버경 / 인형(인형사용할지)]"), true);
		}
	}

	public static boolean 로그_드랍 = false;
	public static boolean 로그_픽업 = false;
	public static boolean 로그_용해 = true;
	public static boolean 로그_상점 = true;
	public static boolean 로그_개인상점 = true;
	public static boolean 로그_교환 = true;
	public static boolean 로그_인첸 = true;

	private void logSwitch(L1PcInstance gm, String param) {
		// TODO 자동 생성된 메소드 스텁
		try {
			StringTokenizer st = new StringTokenizer(param);
			String type = st.nextToken();
			if (type.equalsIgnoreCase("드랍")) {
				로그_드랍 = !로그_드랍;
				gm.sendPackets(new S_SystemMessage("로그 드랍 : " + 로그_드랍), true);
			} else if (type.equalsIgnoreCase("픽업")) {
				로그_픽업 = !로그_픽업;
				gm.sendPackets(new S_SystemMessage("로그 픽업 : " + 로그_픽업), true);
			} else if (type.equalsIgnoreCase("용해")) {
				로그_용해 = !로그_용해;
				gm.sendPackets(new S_SystemMessage("로그 용해 : " + 로그_용해), true);
			} else if (type.equalsIgnoreCase("상점")) {
				로그_상점 = !로그_상점;
				gm.sendPackets(new S_SystemMessage("로그 상점 : " + 로그_상점), true);
			} else if (type.equalsIgnoreCase("개인상점")) {
				로그_개인상점 = !로그_개인상점;
				gm.sendPackets(new S_SystemMessage("로그 개인상점 : " + 로그_개인상점),
						true);
			} else if (type.equalsIgnoreCase("교환")) {
				로그_교환 = !로그_교환;
				gm.sendPackets(new S_SystemMessage("로그 교환 : " + 로그_교환), true);
			} else if (type.equalsIgnoreCase("인첸")) {
				로그_인첸 = !로그_인첸;
				gm.sendPackets(new S_SystemMessage("로그 인첸 : " + 로그_인첸), true);
			}
		} catch (Exception e) {
			gm.sendPackets(new S_SystemMessage(
					".로그기록 [드랍, 픽업, 용해, 상점, 개인상점, 교환, 인첸]"), true);
			// e.printStackTrace();
		}
	}

	public static int 환수율 = 95;

	private void 환수(L1PcInstance gm, String param) {
		// TODO 자동 생성된 메소드 스텁
		// TODO 자동 생성된 메소드 스텁
		try {
			StringTokenizer st = new StringTokenizer(param);
			int i = Integer.parseInt(st.nextToken(), 10);
			환수율 = i;
			gm.sendPackets(new S_SystemMessage("환수율 0." + 환수율 + "로 변경 되었습니다."));
		} catch (Exception e) {
			gm.sendPackets(new S_SystemMessage(".환수 [숫자]"));
		}
	}

	private void 포트변경(L1PcInstance gm, String param) {
		// TODO 자동 생성된 메소드 스텁
		try {
			/*
			 * StringTokenizer st = new StringTokenizer(param); int port =
			 * Integer.parseInt(st.nextToken(), 10); boolean check =
			 * xnetwork.Acceptor.ChangePort(port); if(check) gm.sendPackets(new
			 * S_SystemMessage(port+" 번호로 포트가 변경 되었습니다.")); else
			 */
			gm.sendPackets(new S_SystemMessage("포트 변경을 실패하였습니다."));
		} catch (Exception e) {
			gm.sendPackets(new S_SystemMessage(".포트변경 [Port]"));
		}
	}

	private void SprSpeed(L1PcInstance gm, String param) {
		// TODO 자동 생성된 메소드 스텁
		try {
			StringTokenizer st = new StringTokenizer(param);
			int spr = Integer.parseInt(st.nextToken(), 10);
			int speed1 = 0;
			int speed2 = 0;
			int speed3 = 0;
			int speed4 = 0;
			speed2 = SprTable.getInstance().getAttackSpeed(spr, 1);
			speed1 = SprTable.getInstance().getMoveSpeed(spr, 0);
			speed3 = SprTable.getInstance().getDirSpellSpeed(spr);
			speed4 = SprTable.getInstance().getNodirSpellSpeed(spr);
			gm.sendPackets(new S_SystemMessage("이동속도(" + spr + "): " + speed1));
			gm.sendPackets(new S_SystemMessage("공격속도(" + spr + "): " + speed2));
			gm.sendPackets(new S_SystemMessage("마법속도(" + spr + "): " + speed3));
			gm.sendPackets(new S_SystemMessage("버프속도(" + spr + "): " + speed4));
		} catch (Exception e) {
			gm.sendPackets(new S_SystemMessage(".액션속도 [Spr번호]"));
		}
	}
	   private void BJList(L1PcInstance gm, String param) {
	  	   try {
	  		int SearchCount = 0;
	  		gm.sendPackets(new S_SystemMessage("\\fY----------------------------------------------------"));
	  		for (L1PcInstance pc : L1World.getInstance().getAllPlayers()) {
	  			try {
	  				if (pc == null || pc.getNetConnection() == null || pc.noPlayerCK) {
	  					continue;
	  				}
	  				if(pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.BJBUFF)) {
	  					gm.sendPackets(new S_SystemMessage("\\aG["+pc.getName()+"]" ));
	  					SearchCount++;
	  				}
	  			} catch (Exception e) {
	  			}
	  		}
	  		gm.sendPackets(new S_SystemMessage("\\aD" + SearchCount + "명의 BJ가 방송중입니다."));
	  		gm.sendPackets(new S_SystemMessage("\\fY----------------------------------------------------"));
	  	} catch (Exception e) {
	  		gm.sendPackets(new S_SystemMessage(".방송리스트"));
	  	}
	  }

	private void clanMark(L1PcInstance gm, String param) {
		// TODO 자동 생성된 메소드 스텁
		try {
			StringTokenizer st = new StringTokenizer(param);
			String onoff = st.nextToken();
			if (onoff.equalsIgnoreCase("켬")) {
				gm.sendPackets(new S_문장주시(gm, 2, true), true);
				gm.sendPackets(new S_문장주시(gm, 0, true), true);
			} else if (onoff.equalsIgnoreCase("끔")) {
				gm.sendPackets(new S_문장주시(gm, 2, false), true);
				gm.sendPackets(new S_문장주시(gm, 1, false), true);
			} else {
				gm.sendPackets(new S_SystemMessage(".혈맹마크 [켬 / 끔]"));
				return;
			}
		} catch (Exception e) {
			gm.sendPackets(new S_SystemMessage(".혈맹마크 [켬 / 끔]"));
		}
	}

	private void mapBuff(L1PcInstance pc) {
		// TODO 자동 생성된 메소드 스텁
		try {
			SpecialEventHandler.getInstance().doMapBuf(pc);
		} catch (Exception e) {
			pc.sendPackets(new S_SystemMessage(".맵버프"));
		}
	}

	private void UserGiranTel(L1PcInstance pc, String param) {
		// TODO 자동 생성된 메소드 스텁
		try {
			StringTokenizer st = new StringTokenizer(param);
			String name = st.nextToken();
			L1PcInstance user = L1World.getInstance().getPlayer(name);
			if (user != null) {
				// user.setTelType(11);
				if (user instanceof L1RobotInstance) {
					L1RobotInstance rob = (L1RobotInstance) user;
					int[] loc = new int[3];
					switch (_random.nextInt(9)) {
					case 0:
						loc[0] = 32781;
						loc[1] = 32830;
						loc[2] = 622;
						break;
					case 1:
						loc[0] = 32774;
						loc[1] = 32839;
						loc[2] = 622;
						break;
					case 2:
						loc[0] = 32770;
						loc[1] = 32834;
						loc[2] = 622;
						break;
					case 3:
						loc[0] = 32756;
						loc[1] = 32826;
						loc[2] = 622;
						break;
					case 4:
						loc[0] = 32766;
						loc[1] = 32817;
						loc[2] = 622;
						break;
					case 5:
						loc[0] = 32781;
						loc[1] = 32815;
						loc[2] = 622;
						break;
					case 6:
						loc[0] = 32755;
						loc[1] = 32818;
						loc[2] = 622;
						break;
					case 7:
						loc[0] = 32752;
						loc[1] = 32846;
						loc[2] = 622;
						break;
					default:
						loc[0] = 32763;
						loc[1] = 32835;
						loc[2] = 622;
						break;
					}
					L1Teleport.로봇텔(rob, loc[0], loc[1], (short) loc[2], true);
				} else {
					user.감옥 = false;
					L1Teleport.teleport(user, 33437, 32812, (short) 4, 5, true); // /
																					// 가게될
																					// 지점
																					// (유저가떨어지는지점)
					// user.setTelType(77);
					// user.sendPackets(new S_SabuTell(user), true);
				}
			}
		} catch (Exception e) {
			pc.sendPackets(new S_SystemMessage(".마을 [케릭]"));
		}
	}
    private void GoldDragon(L1PcInstance gm){
   	 String locName = MapsTable.getInstance().getMapName(gm.getMapId());
   	 	try{
			 L1SpawnUtil.spawn2(gm.getX(), gm.getY(), (short) gm.getMapId(), 6801, 50, 3600*1000, 0);
			 L1SpawnUtil.spawn2(gm.getX(), gm.getY(), (short) gm.getMapId(), 6802, 50, 3600*1000, 0);
			 L1SpawnUtil.spawn2(gm.getX(), gm.getY(), (short) gm.getMapId(), 6801, 50, 3600*1000, 0);
			 L1SpawnUtil.spawn2(gm.getX(), gm.getY(), (short) gm.getMapId(), 6801, 50, 3600*1000, 0);
			 L1SpawnUtil.spawn2(gm.getX(), gm.getY(), (short) gm.getMapId(), 6802, 50, 3600*1000, 0);
			 L1SpawnUtil.spawn2(gm.getX(), gm.getY(), (short) gm.getMapId(), 6802, 50, 3600*1000, 0);
			 L1SpawnUtil.spawn2(gm.getX(), gm.getY(), (short) gm.getMapId(), 6801, 50, 3600*1000, 0);
			 L1SpawnUtil.spawn2(gm.getX(), gm.getY(), (short) gm.getMapId(), 6802, 50, 3600*1000, 0);
			 L1SpawnUtil.spawn2(gm.getX(), gm.getY(), (short) gm.getMapId(), 6801, 50, 3600*1000, 0);
			 L1SpawnUtil.spawn2(gm.getX(), gm.getY(), (short) gm.getMapId(), 6801, 50, 3600*1000, 0);
			 L1SpawnUtil.spawn2(gm.getX(), gm.getY(), (short) gm.getMapId(), 6802, 50, 3600*1000, 0);
			 L1SpawnUtil.spawn2(gm.getX(), gm.getY(), (short) gm.getMapId(), 6802, 50, 3600*1000, 0);
			 L1SpawnUtil.spawn2(gm.getX(), gm.getY(), (short) gm.getMapId(), 6801, 50, 3600*1000, 0);
			 L1SpawnUtil.spawn2(gm.getX(), gm.getY(), (short) gm.getMapId(), 6802, 50, 3600*1000, 0);
			 L1SpawnUtil.spawn2(gm.getX(), gm.getY(), (short) gm.getMapId(), 6801, 50, 3600*1000, 0);
			 L1SpawnUtil.spawn2(gm.getX(), gm.getY(), (short) gm.getMapId(), 6801, 50, 3600*1000, 0);
			 L1SpawnUtil.spawn2(gm.getX(), gm.getY(), (short) gm.getMapId(), 6802, 50, 3600*1000, 0);
			 L1SpawnUtil.spawn2(gm.getX(), gm.getY(), (short) gm.getMapId(), 6802, 50, 3600*1000, 0);
			 L1SpawnUtil.spawn2(gm.getX(), gm.getY(), (short) gm.getMapId(), 6801, 50, 3600*1000, 0);
			 L1SpawnUtil.spawn2(gm.getX(), gm.getY(), (short) gm.getMapId(), 6802, 50, 3600*1000, 0);
			 L1SpawnUtil.spawn2(gm.getX(), gm.getY(), (short) gm.getMapId(), 6801, 50, 3600*1000, 0);
			 L1SpawnUtil.spawn2(gm.getX(), gm.getY(), (short) gm.getMapId(), 6801, 50, 3600*1000, 0);
			 L1SpawnUtil.spawn2(gm.getX(), gm.getY(), (short) gm.getMapId(), 6802, 50, 3600*1000, 0);
			 L1SpawnUtil.spawn2(gm.getX(), gm.getY(), (short) gm.getMapId(), 6802, 50, 3600*1000, 0);
				L1World.getInstance().broadcastServerMessage("\\aI[이벤트 스폰]: 괴상한 산타클로스 무리("+locName+")");
				L1World.getInstance().broadcastPacketToAll(
				new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "\\fO[이벤트 스폰]: 괴상한 산타클로스 무리 ("+locName+")"), true);
				L1World.getInstance().broadcastPacketToAll(new S_Weather(3), true);
   	} catch (Exception e){
   		gm.sendPackets(new S_SystemMessage(".산타"));
   	}
   }
    private void GoldDragon2(L1PcInstance gm){
      	 String locName = MapsTable.getInstance().getMapName(gm.getMapId());
      	 	try{
   			 L1SpawnUtil.spawn2(gm.getX(), gm.getY(), (short) gm.getMapId(), 61207, 50, 3600*1000, 0);
   			 L1SpawnUtil.spawn2(gm.getX(), gm.getY(), (short) gm.getMapId(), 61207, 50, 3600*1000, 0);
   			 L1SpawnUtil.spawn2(gm.getX(), gm.getY(), (short) gm.getMapId(), 61207, 50, 3600*1000, 0);
   			 L1SpawnUtil.spawn2(gm.getX(), gm.getY(), (short) gm.getMapId(), 61207, 50, 3600*1000, 0);
   			 L1SpawnUtil.spawn2(gm.getX(), gm.getY(), (short) gm.getMapId(), 61207, 50, 3600*1000, 0);
   			 L1SpawnUtil.spawn2(gm.getX(), gm.getY(), (short) gm.getMapId(), 61207, 50, 3600*1000, 0);
   			 L1SpawnUtil.spawn2(gm.getX(), gm.getY(), (short) gm.getMapId(), 61207, 50, 3600*1000, 0);
   			 L1SpawnUtil.spawn2(gm.getX(), gm.getY(), (short) gm.getMapId(), 61207, 50, 3600*1000, 0);
   			 L1SpawnUtil.spawn2(gm.getX(), gm.getY(), (short) gm.getMapId(), 61207, 50, 3600*1000, 0);
   			 L1SpawnUtil.spawn2(gm.getX(), gm.getY(), (short) gm.getMapId(), 61207, 50, 3600*1000, 0);
   			 L1SpawnUtil.spawn2(gm.getX(), gm.getY(), (short) gm.getMapId(), 61207, 50, 3600*1000, 0);
   			 L1SpawnUtil.spawn2(gm.getX(), gm.getY(), (short) gm.getMapId(), 61207, 50, 3600*1000, 0);
   			 L1SpawnUtil.spawn2(gm.getX(), gm.getY(), (short) gm.getMapId(), 61207, 50, 3600*1000, 0);
   			 L1SpawnUtil.spawn2(gm.getX(), gm.getY(), (short) gm.getMapId(), 61207, 50, 3600*1000, 0);
   			 L1SpawnUtil.spawn2(gm.getX(), gm.getY(), (short) gm.getMapId(), 61207, 50, 3600*1000, 0);
   			 L1SpawnUtil.spawn2(gm.getX(), gm.getY(), (short) gm.getMapId(), 61207, 50, 3600*1000, 0);
   			 L1SpawnUtil.spawn2(gm.getX(), gm.getY(), (short) gm.getMapId(), 61207, 50, 3600*1000, 0);
   			 L1SpawnUtil.spawn2(gm.getX(), gm.getY(), (short) gm.getMapId(), 61207, 50, 3600*1000, 0);
   			 L1SpawnUtil.spawn2(gm.getX(), gm.getY(), (short) gm.getMapId(), 61207, 50, 3600*1000, 0);
   			 L1SpawnUtil.spawn2(gm.getX(), gm.getY(), (short) gm.getMapId(), 61207, 50, 3600*1000, 0);
   				L1World.getInstance().broadcastServerMessage("\\aI[이벤트 스폰]: 원숭이 왕 손오공 무리("+locName+")");
   				L1World.getInstance().broadcastPacketToAll(
   				new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "\\fO[이벤트 스폰]: 원숭이 왕 손오공 무리 ("+locName+")"), true);
   				L1World.getInstance().broadcastPacketToAll(new S_Weather(3), true);
      	} catch (Exception e){
      		gm.sendPackets(new S_SystemMessage(".손오공"));
      	}
      }
    
	private void maphack(L1PcInstance pc, String param) {
		try {
			StringTokenizer st = new StringTokenizer(param);
			String on = st.nextToken();
			if (on.equalsIgnoreCase("켬")) {
				pc.sendPackets(new S_Ability(3, true));
				pc.sendPackets(new S_SystemMessage("맵핵 : [켬]"));
			} else if (on.equals("끔")) {
				pc.sendPackets(new S_Ability(3, false));
				pc.sendPackets(new S_SystemMessage("맵핵 : [끔]"));
			}
		} catch (Exception e) {
			pc.sendPackets(new S_SystemMessage(".맵핵  [켬, 끔]"));
		}
	}

	private void 판매체크(L1PcInstance gm) {
		try {
			// TODO 자동 생성된 메소드 스텁
			if (sellShopNotice) {
				sellShopNotice = false;
			} else
				sellShopNotice = true;
			gm.sendPackets(new S_SystemMessage("상점 판매 체크 : " + sellShopNotice),
					true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void buyshop_user(L1PcInstance gm) {
		// TODO Auto-generated method stub
		try {
			boolean ck = NpcBuyShop.getInstance().BuyShop_Show_or_Delete(false);
			gm.sendPackets(new S_SystemMessage("유저 매입 상점 NPC >> "
					+ (ck ? "[켬]" : "[끔]")), true);
			// 매입상점 = !매입상점;
			// NpcBuyShop.getInstance().BuyShop_Show_or_Delete(매입상점);
			// gm.sendPackets(new S_SystemMessage("매입 상점 NPC >> "+(매입상점 ?
			// "[켬]":"[끔]")), true);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void buyshop(L1PcInstance gm) {
		// TODO Auto-generated method stub
		try {
			boolean ck = NpcBuyShop.getInstance().BuyShop_Show_or_Delete(true);
			gm.sendPackets(new S_SystemMessage("매입 상점 NPC >> "
					+ (ck ? "[켬]" : "[끔]")), true);
			// 매입상점 = !매입상점;
			// NpcBuyShop.getInstance().BuyShop_Show_or_Delete(매입상점);
			// gm.sendPackets(new S_SystemMessage("매입 상점 NPC >> "+(매입상점 ?
			// "[켬]":"[끔]")), true);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void sellshop(L1PcInstance gm) {
		// TODO Auto-generated method stub
		try {
			boolean ck = NpcTradeShop.getInstance().BuyShop_Show_or_Delete();
			gm.sendPackets(new S_SystemMessage("판매 상점 NPC >> "
					+ (ck ? "[켬]" : "[끔]")), true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void gamble(L1PcInstance gm) {
		// TODO Auto-generated method stub
		try {
			boolean ck = Gamble.get().Gamble_Show_or_Delete();
			gm.sendPackets(new S_SystemMessage("겜블 NPC >> "
					+ (ck ? "[켬]" : "[끔]")), true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void spawnmodel(L1PcInstance gm, String param) {
		StringTokenizer st = new StringTokenizer(param);
		int type = Integer.parseInt(st.nextToken(), 10);
		ModelSpawnTable.getInstance().insertmodel(gm, type);
		gm.sendPackets(new S_SystemMessage("불 넣었다"), true);
	}

	private void showHelp(L1PcInstance gm) {
		gm.sendPackets(new S_GMCommands(1), true);
	}

	private void 메모리반환(L1PcInstance gm) {
		System.out.println("강제로 가비지 처리를 진행 합니다.");
		System.gc();
		System.out.println("메모리 정리가 완료 되었습니다.");
	}

	private void 인형청소(L1PcInstance gm) {
		int count = 0;
		int ccount = 0;
		for (Object obj : L1World.getInstance().getObject()) {
			if (obj instanceof L1DollInstance) {
				L1DollInstance 인형 = (L1DollInstance) obj;
				if (인형.getMaster() == null) {
					count++;
					인형.deleteMe();
				} else if (((L1PcInstance) 인형.getMaster()).getNetConnection() == null) {
					ccount++;
					인형.deleteMe();
				}
			}
		}
		gm.sendPackets(new S_SystemMessage("인형청소 갯수 - 주인X: " + count
				+ "  주인접종: " + ccount), true);
	}

	private void 폰인증완료(L1PcInstance gm, String param) {
		try {
			StringTokenizer st = new StringTokenizer(param);
			String 이름 = st.nextToken();
			String 폰번호 = st.nextToken();

			L1PcInstance target = L1World.getInstance().getPlayer(이름);
			if (target == null) {
				gm.sendPackets(new S_SystemMessage(이름 + "는 접속중인 캐릭이 아닙니다."),
						true);
				return;
			}

			int phon = 0;
			try {
				phon = Integer.valueOf(폰번호);
				if (PhoneCheck.폰등록갯수(폰번호) >= 2) {
					gm.sendPackets(new S_SystemMessage(
							"이미 두개의 계정에 폰인증이 완료된 번호 입니다."));
					PhoneCheck.폰인증확인(gm, 폰번호);
					return;
				}
				PhoneCheck.remove(gm.getAccountName());
				PhoneCheck.removenocheck(gm.getAccountName());
				PhoneCheck.폰등록(폰번호, "GM에의해인증", target.getAccountName(), 1);
				// L1Teleport.teleport(target, 34060, 32282, (short) 4, 5,
				// true);
				gm.sendPackets(new S_SystemMessage(target.getName() + " ["
						+ target.getAccountName() + "]의 폰인증을 완료 하였습니다."));
			} catch (NumberFormatException e) {
				gm.sendPackets(new S_SystemMessage(".폰인증완료 [이름] [번호]"));
			} catch (Exception e) {
				gm.sendPackets(new S_SystemMessage(".폰인증완료 [이름] [번호]"));
			}
		} catch (Exception e) {
			gm.sendPackets(new S_SystemMessage(".폰인증완료 [이름] [번호]"));
		}
	}

	private void 폰인증시작(L1PcInstance gm) {
		try {
			Config.폰인증 = true;
			gm.sendPackets(new S_SystemMessage("폰인증 시스템 작동. 9레벨이 되면 감옥으로 텔됩니다."));
		} catch (Exception e) {
			gm.sendPackets(new S_SystemMessage(".폰인증종료"));
		}
	}

	private void 폰인증종료(L1PcInstance gm) {
		try {
			Config.폰인증 = false;
			gm.sendPackets(new S_SystemMessage(
					"폰인증 시스템을 중단합니다. 9레벨이 되어도 정상 이용됩니다."));
		} catch (Exception e) {
			gm.sendPackets(new S_SystemMessage(".폰인증종료"));
		}
	}

	private void 폰인증초기화(L1PcInstance gm) {
		PhoneCheck.clearnocheck();
		gm.sendPackets(new S_SystemMessage("폰인증 대기중인 케릭터를 초기화 시킵니다."));
	}

	private void 던전초기화(L1PcInstance gm, String param) {
		try {
			StringTokenizer st = new StringTokenizer(param);
			String pcName = st.nextToken();
			String dun = st.nextToken();
			L1PcInstance player = L1World.getInstance().getPlayer(pcName);
			if (player == null) {
				gm.sendPackets(
						new S_SystemMessage(pcName + "는 접속중인 캐릭이 아닙니다."), true);
				return;
			}
			Timestamp nowday = new Timestamp(System.currentTimeMillis());

			if (dun.equalsIgnoreCase("수상한감옥")) {
				player.set수상한감옥time(1);
				player.set수상한감옥day(nowday);
				player.getNetConnection().getAccount().updateDGTime();
			} else if (dun.equalsIgnoreCase("검은전함")) {
				player.set수상한천상계곡time(1);
				player.set수상한천상계곡day(nowday);
				player.getNetConnection().getAccount().updateDGTime();
			} else if (dun.equalsIgnoreCase("상아탑발록")) {
				player.setivorytime(1);
				player.setivoryday(nowday);
				player.getNetConnection().getAccount().updateDGTime();
			} else if (dun.equalsIgnoreCase("상아탑야히")) {
				player.setivoryyaheetime(1);
				player.setivoryyaheeday(nowday);
				player.getNetConnection().getAccount().updateDGTime();
			} else if (dun.equalsIgnoreCase("기란") || dun.equalsIgnoreCase("본던")) {
				player.setgirantime(1);
				player.setgiranday(nowday);
				player.getNetConnection().getAccount().updateDGTime();
			} else if (dun.equalsIgnoreCase("말던")) {
				player.set말던time(1);
				player.set말던day(nowday);
				player.setpc말던time(1);
				player.setpc말던day(nowday);
				player.getNetConnection().getAccount().updateDGTime();
			} else if (dun.equalsIgnoreCase("몽섬")) {
				player.set몽섬time(1);
				player.set몽섬day(nowday);
				player.getNetConnection().getAccount().updateDGTime();
			} else if (dun.equalsIgnoreCase("고무") || dun.equalsIgnoreCase("정무")) {
				player.set고무time(1);
				player.set고무day(nowday);
				player.getNetConnection().getAccount().updateDGTime();
			} else if (dun.equalsIgnoreCase("라던")) {
				player.setravatime(1);
				player.setravaday(nowday);
				player.getNetConnection().getAccount().updateDGTime();
			}

			player.sendPackets(new S_SystemMessage("GM 에 의해 " + dun
					+ " 시간이 초기화 되었습니다."));
			gm.sendPackets(new S_SystemMessage(player.getName() + "의 " + dun
					+ "시간을 초기화 시켰습니다."));
		} catch (Exception e) {
			gm.sendPackets(new S_SystemMessage(".던전초기화 [이름] [던전] 으로 입력."));
			gm.sendPackets(new S_SystemMessage(
					"상아탑발록 / 상아탑야히 / 기란,본던 / 용던,수던 / 몽섬 / 고무,정무 / 라던"));
		}
	}

	private void 전원초기화(L1PcInstance gm, String param) {
		try {
			StringTokenizer st = new StringTokenizer(param);
			//String dun = st.nextToken();
			Timestamp nowday = new Timestamp(System.currentTimeMillis());
			
			if (param.equalsIgnoreCase("수상한감옥")) {
				for(L1PcInstance apc : L1World.getInstance().getAllPlayers()){
				apc.set수상한감옥time(1);
				apc.set수상한감옥day(nowday);
				apc.getNetConnection().getAccount().updateDGTime();
				}
			} else if (param.equalsIgnoreCase("검은전함")) {
				for(L1PcInstance apc : L1World.getInstance().getAllPlayers()){
				apc.set수상한천상계곡time(1);
				apc.set수상한천상계곡day(nowday);
				apc.getNetConnection().getAccount().updateDGTime();
				}
			} else if (param.equalsIgnoreCase("상아탑발록")) {
				for(L1PcInstance apc : L1World.getInstance().getAllPlayers()){
				apc.setivorytime(1);
				apc.setivoryday(nowday);
				apc.getNetConnection().getAccount().updateDGTime();
				apc.sendPackets(new S_SystemMessage("GM 에 의해 " + param+ " 시간이 초기화 되었습니다."));
				}
			} else if (param.equalsIgnoreCase("상아탑야히")) {
				for(L1PcInstance apc : L1World.getInstance().getAllPlayers()){
				apc.setivoryyaheetime(1);
				apc.setivoryyaheeday(nowday);
				apc.getNetConnection().getAccount().updateDGTime();
				apc.sendPackets(new S_SystemMessage("GM 에 의해 " + param+ " 시간이 초기화 되었습니다."));
			}
			} else if (param.equalsIgnoreCase("기란") || param.equalsIgnoreCase("본던")) {
				for(L1PcInstance apc : L1World.getInstance().getAllPlayers()){
				apc.setgirantime(1);
				apc.setgiranday(nowday);
				apc.getNetConnection().getAccount().updateDGTime();
				apc.sendPackets(new S_SystemMessage("GM 에 의해 " + param+ " 시간이 초기화 되었습니다."));
			}
			} else if (param.equalsIgnoreCase("말던")) {
				for(L1PcInstance apc : L1World.getInstance().getAllPlayers()){
				apc.set말던time(1);
				apc.set말던day(nowday);
				apc.setpc말던time(1);
				apc.setpc말던day(nowday);
				apc.getNetConnection().getAccount().updateDGTime();
				apc.sendPackets(new S_SystemMessage("GM 에 의해 " + param+ " 시간이 초기화 되었습니다."));
			}
			} else if (param.equalsIgnoreCase("몽섬")) {
				for(L1PcInstance apc : L1World.getInstance().getAllPlayers()){
				apc.set몽섬time(1);
				apc.set몽섬day(nowday);
				apc.getNetConnection().getAccount().updateDGTime();
				apc.sendPackets(new S_SystemMessage("GM 에 의해 " + param+ " 시간이 초기화 되었습니다."));
			}
			} else if (param.equalsIgnoreCase("고무") || param.equalsIgnoreCase("정무")) {
				for(L1PcInstance apc : L1World.getInstance().getAllPlayers()){
				apc.set고무time(1);
				apc.set고무day(nowday);
				apc.getNetConnection().getAccount().updateDGTime();
				apc.sendPackets(new S_SystemMessage("GM 에 의해 " + param+ " 시간이 초기화 되었습니다."));
				}
			} else if (param.equalsIgnoreCase("라던")) {
				for(L1PcInstance apc : L1World.getInstance().getAllPlayers()){
				apc.setravatime(1);
				apc.setravaday(nowday);
				apc.getNetConnection().getAccount().updateDGTime();
				apc.sendPackets(new S_SystemMessage("GM 에 의해 " + param+ " 시간이 초기화 되었습니다."));
			}
			}
			gm.sendPackets(new S_SystemMessage("전체 유저의 " + param + "시간을 초기화 시켰습니다."));
		} catch (Exception e) {
			gm.sendPackets(new S_SystemMessage(".전원초기화 [던전] 으로 입력."));
			gm.sendPackets(new S_SystemMessage(
					"상아탑발록 / 상아탑야히 / 기란,본던 / 용던,수던 / 몽섬 / 고무,정무 / 라던"));
		}
	}
	 private void 룬스톤데페(L1PcInstance gm) { // 데페
  			L1ItemInstance item = null;
  			item = ItemTable.getInstance().createItem(7310);
  			L1World.getInstance().broadcastPacketToAll(new S_PacketBox
  				(S_PacketBox.GREEN_MESSAGE,"아덴 월드의 어느 용사가 " + item.getName() + " 를(을) 획득하였습니다."));
	}
	 private void 룬스톤카배(L1PcInstance gm) { // 데페
			L1ItemInstance item = null;
			item = ItemTable.getInstance().createItem(41148);
			L1World.getInstance().broadcastPacketToAll(new S_PacketBox
				(S_PacketBox.GREEN_MESSAGE,"아덴 월드의 어느 용사가 " + item.getName() + " 를(을) 획득하였습니다."));
	}
	 
	 private void 룬스톤디스(L1PcInstance gm) { // 데페
			L1ItemInstance item = null;
			item = ItemTable.getInstance().createItem(40222);
			L1World.getInstance().broadcastPacketToAll(new S_PacketBox
				(S_PacketBox.GREEN_MESSAGE,"아덴 월드의 어느 용사가 " + item.getName() + " 를(을) 획득하였습니다."));
	}
	 private void 룬스톤아머(L1PcInstance gm) { // 데페
			L1ItemInstance item = null;
			item = ItemTable.getInstance().createItem(60199);
			L1World.getInstance().broadcastPacketToAll(new S_PacketBox
				(S_PacketBox.GREEN_MESSAGE,"아덴 월드의 어느 용사가 " + item.getName() + " 를(을) 획득하였습니다."));
	}
    private void jakjak(L1PcInstance gm) { // 데페
    		L1ItemInstance item = null;
    		item = ItemTable.getInstance().createItem(7310);
			L1World.getInstance().broadcastPacketToAll(new S_PacketBox
			(S_PacketBox.GREEN_MESSAGE,"누군가가 샌드 웜의 모래주머니에서 " + item.getName() + " 를(을) 획득하였습니다."));
	}
   
    private void jakjak2(L1PcInstance gm) { // 아머
		L1ItemInstance item = null;
		item = ItemTable.getInstance().createItem(60199);
		L1World.getInstance().broadcastPacketToAll(new S_PacketBox
		(S_PacketBox.GREEN_MESSAGE,"누군가가 샌드 웜의 모래주머니에서 " + item.getName() + " 를(을) 획득하였습니다."));
	}
    
    private void jakjak3(L1PcInstance gm) { // 카배
		L1ItemInstance item = null;
		item = ItemTable.getInstance().createItem(41148);
		L1World.getInstance().broadcastPacketToAll(new S_PacketBox
		(S_PacketBox.GREEN_MESSAGE,"누군가가 에르자베의 알에서 " + item.getName() + " 를(을) 획득하였습니다."));
    }
    
    private void 에르카배(L1PcInstance gm, String param) {
		try {
			StringTokenizer tokenizer = new StringTokenizer(param);
			String pcName = tokenizer.nextToken();
			L1PcInstance target = null; // q
			target = L1World.getInstance().getPlayer(pcName);
			if (target != null) { // 타겟
				target.카배 = true;
			} else {
				gm.sendPackets(new S_SystemMessage("접속중이지 않는 유저 ID 입니다."), true);
			}
		} catch (Exception e) {
			gm.sendPackets(new S_SystemMessage(".에르카배 [획득대상] 으로 입력해 주세요."), true);
		}
	}
	
	private void 샌드아머(L1PcInstance gm, String param) {
		try {
			StringTokenizer tokenizer = new StringTokenizer(param);
			String pcName = tokenizer.nextToken();
			L1PcInstance target = null; // q
			target = L1World.getInstance().getPlayer(pcName);
			if (target != null) { // 타겟
				target.아머 = true;
			} else {
				gm.sendPackets(new S_SystemMessage("접속중이지 않는 유저 ID 입니다."), true);
			}
		} catch (Exception e) {
			gm.sendPackets(new S_SystemMessage(".샌드아머 [획득대상] 으로 입력해 주세요."), true);
		}
	}
	
	private void 샌드데페(L1PcInstance gm, String param) {
		try {
			StringTokenizer tokenizer = new StringTokenizer(param);
			String pcName = tokenizer.nextToken();
			L1PcInstance target = null; // q
			target = L1World.getInstance().getPlayer(pcName);
			if (target != null) { // 타겟
				target.데페 = true;
			} else {
				gm.sendPackets(new S_SystemMessage("접속중이지 않는 유저 ID 입니다."), true);
			}
		} catch (Exception e) {
			gm.sendPackets(new S_SystemMessage(".샌드데페 [획득대상] 으로 입력해 주세요."), true);
		}
	}
	
    private void dollvala(L1PcInstance gm) { // 발라인형
		L1ItemInstance item = null;
		item = ItemTable.getInstance().createItem(73004);
		L1World.getInstance().broadcastPacketToAll(new S_PacketBox(S_PacketBox.GREEN_MESSAGE,"누군가가 " + item.getName() + " 합성에 성공 하였습니다."));
    }
    private void dollfafu(L1PcInstance gm) { // 파푸인형
		L1ItemInstance item = null;
		item = ItemTable.getInstance().createItem(73002);
		L1World.getInstance().broadcastPacketToAll(new S_PacketBox(S_PacketBox.GREEN_MESSAGE,"누군가가 " + item.getName() + " 합성에 성공 하였습니다."));
    }
    private void dollanta(L1PcInstance gm) { // 안타인형
		L1ItemInstance item = null;
		item = ItemTable.getInstance().createItem(73001);
		L1World.getInstance().broadcastPacketToAll(new S_PacketBox(S_PacketBox.GREEN_MESSAGE,"누군가가 " + item.getName() + " 합성에 성공 하였습니다."));
    }
    private void dolllind(L1PcInstance gm) { // 린드인형
		L1ItemInstance item = null;
		item = ItemTable.getInstance().createItem(73003);
		L1World.getInstance().broadcastPacketToAll(new S_PacketBox(S_PacketBox.GREEN_MESSAGE,"누군가가 " + item.getName() + " 합성에 성공 하였습니다."));
    }
    
    private void dolldemon(L1PcInstance gm) { // 데몬인형
		L1ItemInstance item = null;
		item = ItemTable.getInstance().createItem(600246);
		L1World.getInstance().broadcastPacketToAll(new S_PacketBox(S_PacketBox.GREEN_MESSAGE,"누군가가 " + item.getName() + " 합성에 성공 하였습니다."));
    }
    private void dolldeath(L1PcInstance gm) { // 데스인형
		L1ItemInstance item = null;
		item = ItemTable.getInstance().createItem(600247);
		L1World.getInstance().broadcastPacketToAll(new S_PacketBox(S_PacketBox.GREEN_MESSAGE,"누군가가 " + item.getName() + " 합성에 성공 하였습니다."));
    }
    private void dolltarak(L1PcInstance gm) { // 타락인형
		L1ItemInstance item = null;
		item = ItemTable.getInstance().createItem(19005);
		L1World.getInstance().broadcastPacketToAll(new S_PacketBox(S_PacketBox.GREEN_MESSAGE,"누군가가 " + item.getName() + " 합성에 성공 하였습니다."));
    }
    
    private void dollkurts(L1PcInstance gm) { // 커츠인형
		L1ItemInstance item = null;
		item = ItemTable.getInstance().createItem(19009);
		L1World.getInstance().broadcastPacketToAll(new S_PacketBox(S_PacketBox.GREEN_MESSAGE,"누군가가 " + item.getName() + " 합성에 성공 하였습니다."));
    }
    
    private void dollbapho(L1PcInstance gm) { // 바포인형
		L1ItemInstance item = null;
		item = ItemTable.getInstance().createItem(19007);
		L1World.getInstance().broadcastPacketToAll(new S_PacketBox(S_PacketBox.GREEN_MESSAGE,"누군가가 " + item.getName() + " 합성에 성공 하였습니다."));
    }
    
    private void dollice(L1PcInstance gm) { // 얼녀인형
		L1ItemInstance item = null;
		item = ItemTable.getInstance().createItem(19008);
		L1World.getInstance().broadcastPacketToAll(new S_PacketBox(S_PacketBox.GREEN_MESSAGE,"누군가가 " + item.getName() + " 합성에 성공 하였습니다."));
    }
	private void 맵정리(L1PcInstance gm) {
		int cnt = 0;
		for (L1Object obj : L1World.getInstance().getObject()) {
			if (obj instanceof L1MonsterInstance) {
				L1MonsterInstance mon = (L1MonsterInstance) obj;
				if(mon.getMapId() == gm.getMapId()){
				mon.die(gm);
				cnt++;
				}
			}

		}
		gm.sendPackets(new S_SystemMessage("몬스터 " + cnt + "마리를 죽였습니다."), true);
	}
	private void 전체정리(L1PcInstance gm) {
		int cnt = 0;
		for (L1Object obj : L1World.getInstance().getObject()) {
			if (obj instanceof L1MonsterInstance) {
				L1MonsterInstance mon = (L1MonsterInstance) obj;
				mon.die(gm);
				cnt++;
			}

		}
		gm.sendPackets(new S_SystemMessage("몬스터 " + cnt + "마리를 죽였습니다."), true);
	}
    private void AllPlayerList(L1PcInstance gm, String param) {
  	   try {
  		int SearchCount = 0;
  		AutoShopManager shopManager = AutoShopManager.getInstance();
  		AutoShop autoshop = null;

  		gm.sendPackets(new S_SystemMessage("\\fY----------------------------------------------------"));
  		for (L1PcInstance pc : L1World.getInstance().getAllPlayers()) {
  			try {
  				if (pc == null || pc.getNetConnection() == null || pc.noPlayerCK || pc.isPrivateShop()) {
  					continue;
  				}
  				autoshop = shopManager.getShopPlayer(pc.getName());
  				if (!pc.noPlayerCK && autoshop == null) {
  					gm.sendPackets(new S_SystemMessage
  							("\\aIC[" + pc.getName() + "] A[" + pc.getAccountName()+"] L[" + pc.getLevel() + "] AC["+ pc.getAC().getAc()+"] " +
  									"AG["+ pc.getAge()+"]"));
  					SearchCount++;
  				}
  			} catch (Exception e) {
  			}
  		}
  		gm.sendPackets(new S_SystemMessage("\\aD" + SearchCount + "명의 유저가 겜중입니다."));
  		gm.sendPackets(new S_SystemMessage("\\aEC[케릭] A[계정] L[레벨] AC[방어] AG[나이]"));
  		gm.sendPackets(new S_SystemMessage("\\fY----------------------------------------------------"));
  	} catch (Exception e) {
  		gm.sendPackets(new S_SystemMessage(".온라인"));
  	}
  }
	private void ban(L1PcInstance gm, String param) {
		try {
			StringTokenizer st = new StringTokenizer(param);
			int type = Integer.parseInt(st.nextToken(), 10);
			String account = st.nextToken();
			IpTable iptable = IpTable.getInstance();
			if (account.equalsIgnoreCase("")) {
				gm.sendPackets(new S_SystemMessage(".벤 [1~4] [IP or 계정]"), true);
				gm.sendPackets(new S_SystemMessage("1 = 계정벤 / 3 = 계정벤 해제"),
						true);
				gm.sendPackets(new S_SystemMessage("2 = IP벤 / 4 = IP벤 해제"),
						true);
				gm.sendPackets(new S_SystemMessage("5 = 광역IP벤 / 6 = 광역IP벤 해제"),
						true);
				gm.sendPackets(new S_SystemMessage(
						"7 = 광역IP벤+계정 / 8 = 광역IP벤+계정 해제"), true);
			}
			switch (type) {
			case 1: {// 계정벤
				Account.ban(account);
				gm.sendPackets(new S_SystemMessage(account + " 계정을 압류시켰습니다."),
						true);
			}
				break;
			case 2: {// 아이피벤
				iptable.banIp(account); // BAN 리스트에 IP를 추가한다.
				gm.sendPackets(new S_SystemMessage(account + " IP를 차단 시킵니다."),
						true);
				// iptable.reload();
			}
				break;
			case 3: {// 계정벤풀기
				Connection con = null;
				PreparedStatement pstm = null;
				try {
					con = L1DatabaseFactory.getInstance().getConnection();
					String sqlstr = "UPDATE accounts SET banned=0 WHERE login=?";
					pstm = con.prepareStatement(sqlstr);
					pstm.setString(1, account);
					pstm.executeUpdate();
				} catch (SQLException e) {
					_log.log(Level.SEVERE, e.getLocalizedMessage(), e);
				} finally {
					SQLUtil.close(pstm);
					SQLUtil.close(con);
				}
				gm.sendPackets(
						new S_SystemMessage(account + " 계정의 벤을 해제 시킵니다."), true);
			}
				break;
			case 4: {// 아이피벤풀기
				iptable.liftBanIp(account);
				// iptable.reload();
			}
				break;
			case 5: {// 광역아이피벤
				if (account.lastIndexOf(".") + 1 != account.length()) {
					gm.sendPackets(new S_SystemMessage(
							"123.456.789.  <-- 형식으로 입력하여주세요"), true);
					return;
				}
				for (int i = 1; i <= 255; i++) {
					iptable.banIp(account + i); // BAN 리스트에 IP를 추가한다.
				}
				gm.sendPackets(new S_SystemMessage(account
						+ "1~255 IP를 차단 시킵니다."), true);
			}
				break;
			case 6: {// 광역아이피벤 풀기
				if (account.lastIndexOf(".") + 1 != account.length()) {
					gm.sendPackets(new S_SystemMessage(
							"123.456.789.  <-- 형식으로 입력하여주세요"), true);
					return;
				}
				for (int i = 1; i <= 255; i++) {
					iptable.liftBanIp(account + i); // BAN 리스트에 IP를 삭제한다.
				}
				gm.sendPackets(new S_SystemMessage(account
						+ "1~255 IP를 해제 시킵니다."), true);
			}
				break;
			case 7: {// 광역아이피+계정 벤
				if (account.lastIndexOf(".") + 1 != account.length()) {
					gm.sendPackets(new S_SystemMessage(
							"123.456.789.  <-- 형식으로 입력하여주세요"), true);
					return;
				}
				Connection con = null;
				PreparedStatement pstm = null;
				try {
					con = L1DatabaseFactory.getInstance().getConnection();
					for (int i = 1; i <= 255; i++) {
						iptable.banIp(account + i); // BAN 리스트에 IP를 추가한다.
						String sqlstr = "UPDATE accounts SET banned=1 WHERE ip=?";
						pstm = con.prepareStatement(sqlstr);
						pstm.setString(1, account + i);
						pstm.executeUpdate();
						SQLUtil.close(pstm);
					}
				} catch (SQLException e) {
					_log.log(Level.SEVERE, e.getLocalizedMessage(), e);
				} finally {
					SQLUtil.close(pstm);
					SQLUtil.close(con);
				}
				gm.sendPackets(new S_SystemMessage(account
						+ "1~255 IP 차단과 해당 계정을 압류시킵니다."), true);
			}
				break;
			case 8: {// 광역아이피+계정 벤풀기
				if (account.lastIndexOf(".") + 1 != account.length()) {
					gm.sendPackets(new S_SystemMessage(
							"123.456.789.  <-- 형식으로 입력하여주세요"), true);
					return;
				}
				Connection con = null;
				PreparedStatement pstm = null;
				try {
					con = L1DatabaseFactory.getInstance().getConnection();
					for (int i = 1; i <= 255; i++) {
						iptable.liftBanIp(account + i); // BAN 리스트에 IP를 삭제한다.
						String sqlstr = "UPDATE accounts SET banned=0 WHERE ip=?";
						pstm = con.prepareStatement(sqlstr);
						pstm.setString(1, account + i);
						pstm.executeUpdate();
						SQLUtil.close(pstm);
					}
				} catch (SQLException e) {
					_log.log(Level.SEVERE, e.getLocalizedMessage(), e);
				} finally {
					SQLUtil.close(pstm);
					SQLUtil.close(con);
				}
				gm.sendPackets(new S_SystemMessage(account
						+ "1~255 IP와 계정을 해제 시킵니다."), true);
			}
				break;
			}
		} catch (Exception e) {
			gm.sendPackets(new S_SystemMessage(".벤 [1~4] [IP or 계정]"), true);
			gm.sendPackets(new S_SystemMessage("1 = 계정벤 / 3 = 계정벤 해제"), true);
			gm.sendPackets(new S_SystemMessage("2 = IP벤 / 4 = IP벤 해제"), true);
			gm.sendPackets(new S_SystemMessage("5 = 광역IP벤 / 6 = 광역IP벤 해제"),
					true);
			gm.sendPackets(
					new S_SystemMessage("7 = 광역IP벤+계정 / 8 = 광역IP벤+계정 해제"), true);
		}
	}

	public void 고정인증(L1PcInstance gm, String param){
		try{
			StringTokenizer st = new StringTokenizer(param);
			String charname = st.nextToken();
			L1PcInstance target = L1World.getInstance().getPlayer(charname);
			Account account = Account.load(target.getAccountName());
			if(account.getPrcount() == 0){
				target.sendPackets(new S_ACTION_UI(S_ACTION_UI.신스킬, 6073), true);
				target.sendPackets(new S_ACTION_UI(6073, 172800, 4827, 5072), true);
				target.addDamageReductionByArmor(1);
				target.getSkillEffectTimerSet().setSkillEffect(L1SkillId.고정인증버프, 172800 * 1000);
				Account.repr(target.getAccountName());
				gm.sendPackets(new S_SystemMessage(gm, "[캐릭명: "+charname+"] [계정명:"+target.getAccountName()+"] 고정인증 완료"), true);
			}else{
				gm.sendPackets(new S_SystemMessage(gm, "이미 고정인증이 완료된 대상입니다."), true);
			}
			
		}catch (Exception e) {
			gm.sendPackets(new S_SystemMessage(gm, ".고정인증 [캐릭명]"), true);
		}
	}
	
	private void 텟트(L1PcInstance gm) {
		L1UltimateBattle up = UBTable.getInstance().getUb(1);
		up.start();
	}

	private void 마법속도(L1PcInstance gm) {
		if (마법속도체크) {
			마법속도체크 = false;
			gm.sendPackets(new S_SystemMessage("마법속도 체크 off"));
		} else {
			마법속도체크 = true;
			gm.sendPackets(new S_SystemMessage("마법속도 체크 on"));
		}
	}

	private void kick(L1PcInstance gm, String param) {
		try {
			StringTokenizer st = new StringTokenizer(param);
			String pcName = st.nextToken();
			int type = 0;
			type = Integer.parseInt(st.nextToken(), 10);
			IpTable iptable = IpTable.getInstance();
			if (pcName.equalsIgnoreCase("")) {
				gm.sendPackets(new S_SystemMessage(".압류 [이름] [1,2,3,4]"), true);
				gm.sendPackets(new S_SystemMessage("1 or 없음 = 단순 연결 종료"), true);
				gm.sendPackets(new S_SystemMessage("2 = 케릭터 벤"), true);
				gm.sendPackets(new S_SystemMessage("3 = 벤 해제"), true);
				gm.sendPackets(new S_SystemMessage("4 = 케릭,계정,아이피 모두 압류"), true);
				return;
			}
			L1PcInstance target = L1World.getInstance().getPlayer(pcName);
			if (target == null) {
				gm.sendPackets(new S_SystemMessage(
						"그러한 이름의 캐릭터는 월드내에는 존재하지 않습니다."), true);
				return;
			}
			switch (type) {
			case 1:// 일반튕기기
				target.sendPackets(new S_Disconnect(), true);
				target.getNetConnection().close();
				gm.sendPackets(new S_SystemMessage(pcName
						+ "유저를 서버와의 연결을 종료 시켰습니다."), true);
				break;
			case 2:// 케릭벤 시키기
				Connection con = null;
				PreparedStatement pstm = null;
				try {
					con = L1DatabaseFactory.getInstance().getConnection();
					pstm = con
							.prepareStatement("UPDATE characters SET Banned = 1 WHERE char_name= ?");
					pstm.setString(1, pcName);
					pstm.executeUpdate();
				} catch (Exception e) {

				} finally {
					SQLUtil.close(pstm);
					SQLUtil.close(con);
				}

				target.setBanned(true);
				target.sendPackets(new S_Disconnect(), true);
				target.getNetConnection().close();
				gm.sendPackets(
						new S_SystemMessage(pcName + "유저의 케릭터를 벤 시켰습니다."), true);
				break;
			case 3:// 케릭벤 풀기
				Connection con1 = null;
				PreparedStatement pstm1 = null;
				try {
					con1 = L1DatabaseFactory.getInstance().getConnection();
					pstm1 = con1
							.prepareStatement("UPDATE characters SET Banned = 0 WHERE char_name= ?");
					pstm1.setString(1, pcName);
					pstm1.executeUpdate();
				} catch (Exception e) {

				} finally {
					SQLUtil.close(pstm1);
					SQLUtil.close(con1);
				}
				gm.sendPackets(new S_SystemMessage(pcName
						+ "유저의 벤 상태를 해제 시켰습니다."), true);
				break;
			case 4:// 모든것을 압류
				Account.ban(target.getAccountName());
				gm.sendPackets(new S_SystemMessage(target.getAccountName()
						+ " 계정을 압류시켰습니다."), true);
				iptable.banIp(target.getNetConnection().getIp()); // BAN 리스트에
																	// IP를 추가한다.
				// iptable.reload();
				gm.sendPackets(new S_SystemMessage(target.getNetConnection()
						.getIp() + " IP를 차단 시킵니다."), true);
				Connection con2 = null;
				PreparedStatement pstm2 = null;
				try {
					con2 = L1DatabaseFactory.getInstance().getConnection();
					pstm2 = con2
							.prepareStatement("UPDATE characters SET Banned = 1 WHERE char_name= ?");
					pstm2.setString(1, pcName);
					pstm2.executeUpdate();
				} catch (Exception e) {

				} finally {
					SQLUtil.close(pstm2);
					SQLUtil.close(con2);
				}

				target.setBanned(true);
				target.sendPackets(new S_Disconnect(), true);
				target.getNetConnection().close();
				gm.sendPackets(
						new S_SystemMessage(pcName + "유저의 케릭터를 벤 시켰습니다."), true);
				break;
			default:
				target.sendPackets(new S_Disconnect(), true);
				target.getNetConnection().close();
				gm.sendPackets(new S_SystemMessage(pcName
						+ "유저를 서버와의 연결을 종료 시켰습니다."), true);
				break;
			}
		} catch (Exception e) {
			gm.sendPackets(new S_SystemMessage(".추방 [이름] [1,2,3]"), true);
			gm.sendPackets(new S_SystemMessage("1 or 없음 = 단순 연결 종료"), true);
			gm.sendPackets(new S_SystemMessage("2 = 케릭터 벤"), true);
			gm.sendPackets(new S_SystemMessage("3 = 벤 해제"), true);
			gm.sendPackets(new S_SystemMessage("4 = 케릭,계정,아이피 모두 압류"), true);
		}
	}

	private static Random _random = new Random(System.nanoTime());
	// 라던
	// 용계
	// 화둥
	private static final String 멘트[] = { "손", "손!", "손~", "손!!", "손~!", "발", "발~", "손~~", "발" };

	class 로봇부처핸섬 implements Runnable {
		public 로봇부처핸섬() {
		}

		public void run() {
			try {
				for (L1RobotInstance rob : L1World.getInstance().getAllRobot()) {
					int ran = _random.nextInt(100) + 1;
					if (ran < 50) {
						continue;
					}
					Thread.sleep(_random.nextInt(100) + 50);
					for (L1PcInstance listner : L1World.getInstance()
							.getAllPlayers()) {
						listner.sendPackets(
								new S_ChatPacket(rob, 멘트[_random.nextInt(멘트.length)], Opcodes.S_MESSAGE, 3), true);
					}
				}
			} catch (Exception e) {
			}
		}
	}

	public static void HtmlCheck(String html) {
		for (L1PcInstance pc : L1World.getInstance().getAllPlayers()) {
			pc.sendPackets(new S_NPCTalkReturn(pc.getId(), html));
		}
	}

	private void 버그(L1PcInstance gm, String param) {
		StringTokenizer tokenizer = new StringTokenizer(param);
		int id = Integer.parseInt(tokenizer.nextToken(), 10);
		for (L1Object obj : L1World.getInstance().getVisibleObjects(gm, 10)) {
			if (obj instanceof L1NpcInstance) {
				L1NpcInstance npc = (L1NpcInstance) obj;
				if (npc.getNpcId() == 45601) {
					S_DRAGONPERL gfx2 = new S_DRAGONPERL(npc.getId(), id);
					Broadcaster.broadcastPacket(npc, gfx2, true);
				}
			}
		}
	}

	private void 버그2(L1PcInstance gm, String param) {
		StringTokenizer tokenizer = new StringTokenizer(param);
		int id = Integer.parseInt(tokenizer.nextToken(), 10);
		for (L1Object obj : L1World.getInstance().getVisibleObjects(gm, 10)) {
			if (obj instanceof L1NpcInstance) {
				L1NpcInstance npc = (L1NpcInstance) obj;
				if (npc.getNpcId() == 45601) {
					npc.getMoveState().setMoveSpeed(id);
				}
			}
		}
	}
	private void 태클버그(L1PcInstance gm, String param) {
		try {
			StringTokenizer tok = new StringTokenizer(param);
			int id = Integer.parseInt(tok.nextToken(), 10);
			
			L1BugBearRace.getInstance()._tacklebug = id-1;
		} catch (Exception e) {
			gm.sendPackets(new S_SystemMessage(".쿵 [선수번호]"), true);
		}
	}
	private void html(L1PcInstance gm, String param) {
		try {
			StringTokenizer tokenizer = new StringTokenizer(param);
			String htmlid = tokenizer.nextToken();
			for (L1PcInstance pc : L1World.getInstance().getAllPlayers()) {
				pc.sendPackets(new S_NPCTalkReturn(pc.getId(), htmlid));
			}
		} catch (Exception e) {
			gm.sendPackets(new S_SystemMessage(
					".서버종료 지금, 취소, 시간 [초]  라고 입력해 주세요. "));
		}
	}
	private void 던전축복(L1PcInstance gm, String param){
		// TODO 자동 생성된 메소드 스텁
		try {
			StringTokenizer st = new StringTokenizer(param);
			String 던전이름 = st.nextToken();
			if (던전이름.equalsIgnoreCase("기감")) {
				GidunBlessController.getInstance().isGmOpen = true;
				gm.sendPackets(new S_SystemMessage("기감 축복 발사 완료."), true);
			} else if (던전이름.equalsIgnoreCase("잊섬")) {
				LadunBlessController.getInstance().isGmOpen = true;
				gm.sendPackets(new S_SystemMessage("잊섬 축복 발사 완료."), true);
			} else if (던전이름.equalsIgnoreCase("오만")) {
				OmanBlessController.getInstance().isGmOpen = true;
				gm.sendPackets(new S_SystemMessage("오만 축복 발사 완료."), true);
			} else if (던전이름.equalsIgnoreCase("정상")) {
				GodeBlessController.getInstance().isGmOpen = true;
				gm.sendPackets(new S_SystemMessage("정상 축복 발사 완료."), true);
			} else if (던전이름.equalsIgnoreCase("용던")) {
				MaldunBlessController.getInstance().isGmOpen = true;
				gm.sendPackets(new S_SystemMessage("용던 축복 발사 완료."), true);
			}
		} catch (Exception e) {
			gm.sendPackets(new S_SystemMessage(".축복 [던전이름] (기감/잊섬/오만/정상/말던)"))	;
		}
	}
	private void 서버종료(L1PcInstance gm, String param) {
		try {
			StringTokenizer tokenizer = new StringTokenizer(param);
			String type = tokenizer.nextToken();
			if (type.equalsIgnoreCase("지금")) {
				GameServer.getInstance().shutdown();
				GameServer.getInstance().shutdownWithCountdown(5);
			} else if (type.equalsIgnoreCase("취소")) {
				GameServer.getInstance().abortShutdown();
			} else if (type.equalsIgnoreCase("시간")) {
				int secc = Integer.parseInt(tokenizer.nextToken(), 10);
				GameServer.getInstance().abortShutdown();
				GameServer.getInstance().shutdownWithCountdown(secc);
			}
		} catch (Exception e) {
			gm.sendPackets(new S_SystemMessage(
					".서버종료 지금, 취소, 시간 [초]  라고 입력해 주세요. "));
		}
	}

	private void 버경(L1PcInstance gm, String param) {
		StringTokenizer tokenizer = new StringTokenizer(param);
		int oder = Integer.parseInt(tokenizer.nextToken(), 10);
		int cnt = Integer.parseInt(tokenizer.nextToken(), 10);
		try {
			L1BugBearRace.getInstance().addBetting(oder, cnt);
			gm.sendPackets(new S_SystemMessage(oder + "번째 티켓 " + cnt + "개 구입!! "), true);
		} catch (Exception e) {
			gm.sendPackets(new S_SystemMessage(".버경 [티켓번호(오더번호)0~5] [갯수]"),
					true);
		}
	}

	private void 시바(L1PcInstance gm, String param) {
		StringTokenizer tokenizer = new StringTokenizer(param);
		int id = Integer.parseInt(tokenizer.nextToken(), 10);
		try {
			for (L1Object obj : L1World.getInstance().getVisibleObjects(gm, 10)) {
				if (obj instanceof L1NpcInstance) {
					L1NpcInstance pc = (L1NpcInstance) obj;
					S_DoActionGFX gfx2 = new S_DoActionGFX(pc.getId(), id);
					Broadcaster.broadcastPacket(pc, gfx2, true);
				}
			}

		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	private void 엔샵(L1PcInstance gm) {
		try {
			/*
			 * Connection con = null; PreparedStatement pstm = null; ResultSet
			 * rs = null; try { con =
			 * L1DatabaseFactory.getInstance().getConnection(); pstm =
			 * con.prepareStatement("SELECT * FROM shop_npc WHERE npc_id=?");
			 * L1Shop shop = null; pstm.setInt(1, npcId); rs =
			 * pstm.executeQuery(); shop = loadShop(npcId, rs);
			 * _npcShops.put(npcId, shop); } catch (SQLException e) {
			 * _log.log(Level.SEVERE, e.getLocalizedMessage(), e); } finally {
			 * SQLUtil.close(rs, pstm, con); }
			 */

		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@SuppressWarnings("deprecation")
	private void checktime(L1PcInstance pc) {
		try {
			long nowtime = System.currentTimeMillis();
			Timestamp nowstamp = new Timestamp(nowtime);
			int girantime = 0;
			int girantemp = 18000;
			int giranh = 0;
			int girans = 0;
			int giranm = 0;

			int ivorytime = 0;
			int ivorytemp = 3600;
			int ivoryh = 0;
			int ivorys = 0;
			int ivorym = 0;

			if (pc.getgiranday() != null
					&& pc.getgiranday().getDate() == nowstamp.getDate()
					&& pc.getgirantime() != 0) {
				girantime = pc.getgirantime();
				girans = (girantemp - girantime) % 60;
				giranm = (girantemp - girantime) / 60 % 60;
				giranh = (girantemp - girantime) / 60 / 60;// 시간
			} else {
				giranh = 5;
				giranm = 0;
				girans = 0;
			}
			if (pc.getivoryday() != null
					&& pc.getivoryday().getDate() == nowstamp.getDate()
					&& pc.getivorytime() != 0) {
				ivorytime = pc.getivorytime();
				ivorys = (ivorytemp - ivorytime) % 60;
				ivorym = (ivorytemp - ivorytime) / 60 % 60;
				ivoryh = (ivorytemp - ivorytime) / 60 / 60;// 시간
			} else {
				ivoryh = 1;
				ivorym = 0;
				ivorys = 0;
			}
			pc.sendPackets(new S_SystemMessage("기란감옥 : " + giranh + "시간 "
					+ giranm + "분 " + girans + "초"), true);
			pc.sendPackets(new S_SystemMessage("상아탑 : " + ivoryh + "시간 "
					+ ivorym + "분 " + ivorys + "초"), true);
		} catch (Exception e) {
		}
	}

	private void 기감(L1PcInstance gm) {
		try {
			for (L1PcInstance allpc : L1World.getInstance().getAllPlayers()) {
				allpc.setgirantime(0);
			}
			Connection con2 = null;
			PreparedStatement pstm2 = null;
			try {
				con2 = L1DatabaseFactory.getInstance().getConnection();
				pstm2 = con2
						.prepareStatement("UPDATE characters SET GDGTime = null");
				pstm2.executeUpdate();
				gm.sendPackets(new S_SystemMessage("모든 유저의 기감시간을 초기화 했습니다."),
						true);
			} catch (Exception e) {

			} finally {
				SQLUtil.close(pstm2);
				SQLUtil.close(con2);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	private static Map<Integer, String> _lastCommands = new HashMap<Integer, String>();

	private void redo(L1PcInstance pc, String arg) {
		try {
			String lastCmd = _lastCommands.get(pc.getId());
			if (arg.isEmpty()) {
				pc.sendPackets(new S_SystemMessage("커맨드 " + lastCmd
						+ " 을(를) 재실행합니다."), true);
				handleCommands(pc, lastCmd);
			} else {
				StringTokenizer token = new StringTokenizer(lastCmd);
				String cmd = token.nextToken() + " " + arg;
				pc.sendPackets(new S_SystemMessage("커맨드 " + cmd
						+ " 을(를) 재실행합니다."), true);
				handleCommands(pc, cmd);
			}
		} catch (Exception e) {
			_log.log(Level.SEVERE, e.getLocalizedMessage(), e);
			pc.sendPackets(new S_SystemMessage(".재실행 커맨드에러"), true);
		}
	}

	private void standBy7(L1PcInstance gm, String param) {
		try {
			StringTokenizer tokenizer = new StringTokenizer(param);
			String pcName = tokenizer.nextToken();
			String 앙 = tokenizer.nextToken();
			String 응 = tokenizer.nextToken();
			L1PcInstance target = L1World.getInstance().getPlayer(pcName);
			if (target != null) {
				if (앙.equalsIgnoreCase("아덴")) {
					if (응.equalsIgnoreCase("켬")) {
						gm.sendPackets(new S_SystemMessage(target.getName()
								+ "님의 아덴 감시를 합니다."), true);
						GameServer.getInstance().addbug(target.getName());
					} else if (응.equalsIgnoreCase("끔")) {
						gm.sendPackets(new S_SystemMessage(target.getName()
								+ "님의 아덴 감시를 해제 합니다."), true);
						GameServer.getInstance().removebug(target.getName());
					}
				} else if (앙.equalsIgnoreCase("방어")) {
					if (응.equalsIgnoreCase("켬")) {
						gm.sendPackets(new S_SystemMessage(target.getName()
								+ "님의 방어구 감시를 합니다."), true);
						GameServer.getInstance().addbug(target.getName());
					} else if (응.equalsIgnoreCase("끔")) {
						gm.sendPackets(new S_SystemMessage(target.getName()
								+ "님의 방어구 감시를 해제 합니다."), true);
						GameServer.getInstance().removebug(target.getName());
					}
				} else {
					gm.sendPackets(new S_SystemMessage(
							".버그 [케릭명] [아덴/방어] [켬/끔]"), true);
				}
			} else {
				gm.sendPackets(new S_SystemMessage("월드내에 그런케릭터는 없습니다."), true);
			}
		} catch (Exception eee) {
			gm.sendPackets(new S_SystemMessage(".버그 [케릭명] [아덴/방어] [켬/끔]"), true);
		}
	}

	private void standBy8(L1PcInstance gm, String param) {
		try {
			StringTokenizer tokenizer = new StringTokenizer(param);
			String pcName = tokenizer.nextToken();
			L1PcInstance target = L1World.getInstance().getPlayer(pcName);
			if (target != null) {
				target.getNetConnection().패킷로그 = true;
				gm.sendPackets(new S_SystemMessage(target.getName()
						+ "님의 로그를 기록합니다."), true);
				gm.sendPackets(new S_SystemMessage(target.getNetConnection()
						.getIp() + "로 접속하는 모든 케릭들은 자동 저장됩니다."), true);
				GameServer.getInstance().addipl(
						target.getNetConnection().getIp());
			} else {
				gm.sendPackets(new S_SystemMessage("월드내에 그런케릭터는 없습니다."), true);
			}
		} catch (Exception eee) {
			gm.sendPackets(new S_SystemMessage(".로그 [케릭명]"), true);
		}
	}

	// 1원 많이 깔기
	/*
	 * private void standBy6(L1PcInstance gm, String param){ try{
	 * StringTokenizer tokenizer = new StringTokenizer(param); int id =
	 * Integer.parseInt(tokenizer.nextToken(), 10);
	 * GameServer.getInstance().아덴최저값=id; gm.sendPackets(new
	 * S_SystemMessage("아덴검사 최저값을 "+id+" 로 변경합니다."), true); }catch (Exception
	 * eee){ gm.sendPackets(new S_SystemMessage(".아덴값변경 [0~2000000000]"), true);
	 * } }
	 */
	// private void standBy9(L1PcInstance gm){
	// try{
	// gm.sendPackets(new S_SystemMessage("패킷 녹화 시작."));
	// Config.패킷로그 = false;
	// }catch (Exception eee){
	// gm.sendPackets(new S_SystemMessage("명령어를 재 입력 해 주세요."));
	// }
	// }
	private void returnEXP(L1PcInstance gm, String param) {
		try {
			StringTokenizer tokenizer = new StringTokenizer(param);
			String pcName = tokenizer.nextToken();
			L1PcInstance target = L1World.getInstance().getPlayer(pcName);
			if (target != null) {
				int oldLevel = target.getLevel();
				int needExp = ExpTable.getNeedExpNextLevel(oldLevel);
				int exp = 0;
				if (oldLevel >= 1 && oldLevel < 11) {
					exp = 0;
				} else if (oldLevel >= 11 && oldLevel < 45) {
					exp = (int) (needExp * 0.1);
				} else if (oldLevel == 45) {
					exp = (int) (needExp * 0.09);
				} else if (oldLevel == 46) {
					exp = (int) (needExp * 0.08);
				} else if (oldLevel == 47) {
					exp = (int) (needExp * 0.07);
				} else if (oldLevel == 48) {
					exp = (int) (needExp * 0.06);
				} else if (oldLevel >= 49) {
					exp = (int) (needExp * 0.05);
				}
				target.addExp(+exp);
				target.save();
				target.saveInventory();
			} else {
				gm.sendPackets(new S_SystemMessage(
						"그러한 이름의 캐릭터는 월드내에는 존재하지 않습니다."), true);
			}
		} catch (Exception e) {
			gm.sendPackets(new S_SystemMessage(".겸치복구 [캐릭터명]을 입력 해주세요."), true);
		}
	}

	private int parseNpcId(String nameId) {
		int npcid = 0;
		try {
			npcid = Integer.parseInt(nameId);
		} catch (NumberFormatException e) {
			npcid = NpcTable.getInstance().findNpcIdByNameWithoutSpace(nameId);
		}
		return npcid;
	}

	private void standBy80(L1PcInstance gm) {
		try {
			gm.sendPackets(new S_SystemMessage("컨피그를 리로드 하였습니다."), true);
			Config.load();
		} catch (Exception e) {
			gm.sendPackets(new S_SystemMessage(".컨피그로드 오류"), true);
		}
	}

	private void standBy82(L1PcInstance gm) {
		try {
			for (L1PcInstance pc : L1World.getInstance().getAllPlayers()) {
				if (pc.샌드백) {
					L1World.getInstance().removeObject(pc);
					L1World.getInstance().removeVisibleObject(pc);
					List<L1PcInstance> players = L1World.getInstance()
							.getRecognizePlayer(pc);
					if (players.size() > 0) {
						S_RemoveObject s_deleteNewObject = new S_RemoveObject(
								pc);
						for (L1PcInstance sendbag : players) {
							if (sendbag != null) {
								sendbag.getNearObjects().removeKnownObject(pc);
								// if(!L1Character.distancepc(user, this))
								sendbag.sendPackets(s_deleteNewObject);
							}
						}
					}
				}
			}
			gm.sendPackets(new S_SystemMessage("월드맵의 모든 샌드백을 삭제합니다."), true);
		} catch (Exception e) {
		}
	}

	private int ConvertToInt(String param) {
		int Temp = Integer.parseInt(param);
		int Value = (int) Temp;
		return Value;
	}
	
	private void 판매완료(L1PcInstance pc, String param){
		try{
			StringTokenizer st = new StringTokenizer(param);
			String num = st.nextToken();
			
			int id = ConvertToInt(num);	
			
			String StepNum = BoardTable.getInstance().getStep(id);
			String count = BoardTable.getInstance().getAdena(id);
			String BuyName = BoardTable.getInstance().getSendName(id);
			
			int adena = ConvertToInt(count);	
			
			L1PcInstance target = L1World.getInstance().getPlayer(BuyName);
			
			if(target == null){
				pc.sendPackets(new S_SystemMessage("구매신청자가 접속중이지 않습니다."));
				return;
			}
			
			if(!StepNum.equalsIgnoreCase("입금대기")){
				pc.sendPackets(new S_SystemMessage("입금대기 매물이 아닙니다."));
				return;
			}
			
			updateAdena(pc, id);
			BoardTable.reload();
			target.getInventory().storeItem(40308, adena);
			target.sendPackets(new S_SystemMessage(pc, "[아데나 중계소] 구매신청하신 아데나가 인벤토리에 지급되었습니다."));
			pc.sendPackets(new S_SystemMessage("완료등록: 매물번호("+id+") 구매신청자("+target.getName()+")"));
		} catch (Exception e) {
    		pc.sendPackets(new S_SystemMessage(".판매완료 [매물번호(숫자만기입)]"));
    	}
	}
	
	public void updateAdena(L1PcInstance pc, int id) {
		Connection con = null;
		PreparedStatement pstm = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con.prepareStatement("UPDATE board_user SET step=? WHERE id=?");
			pstm.setString(1, "판매완료");
			pstm.setInt(2, id);
			pstm.executeUpdate();
		} catch (Exception e) {
		} finally {
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
	}
	
	private void 이벤(L1PcInstance gm, String param) {
		try {

			StringTokenizer tokenizer = new StringTokenizer(param);
			String type = tokenizer.nextToken();
			String swich = tokenizer.nextToken();

			String msg1 = null;
			String msg2 = null;
			String msg3 = null;

			int npcid = 0;
			int x = 0;
			int y = 0;
			int m = 0;
			int h = 0;
			Timestamp DelTime = new Timestamp(
					System.currentTimeMillis() + 259200000);// 3일
			if (type.equalsIgnoreCase("스냅퍼")) {
				DelTime = new Timestamp(System.currentTimeMillis()
						+ ((long) 86400000 * (long) 60));// 60일
				npcid = 100670;
				x = 33438;
				y = 32805;
				m = 4;
				h = 2;
				msg1 = "안녕하세요 아덴월드 유저 여러분 스냅퍼의 특별한 제안! 강해지고 싶은 용사라면 이번 기회를 놓치지 마세요!";
				msg2 = "스냅퍼의 반지 이벤트는 총 60일이 진행되며 이벤트 시작 후 60일뒤 스냅퍼의 반지 이벤트는 종료됩니다.";
				msg3 = "스냅퍼의 반지 이벤트가 운영자에 의해 강제 종료 되었습니다.";
				/*
				 * }else if(type.equalsIgnoreCase("신기한반지")){ DelTime = new
				 * Timestamp(System.currentTimeMillis()+((long)86400000 *
				 * (long)60));//60일 npcid=4500163; x = 33438; y = 32805; m = 4;
				 * h = 2; msg1 =
				 * "안녕하세요 아덴월드 유저 여러분 신비한 마법 주간의 이벤트 신기한 반지 이벤트가 시작되었습니다."; msg2
				 * = "신기한 반지 이벤트는 총 60일이 진행되며 이벤트 시작 후 60일뒤 신기한 반지 이벤트는 종료됩니다.";
				 * msg3 = "신기한 반지 이벤트가 운영자에 의해 강제 종료 되었습니다.";
				 */} else if (type.equalsIgnoreCase("수렵")) {
				DelTime = new Timestamp(System.currentTimeMillis() + 604800000);// 7일
				npcid = 100324;
				x = 33445;
				y = 32800;
				m = 4;
				h = 6;
				msg1 = "안녕하세요 아덴월드 유저 여러분 아덴국왕의 수렵 포고령이 떨어졌습니다. 국왕의 수렵 사냥터로 이용하여 격돌의 바람으로 광폭해진 동물들을 처치하고 무기와 방어구를 강화할 수 있는 주문서와 경험치를 보상받으실수 있습니다..";
				msg2 = "수렵 이벤트는 총 7일간 진행되며 이벤트 시작 후 7일뒤 수렵 이벤트는 종료됩니다.";
				msg3 = "수렵 이벤트가 운영자에 의해 강제 종료 되었습니다.";
			} else if (type.equalsIgnoreCase("드큐")) {
				DelTime = new Timestamp(System.currentTimeMillis() + 604800000);// 7일
				npcid = 100375;
				x = 33449;
				y = 32802;
				m = 4;
				h = 6;
				msg1 = "안녕하세요 아덴월드 유저 여러분 신비한 마법 주간의 이벤트 판도라의 깜짝 선물 이벤트가 시작되었습니다.";
				msg2 = "판도라의 깜짝 선물 이벤트는 총 1주 동안 진행되며 이벤트 시작 후 1주 뒤 판도라의 깜쩍 선물은 종료됩니다.";
				msg3 = "판도라의 깜짝 선물 이벤트가 운영자에 의해 강제 종료 되었습니다.";
			} else if (type.equalsIgnoreCase("아툰")) {
				DelTime = new Timestamp(System.currentTimeMillis() + 604800000);// 7일
				npcid = 100213;
				x = 33449;
				y = 32817;
				m = 4;
				h = 6;
				msg1 = "수호기사여,렙업을 부탁해! 이벤트가 시작되었습니다. 레벨 75,레벨80이 되면 변신할 수 있는 켄라우헬,케레니스,군터,조우 등의 변신을 체험해볼 수 있는 이벤트이며 기란 마을 물약 상점 앞에 NPC가 배치되어 있습니다.";
				msg2 = "수호기사여,렙업을 부탁해! 이벤트는 총 7일간 진행되며 이벤트 시작 후 7일 뒤 수호기사여,렙업을 부탁해! 이벤트는 종료됩니다.";
				msg3 = "수호기사여,렙업을 부탁해! 이벤트가 운영자에 의해 강제 종료 되었습니다.";
			} else if (type.equalsIgnoreCase("룸티스")) {
				DelTime = new Timestamp(System.currentTimeMillis()
						+ ((long) 86400000 * (long) 3));// 3일
				npcid = 4500164;
				x = 33438;
				y = 32809;
				m = 4;
				h = 2;
				msg1 = "안녕하세요 아덴월드 유저 여러분 신비한 마법 주간의 이벤트 룸티스의 귀걸이 이벤트가 시작되었습니다.";
				msg2 = "룸티스의 귀걸이 이벤트는 총 3일(72시간)이 진행되며 이벤트 시작 후 72시간뒤 룸티스 귀걸이 이벤트는 종료됩니다.";
				msg3 = "룸티스 귀걸이 이벤트가 운영자에 의해 강제 종료 되었습니다.";
			} else if (type.equalsIgnoreCase("인나드릴")) {
				DelTime = new Timestamp(System.currentTimeMillis()
						+ ((long) 86400000 * (long) 60));// 60일
				npcid = 4500165;
				x = 33435;
				y = 32804;
				m = 4;
				h = 0;
				msg1 = "안녕하세요 아덴월드 유저 여러분 신비한 마법 주간의 이벤트 인나드릴 티셔츠 이벤트가 시작되었습니다.";
				msg2 = "인나드릴 티셔츠 이벤트는 총 60일이 진행되며 이벤트 시작 후 60일뒤 인나드릴 티셔츠 이벤트는 종료됩니다.";
				msg3 = "인나드릴 티셔츠 이벤트가 운영자에 의해 강제 종료 되었습니다.";
			} else if (type.equalsIgnoreCase("드래곤")) {
				DelTime = new Timestamp(System.currentTimeMillis()
						+ ((long) 86400000 * (long) 3));// 3일
				npcid = 4500167;
				x = 33438;
				y = 32811;
				m = 4;
				h = 2;
				msg1 = "안녕하세요 아덴월드 유저 여러분 린드비오르에 맞서 싸울 용사들을 위한 특별한 보물의 귀환!\n"
						+ "풍룡의 둥지에서 새로운 드래곤의 보물이 발견되었다. 아덴의 용사들이여! 신비로운 드래곤의 다이아몬드의 힘으로, 풍룡 린드비오르에 맞서 싸울 힘을 키워라!";
				msg2 = "드래곤의 보물상자 이벤트는 총 3일이 진행되며 이벤트 시작 후 3일뒤 드래곤의 보물상자 이벤트는 종료됩니다.";
				msg3 = "드래곤의 다이아몬드 상자 이벤트가 운영자에 의해 강제 종료 되었습니다.";
			} else if (type.equalsIgnoreCase("신묘한")) {
				DelTime = new Timestamp(System.currentTimeMillis() + 604800000);// 7일
				npcid = 7000043;
				x = 33433;
				y = 32808;
				m = 4;
				h = 6;
				msg1 = "안녕하세요 아덴월드 유저 여러분 신비한 마법 주간의 지나간 신묘년을 회상하는 이벤트가 시작되었습니다.";
				msg2 = "지나간 신묘년을 회상하는 이벤트는 총 2주간 진행되며 이벤트 시작 후 2주 뒤 신묘한 이벤트는 종료됩니다.";
				msg3 = "신묘한 이벤트가 운영자에 의해 강제 종료 되었습니다.";
			} else if (type.equalsIgnoreCase("할로윈")) {
				DelTime = new Timestamp(System.currentTimeMillis()
						+ ((long) 86400000 * (long) 3));// 3일
				npcid = 100672;
				x = 33447;
				y = 32809;
				m = 4;
				h = 5;
				msg1 = "올 해도 어김없이 할로윈 축제를 방해하기 위해 찾아온 '저주받은 할로윈 허수아비!' 할로윈 축제 모자를 쓰고 호박씨를 얻으며 바루와 함께 아덴 여기 저기 출몰하는 외다리 괴물들을 따끔하게 혼내주세요! 허수아비 바루가 알차게 준비한 선물이 여러분을 기다립니다.";
				msg2 = "할로윈 축제 이벤트는 총 3일 동안 진행되며 이벤트 시작 후 3일 뒤 할로윈 축제 이벤트는 종료됩니다.";
				msg3 = "할로윈 축제 이벤트가 운영자에 의해 강제 종료 되었습니다.";
			} else if (type.equalsIgnoreCase("바칸스")) {
				DelTime = new Timestamp(System.currentTimeMillis() + 604800000);// 7일
				npcid = 100396;
				x = 33427;
				y = 32793;
				m = 4;
				h = 4;
				msg1 = "안녕하세요 아덴 유저 여러분 바칸스의 시원 통쾌한 물약 이벤트가 시작되었습니다. 시원한 물약 3종과 특별한 버프효과를 가진 빙수 3종 세트를 기란마을의 바칸스가 판매하니 많은 이용 바랍니다.";
				msg2 = "바칸스의 시원 통쾌한 물약 이벤트 기간은 총 7일이며 7일 이후 자동 일괄 삭제 됩니다.";
				msg3 = "바칸스의 시원 통쾌한 물약 이벤트가 운영자에 의해 강제 종료 되었습니다.";
			} else if (type.equalsIgnoreCase("셀로브")) {
				DelTime = new Timestamp(System.currentTimeMillis() + 604800000);// 7일
				npcid = 100031;
				x = 32725;
				y = 32809;
				m = 99;
				h = 6;
				msg1 = "공포의 대상 셀로브가 과거의 영광을 되찾기 위해 돌아왔습니다.\n"
						+ "두 시간에 한번씩 나타나는 옛 말하는 섬의 셀로브와 그 일당들을 모두가 "
						+ "힘을 모아 공략에 성공하면 1회 이상 타격한 케릭터 모두에게 '옛 말하는 섬의 진귀한"
						+ "주머니'를 드립니다.";
				msg2 = "이벤트는 총 7일간 진행되며 이벤트가 끝나면 습득하셨던 진귀한 식량, 광분의 물약은"
						+ "인벤 및 창고에서 자동 삭제 됩니다.";
				msg3 = "옛 말하는 섬의 셀로브 이벤트가 운영자에 의해 강제 종료 되었습니다.";
			} else if (type.equalsIgnoreCase("판도라")) {
				DelTime = new Timestamp(System.currentTimeMillis()
						+ (long) ((long) 604800000 / 7 * 28));// 4주
				npcid = 100093;
				x = 33436;
				y = 32811;
				m = 4;
				h = 4;
				msg1 = "안녕하세요 아덴월드 유저 여러분 신비한 마법 주간의 이벤트 판도라의 유물 상자 이벤트가 시작되었습니다.";
				msg2 = "판도라의 유물 상자 이벤트는 총 4주 동안 진행되며 이벤트 시작 후 4주 뒤 판도라의 유물 상자는 종료됩니다.";
				msg3 = "판도라의 유물 상자 이벤트가 운영자에 의해 강제 종료 되었습니다.";
			} else if (type.equalsIgnoreCase("음유성")) {
				DelTime = new Timestamp(System.currentTimeMillis() + 604800000);// 7일
				npcid = 100412;
				x = 33445;
				y = 32800;
				m = 4;
				h = 6;
				msg1 = "안녕하세요 아덴 유저 여러분 아이온 음유성, 아덴 월드에서 노래하다!이벤트가 시작되었습니다.";
				msg2 = "음유성 이벤트 기간은 총 7일이며 7일 이후 자동 일괄 삭제 됩니다.";
				msg3 = "음유성 이벤트가 운영자에 의해 강제 종료 되었습니다.";
			} else if (type.equalsIgnoreCase("싸이")) {
				DelTime = new Timestamp(System.currentTimeMillis()
						+ ((long) 86400000 * (long) 14));// 14일
				npcid = 100429;
				x = 33446;
				y = 32802;
				m = 4;
				h = 5;
				msg1 = "월드스타 싸이가 리니지에 등장! 이번 이벤트의 주역은 전세계적으로 폭발적인 인기를 얻은 가수 싸이! 이벤트가 시작되었습니다.";
				msg2 = "싸이 이벤트 기간은 총 14일이며 14일 이후 자동 일괄 삭제 됩니다.";
				msg3 = "싸이 이벤트가 운영자에 의해 강제 종료 되었습니다.";
			} else if (type.equalsIgnoreCase("다이노스")) {
				DelTime = new Timestamp(System.currentTimeMillis()
						+ ((long) 86400000 * (long) 7));// 7일
				npcid = 100578;
				x = 33440;
				y = 32811;
				m = 4;
				h = 5;
				msg1 = "안녕하세요 아덴월드 유저 여러분 다이노스를 위해 거침없이 응원하라! 이벤트가 시작되었습니다.";
				msg2 = "다이노스 이벤트 기간은 총 7일이며 7일 이후 자동 일괄 삭제 됩니다.";
				msg3 = "다이노스 이벤트가 운영자에 의해 강제 종료 되었습니다.";
			} else if (type.equalsIgnoreCase("단테스")) {
				DelTime = new Timestamp(System.currentTimeMillis()
						+ (long) ((long) 604800000 / 7 * 28));// 4주
				npcid = 100577;
				x = 33436;
				y = 32811;
				m = 4;
				h = 4;
				msg1 = "안녕하세요 아덴월드 유저 여러분 열어라! 단테스의 보물 상자! 이벤트가 시작되었습니다.";
				msg2 = "단테스의 보물 상자 이벤트는 총 4주 동안 진행되며 이벤트 시작 후 4주 뒤 단테스의 유물 상자는 삭제됩니다.";
				msg3 = "단테스의 보물 상자 이벤트가 운영자에 의해 강제 종료 되었습니다.";
			} else if (type.equalsIgnoreCase("붉은수색")) {
				DelTime = new Timestamp(System.currentTimeMillis()
						+ ((long) 86400000 * (long) 7));// 7일
				npcid = 100645;
				x = 33443;
				y = 32798;
				m = 4;
				h = 5;
				msg1 = "아덴의 진정한 국왕을 위해 수색 작전에 참가하여 공성전 물자를 확보하라! 붉은기사단의 보급물자 수색작전! 이벤트가 시작되었습니다.";
				msg2 = "붉은기사단의 보급물자 수색작전은 총 7일 동안 진행되며 이벤트 시작 후 7일 뒤 이벤트 관련 아이템은 삭제됩니다.";
				msg3 = "붉은기사단의 보급물자 수색작전 이벤트가 운영자에 의해 강제 종료 되었습니다.";
			} else if (type.equalsIgnoreCase("붉은사자")) {
				DelTime = new Timestamp(System.currentTimeMillis()
						+ ((long) 86400000 * (long) 7));// 7일
				npcid = 100662;
				x = 33426;
				y = 32797;
				m = 4;
				h = 4;
				msg1 = "붉은 사자의 힘이 그대의 검과 방패가 되어, 그대와 함께 하리라! 붉은 사자의 투지! 이벤트가 시작되었습니다.";
				msg2 = "붉은 사자의 투지는 총 7일 동안 진행되며 이벤트 시작 후 7일 뒤 이벤트 관련 아이템은 삭제됩니다.";
				msg3 = "붉은 사자의 투지 이벤트가 운영자에 의해 강제 종료 되었습니다.";
			} else if (type.equalsIgnoreCase("크리스마스")) {
				DelTime = new Timestamp(System.currentTimeMillis()
						+ ((long) 86400000 * (long) 7));// 7일
				npcid = 100691;
				x = 33450;
				y = 32795;
				m = 4;
				h = 6;
				msg1 = "언제나 내 곁을 지켜주는 캐릭터 마법인형과 함께라면 크리스마스에도 전혀 외롭지않아요~ 메리 솔로 크리스마스! 이벤트가 시작되었습니다.";
				msg2 = "메리 솔로 크리스마스는 총 7일 동안 진행되며 이벤트 시작 후 7일 뒤 이벤트 관련 아이템은 삭제됩니다.";
				msg3 = "메리 솔로 크리스마스 이벤트가 운영자에 의해 강제 종료 되었습니다.";
				/*
				 * }else if(type.equalsIgnoreCase("아인하사드시선")){ DelTime = new
				 * Timestamp(System.currentTimeMillis()+((long)86400000 *
				 * (long)30));//30일 npcid=100708; x = 32813; y = 32878; m = 610;
				 * h = 6; msg1 =
				 * "요일별로 특정 던전에 발휘되는 신성한 축복의 기운! 아인하사드의 따스한 시선! 이벤트가 시작되었습니다.";
				 * msg2 =
				 * "아인하사드의 따스한 시선는 총 30일 동안 진행되며 이벤트 시작 후 30일 뒤 이벤트는 종료됩니다.";
				 * msg3 = "아인하사드의 따스한 시선 이벤트가 운영자에 의해 강제 종료 되었습니다.";
				 */
			} else if (type.equalsIgnoreCase("10검")) {
				DelTime = new Timestamp(System.currentTimeMillis()
						+ ((long) 86400000 * (long) 3));// 3일
				npcid = 100709;
				x = 33428;
				y = 32797;
				m = 4;
				h = 4;
				msg1 = "10검! 그것이 내 것이다! 내 10검에 자비란 없다. 이벤트가 시작되었습니다.";
				msg2 = "내 10검에 자비란 없다는 총 3일 동안 진행되며 이벤트 시작 후 3일 뒤 이벤트는 종료됩니다.";
				msg3 = "내 10검에 자비란 없다 이벤트가 운영자에 의해 강제 종료 되었습니다.";
			} else if (type.equalsIgnoreCase("드래곤2")) {
				// BossTimer에 Config로 인해서 자동으로 가동이 됨.
				DelTime = new Timestamp(System.currentTimeMillis()
						+ (3600000 * 30));// 6시간
				npcid = 100764;
				x = 33454;
				y = 32794;
				m = 4;
				h = 6;
				msg1 = "드래곤의 상자 이벤트가 시작되었습니다.";
				msg2 = "이벤트는 매주 토요일 저녁6시부터 일요일 자정까지 진행이 됩니다.";
				msg3 = "드래곤의 상자 이벤트가 운영자에 의해 강제 종료 되었습니다.";
				// BossTimer에 Config로 인해서 자동으로 가동이 됨.
			} else if (type.equalsIgnoreCase("단테스2")) {
				DelTime = new Timestamp(System.currentTimeMillis()
						+ (3600000 * 6));// 6시간
				npcid = 100765;
				x = 33436;
				y = 32811;
				m = 4;
				h = 4;
				msg1 = "단테스의 유물 상자 이벤트가 시작되었습니다.";
				msg2 = "이벤트는 매주 화요일,목요일 오후 6시부터 12시까지 (6시간) 진행이 됩니다.";
				msg3 = "단테스의 유물 상자 이벤트가 운영자에 의해 강제 종료 되었습니다.";

			} else if (type.equalsIgnoreCase("오림")) {
				// BossTimer에 Config로 인해서 자동으로 가동이 됨.
				DelTime = new Timestamp(System.currentTimeMillis()
						+ (3600000 * 6));// 6시간
				npcid = 100900;
				x = 33436;
				y = 32811;
				m = 4;
				h = 4;
				msg1 = "오림의 연구 발표회 이벤트가 시작되었습니다.";
				msg2 = "이벤트는 매주 월요일,토요일 오후 6시부터 12시까지 (6시간) 진행이 됩니다.";
				msg3 = "오림의 연구 발표회 이벤트가 운영자에 의해 강제 종료 되었습니다.";
			} else if (type.equalsIgnoreCase("그렘린")) {
				// BossTimer에 Config로 인해서 자동으로 가동이 됨.
				// DelTime = new
				// Timestamp(System.currentTimeMillis()+150000);//6시간
				DelTime = new Timestamp(System.currentTimeMillis()
						+ ((long) 86400000 * (long) 7));// 3일
				npcid = 100881;
				x = 33435;
				y = 32811;
				m = 4;
				h = 4;
				msg1 = "그렘린의 마법인형 이벤트가 시작되었습니다. 접속 유지시 1시간마다 선물상자가 지급되며, 그렘린 마법인형을 소환중일경우 30분마다 선물상자가 추가 지급됩니다.";
				msg2 = "그렘린의 마법인형 이벤트는 총 7일 동안 진행되며 이벤트 종료시 마법인형도 함께 삭제됩니다.";
				msg3 = "그렘린의 마법인형 이벤트가 운영자에 의해 강제 종료 되었습니다.";

			} else if (type.equalsIgnoreCase("루피주먹")) {
				// BossTimer에 Config로 인해서 자동으로 가동이 됨.
				// DelTime = new
				// Timestamp(System.currentTimeMillis()+150000);//6시간
				DelTime = new Timestamp(System.currentTimeMillis()
						+ (3600000 * 54));// 54시간
				npcid = 100883;
				x = 33435;
				y = 32811;
				m = 4;
				h = 4;
				msg1 = "루피의 용기의 주먹 이벤트가 시작되었습니다. 접속 유지시 1시간마다 주머니가 지급되며, 기란 감옥에서 일정 확률로 주머니가 드랍 됩니다.";
				msg2 = "루피의 용기의 주먹 이벤트는 금요일 오후6시 ~ 일요일 자정 까지 진행됩니다.";
				msg3 = "루피의 용기의 주먹 이벤트가 운영자에 의해 강제 종료 되었습니다.";

			} else if (type.equalsIgnoreCase("감자캐기")) {
				DelTime = new Timestamp(System.currentTimeMillis()
						+ ((long) 86400000 * (long) 2));// 3일
				// DelTime = new
				// Timestamp(System.currentTimeMillis()+((long)10000 *
				// (long)3));//3일
				npcid = 100766;
				x = 32813;
				y = 32878;
				m = 610;
				h = 6;
				msg1 = "이번주는 감자와 함께하는 감자캐기 이벤트! 농작물 수확을 위해 여러분의 호미를 갈아주세요.";
				msg2 = "이벤트는 총 3일동안 진행됩니다";
				msg3 = "감자캐기 이벤트가 운영자에 의해 강제 종료 되었습니다.";
				// msg1 =
				// "이번주 꽃구경은 잘 다녀오셨는지요? 감자서버와 함께하는 화려한 봄날의 외출 벚꽃 이벤트를 진행합니다.";
				// msg2 = "이벤트는 총 3일동안 진행이됩니다.";
				// msg3 = "벚꽃 이벤트가 운영자에 의해 강제 종료 되었습니다.";
			} else {
				gm.sendPackets(new S_SystemMessage(type.toString()
						+ " 은 이벤트 목록이 없습니다."), true);
				return;
			}

			L1Npc npc = NpcTable.getInstance().getTemplate(npcid);
			if (npc == null) {
				gm.sendPackets(new S_SystemMessage(type.toString()
						+ " 엔피시가 없습니당. 확인 바랍니다."), true);
				return;
			}
			if (swich.equalsIgnoreCase("시작")) {
				EventShopTable.Store(npc, x, y, m, h, DelTime);
				EventShopTable.Spawn(npcid, x, y, (short) m, h, DelTime);
				L1World.getInstance().broadcastServerMessage(msg1);
				L1World.getInstance().broadcastServerMessage(msg2);
				if (type.equalsIgnoreCase("아인하사드시선")) {
					Calendar cal = (Calendar) Calendar.getInstance();
					int CurrentDay = cal.getTime().getDay();
					if (CurrentDay == 0) {
						L1World.getInstance().broadcastServerMessage(
								"\\F3현재 오렌 필드 지역에서 아인하사드의 축복이 증폭 되었습니다.");
					} else if (CurrentDay == 1) {
						L1World.getInstance().broadcastServerMessage(
								"\\F3현재 상아탑 발록 진영에서 아인하사드의 축복이 증폭 되었습니다.");
					} else if (CurrentDay == 2) {
						L1World.getInstance().broadcastServerMessage(
								"\\F3현재 기란 감옥에서 아인하사드의 축복이 증폭 되었습니다.");
					} else if (CurrentDay == 3) {
						L1World.getInstance().broadcastServerMessage(
								"\\F3현재 글루디오 던전에서 아인하사드의 축복이 증폭 되었습니다.");
					} else if (CurrentDay == 4) {
						L1World.getInstance().broadcastServerMessage(
								"\\F3현재 라스타바드 던전에서 아인하사드의 축복이 증폭 되었습니다.");
					} else if (CurrentDay == 5) {
						L1World.getInstance().broadcastServerMessage(
								"\\F3현재 용의 계곡 던전에서 아인하사드의 축복이 증폭 되었습니다.");
					} else if (CurrentDay == 6) {
						L1World.getInstance().broadcastServerMessage(
								"\\F3현재 말하는 섬,던전에서 아인하사드의 축복이 증폭 되었습니다.");
					}
				}
			} else if (swich.equalsIgnoreCase("종료")) {
				EventShopTable.Delete(npcid);
				Object[] arrnpc = null;
				arrnpc = EventShopList.getEventShopNpc(npcid).toArray();
				if (arrnpc.length > 0) {
					for (Object DelNpc : arrnpc) {
						L1NpcInstance dnpc = (L1NpcInstance) DelNpc;
						dnpc.deleteMe();
						EventShopList.OddEvents(dnpc);
					}
				}
				arrnpc = null;
				L1World.getInstance().broadcastServerMessage(msg3);
			} else if (swich.equalsIgnoreCase("연장")) {
				int min = Integer.parseInt(tokenizer.nextToken());
				min = min * 60 * 1000;
				EventShopList.AddEventsTime(npcid, min);
			}

		} catch (Exception e) {
			gm.sendPackets(new S_SystemMessage(
					".이벤트 [이벤트명] [시작/종료/연장] [연장경우 분입력]"), true);
		}
	}
	
	private void 상점검사(L1PcInstance gm) {
		try {
			ArrayList<Integer> itemids = new ArrayList<Integer>();
			Connection con = null;
			PreparedStatement pstm = null;
			ResultSet rs = null;
			int cnt;
			Iterator i$;
			try {
				con = L1DatabaseFactory.getInstance().getConnection();
				pstm = con.prepareStatement("SELECT item_id FROM shop");
				rs = pstm.executeQuery();
				while (rs.next()) {
					if (!itemids.contains(Integer.valueOf(rs.getInt("item_id")))) {
						itemids.add(Integer.valueOf(rs.getInt("item_id")));
					}
				}
				cnt = 0;
				for (i$ = itemids.iterator(); i$.hasNext();) {
					int itemid = ((Integer) i$.next()).intValue();
					int 구매최저가 = 최소값(itemid);
					int 판매최고가 = 최대값(itemid);
					if ((구매최저가 != 0) && (구매최저가 < 판매최고가)) {
						gm.sendPackets(new S_SystemMessage("검출됨! [템 " + itemid + " : [구매값 " + 구매최저가 + "] [매입값 " + 판매최고가 + "]"));
					}
					cnt++;
				}
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				SQLUtil.close(rs, pstm, con);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	private int 최소값(int itemid) {
	    try {
	      Connection con = null;
	      PreparedStatement pstm = null;
	      ResultSet rs = null;
	      try {
	        con = L1DatabaseFactory.getInstance().getConnection();
	        pstm = con.prepareStatement("SELECT * FROM shop WHERE item_id = ? AND selling_price NOT IN (-1) ORDER BY selling_price ASC limit 1");
	        pstm.setInt(1, itemid);
	        rs = pstm.executeQuery();
	        if (rs.next()) {
	          int temp = 0;
	          if (rs.getInt("pack_count") > 1)
	            temp = rs.getInt("selling_price") / rs.getInt("pack_count");
	          else {
	            temp = rs.getInt("selling_price");
	          }
	          int i = temp;
	          return i;
	        }
	      }
	      catch (SQLException e)
	      {
	        e.printStackTrace();
	      } finally {
	        SQLUtil.close(rs, pstm, con);
	      }
	      return 0;
	    }
	    catch (Exception e) {
	      e.printStackTrace();
	    }return 0;
	  }

	  private int 최대값(int itemid) {
	    try {
	      Connection con = null;
	      PreparedStatement pstm = null;
	      ResultSet rs = null;
	      try {
	        con = L1DatabaseFactory.getInstance().getConnection();
	        pstm = con.prepareStatement("SELECT purchasing_price FROM shop WHERE item_id = ? ORDER BY purchasing_price DESC limit 1");
	        pstm.setInt(1, itemid);
	        rs = pstm.executeQuery();
	        if (rs.next()) {
	          int i = rs.getInt("purchasing_price");
	          return i;
	        }
	      }
	      catch (SQLException e)
	      {
	        e.printStackTrace();
	      } finally {
	        SQLUtil.close(rs, pstm, con);
	      }
	      return -1;
	    } catch (Exception e) {
	      e.printStackTrace();
	    }return -1;
	  }
	  
	private void standBy79(L1PcInstance gm, String param) {
		try {
			StringTokenizer tokenizer = new StringTokenizer(param);
			String nameId = tokenizer.nextToken();
			int h = Integer.parseInt(tokenizer.nextToken());
			int m = Integer.parseInt(tokenizer.nextToken());
			int npcid = parseNpcId(nameId);
			L1Npc npc = NpcTable.getInstance().getTemplate(npcid);
			if (npc == null) {
				gm.sendPackets(new S_SystemMessage("해당 NPC가 발견되지 않습니다."), true);
				return;
			}
			gm.sendPackets(new S_SystemMessage(npc.get_name() + " 를(을) " + h + "시간 " + m + "분 동안 스폰합니다."), true);
			if (h != 0) {
				h = ((h * 60) * 60) * 1000;
			}
			if (m != 0) {
				m = (m * 60) * 1000;
			}
			int time = 0;
			time = h + m;
			L1SpawnUtil.spawn(gm, npcid, 0, time, false);

		} catch (Exception e) {
			gm.sendPackets(new S_SystemMessage(".이벤상점 [엔피시번호or이름] [시간] [분]"),
					true);
		}
	}

	private void standBy82(L1PcInstance gm, String param) {
		try {
			StringTokenizer tokenizer = new StringTokenizer(param);
			String name = tokenizer.nextToken();
			L1PcInstance ck = L1World.getInstance().getPlayer(name);
			if (ck != null) {
				gm.sendPackets(new S_SystemMessage("접속중인 유저와 동일 이름은 불가능."),
						true);
				return;
			}
			int rnd = 0;
			int rnd2 = 0;
			for (int i = 0; i < 100; i++) {
				L1PcInstance pc = new MySqlCharacterStorage().loadCharacter(gm
						.getName());
				rnd = _random.nextInt(20);
				rnd2 = _random.nextInt(20);
				pc.setId(ObjectIdFactory.getInstance().nextId());
				pc.setX(gm.getX()/*-10+rnd*/);
				pc.setY(gm.getY()/*-10+rnd2*/);
				pc.setMap(gm.getMap());
				pc.setLawful(0);

				pc.getAC().setAc(gm.getAC().getAc());

				pc.setName(name + i);

				pc.setLevel(gm.getLevel());

				pc.getResistance().setBaseMr(gm.getResistance().getMr());
				pc.setTitle("L" + gm.getLevel() + " M"
						+ gm.getResistance().getMr() + " A"
						+ gm.getAC().getAc());
				pc.addBaseMaxHp((short) 32767);
				pc.setCurrentHp(32767);

				pc.샌드백 = true;

				L1World.getInstance().storeObject(pc);
				L1World.getInstance().addVisibleObject(pc);
			}
			gm.sendPackets(new S_SystemMessage("샌드백 스폰!"), true);
		} catch (Exception e) {
			gm.sendPackets(new S_SystemMessage(".분신 [이름]"), true);
		}
	}
	private void GiveHouse(L1PcInstance gm, String poby) {
		   try {
		    StringTokenizer st = new StringTokenizer(poby);
		    String pobyname = st.nextToken();
		    int pobyhouseid = Integer.parseInt(st.nextToken());
		    L1PcInstance target = L1World.getInstance().getPlayer(pobyname);
		    if (target != null) {
		     if (target.getClanid() != 0) {
		      L1Clan TargetClan = L1World.getInstance().getClan(target.getClanname());
		      L1House pobyhouse = HouseTable.getInstance().getHouseTable(pobyhouseid);
		      TargetClan.setHouseId(pobyhouseid);
		      ClanTable.getInstance().updateClan(TargetClan);
		      if(pobyhouseid != 0){
		      gm.sendPackets(new S_SystemMessage(target.getClanname()+" 혈맹에게 "+pobyhouse.getHouseName()+"번을 지급하였습니다."));
		      for (L1PcInstance tc : TargetClan.getOnlineClanMember()) {
		       tc.sendPackets(new S_SystemMessage("게임마스터로부터 "+pobyhouse.getHouseName()+"번을 지급 받았습니다."));
		      }
		      }else{
			      gm.sendPackets(new S_SystemMessage(target.getClanname()+" 혈맹의 아지트 압류."));
		      }
		     } else {
		      gm.sendPackets(new S_SystemMessage(target.getName()+"님은 혈맹에 속해 있지 않습니다."));
		     }
		    } else {
		     gm.sendPackets(new S_ServerMessage(73, pobyname));
		    }
		   } catch (Exception e) {
		    gm.sendPackets(new S_SystemMessage("\\fY.아지트 <지급할혈맹원> <아지트번호>"));
		   }
		  }
	private void standBy81(L1PcInstance gm, String param) {
		try {
			StringTokenizer tokenizer = new StringTokenizer(param);
			String name = tokenizer.nextToken();
			L1PcInstance ck = L1World.getInstance().getPlayer(name);
			if (ck != null) {
				gm.sendPackets(new S_SystemMessage("접속중인 유저와 동일 이름은 불가능."),
						true);
				return;
			}

			L1PcInstance pc = new MySqlCharacterStorage().loadCharacter(gm
					.getName());
			pc.setId(ObjectIdFactory.getInstance().nextId());
			pc.setX(gm.getX());
			pc.setY(gm.getY());
			pc.setMap(gm.getMap());
			pc.setLawful(0);

			pc.getAC().setAc(gm.getAC().getAc());

			pc.setName(name);

			pc.setLevel(gm.getLevel());

			pc.getResistance().setBaseMr(gm.getResistance().getMr());
			pc.setTitle("L" + gm.getLevel() + " M" + gm.getResistance().getMr()
					+ " A" + gm.getAC().getAc());
			pc.addBaseMaxHp((short) 32767);
			pc.setCurrentHp(32767);

			pc.샌드백 = true;

			L1World.getInstance().storeObject(pc);
			L1World.getInstance().addVisibleObject(pc);

			gm.sendPackets(new S_SystemMessage("샌드백 스폰!"), true);
		} catch (Exception e) {
			gm.sendPackets(new S_SystemMessage(".분신 [이름]"), true);
		}
	}

	private void standBy77(L1PcInstance gm, String param) {
		try {
			StringTokenizer tokenizer = new StringTokenizer(param);
			String pcName = tokenizer.nextToken();
			L1PcInstance target = L1World.getInstance().getPlayer(pcName);
			int objid = 0;
			String acname = null;
			if (target != null) {
				target.sendPackets(new S_Disconnect());
			}
			Connection con = null;
			PreparedStatement pstm = null;
			ResultSet rs = null;
			try {
				con = L1DatabaseFactory.getInstance().getConnection();
				pstm = con
						.prepareStatement("SELECT objid, account_name FROM characters WHERE char_name=?");
				pstm.setString(1, pcName);
				rs = pstm.executeQuery();
				while (rs.next()) {
					objid = rs.getInt(1);
					acname = rs.getString(2);
				}
				if (objid == 0) {
					gm.sendPackets(new S_SystemMessage(
							"디비에 해당 유저의 이름이 존재하지 않습니다."), true);

				} else {
					gm.sendPackets(new S_SystemMessage(acname + "계정 " + pcName
							+ "님의 케릭터를 삭제 합니다."), true);
					CharacterTable.getInstance()
							.deleteCharacter(acname, pcName);
					gm.sendPackets(
							new S_SystemMessage("해당유저를 정상적으로 삭제 하였습니다."), true);
				}
			} catch (SQLException e) {

			} finally {
				SQLUtil.close(rs);
				SQLUtil.close(pstm);
				SQLUtil.close(con);
			}

		} catch (Exception eee) {
			gm.sendPackets(new S_SystemMessage(".케릭삭제 [케릭명]"), true);
		}
	}

	private void 매입선물(L1PcInstance gm, String param) {
		try {
			StringTokenizer st = new StringTokenizer(param);
		//	String name = st.nextToken();
			String nameid = st.nextToken();
			L1PcInstance target = L1World.getInstance().getPlayer("매입의여신");
			int count = 1;
			
			if (st.hasMoreTokens()) {
				count = Integer.parseInt(st.nextToken());
			}
			int enchant = 0;
			if (st.hasMoreTokens()) {
				enchant = Integer.parseInt(st.nextToken());
			}
			int attrenchant = 0;
			if (st.hasMoreTokens()) {
				attrenchant = Integer.parseInt(st.nextToken());
			}
			int isId = 1;
			if (st.hasMoreTokens()) {
				isId = Integer.parseInt(st.nextToken());
			}
			int isBless = 1;
			if (st.hasMoreTokens()) {
				isBless = Integer.parseInt(st.nextToken());
			}
			
			int itemid = 0;
			try {
				itemid = Integer.parseInt(nameid);
			} catch (NumberFormatException e) {
				itemid = ItemTable.getInstance().findItemIdByNameWithoutSpace(nameid);
				if (itemid == 0) {
					gm.sendPackets(new S_SystemMessage(gm, "해당 아이템이 발견되지 않았습니다."), true);
					return;
				}
			}
			L1Item temp = ItemTable.getInstance().getTemplate(itemid);
			if (temp != null) {
				if (temp.isStackable()) {
					L1ItemInstance item = ItemTable.getInstance().createItem(itemid);
					item.setEnchantLevel(0);
					item.setCount(count);
					if (isId == 1) {
						item.setIdentified(true);
					}
					if (target.getInventory().checkAddItem(item, count) == L1Inventory.OK) {
						target.getInventory().storeItem(item);
						target.sendPackets(new S_ServerMessage(403, item.getLogName() + "(ID:" + itemid + ")"), true);
					}
				} else {
					L1ItemInstance item = null;
					int createCount;
					for (createCount = 0; createCount < count; createCount++) {
						item = ItemTable.getInstance().createItem(itemid);
						item.setEnchantLevel(enchant);
						item.setAttrEnchantLevel(attrenchant);
						if (isId == 1) {
							item.setIdentified(true);
						}
						if (isBless == 0) {
							item.setBless(0);
						}
						if (target.getInventory().checkAddItem(item, 1) == L1Inventory.OK) {
							target.getInventory().storeItem(item);
						} else {
							break;
						}
					}
					if (createCount > 0) {
						target.sendPackets(new S_ServerMessage(403, item.getLogName() + "(ID:" + itemid + ")"), true);
					}
				}
				gm.sendPackets(new S_SystemMessage(gm, temp.getName() + "를 " + count + " 개 선물 했습니다. "), true);
			} else {
				gm.sendPackets(new S_SystemMessage(gm, "해당 ID의 아이템은 존재하지 않습니다."), true);
			}
		} catch (Exception e) {
			gm.sendPackets(new S_SystemMessage(gm,".매물 [아이템ID] [갯수] [인챈트수] [속성] [확인상태] [축복상태] 라고 입력해 주세요. "), true);
			gm.sendPackets(new S_SystemMessage(gm,"[속성안내 (화령)1/2/3/33/34 (수령)4/5/6/35/36"),true);
			gm.sendPackets(new S_SystemMessage(gm,"[속성안내 (풍령)7/8/9/37/38 (지령)10/11/12/39/40"),true);
			gm.sendPackets(new S_SystemMessage(gm,"[축복안내 0(축복)/1(일반)/2(저주)] "),true);
			gm.sendPackets(new S_SystemMessage(gm,"[확인안내 0(미확)/1(확인)] "),true);
		}
	}
	
	private void accountdel(L1PcInstance gm, String param) {

		try {

			StringTokenizer tokenizer = new StringTokenizer(param);
			String pcName = tokenizer.nextToken();

			Connection con = null;
			Connection con2 = null;
			PreparedStatement pstm = null;
			PreparedStatement pstm2 = null;
			Connection con3 = null;
			PreparedStatement pstm3 = null;
			ResultSet find2 = null;
			ResultSet find = null;
			String findcha = null;

			try {
				con = L1DatabaseFactory.getInstance().getConnection();
				pstm = con
						.prepareStatement("SELECT * FROM characters WHERE char_name=?");
				pstm.setString(1, pcName);
				find = pstm.executeQuery();

				while (find.next()) {
					findcha = find.getString(1);
				}

				if (findcha == null) {
					gm.sendPackets(new S_SystemMessage("DB에 " + pcName
							+ " 케릭명이 존재 하지 않습니다"), true);
				} else {
					con2 = L1DatabaseFactory.getInstance().getConnection();
					pstm2 = con2.prepareStatement("UPDATE accounts SET banned = 0 WHERE login= ?");
					pstm2.setString(1, findcha);
					pstm2.executeUpdate();
					con3 = L1DatabaseFactory.getInstance().getConnection();
					pstm3 = con3.prepareStatement("SELECT * FROM accounts WHERE login=?");
					pstm3.setString(1, findcha);
					find2 = pstm3.executeQuery();

					if (find2.next()) {
						IpTable.getInstance().liftBanIp(find2.getString(5));
					}

					gm.sendPackets(new S_SystemMessage(pcName
							+ " 의 계정압류를 해제 하였습니다"), true);
				}
			} catch (Exception e) {

			} finally {
				SQLUtil.close(find2);
				SQLUtil.close(find);
				SQLUtil.close(pstm3);
				SQLUtil.close(con3);
				SQLUtil.close(pstm2);
				SQLUtil.close(pstm);
				SQLUtil.close(con2);
				SQLUtil.close(con);
			}
		} catch (Exception exception) {
			gm.sendPackets(new S_SystemMessage(".압류해제 케릭명으로 입력해주세요."), true);
		}
	}

	private void packetbox(L1PcInstance pc, String param) {
		try {
			StringTokenizer st = new StringTokenizer(param);
			int id = Integer.parseInt(st.nextToken(), 10);
			pc.sendPackets(new S_PacketBox(83, id), true);
		} catch (Exception exception) {
			pc.sendPackets(new S_SystemMessage(".패킷박스 [id] 입력"), true);
		}
	}

	private void 채팅(L1PcInstance gm, String param) {
		try {
			StringTokenizer tokenizer = new StringTokenizer(param);
			String pcName = tokenizer.nextToken();
			String pcName2 = tokenizer.nextToken();

			if (pcName.equalsIgnoreCase("혈맹")) {
				if (pcName2.equalsIgnoreCase("켬")) {
					Config.add혈맹(gm);
					gm.sendPackets(new S_SystemMessage(".혈맹 채팅을 모니터 합니다."),
							true);
				} else if (pcName2.equalsIgnoreCase("끔")) {
					Config.remove혈맹(gm);
					gm.sendPackets(new S_SystemMessage(".혈맹 채팅 모니터를 중단 합니다."),
							true);
				} else {
					gm.sendPackets(new S_SystemMessage(
							".채팅 [혈맹/파티/귓말/인첸/삭제/접속/버그/전체] [켬/끔]"), true);
					return;
				}
			} else if (pcName.equalsIgnoreCase("파티")) {
				if (pcName2.equalsIgnoreCase("켬")) {
					Config.add파티(gm);
					gm.sendPackets(new S_SystemMessage(".파티 채팅을 모니터 합니다."),
							true);
				} else if (pcName2.equalsIgnoreCase("끔")) {
					Config.remove파티(gm);
					gm.sendPackets(new S_SystemMessage(".파티 채팅 모니터를 중단 합니다."),
							true);
				} else {
					gm.sendPackets(new S_SystemMessage(
							".채팅 [혈맹/파티/귓말/인첸/삭제/접속/버그/전체] [켬/끔]"), true);
					return;
				}
			} else if (pcName.equalsIgnoreCase("귓말")) {
				if (pcName2.equalsIgnoreCase("켬")) {
					Config.add귓말(gm);
					gm.sendPackets(new S_SystemMessage(".귓말 채팅을 모니터 합니다."),
							true);
				} else if (pcName2.equalsIgnoreCase("끔")) {
					Config.remove귓말(gm);
					gm.sendPackets(new S_SystemMessage(".귓말 채팅 모니터를 중단 합니다."),
							true);
				} else {
					gm.sendPackets(new S_SystemMessage(
							".채팅 [혈맹/파티/귓말/인첸/삭제/접속/버그/전체] [켬/끔]"), true);
					return;
				}
			} else if (pcName.equalsIgnoreCase("인첸")) {
				if (pcName2.equalsIgnoreCase("켬")) {
					Config.add인첸(gm);
					gm.sendPackets(new S_SystemMessage(".인첸 로그를 모니터 합니다."),
							true);
				} else if (pcName2.equalsIgnoreCase("끔")) {
					Config.remove인첸(gm);
					gm.sendPackets(new S_SystemMessage(".인첸 로그 모니터를 중단 합니다."),
							true);
				} else {
					gm.sendPackets(new S_SystemMessage(
							".채팅 [혈맹/파티/귓말/인첸/삭제/접속/버그/전체] [켬/끔]"), true);
					return;
				}
			} else if (pcName.equalsIgnoreCase("삭제")) {
				if (pcName2.equalsIgnoreCase("켬")) {
					Config.add삭제(gm);
					gm.sendPackets(new S_SystemMessage(".삭제 로그를 모니터 합니다."),
							true);
				} else if (pcName2.equalsIgnoreCase("끔")) {
					Config.remove삭제(gm);
					gm.sendPackets(new S_SystemMessage(".삭제 로그 모니터를 중단 합니다."),
							true);
				} else {
					gm.sendPackets(new S_SystemMessage(
							".채팅 [혈맹/파티/귓말/인첸/삭제/접속/버그/전체] [켬/끔]"), true);
					return;
				}
			} else if (pcName.equalsIgnoreCase("접속")) {
				if (pcName2.equalsIgnoreCase("켬")) {
					Config.add접속(gm);
					gm.sendPackets(new S_SystemMessage(".접속 로그를 모니터 합니다."),
							true);
				} else if (pcName2.equalsIgnoreCase("끔")) {
					Config.remove접속(gm);
					gm.sendPackets(new S_SystemMessage(".접속 로그 모니터를 중단 합니다."),
							true);
				} else {
					gm.sendPackets(new S_SystemMessage(
							".채팅 [혈맹/파티/귓말/인첸/삭제/접속/버그/전체] [켬/끔]"), true);
					return;
				}
			} else if (pcName.equalsIgnoreCase("버그")) {
				if (pcName2.equalsIgnoreCase("켬")) {
					Config.add버그(gm);
					gm.sendPackets(new S_SystemMessage(".버그 로그를 모니터 합니다."),
							true);
				} else if (pcName2.equalsIgnoreCase("끔")) {
					Config.remove버그(gm);
					gm.sendPackets(new S_SystemMessage(".버그 로그 모니터를 중단 합니다."),
							true);
				} else {
					gm.sendPackets(new S_SystemMessage(
							".채팅 [혈맹/파티/귓말/인첸/삭제/접속/버그/전체] [켬/끔]"), true);
					return;
				}
			} else if (pcName.equalsIgnoreCase("전체")) {
				if (pcName2.equalsIgnoreCase("켬")) {
					Config.add전체(gm);
					gm.sendPackets(new S_SystemMessage(".모든 채팅을 모니터 합니다."),
							true);
				} else if (pcName2.equalsIgnoreCase("끔")) {
					Config.remove전체(gm);
					gm.sendPackets(new S_SystemMessage(".모든 채팅 모니터를 중단 합니다."),
							true);
				} else {
					gm.sendPackets(new S_SystemMessage(
							".채팅 [혈맹/파티/귓말/인첸/삭제/접속/버그/전체] [켬/끔]"), true);
					return;
				}
			} else {
				gm.sendPackets(new S_SystemMessage(
						".채팅 [혈맹/파티/귓말/인첸/삭제/접속/버그/전체] [켬/끔]"), true);
				return;
			}
		} catch (Exception e) {
			gm.sendPackets(new S_SystemMessage(
					".채팅 [혈맹/파티/귓말/인첸/삭제/접속/버그/전체] [켬/끔]"), true);
		}
	}

	private void autoshop(L1PcInstance gm, String param) {
		if (param.equalsIgnoreCase("켬")) {
			AutoShopManager.getInstance().isAutoShop(true);
			gm.sendPackets(new S_SystemMessage("무인상점 켬"), true);
		} else if (param.equalsIgnoreCase("끔")) {
			AutoShopManager.getInstance().isAutoShop(false);
			gm.sendPackets(new S_SystemMessage("무인상점 끔"), true);
		} else {
			gm.sendPackets(new S_SystemMessage(".무인상점 [켬 or 끔] 입력"), true);
		}
	}

	private void 세호상점(L1PcInstance gm, String param) {
		if (param.equalsIgnoreCase("켬")) {
			if (Config.배세호상점 == null) {
				L1NpcInstance npc = L1SpawnUtil.spawn4(33435, 32804, (short) 4,
						0, 100880, 0, 0, 0);
				if (npc != null) {
					Config.배세호상점 = npc;
				}
				gm.sendPackets(new S_SystemMessage("배세호 상점을 스폰 합니다."), true);
			} else {
				gm.sendPackets(new S_SystemMessage("배세호 상점이 이미 스폰중 입니다."), true);
			}
		} else if (param.equalsIgnoreCase("끔")) {
			if (Config.배세호상점 != null) {
				Config.배세호상점.deleteMe();
				Config.배세호상점 = null;
				gm.sendPackets(new S_SystemMessage("배세호 상점을 삭제 합니다."), true);
			} else {
				gm.sendPackets(new S_SystemMessage("배세호 상점을 찾을 수 없습니다."), true);
			}
		} else {
			gm.sendPackets(new S_SystemMessage(".배세호상점 [켬 or 끔] 입력"), true);
		}
	}

	private void nocall(L1PcInstance gm, String param) {
		try {
			StringTokenizer tokenizer = new StringTokenizer(param);
			String pcName = tokenizer.nextToken();

			L1PcInstance target = null; // q
			target = L1World.getInstance().getPlayer(pcName);
			if (target != null) { // 타겟

				L1Teleport.teleport(target, 33437, 32812, (short) 4, 5, true); // /
																				// 가게될
																				// 지점
																				// (유저가떨어지는지점)
			} else {
				gm.sendPackets(new S_SystemMessage("접속중이지 않는 유저 ID 입니다."), true);
			}
		} catch (Exception e) {
			gm.sendPackets(new S_SystemMessage(".가라 (보낼케릭터명) 으로 입력해 주세요."),
					true);
		}
	}

	private void hellcall(L1PcInstance gm, String param) {
		try {
			StringTokenizer tokenizer = new StringTokenizer(param);
			String pcName = tokenizer.nextToken();

			L1PcInstance target = null; // q
			target = L1World.getInstance().getPlayer(pcName);
			if (target != null) { // 타겟

				target.감옥 = true;

				L1Teleport
						.teleport(target, 32928, 32864, (short) 6202, 5, true); // /
																				// 가게될
																				// 지점
																				// (유저가떨어지는지점)

			} else {
				gm.sendPackets(new S_SystemMessage("접속중이지 않는 유저 ID 입니다."), true);
			}
		} catch (Exception e) {
			gm.sendPackets(new S_SystemMessage(".감옥 (보낼케릭터명) 으로 입력해 주세요."),
					true);
		}
	}

	private void allpresent(L1PcInstance gm, String param) {
		try {
			StringTokenizer kwang = new StringTokenizer(param);
			String item = kwang.nextToken();
			if (item.equalsIgnoreCase("깃털")) {
				for (L1PcInstance pc : L1World.getInstance().getAllPlayers()) {
					if (!pc.isPrivateShop() && pc.getNetConnection() != null) {
						L1Item tempItem = ItemTable.getInstance().getTemplate(
								41159);
						if (tempItem != null) {
							PresentGive(tempItem, 41159, 300, 0, pc);
						}
					}
				}
				return;
			} else if (item.equalsIgnoreCase("코마")) {
				for (L1PcInstance pc : L1World.getInstance().getAllPlayers()) {
					if (!pc.isPrivateShop() && pc.getNetConnection() != null) {
						L1Item tempItem = ItemTable.getInstance().getTemplate(
								60217);
						if (tempItem != null) {
							PresentGive(tempItem, 60217, 1, 0, pc);
						}
					}
				}
				return;
			}

			int itemid = Integer.parseInt(item, 10);
			int enchant = Integer.parseInt(kwang.nextToken(), 10);
			int count = Integer.parseInt(kwang.nextToken(), 10);
			for (L1PcInstance pc : L1World.getInstance().getAllPlayers()) {
				if (!pc.isPrivateShop() && pc.getNetConnection() != null) {
					L1Item tempItem = ItemTable.getInstance().getTemplate(
							itemid);
					if (tempItem != null) {
						PresentGive(tempItem, itemid, count, enchant, pc);
						/*
						 * if (pc.isGhost() == false) { L1ItemInstance item =
						 * ItemTable.getInstance().createItem(itemid);
						 * item.setCount(count); item.setEnchantLevel(enchant);
						 * if (item != null) { if
						 * (pc.getInventory().checkAddItem(item, count) ==
						 * L1Inventory.OK) { pc.getInventory().storeItem(item);
						 * } }
						 * 
						 * pc.sendPackets(new
						 * S_SystemMessage("운영자님께서 전체유저에게 선물로[ "+ item.getName()
						 * +(item.getCount() > 0 ? (" ("+item.getCount()+")") :
						 * "")+" ]를 주었습니다."), true);
						 */
					}
				}
			}
		} catch (Exception exception) {
			gm.sendPackets(new S_SystemMessage(
					".전체선물 아이템ID 인첸트수 아이템수로 입력해 주세요."), true);
		}
	}

	private void PresentGive(L1Item temp, int itemid, int count, int enchant,
			L1PcInstance target) {
		SupplementaryService pwh = WarehouseManager.getInstance()
				.getSupplementaryService(target.getAccountName());
		if (temp.isStackable()) {
			L1ItemInstance item = ItemTable.getInstance().createItem(itemid);
			item.setIdentified(true);
			item.setEnchantLevel(0);
			item.setCount(count);

			pwh.storeTradeItem(item);
			if (target != null) {
				// target.sendPackets(new S_ServerMessage(403, // %0를 손에 넣었습니다.
				// item.getLogName()));
				target.sendPackets(
						new S_SystemMessage("운영자 선물 : 당신에게 "
								+ item.getName()
								+ (item.getCount() > 0 ? (" ("
										+ item.getCount() + ")") : "")
								+ " 를 주었습니다."), true);
			}
		} else {
			L1ItemInstance item = null;
			int createCount;
			for (createCount = 0; createCount < count; createCount++) {
				item = ItemTable.getInstance().createItem(itemid);
				item.setIdentified(true);
				item.setEnchantLevel(enchant);
				pwh.storeTradeItem(item);
			}
			if (createCount > 0) {
				if (target != null) {
					// target.sendPackets(new S_ServerMessage(403, // %0를 손에
					// 넣었습니다.
					// item.getLogName()));
					target.sendPackets(
							new S_SystemMessage("운영자 선물 : 당신에게 "
									+ item.getName()
									+ (item.getCount() > 0 ? (" ("
											+ item.getCount() + ")") : "")
									+ " 를 주었습니다."), true);
				}
			}
		}
	}

	// 셋팅 경험치 아이템 아데나 배율
	private void SetRates(L1PcInstance gm, String param) {
		try {
			StringTokenizer tok = new StringTokenizer(param);
			String exp = tok.nextToken();
			String item = tok.nextToken();
			String adena = tok.nextToken();
			if (NumberChk(exp) && NumberChk(item) && NumberChk(adena)) {
				Config.RATE_XP = ConvertToDouble(exp);
				Config.RATE_DROP_ITEMS = ConvertToDouble(item);
				Config.RATE_DROP_ADENA = ConvertToDouble(adena);
				gm.sendPackets(new S_SystemMessage("[경험치 = " + exp + " 배]"),
						true);
				gm.sendPackets(new S_SystemMessage("[아이템 = " + item + " 배]"),
						true);
				gm.sendPackets(new S_SystemMessage("[아데나 = " + adena
						+ " 배]로 셋팅 되었습니다"), true);
			} else {// 입력한 값이 숫자가 아닐경우
				gm.sendPackets(new S_SystemMessage(
						"잘못된 값을 입력하셨습니다. 모두 숫자로 입력해주세요"), true);
			}
		} catch (Exception e) {
			gm.sendPackets(new S_SystemMessage(
					".배율설정 [경험치][아이템][아데나]를 입력 해주세요."), true);
		}
	}

	private void SetEnchantRates(L1PcInstance gm, String param) {
		try {
			StringTokenizer tok = new StringTokenizer(param);
			String weapon = tok.nextToken();
			String armor = tok.nextToken();
			String accessory = tok.nextToken();
			if (NumberChk(weapon) && NumberChk(armor) && NumberChk(accessory)) {
				Config.ENCHANT_CHANCE_WEAPON = Integer.parseInt(weapon);
				Config.ENCHANT_CHANCE_ARMOR = Integer.parseInt(armor);
				Config.ENCHANT_CHANCE_ACCESSORY = Integer.parseInt(accessory);
				gm.sendPackets(new S_SystemMessage("[무기 인챈율 = " + weapon
						+ " 배]"), true);
				gm.sendPackets(new S_SystemMessage("[방어구 인챈율 = " + armor
						+ " 배]"), true);
				gm.sendPackets(new S_SystemMessage("[악세인챈율]: "
						+ Config.ENCHANT_CHANCE_ACCESSORY + "배"));
			} else {// 입력한 값이 숫자가 아닐경우
				gm.sendPackets(new S_SystemMessage(
						"잘못된 값을 입력하셨습니다. 모두 숫자로 입력해주세요"), true);
			}
		} catch (Exception e) {
			gm.sendPackets(new S_SystemMessage(
					".인챈설정 [무기인챈율][방어구인챈율][장신구인챈율]를 입력 해주세요."), true);
		}
	}

	// 입력값이 숫자인지 체크
	private boolean NumberChk(String str) {
		char ch;

		if (str.equals(""))
			return false;

		for (int i = 0; i < str.length(); i++) {
			ch = str.charAt(i);
			if (ch < 48 || ch > 59) {
				return false;
			}
		}
		return true;
	}

	// 입력받은 값을 더블타입으로 변환
	private double ConvertToDouble(String param) {
		Float Temp = Float.parseFloat(param);
		double doubleValue = (double) Temp;
		return doubleValue;
	}

	private void CheckAllRates(L1PcInstance gm) {
		gm.sendPackets(new S_SystemMessage("***** 현재 설정 배율 *****"), true);
		gm.sendPackets(new S_SystemMessage("[경험치]: " + Config.RATE_XP + "배"),
				true);
		gm.sendPackets(new S_SystemMessage("[아이템]: " + Config.RATE_DROP_ITEMS
				+ "배"), true);
		gm.sendPackets(new S_SystemMessage("[아데나]: " + Config.RATE_DROP_ADENA
				+ "배"), true);
		gm.sendPackets(new S_SystemMessage("[무기인챈율]: "
				+ Config.ENCHANT_CHANCE_WEAPON + "배"), true);
		gm.sendPackets(new S_SystemMessage("[방어구인챈율]: "
				+ Config.ENCHANT_CHANCE_ARMOR + "배"), true);
		gm.sendPackets(new S_SystemMessage("[악세인챈율]: "
				+ Config.ENCHANT_CHANCE_ACCESSORY + "배"));
		
	}

	private void 인벤(L1PcInstance gm, String param) {
		try {
			StringTokenizer stringtokenizer = new StringTokenizer(param);
			String s = stringtokenizer.nextToken();

			L1PcInstance pc = L1World.getInstance().getPlayer(s);
			if (pc == null) {
				gm.sendPackets(new S_SystemMessage("접속 중인 케릭이 아닙니다."), true);
				return;
			}

			List<L1ItemInstance> items = gm.getInventory().getItems();
			for (L1ItemInstance item : items) {
				gm.sendPackets(new S_DeleteInventoryItem(item), true);
			}
			List<L1ItemInstance> tempitem = pc.getInventory().getItems();
			gm.sendPackets(new S_TempInv(tempitem), true);
		} catch (Exception exception21) {
			gm.sendPackets(new S_SystemMessage(".인벤 캐릭터명"), true);
		}
	}

	private void chainfo(L1PcInstance gm, String param) {
		try {
			StringTokenizer stringtokenizer = new StringTokenizer(param);
			String s = stringtokenizer.nextToken();
			gm.sendPackets(new S_Chainfo(1, s));
		} catch (Exception exception21) {
			gm.sendPackets(new S_SystemMessage(".케릭검사 캐릭터명"));
		}
	}
	
	
	private void 도감버프(L1PcInstance gm, String param) {
		try {
			StringTokenizer stringtokenizer = new StringTokenizer(param);
			String s = stringtokenizer.nextToken();
			
			L1PcInstance pc = L1World.getInstance().getPlayer(s);
			
			if (pc == null) {
				gm.sendPackets(new S_SystemMessage("접속 중인 케릭이 아닙니다."), true);
				return;
			}
			
			L1Cooking.newEatCooking(pc, L1SkillId.천하장사버프, 1800);
			
			gm.sendPackets(new S_SystemMessage("천하장사버프 지급 완료."), true);
			
		} catch (Exception exception21) {
			gm.sendPackets(new S_SystemMessage(".도감버프 캐릭명"));
		}
	}
	
	private void 드다(L1PcInstance gm, String param) {
		try {
			StringTokenizer stringtokenizer = new StringTokenizer(param);
			String s = stringtokenizer.nextToken();
			
			L1PcInstance pc = L1World.getInstance().getPlayer(s);
			
			if (pc == null) {
				gm.sendPackets(new S_SystemMessage("접속 중인 케릭이 아닙니다."), true);
				return;
			}
			
			int hasad = 2000000;
			int temphasad = pc.getAinHasad() + hasad;
			if (temphasad > 48000000) {
				pc.sendPackets(new S_SystemMessage("남아있는 축복지수가 많아 지급할 수 없습니다."));
				return;
			}
			
			pc.calAinHasad(hasad);
			pc.sendPackets(new S_ACTION_UI(S_ACTION_UI.EINHASAD, pc), true);
			
			gm.sendPackets(new S_SystemMessage("아인하사드 축복 200 충전 완료."), true);
		} catch (Exception exception21) {
			gm.sendPackets(new S_SystemMessage(".드다 캐릭터명"));
		}
	}
	
	private void 보스출동(L1PcInstance gm, String param){
		// TODO 자동 생성된 메소드 스텁
		try {
			StringTokenizer st = new StringTokenizer(param);
			String 보스이름 = st.nextToken();

			 if (보스이름.equalsIgnoreCase("피닉스")) {
				 L1SpawnUtil.spawn2(33687, 32357, (short) 15440, 45617, 0, 3600*1000, 0);//피닉스
					L1World.getInstance().broadcastServerMessage("\\aI[보스 알림]: 피닉스 (화룡의 둥지)");
					L1World.getInstance().broadcastPacketToAll(new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "\\fO[보스 알림]: 피닉스 (화룡의 둥지)"), true);
					
			} else if (보스이름.equalsIgnoreCase("제니스")) {
				 L1SpawnUtil.spawn2(32819, 32800, (short) 12852, 78000, 0, 3600*1000, 0);//제로스
					L1World.getInstance().broadcastServerMessage("\\aI[보스 알림]: 왜곡의 제니스 퀸 (지배의 탑 1층)");
					L1World.getInstance().broadcastPacketToAll(new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "\\fO[보스 알림]: 왜곡의 제니스 퀸 (지배의 탑 1층)"), true);
					
			} else if (보스이름.equalsIgnoreCase("시어")) {
				 L1SpawnUtil.spawn2(32801, 32798, (short) 12853, 78001, 0, 3600*1000, 0);//제로스
					L1World.getInstance().broadcastServerMessage("\\aI[보스 알림]: 불신의 시어 (지배의 탑 2층)");
					L1World.getInstance().broadcastPacketToAll(new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "\\fO[보스 알림]: 불신의 시어 (지배의 탑 2층)"), true);
					
			} else if (보스이름.equalsIgnoreCase("뱀파이어")) {
				 L1SpawnUtil.spawn2(32801, 32795, (short) 12854, 78002, 0, 3600*1000, 0);//제로스
					L1World.getInstance().broadcastServerMessage("\\aI[보스 알림]: 공포의 뱀파이어 (지배의 탑 3층)");
					L1World.getInstance().broadcastPacketToAll(new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "\\fO[보스 알림]: 공포의 뱀파이어 (지배의 탑 3층)"), true);
					
			} else if (보스이름.equalsIgnoreCase("좀비로드")) {
				 L1SpawnUtil.spawn2(32672, 32863, (short) 12855, 78003, 0, 3600*1000, 0);//제로스
					L1World.getInstance().broadcastServerMessage("\\aI[보스 알림]: 죽음의 좀비로드 (지배의 탑 4층)");
					L1World.getInstance().broadcastPacketToAll(new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "\\fO[보스 알림]: 죽음의 좀비로드 (지배의 탑 4층)"), true);
						
			} else if (보스이름.equalsIgnoreCase("쿠거")) {
				 L1SpawnUtil.spawn2(32669, 32868, (short) 12856, 78004, 0, 3600*1000, 0);//제로스
					L1World.getInstance().broadcastServerMessage("\\aI[보스 알림]: 지옥의 쿠거 (지배의 탑 5층)");
					L1World.getInstance().broadcastPacketToAll(new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "\\fO[보스 알림]: 지옥의 쿠거 (지배의 탑 5층)"), true);
			
			} else if (보스이름.equalsIgnoreCase("머미로드")) {
				 L1SpawnUtil.spawn2(32671, 32854, (short) 12857, 78005, 0, 3600*1000, 0);//제로스
					L1World.getInstance().broadcastServerMessage("\\aI[보스 알림]: 불사의 머미로드 (지배의 탑 6층)");
					L1World.getInstance().broadcastPacketToAll(new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "\\fO[보스 알림]: 불사의 머미로드 (지배의 탑 6층)"), true);
						
			} else if (보스이름.equalsIgnoreCase("아이리스")) {
				 L1SpawnUtil.spawn2(32672, 32853, (short) 12858, 78006, 0, 3600*1000, 0);//제로스
					L1World.getInstance().broadcastServerMessage("\\aI[보스 알림]: 잔혹한 아이리스 (지배의 탑 7층)");
					L1World.getInstance().broadcastPacketToAll(new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "\\fO[보스 알림]: 잔혹한 아이리스 (지배의 탑 7층)"), true);
			
			} else if (보스이름.equalsIgnoreCase("나이트발드")) {
				GeneralThreadPool.getInstance().execute(new 나발등장());
				
			} else if (보스이름.equalsIgnoreCase("리치")) {
				 L1SpawnUtil.spawn2(32678, 32851, (short) 12860, 78008, 0, 3600*1000, 0);//제로스
					L1World.getInstance().broadcastServerMessage("\\aI[보스 알림]: 불멸의 리치 (지배의 탑 9층)");
					L1World.getInstance().broadcastPacketToAll(new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "\\fO[보스 알림]: 불멸의 리치 (지배의 탑 9층)"), true);
					
			} else if (보스이름.equalsIgnoreCase("우그누스")) {
				GeneralThreadPool.getInstance().execute(new 우그등장());
						
			} else if (보스이름.equalsIgnoreCase("그림리퍼")) {
				GeneralThreadPool.getInstance().execute(new 리퍼등장());
					
			} else if (보스이름.equalsIgnoreCase("거인모닝스타")) {
				L1World.getInstance().broadcastServerMessage("\\aD[보스 알림]: 거인 모닝스타가 출현합니다.");
				L1World.getInstance().broadcastPacketToAll(new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "\\fO[보스 알림]: 거인 모닝스타가 출현합니다."), true);
				 L1SpawnUtil.spawn2(34242, 33370, (short) 4, 45610, 0, 3600*1000, 0);
			} else if (보스이름.equalsIgnoreCase("드레이크")) {
				L1World.getInstance().broadcastServerMessage("\\aD[보스 알림]: 용의계곡에서 드레이크 킹이 출현합니다.");
				L1World.getInstance().broadcastPacketToAll(new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "\\fO[보스 알림]: 용의계곡에서 드레이크 킹이 출현합니다."), true);
				 L1SpawnUtil.spawn2(33396,32341, (short) 15430, 707017, 0, 3600*1000, 0);//드레이크 킹
			} else if (보스이름.equalsIgnoreCase("거인모닝스타")) {
				L1World.getInstance().broadcastServerMessage("\\aD[보스 알림]: 거인 모닝스타가 출현합니다.");
				L1World.getInstance().broadcastPacketToAll(new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "\\fO[보스 알림]: 거인 모닝스타가 출현합니다."), true);
				 L1SpawnUtil.spawn2(34242, 33370, (short) 4, 45610, 0, 3600*1000, 0);
			} else if (보스이름.equalsIgnoreCase("아크모")) {
				L1World.getInstance().broadcastServerMessage("\\aD[보스 알림]: 마족신전에서 아크모가 출현합니다.");
				L1World.getInstance().broadcastPacketToAll(new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "\\fO[보스 알림]: 마족신전에서 아크모가 출현합니다."), true);
				 L1SpawnUtil.spawn2(32903, 32792, (short) 410, 202083, 0, 3600*1000, 0);//아크모
			} else if (보스이름.equalsIgnoreCase("테베보스")) {
				L1World.getInstance().broadcastServerMessage("\\aD[보스 알림]: 테베보스가 출현합니다.");
				L1World.getInstance().broadcastPacketToAll(new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "\\fO[보스 알림]: 테베보스가 출현합니다."), true);
				 L1SpawnUtil.spawn2(32778, 32832, (short) 782, 400016, 0, 3600*1000, 0);//테베보스
			} else if (보스이름.equalsIgnoreCase("기르타스")) {
				L1World.getInstance().broadcastServerMessage("\\aD[보스 알림]: 기르타스가 출현합니다. ( 기르타스의 전초기지 )");
				L1World.getInstance().broadcastPacketToAll(new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "\\fO[보스 알림]: 기르타스가 출현합니다. ( 기르타스의 전초기지 )"), true);
				 L1SpawnUtil.spawn2(32867, 32862, (short) 537, 100589, 0, 0*0, 0);//뱀파이어
			} else if (보스이름.equalsIgnoreCase("발록")) {
				try {
				 L1SpawnUtil.spawn2(32746, 32864, (short) 15404, 45752, 0, 3600*1000, 0);//발록
					for(L1PcInstance pc : L1World.getInstance().getAllPlayers()) {
					if (pc.getMapId() == 15403 || pc.getMapId() == 15404){
					L1Teleport.teleport(pc, 33429, 32829, (short) 4, 4, true);
						}
					}
					/**L1DoorInstance door = DoorSpawnTable.getInstance().getDoor(225);
					if (door != null) {
						if (door.getOpenStatus() == ActionCodes.ACTION_Close) {
							door.open();
						}
					}
					L1DoorInstance door1 = DoorSpawnTable.getInstance().getDoor(226);
					if (door1 != null) {
					if (door1.getOpenStatus() == ActionCodes.ACTION_Close) {
					door1.open();
						}
					}
					L1DoorInstance door2 = DoorSpawnTable.getInstance().getDoor(227);
					if (door2 != null) {
					if (door2.getOpenStatus() == ActionCodes.ACTION_Close) {
					door2.open();
						}
					}**/
					
					L1World.getInstance().broadcastServerMessage("\\aI[보스 알림]: 발록 (지배의 결계 2층)");
					L1World.getInstance().broadcastPacketToAll(
					new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "\\fO[보스 알림]: 발록 (지배의 결계 2층)"), true);
				} catch (Exception e) {
					e.printStackTrace();
				}

			} else if (보스이름.equalsIgnoreCase("가디언")) {
				L1SpawnUtil.spawn2(32907, 32781, (short) 1708, 707026, 0, 3600*1000, 0);//에이션트가디언
				L1World.getInstance().broadcastServerMessage("\\aI[보스 알림]: 에이션트 가디언(잊혀진 섬)");
				L1World.getInstance().broadcastPacketToAll(
				new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "\\fO[보스 알림]: 에이션트 가디언(잊혀진 섬)"), true);
			}
		} catch (Exception e) {
			gm.sendPackets(new S_SystemMessage(".보스 [보스이름] ex<가디언/커츠/데스나이트567/발록/그림리퍼/제니스"))	;
		}
	}
	

	private void chatx(L1PcInstance gm, String param) {
		try {
			StringTokenizer tokenizer = new StringTokenizer(param);
			String pcName = tokenizer.nextToken();
			L1PcInstance target = null;
			target = L1World.getInstance().getPlayer(pcName);

			if (target != null) {
				target.getSkillEffectTimerSet().killSkillEffectTimer(
						L1SkillId.STATUS_CHAT_PROHIBITED);
				target.sendPackets(new S_SkillIconGFX(36, 0), true);
				target.sendPackets(new S_ServerMessage(288), true);
				gm.sendPackets(new S_SystemMessage("해당캐릭의 채금을 해제 했습니다."), true);
			}
		} catch (Exception e) {
			gm.sendPackets(new S_SystemMessage(".채금풀기 캐릭터명 이라고 입력해 주세요."), true);
		}
	}
	
	private void giveLawful(L1PcInstance gm, String param) {
		try {
			StringTokenizer tokenizer = new StringTokenizer(param);
			String pcName = tokenizer.nextToken();
			L1PcInstance target = null;
			target = L1World.getInstance().getPlayer(pcName);

			if (target != null) {
				target.setLawful(32767);
				target.save();
				gm.sendPackets(new S_SystemMessage("해당캐릭터의 라우풀 수치를 조정하였습니다."), true);
			}
		} catch (Exception e) {
			gm.sendPackets(new S_SystemMessage(".라우풀 캐릭터명 이라고 입력해 주세요."), true);
		}
	}

	private void StartWar(L1PcInstance gm, String param) {
		try {
			StringTokenizer tok = new StringTokenizer(param);
			String clan_name1 = tok.nextToken();
			String clan_name2 = tok.nextToken();

			L1Clan clan1 = L1World.getInstance().getClan(clan_name1);
			L1Clan clan2 = L1World.getInstance().getClan(clan_name2);

			if (clan1 == null) {
				gm.sendPackets(new S_SystemMessage(clan_name1
						+ "혈맹이 존재하지 않습니다."), true);
				return;
			}

			if (clan2 == null) {
				gm.sendPackets(new S_SystemMessage(clan_name2
						+ "혈맹이 존재하지 않습니다."), true);
				return;
			}

			for (L1War war : L1World.getInstance().getWarList()) {
				if (war.CheckClanInSameWar(clan_name1, clan_name2) == true) {
					gm.sendPackets(new S_SystemMessage("[" + clan_name1
							+ "]혈맹과 [" + clan_name2 + "]혈맹은 현재 전쟁 중 입니다."),
							true);
					return;
				}
			}

			L1War war = new L1War();
			war.handleCommands(2, clan_name1, clan_name2); // 모의전 개시
			for (L1PcInstance pc : L1World.getInstance().getAllPlayers()) {
				pc.sendPackets(new S_SystemMessage("[" + clan_name1 + "]혈맹과 ["
						+ clan_name2 + "]혈맹의 전쟁이 시작 되었습니다."), true);
			}
		} catch (Exception e) {
			gm.sendPackets(new S_SystemMessage(".혈전시작 혈맹이름 혈맹이름"), true);
		}
	}

	private void StopWar(L1PcInstance gm, String param) {
		try {
			StringTokenizer tok = new StringTokenizer(param);
			String clan_name1 = tok.nextToken();
			String clan_name2 = tok.nextToken();

			L1Clan clan1 = L1World.getInstance().getClan(clan_name1);
			L1Clan clan2 = L1World.getInstance().getClan(clan_name2);

			if (clan1 == null) {
				gm.sendPackets(new S_SystemMessage(clan_name1
						+ "혈맹이 존재하지 않습니다."), true);
				return;
			}

			if (clan2 == null) {
				gm.sendPackets(new S_SystemMessage(clan_name2
						+ "혈맹이 존재하지 않습니다."), true);
				return;
			}

			for (L1War war : L1World.getInstance().getWarList()) {
				if (war.CheckClanInSameWar(clan_name1, clan_name2) == true) {
					war.CeaseWar(clan_name1, clan_name2);
					for (L1PcInstance pc : L1World.getInstance()
							.getAllPlayers()) {
						pc.sendPackets(
								new S_SystemMessage("[" + clan_name1 + "]혈맹과 ["
										+ clan_name2 + "]혈맹의 전쟁이 종료 되었습니다."),
								true);
					}
					return;
				}
			}
			gm.sendPackets(new S_SystemMessage("[" + clan_name1 + "]혈맹과 ["
					+ clan_name2 + "]혈맹은 현재 전쟁중이지 않습니다."), true);
		} catch (Exception e) {
			gm.sendPackets(new S_SystemMessage(".혈전종료 혈맹이름 혈맹이름"), true);
		}
	}

	private void searchDatabase(L1PcInstance gm, String param) { // 검색기능추가

		try {
			StringTokenizer tok = new StringTokenizer(param);
			int type = Integer.parseInt(tok.nextToken());
			String name = tok.nextToken();

			searchObject(gm, type, "%" + name + "%");

		} catch (Exception e) {
			gm.sendPackets(new S_SystemMessage(".검색 [0~5] [이름]을 입력 해주세요."),
					true);
			gm.sendPackets(new S_SystemMessage("0=잡템, 1=무기, 2=갑옷, 3=npc"), true);
		}
	}

	private void serversave(L1PcInstance pc) {// 검색후 추가
		Saveserver();// 서버세이브 메소드 선언
		pc.sendPackets(new S_SystemMessage("서버저장이 완료되었습니다."), true);// 지엠에게 알려주고
	}

	/** 서버저장 **/
	private void Saveserver() {
		/** 전체플레이어를 호출 **/
		for (L1PcInstance player : L1World.getInstance().getAllPlayers()) {
			try {
				/** 피씨저장해주고 **/
				player.save();
				/** 인벤도 저장하고 **/
				player.saveInventory();
			} catch (Exception ex) {
				/** 예외 인벤저장 **/
				player.saveInventory();
			}
		}
	}

	/** 서버저장 **/

	private static String encodePassword(String rawPassword)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		byte buf[] = rawPassword.getBytes("UTF-8");
		buf = MessageDigest.getInstance("SHA").digest(buf);

		return Base64.encodeBytes(buf);
	}
	
	private void autoloot(L1PcInstance gm, String param) {
		try {
			StringTokenizer tok = new StringTokenizer(param);
			String type = tok.nextToken();
			if (type.equalsIgnoreCase("리로드")) {
				AutoLoot.getInstance().reload();
				gm.sendPackets(new S_SystemMessage("오토루팅 설정이 리로드 되었습니다."));
			} else if (type.equalsIgnoreCase("검색")) {
				java.sql.Connection con = null;
				PreparedStatement pstm = null;
				ResultSet rs = null;

				String nameid = tok.nextToken();
				try {
					con = L1DatabaseFactory.getInstance().getConnection();
					String strQry;
					strQry = " Select e.item_id, e.name from etcitem e, autoloot l where l.item_id = e.item_id and name Like '%"
							+ nameid + "%' ";
					strQry += " union all "
							+ " Select w.item_id, w.name from weapon w, autoloot l where l.item_id = w.item_id and name Like '%"
							+ nameid + "%' ";
					strQry += " union all "
							+ " Select a.item_id, a.name from armor a, autoloot l where l.item_id = a.item_id and name Like '%"
							+ nameid + "%' ";
					pstm = con.prepareStatement(strQry);
					rs = pstm.executeQuery();
					while (rs.next()) {
						gm.sendPackets(new S_SystemMessage("["
								+ rs.getString("item_id") + "] "
								+ rs.getString("name")));
					}
				} catch (Exception e) {
				} finally {
					rs.close();
					pstm.close();
					con.close();
				}
			} else {
				String nameid = tok.nextToken();
				int itemid = 0;
				try {
					itemid = Integer.parseInt(nameid);
				} catch (NumberFormatException e) {
					itemid = ItemTable.getInstance().findItemIdByNameWithoutSpace(nameid);
					if (itemid == 0) {
						gm.sendPackets(new S_SystemMessage("해당 아이템이 발견되지 않습니다. "));
						return;
					}
				}

				L1Item temp = ItemTable.getInstance().getTemplate(itemid);
				if (temp == null) {
					gm.sendPackets(new S_SystemMessage("해당 아이템이 발견되지 않습니다. "));
					return;
				}
				if (type.equalsIgnoreCase("추가")) {
					if (AutoLoot.getInstance().isAutoLoot(itemid)) {gm.sendPackets(new S_SystemMessage("이미 오토루팅 목록에 있습니다."));
						return;
					}
					AutoLoot.getInstance().storeId(itemid);
					gm.sendPackets(new S_SystemMessage("오토루팅 항목에 추가 했습니다."));
				} else if (type.equalsIgnoreCase("삭제")) {
					if (!AutoLoot.getInstance().isAutoLoot(itemid)) {
						gm.sendPackets(new S_SystemMessage(
								"오토루팅 항목에 해당 아이템이 없습니다."));
						return;
					}
					gm.sendPackets(new S_SystemMessage("오토루팅 항목에서 삭제 했습니다."));
					AutoLoot.getInstance().deleteId(itemid);
				}
			}
		} catch (Exception e) {
			gm.sendPackets(new S_SystemMessage(".오토루팅 리로드"));
			gm.sendPackets(new S_SystemMessage(".오토루팅 추가|삭제 itemid|name"));
			gm.sendPackets(new S_SystemMessage(".오토루팅 검색 name"));
		}
	}
	
	private void AddAccount(L1PcInstance gm, String account, String passwd, String Ip, String Host) {
		try {
			String login = null;
			String password = null;
			java.sql.Connection con = null;
			PreparedStatement statement = null;
			PreparedStatement pstm = null;
			ResultSet rs = null;

			try {
				con = L1DatabaseFactory.getInstance().getConnection();
				password = passwd;
				statement = con.prepareStatement("select * from accounts where login Like '" + account + "'");
				rs = statement.executeQuery();

				if (rs.next())
					login = rs.getString(1);
				if (login != null) {
					gm.sendPackets(new S_SystemMessage("이미 계정이 있습니다."), true);
					return;
				} else {

					String sqlstr = "INSERT INTO accounts SET login=?,password=?,lastactive=?,access_level=?,ip=?,host=?,banned=?,charslot=?,gamepassword=?,notice=?";
					pstm = con.prepareStatement(sqlstr);
					pstm.setString(1, account);
					pstm.setString(2, password);
					pstm.setTimestamp(3,
							new Timestamp(System.currentTimeMillis()));
					pstm.setInt(4, 0);
					pstm.setString(5, Ip);
					pstm.setString(6, Host);
					pstm.setInt(7, 0);
					pstm.setInt(8, 6);
					pstm.setInt(9, 0);
					pstm.setInt(10, 0);
					pstm.executeUpdate();
					gm.sendPackets(new S_SystemMessage("계정 추가가 완료되었습니다."), true);
				}

			} catch (Exception e) {

			} finally {
				SQLUtil.close(rs);
				SQLUtil.close(pstm);
				SQLUtil.close(statement);
				SQLUtil.close(con);
			}
		} catch (Exception e) {
		}
	}

	private void searchObject(L1PcInstance gm, int type, String name) {
		try {
			String str1 = null;
			String str2 = null;
			int count = 0;
			java.sql.Connection con = null;
			con = L1DatabaseFactory.getInstance().getConnection();
			PreparedStatement statement = null;
			ResultSet rs = null;
			try {
				switch (type) {
				case 0: // etcitem
					statement = con
							.prepareStatement("select item_id, name from etcitem where name Like '"
									+ name + "'");
					break;
				case 1: // weapon
					statement = con
							.prepareStatement("select item_id, name from weapon where name Like '"
									+ name + "'");
					break;
				case 2: // armor
					statement = con
							.prepareStatement("select item_id, name from armor where name Like '"
									+ name + "'");
					break;
				case 3: // npc
					statement = con
							.prepareStatement("select npcid, name from npc where name Like '"
									+ name + "'");
					break;
				case 4: // polymorphs
					statement = con
							.prepareStatement("select polyid, name from polymorphs where name Like '"
									+ name + "'");
					break;
				default:
					break;
				}
				rs = statement.executeQuery();
				while (rs.next()) {
					str1 = rs.getString(1);
					str2 = rs.getString(2);
					gm.sendPackets(new S_SystemMessage("id : [" + str1
							+ "], name : [" + str2 + "]"), true);
					count++;
				}

				gm.sendPackets(new S_SystemMessage("총 [" + count
						+ "]개의 데이터가 검색되었습니다."), true);
			} catch (Exception e) {

			} finally {
				SQLUtil.close(rs);
				SQLUtil.close(statement);
				SQLUtil.close(con);
			}
		} catch (Exception e) {
		}
	}

	private static boolean isDisitAlpha(String str) {
		boolean check = true;
		for (int i = 0; i < str.length(); i++) {
			if (!Character.isDigit(str.charAt(i)) // 숫자가 아니라면
					&& Character.isLetterOrDigit(str.charAt(i)) // 특수문자라면
					&& !Character.isUpperCase(str.charAt(i)) // 대문자가 아니라면
					&& !Character.isLowerCase(str.charAt(i))) { // 소문자가 아니라면
				check = false;
				break;
			}
		}
		return check;
	}

	private void addaccount(L1PcInstance gm, String param) {
		try {
			StringTokenizer tok = new StringTokenizer(param);
			String user = tok.nextToken();
			String passwd = tok.nextToken();

			if (user.length() < 4) {
				gm.sendPackets(new S_SystemMessage("입력하신 계정명의 자릿수가 너무 짧습니다."),
						true);
				gm.sendPackets(new S_SystemMessage("최소 4자 이상 입력해 주십시오."), true);
				return;
			}
			if (passwd.length() < 4) {
				gm.sendPackets(new S_SystemMessage("입력하신 암호의 자릿수가 너무 짧습니다."),
						true);
				gm.sendPackets(new S_SystemMessage("최소 4자 이상 입력해 주십시오."), true);
				return;
			}

			if (passwd.length() > 12) {
				gm.sendPackets(new S_SystemMessage("입력하신 암호의 자릿수가 너무 깁니다."),
						true);
				gm.sendPackets(new S_SystemMessage("최대 12자 이하로 입력해 주십시오."),
						true);
				return;
			}

			if (isDisitAlpha(passwd) == false) {
				gm.sendPackets(new S_SystemMessage(
						"암호에 허용되지 않는 문자가 포함 되어 있습니다."), true);
				return;
			}
			AddAccount(gm, user, passwd, "127.0.0.1", "127.0.0.1");
		} catch (Exception e) {
			gm.sendPackets(new S_SystemMessage(".계정추가 [계정명] [암호] 입력"), true);
		}
	}

	private void changepassword(L1PcInstance gm, String param) {
		try {
			StringTokenizer tok = new StringTokenizer(param);
			String user = tok.nextToken();
			String oldpasswd = tok.nextToken();
			String newpasswd = tok.nextToken();

			if (user.length() < 4) {
				gm.sendPackets(new S_SystemMessage("입력하신 계정명의 자릿수가 너무 짧습니다."),
						true);
				gm.sendPackets(new S_SystemMessage("최소 4자 이상 입력해 주십시오."), true);
				return;
			}
			if (newpasswd.length() < 4) {
				gm.sendPackets(new S_SystemMessage("입력하신 암호의 자릿수가 너무 짧습니다."),
						true);
				gm.sendPackets(new S_SystemMessage("최소 4자 이상 입력해 주십시오."), true);
				return;
			}
			if (newpasswd.length() > 12) {
				gm.sendPackets(new S_SystemMessage("입력하신 암호의 자릿수가 너무 깁니다."),
						true);
				gm.sendPackets(new S_SystemMessage("최대 12자 이하로 입력해 주십시오."),
						true);
				return;
			}

			if (isDisitAlpha(newpasswd) == false) {
				gm.sendPackets(new S_SystemMessage(
						"암호에 허용되지 않는 문자가 포함 되어 있습니다."), true);
				return;
			}
			chkpassword(gm, user, oldpasswd, newpasswd);
		} catch (Exception e) {
			gm.sendPackets(new S_SystemMessage(".비번변경 [계정] [현재비번] [바꿀비번] 입력"),
					true);
		}
	}

	private void chkpassword(L1PcInstance gm, String account,
			String oldpassword, String newpassword) {
		try {
			String password = null;
			java.sql.Connection con = null;
			con = L1DatabaseFactory.getInstance().getConnection();
			PreparedStatement statement = null;
			PreparedStatement pstm = null;

			statement = con
					.prepareStatement("select password from accounts where login='"
							+ account + "'");
			ResultSet rs = statement.executeQuery();

			try {

				if (rs.next())
					password = rs.getString(1);
				if (password == null) {
					gm.sendPackets(new S_SystemMessage(
							"입력하신 계정은 서버내에 존재 하지 않습니다."), true);
					return;
				}

				if (!isPasswordTrue(password, oldpassword)) {
					// System.out.println("현재 비번 : " +
					// oldpassword+" - 체크 비번 : "+password);
					gm.sendPackets(new S_SystemMessage(
							"기존 계정명의 비밀번호가 일치하지 않습니다. "), true);
					gm.sendPackets(new S_SystemMessage("다시 확인하시고 실행해 주세요."),
							true);
					return;
				} else {
					String sqlstr = "UPDATE accounts SET password=password(?) WHERE login=?";
					pstm = con.prepareStatement(sqlstr);
					pstm.setString(1, newpassword);
					pstm.setString(2, account);
					pstm.executeUpdate();
					gm.sendPackets(new S_SystemMessage("계정명 : " + account
							+ " / 바뀐비밀번호 : " + newpassword), true);
					gm.sendPackets(new S_SystemMessage(
							"비밀번호 변경이 정상적으로 완료되었습니다."), true);
				}
			} catch (Exception e) {

			} finally {
				SQLUtil.close(rs);
				SQLUtil.close(pstm);
				SQLUtil.close(statement);
				SQLUtil.close(con);
			}
		} catch (Exception e) {
		}
	}

	// 패스워드 맞는지 여부 리턴
	public static boolean isPasswordTrue(String Password, String oldPassword) {
		String _rtnPwd = null;
		Connection con = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con.prepareStatement("SELECT password(?) as pwd ");

			pstm.setString(1, oldPassword);
			rs = pstm.executeQuery();
			if (rs.next()) {
				_rtnPwd = rs.getString("pwd");
			}
			if (_rtnPwd.equals(Password)) { // 동일하다면
				return true;
			} else
				return false;
		} catch (Exception e) {
			_log.log(Level.SEVERE, e.getLocalizedMessage(), e);
		} finally {
			SQLUtil.close(rs);
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
		return false;
	}

	String tampage = "cd 01 0a 1c 08 0e 10 b8 f9 1a 18 00 20 00 2a "
			+ "0a c6 c4 c7 c1 b8 ae bf c2 c5 b3 30 30 38 03 40 "
			+ "01 0a 1d 08 0e 10 ba f9 1a 18 00 20 00 2a 0b 4c "
			+ "69 76 65 6f 72 44 65 61 74 68 30 2f 38 02 40 01 "
			+ "0a 1c 08 0e 10 c9 a5 ec 06 18 00 20 00 2a 09 44 "
			+ "64 64 64 64 64 64 64 33 30 10 38 07 40 00 0a 1e "
			+ "08 0e 10 bf f9 1a 18 00 20 00 2a 0c b8 b6 c0 bd "
			+ "b8 b8 ba f1 b4 dc b0 e1 30 0d 38 01 40 00 0a 1a "
			+ "08 0e 10 bb f9 1a 18 00 20 00 2a 08 bb f3 b4 eb "
			+ "c0 fb c0 ce 30 0d 38 02 40 00 0a 19 08 0e 10 a6 "
			+ "e1 49 18 00 20 00 2a 07 59 6f 75 72 6c 69 70 30 "
			+ "0b 38 00 40 00 0a 1f 08 0e 10 ad 87 81 07 18 00 "
			+ "20 00 2a 0c 41 61 77 65 66 69 6f 70 61 77 65 6a "
			+ "30 01 38 07 40 00 10 03 18 00 20 00 00 00";

	private void 화면스폰(L1PcInstance pc, String param) {
		try {
			StringTokenizer st = new StringTokenizer(param);
			int id = Integer.parseInt(st.nextToken(), 10);

			int rnd = 0;
			int rnd2 = 0;
			for (int i = 0; i < id; i++) {
				rnd = _random.nextInt(30);
				rnd2 = _random.nextInt(30);
				L1EffectSpawn.getInstance().spawnEffect(91164, 2000,
						pc.getX() - 15 + rnd, pc.getY() - 15 + rnd2,
						(short) pc.getMapId());
			}
		} catch (Exception exception) {
			pc.sendPackets(new S_SystemMessage(".옵 [숫자] 입력"), true);
		}
	}

	private void 멘트(L1PcInstance pc, String param) {
		try {
			StringTokenizer st = new StringTokenizer(param);
			int id = Integer.parseInt(st.nextToken(), 10);

			pc.sendPackets(new S_ServerMessage(id, null), true);
		} catch (Exception exception) {
			exception.printStackTrace();
			pc.sendPackets(new S_SystemMessage(".옵 [숫자] 입력"), true);
		}
	}

	private void 메세지(L1PcInstance pc, String param) {
		try {
			StringTokenizer st = new StringTokenizer(param);
			int type = Integer.parseInt(st.nextToken(), 10);
			String msg = st.nextToken();

			pc.sendPackets(new S_ServerMessage(type, msg), true);
		} catch (Exception exception) {
			exception.printStackTrace();
			pc.sendPackets(new S_SystemMessage(".옵 [숫자] 입력"), true);
		}
	}

	private static boolean test(String s) {
		boolean flag = true;
		char ac[] = s.toCharArray();
		int i = 0;
		do {
			if (i >= ac.length) {
				break;
			}
			// System.out.println(ac[i]);
			if (!Character.isLetterOrDigit(ac[i])) {
				// System.out.println("걸렸음");
				flag = false;
				break;
			}
			i++;
		} while (true);
		ac = null;
		return flag;
	}

	private void 문패킷(L1PcInstance _pc, L1DoorInstance door) {
		S_Door packet = new S_Door(door);
		_pc.sendPackets(packet);
	}

	private void 옵코드(L1PcInstance pc, String param) {
		try {
			StringTokenizer st = new StringTokenizer(param);
			int type = Integer.parseInt(st.nextToken(), 10);
			Config.test222 = type;
		} catch (Exception exception) {
			exception.printStackTrace();
			pc.sendPackets(new S_SystemMessage(".옵 [숫자] 입력"), true);
		}
	}

	private void byteWrite(int value) {
		int div1 = 64 * 64 * 64 * 64;
		int div2 = 64 * 64 * 64;
		int div3 = 64 * 64;
		int div4 = 64;
		div1 = div1 * 2;
		div2 = div2 * 2;
		div3 = div3 * 2;
		div4 = div4 * 2;

		int i1 = value / div1;
		int i2 = (value % div1) / div2;
		int i3 = (value % div2) / div3;
		int i4 = (value % div3) / div4;
		int i5 = value % div4;

		if (value >= div4)
			System.out.println(64 + i5);
		else if (i5 > 0)
			System.out.println(i5);
		if (value >= div3)
			System.out.println(64 + i4);
		else if (i4 > 0)
			System.out.println(i4);
		if (value >= div2)
			System.out.println(64 + i3);
		else if (i3 > 0)
			System.out.println(i3);
		if (value >= div1)
			System.out.println(64 + i2);
		else if (i2 > 0)
			System.out.println(i2);
		if (value >= div1)
			System.out.println(i1);
	}

	public static int 텔옵 = 0;
	private static boolean in = false;

	private void CodeTest(L1PcInstance pc, String param) {
		try {
			StringTokenizer st = new StringTokenizer(param);
			int codetest = Integer.parseInt(st.nextToken(), 10);
			텔옵 = codetest;
		} catch (Exception exception) {
			pc.sendPackets(new S_SystemMessage(".코드 [숫자] 입력"), true);
		}
	}

	private void Clear(L1PcInstance gm) {
		for (L1Object obj : L1World.getInstance().getVisibleObjects(gm, 15)) { // 10
																				// 범위
																				// 내에
																				// 오브젝트를
																				// 찾아서
			if (obj instanceof L1MonsterInstance) { // 몬스터라면
				L1MonsterInstance npc = (L1MonsterInstance) obj;
				// npc.testonNpcAI(gm);
				npc.receiveDamage(gm, 9999999); // 데미지

				/*
				 * npc.die(gm); npc.die(gm); npc.die(gm); npc.die(gm);
				 * npc.die(gm);
				 */

				gm.sendPackets(new S_SkillSound(obj.getId(), 1815), true);
				Broadcaster.broadcastPacket(gm, new S_SkillSound(obj.getId(),
						1815), true);
			} else if (obj instanceof L1PcInstance) { // pc라면
				L1PcInstance player = (L1PcInstance) obj;
				if (!(obj instanceof L1RobotInstance)) {
					player.receiveDamage(player, 0, false); // 데미지
				}
				gm.sendPackets(new S_SkillSound(obj.getId(), 1815), true);
				Broadcaster.broadcastPacket(gm, new S_SkillSound(obj.getId(),
						1815), true);
			}
		}
	}

	private void castleWarExit(L1PcInstance gm, String param) {
		try {
			StringTokenizer tok = new StringTokenizer(param);
			String name = tok.nextToken();
			WarTimeController.getInstance().setWarExitTime(gm, name);
		} catch (Exception e) {
			gm.sendPackets(new S_SystemMessage(".공성종료 [성이름두글자]"), true);
		}
	}

	private void castleWarStart(L1PcInstance gm, String param) {
		try {
			StringTokenizer tok = new StringTokenizer(param);
			String name = tok.nextToken();
			int minute = Integer.parseInt(tok.nextToken());

			Calendar cal = (Calendar) Calendar.getInstance().clone();
			if (minute != 0)
				cal.add(Calendar.MINUTE, minute);

			CastleTable.getInstance().updateWarTime(name, cal);
			WarTimeController.getInstance().setWarStartTime(name, cal);

			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			gm.sendPackets(new S_SystemMessage(String.format(".공성시간이 %s 으로 변경 되었습니다.", formatter.format(cal.getTime()))), true);
			gm.sendPackets(new S_SystemMessage(param + "분 뒤 공성이 시작합니다."), true);
			formatter = null;
		} catch (Exception e) {
			gm.sendPackets(new S_SystemMessage(".공성시작 [성이름두글자] [분]"), true);
		}
	}

	private void 봇(L1PcInstance gm, String param) {
		StringTokenizer tokenizer = new StringTokenizer(param);
		try {
			String type = tokenizer.nextToken();
			if (type.equalsIgnoreCase("종료")) {
				try {
					String 이름 = tokenizer.nextToken();
					if (이름.equalsIgnoreCase("전체")) {
						int _cnt = 0;
						restartBot = false;
						clanBot = false;
						fishBot = false;
						bugbearBot = false;
						huntBot = false;
						for (L1RobotInstance bot : L1World.getInstance()
								.getAllRobot()) {
							_cnt++;
							L1World world = L1World.getInstance();
							if (bot.사냥봇) {
								if (bot.사냥_종료)
									continue;
								else {
									bot.종료();
									continue;
								}
							}
							bot._스레드종료 = true;
							if (!bot.리스봇 && !bot.낚시봇) {
								for (L1PcInstance pc : L1World.getInstance()
										.getRecognizePlayer(bot)) {
									pc.sendPackets(new S_RemoveObject(bot),
											true);
									pc.getNearObjects().removeKnownObject(bot);
								}
								world.removeVisibleObject(bot);
								world.removeObject(bot);
								bot.getNearObjects().removeAllKnownObjects();
							}
							bot.stopHalloweenRegeneration();
							bot.stopPapuBlessing();
							bot.stopLindBlessing();
							bot.stopHalloweenArmorBlessing();
							bot.stopAHRegeneration();
							bot.stopHpRegenerationByDoll();
							bot.stopMpRegenerationByDoll();
							bot.stopSHRegeneration();
							bot.stopMpDecreaseByScales();
							bot.stopEtcMonitor();
							// if(!bot.리스봇 && !bot.낚시봇)
							// bot.setDead(true);
							if (bot.getClanid() != 0) {
								bot.getClan().removeOnlineClanMember(
										bot.getName());
							}
							Robot.Doll_Delete(bot);
						}
						gm.sendPackets(new S_SystemMessage(_cnt
								+ "마리의 로봇을 종료 시켰습니다."), true);
					} else if (이름.equalsIgnoreCase("사냥")) {
						int _cnt = 0;
						huntBot = false;
						for (L1RobotInstance bot : L1World.getInstance()
								.getAllRobot()) {
							if (!bot.사냥봇 || bot.사냥_종료)
								continue;
							_cnt++;
							bot.종료();
						}
						gm.sendPackets(new S_SystemMessage(_cnt
								+ "마리의 로봇을 종료 시켰습니다."), true);
					} else if (이름.equalsIgnoreCase("리스")) {
						int _cnt = 0;
						restartBot = false;
						for (L1RobotInstance bot : L1World.getInstance()
								.getAllRobot()) {
							if (!bot.리스봇)
								continue;
							_cnt++;
							bot._스레드종료 = true;
							bot.getNearObjects().removeAllKnownObjects();
							bot.stopHalloweenRegeneration();
							bot.stopPapuBlessing();
							bot.stopLindBlessing();
							bot.stopHalloweenArmorBlessing();
							bot.stopAHRegeneration();
							bot.stopHpRegenerationByDoll();
							bot.stopMpRegenerationByDoll();
							bot.stopSHRegeneration();
							bot.stopMpDecreaseByScales();
							bot.stopEtcMonitor();
							Robot.Doll_Delete(bot);
						}
						gm.sendPackets(new S_SystemMessage(_cnt
								+ "마리의 로봇을 종료 시켰습니다."), true);
					} else if (이름.equalsIgnoreCase("버경")) {
						int _cnt = 0;
						bugbearBot = false;
						for (L1RobotInstance bot : L1World.getInstance()
								.getAllRobot()) {
							if (!bot.버경봇)
								continue;
							_cnt++;
							bot._스레드종료 = true;
							bot.getNearObjects().removeAllKnownObjects();
							bot.stopHalloweenRegeneration();
							bot.stopPapuBlessing();
							bot.stopLindBlessing();
							bot.stopHalloweenArmorBlessing();
							bot.stopAHRegeneration();
							bot.stopHpRegenerationByDoll();
							bot.stopMpRegenerationByDoll();
							bot.stopSHRegeneration();
							bot.stopMpDecreaseByScales();
							bot.stopEtcMonitor();
							bot.버경봇_타입 = 0;
							Robot.Doll_Delete(bot);
						}
						gm.sendPackets(new S_SystemMessage(_cnt
								+ "마리의 로봇을 종료 시켰습니다."), true);
					} else if (이름.equalsIgnoreCase("낚시")) {
						int _cnt = 0;
						fishBot = false;
						for (L1RobotInstance bot : L1World.getInstance()
								.getAllRobot()) {
							if (!bot.낚시봇)
								continue;
							_cnt++;
							bot._스레드종료 = true;
							bot.getNearObjects().removeAllKnownObjects();
							bot.stopHalloweenRegeneration();
							bot.stopPapuBlessing();
							bot.stopHalloweenArmorBlessing();
							bot.stopLindBlessing();
							bot.stopAHRegeneration();
							bot.stopHpRegenerationByDoll();
							bot.stopMpRegenerationByDoll();
							bot.stopSHRegeneration();
							bot.stopMpDecreaseByScales();
							bot.stopEtcMonitor();
							Robot.Doll_Delete(bot);
						}
						gm.sendPackets(new S_SystemMessage(_cnt
								+ "마리의 로봇을 종료 시켰습니다."), true);
					} else if (이름.equalsIgnoreCase("군주")) {
						int _cnt = 0;
						clanBot = false;
						for (L1RobotInstance bot : L1World.getInstance()
								.getAllRobot()) {
							if (!bot.가입군주)
								continue;
							_cnt++;
							L1World world = L1World.getInstance();
							bot._스레드종료 = true;
							for (L1PcInstance pc : L1World.getInstance()
									.getRecognizePlayer(bot)) {
								pc.sendPackets(new S_RemoveObject(bot), true);
								pc.getNearObjects().removeKnownObject(bot);
							}
							world.removeVisibleObject(bot);
							world.removeObject(bot);
							bot.stopHalloweenRegeneration();
							bot.stopPapuBlessing();
							bot.stopLindBlessing();
							bot.stopHalloweenArmorBlessing();
							bot.stopAHRegeneration();
							bot.stopHpRegenerationByDoll();
							bot.stopMpRegenerationByDoll();
							bot.stopSHRegeneration();
							bot.stopMpDecreaseByScales();
							bot.stopEtcMonitor();
							if (bot.getClanid() != 0) {
								bot.getClan().removeOnlineClanMember(
										bot.getName());
							}
							Robot.Doll_Delete(bot);
						}
						gm.sendPackets(new S_SystemMessage(_cnt
								+ "마리의 로봇을 종료 시켰습니다."), true);
					} else {
						L1PcInstance 로봇 = L1World.getInstance().getPlayer(이름);
						if (로봇 != null) {
							if (로봇 instanceof L1RobotInstance) {
								L1World world = L1World.getInstance();
								if (((L1RobotInstance) 로봇).사냥봇) {
									if (((L1RobotInstance) 로봇).사냥_종료)
										return;
									else {
										Robot_Hunt.getInstance().delay_spawn(
												((L1RobotInstance) 로봇).사냥봇_위치,
												60000);
										((L1RobotInstance) 로봇).종료();
										return;
									}
								}
								((L1RobotInstance) 로봇)._스레드종료 = true;
								if (!((L1RobotInstance) 로봇).리스봇
										&& !((L1RobotInstance) 로봇).낚시봇) {
									for (L1PcInstance pc : L1World
											.getInstance().getRecognizePlayer(
													로봇)) {
										pc.sendPackets(new S_RemoveObject(로봇),
												true);
										pc.getNearObjects().removeKnownObject(
												로봇);
									}
									world.removeVisibleObject(로봇);
									world.removeObject(로봇);
									로봇.getNearObjects().removeAllKnownObjects();
								}
								로봇.stopHalloweenRegeneration();
								로봇.stopPapuBlessing();
								로봇.stopLindBlessing();
								로봇.stopHalloweenArmorBlessing();
								로봇.stopAHRegeneration();
								로봇.stopHpRegenerationByDoll();
								로봇.stopMpRegenerationByDoll();
								로봇.stopSHRegeneration();
								로봇.stopMpDecreaseByScales();
								로봇.stopEtcMonitor();
								((L1RobotInstance) 로봇).버경봇_타입 = 0;
								((L1RobotInstance) 로봇).loc = null;
								if (로봇.getClanid() != 0) {
									로봇.getClan().removeOnlineClanMember(
											로봇.getName());
								}
								Robot.Doll_Delete((L1RobotInstance) 로봇);
								gm.sendPackets(new S_SystemMessage(이름
										+ "로봇을 종료 시킵니다."), true);
							} else {
								gm.sendPackets(
										new S_SystemMessage("로봇이 아닙니다."), true);
							}
						} else {
							gm.sendPackets(new S_SystemMessage(
									"접속중인 로봇의 이름이 아닙니다."), true);
						}

					}
				} catch (Exception e) {
					gm.sendPackets(new S_SystemMessage("봇 종료 이름/리스/전체/군주"),
							true);
				}
			} else if (type.equalsIgnoreCase("가입")) {
				try {
					String 혈이름 = tokenizer.nextToken();
					String 이름 = tokenizer.nextToken();
					String 호칭 = tokenizer.nextToken();
					L1Clan 혈맹 = L1World.getInstance().getClan(혈이름);
					if (혈맹 == null) {
						gm.sendPackets(new S_SystemMessage(혈이름
								+ "혈맹이 존재하지 않습니다."), true);
						return;
					}
					L1PcInstance 로봇 = L1World.getInstance().getPlayer(이름);
					if (로봇 != null) {
						if (로봇 instanceof L1RobotInstance) {
							L1RobotInstance rob = (L1RobotInstance) 로봇;
							rob.updateclan(혈이름, 혈맹.getClanId(), 호칭, true);
							rob.setClanid(혈맹.getClanId());
							rob.setClanname(혈이름);
							rob.setClanRank(2);
							rob.setTitle(호칭);
							혈맹.addClanMember(rob.getName(), rob.getClanRank(),
									rob.getLevel(), rob.getType(),
									rob.getMemo(), rob.getOnlineStatus(), rob);
							Broadcaster.broadcastPacket(rob, new S_CharTitle(
									rob.getId(), 호칭), true);
							Broadcaster.broadcastPacket(rob,
									new S_Emblem(rob.getClanid()), true);
						} else {
							gm.sendPackets(new S_SystemMessage("로봇이 아닙니다."),
									true);
							return;
						}
					} else {
						gm.sendPackets(
								new S_SystemMessage("접속중인 로봇의 이름이 아닙니다."), true);
						return;
					}
				} catch (Exception e) {
					gm.sendPackets(
							new S_SystemMessage(".봇 가입 [혈이름] [이름] [호칭]"), true);
				}
			} else if (type.equalsIgnoreCase("추방")) {
				try {
					String 이름 = tokenizer.nextToken();
					L1PcInstance 로봇 = L1World.getInstance().getPlayer(이름);
					if (로봇 != null) {
						if (로봇 instanceof L1RobotInstance) {
							L1RobotInstance rob = (L1RobotInstance) 로봇;
							rob.updateclan("", 0, "", false);
							rob.setClanid(0);
							rob.setClanname("");
							rob.setTitle("");
							rob.setClanRank(0);
							Broadcaster.broadcastPacket(rob, new S_CharTitle(
									rob.getId(), ""), true);
							Broadcaster.broadcastPacket(rob, new S_Emblem(0),
									true);
						} else {
							gm.sendPackets(new S_SystemMessage("로봇이 아닙니다."),
									true);
							return;
						}
					} else {
						gm.sendPackets(
								new S_SystemMessage("접속중인 로봇의 이름이 아닙니다."), true);
						return;
					}
				} catch (Exception e) {
					gm.sendPackets(new S_SystemMessage(".봇 추방 [이름]"), true);
				}
			} else if (type.equalsIgnoreCase("압류")) {
				String 이름 = tokenizer.nextToken();
				L1PcInstance 로봇 = L1World.getInstance().getPlayer(이름);
				if (로봇 != null) {
					if (로봇 instanceof L1RobotInstance) {
						L1World world = L1World.getInstance();
						((L1RobotInstance) 로봇)._스레드종료 = true;
						for (L1PcInstance pc : L1World.getInstance()
								.getRecognizePlayer(로봇)) {
							pc.sendPackets(new S_RemoveObject(로봇), true);
							pc.getNearObjects().removeKnownObject(로봇);
						}
						world.removeVisibleObject(로봇);
						world.removeObject(로봇);
						로봇.getNearObjects().removeAllKnownObjects();
						로봇.stopHalloweenRegeneration();
						로봇.stopPapuBlessing();
						로봇.stopLindBlessing();
						로봇.stopHalloweenArmorBlessing();
						로봇.stopAHRegeneration();
						로봇.stopHpRegenerationByDoll();
						로봇.stopMpRegenerationByDoll();
						로봇.stopSHRegeneration();
						로봇.stopMpDecreaseByScales();
						로봇.stopEtcMonitor();
						로봇.setDead(true);
						gm.sendPackets(
								new S_SystemMessage(이름 + "로봇을 압류 시킵니다."), true);
						((L1RobotInstance) 로봇).updateban(true);
					} else {
						gm.sendPackets(new S_SystemMessage("로봇이 아닙니다."), true);
					}
				} else {
					gm.sendPackets(new S_SystemMessage("접속중인 로봇의 이름이 아닙니다."),
							true);
				}
			} else if (type.equalsIgnoreCase("손")) {
				로봇부처핸섬 bot = new 로봇부처핸섬();
				GeneralThreadPool.getInstance().execute(bot);
			} else {
				gm.sendPackets(new S_SystemMessage(".봇 종료 / 압류"), true);
			}
		} catch (Exception e) {
			gm.sendPackets(new S_SystemMessage(".봇 종료 / 압류"), true);
		}
	}

	private void updateClanId(L1PcInstance player) {
		// TODO 자동 생성된 메소드 스텁
		Connection con = null;
		PreparedStatement pstm = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con
					.prepareStatement("UPDATE characters SET ClanID=? WHERE Clanname=?");
			pstm.setInt(1, player.getClanid());
			pstm.setString(2, player.getClanname());
			pstm.executeUpdate();
		} catch (SQLException e) {
			_log.log(Level.SEVERE, e.getLocalizedMessage(), e);
		} finally {
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
	}

	private void robotClanIdChange(L1PcInstance player) {
		// TODO 자동 생성된 메소드 스텁
		Connection con = null;
		PreparedStatement pstm = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con
					.prepareStatement("UPDATE robots_crown SET clanid=? WHERE clanname=?");
			pstm.setInt(1, player.getClanid());
			pstm.setString(2, player.getClanname());
			pstm.executeUpdate();
		} catch (SQLException e) {
			_log.log(Level.SEVERE, e.getLocalizedMessage(), e);
		} finally {
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con
					.prepareStatement("UPDATE robots SET clanid=? WHERE clanname=?");
			pstm.setInt(1, player.getClanid());
			pstm.setString(2, player.getClanname());
			pstm.executeUpdate();
		} catch (SQLException e) {
			_log.log(Level.SEVERE, e.getLocalizedMessage(), e);
		} finally {
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
	}

	private static int gfxii = 0;
	private void 이미지체크(L1PcInstance pc) {
		try {
			int gfxid = gfxii;
			int count = 55;
			int pcX = pc.getX();
			int pcY = pc.getY() - 11;
			L1Npc l1npc = null;
			Constructor<?> constructor = null;
			L1NpcInstance npc = null;
			for (int i = 0; i < count; i++) {
				l1npc = NpcTable.getInstance().getTemplate(45001);
				if (l1npc != null) {
					String s = l1npc.getImpl();
					constructor = Class.forName(
							"l1j.server.server.model.Instance." + s
									+ "Instance").getConstructors()[0];
					Object aobj[] = { l1npc };
					npc = (L1NpcInstance) constructor.newInstance(aobj);
					npc.setId(ObjectIdFactory.getInstance().nextId());
					npc.getGfxId().setGfxId(gfxid + i);
					npc.getGfxId().setTempCharGfx(0);
					npc.setNameId("" + gfxii + "");
					npc.setMap(pc.getMapId());
					int e = i % 7;
					if (e == 0 && i > 0) {
						pcX -= 2;
						pcY += 2;
					}
					npc.setX(pcX + e * 2);
					npc.setY(pcY + e * 2);
					npc.setHomeX(npc.getX());
					npc.setHomeY(npc.getY());
					npc.getMoveState().setHeading(4);

					L1World.getInstance().storeObject(npc);
					L1World.getInstance().addVisibleObject(npc);

					L1NpcDeleteTimer timer = new L1NpcDeleteTimer(npc, 7000);
					timer.begin();

					gfxii++;
					if (gfxii == 12499)
						break;
				}
			}
		} catch (Exception e) {
		}
	}
	
	public void Petlevelup(L1PcInstance gm, String arg) {
		try {
			StringTokenizer tok = new StringTokenizer(arg);
			String user = tok.nextToken();
			L1PcInstance target = L1World.getInstance().getPlayer(user);
			int level = Integer.parseInt(tok.nextToken());
			
			if (!IntRange.includes(level, 1, 99)) {
				gm.sendPackets(new S_SystemMessage("1-99의 범위에서 지정해 주세요"));
				return;
			}
			
			if (target == null) {
				gm.sendPackets(new S_SystemMessage(user + " 은(는) 접속중이지 않습니다."));
				return;
			}
			
			L1PetInstance Pet = (L1PetInstance)target.getPet();
			if (Pet != null) {
				if (level <= Pet.getLevel()) {
					gm.sendPackets(new S_SystemMessage(target.getName() +"의 현재 펫 레벨보다 지정한 레벨이 더 낮습니다."));
					return;
				}

				L1ItemInstance item = target.getInventory().getItem(Pet.getItemObjId());
				int exp = ExpTable.getExpByLevel(level);
				int levelBefore = Pet.getLevel();
				exp = (int) (exp + 0.01D * ExpTable.getNeedExpNextLevel(level));
				Pet.setExp(exp);
				Pet.setLevel(ExpTable.getLevelByExp(exp));
				target.sendPackets(new S_PetWindow(S_PetWindow.PatExp, Pet), true);
				try {
					target.sendPackets(new S_PetPack(Pet, target), true);
					
					int gap = Pet.getLevel() - levelBefore;
					for (int i = 1; i <= gap; i++) {
						IntRange hpUpRange = Pet.getPetType().getHpUpRange();
						Pet.addMaxHp(hpUpRange.randomValue());
						Pet.setCurrentHp(Pet.getMaxHp());
					}
					
					Pet.getPetAc();
					Pet.getPetMr();
					int BonusPoint = Pet.getHunt() + Pet.getSurvival() + Pet.getSacred();
					int BonusPointTemp = 0;
					if (Pet.getLevel() > 50) {
						BonusPointTemp = (5 + (Pet.getLevel() - 50)) - BonusPoint;
					} else
						BonusPointTemp = (Pet.getLevel() / 10) - BonusPoint;
					if (BonusPointTemp > 0)
						Pet.setBonusPoint(BonusPointTemp);
					PetTable.UpDatePet(Pet);
					target.sendPackets(new S_ItemName(item), true);
					;
					target.sendPackets(new S_PetWindow(S_PetWindow.PatLevel, Pet), true);
					target.sendPackets(new S_PetWindow(S_PetWindow.PatStatUpDate, Pet), true);
					try {
						target.sendPackets(new S_ServerMessage(320, Pet.getName()), true);
					} catch (Exception e) {
					}
				} catch (Exception e) {
				}
				gm.sendPackets(new S_SystemMessage(target.getName() + "님의 펫(" + Pet.getName() + ")레벨이 변경됨!"));
			} else {
				gm.sendPackets(new S_SystemMessage(target.getName() +" 은(는) 펫을 소환중이지 않습니다."));
			}
		} catch (Exception e) {
			gm.sendPackets(new S_SystemMessage(".펫레벨 [캐릭명] [레벨] 입력"));
		}
	}
	
	private void Fpoint(L1PcInstance gm, String param) {
		try {
			StringTokenizer st = new StringTokenizer(param);
			String charname = st.nextToken();
			int count = Integer.parseInt(st.nextToken());
			L1PcInstance pc = L1World.getInstance().getPlayer(charname);
			if (pc != null) {
				L1PetInstance Pet = (L1PetInstance)pc.getPet(); 
				if (Pet != null) {
				Pet.setFriendship(Pet.getFriendship() + count);
				pc.sendPackets(new S_PetWindow(S_PetWindow.PatPoint, Pet), true);
				gm.sendPackets(new S_SystemMessage(pc.getName() + "에게 우정포인트(" + count + ") 지급 하였습니다."));
				pc.sendPackets(new S_SystemMessage("\\aA당신에게 메티스님께서 우정포인트 '\\aG(" + count + ")\\aA' 지급 하였습니다."));
				} else {
					gm.sendPackets(new S_SystemMessage(charname +" 은(는) 펫을 소환중이지 않습니다."));
				}
			} else
				gm.sendPackets(new S_SystemMessage(charname +" 은(는) 접속중이지 않습니다."));
		} catch (Exception e) {
			gm.sendPackets(new S_SystemMessage(".펫우정 [캐릭명] [숫자] 입력"));
		}
	}
	
	private void FSkill(L1PcInstance gm, String param) {
		try {
			StringTokenizer st = new StringTokenizer(param);
			String charname = st.nextToken();
			int enchant = Integer.parseInt(st.nextToken());
			L1PcInstance pc = L1World.getInstance().getPlayer(charname);
			if (pc != null) {
				L1PetInstance Pet = (L1PetInstance)pc.getPet(); 
				if (Pet != null) {
					Pet = (L1PetInstance) gm.getPet();
					PetsSkillsTable.SaveSkillMaster(Pet, enchant);
					Pet.deletePet();
					PetSummons.UsePetSummons(gm, Pet.getItemObjId());
				gm.sendPackets(new S_SystemMessage(pc.getName() + "의 펫스킬 레벨을 ("+enchant+")으로 만들었습니다."));
				pc.sendPackets(new S_SystemMessage("\\aA당신에게 메티스님께서 펫스킬 레벨을 ("+enchant+")으로 만들었습니다."));
				} else {
					gm.sendPackets(new S_SystemMessage(charname +" 은(는) 펫을 소환중이지 않습니다."));
				}
			} else
				gm.sendPackets(new S_SystemMessage(charname +" 은(는) 접속중이지 않습니다."));
		} catch (Exception e) {
			gm.sendPackets(new S_SystemMessage(".펫스킬 [캐릭명] [레벨] 입력"));
		}
	}		
}
class 나발등장 implements Runnable {
	L1NpcInstance _npc = null;
	int _x = 0;
	int _y = 0;
	int _mapid = 0;

	public 나발등장() {
		_x = 32669;
		_y = 32843;
		_mapid = 12859;
	}

	@Override
	public void run() {
		while (true) {
			try {
				
				for(L1PcInstance pc : L1World.getInstance().getAllPlayers()) {
					if (pc.getMapId() == 12859){
					pc.sendPackets(new S_PacketBox(S_PacketBox.GREEN_MESSAGE,S_PacketBox.RED_MESSAGE, "나이트 발드 : 왔는가.. 어디, 실력을 보여보아라..", true));
					}
				}
				L1SpawnUtil.spawn2(_x-4, _y+2, (short) _mapid, 47101, 0, 5000, 0);
				L1SpawnUtil.spawn2(_x, _y-5, (short) _mapid, 47101, 0, 6000, 0);
				L1SpawnUtil.spawn2(_x+5, _y, (short) _mapid, 47101, 0, 8000, 0);
				Thread.sleep(5000);
				for(L1PcInstance pc : L1World.getInstance().getAllPlayers()) {
					pc.sendPackets(new S_EffectLocation(_x-4, _y+2, (short) 12189), true);
				}
				Thread.sleep(1000);
				for(L1PcInstance pc : L1World.getInstance().getAllPlayers()) {
					pc.sendPackets(new S_EffectLocation(_x, _y-5, (short) 12189), true);
				}
				Thread.sleep(2000);
				for(L1PcInstance pc : L1World.getInstance().getAllPlayers()) {
					pc.sendPackets(new S_EffectLocation(_x+5, _y, (short) 12189), true);
				}
				 L1SpawnUtil.spawn2(_x, _y, (short) _mapid, 78007, 0, 3600*1000, 0);//나이트발드
					L1World.getInstance().broadcastServerMessage("\\aI[보스 알림]: 어둠의 나이트발드 (지배의 탑 8층)");
					L1World.getInstance().broadcastPacketToAll(new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "\\fO[보스 알림]: 어둠의 나이트발드 (지배의 탑 8층)"), true);
				break;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
class 우그등장 implements Runnable {
	L1NpcInstance _npc = null;
	int _x = 0;
	int _y = 0;
	int _mapid = 0;

	public 우그등장() {
		_x = 32801;
		_y = 32786;
		_mapid = 12861;
	}

	@Override
	public void run() {
		while (true) {
			try {
				for(L1PcInstance pc : L1World.getInstance().getAllPlayers()) {
					if (pc.getMapId() == 12861){
						pc.sendPackets(new S_PacketBox(S_PacketBox.GREEN_MESSAGE,S_PacketBox.RED_MESSAGE, "우그누스 : 제물이 되고자 찾아온 자들인가.", true));
						L1SpawnUtil.spawn2(_x, _y, (short) _mapid, 202066, 0, 1 * 1000, 202066);
					}
				}
				Thread.sleep(5000);
				
				L1SpawnUtil.spawn2(_x, _y, (short) _mapid, 78009, 0, 3600*1000, 0);//우그누스
				
				Thread.sleep(500);
				for(L1PcInstance pc : L1World.getInstance().getAllPlayers()) {
					pc.sendPackets(new S_EffectLocation(32798, 32784, (short) 17081), true);
				}
				Thread.sleep(500);
				for(L1PcInstance pc : L1World.getInstance().getAllPlayers()) {
					pc.sendPackets(new S_EffectLocation(32803, 32787, (short) 17081), true);
				}
				Thread.sleep(500);
				for(L1PcInstance pc : L1World.getInstance().getAllPlayers()) {
					pc.sendPackets(new S_EffectLocation(32803, 32784, (short) 17081), true);
				}
				Thread.sleep(500);
				for(L1PcInstance pc : L1World.getInstance().getAllPlayers()) {
					pc.sendPackets(new S_EffectLocation(32800, 32788, (short) 17081), true);
				}
				Thread.sleep(500);
				for(L1PcInstance pc : L1World.getInstance().getAllPlayers()) {
					pc.sendPackets(new S_EffectLocation(32800, 32783, (short) 17081), true);
				}
				L1World.getInstance().broadcastServerMessage("\\aI[보스 알림]: 오만한 우그누스가 출현합니다. (지배의 탑 10층)");
				L1World.getInstance().broadcastPacketToAll(new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "\\fO[보스 알림]: 오만한 우그누스가 출현합니다. ( 지배의 탑 10층 )"), true);
				break;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}

class 리퍼등장 implements Runnable {

	public 리퍼등장() {
	
	}

	@Override
	public void run() {
		while (true) {
			try {
				
				L1SpawnUtil.spawn2(32713, 32902, (short) 12862, 78011, 0, 3600*1000, 0);//포악한 사신의 영혼
				L1SpawnUtil.spawn2(32722, 32915, (short) 12862, 78012, 0, 3600*1000, 0);//간악한 사신의 영혼
				
				for(L1PcInstance pc : L1World.getInstance().getAllPlayers()) {
					if (pc.getMapId() == 12862){
						pc.sendPackets(new S_PacketBox(S_PacketBox.GREEN_MESSAGE,S_PacketBox.RED_MESSAGE, "\\aI포악한 사신의 영혼 : 여기까지 오다니.. 훌륭한 영혼이구나..", true));
					} else {
						pc.sendPackets(new S_PacketBox(S_PacketBox.GREEN_MESSAGE,S_PacketBox.RED_MESSAGE, "\\fO[보스 알림]: 사신 그림리퍼 (지배의 탑 정상)", true));
					}
				}
				L1World.getInstance().broadcastServerMessage("\\aI[보스 알림]: 사신 그림리퍼 (지배의 탑 정상)");
				Thread.sleep(10000);
				for(L1PcInstance pc : L1World.getInstance().getAllPlayers()) {
					if (pc.getMapId() == 12862){
						pc.sendPackets(new S_PacketBox(S_PacketBox.GREEN_MESSAGE,S_PacketBox.RED_MESSAGE, "간악한 사신의 영혼 : 크크.. 좋은 제물이 되겠군...", true));
					}
				}
				break;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
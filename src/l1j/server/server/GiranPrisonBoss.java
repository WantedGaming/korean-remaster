package l1j.server.server;

import java.util.Calendar;
import java.util.Random;

import l1j.server.server.types.Point;
import l1j.server.server.utils.L1SpawnUtil; 

public class GiranPrisonBoss extends Thread {
	
		private static GiranPrisonBoss _instance;

		private boolean _boss;
		public boolean getGiranPrisonBoss() {
			return _boss;
		}
		public void setGiranPrisonBoss(boolean boss) {
			_boss = boss;
		}

		public boolean isGmOpen = false;

		private static Random _random = new Random(System.nanoTime());
		
		public static GiranPrisonBoss getInstance() {
			if(_instance == null) {
				_instance = new GiranPrisonBoss();
			}
			return _instance;
		}
		
		@Override
		public void run() {
			try	{
				while (true) {
					Thread.sleep(1000);
					if (is타로스()){
						Spawn타로스();
					}
				}
			} catch(Exception e){
				e.printStackTrace();
			}
		}
		
		private static final Point[] BOSS_XY = {
			new Point(32804, 32790), new Point(32791, 32731),
			new Point(32742, 32732), new Point(32736, 32792),};
		
		private void Spawn타로스(){
			int[] loc = new int[2];
			int rnd = _random.nextInt(BOSS_XY.length);
			loc[0] = BOSS_XY[rnd].getX();
			loc[1] = BOSS_XY[rnd].getY();
		    try {
				L1SpawnUtil.spawn2(loc[0], loc[1], (short) 54, 46025, 0, 3600000, 0);
				Thread.sleep(80 * 1000);
			} catch(Exception e2){
				e2.printStackTrace();
			}
		}	 
		
		private boolean is타로스() {
			Calendar calender = Calendar.getInstance();
			int hour, minute;
			hour = calender.get(Calendar.HOUR_OF_DAY);
			minute = calender.get(Calendar.MINUTE);
			if (hour == 21 && minute == 01)  {
				return true;
				}
			return false;
		}
		
		/** 종료 **/
		public void End() {
			setGiranPrisonBoss(false);
		}
			
}
package l1j.server.server;

import java.util.Calendar;
import java.util.Locale;
import java.util.Random;
import java.text.SimpleDateFormat;

import server.manager.eva;

import l1j.server.server.model.L1World;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.serverpackets.S_Message_YN;
import l1j.server.server.serverpackets.S_PacketBox;
import l1j.server.server.utils.L1SpawnUtil;

public class LadunBossController extends Thread {
	
		private static LadunBossController _instance;
		private static Random _random = new Random(System.nanoTime());
		
		private boolean _LadunBossStart;
		public boolean getLadunBossStart() {
			return _LadunBossStart;
		}
		public void setLadunBossStart(boolean LadunBoss) {
			_LadunBossStart = LadunBoss;
		}

		public boolean isGmOpen = false;
		
		public static LadunBossController getInstance() {
			if(_instance == null) {
				_instance = new LadunBossController();
			}
			return _instance;
		}
		
		@Override
			public void run() {
			while (true) {
				try	{
				if(isOpen()){
				Thread.sleep(1000);
				Spawn1();
				Thread.sleep(1200000);
				End();
			}
		} catch(Exception e){
			e.printStackTrace();
		} finally{
			try{
				Thread.sleep(1000);
			}catch(Exception e){
			}
			}
		}
	}

			 private void Spawn1(){ 
				 int mon_num = 45955;
				 String mon_name = null;
				 try{
					 mon_num = _random.nextInt(7)+45955;
					 
					 if(mon_num == 45955){
						 mon_name = "대법관 케이나";
					 } else if(mon_num == 45956){
						 mon_name = "대법관 비아타스";
					 } else if(mon_num == 45957){
						 mon_name = "대법관 바로메스";
					 } else if(mon_num == 45958){
						 mon_name = "대법관 엔디아스";
					 } else if(mon_num == 45959){
						 mon_name = "대법관 이데아";
					 } else if(mon_num == 45960){
						 mon_name = "대법관 티아메스";
					 } else if(mon_num == 45961){
						 mon_name = "대법관 라미아스";
					 } else if(mon_num == 45962){
						 mon_name = "대법관 바로드";
					 }
					 
					/**L1World.getInstance().broadcastServerMessage("\\aI[보스 알림]: "+mon_name+"(라스타바드 중앙광장)");
					L1World.getInstance().broadcastPacketToAll(new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "\\fO[보스 알림]: "+mon_name+"(라스타바드 중앙광장)"), true);
					eva.BossLogAppend("[보스스폰] "+mon_name);	**/
					L1SpawnUtil.spawn2(32837, 32769, (short) 479, mon_num, 0, 3600*1000, 0);//대법관 케이나
				 }catch(Exception e2){
						e2.printStackTrace();
				 }
				}
			 
			 private boolean isOpen() {
				  Calendar calender = Calendar.getInstance();
				  int hour, minute;
				  hour = calender.get(Calendar.HOUR_OF_DAY);
				  minute = calender.get(Calendar.MINUTE);	 
			   if ((hour == 0 && minute == 20) ||
					   (hour == 3 && minute == 20)||
					   (hour == 6 && minute == 20)||
					   (hour == 9 && minute == 20)||
					   (hour == 12 && minute == 20)||
					   (hour == 15 && minute == 20)||
					   (hour == 18 && minute == 20)||
					   (hour == 21 && minute == 20)) {
				  return true;
				  }
				  return false;
				 }
			 
			 /** 종료 **/
			 public void End() {
				 setLadunBossStart(false);
			 }
}
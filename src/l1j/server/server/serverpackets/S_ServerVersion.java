package l1j.server.server.serverpackets;


import l1j.server.server.Opcodes;
import l1j.server.server.model.gametime.GameTimeClock;

public class S_ServerVersion extends ServerBasePacket {
	private static final String S_SERVER_VERSION = "[S] ServerVersion";

	public S_ServerVersion() {    //170106 ����
		int time = (int) GameTimeClock.getInstance().getGameTime().getSeconds();
		time = time - (time % 300);
		int uptime = (int) (System.currentTimeMillis() / 1000L);
		writeC(Opcodes.S_EXTENDED_PROTOBUF);
//		35 
//		03 
		writeC(0x35);
		writeC(0x03);
//		08 00 
		writeC(0x08);
		writeC(0x00);
//		10 02 
		writeC(0x10);
		writeC(0x02);
//		b9 fc c5 ad 06 
		writeC(0x18);
		writeC(0xb9);
		writeC(0xfc);
		writeC(0xc5);
		writeC(0xad);
		writeC(0x06);
//		20 b9 fc c5 ad 06 
		writeC(0x20);
		writeC(0xb9);
		writeC(0xfc);
		writeC(0xc5);
		writeC(0xad);
		writeC(0x06);
//		28 fd ac ef c0 07 
		writeC(0x28);
		writeC(0xfd);
		writeC(0xac);
		writeC(0xef);
		writeC(0xc0);
		writeC(0x07);
//		30 b9 fc c5 ad 06 
		writeC(0x30);
		writeC(0xb9);
		writeC(0xfc);
		writeC(0xc5);
		writeC(0xad);
		writeC(0x06);
//		38 b3 ed d5 c3 05 //time
		writeC(0x38);
		writeBit(time);
//		40 00 
		writeC(0x40);
		writeC(0x00);
//		48 00 
		writeC(0x48);
		writeC(0x00);
//		50 8b fb ff a7 03 
		writeC(0x50);
		writeC(0x8b);
		writeC(0xfb);
		writeC(0xff);
		writeC(0xa7);
		writeC(0x03);
//		58 f7 b9 e1 c3 05 //uptime
		writeC(0x58);
		writeBit(uptime);
//		60 fc 97 87 48 
		writeC(0x60);
		writeC(0xfc);
		writeC(0x97);
		writeC(0x87);
		writeC(0x48);
//		68 95 cc e4 4c 
		writeC(0x68);
		writeC(0x95);
		writeC(0xcc);
		writeC(0xe4);
		writeC(0x4c);
//		70 94 bd e9 4c 
		writeC(0x70);
		writeC(0x94);
		writeC(0xbd);
		writeC(0xe9);
		writeC(0x4c);
//		78 da d8 8e ab 06 
		writeC(0x78);
		writeC(0x99);
		writeC(0x94);
		writeC(0x8a);
		writeC(0xab);
		writeC(0x06);
//		80 01 f4 89 c6 4c
		writeC(0x80);
		writeC(0x01);
		writeC(0xf4);
		writeC(0x89);
		writeC(0xc6);
		writeC(0x4c);
//		88 01 00 58 2b
		writeC(0x88);
		writeC(0x01);
		writeC(0x00);
		writeC(0x01);
		writeC(0x00);
	}


	@Override
	public byte[] getContent() {
		return getBytes();
	}

	@Override
	public String getType() {
		return S_SERVER_VERSION;
	}
}
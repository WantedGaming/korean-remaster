package l1j.server.server.serverpackets;

import l1j.server.server.Opcodes;

public class S_CharRegistNotice extends ServerBasePacket {

	private byte[] _byte = null;

	public static final int REGIST = 1;
	public static final int HIT = 2;
	public static final int ALL = 3;
	
	public static final int CHAR_REGIST_NOTICE = 1015;
	private static final String S_CharRegistNotice = "S_CharRegistNotice";

	public S_CharRegistNotice(int type, int type2, int code, int value) {
		writeC(Opcodes.S_EXTENDED_PROTOBUF);
		writeH(type);
		switch (type) {
		case CHAR_REGIST_NOTICE:
			if (type2 == REGIST) { // 내성 종류
				writeC(0x0a);
				writeBit(0x04);
				writeC(0x08);
				writeBit(code); // 1:기술,2:정령,3:용언,4:공포
				writeC(0x10);
				writeBit(value);
			} else if (type2 == HIT) {
				writeC(0x12);
				writeBit(0x04);
				writeC(0x08);
				writeBit(code); // 1:기술,2:정령,3:용언,4:공포
				writeC(0x10);
				writeBit(value);
			}

			break;
		}
		writeH(0);
	}

	@Override
	public byte[] getContent() {
		if (_byte == null) {
			_byte = getBytes();
		}
		return _byte;
	}

	public String getType() {
		return S_CharRegistNotice;
	}
}

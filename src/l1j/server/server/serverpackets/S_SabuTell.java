package l1j.server.server.serverpackets;

import kr.PeterMonk.ClanBuffSystem.ClanBuffTable;
import kr.PeterMonk.ClanBuffSystem.ClanBuffTable.ClanBuff;
import l1j.server.server.Opcodes;
import l1j.server.server.clientpackets.C_SabuTeleport;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.model.skill.L1SkillId;
import l1j.server.server.model.skill.L1SkillUse;

public class S_SabuTell extends ServerBasePacket {

	private static final String S_SabuTell = "[S] S_SabuTell";
	private byte[] _byte = null;

	public S_SabuTell(L1PcInstance pc) {

		if (pc.텔대기() || pc.isTeleport() || pc.isDead() || pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.데스페라도)) {
			return;
		}
		if (pc.getTelType() != 4) { // 빽스텝
			pc.getSkillEffectTimerSet().setSkillEffect(L1SkillId.ABSOLUTE_BARRIER, 500);
			pc.setTeleport(true);
		}
		pc.텔대기(true);
		if (pc.getTelType() == 1) {
			writeC(Opcodes.S_PORTAL);
			writeH(0x00);
			writeD(0x00);
		} else {
			writeC(Opcodes.S_REQUEST_SUMMON);
			writeH(0x00);
			pc.setTeleLockCheck();
		}
		
		if (pc.getClan() != null) {
			if (pc.getClan().getEinhasadBlessBuff() != 0) {
				ClanBuff Buff = ClanBuffTable.getBuffList(pc.getClan().getEinhasadBlessBuff());
				String[] Buffmap = null;
				Buffmap = Buff.buffmaplist.split(",");
				if (pc.getClan().getEinhasadBlessBuff() != 0) {
					for (int j = 0; j < Buffmap.length; j++) {
						int map_id = 0;
						map_id = Integer.parseInt(Buffmap[j]);
						if (pc.dm == map_id) {
							int mapnum = map_id;
							if (pc.dm == mapnum && pc.getClanBuffMap() == 0) {
								pc.setClanBuffMap(mapnum);
								pc.getResistance().addAinBooster(5);
								pc.sendPackets(new S_ACTION_UI(S_ACTION_UI.EINHASAD, pc));
							}
						}
					}
				}

				if (pc.dm != pc.getClanBuffMap() && pc.getClanBuffMap() != 0) {
					pc.setClanBuffMap(0);
					pc.getResistance().addAinBooster(-5);
					pc.sendPackets(new S_ACTION_UI(S_ACTION_UI.EINHASAD, pc));
				}
			}
		}
		
	}

	public S_SabuTell(L1PcInstance pc, int time) {
		if (pc.텔대기() || pc.isTeleport() || pc.isDead()) {
			return;
		}
		if (pc.getTelType() != 4) { // 빽스텝
			pc.getSkillEffectTimerSet().setSkillEffect(L1SkillId.STATUS_안전모드, time);
			pc.setTeleport(true);
		}
		pc.텔대기(true);
		// if(pc.getTelType() == 1 || pc.getTelType() == 4 || pc.getTelType() == 10){
		if (pc.getTelType() == 1) {
			writeC(Opcodes.S_PORTAL);
			writeH(0x00);
			writeD(0x00);
		} else {
			writeC(Opcodes.S_REQUEST_SUMMON);
			writeH(0x00);
			pc.setTeleLockCheck();
		}
		
		if (pc.getClan() != null) {
			if (pc.getClan().getEinhasadBlessBuff() != 0) {
				ClanBuff Buff = ClanBuffTable.getBuffList(pc.getClan().getEinhasadBlessBuff());
				String[] Buffmap = null;
				Buffmap = Buff.buffmaplist.split(",");
				if (pc.getClan().getEinhasadBlessBuff() != 0) {
					for (int j = 0; j < Buffmap.length; j++) {
						int map_id = 0;
						map_id = Integer.parseInt(Buffmap[j]);
						if (pc.dm == map_id) {
							int mapnum = map_id;
							if (pc.dm == mapnum && pc.getClanBuffMap() == 0) {
								pc.setClanBuffMap(mapnum);
								pc.getResistance().addAinBooster(5);
								pc.sendPackets(new S_ACTION_UI(S_ACTION_UI.EINHASAD, pc));
							}
						}
					}
				}

				if (pc.dm != pc.getClanBuffMap() && pc.getClanBuffMap() != 0) {
					pc.setClanBuffMap(0);
					pc.getResistance().addAinBooster(-5);
					pc.sendPackets(new S_ACTION_UI(S_ACTION_UI.EINHASAD, pc));
				}
			}
		}
	}

	// 순줌 관련 텔
	public S_SabuTell(L1PcInstance pc, boolean ck) {
		if (pc.텔대기() || pc.isTeleport() || pc.isDead()) {
			return;
		}
		if (pc.getTelType() != 4) { // 빽스텝
			pc.getSkillEffectTimerSet().setSkillEffect(L1SkillId.STATUS_안전모드,
					500);
			pc.setTeleport(true);
		}
		pc.텔대기(true);
		if (pc.getTelType() == 1) {
			writeC(Opcodes.S_PORTAL);
			writeH(0x00);
			writeD(0x00);
		} else {
			new C_SabuTeleport(new byte[1], pc.getNetConnection());
		}
		
		if (pc.getClan() != null) {
			if (pc.getClan().getEinhasadBlessBuff() != 0) {
				ClanBuff Buff = ClanBuffTable.getBuffList(pc.getClan().getEinhasadBlessBuff());
				String[] Buffmap = null;
				Buffmap = Buff.buffmaplist.split(",");
				if (pc.getClan().getEinhasadBlessBuff() != 0) {
					for (int j = 0; j < Buffmap.length; j++) {
						int map_id = 0;
						map_id = Integer.parseInt(Buffmap[j]);
						if (pc.dm == map_id) {
							int mapnum = map_id;
							if (pc.dm == mapnum && pc.getClanBuffMap() == 0) {
								pc.setClanBuffMap(mapnum);
								pc.getResistance().addAinBooster(5);
								pc.sendPackets(new S_ACTION_UI(S_ACTION_UI.EINHASAD, pc));
							}
						}
					}
				}

				if (pc.dm != pc.getClanBuffMap() && pc.getClanBuffMap() != 0) {
					pc.setClanBuffMap(0);
					pc.getResistance().addAinBooster(-5);
					pc.sendPackets(new S_ACTION_UI(S_ACTION_UI.EINHASAD, pc));
				}
			}
		}
	}

	@Override
	public byte[] getContent() {
		if (_byte == null) {
			_byte = getBytes();
		}
		return _byte;
	}

	@Override
	public String getType() {
		return S_SabuTell;
	}
}

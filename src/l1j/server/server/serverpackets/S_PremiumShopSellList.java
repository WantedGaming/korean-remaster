/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */
package l1j.server.server.serverpackets;

import java.io.IOException;
import java.util.List;

import javolution.util.FastTable;
import l1j.server.Config;
import l1j.server.server.Opcodes;
import l1j.server.server.datatables.ItemTable;
import l1j.server.server.datatables.ShopTable;
import l1j.server.server.model.ItemClientCode;
import l1j.server.server.model.L1Object;
import l1j.server.server.model.L1World;
import l1j.server.server.model.Instance.L1ItemInstance;
import l1j.server.server.model.Instance.L1MerchantInstance;
import l1j.server.server.model.Instance.L1NpcInstance;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.model.shop.L1Shop;
import l1j.server.server.templates.L1Item;
import l1j.server.server.templates.L1ShopItem;

public class S_PremiumShopSellList extends ServerBasePacket {

	/**
	 * 가게의 물건 리스트를 표시한다. 캐릭터가 BUY 버튼을 눌렀을 때에 보낸다.
	 */
	public S_PremiumShopSellList(int objId, L1PcInstance pc) {
		writeC(Opcodes.S_BUY_LIST);
		writeD(objId);

		L1Object npcObj = L1World.getInstance().findObject(objId);
		if (!(npcObj instanceof L1NpcInstance)) {
			writeH(0);
			return;
		}
		int npcId = ((L1NpcInstance) npcObj).getNpcTemplate().get_npcId();

		L1Shop shop = ShopTable.getInstance().get(npcId);
		FastTable<L1ShopItem> shopItems = new FastTable<L1ShopItem>();
		try {
			shopItems.addAll(shop.getSellingItems());
			for (L1ShopItem si : shopItems.toArray(new L1ShopItem[shopItems.size()])) {
				if (L1MerchantInstance.getOneDayBuy(pc.getAccountName(), npcId, si.getItemId())) {
					shopItems.remove(si);
				}
			}
		} catch (Exception e) {
			System.out.println("S_ShopSellList 오류 엔피시번호 : " + npcId);
		}
		writeH(shopItems.size());
		// L1ItemInstance의 getStatusBytes를 이용하기 위해(때문에)
		L1ItemInstance dummy = new L1ItemInstance();
		L1ShopItem shopItem = null;
		L1Item item = null;
		L1Item template = null;
		for (int i = 0; i < shopItems.size(); i++) {
			shopItem = shopItems.get(i);
			item = shopItem.getItem();
			int price = shopItem.getPrice();
			int itemId = item.getItemId();
			writeD(i);
			writeH(shopItem.getItem().getGfxId());
			writeD(price);
			if (itemId >= 21113 && itemId <= 21120){
				writeS(item.getName() + " [3시간]");
			} else if(itemId == 60499 || itemId == 60500){
				writeS(item.getName() + " [1일]");
			} else if(shopItem.getEnchant() > 0){
				writeS("+"+shopItem.getEnchant()+" "+ item.getNameId());
			} else if (itemId == 430003 || itemId == 430505 || itemId == 430506
					|| itemId == 41915 || itemId == 5000034 || itemId == 60233){
				writeS(item.getName() + " [7일]");
			} else if (shopItem.getPackCount() > 1) {
				writeS(item.getName() + " (" + shopItem.getPackCount() + ")");
			} else if (shopItem.getItem().getMaxUseTime() > 0) {
				writeS(item.getName() + " [" + item.getMaxUseTime() + "]");
			} else {
				writeS(item.getName());
			}

			if (item.getType() == 6 && item.getType2() == 0) {
				writeD(0x33);
			} else if (item.getType() == 15 && item.getType2() == 2) {
				writeD(0x02);
			} else if (item.getType() == 8 && item.getType2() == 0) {
				writeD(0x6);
			} else {
				writeD(item.getUseType());
			}
			
			template = ItemTable.getInstance().getTemplate(item.getItemId());
			if (template == null) {
				writeC(0);
			} else {
				dummy.setItem(template);
				byte[] status = dummy.getStatusBytes();
				writeC(status.length);
				for (byte b : status) {
					writeC(b);
				}
			}
		}
		if (npcId == 100387 || npcId == 6502 || npcId == 68000 || npcId == 61206 || npcId == 14583 || npcId == 14916)// 블룸
			writeH(0x00);
		else if (npcId == 47110)
			writeH(16848);
		else if (npcId == 14918)
			writeH(16661);
		else if (npcId == 14919)
			writeH(17839);
		else if (npcId == 47100)
			writeH(25184);
		else if (npcId == 471001)
			writeH(25184);
		else if (npcId == 471007)
			writeH(25184);
		else if (npcId == 471002)
			writeH(25184);
		else if (npcId == 471003)
			writeH(25184);
		else if (npcId == 471004)
			writeH(25184);
		else if (npcId == 471005)
			writeH(25184);
		else if (npcId == 471006)
			writeH(25184);
		else if (npcId == 47102)
			writeH(17619);
		else if (npcId == 100725)
			writeH(0xFFFD);
		else if (npcId == 47111)
			writeH(25906);
		else if (npcId == 7009)
			writeH(0x43AB);
		else
			writeH(0x0A6F);
	}

	@Override
	public byte[] getContent() throws IOException {
		return _bao.toByteArray();
	}
}

package l1j.server.server.serverpackets;

import java.io.IOException;

import l1j.server.server.Opcodes;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.utils.BinaryOutputStream;

public class S_Party extends ServerBasePacket {

	private static final String _S_Party = "[S] S_Party";

	/** 세부속성에 대한 타입 **/
	public static final int JOIN_YN_OK = 1;
	public static final int MEMBER_TELEPORT = 2;
	public static final int MEMBER_HPMP_CHANGE = 3;
	public static final int MEMBER_MARK_CHANGE = 4;

	/** 패킷에 대한 타입 **/
	public static final int PACKET_NONE = 0;
	public static final int PACKET_TYPE_NEW_MEMBER = 0x22;
	public static final int PACKET_TYPE_CHANGELEADER = 0x0a;
	public static final int PACKET_TYPE_LEAVE_MEMBER = 0x12;

	//SC_PARTY_MEMBER_MARK_CHANGE_NOTI 
	//SC_PARTY_SPELL_AVATAR_NOTI 
	
	/** 옵코드에 대한 타입 **/
	public static final int OPCODE_TYPE_PARTY_MEMBER_LIST = 823; // 파티맴버
	public static final int OPCODE_TYPE_PARTY_MEMBER_LIST_CHANGE = 824; // 파티정보변경
	public static final int OPCODE_TYPE_PARTY_MEMBER_STATUS = 825; // 파티맴버 상태 표식 텔시 피엠피 변동시 
	public static final int OPCODE_TYPE_PARTY_OPERATION_RESULT_NOTI = 539; // 초대수락/거절시 거절시 08 01 / 수락시 08 02
	public static final int OPCODE_TYPE_PARTY_SYNC_PERIODIC_INFO = 827; //파티시 주기적으로 옴 //미니맵 파티위치 정보

	private byte[] _byte = null;

	public S_Party(int type, int sub_code, L1PcInstance pc) {
		writeC(Opcodes.S_EXTENDED_PROTOBUF);
		writeH(type);

		switch (type) {
		case OPCODE_TYPE_PARTY_MEMBER_LIST_CHANGE:
			writeC(sub_code);
			switch (sub_code) {
			case PACKET_TYPE_LEAVE_MEMBER:
			case PACKET_TYPE_CHANGELEADER:
				writeBit(pc.getName().getBytes().length);
				writeByte(pc.getName().getBytes());
				break;
			case PACKET_TYPE_NEW_MEMBER:
				byte[] char_info = sendPartyMemberPacket(pc);
				writeBit(char_info.length);
				writeByte(char_info);
				break;
			}
			break;
		case OPCODE_TYPE_PARTY_OPERATION_RESULT_NOTI:
			writeC(0x08);
			writeBit(sub_code == 0 ? 1 : 2);
			writeC(0x12);
			writeBit(pc.getName().getBytes().length);
			writeByte(pc.getName().getBytes());
			break;
		case OPCODE_TYPE_PARTY_SYNC_PERIODIC_INFO:
			for(L1PcInstance member : pc.getParty().getMembers()){
				byte[] char_info = sendPartyMemberRefreshPacket(member);
				writeC(0x0a);
				writeBit(char_info.length);
				writeByte(char_info);
			}
			
			break;
		case OPCODE_TYPE_PARTY_MEMBER_LIST:
			writeC(0x0a);
			writeBit(pc.getParty().getLeader().getName().getBytes().length);
			writeByte(pc.getParty().getLeader().getName().getBytes());
			for(L1PcInstance member : pc.getParty().getMembers()){
				byte[] char_info = sendPartyMemberPacket(member);
				writeC(0x12);
				writeBit(char_info.length);
				writeByte(char_info);
			}
			break;
		case OPCODE_TYPE_PARTY_MEMBER_STATUS:
			switch(sub_code){
			case MEMBER_TELEPORT:
				writeC(0x0a);
				writeBit(pc.getName().getBytes().length);
				writeByte(pc.getName().getBytes());
				writeC(0x30);
				writeBit(pc.getMapId());
				writeC(0x38);
				writeBit(pc.getX(), pc.getY());
				break;
			case MEMBER_HPMP_CHANGE:
				writeC(0x0a);
				writeBit(pc.getName().getBytes().length);
				writeByte(pc.getName().getBytes());
				int hpRatio = pc.getMaxHp() == 0 ? 0 : 100 * pc.getCurrentHp() / pc.getMaxHp();
				int mpRatio = pc.getMaxMp() == 0 ? 0 : 100 * pc.getCurrentMp() / pc.getMaxMp();
				writeC(0x18);
				writeBit(hpRatio);
				writeC(0x20);
				writeBit(mpRatio);
				break;
			case MEMBER_MARK_CHANGE:
				writeC(0x0a);
				writeBit(pc.getName().getBytes().length);
				writeByte(pc.getName().getBytes());
				writeC(0x40);
				writeBit(pc.getPartySign());
				break;
			}
			break;
		}
		writeH(0);
	}

	public S_Party(String htmlid, int objid) {
		buildPacket(htmlid, objid, "", "", 0);
	}

	public S_Party(String htmlid, int objid, String partyname, String partymembers) {
		buildPacket(htmlid, objid, partyname, partymembers, 1);
	}

	private void buildPacket(String htmlid, int objid, String partyname, String partymembers, int type) {
		writeC(Opcodes.S_HYPERTEXT);
		writeD(objid);
		writeS(htmlid);
		writeH(type);
		writeH(0x02);
		writeS(partyname);
		writeS(partymembers);
	}

	private byte[] sendPartyMemberRefreshPacket(L1PcInstance pc) {
		BinaryOutputStream os = new BinaryOutputStream();

		os.writeC(0x0a);
		os.writeBit(pc.getName().getBytes().length);
		os.writeByte(pc.getName().getBytes());

		os.writeC(0x30);
		os.writeBit(pc.getMapId());

		os.writeC(0x38);
		os.writeBit(pc.getX(), pc.getY());

		try {
			os.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return os.getBytes();
	}

	private byte[] sendPartyMemberPacket(L1PcInstance pc) {
		BinaryOutputStream os = new BinaryOutputStream();

		os.writeC(0x0a);
		os.writeBit(pc.getName().getBytes().length);
		os.writeByte(pc.getName().getBytes());

		os.writeC(0x10);
		os.writeBit(pc.getId());

		os.writeC(0x18);
		os.writeBit(pc.getId());

		os.writeC(0x20);
		os.writeBit(pc.getType());

		os.writeC(0x28);
		os.writeBit(pc.get_sex());

		int hpRatio = pc.getMaxHp() == 0 ? 0 : 100 * pc.getCurrentHp() / pc.getMaxHp();
		int mpRatio = pc.getMaxMp() == 0 ? 0 : 100 * pc.getCurrentMp() / pc.getMaxMp();

		os.writeC(0x30);
		os.writeBit(hpRatio);

		os.writeC(0x38);
		os.writeBit(mpRatio);

		os.writeC(0x40);
		os.writeBit(pc.getMapId());

		os.writeC(0x48);
		os.writeBit(pc.getX(), pc.getY());

		os.writeC(0x50); // set_party_mark
		os.writeBit(pc.getPartySign());

		os.writeC(0x60); // set_server_no
		os.writeBit(0x64);

		try {
			os.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return os.getBytes();
	}

	@Override
	public byte[] getContent() {
		if (_byte == null) {
			_byte = _bao.toByteArray();
		}

		return _byte;
	}

	@Override
	public String getType() {
		return _S_Party;
	}
}

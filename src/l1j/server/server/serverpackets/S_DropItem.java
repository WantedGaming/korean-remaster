/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */

package l1j.server.server.serverpackets;

import java.text.SimpleDateFormat;
import java.util.Locale;

import l1j.server.server.Opcodes;
import l1j.server.server.model.Instance.L1ItemInstance;

// Referenced classes of package l1j.server.server.serverpackets:
// ServerBasePacket

public class S_DropItem extends ServerBasePacket {
	private static final SimpleDateFormat sdf = new SimpleDateFormat(
			"MM-dd HH:mm", Locale.KOREA);
	private static final String _S__OB_DropItem = "[S] S_DropItem";

	private byte[] _byte = null;

	public S_DropItem(L1ItemInstance item) {
		buildPacket(item);
	}

	private void buildPacket(L1ItemInstance item) {
		// int addbyte = 0;
		// int addbyte1 = 1;
		// int addbyte2 = 13;
		// int setting = 4;
		writeC(Opcodes.S_PUT_OBJECT);
		writeH(item.getX());
		writeH(item.getY());
		writeD(item.getId());
		writeH(item.getItem().getGroundGfxId());
		writeC(0);
		writeC(0);
		if (item.isNowLighting()) {
			writeC(item.getItem().getLightRange());
		} else {
			writeC(0);
		}
		writeC(0);
		writeD(item.getCount());
		writeH(0);

		/**
		 * 아이템드랍시 인첸/속성 표시 by사부
		 */
		/***********************************************************************
		 ***********************************************************************/
		StringBuffer SB = null;
		SB = new StringBuffer();
		if (item.isIdentified()) {
			if (item.getItem().getType2() == 1 || item.getItem().getType2() == 2) {
				switch (item.getAttrEnchantLevel()) {
				case 1:
					SB.append("화령:1단 ");
					break;
				case 2:
					SB.append("화령:2단 ");
					break;
				case 3:
					SB.append("화령:3단 ");
					break;
				case 4:
					SB.append("화령:4단 ");
					break;
				case 5:
					SB.append("화령:5단 ");
					break;
				case 6:
					SB.append("수령:1단 ");
					break;
				case 7:
					SB.append("수령:2단 ");
					break;
				case 8:
					SB.append("수령:3단 ");
					break;
				case 9:
					SB.append("수령:4단 ");
					break;
				case 10:
					SB.append("수령:5단 ");
					break;
				case 11:
					SB.append("풍령:1단 ");
					break;
				case 12:
					SB.append("풍령:2단 ");
					break;
				case 13:
					SB.append("풍령:3단 ");
					break;
				case 14:
					SB.append("풍령:4단 ");
					break;
				case 15:
					SB.append("풍령:5단 ");
					break;
				case 16:
					SB.append("지령:1단 ");
					break;
				case 17:
					SB.append("지령:2단 ");
					break;
				case 18:
					SB.append("지령:3단 ");
					break;
				case 19:
					SB.append("지령:4단 ");
					break;
				case 20:
					SB.append("지령:5단 ");
					break;
				default:
					break;
				}
				// 인첸 +0 일때도 표기되게 하실분은 밑에 if (item.getEnchantLevel() >= 0) {로 교체
				// by사부
				if (item.getEnchantLevel() > 0) {
					SB.append("+" + item.getEnchantLevel() + " ");
				} else if (item.getEnchantLevel() < 0) {
					SB.append(String.valueOf(item.getEnchantLevel()) + " ");
				}
			}
		}
		SB.append(item.getItem().getNameId());
		int itemId = item.getItem().getItemId();
		int isId = item.isIdentified() ? 1 : 0;
		if (item.getCount() > 1) {
			SB.append(" (" + item.getCount() + ")");
		} else {
			/*
			 * } else if ((itemId == 40006 || itemId == 40007 || itemId == 40008
			 * || itemId == 40009 || itemId == 140006 || itemId == 140008) &&
			 * isId == 1) { sb.append(" (" + item.getChargeCount() + ")");
			 */
			if (item.getItem().getLightRange() != 0 && item.isNowLighting()) {
				SB.append(" ($10)");
			}
		}

		if (itemId == 40312 || itemId == 49312) {
			if (item.getEndTime() != null) {
				SB.append(" [" + sdf.format(item.getEndTime().getTime())
						+ "] 식별번호 : " + item.getKey());
			}
		}

		if (itemId == 60285) {// 훈련소 열쇠
			if (item.getEndTime() != null) {
				int mapid = item.getKey() - 1399;
				SB.append(" (" + (mapid > 9 ? mapid : "0" + mapid) + ") ["
						+ sdf.format(item.getEndTime().getTime()) + "]");
			}
		}
		writeS(SB.toString());// by사부
		/***********************************************************************
		 ***********************************************************************/
		writeC(0);
		writeD(0);
		writeD(0);
		writeC(0xFF);
		writeC(0);
		writeC(0);
		writeH(0xFFFF);
		writeC(0);
		writeC(0x08);
		writeC(0x12);
		writeC(0x00);
	}

	@Override
	public byte[] getContent() {
		if (_byte == null) {
			_byte = _bao.toByteArray();
		}
		return _byte;
	}

	@Override
	public String getType() {
		return _S__OB_DropItem;
	}

}

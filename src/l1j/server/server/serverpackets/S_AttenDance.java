package l1j.server.server.serverpackets;

import java.util.ArrayList;

import l1j.server.Config;
import l1j.server.server.Opcodes;
import l1j.server.server.datatables.AttenDanceTable;
import l1j.server.server.datatables.AttenDanceTable.Attendtemp;
import l1j.server.server.datatables.CharacterAttendTable.idTemp;
import l1j.server.server.datatables.ItemTable;
import l1j.server.server.model.Instance.L1ItemInstance;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.templates.L1Item;
import l1j.server.server.utils.BinaryOutputStream;


public class S_AttenDance extends ServerBasePacket {
	private static final String S_ACTION_UI = "[S] S_ACTION_UI";
	private byte[] _byte = null;

	public static final int WatchCreate = 544;
	public static final int WatchPcPorfile = 1004;
	public static final int WatchTimeOver = 1005;
	public static final int WatchStoreItem = 1007;
	public static final int WatchItemList = 548;

	// 13 23 02 08 01 10 02 18 00 00 d9
	public S_AttenDance(int subCode, int type, int id) {
		writeC(Opcodes.S_EXTENDED_PROTOBUF);
		writeH(subCode);
		
		switch (subCode) {
		case WatchStoreItem:
			writeC(0x08);
			writeC(type);
			writeC(0x10);
			writeC(id);
			writeC(0x18);
			writeC(3);
			break;
		}
		writeH(0);
	}
	public S_AttenDance(int subCode) {
		writeC(Opcodes.S_EXTENDED_PROTOBUF);
		writeH(subCode);
		switch (subCode) {
		case WatchCreate:
			writeC(0x08);// 체크시간
			writeBit(60);
			writeC(0x10);// 리체크시간
			writeBit(1440);
			writeC(0x18);
			writeC(1);
			writeC(0x20);
			writeC(1);
			writeC(0x28);
			writeC(2);
			break;
		}
		writeH(0);
	}

	public S_AttenDance(int subCode, int type) {
		writeC(Opcodes.S_EXTENDED_PROTOBUF);
		writeH(subCode);
		switch (subCode) {
		case WatchItemList:
			if (type == 0) {
				// 일반 아이템 리스트
				if (AttenDanceTable.getInstance().getNormalSize() > 0) {
					writeC(0x08);
					writeC(0x00);
					byte[] status = itemProfile(0);
					for (byte b : status) {
						writeC(b);
					}
				}
			} else {
				// pc방 아이템 리스트
				if (AttenDanceTable.getInstance().getPcRoomSize() > 0) {
					writeC(0x08);
					writeC(0x01);
					byte[] status = itemProfile(1);
					for (byte b : status) {
						writeC(b);
					}
				}
			}
			break;
		}
		writeH(0);
	}
	
	public S_AttenDance(int subCode, int type, boolean flag) {
		writeC(Opcodes.S_EXTENDED_PROTOBUF);
		writeH(subCode);
		switch (subCode) {
		case WatchTimeOver:
			writeC(0x08);
			writeBit(type);
			writeC(0x10);
			writeBit(flag ? 0x01 : 0x00);
			writeC(0x18);
			writeBit(2);
			break;
		
		}
		
		writeH(0);
	}

	public S_AttenDance(L1PcInstance pc, int subCode, int type) {
		writeC(Opcodes.S_EXTENDED_PROTOBUF);
		writeH(subCode);
		switch (subCode) {
		case WatchPcPorfile:
			int normal_minute = (3600 - pc.attendTemp.time_Normal) / 60;
			int pc_minute = (3600 - pc.attendTemp.time_PcRoom) / 60;
			int normal_size = writeLenght(normal_minute) + 7;
			int pc_size = writeLenght(pc_minute) + 7;
			writeC(0x0a);
			writeBit(normal_size);
			writeC(0x08);
			writeBit(0); // 일반 0 , 피씨방 1
			writeC(0x10);
			writeBit(normal_minute); // 출석 남은 시간
			writeC(0x18);
			writeBit(pc.attendTemp.Count_Normal == 1 ? 1 : 0); // 완료한 총 횟수
			writeC(0x20);
			writeBit(0x01); // 완료가능한 총 횟수

			writeC(0x0a);
			writeBit(pc_size);
			writeC(0x08);
			writeBit(1); // 일반 0 , 피씨방 1
			writeC(0x10);
			writeBit(pc.PC방_버프 ? pc_minute : 60); // 피씨방 출석 남은시간
			writeC(0x18);
			writeBit(pc.attendTemp.Count_PcRoom == 1 ? 1 : 0); // 완료한 총 횟수
			writeC(0x20);
			writeBit(0x01); // 완료가능한 총 횟수
			
			for (int i = 0; i < pc.attendTemp.Nomarlist.size(); i++) {
				writeC(0x12);
				writeBit(0x08);
				writeC(0x08);
				writeBit(i + 1);
				writeC(0x10);
				writeBit(0);
				writeC(0x18);
				int state = pc.attendTemp.Nomarlist.get(i).state == 2 ? 3 : pc.attendTemp.Nomarlist.get(i).state == 1 ? 2 : 1;
				writeBit(state);
				writeC(0x20);
				int time = state == 3 || state == 2 ? 60 : 0;
				writeBit(time);

				if (state == 1)
					break;
			}
			
			for (int i = 0; i < pc.attendTemp.PcRoomlist.size(); i++) {
				writeC(0x12);
				writeBit(0x08);
				writeC(0x08);
				writeBit(i + 1);
				writeC(0x10);
				writeBit(1);
				writeC(0x18);
				int state = pc.attendTemp.PcRoomlist.get(i).state == 2 ? 3 : pc.attendTemp.PcRoomlist.get(i).state == 1 ? 2 : 1;
				writeBit(state);
				writeC(0x20);
				int time = state == 3 || state == 2 ? 60 : 0;
				writeBit(time);

				if (state == 1)
					break;
			}
			
			/*writeC(0x0a);
			byte[] status = PcPorfile(pc, 0);
			writeBit(status.length);
			for (byte b : status) {
				writeC(b);
			}

			writeC(0x0a);
			status = PcPorfile(pc, 1);
			writeBit(status.length);
			for (byte b : status) {
				writeC(b);
			}
			// 시계 타겟으로 불들어오게 하는 부분
			if (type == 0) {
				writeC(0x10);
				writeC(0x00);
				writeC(0x18);
				writeC(0x01);
				writeC(0x18);
				writeC(0x00);
			} else {
				writeC(0x10);
				writeC(0x01);
				writeC(0x18);
				writeC(0x00);
				writeC(0x18);
				writeC(0x01);
			}*/
			break;
		}
		writeH(0);
	}

	private byte[] PcPorfile(L1PcInstance pc, int page) {
		byte[] result = null;
		try {
			BinaryOutputStream os = new BinaryOutputStream();
			os.writeC(0x08); // 페이지 0일반,1피방
			os.writeC(page);
			if (page == 0) {
				for (idTemp temp : pc.attendTemp.Nomarlist) {
					os.writeC(0x12);
					os.writeC(0x06);
					os.writeC(0x08);
					os.writeC(temp.id);
					os.writeC(0x10);
					if (temp.state >= 2) {
						temp.state = 2;
						os.writeC(temp.state);
					} else {
						os.writeC(temp.state);
					}
					os.writeC(0x18);
					os.writeC(0x00);
				}
				os.writeC(0x18);
				os.writeC(pc.attendTemp.isNormal); // 0 활성, 1출석완료
				os.writeC(0x20);
				os.writeC(pc.attendTemp.Count_Normal); // 완료한 겟수의 양
				os.writeC(0x28);
				os.writeC(Config.NormalMaxCount); // 하루에 받을 수 있는 퀘스트 맥스 수치 = 컨피그
				os.writeC(0x30);
				os.writeBit(pc.attendTemp.time_Normal); // 현재 지나간시간
				os.writeC(0x38);
				os.writeBit(Config.NormalMaxTime); // 최대시간 - 컨피그 설정

			} else {
				for (idTemp temp : pc.attendTemp.PcRoomlist) {
					os.writeC(0x12);
					os.writeC(0x06);
					os.writeC(0x08);
					os.writeC(temp.id);
					os.writeC(0x10);
					if (temp.state >= 2) {
						temp.state = 2;
						os.writeC(temp.state);
					} else {
						os.writeC(temp.state);
					}
					os.writeC(0x18);
					os.writeC(0x00);
				}
				os.writeC(0x18);
				if (pc.PC방_버프) {
					os.writeC(pc.attendTemp.isPcRoom); // 0 활성, 1출석완료
				} else {
					os.writeC(1);
				}
				os.writeC(0x20);
				os.writeC(pc.attendTemp.Count_PcRoom); // 완료한 겟수의 양
				os.writeC(0x28);
				os.writeC(Config.PcRoomMaxCount); // 하루에 받을 수 있는 퀘스트 맥스 수치 = 컨피그
				os.writeC(0x30);
				os.writeBit(pc.attendTemp.time_PcRoom); // 현재 지나간시간
				os.writeC(0x38);
				os.writeBit(Config.PcRoomMaxTime); // 최대시간 - 컨피그 설정
			}
			result = os.getBytes();
			os.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	private byte[] itemProfile(int page) {
		byte[] result = null;
		try {
			BinaryOutputStream os = new BinaryOutputStream();
			ArrayList<Attendtemp> itemlist = null;
			if (page == 0) {
				itemlist = AttenDanceTable.getInstance().getNormalList();
			} else {
				itemlist = AttenDanceTable.getInstance().getPcRoomList();
			}

			byte[] status = null;
			for (Attendtemp temp : itemlist) {
				status = itemState(temp.item_id, temp.item_count);
				os.writeC(0x12);
				os.writeBit(status.length + 4);
				os.writeC(0x08);
				os.writeC(temp.id);
				os.writeC(0x12);
				os.writeBit(status.length);
				for (byte b : status) {
					os.writeC(b);
				}
			}
			result = os.getBytes();
			os.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public byte[] itemState(int itemid, int count){
		byte[] result = null;
		try {
			BinaryOutputStream os = new BinaryOutputStream();
			os.writeC(0x08); 
			os.writeC(0x02);
			L1Item item = ItemTable.getInstance().getTemplate(itemid);
			L1ItemInstance temp = ItemTable.getInstance().FunctionItem(item);
			os.writeC(0x10); 
			os.writeBit(item.getItemDescId());
			os.writeC(0x18);
			os.writeBit(count);
			os.writeC(0x22); //영어이름이던데?
			os.writeC(0x00);
			os.writeC(0x28); //불명 찾아야함
			os.writeC(0x00);
			os.writeC(0x30); //이미지
			os.writeBit(item.getGfxId());
			os.writeC(0x38); //불명
			os.writeC(0x01);
			os.writeC(0x42); //이름
			os.writeC(item.getNameId().getBytes().length);
			os.writeByte(item.getNameId().getBytes());
			
			os.writeC(0x4a); 
			byte[] status = temp.getStatusBytes();
			os.writeBit(status.length);
			for (byte b : status) {
				os.writeC(b);
			}
			
			os.writeC(0x50); //불명 0x97, 0x87 2가지 형태인데 찾아야함
			os.writeC(0x97);  
			os.writeC(0xff);
			os.writeC(0xff);
			os.writeC(0xff);
			os.writeC(0xff);
			os.writeC(0xff);
			os.writeC(0xff);
			os.writeC(0xff);
			os.writeC(0xff);
			os.writeC(0x01);
			result = os.getBytes();
			os.close();
		} catch (Exception e) {
			e.printStackTrace();
		}	
		return result;
	}

	@Override
	public byte[] getContent() {
		if (_byte == null) {
			_byte = getBytes();
		}
		return _byte;
	}

	@Override
	public String getType() {
		return S_ACTION_UI;
	}
}
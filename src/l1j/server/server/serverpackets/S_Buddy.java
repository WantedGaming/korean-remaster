/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 * Author: ChrisLiu.2007.07.20
 */
package l1j.server.server.serverpackets;

import l1j.server.server.Opcodes;
import l1j.server.server.model.L1Buddy;
import l1j.server.server.model.L1World;

public class S_Buddy extends ServerBasePacket {
	private static final String _S_Buddy = "[S] _S_Buddy";
	private static final String _HTMLID = "buddy";

	private byte[] _byte = null;

	public S_Buddy(int objId, L1Buddy buddy) {
		buildPacket(objId, buddy);
	}

	/*
	 * 0000: df 51 01 08 01 12 08 0a 02 b0 a1 10 00 1a 00 12 08 0a 02 b3 aa 10
	 * 00 1a 00 12 08 0a 02 b4 d9 10 00 1a 00 68 ee ...h.
	 */
	private void buildPacket(int objId, L1Buddy buddy) {
		writeC(Opcodes.S_EXTENDED_PROTOBUF);
		writeC(0x51);
		writeC(0x01);
		writeC(0x08);
		writeC(0x01);
		for (String buddyname : buddy.getBuddyList()) {
			writeC(0x12);
			writeC(buddyname.getBytes().length + 6);// 이름+6
			writeC(0x0a);
			writeC(buddyname.getBytes().length);// 이름길이
			writeS2(buddyname);
			writeC(0x10);
			if (L1World.getInstance().getPlayer(buddyname) == null) {
				writeC(0x00);// 접속여부
			} else {
				writeC(0x01);// 접속여부
			}
			writeC(0x1a);
			writeC(0x00);
		}
		writeH(0x00);
	}

	@Override
	public byte[] getContent() {
		if (_byte == null) {
			_byte = _bao.toByteArray();
		}
		return _byte;
	}

	@Override
	public String getType() {
		return _S_Buddy;
	}
}
